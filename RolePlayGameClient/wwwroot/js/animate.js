﻿function updateStats(health, armor) {
    document.getElementById("healthBar").style.width = health + "%";
    document.getElementById("armorBar").style.width = armor + "%";
}

function updateBiology(hunger, thirst) {
    document.getElementById("hungerBar").style.width = hunger + "%";
    document.getElementById("thirstBar").style.width = thirst + "%";
}

function updateSpeedometer(speed, fuel) {
    document.getElementById("speedGauge").dataset.value = speed;
    document.getElementById("fuelGauge").dataset.value = fuel;

}

function updateVoiceRange(rangeValue) {
    document.getElementById("voiceBarGauge").dataset.value = rangeValue;
}

function setLeftMessageText(head, text) {
    document.getElementById("leftHead").innerHTML = head;
    document.getElementById("leftText").innerHTML = text;
}

function setRightMessageText(head, text) {
    document.getElementById("rightHead").innerHTML = head;
    document.getElementById("rightText").innerHTML = text;
}

function showLeftMessage() {
    $("#leftMessageWindow").removeClass("animated slideOutLeft 2s");
    $("#leftMessageWindow").addClass("animated slideInLeft 2s");
}

function hideLeftMessage() {
    $("#leftMessageWindow").removeClass("animated slideInLeft 2s");
    $("#leftMessageWindow").addClass("animated slideOutLeft 2s");
}

function showRightMessage() {
    $("#rightMessageWindow").removeClass("animated slideOutRight 2s");
    $("#rightMessageWindow").addClass("animated slideInRight 2s");
}

function hideRightMessage() {
    $("#rightMessageWindow").removeClass("animated slideInRight 2s");
    $("#rightMessageWindow").addClass("animated slideOutRight 2s");
}

function showSpeedometer() {
    $("#speedGauge").removeClass("animated fadeOut 2s");
    $("#fuelGauge").removeClass("animated fadeOut 1s");
    $("#speedGauge").addClass("animated fadeIn 2s");
    $("#fuelGauge").addClass("animated  fadeIn 4s");
}

function hideSpeedometer() {
    $("#speedGauge").removeClass("animated fadeIn 2s");
    $("#fuelGauge").removeClass("animated fadeIn 4s");
    $("#speedGauge").addClass("animated fadeOut 2s");
    $("#fuelGauge").addClass("animated fadeOut 1s");
}

function showProgressBar() {
    document.getElementById("progressBar").style.width = "0%";
    $("#progressBarRow").removeClass("animated fadeOut 2s");
    $("#progressBarRow").addClass("animated fadeIn 2s");
}

function hideProgressBar() {
    $("#progressBarRow").removeClass("animated fadeIn 2s");
    $("#progressBarRow").addClass("animated fadeOut 2s");
}

function updateProgressBar(actionName, value) {
    document.getElementById("progressTitle").innerHTML = actionName;
    document.getElementById("progressBar").style.width = value + "%";
}