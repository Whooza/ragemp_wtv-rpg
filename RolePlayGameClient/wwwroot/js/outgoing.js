﻿window.charCreator = {
    setCreatorCamera: function (value) {
        mp.trigger("setCreatorCamera", value);
        return true;
    },
    changeCameraFocus: function (value) {
        mp.trigger("changeCameraFocus", value);
        return true;
    },
    rotateCharacter: function (value) {
        mp.trigger("rotateCharacter", value);
        return true;
    },
    setHeadOverlay: function (index, value) {
        mp.trigger("setHeadOverlay", index, value);
        return true;
    },
    setHeadOverlayColor: function (overlayId, overlayColor, value) {
        mp.trigger("setHeadOverlayColor", overlayId, overlayColor, value);
        return true;
    },
    setFaceFeature: function (index, value) {
        mp.trigger("setFaceFeature", index, value);
        return true;
    },
    setEyeColor: function (eyesColor) {
        mp.trigger("setEyeColor", eyesColor);
        return true;
    },
    setHairColor: function (firstHairColor, secondHairColor) {
        mp.trigger("setHairColor", firstHairColor, secondHairColor);
        return true;
    },
    setComponentVariation: function (componentId, drawableId, textureId) {
        mp.trigger("setComponentVariation", componentId, drawableId, textureId);
        return true;
    },
    setHeadBlendData: function (firstHeadShape, secondHeadShape, firstSkinTone, secondSkinTone, headMix, skinMix) {
        mp.trigger("setHeadBlendData", firstHeadShape, secondHeadShape, firstSkinTone, secondSkinTone, headMix, skinMix);
        return true;
    }
};