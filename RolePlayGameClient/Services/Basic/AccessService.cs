﻿using System.Threading.Tasks;
using RolePlayGameData.Configs;
using RolePlayGameData.Enums;

namespace RolePlayGameClient.Services.Basic
{
    public class AccessService
    {
        public Task<AccessEnums> GetStatus(string webToken)
        {
            return ServerCfg.AccessServicesEnabled ? Task.Run(() => webToken == "" ? AccessEnums.Error : AccessEnums.Success)
                : Task.Run(() => AccessEnums.Success);
        }
    }
}