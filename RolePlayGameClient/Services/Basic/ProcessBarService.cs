﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RolePlayGameData.Configs;
using RolePlayGameData.Enums;
using RolePlayGameData.Models;

namespace RolePlayGameClient.Services.Basic
{
    public class ProcessBarService
    {
        private bool _continue;

        public List<CookingRecipeModel> AllCookingRecipes = new List<CookingRecipeModel>();

        public Action OnChange;

        public ProcessBarService()
        {
            AllCookingRecipes
                .Concat(CookingCfg.CookingLvLOneRecipes)
                .Concat(CookingCfg.CookingLvLTwoRecipes)
                .Concat(CookingCfg.CookingLvLThreeRecipes)
                .Concat(CookingCfg.CookingLvLFourRecipes)
                .Concat(CookingCfg.CookingLvLFiveRecipes)
                .Concat(CookingCfg.CookingLvLSixRecipes)
                .ToList();
        }

        public List<JobRecipeModel> ProcessingRecipes => ProcessorCfg.ProcessingRecipes;

        public string ProgressName { get; private set; } = "TestProcessName";
        public float Progress { get; private set; } = 69f;
        public bool IsActive { get; private set; }

        public void StartProgress(string social, string progressName, CookingLevelEnums level)
        {
            if (IsActive) return;

            ProgressName = progressName;
            ProgressAction(social, level);
        }

        private async void ProgressAction(string social, CookingLevelEnums level)
        {
            if (IsActive) return;

            IsActive = true;

            var recipe = FindRecipe(ProgressName);
            var processTick = recipe.ProcessTime / 20;

            for (var i = 0; i < 20; i++)
            {
                if (!_continue)
                {
                    ProgressName = "abgebrochen";
                    await Task.Delay(500);
                    Progress = 0f;
                    IsActive = false;
                    return;
                }

                Progress = i * 5;

                await Task.Delay(processTick);

                StateChanged();
            }

            SendGiveNewItem(social, 12);

            IsActive = false;
        }

        private CookingRecipeModel FindRecipe(string recipeName)
        {
            return AllCookingRecipes.FirstOrDefault(x => x.RecipeName == recipeName);
        }

        public List<CookingRecipeModel> GetFilteredCookingRecipes(CookingCategoryEnums category,
            CookingLevelEnums level)
        {
            return AllCookingRecipes.Where(x => x.Category == category && x.Level == level).OrderBy(x => x.RecipeName)
                .ToList();
        }

        private void SendGiveNewItem(string social, uint itemName)
        {
        }

        private void StateChanged()
        {
            OnChange?.Invoke();
        }
    }
}