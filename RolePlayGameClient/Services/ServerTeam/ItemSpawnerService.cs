﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using RolePlayGameData.Configs;
using RolePlayGameData.Enums;
using RolePlayGameData.Models;

namespace RolePlayGameClient.Services.ServerTeam
{
    public class ItemSpawnerService
    {
        private readonly HttpClient _http = new HttpClient();
        private bool _inProgress;

        public Action OnChange;

        public ItemSpawnerService() => _http.BaseAddress = ServerCfg.ClientSiteRunsLocal
                ? new Uri(ServerCfg.ApiLocal) : new Uri(ServerCfg.ApiRemote);

        public List<ItemModel> AllItems { get; private set; } = new List<ItemModel>();
        public List<WeaponModel> AllWeapons { get; private set; } = new List<WeaponModel>();

        public List<ItemModel> SelectedItems { get; private set; } = new List<ItemModel>();
        public List<WeaponModel> SelectedWeapons { get; private set; } = new List<WeaponModel>();

        public SortedDictionary<string, int> ItemTypes { get; } = new SortedDictionary<string, int>();
        public SortedDictionary<string, int> WeaponTypes { get; } = new SortedDictionary<string, int>();

        public void OnInit()
        {
            _inProgress = true;

            foreach (ItemTypeEnums itemType in Enum.GetValues(typeof(ItemTypeEnums)))
                ItemTypes.Add(itemType.ToString(), (int) itemType);

            foreach (WeaponTypeEnums weaponType in Enum.GetValues(typeof(WeaponTypeEnums)))
                WeaponTypes.Add(weaponType.ToString(), (int) weaponType);

            AllItems = ItemCfg.ItemConfig;
            AllWeapons = WeaponCfg.WeaponConfig;

            SelectedItems = AllItems;
            SelectedWeapons = AllWeapons;

            _inProgress = false;
        }

        public void SortItemList(int type)
        {
            _inProgress = true;

            SelectedItems = AllItems
                .Where(x => (int) x.ItemType == type)
                .OrderBy(x => x.DisplayedName)
                .ToList();

            StateChanged();

            _inProgress = false;
        }

        public void SortWeaponList(int type)
        {
            _inProgress = true;

            SelectedWeapons = AllWeapons
                .Where(x => (int) x.WeaponType == type)
                .OrderBy(x => x.DisplayedName)
                .ToList();

            StateChanged();

            _inProgress = false;
        }

        public void GiveItem(string social, int item)
        {
            if (_inProgress) return;

            _inProgress = true;

            _http.DefaultRequestHeaders.Clear();
            _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
            _http.DefaultRequestHeaders.Add("social", social);
            _http.DefaultRequestHeaders.Add("item", item.ToString());

            var result = _http.GetAsync("Api/ItemSpawner/GiveItem").Result;

            _inProgress = false;
        }

        public void GiveWeapon(string social, string weapon)
        {
            if (_inProgress) return;

            _inProgress = true;

            _http.DefaultRequestHeaders.Clear();
            _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
            _http.DefaultRequestHeaders.Add("social", social);
            _http.DefaultRequestHeaders.Add("weapon", weapon);

            var result = _http.GetAsync("Api/ItemSpawner/GiveWeapon").Result;

            _inProgress = false;
        }

        public void CloseItemSpawner(string social)
        {
            if (_inProgress) return;

            _inProgress = true;

            _http.DefaultRequestHeaders.Clear();
            _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
            _http.DefaultRequestHeaders.Add("social", social);

            var result = _http.GetAsync("Api/Window/DestroyWindow").Result;

            _inProgress = false;
        }

        private void StateChanged() => OnChange?.Invoke();
    }
}