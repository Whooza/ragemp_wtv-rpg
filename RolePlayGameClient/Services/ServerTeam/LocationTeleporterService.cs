﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using RolePlayGameData.Configs;
using RolePlayGameData.Models;

namespace RolePlayGameClient.Services.ServerTeam
{
    public class LocationTeleporterService
    {
        private readonly HttpClient _http = new HttpClient();
        private bool _inProgress;

        public LocationTeleporterService() => _http.BaseAddress = ServerCfg.ClientSiteRunsLocal
                ? new Uri(ServerCfg.ApiLocal) : new Uri(ServerCfg.ApiRemote);

        public SortedDictionary<string, LocationIplModel> LocationList => LocationCfg.LocationList;

        public SortedDictionary<string, List<SortedDictionary<string, InteriorPropModel>>> InteriorPropList =>
            LocationCfg.InteriorPropList;

        public void CheckboxClicked(string checkBox, bool clicked)
        {
        }

        public void TeleportToLocation(string social, string location)
        {
            if (_inProgress) return;

            _inProgress = true;

            _http.DefaultRequestHeaders.Clear();
            _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
            _http.DefaultRequestHeaders.Add("social", social);
            _http.DefaultRequestHeaders.Add("location", location);

            var result = _http.GetAsync("Api/LocationTeleporter/TeleportToLocation").Result;

            _inProgress = false;
        }

        public void CloseTeleporter(string social)
        {
            if (_inProgress) return;

            _inProgress = true;

            _http.DefaultRequestHeaders.Clear();
            _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
            _http.DefaultRequestHeaders.Add("social", social);

            var result = _http.GetAsync("Api/Window/DestroyWindow").Result;

            _inProgress = false;
        }
    }
}