﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using RolePlayGameClientData.Models;
using RolePlayGameData.Configs;

namespace RolePlayGameClient.Services.ServerTeam
{
    public class ClothingSpawnerService
    {
        private readonly HttpClient _http = new HttpClient();
        private bool _inProgress;

        public Action OnChange;

        public ClothingSpawnerService() => _http.BaseAddress = ServerCfg.ClientSiteRunsLocal
                ? new Uri(ServerCfg.ApiLocal) : new Uri(ServerCfg.ApiRemote);

        public SortedDictionary<string, int> Slots { get; private set; }
            = new SortedDictionary<string, int>();

        public List<ClothingValues> Values { get; } = new List<ClothingValues>();

        public int RotValue { get; private set; }

        public void OnInit()
        {
            Slots = new SortedDictionary<string, int>
            {
                {"Masks", 1}, {"HairStyles", 2}, {"Torsos", 3}, {"Legs", 4},
                {"BagsAndPara", 5}, {"Shoes", 6}, {"Accessories", 7}, {"Undershirts", 8},
                {"BodyArmors", 9}, {"Decals", 10}, {"Tops", 11}
            };

            foreach (var item in Slots)
                Values.Add(new ClothingValues
                {
                    Slot = item.Value,
                    Drawable = 0,
                    Texture = 0
                });
        }

        public void ResetSlot(string social, int slot)
        {
            var entry = Values.First(x => x.Slot == slot);

            entry.Drawable = 0;
            entry.Texture = 0;

            StateChanged();

            SendChangeClothing(social, slot);
        }

        public void ChangeGender(string social, string gender)
        {
            _inProgress = true;

            _http.DefaultRequestHeaders.Clear();
            _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
            _http.DefaultRequestHeaders.Add("social", social);
            _http.DefaultRequestHeaders.Add("gender", gender);

            var result = _http.GetAsync("Api/ClothingSpawner/ChangeGender").Result;

            foreach (var item in Values)
            {
                item.Drawable = 0;
                item.Texture = 0;
            }

            StateChanged();

            _inProgress = false;
        }

        public void RotateCharacter(string social, bool direction)
        {
            if (_inProgress) return;

            if (direction)
            {
                if (RotValue + 20 <= 360) RotValue += 20;
                else RotValue = 360;
            }
            else
            {
                if (RotValue - 20 >= 0) RotValue -= 20;
                else RotValue = 0;
            }

            StateChanged();

            SendRotateCharacter(social);
        }

        public void ChangeDrawable(string social, int slot, bool dAction)
        {
            if (_inProgress) return;

            if (dAction) Values.First(x => x.Slot == slot).Drawable += 1;
            else Values.First(x => x.Slot == slot).Drawable -= 1;

            StateChanged();

            SendChangeClothing(social, slot);
        }

        public void ChangeTexture(string social, int slot, bool tAction)
        {
            if (_inProgress) return;

            if (tAction) Values.First(x => x.Slot == slot).Texture += 1;
            else Values.First(x => x.Slot == slot).Texture -= 1;

            StateChanged();

            SendChangeClothing(social, slot);
        }

        private void SendChangeClothing(string social, int slot)
        {
            _inProgress = true;

            _http.DefaultRequestHeaders.Clear();
            _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
            _http.DefaultRequestHeaders.Add("social", social);
            _http.DefaultRequestHeaders.Add("slot", slot.ToString());
            _http.DefaultRequestHeaders.Add("drawable", Values.First(x => x.Slot == slot).Drawable.ToString());
            _http.DefaultRequestHeaders.Add("texture", Values.First(x => x.Slot == slot).Texture.ToString());

            var result = _http.GetAsync("Api/ClothingSpawner/ChangeClothes").Result;

            _inProgress = false;
        }

        private void SendRotateCharacter(string social)
        {
            _inProgress = true;

            _http.DefaultRequestHeaders.Clear();
            _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
            _http.DefaultRequestHeaders.Add("social", social);
            _http.DefaultRequestHeaders.Add("direction", RotValue.ToString());

            var result = _http.GetAsync("Api/ClothingSpawner/RotateCharacter").Result;

            _inProgress = false;
        }

        public void CloseClothingSpawner(string social)
        {
            _inProgress = true;

            _http.DefaultRequestHeaders.Clear();
            _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
            _http.DefaultRequestHeaders.Add("social", social);

            var result = _http.GetAsync("Api/Window/DestroyWindow").Result;

            _inProgress = false;
        }

        private void StateChanged() => OnChange?.Invoke();
    }
}