﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using RolePlayGameData.Configs;
using RolePlayGameData.Enums;
using RolePlayGameData.Models;

namespace RolePlayGameClient.Services.ServerTeam
{
    public class MapMarkEditorService
    {
        private readonly HttpClient _http = new HttpClient();
        private bool _inProgress;

        public Action OnChange;

        public MapMarkEditorService() => _http.BaseAddress = ServerCfg.ClientSiteRunsLocal
                ? new Uri(ServerCfg.ApiLocal) : new Uri(ServerCfg.ApiRemote);

        public List<MarkSettingsModel> Civilian { get; private set; } = new List<MarkSettingsModel>();
        public List<MarkSettingsModel> Farming { get; private set; } = new List<MarkSettingsModel>();
        public List<MarkSettingsModel> Faction { get; private set; } = new List<MarkSettingsModel>();
        public List<MarkSettingsModel> Processors { get; private set; } = new List<MarkSettingsModel>();
        public string MarkToDelete { get; set; } = string.Empty;
        public string DumpsterToDelete { get; set; } = string.Empty;

        public void OnInit()
        {
            _inProgress = true;

            Civilian = MarkCfg.LocationMarkSettings.Where(x => x.MarkType.ToString()
                .Contains("Civ")).OrderBy(x => x.MarkText).ToList();

            Farming = MarkCfg.LocationMarkSettings
                .Where(x => x.MarkType.ToString().Contains("Mine") ||
                            x.MarkType.ToString().Contains("Farmspot"))
                .OrderBy(x => x.MarkText).ToList();

            Faction = MarkCfg.LocationMarkSettings
                .Where(x => x.MarkFaction == FactionEnums.Dcr ||
                            x.MarkFaction == FactionEnums.Doj || x.MarkFaction == FactionEnums.Fib ||
                            x.MarkFaction == FactionEnums.Lsc || x.MarkFaction == FactionEnums.Lspd ||
                            x.MarkFaction == FactionEnums.Lsrs || x.MarkFaction == FactionEnums.Lssd)
                .OrderBy(x => x.MarkText).ToList();

            Processors = MarkCfg.LocationMarkSettings
                .Where(x => x.MarkType.ToString().Contains("Process_") ||
                            x.MarkType.ToString().Contains("Trader_"))
                .OrderBy(x => x.MarkText).ToList();

            _inProgress = false;
        }

        public void CreateMark(string social, FactionEnums faction, MarkTypeEnums markType)
        {
            if (_inProgress) return;

            _inProgress = true;

            _http.DefaultRequestHeaders.Clear();
            _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
            _http.DefaultRequestHeaders.Add("social", social);
            _http.DefaultRequestHeaders.Add("faction", ((uint)faction).ToString());
            _http.DefaultRequestHeaders.Add("markType", ((uint)markType).ToString());

            var result = _http.GetAsync("Api/MapMarkEditor/CreateMark").Result;

            _inProgress = false;
        }

        public void DeleteMark(string social)
        {
            if (_inProgress) return;
            if (MarkToDelete == string.Empty) return;

            _inProgress = true;

            _http.DefaultRequestHeaders.Clear();
            _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
            _http.DefaultRequestHeaders.Add("social", social);
            _http.DefaultRequestHeaders.Add("id", MarkToDelete);

            var result = _http.GetAsync("Api/MapMarkEditor/DeleteMark").Result;

            _inProgress = false;
        }

        public void DeleteDumpster(string social)
        {
            if (_inProgress) return;
            if (DumpsterToDelete == string.Empty) return;

            _inProgress = true;

            _http.DefaultRequestHeaders.Clear();
            _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
            _http.DefaultRequestHeaders.Add("social", social);
            _http.DefaultRequestHeaders.Add("id", DumpsterToDelete);

            var result = _http.GetAsync("Api/MapMarkEditor/DeleteDumpster").Result;

            _inProgress = false;
        }

        public void ReloadMarks(string social)
        {
            if (_inProgress) return;

            _inProgress = true;

            _http.DefaultRequestHeaders.Clear();
            _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
            _http.DefaultRequestHeaders.Add("social", social);

            var result = _http.GetAsync("Api/MapMarkEditor/ReloadMarks").Result;

            _inProgress = false;
        }

        public void ReloadDumpsters(string social)
        {
            if (_inProgress) return;

            _inProgress = true;

            _http.DefaultRequestHeaders.Clear();
            _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
            _http.DefaultRequestHeaders.Add("social", social);

            var result = _http.GetAsync("Api/MapMarkEditor/ReloadDumpsters").Result;

            _inProgress = false;
        }

        public void SpawnMappersCar(string social)
        {
            if (_inProgress) return;

            _inProgress = true;

            _http.DefaultRequestHeaders.Clear();
            _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
            _http.DefaultRequestHeaders.Add("social", social);

            var result = _http.GetAsync("Api/MapMarkEditor/SpawnMapperCar").Result;

            _inProgress = false;
        }

        public void RepairCar(string social)
        {
            if (_inProgress) return;

            _inProgress = true;

            _http.DefaultRequestHeaders.Clear();
            _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
            _http.DefaultRequestHeaders.Add("social", social);

            var result = _http.GetAsync("Api/MapMarkEditor/RepairCar").Result;

            _inProgress = false;
        }

        public void ResetBiology(string social)
        {
            if (_inProgress) return;

            _inProgress = true;

            _http.DefaultRequestHeaders.Clear();
            _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
            _http.DefaultRequestHeaders.Add("social", social);

            var result = _http.GetAsync("Api/MapMarkEditor/ResetBiology").Result;

            _inProgress = false;
        }

        public void CloseMapMarkEditor(string social)
        {
            if (_inProgress) return;

            _inProgress = true;

            _http.DefaultRequestHeaders.Clear();
            _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
            _http.DefaultRequestHeaders.Add("social", social);

            var result = _http.GetAsync("Api/Window/DestroyWindow").Result;

            _inProgress = false;
        }

        private void StateChanged() => OnChange?.Invoke();
    }
}