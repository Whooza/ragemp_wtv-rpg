﻿using System;

namespace RolePlayGameClient.Services.Shop
{
    public class TattooShopService
    {
        public Action OnChange;

        private void StateChanged()
        {
            OnChange?.Invoke();
        }
    }
}