﻿using System;

namespace RolePlayGameClient.Services.Shop
{
    public class HatShopService
    {
        public Action OnChange;

        private void StateChanged()
        {
            OnChange?.Invoke();
        }
    }
}