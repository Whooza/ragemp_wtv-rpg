﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using RolePlayGameClientData.Models;
using RolePlayGameData.Configs;
using RolePlayGameData.Enums;
using RolePlayGameData.Models;

namespace RolePlayGameClient.Services.Shop
{
    public class ItemShopService
    {
        public Action OnChange;
        public List<CartItemModel> Cart { get; private set; } = new List<CartItemModel>();
        public List<ItemModel> ShopItems { get; private set; } = new List<ItemModel>();
        public MarkSettingsModel MarkSetting { get; private set; } = new MarkSettingsModel();
        public float Price { get; private set; }

        private readonly HttpClient _http = new HttpClient();
        private bool _inProgress;

        public ItemShopService() => _http.BaseAddress = ServerCfg.ClientSiteRunsLocal
            ? new Uri(ServerCfg.ApiLocal) : new Uri(ServerCfg.ApiRemote);

        public void OnInit(string markName)
        {
            MarkSetting = MarkCfg.LocationMarkSettings.Single(x => (uint)x.MarkType == Convert.ToUInt32(markName));

            ShopItems = ItemCfg.ItemConfig
                .Where(x => x.ItemType == ItemTypeEnums.SoftDrink || x.ItemType == ItemTypeEnums.Food)
                .OrderBy(x => x.DisplayedName)
                .ToList();
        }

        public void AddToCart(uint itemName)
        {
            var entry = Cart.FirstOrDefault(x => x.ItemName == itemName);

            var cfg = ItemCfg.ItemConfig.First(x => (uint)x.ItemName == itemName);

            if (entry == default)
                Cart.Add(new CartItemModel
                {
                    ItemName = itemName,
                    DisplayedName = cfg.DisplayedName,
                    Amount = 1,
                    Price = cfg.DefaultPrice
                });

            else entry.Amount += 1;

            var price = 0f;

            Cart = Cart.OrderBy(x => x.DisplayedName).ToList();

            foreach (var cartItem in Cart)
            {
                if (cartItem.Price == 0f) cartItem.Price = 0.1f;

                price += cartItem.Price * cartItem.Amount;
            }

            Price = price;

            StateChanged();
        }

        public void RemoveFromCart(uint itemName)
        {
            var entry = Cart.FirstOrDefault(x => x.ItemName == itemName);

            if (entry == default) return;

            if (entry.Amount - 1 > 0) entry.Amount -= 1;
            else Cart.Remove(entry);

            var price = 0f;
            Cart = Cart.OrderBy(x => x.DisplayedName).ToList();

            foreach (var cartItem in Cart)
            {
                if (cartItem.Price == 0f) cartItem.Price = 0.1f;
                price += cartItem.Price * cartItem.Amount;
            }

            Price = price;

            StateChanged();
        }

        public void ClearCart(string social)
        {
            Cart = new List<CartItemModel>();
            Price = 0f;

            StateChanged();
        }

        public void BuyCart(string social)
        {
        }

        public void CloseItemShop(string social)
        {
            _inProgress = true;

            _http.DefaultRequestHeaders.Clear();
            _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
            _http.DefaultRequestHeaders.Add("social", social);
            _http.GetAsync("Api/Window/DestroyWindow");

            _inProgress = false;
        }

        private void StateChanged()
        {
            OnChange?.Invoke();
        }
    }
}