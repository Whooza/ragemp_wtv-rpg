﻿using RolePlayGameClientData.Enums;
using RolePlayGameData.Configs;
using System;
using System.Net.Http;

namespace RolePlayGameClient.Services.Shop
{
    public class HairCutShopService
    {
        public HairCutViewEnums View { get; private set; } = HairCutViewEnums.Startscreen;

        public Action OnChange;
        private readonly HttpClient _http = new HttpClient();
        private bool _inProgress;

        public HairCutShopService() => _http.BaseAddress = ServerCfg.ClientSiteRunsLocal
                ? new Uri(ServerCfg.ApiLocal) : new Uri(ServerCfg.ApiRemote);

        #region Buttons

        public void SwitchView(HairCutViewEnums viewEnum)
        {
            if (_inProgress) return;

            _inProgress = true;

            View = viewEnum;

            StateChanged();

            _inProgress = false;
        }

        public void CloseHairCut(string social)
        {
            _inProgress = true;

            _http.DefaultRequestHeaders.Clear();
            _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
            _http.DefaultRequestHeaders.Add("social", social);

            var result = _http.GetAsync("Api/Window/DestroyWindow").Result;

            _inProgress = false;
        }

        #endregion

        private void StateChanged() => OnChange?.Invoke();
    }
}