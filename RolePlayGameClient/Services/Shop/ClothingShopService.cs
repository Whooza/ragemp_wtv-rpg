﻿using System;
using System.Collections.Generic;

namespace RolePlayGameClient.Services.Shop
{
    public class ClothingShopService
    {
        public List<string> Cart { get; } = new List<string>();

        public Action OnChange;

        

        private void StateChanged() => OnChange?.Invoke();
    }
}