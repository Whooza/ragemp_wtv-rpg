﻿using RolePlayGameClientData.Enums;
using RolePlayGameClientData.Models;
using RolePlayGameData.Configs;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace RolePlayGameClient.Services.Money
{
    public class AtmService
    {
        public AtmViewEnums AtmView { get; private set; } = AtmViewEnums.StartScreenView;
        public AtmOutputModel AtmOutput { get; private set; } = new AtmOutputModel();

        public string Version => AtmCfg.Version;
        public string SystemInfo => AtmCfg.SystemInfo;

        public event Action OnChange;
        public event Action CloseWindow;


        private int _bank;
        private string _bankName;

        private readonly HttpClient _http = new HttpClient();
        private float _moneyInput;
        private string _pinInput;

        public AtmService() => _http.BaseAddress = ServerCfg.ClientSiteRunsLocal
                ? new Uri(ServerCfg.ApiLocal) : new Uri(ServerCfg.ApiRemote);

        public async void ShowHomeScreen()
        {
            ShowInProgress();

            await Task.Delay(2000);

            ResetProperties();

            AtmView = AtmViewEnums.StartScreenView;
            AtmOutput = new AtmOutputModel();

            StateChanged();
        }

        public void SendNumber(int number)
        {
            if (_pinInput.Length < 4) _pinInput += number.ToString();

            if (AtmView == AtmViewEnums.PinCodeInputView) AtmOutput = new AtmOutputModel().ShowPinCode(_bankName, _pinInput);
            //if (AtmView.MoneyInputView) AtmOutput = new AtmOutputModel().ShowBankBalance(_bankName, _moneyInput);

            StateChanged();
        }

        public async Task SelectBank(string bankName, int bank)
        {
            if (AtmView != AtmViewEnums.StartScreenView) return;

            _bankName = bankName;
            _bank = bank;

            ShowInProgress();

            await Task.Delay(2000);

            AtmView = AtmViewEnums.PinCodeInputView;
            AtmOutput = new AtmOutputModel().ShowPinCode(_bankName, _pinInput);
            StateChanged();
        }

        public async Task SwitchAction(int action, string bankBalance = "0.00")
        {
            ShowInProgress();

            await Task.Delay(2000);

            switch (action)
            {
                case 0:
                    break;
                case 1:
                    break;
                case 2:
                    AtmView = AtmViewEnums.BankBalanceView;
                    AtmOutput = new AtmOutputModel().ShowBankBalance(_bankName, bankBalance);
                    break;
                default:
                    ResetProperties();
                    AtmView = AtmViewEnums.StartScreenView;
                    AtmOutput = new AtmOutputModel();
                    StateChanged();
                    return;
            }

            StateChanged();
        }

        public async void CloseAtm()
        {
            ShowInProgress();

            await Task.Delay(2000);

            AtmView = AtmViewEnums.ByeScreenView;
            AtmOutput = new AtmOutputModel().ShowByeScreen();
            StateChanged();

            await Task.Delay(2000);

            CancelAtm();
        }

        private void ShowInProgress()
        {
            AtmView = AtmViewEnums.InProgressView;
            AtmOutput = new AtmOutputModel().ShowInProgress();
            StateChanged();
        }

        public void CloseAtm(string social)
        {
            _http.DefaultRequestHeaders.Clear();
            _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
            _http.DefaultRequestHeaders.Add("social", social);

            var result = _http.GetAsync("Api/Window/DestroyWindow").Result;
        }

        private void ResetProperties()
        {
            _pinInput = "";
            _bankName = "";
            _moneyInput = 0f;
            _bank = -1;
        }

        private void StateChanged() => OnChange?.Invoke();

        public void CancelAtm() => CloseWindow?.Invoke();
    }
}