﻿using System;

namespace RolePlayGameClient.Services.Money
{
    public class BankService
    {
        public Action OnChange;

        private void StateChanged()
        {
            OnChange?.Invoke();
        }
    }
}