﻿using System;

namespace RolePlayGameClient.Services.Money
{
    public class CentralBankService
    {
        public Action OnChange;

        private void StateChanged()
        {
            OnChange?.Invoke();
        }
    }
}