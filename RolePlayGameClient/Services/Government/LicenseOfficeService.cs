﻿using System;

namespace RolePlayGameClient.Services.Government
{
    public class LicenseOfficeService
    {
        public Action OnChange;

        private void StateChanged() => OnChange?.Invoke();
    }
}