﻿using System;
using System.Net.Http;
using RolePlayGameData.Configs;

namespace RolePlayGameClient.Services.Government
{
    public class CityHallService
    {
        private readonly HttpClient _http = new HttpClient();
        private bool _inProgress;

        public Action OnChange;

        public CityHallService() => _http.BaseAddress = ServerCfg.ClientSiteRunsLocal
                ? new Uri(ServerCfg.ApiLocal) : new Uri(ServerCfg.ApiRemote);

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Nationality { get; set; }

        public void GetIdentityCard(string social)
        {
            if (_inProgress) return;

            _inProgress = true;

            _http.DefaultRequestHeaders.Clear();
            _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
            _http.DefaultRequestHeaders.Add("social", social);
            _http.DefaultRequestHeaders.Add("firstName", FirstName);
            _http.DefaultRequestHeaders.Add("lastName", LastName);
            _http.DefaultRequestHeaders.Add("nationality", Nationality);
            var result = _http.GetAsync("Api/CityHall/GetIdentityCard").Result;

            _inProgress = false;
        }

        public void CloseCityHall(string social)
        {
            if (_inProgress) return;

            _inProgress = true;

            _http.DefaultRequestHeaders.Clear();
            _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
            _http.DefaultRequestHeaders.Add("social", social);
            var result = _http.GetAsync("Api/Window/DestroyWindow").Result;

            _inProgress = false;
        }

        private void StateChanged() => OnChange?.Invoke();
    }
}