﻿using RolePlayGameData.Configs;
using System;
using System.Net.Http;

namespace RolePlayGameClient.Services.Government
{
    public class JobCenterService
    {
        private readonly HttpClient _http = new HttpClient();

        public Action OnChange;

        public JobCenterService() => _http.BaseAddress = ServerCfg.ClientSiteRunsLocal
                ? new Uri(ServerCfg.ApiLocal) : new Uri(ServerCfg.ApiRemote);

        public uint BankName { get; private set; }
        public string ErrorMsg { get; } = string.Empty;

        public void SetPaymentBank(uint bankName)
        {
            switch (bankName)
            {
                case 0:
                    BankName = 0;
                    break;
                case 1:
                    BankName = 1;
                    break;
                case 2:
                    BankName = 2;
                    break;
            }
        }

        public void ActivateSocialPayment(string social)
        {
        }

        public void DeactivateSocialPayment(string social)
        {
        }

        public void CloseJobCenter(string social)
        {
            _http.DefaultRequestHeaders.Clear();
            _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
            _http.DefaultRequestHeaders.Add("social", social);
            _http.GetAsync("api/global/DestroyWindow");            
        }

        private void StateChanged() => OnChange?.Invoke();
    }
}