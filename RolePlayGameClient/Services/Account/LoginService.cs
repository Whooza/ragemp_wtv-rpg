﻿using System;
using System.Net.Http;
using Newtonsoft.Json;
using RolePlayGameData.Configs;
using RolePlayGameData.Models;

namespace RolePlayGameClient.Services.Account
{
    public class LoginService
    {
        private readonly HttpClient _http = new HttpClient();

        public LoginService() => _http.BaseAddress = ServerCfg.ClientSiteRunsLocal
                ? new Uri(ServerCfg.ApiLocal) : new Uri(ServerCfg.ApiRemote);

        public string UserPass { get; set; }
        public UiInfoModel Info { get; private set; }
        public bool ShowPatchNotes { get; private set; }
        public string LoginErrorMsg { get; private set; }

        public event Action OnChange;
        private bool _inprogress;

        public void LoadInfos(string social)
        {
            _inprogress = true;

            try
            {
                _http.DefaultRequestHeaders.Clear();
                _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
                _http.DefaultRequestHeaders.Add("social", social);

                Info = JsonConvert.DeserializeObject<UiInfoModel>(_http.GetStringAsync("Api/Information/GetCurrentInfo").Result);
            }
            catch
            {
                Info = new UiInfoModel
                {
                    Body = "- Infos konnten nicht geladen werden -",
                    CreationDate = DateTime.Now,
                    Head = "Aktuelle Infos"
                };
            }

            StateChanged();

            _inprogress = false;
        }

        public void LoginPlayer(string social)
        {
            if (_inprogress) return;

            _inprogress = true;

            try
            {
                _http.DefaultRequestHeaders.Clear();
                _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
                _http.DefaultRequestHeaders.Add("social", social);
                _http.DefaultRequestHeaders.Add("pass", UserPass);

                LoginErrorMsg = _http.GetAsync("Api/Login/LoginPlayer").Result.ReasonPhrase;
            }
            catch
            {
                LoginErrorMsg = "Das LoginSystem is nicht erreichbar!";
            }

            StateChanged();

            _inprogress = false;
        }

        public void SwitchShowPatchNotes()
        {
            if (_inprogress) return;

            _inprogress = true;

            ShowPatchNotes = !ShowPatchNotes;

            StateChanged();

            _inprogress = false;
        }

        private void StateChanged() => OnChange?.Invoke();
    }
}