﻿using System;
using System.Net.Http;
using RolePlayGameData.Configs;

namespace RolePlayGameClient.Services.Account
{
    public class RegisterService
    {
        private readonly HttpClient _http = new HttpClient();
        private bool _inProgress;

        public Action OnChange;

        public RegisterService() => _http.BaseAddress = ServerCfg.ClientSiteRunsLocal
                ? new Uri(ServerCfg.ApiLocal) : new Uri(ServerCfg.ApiRemote);

        public string PassOne { get; set; } = string.Empty;
        public string PassTwo { get; set; } = string.Empty;
        public string PassOneError { get; private set; } = string.Empty;
        public string PassTwoError { get; private set; } = string.Empty;
        public string RegisterMsg { get; private set; } = string.Empty;

        public void RegisterPlayer(string social)
        {
            if (_inProgress) return;

            if (PassOne == string.Empty)
            {
                PassOneError = "Passwort vergessen";
                StateChanged();
                return;
            }

            if (PassTwo == string.Empty)
            {
                PassTwoError = "Passwort vergessen";
                StateChanged();
                return;
            }

            if (PassOne != PassTwo)
            {
                PassTwoError = "Passwörter stimmen nicht überein";
                StateChanged();
                return;
            }

            _inProgress = true;

            try
            {
                _http.DefaultRequestHeaders.Clear();
                _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
                _http.DefaultRequestHeaders.Add("social", social);
                _http.DefaultRequestHeaders.Add("mail", "bla@bla.de");
                _http.DefaultRequestHeaders.Add("pass", PassOne);

                RegisterMsg = _http.GetStringAsync("Api/Register/RegisterPlayer").Result;
            }
            catch
            {
                RegisterMsg = "Interner Fehler";
            }

            StateChanged();

            _inProgress = false;
        }

        public void ResetErrors()
        {
            if (_inProgress) return;

            _inProgress = true;

            PassOneError = string.Empty;
            PassTwoError = string.Empty;
            RegisterMsg = string.Empty;

            StateChanged();

            _inProgress = false;
        }

        private void StateChanged() => OnChange?.Invoke();
    }
}