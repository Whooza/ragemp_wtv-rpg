﻿using Newtonsoft.Json;
using RolePlayGameData.Configs;
using RolePlayGameData.Models;
using System;
using System.Net.Http;

namespace RolePlayGameClient.Services.Account
{
    public class BannedService
    {
        private readonly HttpClient _http = new HttpClient();

        public Action OnChange;

        public BannedService() => _http.BaseAddress = ServerCfg.ClientSiteRunsLocal
                ? new Uri(ServerCfg.ApiLocal) : new Uri(ServerCfg.ApiRemote);

        public BanInfoModel BanInfo { get; private set; }

        public void GetBanInfo(string social)
        {
            try
            {
                _http.DefaultRequestHeaders.Clear();
                _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
                _http.DefaultRequestHeaders.Add("social", social);

                BanInfo = JsonConvert.DeserializeObject<BanInfoModel>(_http.GetStringAsync("Api/Banned/GetBanInfo").Result);
            }
            catch
            {
                BanInfo = new BanInfoModel { BanReason = "na", BanTime = "na" };
            }

            StateChanged();
        }

        private void StateChanged() => OnChange?.Invoke();
    }
}