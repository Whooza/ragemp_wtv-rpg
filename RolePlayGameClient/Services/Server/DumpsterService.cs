﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using Newtonsoft.Json;
using RolePlayGameData.Configs;
using RolePlayGameData.Database.MapMarkModels;
using RolePlayGameData.Database.RolePlayModels;
using RolePlayGameData.Models;

namespace RolePlayGameClient.Services.Server
{
    public class DumpsterService
    {
        public Dictionary<PlayerInvItemModel, ItemModel> PlayerInvDic { get; private set; }
            = new Dictionary<PlayerInvItemModel, ItemModel>();

        public Dictionary<DumpsterInvItemModel, ItemModel> DumpsterInvDic { get; private set; }
            = new Dictionary<DumpsterInvItemModel, ItemModel>();

        public float PWeight { get; private set; }
        public float DWeight { get; private set; }
        public int MoveValue { get; set; } = 1;

        private readonly HttpClient _http = new HttpClient();
        private bool _inProgress;
        private string _social;

        public Action OnChange;

        public DumpsterService() => _http.BaseAddress = ServerCfg.ClientSiteRunsLocal
            ? new Uri(ServerCfg.ApiLocal) : new Uri(ServerCfg.ApiRemote);

        public void Init(string social)
        {
            _inProgress = true;

            _social = social;

            _http.DefaultRequestHeaders.Clear();
            _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
            _http.DefaultRequestHeaders.Add("social", social);

            PlayerInvDic = GetPlayerInventory("Api/Dumpster/GetInventory");
            DumpsterInvDic = GetDumpsterInventory("Api/Dumpster/GetDumpster");

            StateChanged();

            _inProgress = false;
        }

        #region GetInventory

        private Dictionary<PlayerInvItemModel, ItemModel> GetPlayerInventory(string query)
        {
            var playerInv = JsonConvert.DeserializeObject<List<PlayerInvItemModel>>(_http.GetStringAsync(query).Result);
            var pInv = new Dictionary<PlayerInvItemModel, ItemModel>();

            PWeight = 0f;

            foreach (var pItem in playerInv)
            {
                var setting = ItemCfg.ItemConfig.First(x => (uint)x.ItemName == pItem.Item);
                PWeight += setting.Weight * pItem.Amount;
                pInv.Add(pItem, setting);
            }

            return pInv;
        }

        private Dictionary<DumpsterInvItemModel, ItemModel> GetDumpsterInventory(string query)
        {
            var dumpster = JsonConvert.DeserializeObject<DumpsterModel>(_http.GetStringAsync(query).Result);
            var dumpsterInv = JsonConvert.DeserializeObject<List<DumpsterInvItemModel>>(dumpster.ItemList);
            var dInv = new Dictionary<DumpsterInvItemModel, ItemModel>();

            DWeight = 0f;

            if (!dumpsterInv.Any()) return new Dictionary<DumpsterInvItemModel, ItemModel>();

            foreach (var dItem in dumpsterInv)
            {
                var setting = ItemCfg.ItemConfig.First(x => (uint)x.ItemName == dItem.Item);
                DWeight += setting.Weight * dItem.Amount;
                dInv.Add(dItem, setting);
            }

            return dInv;
        }

        #endregion

        #region Buttons

        public void ToDumpster(uint itemName)
        {
            if (_inProgress) return;

            _inProgress = true;

            _http.DefaultRequestHeaders.Clear();
            _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
            _http.DefaultRequestHeaders.Add("social", _social);
            _http.DefaultRequestHeaders.Add("itemName", itemName.ToString());

            var weight = ItemCfg.ItemConfig.First(x => (uint)x.ItemName == itemName).Weight;

            if (DWeight + weight * MoveValue > 250f)
            {
                _inProgress = false;
                return;
            }

            var maxAmount = PlayerInvDic.Keys.First(x => x.Item == itemName).Amount;

            _http.DefaultRequestHeaders.Add("amount", MoveValue.ToString());

            var result = _http.GetStringAsync("Api/Dumpster/ToDumpster").Result;

            PlayerInvDic = GetPlayerInventory("Api/Dumpster/GetInventory");
            DumpsterInvDic = GetDumpsterInventory("Api/Dumpster/GetDumpster");

            StateChanged();

            _inProgress = false;
        }

        public void ToInventory(uint itemName)
        {
            if (_inProgress) return;

            _inProgress = true;

            _http.DefaultRequestHeaders.Clear();
            _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
            _http.DefaultRequestHeaders.Add("social", _social);
            _http.DefaultRequestHeaders.Add("itemName", itemName.ToString());

            var weight = ItemCfg.ItemConfig.First(x => (uint)x.ItemName == itemName).Weight;

            if (PWeight + weight * MoveValue > 100f)
            {
                _inProgress = false;
                return;
            }

            _http.DefaultRequestHeaders.Add("amount", MoveValue.ToString());

            var result = _http.GetStringAsync("Api/Dumpster/ToInvntory").Result;

            PlayerInvDic = GetPlayerInventory("Api/Dumpster/GetInventory");
            DumpsterInvDic = GetDumpsterInventory("Api/Dumpster/GetDumpster");

            StateChanged();

            _inProgress = false;
        }

        public void CloseDumpster()
        {
            if (_inProgress) return;

            _inProgress = true;

            _http.DefaultRequestHeaders.Clear();
            _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
            _http.DefaultRequestHeaders.Add("social", _social);
            _http.GetAsync("Api/Dumpster/CloseDumpster");

            _inProgress = false;
        }

        #endregion

        #region Counter

        public void LowerTradeValue(int value)
        {
            if (MoveValue - value < 1) MoveValue = 1;
            else MoveValue -= value;
        }

        public void RaiseTradeValue(int value) => MoveValue += value;

        #endregion

        private void StateChanged() => OnChange?.Invoke();
    }
}
