﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using Newtonsoft.Json;
using RolePlayGameData.Configs;
using RolePlayGameData.Database.RolePlayModels;
using RolePlayGameData.Models;

namespace RolePlayGameClient.Services.Player
{
    public class InventoryService
    {
        public Dictionary<PlayerInvItemModel, ItemModel> PlayerInvDic { get; private set; }
            = new Dictionary<PlayerInvItemModel, ItemModel>();

        public float Weight { get; private set; }
        public event Action OnChange;

        private readonly HttpClient _http = new HttpClient();
        private bool _inprogress;

        public InventoryService() => _http.BaseAddress = ServerCfg.ClientSiteRunsLocal
                ? new Uri(ServerCfg.ApiLocal) : new Uri(ServerCfg.ApiRemote);

        public void OnInit(string social)
        {
            if (_inprogress) return;

            _inprogress = true;

            _http.DefaultRequestHeaders.Clear();
            _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
            _http.DefaultRequestHeaders.Add("social", social);

            var jsonString = _http.GetStringAsync("api/inventory/GetInventory").Result;
            var playerInv = JsonConvert.DeserializeObject<List<PlayerInvItemModel>>(jsonString);
            var inv = new Dictionary<PlayerInvItemModel, ItemModel>();

            foreach (var item in playerInv)
            {
                var setting = ItemCfg.ItemConfig.First(x => (uint) x.ItemName == item.Item);
                Weight += setting.Weight * item.Amount;
                inv.Add(item, setting);
            }

            PlayerInvDic = inv;

            StateChanged();

            _inprogress = false;
        }

        public void UseItem(string social, uint itemName)
        {
            if (_inprogress) return;

            _inprogress = true;

            _http.DefaultRequestHeaders.Clear();
            _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
            _http.DefaultRequestHeaders.Add("social", social);
            _http.DefaultRequestHeaders.Add("itemName", itemName.ToString());

            var jsonString = _http.GetStringAsync("api/inventory/UseInventoryItem").Result;
            var playerInv = JsonConvert.DeserializeObject<List<PlayerInvItemModel>>(jsonString);
            var inv = new Dictionary<PlayerInvItemModel, ItemModel>();

            Weight = 0f;

            foreach (var item in playerInv)
            {
                var setting = ItemCfg.ItemConfig.First(x => (uint) x.ItemName == item.Item);
                Weight += setting.Weight * item.Amount;
                inv.Add(item, setting);
            }

            PlayerInvDic = inv;

            StateChanged();

            _inprogress = false;
        }

        public void DestroyItem(string social, uint itemName)
        {
            if (_inprogress) return;

            _inprogress = true;

            _http.DefaultRequestHeaders.Clear();
            _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
            _http.DefaultRequestHeaders.Add("social", social);
            _http.DefaultRequestHeaders.Add("itemName", itemName.ToString());

            var jsonString = _http.GetStringAsync("api/inventory/DestroyInventoryItem").Result;
            var playerInv = JsonConvert.DeserializeObject<List<PlayerInvItemModel>>(jsonString);
            var inv = new Dictionary<PlayerInvItemModel, ItemModel>();

            Weight = 0f;

            foreach (var item in playerInv)
            {
                var setting = ItemCfg.ItemConfig.First(x => (uint) x.ItemName == item.Item);
                Weight += setting.Weight * item.Amount;
                inv.Add(item, setting);
            }

            PlayerInvDic = inv;

            StateChanged();

            _inprogress = false;
        }

        public void GiveItem(string social, uint itemName)
        {
            if (_inprogress) return;

            _inprogress = true;

            // TODO 

            _inprogress = false;
        }

        public void CloseInventory(string social)
        {
            if (_inprogress) return;

            _http.DefaultRequestHeaders.Clear();
            _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
            _http.DefaultRequestHeaders.Add("social", social);
            _http.GetAsync("Api/Window/DestroyWindow");
        }

        private void StateChanged()
        {
            OnChange?.Invoke();
        }
    }
}