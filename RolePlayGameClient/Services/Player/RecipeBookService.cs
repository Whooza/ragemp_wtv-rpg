﻿using System;

namespace RolePlayGameClient.Services.Player
{
    public class RecipeBookService
    {
        public Action OnChange;

        private void StateChanged()
        {
            OnChange?.Invoke();
        }
    }
}