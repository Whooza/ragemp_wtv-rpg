﻿using RolePlayGameClientData.Enums;
using RolePlayGameData.Configs;
using System;
using System.Net.Http;

namespace RolePlayGameClient.Services.Player
{
    public class SmartPhoneService
    {
        public SmartPhoneViewEnums View { get; private set; } = SmartPhoneViewEnums.HomeScreenView;

        public Action OnChange;

        private readonly HttpClient _http = new HttpClient();
        private string Social = string.Empty;
        private bool _inprogress;

        public SmartPhoneService() => _http.BaseAddress = ServerCfg.ClientSiteRunsLocal
                ? new Uri(ServerCfg.ApiLocal) : new Uri(ServerCfg.ApiRemote);

        public void Init(string social)
        {

        }

        #region Icons

        public void SwitchView(SmartPhoneViewEnums viewEnum)
        {
            View = viewEnum;

            StateChanged();
        }

        #endregion

        private void StateChanged() => OnChange?.Invoke();
    }
}
