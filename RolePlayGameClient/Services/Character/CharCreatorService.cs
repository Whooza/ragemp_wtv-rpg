﻿using Microsoft.JSInterop;
using Newtonsoft.Json;
using RolePlayGameData.Configs;
using RolePlayGameData.Models;
using System;
using System.Net.Http;

namespace RolePlayGameClient.Services.Character
{
    public class CharCreatorService
    {
        public CharSettingsModel CharSettings { get; private set; } = new CharSettingsModel();
        public int ViewValue { get; private set; }

        public Action OnChange;

        private IJSRuntime _jsRuntime;
        private readonly HttpClient _http = new HttpClient();
        private string _socialClub;
        private bool _inProgress;

        public CharCreatorService() => _http.BaseAddress = ServerCfg.ClientSiteRunsLocal
            ? new Uri(ServerCfg.ApiLocal) : new Uri(ServerCfg.ApiRemote);

        public void Init(string social, IJSRuntime runtime)
        {
            _inProgress = true;

            _socialClub = social;
            _jsRuntime = runtime;

            _inProgress = false;
        }

        #region Buttons

        public void ChangeView(int value)
        {
            if (_inProgress) return;

            _inProgress = true;

            ViewValue = value;
            StateChanged();

            _inProgress = false;
        }

        public async void SendSetting(int setting, bool calc)
        {
            if (_inProgress) return;

            _inProgress = true;

            float AddFloatValue(float value, float max) => value + 0.1f <= max ? float.Parse((value + 0.1f).ToString("N1")) : value;
            float SubtractFloatValue(float value, float min) => value - 0.1f >= min ? float.Parse((value - 0.1f).ToString("N1")) : value;

            int AddIntValue(int value, int max)
            {
                if (value == 255) return 0;
                if (value + 1 <= max) return value + 1;
                return value;
            }

            int SubtractIntValue(int value, int min)
            {
                if (value == 255) return 0;
                if (value - 1 >= min) return value - 1;
                return value;
            }

            switch (setting)
            {
                case 0:
                    CharSettings.FirstHeadShape = calc ? AddIntValue(CharSettings.FirstHeadShape, 45) : SubtractIntValue(CharSettings.FirstHeadShape, 0);
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setHeadBlendData", CharSettings.FirstHeadShape,
                        CharSettings.SecondHeadShape, CharSettings.FirstSkinTone, CharSettings.SecondSkinTone, CharSettings.HeadMix, CharSettings.SkinMix)
                        .ConfigureAwait(true);
                    break;
                case 1:
                    CharSettings.SecondHeadShape = calc ? AddIntValue(CharSettings.SecondHeadShape, 45) : SubtractIntValue(CharSettings.SecondHeadShape, 0);
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setHeadBlendData", CharSettings.FirstHeadShape,
                            CharSettings.SecondHeadShape, CharSettings.FirstSkinTone, CharSettings.SecondSkinTone, CharSettings.HeadMix, CharSettings.SkinMix)
                        .ConfigureAwait(true);
                    break;
                case 2:
                    CharSettings.FirstSkinTone = calc ? AddIntValue(CharSettings.FirstSkinTone, 45) : SubtractIntValue(CharSettings.FirstSkinTone, 0);
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setHeadBlendData", CharSettings.FirstHeadShape,
                            CharSettings.SecondHeadShape, CharSettings.FirstSkinTone, CharSettings.SecondSkinTone, CharSettings.HeadMix, CharSettings.SkinMix)
                        .ConfigureAwait(true);
                    break;
                case 3:
                    CharSettings.SecondSkinTone = calc ? AddIntValue(CharSettings.SecondSkinTone, 45) : SubtractIntValue(CharSettings.SecondSkinTone, 0);
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setHeadBlendData", CharSettings.FirstHeadShape,
                            CharSettings.SecondHeadShape, CharSettings.FirstSkinTone, CharSettings.SecondSkinTone, CharSettings.HeadMix, CharSettings.SkinMix)
                        .ConfigureAwait(true);
                    break;
                case 4:
                    CharSettings.HeadMix = calc ? AddFloatValue(CharSettings.HeadMix, 1f) : SubtractFloatValue(CharSettings.HeadMix, 0f);
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setHeadBlendData", CharSettings.FirstHeadShape,
                            CharSettings.SecondHeadShape, CharSettings.FirstSkinTone, CharSettings.SecondSkinTone, CharSettings.HeadMix, CharSettings.SkinMix)
                        .ConfigureAwait(true);
                    break;
                case 5:
                    CharSettings.SkinMix = calc ? AddFloatValue(CharSettings.SkinMix, 1f) : SubtractFloatValue(CharSettings.SkinMix, 0f);
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setHeadBlendData", CharSettings.FirstHeadShape,
                            CharSettings.SecondHeadShape, CharSettings.FirstSkinTone, CharSettings.SecondSkinTone, CharSettings.HeadMix, CharSettings.SkinMix)
                        .ConfigureAwait(true);
                    break;
                case 6:
                    CharSettings.HairModel = calc ? AddIntValue(CharSettings.HairModel, 73) : SubtractIntValue(CharSettings.HairModel, 0);
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setComponentVariation", 2, CharSettings.HairModel, 0)
                        .ConfigureAwait(true);
                    break;
                case 7:
                    CharSettings.FirstHairColor = calc ? AddIntValue(CharSettings.FirstHairColor, 31) : SubtractIntValue(CharSettings.FirstHairColor, 0);
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setHairColor", CharSettings.FirstHairColor, CharSettings.SecondHairColor)
                        .ConfigureAwait(true);
                    break;
                case 8:
                    CharSettings.SecondHairColor = calc ? AddIntValue(CharSettings.SecondHairColor, 31) : SubtractIntValue(CharSettings.SecondHairColor, 0);
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setHairColor", CharSettings.FirstHairColor, CharSettings.SecondHairColor)
                        .ConfigureAwait(true);
                    break;
                case 9:
                    CharSettings.BeardModel = calc ? AddIntValue(CharSettings.BeardModel, 28) : SubtractIntValue(CharSettings.BeardModel, 0);
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setHeadOverlay", 1, CharSettings.BeardModel)
                        .ConfigureAwait(true);
                    break;
                case 10:
                    CharSettings.BeardColor = calc ? AddIntValue(CharSettings.BeardColor, 28) : SubtractIntValue(CharSettings.BeardColor, 0);
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setHeadOverlayColor", 1, 1, CharSettings.BeardColor)
                        .ConfigureAwait(true);
                    break;
                case 11:
                    CharSettings.ChestModel = calc ? AddIntValue(CharSettings.ChestModel, 16) : SubtractIntValue(CharSettings.ChestModel, 0);
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setHeadOverlay", 10, CharSettings.ChestModel)
                        .ConfigureAwait(true);
                    break;
                case 12:
                    CharSettings.ChestColor = calc ? AddIntValue(CharSettings.ChestColor, 16) : SubtractIntValue(CharSettings.ChestColor, 0);
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setHeadOverlayColor", 10, 1, CharSettings.ChestColor)
                        .ConfigureAwait(true);
                    break;
                case 13:
                    CharSettings.BlemishesModel = calc ? AddIntValue(CharSettings.BlemishesModel, 23) : SubtractIntValue(CharSettings.BlemishesModel, 0);
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setHeadOverlay", 0, CharSettings.BlemishesModel)
                        .ConfigureAwait(true);
                    break;
                case 14:
                    CharSettings.AgingModel = calc ? AddIntValue(CharSettings.AgingModel, 14) : SubtractIntValue(CharSettings.AgingModel, 0);
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setHeadOverlay", 3, CharSettings.AgingModel)
                        .ConfigureAwait(true);
                    break;
                case 15:
                    CharSettings.ComplexionModel = calc ? AddIntValue(CharSettings.ComplexionModel, 11) : SubtractIntValue(CharSettings.ComplexionModel, 0);
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setHeadOverlay", 6, CharSettings.ComplexionModel)
                        .ConfigureAwait(true);
                    break;
                case 16:
                    CharSettings.SunDamageModel = calc ? AddIntValue(CharSettings.SunDamageModel, 10) : SubtractIntValue(CharSettings.SunDamageModel, 0);
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setHeadOverlay", 7, CharSettings.SunDamageModel)
                        .ConfigureAwait(true);
                    break;
                case 17:
                    CharSettings.FrecklesModel = calc ? AddIntValue(CharSettings.FrecklesModel, 17) : SubtractIntValue(CharSettings.FrecklesModel, 0);
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setHeadOverlay", 9, CharSettings.FrecklesModel)
                        .ConfigureAwait(true);
                    break;
                case 18:
                    CharSettings.EyesColor = calc ? AddIntValue(CharSettings.EyesColor, 31) : SubtractIntValue(CharSettings.EyesColor, 0);
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setEyeColor", CharSettings.EyesColor)
                        .ConfigureAwait(true);
                    break;
                case 19:
                    CharSettings.EyebrowsModel = calc ? AddIntValue(CharSettings.EyebrowsModel, 33) : SubtractIntValue(CharSettings.EyebrowsModel, 0);
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setHeadOverlay", 2, CharSettings.EyebrowsModel)
                        .ConfigureAwait(true);
                    break;
                case 20:
                    CharSettings.EyebrowsColor = calc ? AddIntValue(CharSettings.EyebrowsColor, 33) : SubtractIntValue(CharSettings.EyebrowsColor, 0);
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setHeadOverlayColor", 2, 1, CharSettings.EyebrowsColor)
                        .ConfigureAwait(true);
                    break;
                case 21:
                    CharSettings.MakeupModel = calc ? AddIntValue(CharSettings.MakeupModel, 74) : SubtractIntValue(CharSettings.MakeupModel, 0);
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setHeadOverlay", 4, CharSettings.MakeupModel)
                        .ConfigureAwait(true);
                    break;
                case 22:
                    CharSettings.BlushModel = calc ? AddIntValue(CharSettings.BlushModel, 6) : SubtractIntValue(CharSettings.BlushModel, 0);
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setHeadOverlay", 5, CharSettings.BlushModel)
                        .ConfigureAwait(true);
                    break;
                case 23:
                    CharSettings.BlushColor = calc ? AddIntValue(CharSettings.BlushColor, 6) : SubtractIntValue(CharSettings.BlushColor, 0);
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setHeadOverlayColor", 5, 2, CharSettings.BlushColor)
                        .ConfigureAwait(true);
                    break;
                case 24:
                    CharSettings.LipstickModel = calc ? AddIntValue(CharSettings.LipstickModel, 9) : SubtractIntValue(CharSettings.LipstickModel, 0);
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setHeadOverlay", 8, CharSettings.LipstickModel)
                        .ConfigureAwait(true);
                    break;
                case 25:
                    CharSettings.LipstickColor = calc ? AddIntValue(CharSettings.LipstickColor, 9) : SubtractIntValue(CharSettings.LipstickColor, 0);
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setHeadOverlayColor", 8, 2, CharSettings.LipstickColor)
                        .ConfigureAwait(true);
                    break;
                case 26:
                    CharSettings.NoseWidth = calc ? AddFloatValue(CharSettings.NoseWidth, 1f) : SubtractFloatValue(CharSettings.NoseWidth, -1f);
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setFaceFeature", 0, CharSettings.NoseWidth)
                        .ConfigureAwait(true);
                    break;
                case 27:
                    CharSettings.NoseHeight = calc ? AddFloatValue(CharSettings.NoseHeight, 1f) : SubtractFloatValue(CharSettings.NoseHeight, -1f);
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setFaceFeature", 1, CharSettings.NoseHeight)
                        .ConfigureAwait(true);
                    break;
                case 28:
                    CharSettings.NoseLength = calc ? AddFloatValue(CharSettings.NoseLength, 1f) : SubtractFloatValue(CharSettings.NoseLength, -1f);
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setFaceFeature", 2, CharSettings.NoseLength)
                        .ConfigureAwait(true);
                    break;
                case 29:
                    CharSettings.NoseBridge = calc ? AddFloatValue(CharSettings.NoseBridge, 1f) : SubtractFloatValue(CharSettings.NoseBridge, -1f);
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setFaceFeature", 3, CharSettings.NoseBridge)
                        .ConfigureAwait(true);
                    break;
                case 30:
                    CharSettings.NoseTip = calc ? AddFloatValue(CharSettings.NoseTip, 1f) : SubtractFloatValue(CharSettings.NoseTip, -1f);
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setFaceFeature", 4, CharSettings.NoseTip)
                        .ConfigureAwait(true);
                    break;
                case 31:
                    CharSettings.NoseShift = calc ? AddFloatValue(CharSettings.NoseShift, 1f) : SubtractFloatValue(CharSettings.NoseShift, -1f);
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setFaceFeature", 5, CharSettings.NoseShift)
                        .ConfigureAwait(true);
                    break;
                case 32:
                    CharSettings.BrowHeight = calc ? AddFloatValue(CharSettings.BrowHeight, 1f) : SubtractFloatValue(CharSettings.BrowHeight, -1f);
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setFaceFeature", 6, CharSettings.BrowHeight)
                        .ConfigureAwait(true);
                    break;
                case 33:
                    CharSettings.BrowWidth = calc ? AddFloatValue(CharSettings.BrowWidth, 1f) : SubtractFloatValue(CharSettings.BrowWidth, -1f);
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setFaceFeature", 7, CharSettings.BrowWidth)
                        .ConfigureAwait(true);
                    break;
                case 34:
                    CharSettings.CheekboneHeight = calc ? AddFloatValue(CharSettings.CheekboneHeight, 1f) : SubtractFloatValue(CharSettings.CheekboneHeight, -1f);
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setFaceFeature", 8, CharSettings.CheekboneHeight)
                        .ConfigureAwait(true);
                    break;
                case 35:
                    CharSettings.CheekboneWidth = calc ? AddFloatValue(CharSettings.CheekboneWidth, 1f) : SubtractFloatValue(CharSettings.CheekboneWidth, -1f);
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setFaceFeature", 9, CharSettings.CheekboneWidth)
                        .ConfigureAwait(true);
                    break;
                case 36:
                    CharSettings.CheeksWidth = calc ? AddFloatValue(CharSettings.CheeksWidth, 1f) : SubtractFloatValue(CharSettings.CheeksWidth, -1f);
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setFaceFeature", 10, CharSettings.CheeksWidth)
                        .ConfigureAwait(true);
                    break;
                case 37:
                    CharSettings.Eyes = calc ? AddFloatValue(CharSettings.Eyes, 1f) : SubtractFloatValue(CharSettings.Eyes, -1f);
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setFaceFeature", 11, CharSettings.Eyes)
                        .ConfigureAwait(true);
                    break;
                case 38:
                    CharSettings.Lips = calc ? AddFloatValue(CharSettings.Lips, 1f) : SubtractFloatValue(CharSettings.Lips, -1f);
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setFaceFeature", 12, CharSettings.Lips)
                        .ConfigureAwait(true);
                    break;
                case 39:
                    CharSettings.JawWidth = calc ? AddFloatValue(CharSettings.JawWidth, 1f) : SubtractFloatValue(CharSettings.JawWidth, -1f);
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setFaceFeature", 13, CharSettings.JawWidth)
                        .ConfigureAwait(true);
                    break;
                case 40:
                    CharSettings.JawHeight = calc ? AddFloatValue(CharSettings.JawHeight, 1f) : SubtractFloatValue(CharSettings.JawHeight, -1f);
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setFaceFeature", 14, CharSettings.JawHeight)
                        .ConfigureAwait(true);
                    break;
                case 41:
                    CharSettings.ChinLength = calc ? AddFloatValue(CharSettings.ChinLength, 1f) : SubtractFloatValue(CharSettings.ChinLength, -1f);
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setFaceFeature", 15, CharSettings.ChinLength)
                        .ConfigureAwait(true);
                    break;
                case 42:
                    CharSettings.ChinPosition = calc ? AddFloatValue(CharSettings.ChinPosition, 1f) : SubtractFloatValue(CharSettings.ChinPosition, -1f);
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setFaceFeature", 16, CharSettings.ChinPosition)
                        .ConfigureAwait(true);
                    break;
                case 43:
                    CharSettings.ChinWidth = calc ? AddFloatValue(CharSettings.ChinWidth, 1f) : SubtractFloatValue(CharSettings.ChinWidth, -1f);
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setFaceFeature", 17, CharSettings.ChinWidth)
                        .ConfigureAwait(true);
                    break;
                case 44:
                    CharSettings.ChinShape = calc ? AddFloatValue(CharSettings.ChinShape, 1f) : SubtractFloatValue(CharSettings.ChinShape, -1f);
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setFaceFeature", 18, CharSettings.ChinShape)
                        .ConfigureAwait(true);
                    break;
                case 45:
                    CharSettings.NeckWidth = calc ? AddFloatValue(CharSettings.NeckWidth, 1f) : SubtractFloatValue(CharSettings.NeckWidth, -1f);
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setFaceFeature", 19, CharSettings.NeckWidth)
                        .ConfigureAwait(true);
                    break;
            }

            StateChanged();

            _inProgress = false;
        }

        public async void ResetSetting(int setting)
        {
            if (_inProgress) return;

            _inProgress = true;

            switch (setting)
            {
                case 0:
                    CharSettings.FirstHeadShape = 0;
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setHeadBlendData", CharSettings.FirstHeadShape,
                        CharSettings.SecondHeadShape, CharSettings.FirstSkinTone, CharSettings.SecondSkinTone, CharSettings.HeadMix, CharSettings.SkinMix)
                        .ConfigureAwait(true);
                    break;
                case 1:
                    CharSettings.SecondHeadShape = 0;
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setHeadBlendData", CharSettings.FirstHeadShape,
                            CharSettings.SecondHeadShape, CharSettings.FirstSkinTone, CharSettings.SecondSkinTone, CharSettings.HeadMix, CharSettings.SkinMix)
                        .ConfigureAwait(true);
                    break;
                case 2:
                    CharSettings.FirstSkinTone = 0;
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setHeadBlendData", CharSettings.FirstHeadShape,
                            CharSettings.SecondHeadShape, CharSettings.FirstSkinTone, CharSettings.SecondSkinTone, CharSettings.HeadMix, CharSettings.SkinMix)
                        .ConfigureAwait(true);
                    break;
                case 3:
                    CharSettings.SecondSkinTone = 0;
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setHeadBlendData", CharSettings.FirstHeadShape,
                            CharSettings.SecondHeadShape, CharSettings.FirstSkinTone, CharSettings.SecondSkinTone, CharSettings.HeadMix, CharSettings.SkinMix)
                        .ConfigureAwait(true);
                    break;
                case 4:
                    CharSettings.HeadMix = 0.5f;
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setHeadBlendData", CharSettings.FirstHeadShape,
                            CharSettings.SecondHeadShape, CharSettings.FirstSkinTone, CharSettings.SecondSkinTone, CharSettings.HeadMix, CharSettings.SkinMix)
                        .ConfigureAwait(true);
                    break;
                case 5:
                    CharSettings.SkinMix = 0.5f;
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setHeadBlendData", CharSettings.FirstHeadShape,
                            CharSettings.SecondHeadShape, CharSettings.FirstSkinTone, CharSettings.SecondSkinTone, CharSettings.HeadMix, CharSettings.SkinMix)
                        .ConfigureAwait(true);
                    break;
                case 6:
                    CharSettings.HairModel = 0;
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setComponentVariation", CharSettings.HairModel)
                        .ConfigureAwait(true);
                    break;
                case 7:
                    CharSettings.FirstHairColor = 0;
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setHairColor", CharSettings.FirstHairColor, CharSettings.SecondHairColor)
                        .ConfigureAwait(true);
                    break;
                case 8:
                    CharSettings.SecondHairColor = 0;
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setHairColor", CharSettings.FirstHairColor, CharSettings.SecondHairColor)
                        .ConfigureAwait(true);
                    break;
                case 9:
                    CharSettings.BeardModel = 255;
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setHeadOverlay", 1, CharSettings.BeardModel)
                        .ConfigureAwait(true);
                    break;
                case 10:
                    CharSettings.BeardColor = 0;
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setHeadOverlayColor", 1, 1, CharSettings.BeardColor)
                        .ConfigureAwait(true);
                    break;
                case 11:
                    CharSettings.ChestModel = 255;
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setHeadOverlay", 10, CharSettings.ChestModel)
                        .ConfigureAwait(true);
                    break;
                case 12:
                    CharSettings.ChestColor = 0;
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setHeadOverlayColor", 10, 1, CharSettings.ChestColor)
                        .ConfigureAwait(true);
                    break;
                case 13:
                    CharSettings.BlemishesModel = 255;
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setHeadOverlay", 0, CharSettings.BlemishesModel)
                        .ConfigureAwait(true);
                    break;
                case 14:
                    CharSettings.AgingModel = 255;
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setHeadOverlay", 3, CharSettings.AgingModel)
                        .ConfigureAwait(true);
                    break;
                case 15:
                    CharSettings.ComplexionModel = 255;
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setHeadOverlay", 6, CharSettings.ComplexionModel)
                        .ConfigureAwait(true);
                    break;
                case 16:
                    CharSettings.SunDamageModel = 255;
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setHeadOverlay", 7, CharSettings.SunDamageModel)
                        .ConfigureAwait(true);
                    break;
                case 17:
                    CharSettings.FrecklesModel = 255;
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setHeadOverlay", 9, CharSettings.FrecklesModel)
                        .ConfigureAwait(true);
                    break;
                case 18:
                    CharSettings.EyesColor = 0;
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setEyeColor", CharSettings.EyesColor)
                        .ConfigureAwait(true);
                    break;
                case 19:
                    CharSettings.EyebrowsModel = 255;
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setHeadOverlay", 2, CharSettings.EyebrowsModel)
                        .ConfigureAwait(true);
                    break;
                case 20:
                    CharSettings.EyebrowsColor = 0;
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setHeadOverlayColor", 2, 1, CharSettings.EyebrowsColor)
                        .ConfigureAwait(true);
                    break;
                case 21:
                    CharSettings.MakeupModel = 255;
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setHeadOverlay", 4, CharSettings.MakeupModel)
                        .ConfigureAwait(true);
                    break;
                case 22:
                    CharSettings.BlushModel = 255;
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setHeadOverlay", 5, CharSettings.BlushModel)
                        .ConfigureAwait(true);
                    break;
                case 23:
                    CharSettings.BlushColor = 0;
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setHeadOverlayColor", 5, 2, CharSettings.BlushColor)
                        .ConfigureAwait(true);
                    break;
                case 24:
                    CharSettings.LipstickModel = 255;
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setHeadOverlay", 8, CharSettings.LipstickModel)
                        .ConfigureAwait(true);
                    break;
                case 25:
                    CharSettings.LipstickColor = 0;
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setHeadOverlayColor", 8, 2, CharSettings.LipstickColor)
                        .ConfigureAwait(true);
                    break;
                case 26:
                    CharSettings.NoseWidth = 0.0f;
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setFaceFeature", 0, CharSettings.NoseWidth)
                        .ConfigureAwait(true);
                    break;
                case 27:
                    CharSettings.NoseHeight = 0.0f;
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setFaceFeature", 1, CharSettings.NoseHeight)
                        .ConfigureAwait(true);
                    break;
                case 28:
                    CharSettings.NoseLength = 0.0f;
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setFaceFeature", 2, CharSettings.NoseLength)
                        .ConfigureAwait(true);
                    break;
                case 29:
                    CharSettings.NoseBridge = 0.0f;
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setFaceFeature", 3, CharSettings.NoseBridge)
                        .ConfigureAwait(true);
                    break;
                case 30:
                    CharSettings.NoseTip = 0.0f;
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setFaceFeature", 4, CharSettings.NoseTip)
                        .ConfigureAwait(true);
                    break;
                case 31:
                    CharSettings.NoseShift = 0.0f;
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setFaceFeature", 5, CharSettings.NoseShift)
                        .ConfigureAwait(true);
                    break;
                case 32:
                    CharSettings.BrowHeight = 0.0f;
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setFaceFeature", 6, CharSettings.BrowHeight)
                        .ConfigureAwait(true);
                    break;
                case 33:
                    CharSettings.BrowWidth = 0.0f;
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setFaceFeature", 7, CharSettings.BrowWidth)
                        .ConfigureAwait(true);
                    break;
                case 34:
                    CharSettings.CheekboneHeight = 0.0f;
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setFaceFeature", 8, CharSettings.CheekboneHeight)
                        .ConfigureAwait(true);
                    break;
                case 35:
                    CharSettings.CheekboneWidth = 0.0f;
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setFaceFeature", 9, CharSettings.CheekboneWidth)
                        .ConfigureAwait(true);
                    break;
                case 36:
                    CharSettings.CheeksWidth = 0.0f;
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setFaceFeature", 10, CharSettings.CheeksWidth)
                        .ConfigureAwait(true);
                    break;
                case 37:
                    CharSettings.Eyes = 0.0f;
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setFaceFeature", 11, CharSettings.Eyes)
                        .ConfigureAwait(true);
                    break;
                case 38:
                    CharSettings.Lips = 0.0f;
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setFaceFeature", 12, CharSettings.Lips)
                        .ConfigureAwait(true);
                    break;
                case 39:
                    CharSettings.JawWidth = 0.0f;
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setFaceFeature", 13, CharSettings.JawWidth)
                        .ConfigureAwait(true);
                    break;
                case 40:
                    CharSettings.JawHeight = 0.0f;
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setFaceFeature", 14, CharSettings.JawHeight)
                        .ConfigureAwait(true);
                    break;
                case 41:
                    CharSettings.ChinLength = 0.0f;
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setFaceFeature", 15, CharSettings.ChinLength)
                        .ConfigureAwait(true);
                    break;
                case 42:
                    CharSettings.ChinPosition = 0.0f;
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setFaceFeature", 16, CharSettings.ChinPosition)
                        .ConfigureAwait(true);
                    break;
                case 43:
                    CharSettings.ChinWidth = 0.0f;
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setFaceFeature", 17, CharSettings.ChinWidth)
                        .ConfigureAwait(true);
                    break;
                case 44:
                    CharSettings.ChinShape = 0.0f;
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setFaceFeature", 18, CharSettings.ChinShape)
                        .ConfigureAwait(true);
                    break;
                case 45:
                    CharSettings.NeckWidth = 0.0f;
                    await _jsRuntime.InvokeAsync<bool>("charCreator.setFaceFeature", 19, CharSettings.NeckWidth)
                        .ConfigureAwait(true);
                    break;
            }

            StateChanged();

            _inProgress = false;
        }

        public async void Summit()
        {
            if (_inProgress) return;

            _inProgress = true;

            _http.DefaultRequestHeaders.Clear();
            _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
            _http.DefaultRequestHeaders.Add("social", _socialClub);
            _http.DefaultRequestHeaders.Add("settings", JsonConvert.SerializeObject(CharSettings));

            await _jsRuntime.InvokeAsync<bool>("charCreator.setCreatorCamera", false).ConfigureAwait(true);
            await _http.GetStringAsync("Api/CharCreator/SummitCreator").ConfigureAwait(true);

            _inProgress = false;
        }

        #endregion

        private void StateChanged() => OnChange?.Invoke();
    }
}