﻿using System;

namespace RolePlayGameClient.Services.Vehicle
{
    public class VehicleInteractionService
    {
        public Action OnChange;

        private void StateChanged()
        {
            OnChange?.Invoke();
        }
    }
}