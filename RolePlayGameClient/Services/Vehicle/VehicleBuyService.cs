﻿using System;
using System.Linq;
using System.Net.Http;
using Newtonsoft.Json;
using RolePlayGameData.Configs;
using RolePlayGameData.Configs.Vehicle;
using RolePlayGameData.Database.MapMarkModels;
using RolePlayGameData.Models;

namespace RolePlayGameClient.Services.Vehicle
{
    public class VehicleBuyService
    {
        private readonly HttpClient _http = new HttpClient();
        private bool _inProgress;

        public Action OnChange;

        public VehicleBuyService() => _http.BaseAddress = ServerCfg.ClientSiteRunsLocal
                ? new Uri(ServerCfg.ApiLocal) : new Uri(ServerCfg.ApiRemote);

        public ShopSlotPosModel VehicleData { get; private set; } = new ShopSlotPosModel();
        public NewVehicleModel VehicleConfig { get; private set; } = new NewVehicleModel();

        public void OnInit(string social, string markName)
        {
            _http.DefaultRequestHeaders.Clear();
            _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
            _http.DefaultRequestHeaders.Add("social", social);
            _http.DefaultRequestHeaders.Add("markName", markName);

            var dataString = _http.GetStringAsync("Api/VehicleBuy/GetNewVehicleData").Result;

            VehicleData = JsonConvert.DeserializeObject<ShopSlotPosModel>(dataString);

            VehicleConfig = VehicleCfg.AllVehicles.FirstOrDefault(x => (uint) x.VehicleHash == VehicleData.VehicleHash);
        }

        #region Buttons

        public void BuyVehicle(string social, string markName)
        {
            if (_inProgress) return;

            _inProgress = true;

            _http.DefaultRequestHeaders.Clear();
            _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
            _http.DefaultRequestHeaders.Add("social", social);
            _http.DefaultRequestHeaders.Add("markName", markName);

            var result = _http.GetAsync("Api/VehicleBuy/BuyVehicle").Result;

            _inProgress = false;
        }

        public void CloseVehicleBuy(string social)
        {
            if (_inProgress) return;

            _inProgress = true;

            _http.DefaultRequestHeaders.Clear();
            _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
            _http.DefaultRequestHeaders.Add("social", social);

            var result = _http.GetAsync("Api/Window/DestroyWindow").Result;

            _inProgress = false;
        }

        #endregion

        private void StateChanged() => OnChange?.Invoke();
    }
}