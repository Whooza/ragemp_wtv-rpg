﻿using RolePlayGameData.Configs;
using System;
using System.Net.Http;

namespace RolePlayGameClient.Services.Vehicle
{
    public class VehicleGarageService
    {
        public Action OnChange;

        private readonly HttpClient _http = new HttpClient();
        private bool _inProgress;

        public VehicleGarageService() => _http.BaseAddress = ServerCfg.ClientSiteRunsLocal
                ? new Uri(ServerCfg.ApiLocal) : new Uri(ServerCfg.ApiRemote);

        private void StateChanged() => OnChange?.Invoke();
    }
}
