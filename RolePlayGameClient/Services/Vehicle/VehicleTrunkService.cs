﻿using Newtonsoft.Json;
using RolePlayGameData.Configs;
using RolePlayGameData.Configs.Vehicle;
using RolePlayGameData.Database.RolePlayModels;
using RolePlayGameData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;

namespace RolePlayGameClient.Services.Vehicle
{
    public class VehicleTrunkService
    {
        public Dictionary<PlayerInvItemModel, ItemModel> PlayerInvDic { get; private set; }
            = new Dictionary<PlayerInvItemModel, ItemModel>();

        public Dictionary<VehicleInvItemModel, ItemModel> VehicleInvDic { get; private set; }
            = new Dictionary<VehicleInvItemModel, ItemModel>();

        public float PWeight { get; private set; }
        public float VWeight { get; private set; }
        public float VMaxWeight { get; private set; }
        public int MoveValue { get; set; } = 1;

        public Action OnChange;

        private ApiVehicleInfoModel _vehicle;
        private readonly HttpClient _http = new HttpClient();
        private bool _inProgress;

        public VehicleTrunkService() => _http.BaseAddress = ServerCfg.ClientSiteRunsLocal
                ? new Uri(ServerCfg.ApiLocal) : new Uri(ServerCfg.ApiRemote);

        public void OnInit(string social)
        {
            if (_inProgress) return;

            _inProgress = true;

            _http.DefaultRequestHeaders.Clear();
            _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
            _http.DefaultRequestHeaders.Add("social", social);

            _vehicle = JsonConvert.DeserializeObject<ApiVehicleInfoModel>(_http.GetStringAsync("Api/VehicleTrunk/GetVehicleInfoModel").Result);
            PlayerInvDic = GetPlayerInventory("Api/VehicleTrunk/GetInventory");
            VehicleInvDic = GetVehicleInventory("Api/VehicleTrunk/FindNextVehicleInventory");

            VMaxWeight = VehicleCfg.AllVehicles.FirstOrDefault(x => (uint)x.VehicleHash == _vehicle.Model).InventoryCapacity;

            StateChanged();

            _inProgress = false;
        }

        #region GetInventory

        private Dictionary<PlayerInvItemModel, ItemModel> GetPlayerInventory(string query)
        {
            var playerInvString = _http.GetStringAsync(query).Result;
            var playerInv = JsonConvert.DeserializeObject<List<PlayerInvItemModel>>(playerInvString);
            var pInv = new Dictionary<PlayerInvItemModel, ItemModel>();

            PWeight = 0f;

            foreach (var pItem in playerInv)
            {
                var setting = ItemCfg.ItemConfig.First(x => (uint)x.ItemName == pItem.Item);
                PWeight += setting.Weight * pItem.Amount;
                pInv.Add(pItem, setting);
            }

            return pInv;
        }

        private Dictionary<VehicleInvItemModel, ItemModel> GetVehicleInventory(string query)
        {
            var vehicleInvString = _http.GetStringAsync(query).Result;
            var vehicleInv = JsonConvert.DeserializeObject<List<VehicleInvItemModel>>(vehicleInvString);
            var vInv = new Dictionary<VehicleInvItemModel, ItemModel>();

            VWeight = 0f;

            foreach (var vItem in vehicleInv)
            {
                var setting = ItemCfg.ItemConfig.First(x => (uint)x.ItemName == vItem.Item);
                VWeight += setting.Weight * vItem.Amount;
                vInv.Add(vItem, setting);
            }

            return vInv;
        }

        #endregion

        #region Buttons

        public void MoveItem(string social, bool toVehicle, uint itemName)
        {
            if (_inProgress) return;

            _inProgress = true;

            _http.DefaultRequestHeaders.Clear();
            _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
            _http.DefaultRequestHeaders.Add("social", social);
            _http.DefaultRequestHeaders.Add("itemName", itemName.ToString());
            _http.DefaultRequestHeaders.Add("toVehicle", toVehicle.ToString());

            if (toVehicle)
            {
                var weight = ItemCfg.ItemConfig.First(x => (uint)x.ItemName == itemName).Weight;

                if (VWeight + (weight * MoveValue) > VMaxWeight)
                {
                    _inProgress = false;
                    return;
                }

                var maxAmount = PlayerInvDic.Keys.First(x => x.Item == itemName).Amount;

                _http.DefaultRequestHeaders.Add("amount",
                    MoveValue > maxAmount ? maxAmount.ToString() : MoveValue.ToString());

                VehicleInvDic = GetVehicleInventory("Api/VehicleTrunk/MoveItem");
                PlayerInvDic = GetPlayerInventory("Api/VehicleTrunk/GetInventory");
            }
            else
            {
                var weight = ItemCfg.ItemConfig.First(x => (uint)x.ItemName == itemName).Weight;

                if (PWeight + (weight * MoveValue) > 100f)
                {
                    _inProgress = false;
                    return;
                }

                var maxAmount = VehicleInvDic.Keys.First(x => x.Item == itemName).Amount;

                _http.DefaultRequestHeaders.Add("amount",
                    MoveValue > maxAmount ? maxAmount.ToString() : MoveValue.ToString());

                VehicleInvDic = GetVehicleInventory("Api/VehicleTrunk/MoveItem");
                PlayerInvDic = GetPlayerInventory("Api/VehicleTrunk/GetInventory");
            }

            StateChanged();

            _inProgress = false;
        }

        public void CloseVehicleTrunk(string social)
        {
            if (_inProgress) return;

            _inProgress = true;

            _http.DefaultRequestHeaders.Clear();
            _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
            _http.DefaultRequestHeaders.Add("social", social);

            var result = _http.GetAsync("Api/Window/DestroyWindow").Result;

            _inProgress = false;
        }

        #endregion

        #region Counter

        public void LowerTradeValue(int value)
        {
            if (MoveValue - value < 1)
                MoveValue = 1;
            else
                MoveValue -= value;
        }

        public void RaiseTradeValue(int value)
        {
            MoveValue += value;
        }

        #endregion

        private void StateChanged() => OnChange?.Invoke();
    }
}