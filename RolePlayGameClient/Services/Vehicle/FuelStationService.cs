﻿using Newtonsoft.Json;
using RolePlayGameData.Configs;
using RolePlayGameData.Configs.Vehicle;
using RolePlayGameData.Database.RolePlayModels;
using RolePlayGameData.Enums;
using RolePlayGameData.Models;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace RolePlayGameClient.Services.Vehicle
{
    public class FuelStationService
    {
        public float ElectricPrice => new VehFuelCfg().GetElectricPrice();
        public float PetrolPrice => new VehFuelCfg().GetPetrolPrice();
        public float DieselPrice => new VehFuelCfg().GetDieselPrice();

        public float CurrBill_Electric { get; private set; } = 0f;
        public float CurrBill_Petrol { get; private set; } = 0f;
        public float CurrBill_Diesel { get; private set; } = 0f;

        public float CurrAmount_Electric { get; private set; } = 0f;
        public float CurrAmount_Petrol { get; private set; } = 0f;
        public float CurrAmount_Diesel { get; private set; } = 0f;

        public bool VehicleFound { get; private set; } = true;

        private VehicleModel _vehModel = new VehicleModel();
        public VehicleFuelModel _vehFuel = new VehicleFuelModel();
        private NewVehicleModel _vehSettings = new NewVehicleModel();
        private PlayerMoneyModel _playerMoney = new PlayerMoneyModel();

        public Action OnChange;

        private readonly HttpClient _http = new HttpClient();
        private bool _inProgress;
        private string _social;
        private bool _continue;
        private bool _wrongFuel;

        public FuelStationService() => _http.BaseAddress = ServerCfg.ClientSiteRunsLocal
            ? new Uri(ServerCfg.ApiLocal) : new Uri(ServerCfg.ApiRemote);

        public void Init(string social)
        {
            try
            {
                _social = social;

                _http.DefaultRequestHeaders.Clear();
                _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
                _http.DefaultRequestHeaders.Add("social", social);

                var response = _http.GetAsync("Api/FuelStation/GetVehicleData").Result;

                if (response.StatusCode == HttpStatusCode.NoContent)
                {
                    VehicleFound = false;
                }
                else
                {
                    var fuelStationData = JsonConvert.
                        DeserializeObject<FuelStationModel>(response.Content.ReadAsStringAsync().Result);

                    _vehModel = fuelStationData.Vehicle;
                    _vehFuel = fuelStationData.VehFuel;
                    _playerMoney = fuelStationData.PlayerMoney;

                    _vehSettings = VehicleCfg.AllVehicles.First(x => (uint)x.VehicleHash == _vehModel.VehicleHash);
                }
            }
            catch
            {
                _inProgress = true;
                VehicleFound = false;
            }
        }

        private async void RefuelLoop(VehicleFuelEnums fuelType)
        {
            var maxFuel = _vehSettings.TankCapacity - _vehFuel.TankStatus;
            _continue = true;

            switch (fuelType)
            {
                case VehicleFuelEnums.Electric:
                    while (_continue && CurrAmount_Electric + 1f <= maxFuel && CurrBill_Electric < _playerMoney.CashMoney)
                    {
                        CurrAmount_Electric += 1f;
                        CurrBill_Electric = CurrAmount_Electric * ElectricPrice;
                        await Task.Delay(500);
                        StateChanged();
                    }
                    break;
                case VehicleFuelEnums.Petrol:
                    while (_continue && CurrAmount_Petrol + 1f <= maxFuel && CurrBill_Petrol < _playerMoney.CashMoney)
                    {
                        CurrAmount_Petrol += 1f;
                        CurrBill_Petrol = CurrAmount_Petrol * PetrolPrice;
                        await Task.Delay(500);
                        StateChanged();
                    }

                    if (_vehSettings.FuelType != VehicleFuelEnums.Petrol && CurrAmount_Petrol > (_vehFuel.TankStatus / 4))
                        _wrongFuel = true;
                    break;
                case VehicleFuelEnums.Diesel:
                    while (_continue && CurrAmount_Diesel + 1f <= maxFuel && CurrBill_Diesel < _playerMoney.CashMoney)
                    {
                        CurrAmount_Diesel += 1f;
                        CurrBill_Diesel = CurrAmount_Diesel * DieselPrice;
                        await Task.Delay(500);
                        StateChanged();
                    }

                    if (_vehSettings.FuelType != VehicleFuelEnums.Diesel && CurrAmount_Petrol > (_vehFuel.TankStatus / 4))
                        _wrongFuel = true;
                    break;
            }

            _continue = false;
            _inProgress = false;
        }

        #region Buttons

        public void StartPump(VehicleFuelEnums fuelType)
        {
            if (_inProgress) return;

            if (fuelType == VehicleFuelEnums.Electric && _vehSettings.FuelType != VehicleFuelEnums.Electric)
            {
                // TODO: trigger window with no-electric info
                return;
            }

            if (_vehFuel.EngineStatus)
            {
                // TODO: trigger window with running engine info
                return;
            }

            _inProgress = true;

            RefuelLoop(fuelType);
        }

        public void StopPump() => _continue = false;

        public void PayFuel(VehicleFuelEnums fuelType)
        {
            if (_inProgress) return;

            _inProgress = true;

            _http.DefaultRequestHeaders.Clear();
            _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
            _http.DefaultRequestHeaders.Add("social", _social);
            _http.DefaultRequestHeaders.Add("wrongFuel", _wrongFuel.ToString());

            switch (fuelType)
            {
                case VehicleFuelEnums.Petrol:
                    _http.DefaultRequestHeaders.Add("price", CurrBill_Petrol.ToString());
                    _http.DefaultRequestHeaders.Add("amount", CurrAmount_Petrol.ToString());
                    break;
                case VehicleFuelEnums.Diesel:
                    _http.DefaultRequestHeaders.Add("price", CurrBill_Diesel.ToString());
                    _http.DefaultRequestHeaders.Add("amount", CurrAmount_Diesel.ToString());
                    break;
                case VehicleFuelEnums.Electric:
                    _http.DefaultRequestHeaders.Add("price", CurrBill_Electric.ToString());
                    _http.DefaultRequestHeaders.Add("amount", CurrAmount_Electric.ToString());
                    break;
            }

            var result = _http.GetAsync("Api/FuelStation/PayFuel").Result;

            _inProgress = false;
        }

        public void CloseFuelStation()
        {
            if (_inProgress) return;

            _inProgress = true;

            _http.DefaultRequestHeaders.Clear();
            _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
            _http.DefaultRequestHeaders.Add("social", _social);
            _http.DefaultRequestHeaders.Add("bill", CurrBill_Electric.ToString());
            _http.DefaultRequestHeaders.Add("amount", CurrAmount_Electric.ToString());
            _http.DefaultRequestHeaders.Add("wrongFuel", _wrongFuel.ToString());

            var result = _http.GetAsync("Api/FuelStation/CloseFuelStation").Result;

            _inProgress = false;
        }

        #endregion

        private void StateChanged() => OnChange?.Invoke();
    }
}