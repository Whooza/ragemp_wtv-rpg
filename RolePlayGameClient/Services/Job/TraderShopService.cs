﻿using Newtonsoft.Json;
using RolePlayGameData.Configs;
using RolePlayGameData.Database.RolePlayModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;

namespace RolePlayGameClient.Services.Job
{
    public class TraderShopService
    {
        public List<PlayerInvItemModel> PlayerInventory { get; private set; } = new List<PlayerInvItemModel>();
        public List<VehicleInvItemModel> VehicleInventory { get; private set; }
        public List<MarketSystemModel> MarketPrices { get; private set; }

        public string TraderName { get; set; } = string.Empty;
        public int TradeValue { get; set; } = 1;

        public Action OnChange;

        private readonly HttpClient _http = new HttpClient();
        private bool _inProgress;
        private string _social;
        private uint _markType;
        private string _vehicleValue;

        private static Dictionary<uint, List<uint>> TraderItems => TraderCfg.TraderItems;

        public TraderShopService() => _http.BaseAddress = ServerCfg.ClientSiteRunsLocal
                ? new Uri(ServerCfg.ApiLocal) : new Uri(ServerCfg.ApiRemote);

        public void Init(string social, string markType)
        {
            _social = social;
            _markType = Convert.ToUInt32(markType);

            TraderName = MarkCfg.LocationMarkSettings.FirstOrDefault(x => (uint)x.MarkType == _markType).MarkText;

            _http.DefaultRequestHeaders.Clear();
            _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
            _http.DefaultRequestHeaders.Add("social", _social);
            _http.DefaultRequestHeaders.Add("markType", markType);

            _vehicleValue = _http.GetStringAsync("Api/Trader/GetVehicleHandleValue").Result;   

            MarketPrices = GetMarketPrices();

            try
            {
                PlayerInventory = GetPlayerInventory(_markType, "Api/Trader/GetInventory");
            }
            catch
            {
                PlayerInventory = new List<PlayerInvItemModel>();
            }

            try
            {
                VehicleInventory = GetVehicleInventory(_markType, "Api/Trader/FindNextVehicleInventory");
            }
            catch
            {
                VehicleInventory = null;
            }

            StateChanged();
        }

        private List<MarketSystemModel> GetMarketPrices()
        {
            var result = _http.GetStringAsync("Api/Trader/GetPricesByMarkType").Result;

            return JsonConvert.DeserializeObject<List<MarketSystemModel>>(result);
        }

        #region GetAndSortInventory

        private List<PlayerInvItemModel> GetPlayerInventory(uint markType, string apiPath)
        {
            var vehInvString = _http.GetStringAsync(apiPath).Result;
            var playerInv = JsonConvert.DeserializeObject<List<PlayerInvItemModel>>(vehInvString);
            var newInv = new List<PlayerInvItemModel>();

            TraderItems.TryGetValue(markType, out var items);

            foreach (var pItem in playerInv)
            {
                if (!items.Contains(pItem.Item)) continue;

                newInv.Add(pItem);
            }

            return newInv;
        }

        private List<VehicleInvItemModel> GetVehicleInventory(uint markType, string apiPath)
        {
            var vehInvString = _http.GetStringAsync(apiPath).Result;
            var vehInv = JsonConvert.DeserializeObject<List<VehicleInvItemModel>>(vehInvString);
            var newInv = new List<VehicleInvItemModel>();

            TraderItems.TryGetValue(markType, out var items);

            foreach (var vItem in vehInv)
            {
                if (!items.Contains(vItem.Item)) continue;

                newInv.Add(vItem);
            }

            return newInv;
        }

        #endregion

        #region TradeValue

        public void LowerTradeValue(int value)
        {
            if (TradeValue - value < 1)
                TradeValue = 1;
            else
                TradeValue -= value;
        }

        public void RaiseTradeValue(int value)
        {
            TradeValue += value;
        }

        #endregion

        #region Buttons

        public void SellItems(string invType, uint itemName)
        {
            _http.DefaultRequestHeaders.Clear();
            _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
            _http.DefaultRequestHeaders.Add("social", _social);
            _http.DefaultRequestHeaders.Add("invType", invType);
            _http.DefaultRequestHeaders.Add("itemName", itemName.ToString());
            _http.DefaultRequestHeaders.Add("amount", TradeValue.ToString());
            _http.DefaultRequestHeaders.Add("vehicleHandle", _vehicleValue);

            switch (invType)
            {
                case "player":
                    try
                    {
                        PlayerInventory = GetPlayerInventory(_markType, "Api/Trader/SellTraderItems");
                    }
                    catch
                    {
                        PlayerInventory = new List<PlayerInvItemModel>();
                    }
                    break;
                case "vehicle":
                    try
                    {
                        VehicleInventory = GetVehicleInventory(_markType, "Api/Trader/SellTraderItems");
                    }
                    catch
                    {
                        VehicleInventory = null;
                    }
                    break;
            }

            StateChanged();
        }

        public void CloseTrader(string social)
        {
            if (_inProgress) return;

            _inProgress = true;

            _http.DefaultRequestHeaders.Clear();
            _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
            _http.DefaultRequestHeaders.Add("social", social);
            var result = _http.GetAsync("Api/Window/DestroyWindow").Result;

            _inProgress = false;
        }

        #endregion

        private void StateChanged() => OnChange?.Invoke();
    }
}