﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RolePlayGameData.Configs;
using RolePlayGameData.Database.RolePlayModels;
using RolePlayGameData.Models;

namespace RolePlayGameClient.Services.Job
{
    public class ProcessorService
    {
        private readonly HttpClient _http = new HttpClient();
        private bool _uiInProgress;

        public Action OnChange;

        public ProcessorService() => _http.BaseAddress = ServerCfg.ClientSiteRunsLocal
                ? new Uri(ServerCfg.ApiLocal) : new Uri(ServerCfg.ApiRemote);

        public List<PlayerInvItemModel> PlayerInventory { get; private set; } = new List<PlayerInvItemModel>();
        public List<JobRecipeModel> AvailableJobRecipes { get; private set; } = new List<JobRecipeModel>();
        public List<JobRecipeModel> ProcessingRecipes => ProcessorCfg.ProcessingRecipes;
        public RecipeBookModel RecipeBook { get; private set; } = new RecipeBookModel();
        public string ProcessorName { get; private set; } = string.Empty;

        public int Amount { get; set; } = 1;
        public int WaitList { get; set; }
        public string ProgressName { get; private set; } = string.Empty;
        public float Progress { get; private set; }
        public bool Continue { get; private set; }

        public void Init(string social, string markName)
        {
            var markType = Convert.ToUInt32(markName);

            ProcessorName = MarkCfg.LocationMarkSettings.FirstOrDefault(x => (uint) x.MarkType == markType).MarkText;

            _http.DefaultRequestHeaders.Clear();
            _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
            _http.DefaultRequestHeaders.Add("social", social);

            var inventoryString = _http.GetStringAsync("Api/Processor/GetInventory").Result;
            PlayerInventory = JsonConvert.DeserializeObject<List<PlayerInvItemModel>>(inventoryString);

            var recipeBookString = _http.GetStringAsync("Api/Processor/GetRecipeBook").Result;

            RecipeBook = JsonConvert.DeserializeObject<RecipeBookModel>(recipeBookString);

            var ownedRecipes = FillRecipes(markType);

            foreach (var item in ownedRecipes)
            {
                var entry = ProcessingRecipes.FirstOrDefault(x => (uint) x.Processor == markType && x.RecipeId == item);

                if (entry == default) continue;

                AvailableJobRecipes.Add(entry);
            }

            AvailableJobRecipes = AvailableJobRecipes.OrderBy(x => x.RecipeName).ToList();
        }

        private IEnumerable<int> FillRecipes(uint markType)
        {
            switch (markType)
            {
                case 129:
                    return RecipeBook.Butcher;
                case 130:
                    return RecipeBook.Carpenter;
                case 131:
                    return RecipeBook.CementFactory;
                case 132:
                    return RecipeBook.Dairy;
                case 133:
                    return RecipeBook.Distillery;
                case 134:
                    return RecipeBook.DrugFactory;
                case 135:
                    return RecipeBook.FlourMill;
                case 136:
                    return RecipeBook.Forge;
                case 137:
                    return RecipeBook.Glassworks;
                case 138:
                    return RecipeBook.JuicePress;
                case 139:
                    return RecipeBook.KetchupFactory;
                case 140:
                    return RecipeBook.MarijuhanaFactory;
                case 141:
                    return RecipeBook.Melt;
                case 142:
                    return RecipeBook.Minter;
                case 143:
                    return RecipeBook.PaperFactory;
                case 144:
                    return RecipeBook.RollingMill;
                case 145:
                    return RecipeBook.Sawmill;
                case 146:
                    return RecipeBook.StoneMill;
                case 147:
                    return RecipeBook.WalnutProcessing;
                default:
                    return new List<int>();
            }
        }

        private void StateChanged()
        {
            OnChange?.Invoke();
        }

        #region Processing

        private async void ProgressAction(string social, JobRecipeModel recipe)
        {
            if (Continue) return;

            var maxAmount = GetMaxProcessCount(recipe);
            var counter = 0;

            if (Amount > maxAmount) Amount = maxAmount;

            _uiInProgress = true;
            Continue = true;

            var processTick = recipe.ProcessTime / 100;

            WaitList = Amount;

            StateChanged();

            for (var a = 0; a < Amount; a++)
            {
                if (!Continue)
                {
                    if (counter > 0) GiveItemList(social, recipe.RecipeName, counter);

                    ResetProcess();
                    return;
                }

                for (var i = 0; i < 105; i++)
                {
                    if (!Continue)
                    {
                        ResetProcess();
                        continue;
                    }

                    Progress = i;

                    StateChanged();

                    await Task.Delay(processTick)
                        .ConfigureAwait(true);
                }

                if (Continue)
                {
                    counter += 1;
                    WaitList -= 1;
                }

                Progress = 0f;

                StateChanged();

                await Task.Delay(500).ConfigureAwait(true);
            }

            ProgressName = string.Empty;
            Continue = false;

            StateChanged();

            if (counter > 0) GiveItemList(social, recipe.RecipeName, counter);

            _uiInProgress = false;
        }

        private async void GiveItemList(string social, string recipeName, int amount)
        {
            _uiInProgress = true;

            try
            {
                _http.DefaultRequestHeaders.Clear();
                _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
                _http.DefaultRequestHeaders.Add("social", social);
                _http.DefaultRequestHeaders.Add("recipeName", recipeName);
                _http.DefaultRequestHeaders.Add("amount", amount.ToString());

                var result = await _http.GetStringAsync("Api/Processor/UpdateProcessedItemList")
                    .ConfigureAwait(false);

                PlayerInventory = JsonConvert.DeserializeObject<List<PlayerInvItemModel>>(result);
            }
            catch
            {
                return;
            }

            _uiInProgress = false;
        }

        private void ResetProcess()
        {
            ProgressName = string.Empty;
            WaitList = 0;
            Progress = 0f;
            Continue = false;
            StateChanged();
        }

        private int GetMaxProcessCount(JobRecipeModel recipe)
        {
            var mats = recipe.MatsList;
            var maxAmount = 100000;

            foreach (var (key, value) in mats)
            {
                var entry = PlayerInventory.FirstOrDefault(x => x.Item == (uint) key);

                if (entry == default) return 0;

                var maxA = ((int) entry.Amount - (int) entry.Amount % value) / value;

                if (maxA < maxAmount) maxAmount = maxA;
            }

            return maxAmount;
        }

        #endregion

        #region Buttons

        public void StartProcessing(string social, string recipeName)
        {
            if (Continue) return;
            if (_uiInProgress) return;

            _uiInProgress = true;

            var recipe = ProcessingRecipes.First(x => x.RecipeName == recipeName);

            ProgressName = recipe.RecipeName;

            ProgressAction(social, recipe);

            _uiInProgress = false;
        }

        public void StopProcessing()
        {
            Continue = false;
        }

        public void CloseProcessor(string social)
        {
            if (_uiInProgress) return;

            _uiInProgress = true;

            _http.DefaultRequestHeaders.Clear();
            _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
            _http.DefaultRequestHeaders.Add("social", social);
            var result = _http.GetAsync("Api/Window/DestroyWindow").Result;

            _uiInProgress = false;
        }

        #endregion
    }
}