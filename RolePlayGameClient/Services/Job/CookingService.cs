﻿using Newtonsoft.Json;
using RolePlayGameClientData.Enums;
using RolePlayGameData.Configs;
using RolePlayGameData.Database.RolePlayModels;
using RolePlayGameData.Enums;
using RolePlayGameData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace RolePlayGameClient.Services.Job
{
    public class CookingService
    {
        private readonly HttpClient _http = new HttpClient();
        private bool _uiInProgress;

        public Action OnChange;

        public CookingService()
        {
            _http.BaseAddress = ServerCfg.ClientSiteRunsLocal ? new Uri(ServerCfg.ApiLocal) : new Uri(ServerCfg.ApiRemote);

            AllCookingRecipes.AddRange(CookingCfg.CookingLvLOneRecipes);
            AllCookingRecipes.AddRange(CookingCfg.CookingLvLTwoRecipes);
            AllCookingRecipes.AddRange(CookingCfg.CookingLvLThreeRecipes);
            AllCookingRecipes.AddRange(CookingCfg.CookingLvLFourRecipes);
            AllCookingRecipes.AddRange(CookingCfg.CookingLvLFiveRecipes);
            AllCookingRecipes.AddRange(CookingCfg.CookingLvLSixRecipes);
        }

        public PlayerCookingModel PlayerCooking { get; private set; } = new PlayerCookingModel();
        public List<PlayerInvItemModel> PlayerInventory { get; private set; } = new List<PlayerInvItemModel>();
        public List<CookingRecipeModel> AllCookingRecipes { get; } = new List<CookingRecipeModel>();
        public CookingViewEnums View { get; private set; } = CookingViewEnums.ShowCategories;

        private string _social;
        public int Amount { get; set; } = 1;
        public int WaitList { get; set; }
        public string ProgressName { get; private set; } = string.Empty;
        public float Progress { get; private set; }
        public bool Continue { get; private set; }

        public void OnInit(string social)
        {
            _social = social;

            _http.DefaultRequestHeaders.Clear();
            _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
            _http.DefaultRequestHeaders.Add("social", social);

            var cooking = _http.GetStringAsync("Api/Cooking/GetCooking").Result;
            PlayerCooking = JsonConvert.DeserializeObject<PlayerCookingModel>(cooking);

            var inventory = _http.GetStringAsync("Api/Cooking/GetInventory").Result;
            PlayerInventory = JsonConvert.DeserializeObject<List<PlayerInvItemModel>>(inventory);
        }

        public void ChangeView(CookingViewEnums viewEnum)
        {
            if (_uiInProgress) return;

            _uiInProgress = true;

            View = viewEnum;

            StateChanged();

            _uiInProgress = false;
        }

        public void StartProcess(string recipeName)
        {
            if (Continue) return;
            if (_uiInProgress) return;

            _uiInProgress = true;

            var recipe = AllCookingRecipes.First(x => x.RecipeName == recipeName);

            ProgressName = recipe.RecipeName;

            ProgressAction(recipe);

            _uiInProgress = false;
        }

        public void StopProcess()
        {
            Continue = false;
        }

        private void ResetProcess()
        {
            ProgressName = string.Empty;
            WaitList = 0;
            Progress = 0f;
            Continue = false;
            StateChanged();
        }

        private async void ProgressAction(CookingRecipeModel recipe)
        {
            if (Continue) return;

            var maxAmount = GetMaxProcessCount(recipe);
            var counter = 0;

            if (Amount > maxAmount) Amount = maxAmount;

            _uiInProgress = true;
            Continue = true;

            var processTick = recipe.ProcessTime / 100;

            WaitList = Amount;

            StateChanged();

            for (var a = 0; a < Amount; a++)
            {
                if (!Continue)
                {
                    if (counter > 0) GiveItems((uint) recipe.CookedItemType, counter);
                    ResetProcess();
                    return;
                }

                for (var i = 0; i < 105; i++)
                {
                    if (!Continue)
                    {
                        ResetProcess();
                        continue;
                    }

                    Progress = i;
                    StateChanged();
                    await Task.Delay(processTick)
                        .ConfigureAwait(true);
                }

                if (Continue)
                {
                    counter += 1;
                    WaitList -= 1;
                }

                Progress = 0f;

                StateChanged();

                await Task.Delay(500).ConfigureAwait(true);
            }

            ProgressName = string.Empty;
            Continue = false;

            StateChanged();

            if (counter > 0) GiveItems((uint) recipe.CookedItemType, counter);

            _uiInProgress = false;
        }

        private async void GiveItems(uint itemName, int amount)
        {
            _uiInProgress = true;

            try
            {
                _http.DefaultRequestHeaders.Clear();
                _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
                _http.DefaultRequestHeaders.Add("social", _social);
                _http.DefaultRequestHeaders.Add("itemName", itemName.ToString());
                _http.DefaultRequestHeaders.Add("amount", amount.ToString());

                var result = await _http.GetStringAsync("Api/Cooking/UpdateProcessedItem")
                    .ConfigureAwait(false);

                PlayerInventory = JsonConvert.DeserializeObject<List<PlayerInvItemModel>>(result);
            }
            catch
            {
                return;
            }

            _uiInProgress = false;
        }

        private int GetMaxProcessCount(CookingRecipeModel recipe)
        {
            var mats = recipe.MatsList;
            var maxAmount = 100000;

            foreach (var (key, value) in mats)
            {
                var entry = PlayerInventory.FirstOrDefault(x => x.Item == (uint) key);

                if (entry == default) return 0;

                var maxA = ((int) entry.Amount - (int) entry.Amount % value) / value;

                if (maxA < maxAmount) maxAmount = maxA;
            }

            return maxAmount;
        }

        public List<CookingRecipeModel> GetFilteredCookingRecipes(CookingCategoryEnums category)
        {
            var points = PlayerCooking.CookingPoints;

            if (points > 5000)
                return AllCookingRecipes.Where(x => x.Category == category && x.Level <= CookingLevelEnums.LevelSix)
                    .OrderBy(x => x.RecipeName).ToList();
            if (points > 4000)
                return AllCookingRecipes.Where(x => x.Category == category && x.Level <= CookingLevelEnums.LevelFive)
                    .OrderBy(x => x.RecipeName).ToList();
            if (points > 3000)
                return AllCookingRecipes.Where(x => x.Category == category && x.Level <= CookingLevelEnums.LevelFour)
                    .OrderBy(x => x.RecipeName).ToList();
            if (points > 2000)
                return AllCookingRecipes.Where(x => x.Category == category && x.Level <= CookingLevelEnums.LevelThree)
                    .OrderBy(x => x.RecipeName).ToList();
            return points > 1000
                ? AllCookingRecipes.Where(x => x.Category == category && x.Level <= CookingLevelEnums.LevelTwo)
                    .OrderBy(x => x.RecipeName).ToList()
                : AllCookingRecipes.Where(x => x.Category == category && x.Level <= CookingLevelEnums.LevelOne)
                    .OrderBy(x => x.RecipeName).ToList();
        }

        public void CloseCooking(string social)
        {
            if (_uiInProgress) return;

            _uiInProgress = true;

            _http.DefaultRequestHeaders.Clear();
            _http.DefaultRequestHeaders.Add("token", ServerCfg.ApiToken);
            _http.DefaultRequestHeaders.Add("social", social);
            
            var result = _http.GetAsync("Api/Window/DestroyWindow").Result;

            _uiInProgress = false;
        }

        private void StateChanged() => OnChange?.Invoke();
    }
}