using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using RolePlayGameClient.Services.Account;
using RolePlayGameClient.Services.Basic;
using RolePlayGameClient.Services.Character;
using RolePlayGameClient.Services.Government;
using RolePlayGameClient.Services.Job;
using RolePlayGameClient.Services.Money;
using RolePlayGameClient.Services.Player;
using RolePlayGameClient.Services.Server;
using RolePlayGameClient.Services.ServerTeam;
using RolePlayGameClient.Services.Shop;
using RolePlayGameClient.Services.Vehicle;

namespace RolePlayGameClient
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            // blazor default //
            services.AddRazorPages();
            services.AddServerSideBlazor();

            // basic //
            services.AddScoped<AccessService>();
            services.AddScoped<ProcessBarService>();

            // account //
            services.AddScoped<LoginService>();
            services.AddScoped<RegisterService>();
            services.AddScoped<BannedService>();

            // character //
            services.AddScoped<CharCreatorService>();

            // government //
            services.AddScoped<CityHallService>();
            services.AddScoped<JobCenterService>();
            services.AddScoped<LicenseOfficeService>();

            // job //
            services.AddScoped<CookingService>();
            services.AddScoped<ProcessorService>();
            services.AddScoped<TraderShopService>();

            // money //
            services.AddScoped<AtmService>();
            services.AddScoped<BankService>();
            services.AddScoped<CentralBankService>();

            // player //
            services.AddScoped<InventoryService>();
            services.AddScoped<RecipeBookService>();
            services.AddScoped<SmartPhoneService>();

            // server //
            services.AddScoped<DumpsterService>();

            // serverTeam //
            services.AddScoped<ClothingSpawnerService>();
            services.AddScoped<ItemSpawnerService>();
            services.AddScoped<MapMarkEditorService>();
            services.AddScoped<LocationTeleporterService>();

            // shop //
            services.AddScoped<ClothingShopService>();
            services.AddScoped<HairCutShopService>();
            services.AddScoped<HatShopService>();
            services.AddScoped<ItemShopService>();
            services.AddScoped<TattooShopService>();

            // vehicle //
            services.AddScoped<FuelStationService>();
            services.AddScoped<VehicleBuyService>();
            services.AddScoped<VehicleGarageService>();
            services.AddScoped<VehicleInteractionService>();
            services.AddScoped<VehicleTrunkService>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();
            else
                app.UseHsts();

            app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
            });
        }
    }
}