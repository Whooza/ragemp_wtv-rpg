﻿function updateSpeedometer(speed) {
    document.getElementById("speedGauge").dataset.value = speed;
}

function updateKm(km) {
    document.getElementById("kml").innerHTML = km;
}

function updateFuel(fuel) {
    document.getElementById("fuelGauge").dataset.value = fuel;
}

function initialLoad() {
    $("#speedGauge").addClass("animated fadeOut 2s");
    $("#fuelGauge").addClass("animated fadeOut 1s");
    $("#kmDisplay").addClass("animated fadeOut 1s");
    $("#progressBarRow").addClass("animated fadeOut 2s");
    document.getElementById("healthIcon").hidden = true;
    document.getElementById("armorIcon").hidden = true;
    document.getElementById("hungerIcon").hidden = true;
    document.getElementById("thirstIcon").hidden = true;
    document.getElementById("voiceStatusPic").src = "/images/voice/micError.png";
}

function showSpeedometer() {
    $("#speedGauge").removeClass("animated fadeOut 2s");
    $("#fuelGauge").removeClass("animated fadeOut 1s");
    $("#kmDisplay").removeClass("animated fadeOut 1s");
    $("#speedGauge").addClass("animated fadeIn 2s");
    $("#fuelGauge").addClass("animated  fadeIn 4s");
    $("#kmDisplay").addClass("animated  fadeIn 4s");
}

function hideSpeedometer() {
    $("#speedGauge").removeClass("animated fadeIn 2s");
    $("#fuelGauge").removeClass("animated fadeIn 4s");
    $("#kmDisplay").removeClass("animated fadeIn 4s");
    $("#speedGauge").addClass("animated fadeOut 2s");
    $("#fuelGauge").addClass("animated fadeOut 1s");
    $("#kmDisplay").addClass("animated fadeOut 1s");
}

function changeVoiceIcon(name) {
    switch (name.toString()) {
        case "0":
            document.getElementById("voiceStatusPic").src = "/images/voice/micError.png";
            break;
        case "1":
            document.getElementById("voiceStatusPic").src = "/images/voice/micMute.png";
            break;
        case "2":
            document.getElementById("voiceStatusPic").src = "/images/voice/micGreenOne.png";
            break;
        case "3":
            document.getElementById("voiceStatusPic").src = "/images/voice/micGreenTwo.png";
            break;
        case "4":
            document.getElementById("voiceStatusPic").src = "/images/voice/micGreenThree.png";
            break;
        default:
            break;
    }
}

function showHideSackHud(value) {
    if (value.toString() === "1") {
        document.getElementById("uiTopRow").hidden = true;
        document.getElementById("uiFooter").hidden = true;
        document.getElementById("sackHud").hidden = false;
    } else {
        document.getElementById("sackHud").hidden = true;
        document.getElementById("uiTopRow").hidden = false;
        document.getElementById("uiFooter").hidden = false;
    }
}

function showHideLowHunger(value) {
    if (value.toString() === "1") {
        $("#hungerIcon").addClass("animated flash slower infinite");
        document.getElementById("hungerIcon").hidden = false;
        setTimeout(function () {
            $("#hungerIcon").removeClass("animated flash slower infinite");
            showHideLowHunger(0);
        },
            3500);
    } else if (value.toString() === "2") {
        document.getElementById("hungerIcon").hidden = false;
    } else document.getElementById("hungerIcon").hidden = true;
}

function showHideLowThirst(value) {
    if (value.toString() === "1") {
        $("#thirstIcon").addClass("animated flash slower infinite");
        document.getElementById("thirstIcon").hidden = false;
        setTimeout(function () {
            $("#thirstIcon").removeClass("animated flash slower infinite");
            showHideLowThirst(0);
        },
            3500);
    } else if (value.toString() === "2") {
        document.getElementById("thirstIcon").hidden = false;
    } else document.getElementById("thirstIcon").hidden = true;
}

function showHideLowHealth(value) {
    if (value.toString() === "1") {
        $("#healthIcon").addClass("animated flash slower infinite");
        document.getElementById("healthIcon").hidden = false;
        setTimeout(function () {
            $("#healthIcon").removeClass("animated flash slower infinite");
            showHideLowHealth(0);
        },
            3500);
    } else if (value.toString() === "2") {
        document.getElementById("healthIcon").hidden = false;
    } else document.getElementById("healthIcon").hidden = true;
}

function showHideLowArmor(value) {
    if (value.toString() === "1") {
        $("#armorIcon").addClass("animated flash slower infinite");
        document.getElementById("armorIcon").hidden = false;
        setTimeout(function () {
            $("#armorIcon").removeClass("animated flash slower infinite");
            showHideLowArmor(0);
        },
            3500);
    } else if (value.toString() === "2") {
        document.getElementById("armorIcon").hidden = false;
    } else document.getElementById("armorIcon").hidden = true;
}

function showHideDmgHud(value) {
    if (value.toString() === "1") {
        document.getElementById("uiTopRow").hidden = true;
        document.getElementById("uiFooter").hidden = true;
        document.getElementById("dmgHud").hidden = false;
    } else {
        document.getElementById("dmgHud").hidden = true;
        document.getElementById("uiTopRow").hidden = false;
        document.getElementById("uiFooter").hidden = false;
    }
}

function showHitMarker() {

    var hitNumber = getRandomInt(5);

    switch (hitNumber) {
        case 0:
            document.getElementById("hitMark").src = "/images/hitMarkers/hitMarkerOne.png";
            break;
        case 1:
            document.getElementById("hitMark").src = "/images/hitMarkers/hitMarkerTwo.png";
            break;
        case 2:
            document.getElementById("hitMark").src = "/images/hitMarkers/hitMarkerThree.png";
            break;
        case 3:
            document.getElementById("hitMark").src = "/images/hitMarkers/hitMarkerFour.png";
            break;
        case 4:
            document.getElementById("hitMark").src = "/images/hitMarkers/hitMarkerFive.png";
            break;
        default:
            break;
    }

    document.getElementById("hitMark").hidden = false;
    setTimeout(function () {
        document.getElementById("hitMark").hidden = true;
    }, 750);
}

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

function showProgressBar() {
    document.getElementById("progressBar").style.width = "0%";
    $("#progressBarRow").removeClass("animated fadeOut 2s");
    $("#progressBarRow").addClass("animated fadeIn 2s");
}

function hideProgressBar() {
    $("#progressBarRow").removeClass("animated fadeIn 2s");
    $("#progressBarRow").addClass("animated fadeOut 2s");
}

function updateProgressBar(actionName, value) {
    document.getElementById("progressTitle").innerHTML = actionName;
    document.getElementById("progressBar").style.width = value + "%";
}

var lastMsgLeft = 0;
var lastMsgRight = 0;

function showLeftMessage(body, color) {
    switch (lastMsgLeft) {
        case 0:
            lastMsgLeft = 1;
            document.getElementById("leftMsg0text").innerHTML = body;
            document.getElementById("leftMsg0text").style.color = color;
            $("#leftMsg0").toast("show");
            break;
        case 1:
            lastMsgLeft = 2;
            document.getElementById("leftMsg1text").innerHTML = body;
            document.getElementById("leftMsg1text").style.color = color;
            $("#leftMsg1").toast("show");
            break;
        case 2:
            lastMsgLeft = 3;
            document.getElementById("leftMsg2text").innerHTML = body;
            document.getElementById("leftMsg2text").style.color = color;
            $("#leftMsg2").toast("show");
            break;
        case 3:
            lastMsgLeft = 4;
            document.getElementById("leftMsg3text").innerHTML = body;
            document.getElementById("leftMsg3text").style.color = color;
            $("#leftMsg3").toast("show");
            break;
        case 4:
            lastMsgLeft = 5;
            document.getElementById("leftMsg4text").innerHTML = body;
            document.getElementById("leftMsg4text").style.color = color;
            $("#leftMsg4").toast("show");
            break;
        case 5:
            lastMsgLeft = 6;
            document.getElementById("leftMsg5text").innerHTML = body;
            document.getElementById("leftMsg5text").style.color = color;
            $("#leftMsg5").toast("show");
            break;
        case 6:
            lastMsgLeft = 7;
            document.getElementById("leftMsg6text").innerHTML = body;
            document.getElementById("leftMsg6text").style.color = color;
            $("#leftMsg6").toast("show");
            break;
        case 7:
            lastMsgLeft = 8;
            document.getElementById("leftMsg7text").innerHTML = body;
            document.getElementById("leftMsg7text").style.color = color;
            $("#leftMsg7").toast("show");
            break;
        case 8:
            lastMsgLeft = 9;
            document.getElementById("leftMsg8text").innerHTML = body;
            document.getElementById("leftMsg8text").style.color = color;
            $("#leftMsg8").toast("show");
            break;
        case 9:
            lastMsgLeft = 0;
            document.getElementById("leftMsg9text").innerHTML = body;
            document.getElementById("leftMsg9text").style.color = color;
            $("#leftMsg9").toast("show");
            break;
    }
}

function showRightMessage(header, body, color) {
    switch (lastMsgRight) {
        case 0:
            lastMsgRight = 1;
            document.getElementById("rightMsg0head").innerHTML = header;
            document.getElementById("rightMsg0text").innerHTML = body;
            document.getElementById("rightMsg0text").style.color = color;
            $("#rightMsg0").toast("show");
            break;
        case 1:
            lastMsgRight = 2;
            document.getElementById("rightMsg1head").innerHTML = header;
            document.getElementById("rightMsg1text").innerHTML = body;
            document.getElementById("rightMsg1text").style.color = color;
            $("#rightMsg1").toast("show");
            break;
        case 2:
            lastMsgRight = 3;
            document.getElementById("rightMsg2head").innerHTML = header;
            document.getElementById("rightMsg2text").innerHTML = body;
            document.getElementById("rightMsg2text").style.color = color;
            $("#rightMsg2").toast("show");
            break;
        case 3:
            lastMsgRight = 4;
            document.getElementById("rightMsg3head").innerHTML = header;
            document.getElementById("rightMsg3text").innerHTML = body;
            document.getElementById("rightMsg3text").style.color = color;
            $("#rightMsg3").toast("show");
            break;
        case 4:
            lastMsgRight = 0;
            document.getElementById("rightMsg4head").innerHTML = header;
            document.getElementById("rightMsg4text").innerHTML = body;
            document.getElementById("rightMsg4text").style.color = color;
            $("#rightMsg4").toast("show");
            break;
    }
}
