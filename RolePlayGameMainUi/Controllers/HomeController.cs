﻿using Microsoft.AspNetCore.Mvc;
using RolePlayGameData.Configs;

namespace RolePlayGameMainUi.Controllers
{
    public class HomeController : Controller
    {
        private static string Token => ServerCfg.WebToken;

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Ui(string id)
        {
            if (!ServerCfg.AccessServicesEnabled) return View();
            if (id != Token) return RedirectToAction("Index", "Home");
            return View();
        }
    }
}