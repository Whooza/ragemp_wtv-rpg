﻿using GTANetworkAPI;
using RolePlayGameData.Enums;
using RolePlayGameServer.Database.Controllers;
using RolePlayGameServer.Util;
using System;
using System.Threading.Tasks;

namespace RolePlayGameServer.Players
{
    internal class PlayerRegistration
    {
        private readonly Client _player;

        public PlayerRegistration(Client player) => _player = player;

        public async Task<bool> RegisterRequestAsync(string mail, string pass)
        {
            try
            {
                await RolePlayDbController.SetPlayerAccByParams(_player.SocialClubName, mail, pass).ConfigureAwait(false);

                await RolePlayDbController.SetBankFleeca(_player.SocialClubName).ConfigureAwait(false);

                await RolePlayDbController.SetBankMaze(_player.SocialClubName).ConfigureAwait(false);

                await RolePlayDbController.SetBankUnion(_player.SocialClubName).ConfigureAwait(false);

                var account = await RolePlayDbController.GetPlayerAccBySocial(_player.SocialClubName).ConfigureAwait(false);

                await RolePlayDbController.SetGarageTruckByParams(_player.SocialClubName, account.Id).ConfigureAwait(false);

                await RolePlayDbController.SetGarageBigByParams(_player.SocialClubName, account.Id).ConfigureAwait(false);

                await RolePlayDbController.SetGarageMediumByParams(_player.SocialClubName, account.Id).ConfigureAwait(false);

                await RolePlayDbController.SetGarageSmallByParams(_player.SocialClubName, account.Id).ConfigureAwait(false);

                await RolePlayDbController.SetPlayerBioByParams(_player.SocialClubName).ConfigureAwait(false);

                await RolePlayDbController.SetPlayerCharSettings(_player.SocialClubName).ConfigureAwait(false);

                await RolePlayDbController.SetPlayerCooking(_player.SocialClubName).ConfigureAwait(false);

                await RolePlayDbController.SetPlayerInvItemInDb(_player.SocialClubName, (uint)ItemNameEnums.Water, 5)
                    .ConfigureAwait(false);

                await RolePlayDbController.SetPlayerInvItemInDb(_player.SocialClubName, (uint)ItemNameEnums.Donut, 3)
                    .ConfigureAwait(false);

                await RolePlayDbController.SetPlayerMoney(_player.SocialClubName, 2500.0f).ConfigureAwait(false);

                await RolePlayDbController.SetPlayerParticular(_player.SocialClubName, false, string.Empty, string.Empty,
                        string.Empty, string.Empty, string.Empty, string.Empty, true).ConfigureAwait(false);

                await RolePlayDbController.SetPlayerPaycheck(_player.SocialClubName).ConfigureAwait(false);

                await RolePlayDbController.SetPlayerPosInDb(_player.SocialClubName, _player.Position.X, _player.Position.Y,
                    _player.Position.Z, _player.Rotation.Z, _player.Dimension).ConfigureAwait(false);

                await RolePlayDbController.SetPlayerRecipeBook(_player.SocialClubName).ConfigureAwait(false);

                await RolePlayDbController.SetPlayerWeapon(_player.SocialClubName, (uint)WeaponHashes.Firework, 3)
                    .ConfigureAwait(false);

                NAPI.Task.Run(() =>
                {
                    NAPI.Player.SetPlayerSkin(_player, PedHash.FreemodeMale01);

                    _player.Dimension = (uint)account.Id;
                    _player.Position = new Vector3(152.3787f, -1000.644f, -99f);
                    _player.Rotation = new Vector3(0.0f, 0.0f, 180.0f);

                    _player.SetClothes(11, 15, 0);
                    _player.SetClothes(3, 15, 0);
                    _player.SetClothes(8, 15, 0);

                    UiHelper.ShowCharCreator(_player);
                    UiHelper.SetCreatorCamera(_player);
                });

                return true;
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"RegisterRequestAsync - {e.Message}");
                return false;
            }
        }
    }
}