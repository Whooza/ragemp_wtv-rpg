﻿using GTANetworkAPI;
using Newtonsoft.Json;
using RolePlayGameData.Database.RolePlayModels;
using RolePlayGameData.EntityDataConsts;
using RolePlayGameServer.Database.Controllers;
using RolePlayGameServer.Garages;
using RolePlayGameServer.ServerLists;
using RolePlayGameServer.Util;
using RolePlayGameServer.Voice;
using System;
using System.Threading.Tasks;

namespace RolePlayGameServer.Players
{
    internal class PlayerLogin
    {
        private readonly Client _player;

        public PlayerLogin(Client player) => _player = player;

        public async Task<bool> LoginRequestAsync(string pass)
        {
            try
            {
                var account = await RolePlayDbController.GetPlayerAccBySocial(_player.SocialClubName)
                    .ConfigureAwait(false);

                if (pass != account.UserPass) return false;
#if DEBUG
                Logging.PlayerInfoLog($"Player: {_player.SocialClubName} - login started!");
#endif
                if (await LoginActionAsync(account).ConfigureAwait(false))
                {                    
                    _player.ResetData(PlayerDataConst.IsInLoginActionFlag);
                    _player.SetData(PlayerDataConst.IsLoggedInFlag, true);

                    UiHelper.DestroyBrowserWindow(_player);
                    UiHelper.ShowMainUi(_player);

                    _player.TriggerEvent("setCivillianBlips", JsonConvert.SerializeObject(BlipLists.CivillianBlips));

                    new VoiceController().Connect(_player, _player.SocialClubName);

                    new GarageAreaSpawner((uint)account.Id).SpawnGarageLocations();

                    return true;
                }

                _player.SendNotification("~r~Error while loginAction");
                _player.Kick();

                return false;
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"LoginRequestAsync - {e.Message}");
                return false;
            }
        }

        private async Task<bool> LoginActionAsync(PlayerAccountModel account)
        {
            try
            {
                // load from db //                
                var biology = await RolePlayDbController.GetPlayerBioFromDb(account.SocialClubName)
                    .ConfigureAwait(false);
                var charset = await RolePlayDbController.GetPlayerCharSettings(account.SocialClubName)
                    .ConfigureAwait(false);
                var money = await RolePlayDbController.GetPlayerMoney(account.SocialClubName)
                    .ConfigureAwait(false);
                var particular = await RolePlayDbController.GetPlayerParticular(account.SocialClubName)
                    .ConfigureAwait(false);
                var paycheck = await RolePlayDbController.GetPlayerPaycheckFromDb(account.SocialClubName)
                    .ConfigureAwait(false);
                var position = await RolePlayDbController.GetPlayerPosFromDb(account.SocialClubName)
                    .ConfigureAwait(false);

                // set to player //
                _player.SetData(PlayerDataConst.PlayerAccount, account);
                _player.SetData(PlayerDataConst.PlayerBiology, biology);
                _player.SetData(PlayerDataConst.PlayerCharSettings, charset);
                _player.SetData(PlayerDataConst.PlayerMoney, money);
                _player.SetData(PlayerDataConst.PlayerParticular, particular);

                NAPI.Task.Run(() => new CharCustomization(_player, particular).ApplyPlayerCustomization());

                paycheck.InCivActive = true;
                _player.SetData(PlayerDataConst.PlayerPaycheck, paycheck);

                await RolePlayDbController.UpdatePlayerPaycheck(paycheck)
                    .ConfigureAwait(false);

                _player.SetData(PlayerDataConst.PlayerPosition, position);

                NAPI.Entity.SetEntityPosition(_player,
                    new Vector3(position.LastPosX, position.LastPosY, position.LastPosZ));

                return true;
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"LoginActionAsync - {e.Message}");
                return false;
            }
        }
    }
}