﻿using System;
using System.Linq;
using System.Threading.Tasks;
using GTANetworkAPI;
using RolePlayGameData.Configs;
using RolePlayGameData.Database.RolePlayModels;
using RolePlayGameData.EntityDataConsts;
using RolePlayGameServer.Database.Controllers;
using RolePlayGameServer.Util;

namespace RolePlayGameServer.Players
{
    internal class PlayerBiology
    {
        public async Task StartPlayerBioAsync()
        {
#if DEBUG
            Logging.ServerInfoLog("starting playerBiology");
#endif
            try
            {
                var allPlayers = NAPI.Pools.GetAllPlayers();

                if (!allPlayers.Any())
                {
#if DEBUG
                    Logging.PlayerBiologyInfoLog("StartPlayerBioAsync - no player found");
#endif
                    return;
                }

                foreach (var item in allPlayers)
                    if (item.HasData(PlayerDataConst.IsLoggedInFlag))
                        await Task.Run(() => BiologyAction(item)).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"{this} - {e.Message}");
            }
        }

        private async void BiologyAction(Client player)
        {
            try
            {
                var biology = (PlayerBiologyModel)player.GetData(PlayerDataConst.PlayerBiology);

                if (!player.Dead && (biology.Hunger <= 0.0f || biology.Thirst <= 0.0f))
                {
                    if (biology.Hunger <= 0.0f) biology.Hunger = 0f;
                    if (biology.Thirst <= 0.0f) biology.Thirst = 0f;

                    player.Health -= 20;

                    UiHelper.SendHungerWarning(player, biology.Hunger);
                    UiHelper.SendThirstWarning(player, biology.Thirst);
                }
                else if (!player.Dead && biology.Hunger > 0f && biology.Thirst > 0f)
                {
                    biology.Hunger -= PlayerCfg.Hunger;
                    biology.Thirst -= PlayerCfg.Thirst;

                    if (biology.Hunger < 0f) biology.Hunger = 0f;
                    if (biology.Thirst < 0f) biology.Thirst = 0f;

                    UiHelper.SendHungerWarning(player, biology.Hunger);
                    UiHelper.SendThirstWarning(player, biology.Thirst);
                }
                else
                {
                    Logging.PlayerErrorLog($"{this}");
                }

                player.SetData(PlayerDataConst.PlayerBiology, biology);

                await RolePlayDbController.UpdatePlayerBioInDb(biology)
                    .ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"BiologyAction - {e.Message}");
            }
        }

        public static async Task SetFullBiology(Client player)
        {
            try
            {
                var biology = (PlayerBiologyModel)player.GetData(PlayerDataConst.PlayerBiology);

                biology.Hunger = 100.0f;
                biology.Thirst = 100.0f;

                player.SetData(PlayerDataConst.PlayerBiology, biology);

                await RolePlayDbController.UpdatePlayerBioInDb(biology)
                    .ConfigureAwait(false);

                UiHelper.UpdatePlayerBiology(player, biology.Hunger, biology.Thirst);
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"SetFullBiologyAsync - {e.Message}");
            }
        }
    }
}