﻿using GTANetworkAPI;
using Microsoft.EntityFrameworkCore.Internal;
using RolePlayGameData.Database.RolePlayModels;
using RolePlayGameData.EntityDataConsts;
using RolePlayGameServer.Database.Controllers;
using RolePlayGameServer.Util;
using System;
using System.Threading.Tasks;

namespace RolePlayGameServer.Players
{
    internal class PlayerPosition
    {
        public async Task StartPlayerPosAsync()
        {
#if DEBUG
            Logging.ServerInfoLog($"starting {this}");
#endif
            try
            {
                var allPlayers = NAPI.Pools.GetAllPlayers();

                if (!allPlayers.Any())
                {
#if DEBUG
                    Logging.PlayerPosInfoLog("no player found");
#endif
                    return;
                }

                foreach (var item in allPlayers)
                {
                    if (!item.HasData(PlayerDataConst.IsLoggedInFlag) || item.HasData(PlayerDataConst.IsInGarageFlag)) continue;
                    await Task.Run(() => PlayerPosAction(item)).ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"StartPlayerPosAsync - {e.Message}");
            }
        }

        private static async void PlayerPosAction(Entity player)
        {
            try
            {
                var position = (PlayerPositionModel)player.GetData(PlayerDataConst.PlayerPosition);

                position.LastPosX = player.Position.X;
                position.LastPosY = player.Position.Y;
                position.LastPosZ = player.Position.Z + 1.5f;
                position.Rotation = player.Rotation.Z;
                position.Dimension = player.Dimension;

                player.SetData(PlayerDataConst.PlayerPosition, position);

                await RolePlayDbController.UpdatePlayerPosInDb(position).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"PlayerPosAction - {e.Message}");
            }
        }
    }
}