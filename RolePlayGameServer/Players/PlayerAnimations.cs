﻿using GTANetworkAPI;
using RolePlayGameData.Enums;

namespace RolePlayGameServer.Players
{
    internal class PlayerAnimations
    {
        private readonly int _animName;
        private readonly int _animType;
        private readonly Client _player;

        public PlayerAnimations(Client player, int animType, int animName)
        {
            _player = player;
            _animType = animType;
            _animName = animName;
        }

        public void PlayAnimation()
        {
            switch (_animType)
            {
                case 0:
                    SetWalkingStyle();
                    break;
                case 1:
                    PublicAnimation();
                    break;
                case 2:
                    PlayDanceAnimation();
                    break;
                case 3:
                    PlayFactionAnimation();
                    break;
                case 4:
                    PlayMiningAnimation();
                    break;
                default:
                    _player.StopAnimation();
                    return;
            }
        }

        private void SetWalkingStyle()
        {
            _player.StopAnimation();

            switch (_animName)
            {
                case 1:
                    _player.PlayAnimation("move_f@heels@c", "walk",
                        (int) (AnimFlagsEnums.OnlyAnimateUpperBody | AnimFlagsEnums.AllowPlayerControl));
                    break;
                case 2:
                    _player.PlayAnimation("move_f@arrogant@a", "walk",
                        (int) (AnimFlagsEnums.OnlyAnimateUpperBody | AnimFlagsEnums.AllowPlayerControl));
                    break;
                case 3:
                    _player.PlayAnimation("move_f@sad@a", "walk",
                        (int) (AnimFlagsEnums.OnlyAnimateUpperBody | AnimFlagsEnums.AllowPlayerControl));
                    break;
                case 4:
                    _player.PlayAnimation("move_m@drunk@moderatedrunk", "walk",
                        (int) (AnimFlagsEnums.OnlyAnimateUpperBody | AnimFlagsEnums.AllowPlayerControl));
                    break;
                case 5:
                    _player.PlayAnimation("move_m@shadyped@a", "walk",
                        (int) (AnimFlagsEnums.OnlyAnimateUpperBody | AnimFlagsEnums.AllowPlayerControl));
                    break;
                case 6:
                    _player.PlayAnimation("move_f@gangster@ng", "walk",
                        (int) (AnimFlagsEnums.OnlyAnimateUpperBody | AnimFlagsEnums.AllowPlayerControl));
                    break;
                case 7:
                    _player.PlayAnimation("move_f@generic", "walk",
                        (int) (AnimFlagsEnums.OnlyAnimateUpperBody | AnimFlagsEnums.AllowPlayerControl));
                    break;
                case 8:
                    _player.PlayAnimation("move_f@heels@d", "walk",
                        (int) (AnimFlagsEnums.OnlyAnimateUpperBody | AnimFlagsEnums.AllowPlayerControl));
                    break;
                case 9:
                    _player.PlayAnimation("move_f@posh@", "walk",
                        (int) (AnimFlagsEnums.OnlyAnimateUpperBody | AnimFlagsEnums.AllowPlayerControl));
                    break;
                case 10:
                    _player.PlayAnimation("move_m@brave@b", "walk",
                        (int) (AnimFlagsEnums.OnlyAnimateUpperBody | AnimFlagsEnums.AllowPlayerControl));
                    break;
                case 11:
                    _player.PlayAnimation("move_m@confident", "walk",
                        (int) (AnimFlagsEnums.OnlyAnimateUpperBody | AnimFlagsEnums.AllowPlayerControl));
                    break;
                case 12:
                    _player.PlayAnimation("move_m@depressed@d", "walk",
                        (int) (AnimFlagsEnums.OnlyAnimateUpperBody | AnimFlagsEnums.AllowPlayerControl));
                    break;
                case 13:
                    _player.PlayAnimation("move_m@favor_right_foot", "walk",
                        (int) (AnimFlagsEnums.OnlyAnimateUpperBody | AnimFlagsEnums.AllowPlayerControl));
                    break;
                case 14:
                    _player.PlayAnimation("move_m@generic", "walk",
                        (int) (AnimFlagsEnums.OnlyAnimateUpperBody | AnimFlagsEnums.AllowPlayerControl));
                    break;
                case 15:
                    _player.PlayAnimation("move_m@generic_variations@walk", "walk_a",
                        (int) (AnimFlagsEnums.OnlyAnimateUpperBody | AnimFlagsEnums.AllowPlayerControl));
                    break;
                case 16:
                    _player.PlayAnimation("move_m@generic_variations@walk", "walk_f",
                        (int) (AnimFlagsEnums.OnlyAnimateUpperBody | AnimFlagsEnums.AllowPlayerControl));
                    break;
                case 17:
                    _player.PlayAnimation("move_m@golfer@", "golf_walk",
                        (int) (AnimFlagsEnums.OnlyAnimateUpperBody | AnimFlagsEnums.AllowPlayerControl));
                    break;
                case 18:
                    _player.PlayAnimation("move_m@money", "walk",
                        (int) (AnimFlagsEnums.OnlyAnimateUpperBody | AnimFlagsEnums.AllowPlayerControl));
                    break;
                case 19:
                    _player.PlayAnimation("move_m@shadyped@a", "walk",
                        (int) (AnimFlagsEnums.OnlyAnimateUpperBody | AnimFlagsEnums.AllowPlayerControl));
                    break;
                case 20:
                    _player.PlayAnimation("move_m@swagger@b", "walk",
                        (int) (AnimFlagsEnums.OnlyAnimateUpperBody | AnimFlagsEnums.AllowPlayerControl));
                    break;
                case 21:
                    _player.PlayAnimation("switch@franklin@dispensary", "exit_dispensary_outro_ped_f_a",
                        (int) (AnimFlagsEnums.OnlyAnimateUpperBody | AnimFlagsEnums.AllowPlayerControl));
                    break;
                default:
                    NAPI.Util.ConsoleOutput("error set walking style");
                    return;
            }
        }

        private void PublicAnimation()
        {
            _player.StopAnimation();

            switch (_animName)
            {
                case (int) CivilianAnimations.MoneyGrab:
                    _player.PlayAnimation("anim@heists@money_grab@duffel", "loop",
                        (int) AnimFlagsEnums.StopOnLastFrame);
                    break;
                case (int) CivilianAnimations.FacePalm:
                    _player.PlayAnimation("anim@mp_player_intcelebrationmale@face_palm", "face_palm",
                        (int) AnimFlagsEnums.StopOnLastFrame);
                    break;
                case (int) CivilianAnimations.YouLoco:
                    _player.PlayAnimation("anim@mp_player_intcelebrationmale@you_loco", "you_loco",
                        (int) AnimFlagsEnums.StopOnLastFrame);
                    break;
                case (int) CivilianAnimations.FreakOut:
                    _player.PlayAnimation("anim@mp_player_intcelebrationmale@freakout", "freakout",
                        (int) AnimFlagsEnums.StopOnLastFrame);
                    break;
                case (int) CivilianAnimations.ThumbOnEars:
                    _player.PlayAnimation("anim@mp_player_intcelebrationmale@thumb_on_ears", "thumb_on_ears",
                        (int) AnimFlagsEnums.StopOnLastFrame);
                    break;
                case (int) CivilianAnimations.Victory:
                    _player.PlayAnimation("anim@mp_player_intcelebrationmale@v_sign", "v_sign",
                        (int) AnimFlagsEnums.StopOnLastFrame);
                    break;
                case (int) CivilianAnimations.Crouch:
                    _player.PlayAnimation("misscarstealfinalecar_5_ig_3", "crouchloop",
                        (int) AnimFlagsEnums.StopOnLastFrame);
                    break;
                case (int) CivilianAnimations.Dig:
                    _player.PlayAnimation("missmic1leadinoutmic_1_mcs_2", "_leadin_trevor",
                        (int) AnimFlagsEnums.StopOnLastFrame);
                    break;
                case (int) CivilianAnimations.Cry:
                    _player.PlayAnimation("mp_bank_heist_1", "f_cower_01",
                        (int) AnimFlagsEnums.StopOnLastFrame);
                    break;
                case (int) CivilianAnimations.HurryUp:
                    _player.PlayAnimation("missfam4", "say_hurry_up_a_trevor",
                        (int) AnimFlagsEnums.StopOnLastFrame);
                    break;
                default:
                    NAPI.Util.ConsoleOutput("animation error");
                    return;
            }
        }

        private void PlayDanceAnimation()
        {
            _player.StopAnimation();

            switch (_animName)
            {
                case (int) DanceAnimations.Dj:
                    _player.PlayAnimation("anim@mp_player_intupperdj", "enter",
                        (int) AnimFlagsEnums.Loop);
                    _player.SendNotification($"~b~Setze Dance auf: ~y~{DanceAnimations.Dj}");
                    break;
                default:
                    NAPI.Util.ConsoleOutput("animation error");
                    return;
            }
        }

        private void PlayFactionAnimation()
        {
            _player.StopAnimation();

            switch (_animName)
            {
                case (int) FactionAnimations.HandCuffed:
                    _player.PlayAnimation("mp_arresting", "idle",
                        (int) (AnimFlagsEnums.Loop | AnimFlagsEnums.OnlyAnimateUpperBody |
                               AnimFlagsEnums.AllowPlayerControl));
                    break;
                case (int) FactionAnimations.MedicRevive:
                    _player.PlayAnimation("amb@medic@standing@kneel@base", "base",
                        (int) AnimFlagsEnums.Loop);
                    break;
                case (int) FactionAnimations.MedicKneel:
                    _player.PlayAnimation("amb@medic@standing@kneel@base", "base",
                        (int) AnimFlagsEnums.Loop);
                    break;
                case (int) FactionAnimations.MechanicVehicle:
                    _player.PlayAnimation("amb@world_human_vehicle_mechanic@male@idle_a", "idle_a",
                        (int) AnimFlagsEnums.Loop);
                    break;
                case (int) FactionAnimations.MechanicFixing:
                    _player.PlayAnimation("mini@repair", "fixing_a_ped",
                        (int) AnimFlagsEnums.Loop);
                    break;
                default:
                    NAPI.Util.ConsoleOutput("animation error");
                    return;
            }
        }

        private void PlayMiningAnimation()
        {
            switch (_animName)
            {
                case (int) FactionAnimations.MedicRevive:
                    _player.PlayAnimation("amb@medic@standing@kneel@base", "base", (int) AnimFlagsEnums.Loop);
                    break;
                case (int) FactionAnimations.MedicKneel:
                    _player.PlayAnimation("amb@medic@standing@kneel@base", "base",
                        (int) AnimFlagsEnums.Loop);
                    break;
                case (int) FactionAnimations.MechanicVehicle:
                    _player.PlayAnimation("amb@world_human_vehicle_mechanic@male@idle_a", "idle_a",
                        (int) AnimFlagsEnums.Loop);
                    break;
                case (int) FactionAnimations.MechanicFixing:
                    _player.PlayAnimation("mini@repair", "fixing_a_ped",
                        (int) AnimFlagsEnums.Loop);
                    break;
                default:
                    NAPI.Util.ConsoleOutput("animation error");
                    return;
            }
        }
    }
}