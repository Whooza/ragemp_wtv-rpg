﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GTANetworkAPI;
using RolePlayGameData.Configs;
using RolePlayGameData.Database.RolePlayModels;
using RolePlayGameData.EntityDataConsts;
using RolePlayGameData.Enums;
using RolePlayGameData.Models;
using RolePlayGameServer.Database.Controllers;
using RolePlayGameServer.Handlers;
using RolePlayGameServer.Util;

namespace RolePlayGameServer.Players
{
    internal class PlayerInventory
    {
        private readonly List<PlayerInvItemModel> _inventory;
        private readonly Client _player;
        private readonly PlayerAccountModel _playerData;

        public PlayerInventory(Client player)
        {
            _player = player;
            _playerData = (PlayerAccountModel) player.GetData(PlayerDataConst.PlayerAccount);
            _inventory = GetInventory().Result;
        }

        #region InvActions

        public async Task<bool> AddItem(uint itemName, uint amount)
        {
            try
            {
                var itemModel = ItemCfg.ItemConfig.Single(x => (uint) x.ItemName == itemName);
                var playerItem = _inventory.SingleOrDefault(x => x.Item == itemName);

                if (CheckInventoryWeight(_inventory, itemModel, amount))
                {
                    _player.SendNotification("~r~Dein Iventar ist voll!");
                    return false;
                }

                if (playerItem == null)
                {
                    await RolePlayDbController.SetPlayerInvItemInDb(_playerData.SocialClubName, itemName, amount)
                        .ConfigureAwait(false);

                    _player.SendNotification($"~b~Item bekommen: ~o~{amount}x {itemModel.DisplayedName}~b~.");

                    return true;
                }

                playerItem.Amount += amount;

                await RolePlayDbController.UpdatePlayerInvItem(playerItem).ConfigureAwait(false);

                _player.SendNotification($"~b~Item bekommen: ~o~{amount}x {itemModel.DisplayedName}~b~.");

                return true;
            }
            catch (Exception e)
            {
                Logging.InventoryErrorLog($"GiveInvItem - {e.Message}");
                return false;
            }
        }

        public void UseInvItem(uint itemName)
        {
            try
            {
                var currentItem = _inventory.FirstOrDefault(x => x.Item == itemName);

                if (currentItem == null)
                {
                    _player.SendNotification("~r~Item wurde nicht gefunden!");
                    return;
                }

                var itemSettings = ItemCfg.ItemConfig.Single(x => (int) x.ItemName == itemName);

                if (itemSettings.ItemType == ItemTypeEnums.CrateItem)
                    UnpackItem(currentItem, itemSettings);
                else
                    ConsumeItem(currentItem, itemSettings);
            }
            catch (Exception e)
            {
                Logging.InventoryErrorLog($"UseInvItem - {e.Message}");
            }
        }

        private async void UnpackItem(PlayerInvItemModel currentItem, ItemModel itemSettings)
        {
            try
            {
                var unpackedItems = UnpackCfg.UnpackItems.Single(x => (uint) x.Key == currentItem.Item);

                if (CheckInventoryUnpackWeight(_inventory, unpackedItems.Value))
                {
                    _player.SendNotification("~r~Nicht genug Platz im Inventar zum Entpacken!");
                    return;
                }

                currentItem.Amount -= 1;

                if (currentItem.Amount == 0)
                    await RolePlayDbController.DeletePlayerInvItem(currentItem)
                        .ConfigureAwait(false);
                else
                    await RolePlayDbController.UpdatePlayerInvItem(currentItem)
                        .ConfigureAwait(false);

                foreach (var item in unpackedItems.Value)
                    await AddItem((uint) item.Key, (uint) item.Value).ConfigureAwait(false);

                _player.SendNotification($"~b~Item entpackt: ~y~{itemSettings.DisplayedName}~b~.");
            }
            catch (Exception e)
            {
                Logging.InventoryErrorLog($"UseInvItem - {e.Message}");
            }
        }

        private async void ConsumeItem(PlayerInvItemModel currentItem, ItemModel itemSettings)
        {
            try
            {
                var itemUsed = new ItemActions(_player).ExecuteItemAction(itemSettings.ItemType, itemSettings.Value);

                if (!itemUsed) return;

                currentItem.Amount -= 1;

                if (currentItem.Amount == 0)
                    await RolePlayDbController.DeletePlayerInvItem(currentItem)
                        .ConfigureAwait(false);
                else
                    await RolePlayDbController.UpdatePlayerInvItem(currentItem)
                        .ConfigureAwait(false);

                _player.SendNotification($"~b~Item benutzt: ~y~{itemSettings.DisplayedName}~b~.");
            }
            catch (Exception e)
            {
                Logging.InventoryErrorLog($"UseInvItem - {e.Message}");
            }
        }

        public async Task<bool> DestroyInvItem(uint itemName, uint amount = 1000)
        {
            try
            {
                var currentItem = _inventory.Single(x => x.Item == itemName);

                if (currentItem == null)
                {
                    _player.SendNotification("~r~Item wurde nicht gefunden!");
                    return false;
                }

                var itemSettings = ItemCfg.ItemConfig.Single(x => (int) x.ItemName == itemName);

                if (amount >= currentItem.Amount)
                {
                    await RolePlayDbController.DeletePlayerInvItem(currentItem)
                        .ConfigureAwait(false);

                    _player.SendNotification(
                        $"~b~Item entfernt: ~y~{currentItem.Amount}x {itemSettings.DisplayedName}~b~.");

                    return true;
                }

                currentItem.Amount -= amount;

                await RolePlayDbController.UpdatePlayerInvItem(currentItem)
                    .ConfigureAwait(false);

                _player.SendNotification(
                    $"~b~Item entfernt: ~y~{amount}x {itemSettings.DisplayedName}~b~.");

                return true;
            }
            catch (Exception e)
            {
                Logging.InventoryErrorLog($"DestroyInvItem - {e.Message}");
                return false;
            }
        }

        public async Task GiveInvItem(Client target, uint itemName, uint amount)
        {
            try
            {
                var playerItem = _inventory.Single(x => x.Item == itemName);
                var itemModel = ItemCfg.ItemConfig.Single(x => (uint) x.ItemName == itemName);

                if (playerItem == null)
                {
                    _player.SendNotification("~r~Item wurde nicht gefunden!");
                    return;
                }

                var targetData = (PlayerAccountModel) target.GetData(PlayerDataConst.PlayerAccount);

                var targetInventory = await RolePlayDbController.GetPlayerInvList(targetData.SocialClubName)
                    .ConfigureAwait(false);

                if (CheckInventoryWeight(targetInventory, itemModel, amount))
                {
                    _player.SendNotification("~r~Das Iventar ist voll, Du kannst das Item nicht verschieben!");
                    target.SendNotification("~r~Dein Inventar ist voll!");
                    return;
                }

                var targetItem = targetInventory.Single(x => x.Item == itemName);
                var itemSettings = ItemCfg.ItemConfig.Single(x => (int) x.ItemName == itemName);

                playerItem.Amount -= amount;

                if (playerItem.Amount > 0)
                    await RolePlayDbController.UpdatePlayerInvItem(playerItem)
                        .ConfigureAwait(false);
                else
                    await RolePlayDbController.DeletePlayerInvItem(playerItem)
                        .ConfigureAwait(false);

                if (targetItem == null)
                {
                    var newInvItem = new PlayerInvItemModel
                    {
                        SocialClub = targetData.SocialClubName,
                        Item = playerItem.Item,
                        Amount = amount
                    };

                    await RolePlayDbController.SetPlayerInvItemInDb(targetData.SocialClubName, newInvItem.Item, amount)
                        .ConfigureAwait(false);
                }
                else
                {
                    targetItem.Amount += amount;

                    await RolePlayDbController.UpdatePlayerInvItem(targetItem)
                        .ConfigureAwait(false);
                }

                _player.SendNotification($"~b~Du hast ~y~{itemSettings.DisplayedName} ~b~verschoben.");
                target.SendNotification($"~b~Du hast ~y~{itemSettings.DisplayedName} ~b~erhalten.");
            }
            catch (Exception e)
            {
                Logging.InventoryErrorLog($"GiveInvItem - {e.Message}");
            }
        }

        #endregion

        #region Helpers

        private bool CheckInventoryWeight(List<PlayerInvItemModel> inventory, ItemModel itemModel, uint amount)
        {
            var weight = 0f;

            weight += itemModel.Weight * amount;

            foreach (var item in inventory)
            {
                var settings = ItemCfg.ItemConfig.Single(x => (uint) x.ItemName == item.Item);

                weight += settings.Weight * item.Amount;
            }

            if (weight >= 100f) return true;

            return false;
        }

        private bool CheckInventoryUnpackWeight(List<PlayerInvItemModel> inventory,
            Dictionary<ItemNameEnums, int> unpackItems)
        {
            var weight = 0f;

            foreach (var item in inventory)
            {
                var settings = ItemCfg.ItemConfig.Single(x => (uint) x.ItemName == item.Item);

                weight += settings.Weight * item.Amount;
            }

            foreach (var item in unpackItems)
            {
                var settings = ItemCfg.ItemConfig.Single(x => x.ItemName == item.Key);

                weight += settings.Weight * item.Value;
            }

            if (weight >= 100f) return true;

            return false;
        }

        private async Task<List<PlayerInvItemModel>> GetInventory()
        {
            return await RolePlayDbController.GetPlayerInvList(_playerData.SocialClubName)
                .ConfigureAwait(false);
        }

        #endregion
    }
}