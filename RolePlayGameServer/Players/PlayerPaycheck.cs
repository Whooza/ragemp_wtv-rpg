﻿using System;
using System.Threading.Tasks;
using GTANetworkAPI;
using Microsoft.EntityFrameworkCore.Internal;
using RolePlayGameData.Configs;
using RolePlayGameData.Database.RolePlayModels;
using RolePlayGameData.EntityDataConsts;
using RolePlayGameServer.Database.Controllers;
using RolePlayGameServer.Util;

namespace RolePlayGameServer.Players
{
    internal class PlayerPaycheck
    {
        public async Task StartPlayerPayAsync()
        {
#if DEBUG
            Logging.ServerInfoLog($"starting {this}");
#endif
            try
            {
                var allPlayers = NAPI.Pools.GetAllPlayers();

                if (!allPlayers.Any())
                {
#if DEBUG
                    Logging.PlayerPaycheckInfoLog("StartPlayerPayAsync - no player found");
#endif
                    return;
                }

                foreach (var item in allPlayers)
                    if (item.HasData(PlayerDataConst.IsLoggedInFlag))
                        await Task.Run(() => PayCheckActionAsync(item)).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"StartPlayerPayAsync - {e.Message}");
            }
        }

        private void PayCheckActionAsync(Client player)
        {
            try
            {
                var account = (PlayerAccountModel) player.GetData(PlayerDataConst.PlayerAccount);
                var money = (PlayerMoneyModel) player.GetData(PlayerDataConst.PlayerMoney);
                var paycheck = (PlayerPaycheckModel) player.GetData(PlayerDataConst.PlayerPaycheck);

                if (account.LscLevel > 0 && paycheck.InLscActive)
                {
                    paycheck.LscMinutes += 1;

                    if (paycheck.LastPayment.AddHours(1) <= DateTime.Now && paycheck.LscMinutes > 0)
                    {
                        var rankBonus = PlayerCfg.LscSalary / 10 * account.LscLevel;
                        var payment = (money.BackUpMoney + PlayerCfg.LscSalary + rankBonus) / 60 * paycheck.LscMinutes;
                        money.BackUpMoney += payment;
                        paycheck.LscMinutes = 0;
                        paycheck.LastPayment = DateTime.Now;
                        SendFactionPaymentMsgToPlayer(player, paycheck, money, "LSC", payment);
                    }

                    SaveAll(player, paycheck, money);
                    return;
                }

                if (account.LsrsLevel > 0 && paycheck.InLsrsActive)
                {
                    paycheck.LsrsMinutes += 1;

                    if (paycheck.LastPayment.AddHours(1) <= DateTime.Now && paycheck.LsrsMinutes > 0)
                    {
                        var rankBonus = PlayerCfg.LsrsSalary / 10 * account.LsrsLevel;
                        var payment = (money.BackUpMoney + PlayerCfg.LsrsSalary + rankBonus) / 60 *
                                      paycheck.LsrsMinutes;
                        money.BackUpMoney += payment;
                        paycheck.LsrsMinutes = 0;
                        paycheck.LastPayment = DateTime.Now;
                        SendFactionPaymentMsgToPlayer(player, paycheck, money, "LSRS", payment);
                    }

                    SaveAll(player, paycheck, money);
                    return;
                }

                if (account.LspdLevel > 0 && paycheck.InLspdActive)
                {
                    paycheck.LspdMinutes += 1;

                    if (paycheck.LastPayment.AddHours(1) <= DateTime.Now && paycheck.LspdMinutes > 0)
                    {
                        var rankBonus = PlayerCfg.LspdSalary / 10 * account.LspdLevel;
                        var payment = (money.BackUpMoney + PlayerCfg.LspdSalary + rankBonus) / 60 *
                                      paycheck.LspdMinutes;
                        money.BackUpMoney += payment;
                        paycheck.LspdMinutes = 0;
                        paycheck.LastPayment = DateTime.Now;
                        SendFactionPaymentMsgToPlayer(player, paycheck, money, "LSPD", payment);
                    }

                    SaveAll(player, paycheck, money);
                    return;
                }

                if (account.FibLevel > 0 && paycheck.InFibActive)
                {
                    paycheck.FibMinutes += 1;

                    if (paycheck.LastPayment.AddHours(1) <= DateTime.Now && paycheck.FibMinutes > 0)
                    {
                        var rankBonus = PlayerCfg.FibSalary / 10 * account.FibLevel;
                        var payment = (money.BackUpMoney + PlayerCfg.FibSalary + rankBonus) / 60 * paycheck.FibMinutes;
                        money.BackUpMoney += payment;
                        paycheck.FibMinutes = 0;
                        paycheck.LastPayment = DateTime.Now;
                        SendFactionPaymentMsgToPlayer(player, paycheck, money, "FIB", payment);
                    }

                    SaveAll(player, paycheck, money);
                    return;
                }

                if (account.LssdLevel > 0 && paycheck.InLssdActive)
                {
                    paycheck.LssdMinutes += 1;

                    if (paycheck.LastPayment.AddHours(1) <= DateTime.Now && paycheck.LssdMinutes > 0)
                    {
                        var rankBonus = PlayerCfg.LssdSalary / 10 * account.LssdLevel;
                        var payment = (money.BackUpMoney + PlayerCfg.LssdSalary + rankBonus) / 60 *
                                      paycheck.LssdMinutes;
                        money.BackUpMoney += payment;
                        paycheck.LssdMinutes = 0;
                        paycheck.LastPayment = DateTime.Now;
                        SendFactionPaymentMsgToPlayer(player, paycheck, money, "LSSD", payment);
                    }

                    SaveAll(player, paycheck, money);
                    return;
                }

                if (account.DojLevel > 0 && paycheck.InDojActive)
                {
                    paycheck.DojMinutes += 1;

                    if (paycheck.LastPayment.AddHours(1) <= DateTime.Now && paycheck.DojMinutes > 0)
                    {
                        var rankBonus = PlayerCfg.DojSalary / 10 * account.DojLevel;
                        var payment = (money.BackUpMoney + PlayerCfg.DojSalary + rankBonus) / 60 * paycheck.DojMinutes;
                        money.BackUpMoney += payment;
                        paycheck.DojMinutes = 0;
                        paycheck.LastPayment = DateTime.Now;
                        SendFactionPaymentMsgToPlayer(player, paycheck, money, "DOJ", payment);
                    }

                    SaveAll(player, paycheck, money);
                    return;
                }

                if (account.DcrLevel > 0 && paycheck.InDcrActive)
                {
                    paycheck.DcrMinutes += 1;

                    if (paycheck.LastPayment.AddHours(1) <= DateTime.Now && paycheck.DcrMinutes > 0)
                    {
                        var rankBonus = PlayerCfg.DcrSalary / 10 * account.DcrLevel;
                        var payment = (money.BackUpMoney + PlayerCfg.DcrSalary + rankBonus) / 60 * paycheck.DcrMinutes;
                        money.BackUpMoney += payment;
                        paycheck.DcrMinutes = 0;
                        paycheck.LastPayment = DateTime.Now;
                        SendFactionPaymentMsgToPlayer(player, paycheck, money, "DCR", payment);
                    }

                    SaveAll(player, paycheck, money);
                    return;
                }

                if (paycheck.InCivActive && paycheck.SocialPayIsActive)
                {
                    paycheck.SocialPayMinutes += 1;

                    if (paycheck.LastPayment.AddHours(1) <= DateTime.Now && paycheck.SocialPayMinutes > 0)
                    {
                        var payment = PlayerCfg.SocialSalary / 60 * paycheck.SocialPayMinutes;
                        money.BackUpMoney += payment;
                        paycheck.SocialPayMinutes = 0;
                        paycheck.LastPayment = DateTime.Now;
                        SendSocialPaymentMsgToPlayer(player, paycheck, payment);
                    }

                    SaveAll(player, paycheck, money);
                    return;
                }

                if (paycheck.InCivActive && !paycheck.SocialPayIsActive)
                {
                    if (paycheck.LastPayment.AddHours(1) <= DateTime.Now)
                    {
                        paycheck.LastPayment = DateTime.Now;
                        SendSocialPaymentInfoToPlayer(player);
                    }

                    SaveAll(player, paycheck, money);
                }
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"PayCheckActionAsync - {e.Message}");
            }
        }

        private static void SaveAll(Client player, PlayerPaycheckModel paycheck, PlayerMoneyModel money)
        {
            try
            {
                player.SetData(PlayerDataConst.PlayerPaycheck, paycheck);
                player.SetData(PlayerDataConst.PlayerMoney, money);

                RolePlayDbController.UpdatePlayerPaycheck(paycheck).ConfigureAwait(false);
                RolePlayDbController.UpdatePlayerMoney(money).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"SaveAll - {e.Message}");
            }
        }

        private static void SendFactionPaymentMsgToPlayer(Client player, PlayerPaycheckModel paycheck,
            PlayerMoneyModel money, string faction, float payment)
        {
            UiHelper.SendLeftMessageToPlayer(player, "Gehaltszahlung",
                $"Du hast Dein Gehalt für Tätigkeiten beim {faction} in Höhe von <font color=green>{payment} Dollar</font> erhalten." +
                $" Dein neuer Kontostand: <font color=green>{money.BackUpMoney} Dollar</font>." +
                $" Die nächste Auszahlung ist: <font color=yellow>{paycheck.LastPayment.AddHours(1):hh:mm} Dollar</font>");
        }

        private static void SendSocialPaymentMsgToPlayer(Client player, PlayerPaycheckModel paycheck, float payment)
        {
            UiHelper.SendLeftMessageToPlayer(player, "Soziale Leistungen",
                $"~b~Du hast soziale Leistungen in Höhe von: ~g~{payment} Dollar ~b~erhalten." +
                $" ~b~Die nächste Auszahlung ist: ~y~{paycheck.LastPayment.AddHours(1):hh:mm}");
        }

        private static void SendSocialPaymentInfoToPlayer(Client player)
        {
            UiHelper.SendLeftMessageToPlayer(player, "Information",
                "Es ist möglich soziale Leistungen unter bestimmten Voraussetzungen zu beantragen." +
                " Informiere Dich im Jobcenter für Weitere Informationen.");
        }
    }
}