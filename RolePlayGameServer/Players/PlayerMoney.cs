﻿using GTANetworkAPI;
using RolePlayGameData.Database.RolePlayModels;
using RolePlayGameData.EntityDataConsts;
using RolePlayGameData.Texts;
using RolePlayGameServer.Database.Controllers;
using RolePlayGameServer.Util;
using System;
using System.Threading.Tasks;

namespace RolePlayGameServer.Players
{
    internal class PlayerMoney
    {
        private readonly PlayerMoneyModel _money;
        private readonly Client _player;

        public PlayerMoney(Client player)
        {
            _player = player;
            _money = (PlayerMoneyModel) player.GetData(PlayerDataConst.PlayerMoney);
        }

        public async Task<bool> AddCashMoney(float amount)
        {
            try
            {
                _money.CashMoney += amount;

                _player.SetData(PlayerDataConst.PlayerMoney, _money);

                _player.SendNotification($"~b~Du hast ~g~{amount.ToString("0.00")}$ ~b~Bargeld erhalten.");

                await RolePlayDbController.UpdatePlayerMoney(_money)
                    .ConfigureAwait(false);

                return true;
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"AddCashMoney - {e.Message}");
                return false;
            }
        }

        public async Task<bool> RemoveCashMoney(float amount)
        {
            try
            {
                _money.CashMoney -= amount;

                if (_money.CashMoney < 0f)
                {
                    _player.SendNotification(PlayerNotificationTexts.NotEnoughCashMoney);
                    return false;
                }

                _player.SetData(PlayerDataConst.PlayerMoney, _money);

                UiHelper.UpdatePlayerCash(_player, _money.CashMoney);

                _player.SendNotification($"~b~Du hast nun ~r~{amount.ToString("0.00")}$ ~b~weniger Bargeld bei Dir.");

                await RolePlayDbController.UpdatePlayerMoney(_money).ConfigureAwait(false);

                return true;
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"RemoveCashMoney - {e.Message}");
                return false;
            }
        }
    }
}