﻿using GTANetworkAPI;
using RolePlayGameData.Configs.Vehicle;
using RolePlayGameData.EntityDataConsts;
using RolePlayGameData.Enums;
using RolePlayGameData.Models;

namespace RolePlayGameServer.Garages
{
    public class GarageAreaSpawner
    {
        private readonly uint _playerId;

        private readonly MarkDataModel _exitSmall_1 = new MarkDataModel
        {
            MarkId = 0,
            MarkFaction = (uint)FactionEnums.Civ,
            MarkType = (uint)MarkTypeEnums.GarageExitSmall,
            MarkName = 1,
            PosX = 0,
            PosY = 0,
            PosZ = 0
        };

        private readonly MarkDataModel _exitSmall_2 = new MarkDataModel
        {
            MarkId = 0,
            MarkFaction = (uint)FactionEnums.Civ,
            MarkType = (uint)MarkTypeEnums.GarageExitSmall,
            MarkName = 2,
            PosX = 0,
            PosY = 0,
            PosZ = 0
        };

        private readonly MarkDataModel _exitSmall_3 = new MarkDataModel
        {
            MarkId = 0,
            MarkFaction = (uint)FactionEnums.Civ,
            MarkType = (uint)MarkTypeEnums.GarageExitSmall,
            MarkName = 3,
            PosX = 0,
            PosY = 0,
            PosZ = 0
        };

        private readonly MarkDataModel _exitSmall_4 = new MarkDataModel
        {
            MarkId = 0,
            MarkFaction = (uint)FactionEnums.Civ,
            MarkType = (uint)MarkTypeEnums.GarageExitSmall,
            MarkName = 4,
            PosX = 0,
            PosY = 0,
            PosZ = 0
        };

        private readonly MarkDataModel _exitSmall_5 = new MarkDataModel
        {
            MarkId = 0,
            MarkFaction = (uint)FactionEnums.Civ,
            MarkType = (uint)MarkTypeEnums.GarageExitSmall,
            MarkName = 5,
            PosX = 0,
            PosY = 0,
            PosZ = 0
        };

        private MarkDataModel _exitSmall_6 = new MarkDataModel
        {
            MarkId = 0,
            MarkFaction = (uint)FactionEnums.Civ,
            MarkType = (uint)MarkTypeEnums.GarageExitSmall,
            MarkName = 6,
            PosX = 0,
            PosY = 0,
            PosZ = 0
        };

        private readonly MarkDataModel _exitSmall_7 = new MarkDataModel
        {
            MarkId = 0,
            MarkFaction = (uint)FactionEnums.Civ,
            MarkType = (uint)MarkTypeEnums.GarageExitSmall,
            MarkName = 7,
            PosX = 0,
            PosY = 0,
            PosZ = 0
        };

        private readonly MarkDataModel _exitSmall_8 = new MarkDataModel
        {
            MarkId = 0,
            MarkFaction = (uint)FactionEnums.Civ,
            MarkType = (uint)MarkTypeEnums.GarageExitSmall,
            MarkName = 8,
            PosX = 0,
            PosY = 0,
            PosZ = 0
        };

        private readonly MarkDataModel _exitSmall_9 = new MarkDataModel
        {
            MarkId = 0,
            MarkFaction = (uint)FactionEnums.Civ,
            MarkType = (uint)MarkTypeEnums.GarageExitSmall,
            MarkName = 9,
            PosX = 0,
            PosY = 0,
            PosZ = 0
        };

        private readonly MarkDataModel _exitSmall_10 = new MarkDataModel
        {
            MarkId = 0,
            MarkFaction = (uint)FactionEnums.Civ,
            MarkType = (uint)MarkTypeEnums.GarageExitSmall,
            MarkName = 10,
            PosX = 0,
            PosY = 0,
            PosZ = 0
        };

        private readonly MarkDataModel _exitSmall_11 = new MarkDataModel
        {
            MarkId = 0,
            MarkFaction = (uint)FactionEnums.Civ,
            MarkType = (uint)MarkTypeEnums.GarageExitSmall,
            MarkName = 11,
            PosX = 0,
            PosY = 0,
            PosZ = 0
        };

        private readonly MarkDataModel _exitSmall_12 = new MarkDataModel
        {
            MarkId = 0,
            MarkFaction = (uint)FactionEnums.Civ,
            MarkType = (uint)MarkTypeEnums.GarageExitSmall,
            MarkName = 12,
            PosX = 0,
            PosY = 0,
            PosZ = 0
        };

        private readonly MarkDataModel _exitMedium_1 = new MarkDataModel
        {
            MarkId = 0,
            MarkFaction = (uint)FactionEnums.Civ,
            MarkType = (uint)MarkTypeEnums.GarageExitMedium,
            MarkName = 1,
            PosX = 0,
            PosY = 0,
            PosZ = 0
        };

        private readonly MarkDataModel _exitMedium_2 = new MarkDataModel
        {
            MarkId = 0,
            MarkFaction = (uint)FactionEnums.Civ,
            MarkType = (uint)MarkTypeEnums.GarageExitMedium,
            MarkName = 2,
            PosX = 0,
            PosY = 0,
            PosZ = 0
        };

        private readonly MarkDataModel _exitMedium_3 = new MarkDataModel
        {
            MarkId = 0,
            MarkFaction = (uint)FactionEnums.Civ,
            MarkType = (uint)MarkTypeEnums.GarageExitMedium,
            MarkName = 3,
            PosX = 0,
            PosY = 0,
            PosZ = 0
        };

        private readonly MarkDataModel _exitMedium_4 = new MarkDataModel
        {
            MarkId = 0,
            MarkFaction = (uint)FactionEnums.Civ,
            MarkType = (uint)MarkTypeEnums.GarageExitMedium,
            MarkName = 4,
            PosX = 0,
            PosY = 0,
            PosZ = 0
        };

        private readonly MarkDataModel _exitMedium_5 = new MarkDataModel
        {
            MarkId = 0,
            MarkFaction = (uint)FactionEnums.Civ,
            MarkType = (uint)MarkTypeEnums.GarageExitMedium,
            MarkName = 5,
            PosX = 0,
            PosY = 0,
            PosZ = 0
        };

        private readonly MarkDataModel _exitMedium_6 = new MarkDataModel
        {
            MarkId = 0,
            MarkFaction = (uint)FactionEnums.Civ,
            MarkType = (uint)MarkTypeEnums.GarageExitMedium,
            MarkName = 6,
            PosX = 0,
            PosY = 0,
            PosZ = 0
        };

        private readonly MarkDataModel _exitBig_1 = new MarkDataModel
        {
            MarkId = 0,
            MarkFaction = (uint)FactionEnums.Civ,
            MarkType = (uint)MarkTypeEnums.GarageExitBig,
            MarkName = 1,
            PosX = 0,
            PosY = 0,
            PosZ = 0
        };

        private readonly MarkDataModel _exitBig_2 = new MarkDataModel
        {
            MarkId = 0,
            MarkFaction = (uint)FactionEnums.Civ,
            MarkType = (uint)MarkTypeEnums.GarageExitBig,
            MarkName = 2,
            PosX = 0,
            PosY = 0,
            PosZ = 0
        };

        private readonly MarkDataModel _areaSmall = new MarkDataModel
        {
            MarkId = 0,
            MarkFaction = (uint)FactionEnums.Civ,
            MarkType = (uint)MarkTypeEnums.GarageAreaSmall,
            MarkName = 0,
            PosX = 0,
            PosY = 0,
            PosZ = 0
        };

        private readonly MarkDataModel _areaMedium = new MarkDataModel
        {
            MarkId = 0,
            MarkFaction = (uint)FactionEnums.Civ,
            MarkType = (uint)MarkTypeEnums.GarageAreaMedium,
            MarkName = 0,
            PosX = 0,
            PosY = 0,
            PosZ = 0
        };

        private readonly MarkDataModel _areaBig = new MarkDataModel
        {
            MarkId = 0,
            MarkFaction = (uint)FactionEnums.Civ,
            MarkType = (uint)MarkTypeEnums.GarageAreaBig,
            MarkName = 0,
            PosX = 0,
            PosY = 0,
            PosZ = 0
        };

        public GarageAreaSpawner(uint playerId) => _playerId = playerId;

        public void SpawnGarageLocations()
        {
            NAPI.Task.Run(() =>
                {
                    var markSmall_d1 = NAPI.Marker.CreateMarker(1, new Vector3(VehGarageCfg.SpawnPosSmall.X, VehGarageCfg.SpawnPosSmall.Y, VehGarageCfg.SpawnPosSmall.Z - 1f), new Vector3(0, 0, 0), new Vector3(0, 0, 0), 1.1f, 155, 155, 155, false, VehGarageCfg.DimS1 + _playerId);
                    var markSmall_d2 = NAPI.Marker.CreateMarker(1, new Vector3(VehGarageCfg.SpawnPosSmall.X, VehGarageCfg.SpawnPosSmall.Y, VehGarageCfg.SpawnPosSmall.Z - 1f), new Vector3(0, 0, 0), new Vector3(0, 0, 0), 1.1f, 155, 155, 155, false, VehGarageCfg.DimS2 + _playerId);
                    var markSmall_d3 = NAPI.Marker.CreateMarker(1, new Vector3(VehGarageCfg.SpawnPosSmall.X, VehGarageCfg.SpawnPosSmall.Y, VehGarageCfg.SpawnPosSmall.Z - 1f), new Vector3(0, 0, 0), new Vector3(0, 0, 0), 1.1f, 155, 155, 155, false, VehGarageCfg.DimS3 + _playerId);
                    var markSmall_d4 = NAPI.Marker.CreateMarker(1, new Vector3(VehGarageCfg.SpawnPosSmall.X, VehGarageCfg.SpawnPosSmall.Y, VehGarageCfg.SpawnPosSmall.Z - 1f), new Vector3(0, 0, 0), new Vector3(0, 0, 0), 1.1f, 155, 155, 155, false, VehGarageCfg.DimS4 + _playerId);
                    var markSmall_d5 = NAPI.Marker.CreateMarker(1, new Vector3(VehGarageCfg.SpawnPosSmall.X, VehGarageCfg.SpawnPosSmall.Y, VehGarageCfg.SpawnPosSmall.Z - 1f), new Vector3(0, 0, 0), new Vector3(0, 0, 0), 1.1f, 155, 155, 155, false, VehGarageCfg.DimS5 + _playerId);
                    var markSmall_d6 = NAPI.Marker.CreateMarker(1, new Vector3(VehGarageCfg.SpawnPosSmall.X, VehGarageCfg.SpawnPosSmall.Y, VehGarageCfg.SpawnPosSmall.Z - 1f), new Vector3(0, 0, 0), new Vector3(0, 0, 0), 1.1f, 155, 155, 155, false, VehGarageCfg.DimS6 + _playerId);
                    var markSmall_d7 = NAPI.Marker.CreateMarker(1, new Vector3(VehGarageCfg.SpawnPosSmall.X, VehGarageCfg.SpawnPosSmall.Y, VehGarageCfg.SpawnPosSmall.Z - 1f), new Vector3(0, 0, 0), new Vector3(0, 0, 0), 1.1f, 155, 155, 155, false, VehGarageCfg.DimS7 + _playerId);
                    var markSmall_d8 = NAPI.Marker.CreateMarker(1, new Vector3(VehGarageCfg.SpawnPosSmall.X, VehGarageCfg.SpawnPosSmall.Y, VehGarageCfg.SpawnPosSmall.Z - 1f), new Vector3(0, 0, 0), new Vector3(0, 0, 0), 1.1f, 155, 155, 155, false, VehGarageCfg.DimS8 + _playerId);
                    var markSmall_d9 = NAPI.Marker.CreateMarker(1, new Vector3(VehGarageCfg.SpawnPosSmall.X, VehGarageCfg.SpawnPosSmall.Y, VehGarageCfg.SpawnPosSmall.Z - 1f), new Vector3(0, 0, 0), new Vector3(0, 0, 0), 1.1f, 155, 155, 155, false, VehGarageCfg.DimS9 + _playerId);
                    var markSmall_d10 = NAPI.Marker.CreateMarker(1, new Vector3(VehGarageCfg.SpawnPosSmall.X, VehGarageCfg.SpawnPosSmall.Y, VehGarageCfg.SpawnPosSmall.Z - 1f), new Vector3(0, 0, 0), new Vector3(0, 0, 0), 1.1f, 155, 155, 155, false, VehGarageCfg.DimS10 + _playerId);
                    var markSmall_d11 = NAPI.Marker.CreateMarker(1, new Vector3(VehGarageCfg.SpawnPosSmall.X, VehGarageCfg.SpawnPosSmall.Y, VehGarageCfg.SpawnPosSmall.Z - 1f), new Vector3(0, 0, 0), new Vector3(0, 0, 0), 1.1f, 155, 155, 155, false, VehGarageCfg.DimS11 + _playerId);
                    var markSmall_d12 = NAPI.Marker.CreateMarker(1, new Vector3(VehGarageCfg.SpawnPosSmall.X, VehGarageCfg.SpawnPosSmall.Y, VehGarageCfg.SpawnPosSmall.Z - 1f), new Vector3(0, 0, 0), new Vector3(0, 0, 0), 1.1f, 155, 155, 155, false, VehGarageCfg.DimS12 + _playerId);
                    var colSmall_d1 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.SpawnPosSmall.X, VehGarageCfg.SpawnPosSmall.Y, VehGarageCfg.SpawnPosSmall.Z - 5f), 1.5f, 15f, VehGarageCfg.DimS1 + _playerId);
                    var colSmall_d2 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.SpawnPosSmall.X, VehGarageCfg.SpawnPosSmall.Y, VehGarageCfg.SpawnPosSmall.Z - 5f), 1.5f, 15f, VehGarageCfg.DimS2 + _playerId);
                    var colSmall_d3 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.SpawnPosSmall.X, VehGarageCfg.SpawnPosSmall.Y, VehGarageCfg.SpawnPosSmall.Z - 5f), 1.5f, 15f, VehGarageCfg.DimS3 + _playerId);
                    var colSmall_d4 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.SpawnPosSmall.X, VehGarageCfg.SpawnPosSmall.Y, VehGarageCfg.SpawnPosSmall.Z - 5f), 1.5f, 15f, VehGarageCfg.DimS4 + _playerId);
                    var colSmall_d5 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.SpawnPosSmall.X, VehGarageCfg.SpawnPosSmall.Y, VehGarageCfg.SpawnPosSmall.Z - 5f), 1.5f, 15f, VehGarageCfg.DimS5 + _playerId);
                    var colSmall_d6 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.SpawnPosSmall.X, VehGarageCfg.SpawnPosSmall.Y, VehGarageCfg.SpawnPosSmall.Z - 5f), 1.5f, 15f, VehGarageCfg.DimS6 + _playerId);
                    var colSmall_d7 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.SpawnPosSmall.X, VehGarageCfg.SpawnPosSmall.Y, VehGarageCfg.SpawnPosSmall.Z - 5f), 1.5f, 15f, VehGarageCfg.DimS7 + _playerId);
                    var colSmall_d8 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.SpawnPosSmall.X, VehGarageCfg.SpawnPosSmall.Y, VehGarageCfg.SpawnPosSmall.Z - 5f), 1.5f, 15f, VehGarageCfg.DimS8 + _playerId);
                    var colSmall_d9 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.SpawnPosSmall.X, VehGarageCfg.SpawnPosSmall.Y, VehGarageCfg.SpawnPosSmall.Z - 5f), 1.5f, 15f, VehGarageCfg.DimS9 + _playerId);
                    var colSmall_d10 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.SpawnPosSmall.X, VehGarageCfg.SpawnPosSmall.Y, VehGarageCfg.SpawnPosSmall.Z - 5f), 1.5f, 15f, VehGarageCfg.DimS10 + _playerId);
                    var colSmall_d11 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.SpawnPosSmall.X, VehGarageCfg.SpawnPosSmall.Y, VehGarageCfg.SpawnPosSmall.Z - 5f), 1.5f, 15f, VehGarageCfg.DimS11 + _playerId);
                    var colSmall_d12 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.SpawnPosSmall.X, VehGarageCfg.SpawnPosSmall.Y, VehGarageCfg.SpawnPosSmall.Z - 5f), 1.5f, 15f, VehGarageCfg.DimS12 + _playerId);
                    markSmall_d1.SetData(MarkDataConst.MarkData, _exitSmall_1);
                    colSmall_d1.SetData(MarkDataConst.MarkData, _exitSmall_1);
                    markSmall_d2.SetData(MarkDataConst.MarkData, _exitSmall_2);
                    colSmall_d2.SetData(MarkDataConst.MarkData, _exitSmall_2);
                    markSmall_d3.SetData(MarkDataConst.MarkData, _exitSmall_3);
                    colSmall_d3.SetData(MarkDataConst.MarkData, _exitSmall_3);
                    markSmall_d4.SetData(MarkDataConst.MarkData, _exitSmall_4);
                    colSmall_d4.SetData(MarkDataConst.MarkData, _exitSmall_4);
                    markSmall_d5.SetData(MarkDataConst.MarkData, _exitSmall_5);
                    colSmall_d5.SetData(MarkDataConst.MarkData, _exitSmall_5);
                    markSmall_d6.SetData(MarkDataConst.MarkData, _exitSmall_6);
                    colSmall_d6.SetData(MarkDataConst.MarkData, _exitSmall_6);
                    markSmall_d7.SetData(MarkDataConst.MarkData, _exitSmall_7);
                    colSmall_d7.SetData(MarkDataConst.MarkData, _exitSmall_7);
                    markSmall_d8.SetData(MarkDataConst.MarkData, _exitSmall_8);
                    colSmall_d8.SetData(MarkDataConst.MarkData, _exitSmall_8);
                    markSmall_d9.SetData(MarkDataConst.MarkData, _exitSmall_9);
                    colSmall_d9.SetData(MarkDataConst.MarkData, _exitSmall_9);
                    markSmall_d10.SetData(MarkDataConst.MarkData, _exitSmall_10);
                    colSmall_d10.SetData(MarkDataConst.MarkData, _exitSmall_10);
                    markSmall_d11.SetData(MarkDataConst.MarkData, _exitSmall_11);
                    colSmall_d11.SetData(MarkDataConst.MarkData, _exitSmall_11);
                    markSmall_d12.SetData(MarkDataConst.MarkData, _exitSmall_12);
                    colSmall_d12.SetData(MarkDataConst.MarkData, _exitSmall_12);
                });

            NAPI.Task.Run(() =>
                {
                    var markMedium_d1 = NAPI.Marker.CreateMarker(1, new Vector3(VehGarageCfg.SpawnPosMed.X, VehGarageCfg.SpawnPosMed.Y, VehGarageCfg.SpawnPosMed.Z - 1f), new Vector3(0, 0, 0), new Vector3(0, 0, 0), 1.1f, 155, 155, 155, false, VehGarageCfg.DimMed1 + _playerId);
                    var markMedium_d2 = NAPI.Marker.CreateMarker(1, new Vector3(VehGarageCfg.SpawnPosMed.X, VehGarageCfg.SpawnPosMed.Y, VehGarageCfg.SpawnPosMed.Z - 1f), new Vector3(0, 0, 0), new Vector3(0, 0, 0), 1.1f, 155, 155, 155, false, VehGarageCfg.DimMed2 + _playerId);
                    var markMedium_d3 = NAPI.Marker.CreateMarker(1, new Vector3(VehGarageCfg.SpawnPosMed.X, VehGarageCfg.SpawnPosMed.Y, VehGarageCfg.SpawnPosMed.Z - 1f), new Vector3(0, 0, 0), new Vector3(0, 0, 0), 1.1f, 155, 155, 155, false, VehGarageCfg.DimMed3 + _playerId);
                    var markMedium_d4 = NAPI.Marker.CreateMarker(1, new Vector3(VehGarageCfg.SpawnPosMed.X, VehGarageCfg.SpawnPosMed.Y, VehGarageCfg.SpawnPosMed.Z - 1f), new Vector3(0, 0, 0), new Vector3(0, 0, 0), 1.1f, 155, 155, 155, false, VehGarageCfg.DimMed4 + _playerId);
                    var markMedium_d5 = NAPI.Marker.CreateMarker(1, new Vector3(VehGarageCfg.SpawnPosMed.X, VehGarageCfg.SpawnPosMed.Y, VehGarageCfg.SpawnPosMed.Z - 1f), new Vector3(0, 0, 0), new Vector3(0, 0, 0), 1.1f, 155, 155, 155, false, VehGarageCfg.DimMed5 + _playerId);
                    var markMedium_d6 = NAPI.Marker.CreateMarker(1, new Vector3(VehGarageCfg.SpawnPosMed.X, VehGarageCfg.SpawnPosMed.Y, VehGarageCfg.SpawnPosMed.Z - 1f), new Vector3(0, 0, 0), new Vector3(0, 0, 0), 1.1f, 155, 155, 155, false, VehGarageCfg.DimMed6 + _playerId);
                    var colMedium_d1 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.SpawnPosMed.X, VehGarageCfg.SpawnPosMed.Y, VehGarageCfg.SpawnPosMed.Z - 5f), 1.5f, 15f, VehGarageCfg.DimMed1 + _playerId);
                    var colMedium_d2 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.SpawnPosMed.X, VehGarageCfg.SpawnPosMed.Y, VehGarageCfg.SpawnPosMed.Z - 5f), 1.5f, 15f, VehGarageCfg.DimMed2 + _playerId);
                    var colMedium_d3 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.SpawnPosMed.X, VehGarageCfg.SpawnPosMed.Y, VehGarageCfg.SpawnPosMed.Z - 5f), 1.5f, 15f, VehGarageCfg.DimMed3 + _playerId);
                    var colMedium_d4 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.SpawnPosMed.X, VehGarageCfg.SpawnPosMed.Y, VehGarageCfg.SpawnPosMed.Z - 5f), 1.5f, 15f, VehGarageCfg.DimMed4 + _playerId);
                    var colMedium_d5 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.SpawnPosMed.X, VehGarageCfg.SpawnPosMed.Y, VehGarageCfg.SpawnPosMed.Z - 5f), 1.5f, 15f, VehGarageCfg.DimMed5 + _playerId);
                    var colMedium_d6 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.SpawnPosMed.X, VehGarageCfg.SpawnPosMed.Y, VehGarageCfg.SpawnPosMed.Z - 5f), 1.5f, 15f, VehGarageCfg.DimMed6 + _playerId);
                    markMedium_d1.SetData(MarkDataConst.MarkData, _exitMedium_1);
                    colMedium_d1.SetData(MarkDataConst.MarkData, _exitMedium_1);
                    markMedium_d2.SetData(MarkDataConst.MarkData, _exitMedium_2);
                    colMedium_d2.SetData(MarkDataConst.MarkData, _exitMedium_2);
                    markMedium_d3.SetData(MarkDataConst.MarkData, _exitMedium_3);
                    colMedium_d3.SetData(MarkDataConst.MarkData, _exitMedium_3);
                    markMedium_d4.SetData(MarkDataConst.MarkData, _exitMedium_4);
                    colMedium_d4.SetData(MarkDataConst.MarkData, _exitMedium_4);
                    markMedium_d5.SetData(MarkDataConst.MarkData, _exitMedium_5);
                    colMedium_d5.SetData(MarkDataConst.MarkData, _exitMedium_5);
                    markMedium_d6.SetData(MarkDataConst.MarkData, _exitMedium_6);
                    colMedium_d6.SetData(MarkDataConst.MarkData, _exitMedium_6);
                });

            NAPI.Task.Run(() =>
                {
                    var markBig_d1 = NAPI.Marker.CreateMarker(1, new Vector3(VehGarageCfg.SpawnPosBig.X, VehGarageCfg.SpawnPosBig.Y, VehGarageCfg.SpawnPosBig.Z - 1f), new Vector3(0, 0, 0), new Vector3(0, 0, 0), 1.1f, 155, 155, 155, false, VehGarageCfg.DimBig1 + _playerId);
                    var markBig_d2 = NAPI.Marker.CreateMarker(1, new Vector3(VehGarageCfg.SpawnPosBig.X, VehGarageCfg.SpawnPosBig.Y, VehGarageCfg.SpawnPosBig.Z - 1f), new Vector3(0, 0, 0), new Vector3(0, 0, 0), 1.1f, 155, 155, 155, false, VehGarageCfg.DimBig2 + _playerId);
                    var colBig_d1 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.SpawnPosBig.X, VehGarageCfg.SpawnPosBig.Y, VehGarageCfg.SpawnPosBig.Z - 5f), 1.5f, 15f, VehGarageCfg.DimBig1 + _playerId);
                    var colBig_d2 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.SpawnPosBig.X, VehGarageCfg.SpawnPosBig.Y, VehGarageCfg.SpawnPosBig.Z - 5f), 1.5f, 15f, VehGarageCfg.DimBig2 + _playerId);
                    markBig_d1.SetData(MarkDataConst.MarkData, _exitBig_1);
                    colBig_d1.SetData(MarkDataConst.MarkData, _exitBig_1);
                    markBig_d2.SetData(MarkDataConst.MarkData, _exitBig_2);
                    colBig_d2.SetData(MarkDataConst.MarkData, _exitBig_2);
                });

            NAPI.Task.Run(() =>
                {
                    var colAreaSmall_d1 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos1Small.X, VehGarageCfg.Pos1Small.Y, VehGarageCfg.Pos1Small.Z - 5f), 2.5f, 15f, VehGarageCfg.DimS1 + _playerId);
                    colAreaSmall_d1.SetData(MarkDataConst.MarkData, _areaSmall);

                    var colAreaSmall_d2 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos1Small.X, VehGarageCfg.Pos1Small.Y, VehGarageCfg.Pos1Small.Z - 5f), 2.5f, 15f, VehGarageCfg.DimS2 + _playerId);
                    colAreaSmall_d2.SetData(MarkDataConst.MarkData, _areaSmall);

                    var colAreaSmall_d3 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos1Small.X, VehGarageCfg.Pos1Small.Y, VehGarageCfg.Pos1Small.Z - 5f), 2.5f, 15f, VehGarageCfg.DimS3 + _playerId);
                    colAreaSmall_d3.SetData(MarkDataConst.MarkData, _areaSmall);

                    var colAreaSmall_d4 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos1Small.X, VehGarageCfg.Pos1Small.Y, VehGarageCfg.Pos1Small.Z - 5f), 2.5f, 15f, VehGarageCfg.DimS4 + _playerId);
                    colAreaSmall_d4.SetData(MarkDataConst.MarkData, _areaSmall);

                    var colAreaSmall_d5 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos1Small.X, VehGarageCfg.Pos1Small.Y, VehGarageCfg.Pos1Small.Z - 5f), 2.5f, 15f, VehGarageCfg.DimS5 + _playerId);
                    colAreaSmall_d5.SetData(MarkDataConst.MarkData, _areaSmall);

                    var colAreaSmall_d6 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos1Small.X, VehGarageCfg.Pos1Small.Y, VehGarageCfg.Pos1Small.Z - 5f), 2.5f, 15f, VehGarageCfg.DimS6 + _playerId);
                    colAreaSmall_d6.SetData(MarkDataConst.MarkData, _areaSmall);

                    var colAreaSmall_d7 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos1Small.X, VehGarageCfg.Pos1Small.Y, VehGarageCfg.Pos1Small.Z - 5f), 2.5f, 15f, VehGarageCfg.DimS7 + _playerId);
                    colAreaSmall_d7.SetData(MarkDataConst.MarkData, _areaSmall);

                    var colAreaSmall_d8 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos1Small.X, VehGarageCfg.Pos1Small.Y, VehGarageCfg.Pos1Small.Z - 5f), 2.5f, 15f, VehGarageCfg.DimS8 + _playerId);
                    colAreaSmall_d8.SetData(MarkDataConst.MarkData, _areaSmall);

                    var colAreaSmall_d9 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos1Small.X, VehGarageCfg.Pos1Small.Y, VehGarageCfg.Pos1Small.Z - 5f), 2.5f, 15f, VehGarageCfg.DimS9 + _playerId);
                    colAreaSmall_d9.SetData(MarkDataConst.MarkData, _areaSmall);

                    var colAreaSmall_d10 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos1Small.X, VehGarageCfg.Pos1Small.Y, VehGarageCfg.Pos1Small.Z - 5f), 2.5f, 15f, VehGarageCfg.DimS10 + _playerId);
                    colAreaSmall_d10.SetData(MarkDataConst.MarkData, _areaSmall);

                    var colAreaSmall_d11 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos1Small.X, VehGarageCfg.Pos1Small.Y, VehGarageCfg.Pos1Small.Z - 5f), 2.5f, 15f, VehGarageCfg.DimS11 + _playerId);
                    colAreaSmall_d11.SetData(MarkDataConst.MarkData, _areaSmall);

                    var colAreaSmall_d12 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos1Small.X, VehGarageCfg.Pos1Small.Y, VehGarageCfg.Pos1Small.Z - 5f), 2.5f, 15f, VehGarageCfg.DimS12 + _playerId);
                    colAreaSmall_d12.SetData(MarkDataConst.MarkData, _areaSmall);
                });

            NAPI.Task.Run(() =>
                {
                    var colAreaMedium1_d1 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos1Med.X, VehGarageCfg.Pos1Med.Y, VehGarageCfg.Pos1Med.Z - 5f), 2.5f, 15f, VehGarageCfg.DimMed1 + _playerId);
                    var colAreaMedium1_d2 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos1Med.X, VehGarageCfg.Pos1Med.Y, VehGarageCfg.Pos1Med.Z - 5f), 2.5f, 15f, VehGarageCfg.DimMed2 + _playerId);
                    var colAreaMedium1_d3 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos1Med.X, VehGarageCfg.Pos1Med.Y, VehGarageCfg.Pos1Med.Z - 5f), 2.5f, 15f, VehGarageCfg.DimMed3 + _playerId);
                    var colAreaMedium1_d4 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos1Med.X, VehGarageCfg.Pos1Med.Y, VehGarageCfg.Pos1Med.Z - 5f), 2.5f, 15f, VehGarageCfg.DimMed4 + _playerId);
                    var colAreaMedium1_d5 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos1Med.X, VehGarageCfg.Pos1Med.Y, VehGarageCfg.Pos1Med.Z - 5f), 2.5f, 15f, VehGarageCfg.DimMed5 + _playerId);
                    var colAreaMedium1_d6 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos1Med.X, VehGarageCfg.Pos1Med.Y, VehGarageCfg.Pos1Med.Z - 5f), 2.5f, 15f, VehGarageCfg.DimMed6 + _playerId);
                    colAreaMedium1_d1.SetData(MarkDataConst.MarkData, _areaMedium);
                    colAreaMedium1_d2.SetData(MarkDataConst.MarkData, _areaMedium);
                    colAreaMedium1_d3.SetData(MarkDataConst.MarkData, _areaMedium);
                    colAreaMedium1_d4.SetData(MarkDataConst.MarkData, _areaMedium);
                    colAreaMedium1_d5.SetData(MarkDataConst.MarkData, _areaMedium);
                    colAreaMedium1_d6.SetData(MarkDataConst.MarkData, _areaMedium);

                    var colAreaMedium2_d1 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos2Med.X, VehGarageCfg.Pos2Med.Y, VehGarageCfg.Pos2Med.Z - 5f), 2.5f, 15f, VehGarageCfg.DimMed1 + _playerId);
                    var colAreaMedium2_d2 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos2Med.X, VehGarageCfg.Pos2Med.Y, VehGarageCfg.Pos2Med.Z - 5f), 2.5f, 15f, VehGarageCfg.DimMed2 + _playerId);
                    var colAreaMedium2_d3 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos2Med.X, VehGarageCfg.Pos2Med.Y, VehGarageCfg.Pos2Med.Z - 5f), 2.5f, 15f, VehGarageCfg.DimMed3 + _playerId);
                    var colAreaMedium2_d4 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos2Med.X, VehGarageCfg.Pos2Med.Y, VehGarageCfg.Pos2Med.Z - 5f), 2.5f, 15f, VehGarageCfg.DimMed4 + _playerId);
                    var colAreaMedium2_d5 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos2Med.X, VehGarageCfg.Pos2Med.Y, VehGarageCfg.Pos2Med.Z - 5f), 2.5f, 15f, VehGarageCfg.DimMed5 + _playerId);
                    var colAreaMedium2_d6 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos2Med.X, VehGarageCfg.Pos2Med.Y, VehGarageCfg.Pos2Med.Z - 5f), 2.5f, 15f, VehGarageCfg.DimMed6 + _playerId);
                    colAreaMedium2_d1.SetData(MarkDataConst.MarkData, _areaMedium);
                    colAreaMedium2_d2.SetData(MarkDataConst.MarkData, _areaMedium);
                    colAreaMedium2_d3.SetData(MarkDataConst.MarkData, _areaMedium);
                    colAreaMedium2_d4.SetData(MarkDataConst.MarkData, _areaMedium);
                    colAreaMedium2_d5.SetData(MarkDataConst.MarkData, _areaMedium);
                    colAreaMedium2_d6.SetData(MarkDataConst.MarkData, _areaMedium);

                    var colAreaMedium3_d1 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos3Med.X, VehGarageCfg.Pos3Med.Y, VehGarageCfg.Pos3Med.Z - 5f), 2.5f, 15f, VehGarageCfg.DimMed1 + _playerId);
                    var colAreaMedium3_d2 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos3Med.X, VehGarageCfg.Pos3Med.Y, VehGarageCfg.Pos3Med.Z - 5f), 2.5f, 15f, VehGarageCfg.DimMed2 + _playerId);
                    var colAreaMedium3_d3 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos3Med.X, VehGarageCfg.Pos3Med.Y, VehGarageCfg.Pos3Med.Z - 5f), 2.5f, 15f, VehGarageCfg.DimMed3 + _playerId);
                    var colAreaMedium3_d4 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos3Med.X, VehGarageCfg.Pos3Med.Y, VehGarageCfg.Pos3Med.Z - 5f), 2.5f, 15f, VehGarageCfg.DimMed4 + _playerId);
                    var colAreaMedium3_d5 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos3Med.X, VehGarageCfg.Pos3Med.Y, VehGarageCfg.Pos3Med.Z - 5f), 2.5f, 15f, VehGarageCfg.DimMed5 + _playerId);
                    var colAreaMedium3_d6 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos3Med.X, VehGarageCfg.Pos3Med.Y, VehGarageCfg.Pos3Med.Z - 5f), 2.5f, 15f, VehGarageCfg.DimMed6 + _playerId);
                    colAreaMedium3_d1.SetData(MarkDataConst.MarkData, _areaMedium);
                    colAreaMedium3_d2.SetData(MarkDataConst.MarkData, _areaMedium);
                    colAreaMedium3_d3.SetData(MarkDataConst.MarkData, _areaMedium);
                    colAreaMedium3_d4.SetData(MarkDataConst.MarkData, _areaMedium);
                    colAreaMedium3_d5.SetData(MarkDataConst.MarkData, _areaMedium);
                    colAreaMedium3_d6.SetData(MarkDataConst.MarkData, _areaMedium);
                });

            NAPI.Task.Run(() =>
                {
                    var colAreaBig1_d1 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos1Big.X, VehGarageCfg.Pos1Big.Y, VehGarageCfg.Pos1Big.Z - 5f), 2.5f, 15f, VehGarageCfg.DimBig1 + _playerId);
                    var colAreaBig1_d2 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos1Big.X, VehGarageCfg.Pos1Big.Y, VehGarageCfg.Pos1Big.Z - 5f), 2.5f, 15f, VehGarageCfg.DimBig2 + _playerId);
                    colAreaBig1_d1.SetData(MarkDataConst.MarkData, _areaBig);
                    colAreaBig1_d2.SetData(MarkDataConst.MarkData, _areaBig);

                    var colAreaBig2_d1 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos2Big.X, VehGarageCfg.Pos2Big.Y, VehGarageCfg.Pos2Big.Z - 5f), 2.5f, 15f, VehGarageCfg.DimBig1 + _playerId);
                    var colAreaBig2_d2 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos2Big.X, VehGarageCfg.Pos2Big.Y, VehGarageCfg.Pos2Big.Z - 5f), 2.5f, 15f, VehGarageCfg.DimBig2 + _playerId);
                    colAreaBig2_d1.SetData(MarkDataConst.MarkData, _areaBig);
                    colAreaBig2_d2.SetData(MarkDataConst.MarkData, _areaBig);

                    var colAreaBig3_d1 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos3Big.X, VehGarageCfg.Pos3Big.Y, VehGarageCfg.Pos3Big.Z - 5f), 2.5f, 15f, VehGarageCfg.DimBig1 + _playerId);
                    var colAreaBig3_d2 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos3Big.X, VehGarageCfg.Pos3Big.Y, VehGarageCfg.Pos3Big.Z - 5f), 2.5f, 15f, VehGarageCfg.DimBig2 + _playerId);
                    colAreaBig3_d1.SetData(MarkDataConst.MarkData, _areaBig);
                    colAreaBig3_d2.SetData(MarkDataConst.MarkData, _areaBig);

                    var colAreaBig4_d1 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos4Big.X, VehGarageCfg.Pos4Big.Y, VehGarageCfg.Pos4Big.Z - 5f), 2.5f, 15f, VehGarageCfg.DimBig1 + _playerId);
                    var colAreaBig4_d2 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos4Big.X, VehGarageCfg.Pos4Big.Y, VehGarageCfg.Pos4Big.Z - 5f), 2.5f, 15f, VehGarageCfg.DimBig2 + _playerId);
                    colAreaBig4_d1.SetData(MarkDataConst.MarkData, _areaBig);
                    colAreaBig4_d2.SetData(MarkDataConst.MarkData, _areaBig);

                    var colAreaBig5_d1 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos5Big.X, VehGarageCfg.Pos5Big.Y, VehGarageCfg.Pos5Big.Z - 5f), 2.5f, 15f, VehGarageCfg.DimBig1 + _playerId);
                    var colAreaBig5_d2 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos5Big.X, VehGarageCfg.Pos5Big.Y, VehGarageCfg.Pos5Big.Z - 5f), 2.5f, 15f, VehGarageCfg.DimBig2 + _playerId);
                    colAreaBig5_d1.SetData(MarkDataConst.MarkData, _areaBig);
                    colAreaBig5_d2.SetData(MarkDataConst.MarkData, _areaBig);

                    var colAreaBig6_d1 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos6Big.X, VehGarageCfg.Pos6Big.Y, VehGarageCfg.Pos6Big.Z - 5f), 2.5f, 15f, VehGarageCfg.DimBig1 + _playerId);
                    var colAreaBig6_d2 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos6Big.X, VehGarageCfg.Pos6Big.Y, VehGarageCfg.Pos6Big.Z - 5f), 2.5f, 15f, VehGarageCfg.DimBig2 + _playerId);
                    colAreaBig6_d1.SetData(MarkDataConst.MarkData, _areaBig);
                    colAreaBig6_d2.SetData(MarkDataConst.MarkData, _areaBig);

                    var colAreaBig7_d1 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos7Big.X, VehGarageCfg.Pos7Big.Y, VehGarageCfg.Pos7Big.Z - 5f), 2.5f, 15f, VehGarageCfg.DimBig1 + _playerId);
                    var colAreaBig7_d2 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos7Big.X, VehGarageCfg.Pos7Big.Y, VehGarageCfg.Pos7Big.Z - 5f), 2.5f, 15f, VehGarageCfg.DimBig2 + _playerId);
                    colAreaBig7_d1.SetData(MarkDataConst.MarkData, _areaBig);
                    colAreaBig7_d2.SetData(MarkDataConst.MarkData, _areaBig);

                    var colAreaBig8_d1 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos8Big.X, VehGarageCfg.Pos8Big.Y, VehGarageCfg.Pos8Big.Z - 5f), 2.5f, 15f, VehGarageCfg.DimBig1 + _playerId);
                    var colAreaBig8_d2 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos8Big.X, VehGarageCfg.Pos8Big.Y, VehGarageCfg.Pos8Big.Z - 5f), 2.5f, 15f, VehGarageCfg.DimBig2 + _playerId);
                    colAreaBig8_d1.SetData(MarkDataConst.MarkData, _areaBig);
                    colAreaBig8_d2.SetData(MarkDataConst.MarkData, _areaBig);
                });
        }

        public void SpawnGarageDefaultLocations()
        {
            NAPI.Task.Run(() =>
                {
                    var markSmall_d1 = NAPI.Marker.CreateMarker(1, new Vector3(VehGarageCfg.SpawnPosSmall.X, VehGarageCfg.SpawnPosSmall.Y, VehGarageCfg.SpawnPosSmall.Z - 1f), new Vector3(0, 0, 0), new Vector3(0, 0, 0), 1.1f, 155, 155, 155, false, _playerId);
                    var markSmall_d2 = NAPI.Marker.CreateMarker(1, new Vector3(VehGarageCfg.SpawnPosSmall.X, VehGarageCfg.SpawnPosSmall.Y, VehGarageCfg.SpawnPosSmall.Z - 1f), new Vector3(0, 0, 0), new Vector3(0, 0, 0), 1.1f, 155, 155, 155, false, _playerId);
                    var markSmall_d3 = NAPI.Marker.CreateMarker(1, new Vector3(VehGarageCfg.SpawnPosSmall.X, VehGarageCfg.SpawnPosSmall.Y, VehGarageCfg.SpawnPosSmall.Z - 1f), new Vector3(0, 0, 0), new Vector3(0, 0, 0), 1.1f, 155, 155, 155, false, _playerId);
                    var markSmall_d4 = NAPI.Marker.CreateMarker(1, new Vector3(VehGarageCfg.SpawnPosSmall.X, VehGarageCfg.SpawnPosSmall.Y, VehGarageCfg.SpawnPosSmall.Z - 1f), new Vector3(0, 0, 0), new Vector3(0, 0, 0), 1.1f, 155, 155, 155, false, _playerId);
                    var markSmall_d5 = NAPI.Marker.CreateMarker(1, new Vector3(VehGarageCfg.SpawnPosSmall.X, VehGarageCfg.SpawnPosSmall.Y, VehGarageCfg.SpawnPosSmall.Z - 1f), new Vector3(0, 0, 0), new Vector3(0, 0, 0), 1.1f, 155, 155, 155, false, _playerId);
                    var markSmall_d6 = NAPI.Marker.CreateMarker(1, new Vector3(VehGarageCfg.SpawnPosSmall.X, VehGarageCfg.SpawnPosSmall.Y, VehGarageCfg.SpawnPosSmall.Z - 1f), new Vector3(0, 0, 0), new Vector3(0, 0, 0), 1.1f, 155, 155, 155, false, _playerId);
                    var markSmall_d7 = NAPI.Marker.CreateMarker(1, new Vector3(VehGarageCfg.SpawnPosSmall.X, VehGarageCfg.SpawnPosSmall.Y, VehGarageCfg.SpawnPosSmall.Z - 1f), new Vector3(0, 0, 0), new Vector3(0, 0, 0), 1.1f, 155, 155, 155, false, _playerId);
                    var markSmall_d8 = NAPI.Marker.CreateMarker(1, new Vector3(VehGarageCfg.SpawnPosSmall.X, VehGarageCfg.SpawnPosSmall.Y, VehGarageCfg.SpawnPosSmall.Z - 1f), new Vector3(0, 0, 0), new Vector3(0, 0, 0), 1.1f, 155, 155, 155, false, _playerId);
                    var markSmall_d9 = NAPI.Marker.CreateMarker(1, new Vector3(VehGarageCfg.SpawnPosSmall.X, VehGarageCfg.SpawnPosSmall.Y, VehGarageCfg.SpawnPosSmall.Z - 1f), new Vector3(0, 0, 0), new Vector3(0, 0, 0), 1.1f, 155, 155, 155, false, _playerId);
                    var markSmall_d10 = NAPI.Marker.CreateMarker(1, new Vector3(VehGarageCfg.SpawnPosSmall.X, VehGarageCfg.SpawnPosSmall.Y, VehGarageCfg.SpawnPosSmall.Z - 1f), new Vector3(0, 0, 0), new Vector3(0, 0, 0), 1.1f, 155, 155, 155, false, _playerId);
                    var markSmall_d11 = NAPI.Marker.CreateMarker(1, new Vector3(VehGarageCfg.SpawnPosSmall.X, VehGarageCfg.SpawnPosSmall.Y, VehGarageCfg.SpawnPosSmall.Z - 1f), new Vector3(0, 0, 0), new Vector3(0, 0, 0), 1.1f, 155, 155, 155, false, _playerId);
                    var markSmall_d12 = NAPI.Marker.CreateMarker(1, new Vector3(VehGarageCfg.SpawnPosSmall.X, VehGarageCfg.SpawnPosSmall.Y, VehGarageCfg.SpawnPosSmall.Z - 1f), new Vector3(0, 0, 0), new Vector3(0, 0, 0), 1.1f, 155, 155, 155, false, _playerId);
                    var colSmall_d1 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.SpawnPosSmall.X, VehGarageCfg.SpawnPosSmall.Y, VehGarageCfg.SpawnPosSmall.Z - 5f), 1.5f, 15f, _playerId);
                    var colSmall_d2 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.SpawnPosSmall.X, VehGarageCfg.SpawnPosSmall.Y, VehGarageCfg.SpawnPosSmall.Z - 5f), 1.5f, 15f, _playerId);
                    var colSmall_d3 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.SpawnPosSmall.X, VehGarageCfg.SpawnPosSmall.Y, VehGarageCfg.SpawnPosSmall.Z - 5f), 1.5f, 15f, _playerId);
                    var colSmall_d4 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.SpawnPosSmall.X, VehGarageCfg.SpawnPosSmall.Y, VehGarageCfg.SpawnPosSmall.Z - 5f), 1.5f, 15f, _playerId);
                    var colSmall_d5 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.SpawnPosSmall.X, VehGarageCfg.SpawnPosSmall.Y, VehGarageCfg.SpawnPosSmall.Z - 5f), 1.5f, 15f, _playerId);
                    var colSmall_d6 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.SpawnPosSmall.X, VehGarageCfg.SpawnPosSmall.Y, VehGarageCfg.SpawnPosSmall.Z - 5f), 1.5f, 15f, _playerId);
                    var colSmall_d7 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.SpawnPosSmall.X, VehGarageCfg.SpawnPosSmall.Y, VehGarageCfg.SpawnPosSmall.Z - 5f), 1.5f, 15f, _playerId);
                    var colSmall_d8 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.SpawnPosSmall.X, VehGarageCfg.SpawnPosSmall.Y, VehGarageCfg.SpawnPosSmall.Z - 5f), 1.5f, 15f, _playerId);
                    var colSmall_d9 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.SpawnPosSmall.X, VehGarageCfg.SpawnPosSmall.Y, VehGarageCfg.SpawnPosSmall.Z - 5f), 1.5f, 15f, _playerId);
                    var colSmall_d10 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.SpawnPosSmall.X, VehGarageCfg.SpawnPosSmall.Y, VehGarageCfg.SpawnPosSmall.Z - 5f), 1.5f, 15f, _playerId);
                    var colSmall_d11 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.SpawnPosSmall.X, VehGarageCfg.SpawnPosSmall.Y, VehGarageCfg.SpawnPosSmall.Z - 5f), 1.5f, 15f, _playerId);
                    var colSmall_d12 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.SpawnPosSmall.X, VehGarageCfg.SpawnPosSmall.Y, VehGarageCfg.SpawnPosSmall.Z - 5f), 1.5f, 15f, _playerId);
                    markSmall_d1.SetData(MarkDataConst.MarkData, _exitSmall_1);
                    colSmall_d1.SetData(MarkDataConst.MarkData, _exitSmall_1);
                    markSmall_d2.SetData(MarkDataConst.MarkData, _exitSmall_2);
                    colSmall_d2.SetData(MarkDataConst.MarkData, _exitSmall_2);
                    markSmall_d3.SetData(MarkDataConst.MarkData, _exitSmall_3);
                    colSmall_d3.SetData(MarkDataConst.MarkData, _exitSmall_3);
                    markSmall_d4.SetData(MarkDataConst.MarkData, _exitSmall_4);
                    colSmall_d4.SetData(MarkDataConst.MarkData, _exitSmall_4);
                    markSmall_d5.SetData(MarkDataConst.MarkData, _exitSmall_5);
                    colSmall_d5.SetData(MarkDataConst.MarkData, _exitSmall_5);
                    markSmall_d6.SetData(MarkDataConst.MarkData, _exitSmall_6);
                    colSmall_d6.SetData(MarkDataConst.MarkData, _exitSmall_6);
                    markSmall_d7.SetData(MarkDataConst.MarkData, _exitSmall_7);
                    colSmall_d7.SetData(MarkDataConst.MarkData, _exitSmall_7);
                    markSmall_d8.SetData(MarkDataConst.MarkData, _exitSmall_8);
                    colSmall_d8.SetData(MarkDataConst.MarkData, _exitSmall_8);
                    markSmall_d9.SetData(MarkDataConst.MarkData, _exitSmall_9);
                    colSmall_d9.SetData(MarkDataConst.MarkData, _exitSmall_9);
                    markSmall_d10.SetData(MarkDataConst.MarkData, _exitSmall_10);
                    colSmall_d10.SetData(MarkDataConst.MarkData, _exitSmall_10);
                    markSmall_d11.SetData(MarkDataConst.MarkData, _exitSmall_11);
                    colSmall_d11.SetData(MarkDataConst.MarkData, _exitSmall_11);
                    markSmall_d12.SetData(MarkDataConst.MarkData, _exitSmall_12);
                    colSmall_d12.SetData(MarkDataConst.MarkData, _exitSmall_12);
                });

            NAPI.Task.Run(() =>
                {
                    var markMedium_d1 = NAPI.Marker.CreateMarker(1, new Vector3(VehGarageCfg.SpawnPosMed.X, VehGarageCfg.SpawnPosMed.Y, VehGarageCfg.SpawnPosMed.Z - 1f), new Vector3(0, 0, 0), new Vector3(0, 0, 0), 1.1f, 155, 155, 155, false, _playerId);
                    var markMedium_d2 = NAPI.Marker.CreateMarker(1, new Vector3(VehGarageCfg.SpawnPosMed.X, VehGarageCfg.SpawnPosMed.Y, VehGarageCfg.SpawnPosMed.Z - 1f), new Vector3(0, 0, 0), new Vector3(0, 0, 0), 1.1f, 155, 155, 155, false, _playerId);
                    var markMedium_d3 = NAPI.Marker.CreateMarker(1, new Vector3(VehGarageCfg.SpawnPosMed.X, VehGarageCfg.SpawnPosMed.Y, VehGarageCfg.SpawnPosMed.Z - 1f), new Vector3(0, 0, 0), new Vector3(0, 0, 0), 1.1f, 155, 155, 155, false, _playerId);
                    var markMedium_d4 = NAPI.Marker.CreateMarker(1, new Vector3(VehGarageCfg.SpawnPosMed.X, VehGarageCfg.SpawnPosMed.Y, VehGarageCfg.SpawnPosMed.Z - 1f), new Vector3(0, 0, 0), new Vector3(0, 0, 0), 1.1f, 155, 155, 155, false, _playerId);
                    var markMedium_d5 = NAPI.Marker.CreateMarker(1, new Vector3(VehGarageCfg.SpawnPosMed.X, VehGarageCfg.SpawnPosMed.Y, VehGarageCfg.SpawnPosMed.Z - 1f), new Vector3(0, 0, 0), new Vector3(0, 0, 0), 1.1f, 155, 155, 155, false, _playerId);
                    var markMedium_d6 = NAPI.Marker.CreateMarker(1, new Vector3(VehGarageCfg.SpawnPosMed.X, VehGarageCfg.SpawnPosMed.Y, VehGarageCfg.SpawnPosMed.Z - 1f), new Vector3(0, 0, 0), new Vector3(0, 0, 0), 1.1f, 155, 155, 155, false, _playerId);
                    var colMedium_d1 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.SpawnPosMed.X, VehGarageCfg.SpawnPosMed.Y, VehGarageCfg.SpawnPosMed.Z - 5f), 1.5f, 15f, VehGarageCfg.DimMed1 + _playerId);
                    var colMedium_d2 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.SpawnPosMed.X, VehGarageCfg.SpawnPosMed.Y, VehGarageCfg.SpawnPosMed.Z - 5f), 1.5f, 15f, VehGarageCfg.DimMed2 + _playerId);
                    var colMedium_d3 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.SpawnPosMed.X, VehGarageCfg.SpawnPosMed.Y, VehGarageCfg.SpawnPosMed.Z - 5f), 1.5f, 15f, VehGarageCfg.DimMed3 + _playerId);
                    var colMedium_d4 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.SpawnPosMed.X, VehGarageCfg.SpawnPosMed.Y, VehGarageCfg.SpawnPosMed.Z - 5f), 1.5f, 15f, VehGarageCfg.DimMed4 + _playerId);
                    var colMedium_d5 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.SpawnPosMed.X, VehGarageCfg.SpawnPosMed.Y, VehGarageCfg.SpawnPosMed.Z - 5f), 1.5f, 15f, VehGarageCfg.DimMed5 + _playerId);
                    var colMedium_d6 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.SpawnPosMed.X, VehGarageCfg.SpawnPosMed.Y, VehGarageCfg.SpawnPosMed.Z - 5f), 1.5f, 15f, VehGarageCfg.DimMed6 + _playerId);
                    markMedium_d1.SetData(MarkDataConst.MarkData, _exitMedium_1);
                    colMedium_d1.SetData(MarkDataConst.MarkData, _exitMedium_1);
                    markMedium_d2.SetData(MarkDataConst.MarkData, _exitMedium_2);
                    colMedium_d2.SetData(MarkDataConst.MarkData, _exitMedium_2);
                    markMedium_d3.SetData(MarkDataConst.MarkData, _exitMedium_3);
                    colMedium_d3.SetData(MarkDataConst.MarkData, _exitMedium_3);
                    markMedium_d4.SetData(MarkDataConst.MarkData, _exitMedium_4);
                    colMedium_d4.SetData(MarkDataConst.MarkData, _exitMedium_4);
                    markMedium_d5.SetData(MarkDataConst.MarkData, _exitMedium_5);
                    colMedium_d5.SetData(MarkDataConst.MarkData, _exitMedium_5);
                    markMedium_d6.SetData(MarkDataConst.MarkData, _exitMedium_6);
                    colMedium_d6.SetData(MarkDataConst.MarkData, _exitMedium_6);
                });

            NAPI.Task.Run(() =>
                {
                    var markBig_d1 = NAPI.Marker.CreateMarker(1, new Vector3(VehGarageCfg.SpawnPosBig.X, VehGarageCfg.SpawnPosBig.Y, VehGarageCfg.SpawnPosBig.Z - 1f), new Vector3(0, 0, 0), new Vector3(0, 0, 0), 1.1f, 155, 155, 155, false, _playerId);
                    var markBig_d2 = NAPI.Marker.CreateMarker(1, new Vector3(VehGarageCfg.SpawnPosBig.X, VehGarageCfg.SpawnPosBig.Y, VehGarageCfg.SpawnPosBig.Z - 1f), new Vector3(0, 0, 0), new Vector3(0, 0, 0), 1.1f, 155, 155, 155, false, _playerId);
                    var colBig_d1 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.SpawnPosBig.X, VehGarageCfg.SpawnPosBig.Y, VehGarageCfg.SpawnPosBig.Z - 5f), 1.5f, 15f, VehGarageCfg.DimBig1 + _playerId);
                    var colBig_d2 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.SpawnPosBig.X, VehGarageCfg.SpawnPosBig.Y, VehGarageCfg.SpawnPosBig.Z - 5f), 1.5f, 15f, VehGarageCfg.DimBig2 + _playerId);
                    markBig_d1.SetData(MarkDataConst.MarkData, _exitBig_1);
                    colBig_d1.SetData(MarkDataConst.MarkData, _exitBig_1);
                    markBig_d2.SetData(MarkDataConst.MarkData, _exitBig_2);
                    colBig_d2.SetData(MarkDataConst.MarkData, _exitBig_2);
                });

            NAPI.Task.Run(() =>
                {
                    var colAreaSmall_d1 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos1Small.X, VehGarageCfg.Pos1Small.Y, VehGarageCfg.Pos1Small.Z - 5f), 2.5f, 15f, _playerId);
                    colAreaSmall_d1.SetData(MarkDataConst.MarkData, _areaSmall);

                    var colAreaSmall_d2 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos1Small.X, VehGarageCfg.Pos1Small.Y, VehGarageCfg.Pos1Small.Z - 5f), 2.5f, 15f, _playerId);
                    colAreaSmall_d2.SetData(MarkDataConst.MarkData, _areaSmall);

                    var colAreaSmall_d3 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos1Small.X, VehGarageCfg.Pos1Small.Y, VehGarageCfg.Pos1Small.Z - 5f), 2.5f, 15f, _playerId);
                    colAreaSmall_d3.SetData(MarkDataConst.MarkData, _areaSmall);

                    var colAreaSmall_d4 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos1Small.X, VehGarageCfg.Pos1Small.Y, VehGarageCfg.Pos1Small.Z - 5f), 2.5f, 15f, _playerId);
                    colAreaSmall_d4.SetData(MarkDataConst.MarkData, _areaSmall);

                    var colAreaSmall_d5 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos1Small.X, VehGarageCfg.Pos1Small.Y, VehGarageCfg.Pos1Small.Z - 5f), 2.5f, 15f, _playerId);
                    colAreaSmall_d5.SetData(MarkDataConst.MarkData, _areaSmall);

                    var colAreaSmall_d6 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos1Small.X, VehGarageCfg.Pos1Small.Y, VehGarageCfg.Pos1Small.Z - 5f), 2.5f, 15f, _playerId);
                    colAreaSmall_d6.SetData(MarkDataConst.MarkData, _areaSmall);

                    var colAreaSmall_d7 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos1Small.X, VehGarageCfg.Pos1Small.Y, VehGarageCfg.Pos1Small.Z - 5f), 2.5f, 15f, _playerId);
                    colAreaSmall_d7.SetData(MarkDataConst.MarkData, _areaSmall);

                    var colAreaSmall_d8 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos1Small.X, VehGarageCfg.Pos1Small.Y, VehGarageCfg.Pos1Small.Z - 5f), 2.5f, 15f, _playerId);
                    colAreaSmall_d8.SetData(MarkDataConst.MarkData, _areaSmall);

                    var colAreaSmall_d9 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos1Small.X, VehGarageCfg.Pos1Small.Y, VehGarageCfg.Pos1Small.Z - 5f), 2.5f, 15f, _playerId);
                    colAreaSmall_d9.SetData(MarkDataConst.MarkData, _areaSmall);

                    var colAreaSmall_d10 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos1Small.X, VehGarageCfg.Pos1Small.Y, VehGarageCfg.Pos1Small.Z - 5f), 2.5f, 15f, _playerId);
                    colAreaSmall_d10.SetData(MarkDataConst.MarkData, _areaSmall);

                    var colAreaSmall_d11 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos1Small.X, VehGarageCfg.Pos1Small.Y, VehGarageCfg.Pos1Small.Z - 5f), 2.5f, 15f, _playerId);
                    colAreaSmall_d11.SetData(MarkDataConst.MarkData, _areaSmall);

                    var colAreaSmall_d12 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos1Small.X, VehGarageCfg.Pos1Small.Y, VehGarageCfg.Pos1Small.Z - 5f), 2.5f, 15f, _playerId);
                    colAreaSmall_d12.SetData(MarkDataConst.MarkData, _areaSmall);
                });

            NAPI.Task.Run(() =>
                {
                    var colAreaMedium1_d1 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos1Med.X, VehGarageCfg.Pos1Med.Y, VehGarageCfg.Pos1Med.Z - 5f), 2.5f, 15f, _playerId);
                    var colAreaMedium1_d2 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos1Med.X, VehGarageCfg.Pos1Med.Y, VehGarageCfg.Pos1Med.Z - 5f), 2.5f, 15f, _playerId);
                    var colAreaMedium1_d3 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos1Med.X, VehGarageCfg.Pos1Med.Y, VehGarageCfg.Pos1Med.Z - 5f), 2.5f, 15f, _playerId);
                    var colAreaMedium1_d4 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos1Med.X, VehGarageCfg.Pos1Med.Y, VehGarageCfg.Pos1Med.Z - 5f), 2.5f, 15f, _playerId);
                    var colAreaMedium1_d5 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos1Med.X, VehGarageCfg.Pos1Med.Y, VehGarageCfg.Pos1Med.Z - 5f), 2.5f, 15f, _playerId);
                    var colAreaMedium1_d6 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos1Med.X, VehGarageCfg.Pos1Med.Y, VehGarageCfg.Pos1Med.Z - 5f), 2.5f, 15f, _playerId);
                    colAreaMedium1_d1.SetData(MarkDataConst.MarkData, _areaMedium);
                    colAreaMedium1_d2.SetData(MarkDataConst.MarkData, _areaMedium);
                    colAreaMedium1_d3.SetData(MarkDataConst.MarkData, _areaMedium);
                    colAreaMedium1_d4.SetData(MarkDataConst.MarkData, _areaMedium);
                    colAreaMedium1_d5.SetData(MarkDataConst.MarkData, _areaMedium);
                    colAreaMedium1_d6.SetData(MarkDataConst.MarkData, _areaMedium);

                    var colAreaMedium2_d1 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos2Med.X, VehGarageCfg.Pos2Med.Y, VehGarageCfg.Pos2Med.Z - 5f), 2.5f, 15f, _playerId);
                    var colAreaMedium2_d2 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos2Med.X, VehGarageCfg.Pos2Med.Y, VehGarageCfg.Pos2Med.Z - 5f), 2.5f, 15f, _playerId);
                    var colAreaMedium2_d3 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos2Med.X, VehGarageCfg.Pos2Med.Y, VehGarageCfg.Pos2Med.Z - 5f), 2.5f, 15f, _playerId);
                    var colAreaMedium2_d4 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos2Med.X, VehGarageCfg.Pos2Med.Y, VehGarageCfg.Pos2Med.Z - 5f), 2.5f, 15f, _playerId);
                    var colAreaMedium2_d5 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos2Med.X, VehGarageCfg.Pos2Med.Y, VehGarageCfg.Pos2Med.Z - 5f), 2.5f, 15f, _playerId);
                    var colAreaMedium2_d6 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos2Med.X, VehGarageCfg.Pos2Med.Y, VehGarageCfg.Pos2Med.Z - 5f), 2.5f, 15f, _playerId);
                    colAreaMedium2_d1.SetData(MarkDataConst.MarkData, _areaMedium);
                    colAreaMedium2_d2.SetData(MarkDataConst.MarkData, _areaMedium);
                    colAreaMedium2_d3.SetData(MarkDataConst.MarkData, _areaMedium);
                    colAreaMedium2_d4.SetData(MarkDataConst.MarkData, _areaMedium);
                    colAreaMedium2_d5.SetData(MarkDataConst.MarkData, _areaMedium);
                    colAreaMedium2_d6.SetData(MarkDataConst.MarkData, _areaMedium);

                    var colAreaMedium3_d1 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos3Med.X, VehGarageCfg.Pos3Med.Y, VehGarageCfg.Pos3Med.Z - 5f), 2.5f, 15f, _playerId);
                    var colAreaMedium3_d2 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos3Med.X, VehGarageCfg.Pos3Med.Y, VehGarageCfg.Pos3Med.Z - 5f), 2.5f, 15f, _playerId);
                    var colAreaMedium3_d3 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos3Med.X, VehGarageCfg.Pos3Med.Y, VehGarageCfg.Pos3Med.Z - 5f), 2.5f, 15f, _playerId);
                    var colAreaMedium3_d4 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos3Med.X, VehGarageCfg.Pos3Med.Y, VehGarageCfg.Pos3Med.Z - 5f), 2.5f, 15f, _playerId);
                    var colAreaMedium3_d5 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos3Med.X, VehGarageCfg.Pos3Med.Y, VehGarageCfg.Pos3Med.Z - 5f), 2.5f, 15f, _playerId);
                    var colAreaMedium3_d6 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos3Med.X, VehGarageCfg.Pos3Med.Y, VehGarageCfg.Pos3Med.Z - 5f), 2.5f, 15f, _playerId);
                    colAreaMedium3_d1.SetData(MarkDataConst.MarkData, _areaMedium);
                    colAreaMedium3_d2.SetData(MarkDataConst.MarkData, _areaMedium);
                    colAreaMedium3_d3.SetData(MarkDataConst.MarkData, _areaMedium);
                    colAreaMedium3_d4.SetData(MarkDataConst.MarkData, _areaMedium);
                    colAreaMedium3_d5.SetData(MarkDataConst.MarkData, _areaMedium);
                    colAreaMedium3_d6.SetData(MarkDataConst.MarkData, _areaMedium);
                });

            NAPI.Task.Run(() =>
                {
                    var colAreaBig1_d1 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos1Big.X, VehGarageCfg.Pos1Big.Y, VehGarageCfg.Pos1Big.Z - 5f), 2.5f, 15f, _playerId);
                    var colAreaBig1_d2 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos1Big.X, VehGarageCfg.Pos1Big.Y, VehGarageCfg.Pos1Big.Z - 5f), 2.5f, 15f, _playerId);
                    colAreaBig1_d1.SetData(MarkDataConst.MarkData, _areaBig);
                    colAreaBig1_d2.SetData(MarkDataConst.MarkData, _areaBig);

                    var colAreaBig2_d1 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos2Big.X, VehGarageCfg.Pos2Big.Y, VehGarageCfg.Pos2Big.Z - 5f), 2.5f, 15f, _playerId);
                    var colAreaBig2_d2 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos2Big.X, VehGarageCfg.Pos2Big.Y, VehGarageCfg.Pos2Big.Z - 5f), 2.5f, 15f, _playerId);
                    colAreaBig2_d1.SetData(MarkDataConst.MarkData, _areaBig);
                    colAreaBig2_d2.SetData(MarkDataConst.MarkData, _areaBig);

                    var colAreaBig3_d1 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos3Big.X, VehGarageCfg.Pos3Big.Y, VehGarageCfg.Pos3Big.Z - 5f), 2.5f, 15f, _playerId);
                    var colAreaBig3_d2 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos3Big.X, VehGarageCfg.Pos3Big.Y, VehGarageCfg.Pos3Big.Z - 5f), 2.5f, 15f, _playerId);
                    colAreaBig3_d1.SetData(MarkDataConst.MarkData, _areaBig);
                    colAreaBig3_d2.SetData(MarkDataConst.MarkData, _areaBig);

                    var colAreaBig4_d1 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos4Big.X, VehGarageCfg.Pos4Big.Y, VehGarageCfg.Pos4Big.Z - 5f), 2.5f, 15f, _playerId);
                    var colAreaBig4_d2 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos4Big.X, VehGarageCfg.Pos4Big.Y, VehGarageCfg.Pos4Big.Z - 5f), 2.5f, 15f, _playerId);
                    colAreaBig4_d1.SetData(MarkDataConst.MarkData, _areaBig);
                    colAreaBig4_d2.SetData(MarkDataConst.MarkData, _areaBig);

                    var colAreaBig5_d1 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos5Big.X, VehGarageCfg.Pos5Big.Y, VehGarageCfg.Pos5Big.Z - 5f), 2.5f, 15f, _playerId);
                    var colAreaBig5_d2 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos5Big.X, VehGarageCfg.Pos5Big.Y, VehGarageCfg.Pos5Big.Z - 5f), 2.5f, 15f, _playerId);
                    colAreaBig5_d1.SetData(MarkDataConst.MarkData, _areaBig);
                    colAreaBig5_d2.SetData(MarkDataConst.MarkData, _areaBig);

                    var colAreaBig6_d1 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos6Big.X, VehGarageCfg.Pos6Big.Y, VehGarageCfg.Pos6Big.Z - 5f), 2.5f, 15f, _playerId);
                    var colAreaBig6_d2 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos6Big.X, VehGarageCfg.Pos6Big.Y, VehGarageCfg.Pos6Big.Z - 5f), 2.5f, 15f, _playerId);
                    colAreaBig6_d1.SetData(MarkDataConst.MarkData, _areaBig);
                    colAreaBig6_d2.SetData(MarkDataConst.MarkData, _areaBig);

                    var colAreaBig7_d1 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos7Big.X, VehGarageCfg.Pos7Big.Y, VehGarageCfg.Pos7Big.Z - 5f), 2.5f, 15f, _playerId);
                    var colAreaBig7_d2 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos7Big.X, VehGarageCfg.Pos7Big.Y, VehGarageCfg.Pos7Big.Z - 5f), 2.5f, 15f, _playerId);
                    colAreaBig7_d1.SetData(MarkDataConst.MarkData, _areaBig);
                    colAreaBig7_d2.SetData(MarkDataConst.MarkData, _areaBig);

                    var colAreaBig8_d1 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos8Big.X, VehGarageCfg.Pos8Big.Y, VehGarageCfg.Pos8Big.Z - 5f), 2.5f, 15f, _playerId);
                    var colAreaBig8_d2 = NAPI.ColShape.CreateCylinderColShape(new Vector3(VehGarageCfg.Pos8Big.X, VehGarageCfg.Pos8Big.Y, VehGarageCfg.Pos8Big.Z - 5f), 2.5f, 15f, _playerId);
                    colAreaBig8_d1.SetData(MarkDataConst.MarkData, _areaBig);
                    colAreaBig8_d2.SetData(MarkDataConst.MarkData, _areaBig);
                });
        }
    }
}