﻿using GTANetworkAPI;
using Newtonsoft.Json;
using RolePlayGameData.Configs.Vehicle;
using RolePlayGameData.Database.RolePlayModels;
using RolePlayGameData.EntityDataConsts;
using RolePlayGameData.Models;
using RolePlayGameServer.Database.Controllers;
using RolePlayGameServer.ServerLists;
using RolePlayGameServer.Util;
using RolePlayGameServer.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RolePlayGameServer.Garages
{
    public class BigGarage
    {
        public static BigGarage Instance => _instance.Value;

        private static SortedDictionary<string, GarageSpawnModel> _entryPoints = new SortedDictionary<string, GarageSpawnModel>();
        private static readonly Lazy<BigGarage> _instance = new Lazy<BigGarage>(() => new BigGarage());

        private BigGarage() { }

        public async void ToGarage(Client player, MarkDataModel markData)
        {
            player.SetData(PlayerDataConst.IsInGarageFlag, true);

            var playerPos = player.Position;
            var playerVeh = player.Vehicle;
            var playerData = (PlayerAccountModel)player.GetData(PlayerDataConst.PlayerAccount);
            var dbGarage = await RolePlayDbController.GetGarageBig(player.SocialClubName).ConfigureAwait(false);
            uint dimension = 0;
            bool isOwned = false;
            bool isPayed = false;

            switch (markData.MarkName)
            {
                case 1:
                    dimension = VehGarageCfg.DimBig1 + (uint)playerData.Id;
                    isOwned = dbGarage.OneIsOwned;
                    isPayed = dbGarage.OneIsPayed;
                    break;
                case 2:
                    dimension = VehGarageCfg.DimBig2 + (uint)playerData.Id;
                    isOwned = dbGarage.TwoIsOwned;
                    isPayed = dbGarage.TwoIsPayed;
                    break;
            }

            GarageSpawnModel spawns = null;

            if (VehGarageSpawnCfg.GarageSpawnList.TryGetValue(markData.MarkType, out var spawnData))
            {
                spawns = spawnData.First(x => x.MarkName == markData.MarkName);
            }
            else NAPI.Util.ConsoleOutput($"garageSpawn in vehicleGarageSpawnCfg for markId {markData.MarkName} not found");

            if (_entryPoints.ContainsKey(player.SocialClubName)) _entryPoints.Remove(player.SocialClubName);

            _entryPoints.Add(player.SocialClubName, spawns);

            dbGarage.LastEntryX = playerPos.X;
            dbGarage.LastEntryY = playerPos.Y;
            dbGarage.LastEntryZ = playerPos.Z;

            await RolePlayDbController.UpdateGarageBig(dbGarage).ConfigureAwait(false);

            if (playerVeh != null)
            {
                var vehData = (VehicleModel)playerVeh.GetData(VehicleDataConst.CivVehicleData);
                var dbvehicle = await RolePlayDbController.GetVehicle(vehData.Id).ConfigureAwait(false);

                if (await GetNextSlot(dbvehicle, dbGarage, markData, (uint)playerData.Id).ConfigureAwait(false))
                {
                    NAPI.Task.Run(playerVeh.Delete);

                    player.Dimension = dimension;
                    player.Position = VehGarageCfg.SpawnPosBig;
                    player.Rotation = VehGarageCfg.SpawnRotBig;
                }
                else
                {
                    player.SendNotification("~r~Du hast keinen Platz mehr in der Garage!");
                    return;
                }
            }
            else
            {
                player.Dimension = dimension;
                player.Position = VehGarageCfg.SpawnPosBig;
                player.Rotation = VehGarageCfg.SpawnRotBig;
            }
        }

        public async void OutOfGarage(Client player, MarkDataModel markData)
        {
            var vehicle = player.Vehicle;
            var dbGarage = await RolePlayDbController.GetGarageBig(player.SocialClubName).ConfigureAwait(false);

            if (_entryPoints.TryGetValue(player.SocialClubName, out var spawns))
            {
                var spawnPos = GetFreeSpawnPos(spawns).Result;

                if (spawnPos.Key == default)
                {
                    player.SendNotification("~r~Alle Spawnpunkte sind belegt, bitte warten!");
                    return;
                }

                if (vehicle != null)
                {
                    var vehData = (VehicleModel)vehicle.GetData(VehicleDataConst.CivVehicleData);

                    vehicle.Dimension = 0;
                    vehicle.Position = spawnPos.Key;
                    vehicle.Rotation = spawnPos.Value;

                    await Task.Delay(1000).ConfigureAwait(false);

                    player.Dimension = 0;
                    player.SetIntoVehicle(vehicle, -1);
                    player.ResetData(PlayerDataConst.IsInGarageFlag);

                    dbGarage.LastEntryX = spawnPos.Key.X;
                    dbGarage.LastEntryY = spawnPos.Key.Y;
                    dbGarage.LastEntryZ = spawnPos.Key.Z;

                    var slotsOne = JsonConvert.DeserializeObject<int[]>(dbGarage.OneVehicleSlots);
                    var slotsTwo = JsonConvert.DeserializeObject<int[]>(dbGarage.TwoVehicleSlots);

                    if (slotsOne.Contains(vehData.Id))
                    {
                        var i = Array.IndexOf(slotsOne, vehData.Id);
                        slotsOne[i] = -1;
                        dbGarage.OneVehicleSlots = JsonConvert.SerializeObject(slotsOne);
                    }
                    else if (slotsTwo.Contains(vehData.Id))
                    {
                        var i = Array.IndexOf(slotsTwo, vehData.Id);
                        slotsTwo[i] = -1;
                        dbGarage.TwoVehicleSlots = JsonConvert.SerializeObject(slotsTwo);
                    }
                    else return;

                    await RolePlayDbController.UpdateGarageBig(dbGarage).ConfigureAwait(false);
                }
                else
                {
                    player.Dimension = 0;

                    switch (markData.MarkName)
                    {
                        case 1:                            
                            player.Position = VehGarageCfg.ExitSpawnPosB1;
                            player.Rotation = VehGarageCfg.ExitSpawnRotB1;
                            break;
                        case 2:
                            player.Position = VehGarageCfg.ExitSpawnPosB2;
                            player.Rotation = VehGarageCfg.ExitSpawnRotB2;
                            break;
                    }

                    player.ResetData(PlayerDataConst.IsInGarageFlag);
                }

                await Task.Run(() => LocationList.Instance.RemovePlayer(player)).ConfigureAwait(false);
            }
            else
            {
                player.Dimension = 0;
                player.Position = new Vector3(dbGarage.LastEntryX, dbGarage.LastEntryY, dbGarage.LastEntryZ);
                player.ResetData(PlayerDataConst.IsInGarageFlag);
            }
        }

        private Task<KeyValuePair<Vector3, Vector3>> GetFreeSpawnPos(GarageSpawnModel spawns)
        {
            return Task.Run(() =>
            {
                try
                {
                    var entities = new List<Entity>();
                    Vector3 spawnPos = null;

                    entities.AddRange(NAPI.Pools.GetAllPlayers());
                    entities.AddRange(NAPI.Pools.GetAllVehicles());

                    foreach (var item in spawns.SpawnList)
                    {
                        spawnPos = item.Key;

                        foreach (var entity in entities)
                        {
                            if (entity.Position.DistanceTo(item.Key) < 5f) spawnPos = null;
                        }

                        if (spawnPos != null) return item;
                    }

                    return new KeyValuePair<Vector3, Vector3>();
                }
                catch (Exception e)
                {
                    Logging.ServerErrorLog($"NextEntity.NextPlayer - {e.Message}");
                    return new KeyValuePair<Vector3, Vector3>();
                }
            });
        }

        private async Task<bool> GetNextSlot(VehicleModel dbVehicle, GarageBigModel dbGarage, MarkDataModel markData, uint playerId)
        {
            int[] slots = null;
            uint dimension = 0;

            switch (markData.MarkName)
            {
                case 1:
                    slots = JsonConvert.DeserializeObject<int[]>(dbGarage.OneVehicleSlots);
                    dimension = VehGarageCfg.DimBig1 + playerId;
                    break;
                case 2:
                    slots = JsonConvert.DeserializeObject<int[]>(dbGarage.TwoVehicleSlots);
                    dimension = VehGarageCfg.DimBig2 + playerId;
                    break;
                default:
                    return false;
            }

            if (slots[0] == -1)
            {
                slots[0] = dbVehicle.Id;
                NAPI.Task.Run(() => new VehicleSpawner().SpawnInGarageVehicle(dbVehicle.Id, dimension,
                    VehGarageCfg.Pos1Big, VehGarageCfg.Rot1Big));
            }
            else if (slots[1] == -1)
            {
                slots[1] = dbVehicle.Id;
                NAPI.Task.Run(() => new VehicleSpawner().SpawnInGarageVehicle(dbVehicle.Id, dimension,
                    VehGarageCfg.Pos2Big, VehGarageCfg.Rot2Big));
            }
            else if (slots[2] == -1)
            {
                slots[2] = dbVehicle.Id;
                NAPI.Task.Run(() => new VehicleSpawner().SpawnInGarageVehicle(dbVehicle.Id, dimension,
                    VehGarageCfg.Pos3Big, VehGarageCfg.Rot3Big));
            }
            else if (slots[3] == -1)
            {
                slots[3] = dbVehicle.Id;
                NAPI.Task.Run(() => new VehicleSpawner().SpawnInGarageVehicle(dbVehicle.Id, dimension,
                    VehGarageCfg.Pos4Big, VehGarageCfg.Rot4Big));
            }
            else if (slots[4] == -1)
            {
                slots[4] = dbVehicle.Id;
                NAPI.Task.Run(() => new VehicleSpawner().SpawnInGarageVehicle(dbVehicle.Id, dimension,
                    VehGarageCfg.Pos5Big, VehGarageCfg.Rot5Big));
            }
            else if (slots[5] == -1)
            {
                slots[5] = dbVehicle.Id;
                NAPI.Task.Run(() => new VehicleSpawner().SpawnInGarageVehicle(dbVehicle.Id, dimension,
                    VehGarageCfg.Pos6Big, VehGarageCfg.Rot6Big));
            }
            else if (slots[6] == -1)
            {
                slots[6] = dbVehicle.Id;
                NAPI.Task.Run(() => new VehicleSpawner().SpawnInGarageVehicle(dbVehicle.Id, dimension,
                    VehGarageCfg.Pos7Big, VehGarageCfg.Rot7Big));
            }
            else if (slots[7] == -1)
            {
                slots[7] = dbVehicle.Id;
                NAPI.Task.Run(() => new VehicleSpawner().SpawnInGarageVehicle(dbVehicle.Id, dimension,
                    VehGarageCfg.Pos8Big, VehGarageCfg.Rot8Big));
            }
            else return false;

            switch (markData.MarkName)
            {
                case 1:
                    dbGarage.OneVehicleSlots = JsonConvert.SerializeObject(slots);
                    break;
                case 2:
                    dbGarage.TwoVehicleSlots = JsonConvert.SerializeObject(slots);
                    break;
            }

            if (!await RolePlayDbController.UpdateGarageBig(dbGarage).ConfigureAwait(false)) return false;
            else return true;
        }
    }
}
