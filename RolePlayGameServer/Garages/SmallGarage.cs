﻿using GTANetworkAPI;
using Newtonsoft.Json;
using RolePlayGameData.Configs.Vehicle;
using RolePlayGameData.Database.RolePlayModels;
using RolePlayGameData.EntityDataConsts;
using RolePlayGameData.Models;
using RolePlayGameServer.Database.Controllers;
using RolePlayGameServer.ServerLists;
using RolePlayGameServer.Util;
using RolePlayGameServer.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RolePlayGameServer.Garages
{
    internal sealed class SmallGarage
    {
        public static SmallGarage Instance => _instance.Value;

        private static SortedDictionary<string, GarageSpawnModel> _entryPoints = new SortedDictionary<string, GarageSpawnModel>();
        private static readonly Lazy<SmallGarage> _instance = new Lazy<SmallGarage>(() => new SmallGarage());

        private SmallGarage() { }

        public async void ToGarage(Client player, MarkDataModel markData)
        {
            player.SetData(PlayerDataConst.IsInGarageFlag, true);

            var playerPos = player.Position;
            var playerVeh = player.Vehicle;
            var playerData = (PlayerAccountModel)player.GetData(PlayerDataConst.PlayerAccount);
            var dbGarage = await RolePlayDbController.GetGarageSmall(player.SocialClubName).ConfigureAwait(false);
            uint dimension = 0;
            bool isOwned = false;
            bool isPayed = false;

            switch (markData.MarkName)
            {
                case 1:
                    dimension = VehGarageCfg.DimS1 + (uint)playerData.Id;
                    isOwned = dbGarage.OneIsOwned;
                    isPayed = dbGarage.OneIsPayed;
                    break;
                case 2:
                    dimension = VehGarageCfg.DimS2 + (uint)playerData.Id;
                    isOwned = dbGarage.TwoIsOwned;
                    isPayed = dbGarage.TwoIsPayed;
                    break;
                case 3:
                    dimension = VehGarageCfg.DimS3 + (uint)playerData.Id;
                    isOwned = dbGarage.ThreeIsOwned;
                    isPayed = dbGarage.ThreeIsPayed;
                    break;
                case 4:
                    dimension = VehGarageCfg.DimS4 + (uint)playerData.Id;
                    isOwned = dbGarage.FourIsOwned;
                    isPayed = dbGarage.FourIsPayed;
                    break;
                case 5:
                    dimension = VehGarageCfg.DimS5 + (uint)playerData.Id;
                    isOwned = dbGarage.FiveIsOwned;
                    isPayed = dbGarage.FiveIsPayed;
                    break;
                case 6:
                    dimension = VehGarageCfg.DimS6 + (uint)playerData.Id;
                    isOwned = dbGarage.SixIsOwned;
                    isPayed = dbGarage.SixIsPayed;
                    break;
                case 7:
                    dimension = VehGarageCfg.DimS7 + (uint)playerData.Id;
                    isOwned = dbGarage.SevenIsOwned;
                    isPayed = dbGarage.SevenIsPayed;
                    break;
                case 8:
                    dimension = VehGarageCfg.DimS8 + (uint)playerData.Id;
                    isOwned = dbGarage.AightIsOwned;
                    isPayed = dbGarage.AightIsPayed;
                    break;
                case 9:
                    dimension = VehGarageCfg.DimS9 + (uint)playerData.Id;
                    isOwned = dbGarage.NineIsOwned;
                    isPayed = dbGarage.NineIsPayed;
                    break;
                case 10:
                    dimension = VehGarageCfg.DimS10 + (uint)playerData.Id;
                    isOwned = dbGarage.TenIsOwned;
                    isPayed = dbGarage.TenIsPayed;
                    break;
                case 11:
                    dimension = VehGarageCfg.DimS11 + (uint)playerData.Id;
                    isOwned = dbGarage.ElevenIsOwned;
                    isPayed = dbGarage.ElevenIsPayed;
                    break;
                case 12:
                    dimension = VehGarageCfg.DimS12 + (uint)playerData.Id;
                    isOwned = dbGarage.TwelveIsOwned;
                    isPayed = dbGarage.TwelveIsPayed;
                    break;
            }

            GarageSpawnModel spawns = null;

            if (VehGarageSpawnCfg.GarageSpawnList.TryGetValue(markData.MarkType, out var spawnData))
            {
                spawns = spawnData.First(x => x.MarkName == markData.MarkName);
            }
            else NAPI.Util.ConsoleOutput($"garageSpawn in vehicleGarageSpawnCfg for markId {markData.MarkName} not found");

            if (_entryPoints.ContainsKey(player.SocialClubName)) _entryPoints.Remove(player.SocialClubName);

            _entryPoints.Add(player.SocialClubName, spawns);

            dbGarage.LastEntryX = playerPos.X;
            dbGarage.LastEntryY = playerPos.Y;
            dbGarage.LastEntryZ = playerPos.Z;

            await RolePlayDbController.UpdateGarageSmall(dbGarage).ConfigureAwait(false);

            if (playerVeh != null)
            {
                var vehData = (VehicleModel)playerVeh.GetData(VehicleDataConst.CivVehicleData);
                var dbvehicle = await RolePlayDbController.GetVehicle(vehData.Id).ConfigureAwait(false);

                if (await GetNextSlot(dbvehicle, dbGarage, markData, (uint)playerData.Id).ConfigureAwait(false))
                {
                    NAPI.Task.Run(playerVeh.Delete);

                    player.Dimension = dimension;
                    player.Position = VehGarageCfg.SpawnPosSmall;
                    player.Rotation = VehGarageCfg.SpawnRotSmall;
                }
                else
                {
                    player.SendNotification("~r~Du hast keinen Platz mehr in der Garage!");
                    return;
                }
            }
            else
            {
                player.Dimension = dimension;
                player.Position = VehGarageCfg.SpawnPosSmall;
                player.Rotation = VehGarageCfg.SpawnRotSmall;
            }
        }

        public async void OutOfGarage(Client player, MarkDataModel markData)
        {
            var vehicle = player.Vehicle;
            var dbGarage = await RolePlayDbController.GetGarageSmall(player.SocialClubName).ConfigureAwait(false);

            if (_entryPoints.TryGetValue(player.SocialClubName, out var spawns))
            {
                var spawnPos = GetFreeSpawnPos(spawns).Result;

                if (spawnPos.Key == default)
                {
                    player.SendNotification("~r~Alle Spawnpunkte sind belegt, bitte warten!");
                    return;
                }

                if (vehicle != null)
                {
                    var vehData = (VehicleModel)vehicle.GetData(VehicleDataConst.CivVehicleData);

                    vehicle.Dimension = 0;
                    vehicle.Position = spawnPos.Key;
                    vehicle.Rotation = spawnPos.Value;

                    await Task.Delay(1000).ConfigureAwait(false);

                    player.Dimension = 0;
                    player.SetIntoVehicle(vehicle, -1);
                    player.ResetData(PlayerDataConst.IsInGarageFlag);

                    dbGarage.LastEntryX = spawnPos.Key.X;
                    dbGarage.LastEntryY = spawnPos.Key.Y;
                    dbGarage.LastEntryZ = spawnPos.Key.Z;

                    var slotsOne = JsonConvert.DeserializeObject<int[]>(dbGarage.OneVehicleSlots);
                    var slotsTwo = JsonConvert.DeserializeObject<int[]>(dbGarage.TwoVehicleSlots);
                    var slotsThree = JsonConvert.DeserializeObject<int[]>(dbGarage.ThreeVehicleSlots);
                    var slotsFour = JsonConvert.DeserializeObject<int[]>(dbGarage.FourVehicleSlots);
                    var slotsFive = JsonConvert.DeserializeObject<int[]>(dbGarage.FiveVehicleSlots);
                    var slotsSix = JsonConvert.DeserializeObject<int[]>(dbGarage.SixVehicleSlots);
                    var slotsSeven = JsonConvert.DeserializeObject<int[]>(dbGarage.SevenVehicleSlots);
                    var slotsAight = JsonConvert.DeserializeObject<int[]>(dbGarage.AightVehicleSlots);
                    var slotsNine = JsonConvert.DeserializeObject<int[]>(dbGarage.NineVehicleSlots);
                    var slotsTen = JsonConvert.DeserializeObject<int[]>(dbGarage.TenVehicleSlots);
                    var slotsEleven = JsonConvert.DeserializeObject<int[]>(dbGarage.ElevenVehicleSlots);
                    var slotsTwelve = JsonConvert.DeserializeObject<int[]>(dbGarage.TwelveVehicleSlots);

                    if (slotsOne.Contains(vehData.Id))
                    {
                        var i = Array.IndexOf(slotsOne, vehData.Id);
                        slotsOne[i] = -1;
                        dbGarage.OneVehicleSlots = JsonConvert.SerializeObject(slotsOne);
                    }
                    else if (slotsTwo.Contains(vehData.Id))
                    {
                        var i = Array.IndexOf(slotsTwo, vehData.Id);
                        slotsTwo[i] = -1;
                        dbGarage.TwoVehicleSlots = JsonConvert.SerializeObject(slotsTwo);
                    }
                    else if (slotsThree.Contains(vehData.Id))
                    {
                        var i = Array.IndexOf(slotsThree, vehData.Id);
                        slotsThree[i] = -1;
                        dbGarage.ThreeVehicleSlots = JsonConvert.SerializeObject(slotsThree);
                    }
                    else if (slotsFour.Contains(vehData.Id))
                    {
                        var i = Array.IndexOf(slotsFour, vehData.Id);
                        slotsFour[i] = -1;
                        dbGarage.FourVehicleSlots = JsonConvert.SerializeObject(slotsFour);
                    }
                    else if (slotsFive.Contains(vehData.Id))
                    {
                        var i = Array.IndexOf(slotsFive, vehData.Id);
                        slotsFive[i] = -1;
                        dbGarage.FiveVehicleSlots = JsonConvert.SerializeObject(slotsFive);
                    }
                    else if (slotsSix.Contains(vehData.Id))
                    {
                        var i = Array.IndexOf(slotsSix, vehData.Id);
                        slotsSix[i] = -1;
                        dbGarage.SixVehicleSlots = JsonConvert.SerializeObject(slotsSix);
                    }
                    else if (slotsSeven.Contains(vehData.Id))
                    {
                        var i = Array.IndexOf(slotsSeven, vehData.Id);
                        slotsSeven[i] = -1;
                        dbGarage.SevenVehicleSlots = JsonConvert.SerializeObject(slotsSeven);
                    }
                    else if (slotsAight.Contains(vehData.Id))
                    {
                        var i = Array.IndexOf(slotsAight, vehData.Id);
                        slotsAight[i] = -1;
                        dbGarage.AightVehicleSlots = JsonConvert.SerializeObject(slotsAight);
                    }
                    else if (slotsNine.Contains(vehData.Id))
                    {
                        var i = Array.IndexOf(slotsNine, vehData.Id);
                        slotsNine[i] = -1;
                        dbGarage.NineVehicleSlots = JsonConvert.SerializeObject(slotsNine);
                    }
                    else if (slotsTen.Contains(vehData.Id))
                    {
                        var i = Array.IndexOf(slotsTen, vehData.Id);
                        slotsTen[i] = -1;
                        dbGarage.TenVehicleSlots = JsonConvert.SerializeObject(slotsTen);
                    }
                    else if (slotsEleven.Contains(vehData.Id))
                    {
                        var i = Array.IndexOf(slotsEleven, vehData.Id);
                        slotsEleven[i] = -1;
                        dbGarage.ElevenVehicleSlots = JsonConvert.SerializeObject(slotsEleven);
                    }
                    else if (slotsTwelve.Contains(vehData.Id))
                    {
                        var i = Array.IndexOf(slotsTwelve, vehData.Id);
                        slotsTwelve[i] = -1;
                        dbGarage.TwelveVehicleSlots = JsonConvert.SerializeObject(slotsTwelve);
                    }
                    else return;

                    await RolePlayDbController.UpdateGarageSmall(dbGarage).ConfigureAwait(false);
                }
                else
                {                  
                    switch (markData.MarkName)
                    {
                        case 1:                            
                            player.Position = VehGarageCfg.ExitSpawnPosS1;
                            player.Rotation = VehGarageCfg.ExitSpawnRotS1;
                            break;
                        case 2:
                            player.Position = VehGarageCfg.ExitSpawnPosS2;
                            player.Rotation = VehGarageCfg.ExitSpawnRotS2;
                            break;
                        case 3:
                            player.Position = VehGarageCfg.ExitSpawnPosS3;
                            player.Rotation = VehGarageCfg.ExitSpawnRotS3;
                            break;
                        case 4:
                            player.Position = VehGarageCfg.ExitSpawnPosS4;
                            player.Rotation = VehGarageCfg.ExitSpawnRotS4;
                            break;
                        case 5:
                            player.Position = VehGarageCfg.ExitSpawnPosS5;
                            player.Rotation = VehGarageCfg.ExitSpawnRotS5;
                            break;
                        case 6:
                            player.Position = VehGarageCfg.ExitSpawnPosS6;
                            player.Rotation = VehGarageCfg.ExitSpawnRotS6;
                            break;
                        case 7:
                            player.Position = VehGarageCfg.ExitSpawnPosS7;
                            player.Rotation = VehGarageCfg.ExitSpawnRotS7;
                            break;
                        case 8:
                            player.Position = VehGarageCfg.ExitSpawnPosS8;
                            player.Rotation = VehGarageCfg.ExitSpawnRotS8;
                            break;
                        case 9:
                            player.Position = VehGarageCfg.ExitSpawnPosS9;
                            player.Rotation = VehGarageCfg.ExitSpawnRotS9;
                            break;
                        case 10:
                            player.Position = VehGarageCfg.ExitSpawnPosS10;
                            player.Rotation = VehGarageCfg.ExitSpawnRotS10;
                            break;
                        case 11:
                            player.Position = VehGarageCfg.ExitSpawnPosS11;
                            player.Rotation = VehGarageCfg.ExitSpawnRotS11;
                            break;
                        case 12:
                            player.Position = VehGarageCfg.ExitSpawnPosS12;
                            player.Rotation = VehGarageCfg.ExitSpawnRotS12;
                            break;
                    }

                    player.Dimension = 0;
                    player.ResetData(PlayerDataConst.IsInGarageFlag);
                }

                await Task.Run(() => LocationList.Instance.RemovePlayer(player)).ConfigureAwait(false);
            }
            else
            {
                player.Dimension = 0;
                player.Position = new Vector3(dbGarage.LastEntryX, dbGarage.LastEntryY, dbGarage.LastEntryZ);
                player.ResetData(PlayerDataConst.IsInGarageFlag);
            }
        }

        private Task<KeyValuePair<Vector3, Vector3>> GetFreeSpawnPos(GarageSpawnModel spawns)
        {
            return Task.Run(() =>
            {
                try
                {
                    var entities = new List<Entity>();
                    Vector3 spawnPos = null;

                    entities.AddRange(NAPI.Pools.GetAllPlayers());
                    entities.AddRange(NAPI.Pools.GetAllVehicles());

                    foreach (var item in spawns.SpawnList)
                    {
                        spawnPos = item.Key;

                        foreach (var entity in entities)
                        {
                            if (entity.Position.DistanceTo(item.Key) < 5f) spawnPos = null;
                        }

                        if (spawnPos != null) return item;
                    }

                    return new KeyValuePair<Vector3, Vector3>();
                }
                catch (Exception e)
                {
                    Logging.ServerErrorLog($"NextEntity.NextPlayer - {e.Message}");
                    return new KeyValuePair<Vector3, Vector3>();
                }
            });
        }

        private async Task<bool> GetNextSlot(VehicleModel dbVehicle, GarageSmallModel dbGarage, MarkDataModel markData, uint playerId)
        {
            int[] slots = null;
            uint dimension = 0;

            switch (markData.MarkName)
            {
                case 1:
                    slots = JsonConvert.DeserializeObject<int[]>(dbGarage.OneVehicleSlots);
                    dimension = VehGarageCfg.DimS1 + playerId;
                    break;
                case 2:
                    slots = JsonConvert.DeserializeObject<int[]>(dbGarage.TwoVehicleSlots);
                    dimension = VehGarageCfg.DimS2 + playerId;
                    break;
                case 3:
                    slots = JsonConvert.DeserializeObject<int[]>(dbGarage.ThreeVehicleSlots);
                    dimension = VehGarageCfg.DimS3 + playerId;
                    break;
                case 4:
                    slots = JsonConvert.DeserializeObject<int[]>(dbGarage.FourVehicleSlots);
                    dimension = VehGarageCfg.DimS4 + playerId;
                    break;
                case 5:
                    slots = JsonConvert.DeserializeObject<int[]>(dbGarage.FiveVehicleSlots);
                    dimension = VehGarageCfg.DimS5 + playerId;
                    break;
                case 6:
                    slots = JsonConvert.DeserializeObject<int[]>(dbGarage.SixVehicleSlots);
                    dimension = VehGarageCfg.DimS6 + playerId;
                    break;
                case 7:
                    slots = JsonConvert.DeserializeObject<int[]>(dbGarage.SevenVehicleSlots);
                    dimension = VehGarageCfg.DimS7 + playerId;
                    break;
                case 8:
                    slots = JsonConvert.DeserializeObject<int[]>(dbGarage.AightVehicleSlots);
                    dimension = VehGarageCfg.DimS8 + playerId;
                    break;
                case 9:
                    slots = JsonConvert.DeserializeObject<int[]>(dbGarage.NineVehicleSlots);
                    dimension = VehGarageCfg.DimS9 + playerId;
                    break;
                case 10:
                    slots = JsonConvert.DeserializeObject<int[]>(dbGarage.TenVehicleSlots);
                    dimension = VehGarageCfg.DimS10 + playerId;
                    break;
                case 11:
                    slots = JsonConvert.DeserializeObject<int[]>(dbGarage.ElevenVehicleSlots);
                    dimension = VehGarageCfg.DimS11 + playerId;
                    break;
                case 12:
                    slots = JsonConvert.DeserializeObject<int[]>(dbGarage.TwelveVehicleSlots);
                    dimension = VehGarageCfg.DimS12 + playerId;
                    break;
                default:
                    return false;
            }

            if (slots[0] == -1)
            {
                slots[0] = dbVehicle.Id;
                NAPI.Task.Run(() => new VehicleSpawner().SpawnInGarageVehicle(dbVehicle.Id, dimension, VehGarageCfg.Pos1Small, VehGarageCfg.Rot1Small));
            }
            else return false;

            switch (markData.MarkName)
            {
                case 1:
                    dbGarage.OneVehicleSlots = JsonConvert.SerializeObject(slots);
                    break;
                case 2:
                    dbGarage.TwoVehicleSlots = JsonConvert.SerializeObject(slots);
                    break;
                case 3:
                    dbGarage.ThreeVehicleSlots = JsonConvert.SerializeObject(slots);
                    break;
                case 4:
                    dbGarage.FourVehicleSlots = JsonConvert.SerializeObject(slots);
                    break;
                case 5:
                    dbGarage.FiveVehicleSlots = JsonConvert.SerializeObject(slots);
                    break;
                case 6:
                    dbGarage.SixVehicleSlots = JsonConvert.SerializeObject(slots);
                    break;
                case 7:
                    dbGarage.SevenVehicleSlots = JsonConvert.SerializeObject(slots);
                    break;
                case 8:
                    dbGarage.AightVehicleSlots = JsonConvert.SerializeObject(slots);
                    break;
                case 9:
                    dbGarage.NineVehicleSlots = JsonConvert.SerializeObject(slots);
                    break;
                case 10:
                    dbGarage.TenVehicleSlots = JsonConvert.SerializeObject(slots);
                    break;
                case 11:
                    dbGarage.ElevenVehicleSlots = JsonConvert.SerializeObject(slots);
                    break;
                case 12:
                    dbGarage.TwelveVehicleSlots = JsonConvert.SerializeObject(slots);
                    break;
            }

            if (!await RolePlayDbController.UpdateGarageSmall(dbGarage).ConfigureAwait(false)) return false;
            else return true;
        }
    }
}