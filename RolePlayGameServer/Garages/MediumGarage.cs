﻿using GTANetworkAPI;
using Newtonsoft.Json;
using RolePlayGameData.Configs.Vehicle;
using RolePlayGameData.Database.RolePlayModels;
using RolePlayGameData.EntityDataConsts;
using RolePlayGameData.Models;
using RolePlayGameServer.Database.Controllers;
using RolePlayGameServer.ServerLists;
using RolePlayGameServer.Util;
using RolePlayGameServer.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RolePlayGameServer.Garages
{
    public class MediumGarage
    {
        public static MediumGarage Instance => _instance.Value;

        private static SortedDictionary<string, GarageSpawnModel> _entryPoints = new SortedDictionary<string, GarageSpawnModel>();
        private static readonly Lazy<MediumGarage> _instance = new Lazy<MediumGarage>(() => new MediumGarage());

        private MediumGarage() { }

        public async void ToGarage(Client player, MarkDataModel markData)
        {
            player.SetData(PlayerDataConst.IsInGarageFlag, true);

            var playerPos = player.Position;
            var playerVeh = player.Vehicle;
            var playerData = (PlayerAccountModel)player.GetData(PlayerDataConst.PlayerAccount);
            var dbGarage = await RolePlayDbController.GetGarageMedium(player.SocialClubName).ConfigureAwait(false);
            uint dimension = 0;
            bool isOwned = false;
            bool isPayed = false;

            switch (markData.MarkName)
            {
                case 1:
                    dimension = VehGarageCfg.DimMed1 + (uint)playerData.Id;
                    isOwned = dbGarage.OneIsOwned;
                    isPayed = dbGarage.OneIsPayed;
                    break;
                case 2:
                    dimension = VehGarageCfg.DimMed2 + (uint)playerData.Id;
                    isOwned = dbGarage.TwoIsOwned;
                    isPayed = dbGarage.TwoIsPayed;
                    break;
                case 3:
                    dimension = VehGarageCfg.DimMed3 + (uint)playerData.Id;
                    isOwned = dbGarage.ThreeIsOwned;
                    isPayed = dbGarage.ThreeIsPayed;
                    break;
                case 4:
                    dimension = VehGarageCfg.DimMed4 + (uint)playerData.Id;
                    isOwned = dbGarage.FourIsOwned;
                    isPayed = dbGarage.FourIsPayed;
                    break;
                case 5:
                    dimension = VehGarageCfg.DimMed5 + (uint)playerData.Id;
                    isOwned = dbGarage.FiveIsOwned;
                    isPayed = dbGarage.FiveIsPayed;
                    break;
                case 6:
                    dimension = VehGarageCfg.DimMed6 + (uint)playerData.Id;
                    isOwned = dbGarage.SixIsOwned;
                    isPayed = dbGarage.SixIsPayed;
                    break;
            }

            GarageSpawnModel spawns = null;

            if (VehGarageSpawnCfg.GarageSpawnList.TryGetValue(markData.MarkType, out var spawnData))
            {
                spawns = spawnData.First(x => x.MarkName == markData.MarkName);
            }
            else NAPI.Util.ConsoleOutput($"garageSpawn in vehicleGarageSpawnCfg for markId {markData.MarkName} not found");

            if (_entryPoints.ContainsKey(player.SocialClubName)) _entryPoints.Remove(player.SocialClubName);

            _entryPoints.Add(player.SocialClubName, spawns);

            dbGarage.LastEntryX = playerPos.X;
            dbGarage.LastEntryY = playerPos.Y;
            dbGarage.LastEntryZ = playerPos.Z;

            await RolePlayDbController.UpdateGarageMedium(dbGarage).ConfigureAwait(false);

            if (playerVeh != null)
            {
                var vehData = (VehicleModel)playerVeh.GetData(VehicleDataConst.CivVehicleData);
                var dbvehicle = await RolePlayDbController.GetVehicle(vehData.Id).ConfigureAwait(false);

                if (await GetNextSlot(dbvehicle, dbGarage, markData, (uint)playerData.Id).ConfigureAwait(false))
                {
                    NAPI.Task.Run(playerVeh.Delete);

                    player.Dimension = dimension;
                    player.Position = VehGarageCfg.SpawnPosMed;
                    player.Rotation = VehGarageCfg.SpawnRotMed;
                }
                else
                {
                    player.SendNotification("~r~Du hast keinen Platz mehr in der Garage!");
                    return;
                }
            }
            else
            {
                player.Dimension = dimension;
                player.Position = VehGarageCfg.SpawnPosMed;
                player.Rotation = VehGarageCfg.SpawnRotMed;
            }
        }

        public async void OutOfGarage(Client player, MarkDataModel markData)
        {
            var vehicle = player.Vehicle;
            var dbGarage = await RolePlayDbController.GetGarageMedium(player.SocialClubName).ConfigureAwait(false);

            if (_entryPoints.TryGetValue(player.SocialClubName, out var spawns))
            {
                var spawnPos = GetFreeSpawnPos(spawns).Result;

                if (spawnPos.Key == default)
                {
                    player.SendNotification("~r~Alle Spawnpunkte sind belegt, bitte warten!");
                    return;
                }

                if (vehicle != null)
                {
                    var vehData = (VehicleModel)vehicle.GetData(VehicleDataConst.CivVehicleData);

                    vehicle.Dimension = 0;
                    vehicle.Position = spawnPos.Key;
                    vehicle.Rotation = spawnPos.Value;

                    await Task.Delay(1000).ConfigureAwait(false);

                    player.Dimension = 0;
                    player.SetIntoVehicle(vehicle, -1);
                    player.ResetData(PlayerDataConst.IsInGarageFlag);

                    dbGarage.LastEntryX = spawnPos.Key.X;
                    dbGarage.LastEntryY = spawnPos.Key.Y;
                    dbGarage.LastEntryZ = spawnPos.Key.Z;

                    var slotsOne = JsonConvert.DeserializeObject<int[]>(dbGarage.OneVehicleSlots);
                    var slotsTwo = JsonConvert.DeserializeObject<int[]>(dbGarage.TwoVehicleSlots);
                    var slotsThree = JsonConvert.DeserializeObject<int[]>(dbGarage.ThreeVehicleSlots);
                    var slotsFour = JsonConvert.DeserializeObject<int[]>(dbGarage.FourVehicleSlots);
                    var slotsFive = JsonConvert.DeserializeObject<int[]>(dbGarage.FiveVehicleSlots);
                    var slotsSix = JsonConvert.DeserializeObject<int[]>(dbGarage.SixVehicleSlots);

                    if (slotsOne.Contains(vehData.Id))
                    {
                        var i = Array.IndexOf(slotsOne, vehData.Id);
                        slotsOne[i] = -1;
                        dbGarage.OneVehicleSlots = JsonConvert.SerializeObject(slotsOne);
                    }
                    else if (slotsTwo.Contains(vehData.Id))
                    {
                        var i = Array.IndexOf(slotsTwo, vehData.Id);
                        slotsTwo[i] = -1;
                        dbGarage.TwoVehicleSlots = JsonConvert.SerializeObject(slotsTwo);
                    }
                    else if (slotsThree.Contains(vehData.Id))
                    {
                        var i = Array.IndexOf(slotsThree, vehData.Id);
                        slotsThree[i] = -1;
                        dbGarage.ThreeVehicleSlots = JsonConvert.SerializeObject(slotsThree);
                    }
                    else if (slotsFour.Contains(vehData.Id))
                    {
                        var i = Array.IndexOf(slotsFour, vehData.Id);
                        slotsFour[i] = -1;
                        dbGarage.FourVehicleSlots = JsonConvert.SerializeObject(slotsFour);
                    }
                    else if (slotsFive.Contains(vehData.Id))
                    {
                        var i = Array.IndexOf(slotsFive, vehData.Id);
                        slotsFive[i] = -1;
                        dbGarage.FiveVehicleSlots = JsonConvert.SerializeObject(slotsFive);
                    }
                    else if (slotsSix.Contains(vehData.Id))
                    {
                        var i = Array.IndexOf(slotsSix, vehData.Id);
                        slotsSix[i] = -1;
                        dbGarage.SixVehicleSlots = JsonConvert.SerializeObject(slotsSix);
                    }
                    else return;

                    await RolePlayDbController.UpdateGarageMedium(dbGarage).ConfigureAwait(false);
                }
                else
                {
                    player.Dimension = 0;

                    switch (markData.MarkName)
                    {
                        case 1:                            
                            player.Position = VehGarageCfg.ExitSpawnPosM1;
                            player.Rotation = VehGarageCfg.ExitSpawnRotM1;
                            break;
                        case 2:
                            player.Position = VehGarageCfg.ExitSpawnPosM2;
                            player.Rotation = VehGarageCfg.ExitSpawnRotM2;
                            break;
                        case 3:
                            player.Position = VehGarageCfg.ExitSpawnPosM3;
                            player.Rotation = VehGarageCfg.ExitSpawnRotM3;
                            break;
                        case 4:
                            player.Position = VehGarageCfg.ExitSpawnPosM4;
                            player.Rotation = VehGarageCfg.ExitSpawnRotM4;
                            break;
                        case 5:
                            player.Position = VehGarageCfg.ExitSpawnPosM5;
                            player.Rotation = VehGarageCfg.ExitSpawnRotM5;
                            break;
                        case 6:
                            player.Position = VehGarageCfg.ExitSpawnPosM6;
                            player.Rotation = VehGarageCfg.ExitSpawnRotM6;
                            break;
                    }

                    player.ResetData(PlayerDataConst.IsInGarageFlag);
                }

                await Task.Run(() => LocationList.Instance.RemovePlayer(player)).ConfigureAwait(false);
            }
            else
            {
                player.Dimension = 0;
                player.Position = new Vector3(dbGarage.LastEntryX, dbGarage.LastEntryY, dbGarage.LastEntryZ);
                player.ResetData(PlayerDataConst.IsInGarageFlag);
            }
        }

        private Task<KeyValuePair<Vector3, Vector3>> GetFreeSpawnPos(GarageSpawnModel spawns)
        {
            return Task.Run(() =>
            {
                try
                {
                    var entities = new List<Entity>();
                    Vector3 spawnPos = null;

                    entities.AddRange(NAPI.Pools.GetAllPlayers());
                    entities.AddRange(NAPI.Pools.GetAllVehicles());

                    foreach (var item in spawns.SpawnList)
                    {
                        spawnPos = item.Key;

                        foreach (var entity in entities)
                        {
                            if (entity.Position.DistanceTo(item.Key) < 5f) spawnPos = null;
                        }

                        if (spawnPos != null) return item;
                    }

                    return new KeyValuePair<Vector3, Vector3>();
                }
                catch (Exception e)
                {
                    Logging.ServerErrorLog($"NextEntity.NextPlayer - {e.Message}");
                    return new KeyValuePair<Vector3, Vector3>();
                }
            });
        }

        private async Task<bool> GetNextSlot(VehicleModel dbVehicle, GarageMediumModel dbGarage, MarkDataModel markData, uint playerId)
        {
            int[] slots = null;
            uint dimension = 0;

            switch (markData.MarkName)
            {
                case 1:
                    slots = JsonConvert.DeserializeObject<int[]>(dbGarage.OneVehicleSlots);
                    dimension = VehGarageCfg.DimMed1 + playerId;
                    break;
                case 2:
                    slots = JsonConvert.DeserializeObject<int[]>(dbGarage.TwoVehicleSlots);
                    dimension = VehGarageCfg.DimMed2 + playerId;
                    break;
                case 3:
                    slots = JsonConvert.DeserializeObject<int[]>(dbGarage.ThreeVehicleSlots);
                    dimension = VehGarageCfg.DimMed3 + playerId;
                    break;
                case 4:
                    slots = JsonConvert.DeserializeObject<int[]>(dbGarage.FourVehicleSlots);
                    dimension = VehGarageCfg.DimMed4 + playerId;
                    break;
                case 5:
                    slots = JsonConvert.DeserializeObject<int[]>(dbGarage.FiveVehicleSlots);
                    dimension = VehGarageCfg.DimMed5 + playerId;
                    break;
                case 6:
                    slots = JsonConvert.DeserializeObject<int[]>(dbGarage.SixVehicleSlots);
                    dimension = VehGarageCfg.DimMed6 + playerId;
                    break;
                default:
                    return false;
            }

            if (slots[0] == -1)
            {
                slots[0] = dbVehicle.Id;
                NAPI.Task.Run(() => new VehicleSpawner().SpawnInGarageVehicle(dbVehicle.Id, dimension, VehGarageCfg.Pos1Med, VehGarageCfg.Rot1Med));
            }
            else if (slots[1] == -1)
            {
                slots[1] = dbVehicle.Id;
                NAPI.Task.Run(() => new VehicleSpawner().SpawnInGarageVehicle(dbVehicle.Id, dimension, VehGarageCfg.Pos2Med, VehGarageCfg.Rot2Med));
            }
            else if (slots[2] == -1)
            {
                slots[2] = dbVehicle.Id;
                NAPI.Task.Run(() => new VehicleSpawner().SpawnInGarageVehicle(dbVehicle.Id, dimension, VehGarageCfg.Pos3Med, VehGarageCfg.Rot3Med));
            }
            else return false;

            switch (markData.MarkName)
            {
                case 1:
                    dbGarage.OneVehicleSlots = JsonConvert.SerializeObject(slots);
                    break;
                case 2:
                    dbGarage.TwoVehicleSlots = JsonConvert.SerializeObject(slots);
                    break;
                case 3:
                    dbGarage.ThreeVehicleSlots = JsonConvert.SerializeObject(slots);
                    break;
                case 4:
                    dbGarage.FourVehicleSlots = JsonConvert.SerializeObject(slots);
                    break;
                case 5:
                    dbGarage.FiveVehicleSlots = JsonConvert.SerializeObject(slots);
                    break;
                case 6:
                    dbGarage.SixVehicleSlots = JsonConvert.SerializeObject(slots);
                    break;
            }

            if (!await RolePlayDbController.UpdateGarageMedium(dbGarage).ConfigureAwait(false)) return false;
            else return true;
        }
    }
}
