﻿using GTANetworkAPI;
using RolePlayGameData.Configs;
using RolePlayGameData.Database.MapMarkModels;
using RolePlayGameData.Enums;
using RolePlayGameData.Models;
using RolePlayGameServer.Database.Controllers;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RolePlayGameServer.ServerLists
{
    public class BlipLists
    {
        private List<MarkPositionModel> AllMarkPositions => GetAllMarksPositions().Result;

        public static List<BlipModel> CivillianBlips { get; private set; } = new List<BlipModel>();

        public void UpdateCivillianBlips()
        {
            foreach (var item in AllMarkPositions)
            {
                if (item.MarkFaction == (uint)FactionEnums.Civ)
                {
                    var cfg = MarkCfg.LocationMarkSettings.First(x => (uint)x.MarkType == item.MarkType);

                    if (cfg.HasMapMark == false) continue;

                    CivillianBlips.Add(new BlipModel
                    {
                        Sprite = cfg.BlipSprite,
                        Position = new Vector3(item.PositionX, item.PositionY, item.PositionZ),
                        Name = cfg.MarkText,
                        Scale = cfg.BlipScale,
                        Color = cfg.BlipColor,
                        Alpha = cfg.BlipAlpha,
                        DrawDistance = cfg.BlipDrawDistance,
                        ShortRange = cfg.BlipShortRange,
                        Rotation = cfg.BlipRotation,
                        Dimension = item.Dimension
                    });
                }
            }
        }

        private async Task<List<MarkPositionModel>> GetAllMarksPositions() =>
            await MapObjectDbController.GetMarkPositionListFromDb().ConfigureAwait(false);
    }
}
