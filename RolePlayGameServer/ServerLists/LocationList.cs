﻿using GTANetworkAPI;
using RolePlayGameData.Models;
using System;
using System.Collections.Generic;

namespace RolePlayGameServer.ServerLists
{
    public class LocationList
    {
        public static SortedDictionary<string, MarkDataModel> LocationPlayers { get; } =
            new SortedDictionary<string, MarkDataModel>();

        private static readonly Lazy<LocationList> _instance = new Lazy<LocationList>(() => new LocationList());

        private LocationList() { }

        public static LocationList Instance => _instance.Value;

        public void AddPlayer(Client player, MarkDataModel markData)
        {
            if (LocationPlayers.ContainsKey(player.SocialClubName)) LocationPlayers[player.SocialClubName] = markData;
            else LocationPlayers.Add(player.SocialClubName, markData);
        }

        public void RemovePlayer(Client player)
        {
            if (LocationPlayers.ContainsKey(player.SocialClubName)) LocationPlayers.Remove(player.SocialClubName);
        }
    }
}