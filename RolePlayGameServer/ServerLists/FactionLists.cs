﻿using System;
using System.Collections.Generic;
using GTANetworkAPI;
using RolePlayGameData.Enums;

namespace RolePlayGameServer.ServerLists
{
    public sealed class FactionLists
    {
        public static SortedDictionary<string, Client> AllCiv = new SortedDictionary<string, Client>();
        public static SortedDictionary<string, Client> AllLsc = new SortedDictionary<string, Client>();
        public static SortedDictionary<string, Client> AllLsrs = new SortedDictionary<string, Client>();
        public static SortedDictionary<string, Client> AllLspd = new SortedDictionary<string, Client>();
        public static SortedDictionary<string, Client> AllFib = new SortedDictionary<string, Client>();
        public static SortedDictionary<string, Client> AllLssd = new SortedDictionary<string, Client>();
        public static SortedDictionary<string, Client> AllDoj = new SortedDictionary<string, Client>();
        public static SortedDictionary<string, Client> AllDcr = new SortedDictionary<string, Client>();

        private static readonly Lazy<FactionLists> _instance = new Lazy<FactionLists>(() => new FactionLists());

        private FactionLists() { }

        public static FactionLists Instance => _instance.Value;

        public bool AddToList(Client player, FactionEnums faction)
        {
            ClearPlayerEntries(player);

            switch (faction)
            {
                case FactionEnums.Civ:
                    if (!AllCiv.ContainsKey(player.SocialClubName)) AllCiv.Add(player.SocialClubName, player);
                    break;
                case FactionEnums.Lspd:
                    if (!AllLspd.ContainsKey(player.SocialClubName)) AllLspd.Add(player.SocialClubName, player);
                    break;
                case FactionEnums.Lssd:
                    if (!AllLssd.ContainsKey(player.SocialClubName)) AllLssd.Add(player.SocialClubName, player);
                    break;
                case FactionEnums.Fib:
                    if (!AllFib.ContainsKey(player.SocialClubName)) AllFib.Add(player.SocialClubName, player);
                    break;
                case FactionEnums.Lsrs:
                    if (!AllLsrs.ContainsKey(player.SocialClubName)) AllLsrs.Add(player.SocialClubName, player);
                    break;
                case FactionEnums.Lsc:
                    if (!AllLsc.ContainsKey(player.SocialClubName)) AllLsc.Add(player.SocialClubName, player);
                    break;
                case FactionEnums.Doj:
                    if (!AllDoj.ContainsKey(player.SocialClubName)) AllDoj.Add(player.SocialClubName, player);
                    break;
                case FactionEnums.Dcr:
                    if (!AllDcr.ContainsKey(player.SocialClubName)) AllDcr.Add(player.SocialClubName, player);
                    break;
                default:
                    return false;
            }

            return true;
        }

        private static void ClearPlayerEntries(Client player)
        {
            if (AllCiv.ContainsKey(player.SocialClubName)) AllCiv.Remove(player.SocialClubName);
            if (AllLsc.ContainsKey(player.SocialClubName)) AllLsc.Remove(player.SocialClubName);
            if (AllLsrs.ContainsKey(player.SocialClubName)) AllLsrs.Remove(player.SocialClubName);
            if (AllLspd.ContainsKey(player.SocialClubName)) AllLspd.Remove(player.SocialClubName);
            if (AllFib.ContainsKey(player.SocialClubName)) AllFib.Remove(player.SocialClubName);
            if (AllLssd.ContainsKey(player.SocialClubName)) AllLssd.Remove(player.SocialClubName);
            if (AllDoj.ContainsKey(player.SocialClubName)) AllDoj.Remove(player.SocialClubName);
            if (AllDcr.ContainsKey(player.SocialClubName)) AllDcr.Remove(player.SocialClubName);
        }
    }
}