﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RolePlayGameServer.Database.Migrations.RolePlay
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                "BankFleeca",
                table => new
                {
                    Id = table.Column<int>()
                        .Annotation("SqlServer:ValueGenerationStrategy",
                            SqlServerValueGenerationStrategy.IdentityColumn),
                    SocialClub = table.Column<string>(),
                    IsAccountActive = table.Column<bool>(),
                    IsCreditCardActive = table.Column<bool>(),
                    PinCode = table.Column<string>(maxLength: 64),
                    AccountBalance = table.Column<float>(),
                    CreditCardBalance = table.Column<float>()
                },
                constraints: table => { table.PrimaryKey("PK_BankFleeca", x => x.Id); });

            migrationBuilder.CreateTable(
                "BankMaze",
                table => new
                {
                    Id = table.Column<int>()
                        .Annotation("SqlServer:ValueGenerationStrategy",
                            SqlServerValueGenerationStrategy.IdentityColumn),
                    SocialClub = table.Column<string>(),
                    IsAccountActive = table.Column<bool>(),
                    IsCreditCardActive = table.Column<bool>(),
                    PinCode = table.Column<string>(maxLength: 64),
                    AccountBalance = table.Column<float>(),
                    CreditCardBalance = table.Column<float>()
                },
                constraints: table => { table.PrimaryKey("PK_BankMaze", x => x.Id); });

            migrationBuilder.CreateTable(
                "BankUnion",
                table => new
                {
                    Id = table.Column<int>()
                        .Annotation("SqlServer:ValueGenerationStrategy",
                            SqlServerValueGenerationStrategy.IdentityColumn),
                    SocialClub = table.Column<string>(),
                    IsAccountActive = table.Column<bool>(),
                    IsCreditCardActive = table.Column<bool>(),
                    PinCode = table.Column<string>(maxLength: 64),
                    AccountBalance = table.Column<float>(),
                    CreditCardBalance = table.Column<float>()
                },
                constraints: table => { table.PrimaryKey("PK_BankUnion", x => x.Id); });

            migrationBuilder.CreateTable(
                "PlayerAccounts",
                table => new
                {
                    Id = table.Column<int>()
                        .Annotation("SqlServer:ValueGenerationStrategy",
                            SqlServerValueGenerationStrategy.IdentityColumn),
                    SocialClubName = table.Column<string>(maxLength: 64),
                    UserMail = table.Column<string>(maxLength: 64),
                    UserPass = table.Column<string>(maxLength: 64),
                    AdminLevel = table.Column<int>(),
                    SupportLevel = table.Column<int>(),
                    LscLevel = table.Column<int>(),
                    LsrsLevel = table.Column<int>(),
                    LspdLevel = table.Column<int>(),
                    FibLevel = table.Column<int>(),
                    LssdLevel = table.Column<int>(),
                    DojLevel = table.Column<int>(),
                    DcrLevel = table.Column<int>()
                },
                constraints: table => { table.PrimaryKey("PK_PlayerAccounts", x => x.Id); });

            migrationBuilder.CreateTable(
                "PlayerBiology",
                table => new
                {
                    Id = table.Column<int>()
                        .Annotation("SqlServer:ValueGenerationStrategy",
                            SqlServerValueGenerationStrategy.IdentityColumn),
                    SocialClub = table.Column<string>(),
                    Thirst = table.Column<float>(),
                    Hunger = table.Column<float>()
                },
                constraints: table => { table.PrimaryKey("PK_PlayerBiology", x => x.Id); });

            migrationBuilder.CreateTable(
                "PlayerCharSettings",
                table => new
                {
                    Id = table.Column<int>()
                        .Annotation("SqlServer:ValueGenerationStrategy",
                            SqlServerValueGenerationStrategy.IdentityColumn),
                    SocialClub = table.Column<string>(),
                    FirstHeadShape = table.Column<int>(),
                    SecondHeadShape = table.Column<int>(),
                    FirstSkinTone = table.Column<int>(),
                    SecondSkinTone = table.Column<int>(),
                    HeadMix = table.Column<float>(),
                    SkinMix = table.Column<float>(),
                    HairModel = table.Column<int>(),
                    FirstHairColor = table.Column<int>(),
                    SecondHairColor = table.Column<int>(),
                    BeardModel = table.Column<int>(),
                    BeardColor = table.Column<int>(),
                    ChestModel = table.Column<int>(),
                    ChestColor = table.Column<int>(),
                    BlemishesModel = table.Column<int>(),
                    BodyBlemishes = table.Column<int>(),
                    AgingModel = table.Column<int>(),
                    ComplexionModel = table.Column<int>(),
                    SunDamageModel = table.Column<int>(),
                    FrecklesModel = table.Column<int>(),
                    NoseWidth = table.Column<float>(),
                    NoseHeight = table.Column<float>(),
                    NoseLength = table.Column<float>(),
                    NoseBridge = table.Column<float>(),
                    NoseTip = table.Column<float>(),
                    NoseShift = table.Column<float>(),
                    BrowHeight = table.Column<float>(),
                    BrowWidth = table.Column<float>(),
                    CheekboneHeight = table.Column<float>(),
                    CheekboneWidth = table.Column<float>(),
                    CheeksWidth = table.Column<float>(),
                    Eyes = table.Column<float>(),
                    Lips = table.Column<float>(),
                    JawWidth = table.Column<float>(),
                    JawHeight = table.Column<float>(),
                    ChinLength = table.Column<float>(),
                    ChinPosition = table.Column<float>(),
                    ChinWidth = table.Column<float>(),
                    ChinShape = table.Column<float>(),
                    NeckWidth = table.Column<float>(),
                    EyesColor = table.Column<int>(),
                    EyebrowsModel = table.Column<int>(),
                    EyebrowsColor = table.Column<int>(),
                    MakeupModel = table.Column<int>(),
                    BlushModel = table.Column<int>(),
                    BlushColor = table.Column<int>(),
                    LipstickModel = table.Column<int>(),
                    LipstickColor = table.Column<int>()
                },
                constraints: table => { table.PrimaryKey("PK_PlayerCharSettings", x => x.Id); });

            migrationBuilder.CreateTable(
                "PlayerInvItems",
                table => new
                {
                    Id = table.Column<int>()
                        .Annotation("SqlServer:ValueGenerationStrategy",
                            SqlServerValueGenerationStrategy.IdentityColumn),
                    SocialClub = table.Column<string>(),
                    Item = table.Column<long>(),
                    Amount = table.Column<long>()
                },
                constraints: table => { table.PrimaryKey("PK_PlayerInvItems", x => x.Id); });

            migrationBuilder.CreateTable(
                "PlayerMoney",
                table => new
                {
                    Id = table.Column<int>()
                        .Annotation("SqlServer:ValueGenerationStrategy",
                            SqlServerValueGenerationStrategy.IdentityColumn),
                    SocialClub = table.Column<string>(),
                    CashMoney = table.Column<float>(),
                    BackUpMoney = table.Column<float>(),
                    SocialPaymentBank = table.Column<int>(),
                    StatePaymentBank = table.Column<int>()
                },
                constraints: table => { table.PrimaryKey("PK_PlayerMoney", x => x.Id); });

            migrationBuilder.CreateTable(
                "PlayerParticulars",
                table => new
                {
                    Id = table.Column<int>()
                        .Annotation("SqlServer:ValueGenerationStrategy",
                            SqlServerValueGenerationStrategy.IdentityColumn),
                    SocialClub = table.Column<string>(),
                    HasIdentityCard = table.Column<bool>(),
                    FirstName = table.Column<string>(maxLength: 64),
                    LastName = table.Column<string>(maxLength: 64),
                    Birthday = table.Column<string>(maxLength: 64),
                    Nationality = table.Column<string>(maxLength: 64),
                    EyeColor = table.Column<string>(maxLength: 64),
                    Height = table.Column<string>()
                },
                constraints: table => { table.PrimaryKey("PK_PlayerParticulars", x => x.Id); });

            migrationBuilder.CreateTable(
                "PlayerPaychecks",
                table => new
                {
                    Id = table.Column<int>()
                        .Annotation("SqlServer:ValueGenerationStrategy",
                            SqlServerValueGenerationStrategy.IdentityColumn),
                    SocialClub = table.Column<string>(),
                    SocialPayMinutes = table.Column<long>(),
                    SocialPayIsActive = table.Column<bool>(),
                    InCivActive = table.Column<bool>(),
                    LscMinutes = table.Column<long>(),
                    InLscActive = table.Column<bool>(),
                    LsrsMinutes = table.Column<long>(),
                    InLsrsActive = table.Column<bool>(),
                    LspdMinutes = table.Column<long>(),
                    InLspdActive = table.Column<bool>(),
                    FibMinutes = table.Column<long>(),
                    InFibActive = table.Column<bool>(),
                    LssdMinutes = table.Column<long>(),
                    InLssdActive = table.Column<bool>(),
                    DojMinutes = table.Column<long>(),
                    InDojActive = table.Column<bool>(),
                    DcrMinutes = table.Column<long>(),
                    InDcrActive = table.Column<bool>(),
                    LastPayment = table.Column<DateTime>()
                },
                constraints: table => { table.PrimaryKey("PK_PlayerPaychecks", x => x.Id); });

            migrationBuilder.CreateTable(
                "PlayerPhones",
                table => new
                {
                    Id = table.Column<int>()
                        .Annotation("SqlServer:ValueGenerationStrategy",
                            SqlServerValueGenerationStrategy.IdentityColumn),
                    SocialClub = table.Column<string>(),
                    HasPhone = table.Column<bool>(),
                    IsPhoneActive = table.Column<bool>(),
                    PhoneNumber = table.Column<long>()
                },
                constraints: table => { table.PrimaryKey("PK_PlayerPhones", x => x.Id); });

            migrationBuilder.CreateTable(
                "PlayerPositions",
                table => new
                {
                    Id = table.Column<int>()
                        .Annotation("SqlServer:ValueGenerationStrategy",
                            SqlServerValueGenerationStrategy.IdentityColumn),
                    SocialClub = table.Column<string>(),
                    IsDead = table.Column<bool>(),
                    TimeUntilRespawn = table.Column<long>(),
                    LastPosX = table.Column<float>(),
                    LastPosY = table.Column<float>(),
                    LastPosZ = table.Column<float>(),
                    Rotation = table.Column<float>(),
                    Dimension = table.Column<long>()
                },
                constraints: table => { table.PrimaryKey("PK_PlayerPositions", x => x.Id); });

            migrationBuilder.CreateTable(
                "PlayerRecipeBook",
                table => new
                {
                    Id = table.Column<int>()
                        .Annotation("SqlServer:ValueGenerationStrategy",
                            SqlServerValueGenerationStrategy.IdentityColumn),
                    SocialClub = table.Column<string>(),
                    RecipeBook = table.Column<string>()
                },
                constraints: table => { table.PrimaryKey("PK_PlayerRecipeBook", x => x.Id); });

            migrationBuilder.CreateTable(
                "PlayerWeapons",
                table => new
                {
                    Id = table.Column<int>()
                        .Annotation("SqlServer:ValueGenerationStrategy",
                            SqlServerValueGenerationStrategy.IdentityColumn),
                    SocialClub = table.Column<string>(),
                    Weapon = table.Column<long>(),
                    Amount = table.Column<long>()
                },
                constraints: table => { table.PrimaryKey("PK_PlayerWeapons", x => x.Id); });

            migrationBuilder.CreateTable(
                "ServerPlayerCounts",
                table => new
                {
                    Id = table.Column<int>()
                        .Annotation("SqlServer:ValueGenerationStrategy",
                            SqlServerValueGenerationStrategy.IdentityColumn),
                    CivCount = table.Column<long>(),
                    LscCount = table.Column<long>(),
                    LsrsCount = table.Column<long>(),
                    LspdCount = table.Column<long>(),
                    FibCount = table.Column<long>(),
                    LssdCount = table.Column<long>(),
                    DojCount = table.Column<long>(),
                    DcrCount = table.Column<long>()
                },
                constraints: table => { table.PrimaryKey("PK_ServerPlayerCounts", x => x.Id); });

            migrationBuilder.CreateTable(
                "ServerPlayerWhiteList",
                table => new
                {
                    Id = table.Column<string>(),
                    SocialClubName = table.Column<string>(maxLength: 64),
                    ListingDate = table.Column<DateTime>(),
                    LastLogin = table.Column<DateTime>(),
                    LastIp = table.Column<string>(),
                    IsOnline = table.Column<bool>(),
                    BannedUntil = table.Column<DateTime>(),
                    BanReason = table.Column<string>(maxLength: 64)
                },
                constraints: table => { table.PrimaryKey("PK_ServerPlayerWhiteList", x => x.Id); });

            migrationBuilder.CreateTable(
                "VehicleFuel",
                table => new
                {
                    Id = table.Column<int>()
                        .Annotation("SqlServer:ValueGenerationStrategy",
                            SqlServerValueGenerationStrategy.IdentityColumn),
                    VehicleId = table.Column<int>(),
                    TankStatus = table.Column<float>(),
                    EngineStatus = table.Column<bool>()
                },
                constraints: table => { table.PrimaryKey("PK_VehicleFuel", x => x.Id); });

            migrationBuilder.CreateTable(
                "VehicleInvItems",
                table => new
                {
                    Id = table.Column<int>()
                        .Annotation("SqlServer:ValueGenerationStrategy",
                            SqlServerValueGenerationStrategy.IdentityColumn),
                    VehicleId = table.Column<int>(),
                    Item = table.Column<long>(),
                    Amount = table.Column<long>()
                },
                constraints: table => { table.PrimaryKey("PK_VehicleInvItems", x => x.Id); });

            migrationBuilder.CreateTable(
                "VehicleKeys",
                table => new
                {
                    Id = table.Column<int>()
                        .Annotation("SqlServer:ValueGenerationStrategy",
                            SqlServerValueGenerationStrategy.IdentityColumn),
                    VehicleId = table.Column<int>(),
                    FirstKeyPlayerSocial = table.Column<string>(),
                    SecondKeyPlayerIdSocial = table.Column<string>(),
                    ThirdKeyPlayerIdSocial = table.Column<string>()
                },
                constraints: table => { table.PrimaryKey("PK_VehicleKeys", x => x.Id); });

            migrationBuilder.CreateTable(
                "VehiclePositions",
                table => new
                {
                    Id = table.Column<int>()
                        .Annotation("SqlServer:ValueGenerationStrategy",
                            SqlServerValueGenerationStrategy.IdentityColumn),
                    VehicleId = table.Column<int>(),
                    LastPosX = table.Column<float>(),
                    LastPosY = table.Column<float>(),
                    LastPosZ = table.Column<float>(),
                    Rotation = table.Column<float>(),
                    Dimension = table.Column<long>()
                },
                constraints: table => { table.PrimaryKey("PK_VehiclePositions", x => x.Id); });

            migrationBuilder.CreateTable(
                "Vehicles",
                table => new
                {
                    Id = table.Column<int>()
                        .Annotation("SqlServer:ValueGenerationStrategy",
                            SqlServerValueGenerationStrategy.IdentityColumn),
                    SocialClub = table.Column<string>(),
                    VehicleHash = table.Column<long>(),
                    Faction = table.Column<long>(),
                    LockStatus = table.Column<bool>(),
                    PlateText = table.Column<string>(maxLength: 64),
                    BuyDate = table.Column<DateTime>()
                },
                constraints: table => { table.PrimaryKey("PK_Vehicles", x => x.Id); });

            migrationBuilder.CreateTable(
                "VehicleSettings",
                table => new
                {
                    Id = table.Column<int>()
                        .Annotation("SqlServer:ValueGenerationStrategy",
                            SqlServerValueGenerationStrategy.IdentityColumn),
                    VehicleId = table.Column<int>(),
                    FirstColor = table.Column<int>(),
                    SecondColor = table.Column<int>(),
                    Pearlescent = table.Column<bool>(),
                    Spoiler = table.Column<int>(),
                    FrontBumper = table.Column<int>(),
                    RearBumper = table.Column<int>(),
                    SideSkirt = table.Column<int>(),
                    Exhaust = table.Column<int>(),
                    Frame = table.Column<int>(),
                    Grille = table.Column<int>(),
                    Hood = table.Column<int>(),
                    Fender = table.Column<int>(),
                    RightFender = table.Column<int>(),
                    Roof = table.Column<int>(),
                    Engine = table.Column<int>(),
                    Brakes = table.Column<int>(),
                    Transmission = table.Column<int>(),
                    Horn = table.Column<int>(),
                    Suspension = table.Column<int>(),
                    Armor = table.Column<int>(),
                    Turbo = table.Column<int>(),
                    Xenon = table.Column<int>(),
                    FrontWheels = table.Column<int>(),
                    BackWheels = table.Column<int>(),
                    UtilShadowSilver = table.Column<int>(),
                    PlateHolders = table.Column<int>(),
                    TrimDesign = table.Column<int>(),
                    Ornaments = table.Column<int>(),
                    DialDesign = table.Column<int>(),
                    SteeringWheel = table.Column<int>(),
                    ShiftLever = table.Column<int>(),
                    Plaques = table.Column<int>(),
                    Hydraulics = table.Column<int>(),
                    Livery = table.Column<int>(),
                    Plate = table.Column<int>(),
                    WindowTint = table.Column<int>()
                },
                constraints: table => { table.PrimaryKey("PK_VehicleSettings", x => x.Id); });

            migrationBuilder.CreateTable(
                "GarageBig",
                table => new
                {
                    Id = table.Column<int>()
                        .Annotation("SqlServer:ValueGenerationStrategy",
                            SqlServerValueGenerationStrategy.IdentityColumn),
                    SocialClub = table.Column<string>(),
                    Dimension = table.Column<long>(),
                    IsOwned = table.Column<bool>(),
                    IsPayed = table.Column<bool>(),
                    VehicleOnSlot1Id = table.Column<int>(nullable: true),
                    VehicleOnSlot2Id = table.Column<int>(nullable: true),
                    VehicleOnSlot3Id = table.Column<int>(nullable: true),
                    VehicleOnSlot4Id = table.Column<int>(nullable: true),
                    VehicleOnSlot5Id = table.Column<int>(nullable: true),
                    VehicleOnSlot6Id = table.Column<int>(nullable: true),
                    VehicleOnSlot7Id = table.Column<int>(nullable: true),
                    VehicleOnSlot8Id = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GarageBig", x => x.Id);
                    table.ForeignKey(
                        "FK_GarageBig_Vehicles_VehicleOnSlot1Id",
                        x => x.VehicleOnSlot1Id,
                        "Vehicles",
                        "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        "FK_GarageBig_Vehicles_VehicleOnSlot2Id",
                        x => x.VehicleOnSlot2Id,
                        "Vehicles",
                        "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        "FK_GarageBig_Vehicles_VehicleOnSlot3Id",
                        x => x.VehicleOnSlot3Id,
                        "Vehicles",
                        "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        "FK_GarageBig_Vehicles_VehicleOnSlot4Id",
                        x => x.VehicleOnSlot4Id,
                        "Vehicles",
                        "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        "FK_GarageBig_Vehicles_VehicleOnSlot5Id",
                        x => x.VehicleOnSlot5Id,
                        "Vehicles",
                        "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        "FK_GarageBig_Vehicles_VehicleOnSlot6Id",
                        x => x.VehicleOnSlot6Id,
                        "Vehicles",
                        "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        "FK_GarageBig_Vehicles_VehicleOnSlot7Id",
                        x => x.VehicleOnSlot7Id,
                        "Vehicles",
                        "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        "FK_GarageBig_Vehicles_VehicleOnSlot8Id",
                        x => x.VehicleOnSlot8Id,
                        "Vehicles",
                        "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                "GarageMedium",
                table => new
                {
                    Id = table.Column<int>()
                        .Annotation("SqlServer:ValueGenerationStrategy",
                            SqlServerValueGenerationStrategy.IdentityColumn),
                    SocialClub = table.Column<string>(),
                    Dimension = table.Column<long>(),
                    IsOwned = table.Column<bool>(),
                    IsPayed = table.Column<bool>(),
                    VehicleOnSlot1Id = table.Column<int>(nullable: true),
                    VehicleOnSlot2Id = table.Column<int>(nullable: true),
                    VehicleOnSlot3Id = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GarageMedium", x => x.Id);
                    table.ForeignKey(
                        "FK_GarageMedium_Vehicles_VehicleOnSlot1Id",
                        x => x.VehicleOnSlot1Id,
                        "Vehicles",
                        "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        "FK_GarageMedium_Vehicles_VehicleOnSlot2Id",
                        x => x.VehicleOnSlot2Id,
                        "Vehicles",
                        "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        "FK_GarageMedium_Vehicles_VehicleOnSlot3Id",
                        x => x.VehicleOnSlot3Id,
                        "Vehicles",
                        "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                "GarageSmall",
                table => new
                {
                    Id = table.Column<int>()
                        .Annotation("SqlServer:ValueGenerationStrategy",
                            SqlServerValueGenerationStrategy.IdentityColumn),
                    SocialClub = table.Column<string>(),
                    Dimension = table.Column<long>(),
                    IsOwned = table.Column<bool>(),
                    IsPayed = table.Column<bool>(),
                    VehicleOnSlot1Id = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GarageSmall", x => x.Id);
                    table.ForeignKey(
                        "FK_GarageSmall_Vehicles_VehicleOnSlot1Id",
                        x => x.VehicleOnSlot1Id,
                        "Vehicles",
                        "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                "IX_GarageBig_VehicleOnSlot1Id",
                "GarageBig",
                "VehicleOnSlot1Id");

            migrationBuilder.CreateIndex(
                "IX_GarageBig_VehicleOnSlot2Id",
                "GarageBig",
                "VehicleOnSlot2Id");

            migrationBuilder.CreateIndex(
                "IX_GarageBig_VehicleOnSlot3Id",
                "GarageBig",
                "VehicleOnSlot3Id");

            migrationBuilder.CreateIndex(
                "IX_GarageBig_VehicleOnSlot4Id",
                "GarageBig",
                "VehicleOnSlot4Id");

            migrationBuilder.CreateIndex(
                "IX_GarageBig_VehicleOnSlot5Id",
                "GarageBig",
                "VehicleOnSlot5Id");

            migrationBuilder.CreateIndex(
                "IX_GarageBig_VehicleOnSlot6Id",
                "GarageBig",
                "VehicleOnSlot6Id");

            migrationBuilder.CreateIndex(
                "IX_GarageBig_VehicleOnSlot7Id",
                "GarageBig",
                "VehicleOnSlot7Id");

            migrationBuilder.CreateIndex(
                "IX_GarageBig_VehicleOnSlot8Id",
                "GarageBig",
                "VehicleOnSlot8Id");

            migrationBuilder.CreateIndex(
                "IX_GarageMedium_VehicleOnSlot1Id",
                "GarageMedium",
                "VehicleOnSlot1Id");

            migrationBuilder.CreateIndex(
                "IX_GarageMedium_VehicleOnSlot2Id",
                "GarageMedium",
                "VehicleOnSlot2Id");

            migrationBuilder.CreateIndex(
                "IX_GarageMedium_VehicleOnSlot3Id",
                "GarageMedium",
                "VehicleOnSlot3Id");

            migrationBuilder.CreateIndex(
                "IX_GarageSmall_VehicleOnSlot1Id",
                "GarageSmall",
                "VehicleOnSlot1Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                "BankFleeca");

            migrationBuilder.DropTable(
                "BankMaze");

            migrationBuilder.DropTable(
                "BankUnion");

            migrationBuilder.DropTable(
                "GarageBig");

            migrationBuilder.DropTable(
                "GarageMedium");

            migrationBuilder.DropTable(
                "GarageSmall");

            migrationBuilder.DropTable(
                "PlayerAccounts");

            migrationBuilder.DropTable(
                "PlayerBiology");

            migrationBuilder.DropTable(
                "PlayerCharSettings");

            migrationBuilder.DropTable(
                "PlayerInvItems");

            migrationBuilder.DropTable(
                "PlayerMoney");

            migrationBuilder.DropTable(
                "PlayerParticulars");

            migrationBuilder.DropTable(
                "PlayerPaychecks");

            migrationBuilder.DropTable(
                "PlayerPhones");

            migrationBuilder.DropTable(
                "PlayerPositions");

            migrationBuilder.DropTable(
                "PlayerRecipeBook");

            migrationBuilder.DropTable(
                "PlayerWeapons");

            migrationBuilder.DropTable(
                "ServerPlayerCounts");

            migrationBuilder.DropTable(
                "ServerPlayerWhiteList");

            migrationBuilder.DropTable(
                "VehicleFuel");

            migrationBuilder.DropTable(
                "VehicleInvItems");

            migrationBuilder.DropTable(
                "VehicleKeys");

            migrationBuilder.DropTable(
                "VehiclePositions");

            migrationBuilder.DropTable(
                "VehicleSettings");

            migrationBuilder.DropTable(
                "Vehicles");
        }
    }
}