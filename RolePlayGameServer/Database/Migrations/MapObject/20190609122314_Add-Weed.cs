﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RolePlayGameServer.Database.Migrations.MapObject
{
    public partial class AddWeed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                "WeedPlants",
                table => new
                {
                    Id = table.Column<int>()
                        .Annotation("SqlServer:ValueGenerationStrategy",
                            SqlServerValueGenerationStrategy.IdentityColumn),
                    Type = table.Column<long>(),
                    Gender = table.Column<bool>(),
                    GrowBooster = table.Column<bool>(),
                    BudBooster = table.Column<bool>(),
                    FertilizerK = table.Column<bool>(),
                    FertilizerM = table.Column<bool>(),
                    FertilizerS = table.Column<bool>(),
                    PosX = table.Column<float>(),
                    PosY = table.Column<float>(),
                    PosZ = table.Column<float>(),
                    PlantTime = table.Column<DateTime>()
                },
                constraints: table => { table.PrimaryKey("PK_WeedPlants", x => x.Id); });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                "WeedPlants");
        }
    }
}