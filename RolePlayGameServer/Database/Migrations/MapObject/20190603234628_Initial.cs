﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RolePlayGameServer.Database.Migrations.MapObject
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                "MarkPositions",
                table => new
                {
                    Id = table.Column<int>()
                        .Annotation("SqlServer:ValueGenerationStrategy",
                            SqlServerValueGenerationStrategy.IdentityColumn),
                    MarkFaction = table.Column<long>(),
                    MarkType = table.Column<long>(),
                    MarkName = table.Column<long>(),
                    PositionX = table.Column<float>(),
                    PositionY = table.Column<float>(),
                    PositionZ = table.Column<float>(),
                    RotationX = table.Column<float>(),
                    RotationY = table.Column<float>(),
                    RotationZ = table.Column<float>(),
                    Dimension = table.Column<long>()
                },
                constraints: table => { table.PrimaryKey("PK_MarkPositions", x => x.Id); });

            migrationBuilder.CreateTable(
                "ShopSlotPositions",
                table => new
                {
                    Id = table.Column<int>()
                        .Annotation("SqlServer:ValueGenerationStrategy",
                            SqlServerValueGenerationStrategy.IdentityColumn),
                    MarkName = table.Column<long>(),
                    IsActive = table.Column<bool>(),
                    VehicleHash = table.Column<long>(),
                    FirstColor = table.Column<long>(),
                    SecondColor = table.Column<long>(),
                    CreationDate = table.Column<DateTime>()
                },
                constraints: table => { table.PrimaryKey("PK_ShopSlotPositions", x => x.Id); });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                "MarkPositions");

            migrationBuilder.DropTable(
                "ShopSlotPositions");
        }
    }
}