﻿using Microsoft.EntityFrameworkCore;
using RolePlayGameData.Database.RolePlayModels;

namespace RolePlayGameServer.Database.Contexts
{
    public class RolePlayContext : DbContext
    {
        public DbSet<AuctionModel> Auctions { get; set; }
        public DbSet<AuctionBufferModel> AuctionBuffer { get; set; }

        public DbSet<BankFleecaModel> BankFleeca { get; set; }
        public DbSet<BankMazeModel> BankMaze { get; set; }
        public DbSet<BankUnionModel> BankUnion { get; set; }

        public DbSet<GarageTruckModel> GarageTruck { get; set; }
        public DbSet<GarageBigModel> GarageBig { get; set; }
        public DbSet<GarageMediumModel> GarageMedium { get; set; }
        public DbSet<GarageSmallModel> GarageSmall { get; set; }

        public DbSet<MarketSystemModel> MarketSystemPrices { get; set; }

        public DbSet<PlayerAccountModel> PlayerAccounts { get; set; }
        public DbSet<PlayerBiologyModel> PlayerBiology { get; set; }
        public DbSet<PlayerCharSettingModel> PlayerCharSettings { get; set; }
        public DbSet<PlayerCookingModel> PlayerCookings { get; set; }
        public DbSet<PlayerInvItemModel> PlayerInvItems { get; set; }
        public DbSet<PlayerMoneyModel> PlayerMoney { get; set; }
        public DbSet<PlayerParticularModel> PlayerParticulars { get; set; }
        public DbSet<PlayerPaycheckModel> PlayerPaychecks { get; set; }
        public DbSet<PlayerPhoneModel> PlayerPhones { get; set; }
        public DbSet<PlayerPositionModel> PlayerPositions { get; set; }
        public DbSet<PlayerRecipeBookModel> PlayerRecipeBook { get; set; }
        public DbSet<PlayerWeaponModel> PlayerWeapons { get; set; }

        public DbSet<ServerPlayerCountModel> ServerPlayerCounts { get; set; }
        public DbSet<ServerPlayerWhiteListModel> ServerPlayerWhiteList { get; set; }

        public DbSet<VehicleFuelModel> VehicleFuel { get; set; }
        public DbSet<VehicleInvItemModel> VehicleInvItems { get; set; }
        public DbSet<VehicleKeyModel> VehicleKeys { get; set; }
        public DbSet<VehicleModel> Vehicles { get; set; }
        public DbSet<VehiclePositionModel> VehiclePositions { get; set; }
        public DbSet<VehicleSettingModel> VehicleSettings { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySql("server=127.0.0.1;database=roleplay_db;user=root;password=K3vIn#403500%");
        }
    }
}

//optionsBuilder.UseSqlServer(@"Server=localhost\SQLEXPRESS;Database=RolePlayDb;Trusted_Connection=True;");
//optionsBuilder.UseSqlServer(@"Server=94.16.115.227;Database=RolePlayDb;User Id=whooza;Password=K3vIn#403500%;");