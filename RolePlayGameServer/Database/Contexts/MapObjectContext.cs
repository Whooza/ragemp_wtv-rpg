﻿using Microsoft.EntityFrameworkCore;
using RolePlayGameData.Database.MapMarkModels;

namespace RolePlayGameServer.Database.Contexts
{
    public class MapObjectContext : DbContext
    {
        public DbSet<BagModel> Bags { get; set; }
        public DbSet<DumpsterModel> Dumpsters { get; set; }
        public DbSet<MarkPositionModel> MarkPositions { get; set; }
        public DbSet<ShopSlotPosModel> ShopSlotPositions { get; set; }
        public DbSet<WeedPlantModel> WeedPlants { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySql("server=127.0.0.1;database=mapobject_db;user=root;password=K3vIn#403500%");
        }
    }
}

//optionsBuilder.UseSqlServer(@"Server=localhost\SQLEXPRESS;Database=MapObjectDb;Trusted_Connection=True;");
//optionsBuilder.UseSqlServer(@"Server=94.16.115.227;Database=MapObjectDb;User Id=whooza;Password=K3vIn#403500%;");