﻿using RolePlayGameData.Configs;
using RolePlayGameData.Configs.Vehicle;
using RolePlayGameData.Enums;
using RolePlayGameServer.Database.Contexts;
using RolePlayGameServer.Util;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RolePlayGameServer.Database.Controllers
{
    public class DbDefaultController
    {
        public async Task InitDbDefaultAsync()
        {
            try
            {
#if DEBUG
                Logging.DatabaseInfoLog($"starting {this}");
#endif
                await SetDefaultEntriesAsync().ConfigureAwait(false);
                await SetAllShopSlotMarks().ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"InitDbDefaultAsync - {e.Message}");
            }
        }

        private async Task SetDefaultEntriesAsync()
        {
            using (var db = new RolePlayContext())
            {
                // auction //
                if (!db.Auctions.Any()) await SetDefaultAuctionAsync().ConfigureAwait(false);
                if (!db.AuctionBuffer.Any()) await SetDefaultAuctionBufferAsync().ConfigureAwait(false);
                // bank //
                if (!db.BankFleeca.Any()) await SetDefaultFleecaBankAsync().ConfigureAwait(false);
                if (!db.BankMaze.Any()) await SetDefaultMazeBankAsync().ConfigureAwait(false);
                if (!db.BankUnion.Any()) await SetDefaultUnionBankAsync().ConfigureAwait(false);
                // garages //
                if (!db.GarageTruck.Any()) await SetDefaultTruckGarageAsync().ConfigureAwait(false);
                if (!db.GarageBig.Any()) await SetDefaultBigGarageAsync().ConfigureAwait(false);
                if (!db.GarageMedium.Any()) await SetDefaultMediumGarageAsync().ConfigureAwait(false);
                if (!db.GarageSmall.Any()) await SetDefaultSmallGarageAsync().ConfigureAwait(false);
                // marketSystem //
                if (!db.MarketSystemPrices.Any()) await SetDefaultMarketSystemPrices().ConfigureAwait(false);
                // player //
                if (!db.PlayerAccounts.Any()) await SetDefaultPlayerAccAsync().ConfigureAwait(false);
                if (!db.PlayerBiology.Any()) await SetDefaultPlayerBiologyAsync().ConfigureAwait(false);
                if (!db.PlayerCharSettings.Any()) await SetDefaultPlayerCharSettingsAsync().ConfigureAwait(false);
                if (!db.PlayerCookings.Any()) await SetDefaultPlayerCookingAsync().ConfigureAwait(false);
                if (!db.PlayerInvItems.Any()) await SetDefaultPlayerInventoryItemAsync().ConfigureAwait(false);
                if (!db.PlayerMoney.Any()) await SetDefaultPlayerMoneyAsync().ConfigureAwait(false);
                if (!db.PlayerParticulars.Any()) await SetDefaultPlayerParticularAsync().ConfigureAwait(false);
                if (!db.PlayerPaychecks.Any()) await SetDefaultPlayerPayAsync().ConfigureAwait(false);
                if (!db.PlayerPositions.Any()) await SetDefaultPlayerPosAsync().ConfigureAwait(false);
                if (!db.PlayerRecipeBook.Any()) await SetDefaultPlayerRecipeBookAsync().ConfigureAwait(false);
                if (!db.PlayerWeapons.Any()) await SetDefaultPlayerWeaponAsync().ConfigureAwait(false);
                // vehicle //
                if (!db.VehicleFuel.Any()) await SetDefaultVehicleFuelAsync().ConfigureAwait(false);
                if (!db.VehicleInvItems.Any()) await SetDefaultVehicleInventoryItemAsync().ConfigureAwait(false);
                if (!db.VehicleKeys.Any()) await SetDefaultVehicleKeyAsync().ConfigureAwait(false);
                if (!db.Vehicles.Any()) await SetDefaultVehicleAsync().ConfigureAwait(false);
                if (!db.VehiclePositions.Any()) await SetDefaultVehiclePositionsAsync().ConfigureAwait(false);
                if (!db.VehicleSettings.Any()) await SetDefaultVehicleSettingsAsync().ConfigureAwait(false);
                // server //
                if (!db.ServerPlayerCounts.Any()) await SetDefaultPlayerCountAsync().ConfigureAwait(false);
                if (!db.ServerPlayerWhiteList.Any()) await SetDefaultPlayerWhiteListAsync().ConfigureAwait(false);
            }

            using (var db = new MapObjectContext())
            {
                // mapMarks //
                if (!db.MarkPositions.Any()) await SetDefaultMarkPosAsync().ConfigureAwait(false);
                // vehicleShop //
                if (!db.ShopSlotPositions.Any()) await SetDefaultVehicleShopAsync().ConfigureAwait(false);
            }
        }

        #region SetDefaultsForMapMarks

        private async Task SetDefaultMarkPosAsync()
        {
            try
            {
                await MapObjectDbController.SetMarkPosInDb((uint)FactionEnums.Civ, (uint)MarkTypeEnums.CivGouvernment,
                    (uint)MarkNameEnums.NoNameNeeded, -429.1249f, 1110.852f, 327.6966f, 0, 0, 0, 0).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{this} - {e.Message}");
            }
        }

        #endregion

        #region SetDefaultAuctions

        private async Task SetDefaultAuctionBufferAsync()
        {
            await RolePlayDbController.SetAuctionBuffer("default", 1, 1).ConfigureAwait(false);
        }

        private async Task SetDefaultAuctionAsync()
        {
            await RolePlayDbController.SetAuction("default", 1, 1, 0f, false, 0f, 0).ConfigureAwait(false);
        }

        #endregion

        #region SetDafaultForBanks

        private async Task SetDefaultFleecaBankAsync()
        {
            try
            {
                await RolePlayDbController.SetBankFleeca("default").ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{this} - {e.Message}");
            }
        }

        private async Task SetDefaultMazeBankAsync()
        {
            try
            {
                await RolePlayDbController.SetBankMaze("default").ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{this} - {e.Message}");
            }
        }

        private async Task SetDefaultUnionBankAsync()
        {
            try
            {
                await RolePlayDbController.SetBankUnion("default").ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{this} - {e.Message}");
            }
        }

        #endregion

        #region SetDefaultsForGarages

        private async Task SetDefaultTruckGarageAsync()
        {
            try
            {
                await RolePlayDbController.SetGarageTruckByParams("default", 1).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{this} - {e.Message}");
            }
        }

        private async Task SetDefaultBigGarageAsync()
        {
            try
            {
                await RolePlayDbController.SetGarageBigByParams("default", 1).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{this} - {e.Message}");
            }
        }

        private async Task SetDefaultMediumGarageAsync()
        {
            try
            {
                await RolePlayDbController.SetGarageMediumByParams("default", 1).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{this} - {e.Message}");
            }
        }

        private async Task SetDefaultSmallGarageAsync()
        {
            try
            {
                await RolePlayDbController.SetGarageSmallByParams("default", 1).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{this} - {e.Message}");
            }
        }

        #endregion

        #region SetDefaultforMarketPrices

        private async Task SetDefaultMarketSystemPrices()
        {
            try
            {
                foreach (var item in ItemCfg.ItemConfig)
                {
                    if (item.MinPrice == item.MaxPrice) continue;

                    await RolePlayDbController.SetCurrentPrice((uint)item.ItemName, (uint)item.ItemType, item.DefaultPrice)
                        .ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{this} - {e.Message}");
            }
        }

        #endregion

        #region SetDefaultsForPlayer

        private async Task SetDefaultPlayerAccAsync()
        {
            try
            {
                await RolePlayDbController.SetPlayerAccByParams("default", "default", "default").ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{this} - {e.Message}");
            }
        }

        private async Task SetDefaultPlayerBiologyAsync()
        {
            try
            {
                await RolePlayDbController.SetPlayerBioByParams("default").ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{this} - {e.Message}");
            }
        }

        private async Task SetDefaultPlayerCharSettingsAsync()
        {
            try
            {
                await RolePlayDbController.SetPlayerCharSettings("default").ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{this} - {e.Message}");
            }
        }

        private async Task SetDefaultPlayerCookingAsync()
        {
            try
            {
                await RolePlayDbController.SetPlayerCooking("default").ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{this} - {e.Message}");
            }
        }

        private async Task SetDefaultPlayerInventoryItemAsync()
        {
            try
            {
                await RolePlayDbController.SetPlayerInvItemInDb("default", (uint)ItemNameEnums.Water, 1).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{this} - {e.Message}");
            }
        }

        private async Task SetDefaultPlayerMoneyAsync()
        {
            try
            {
                await RolePlayDbController.SetPlayerMoney("default", 2500f).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{this} - {e.Message}");
            }
        }

        private async Task SetDefaultPlayerParticularAsync()
        {
            try
            {
                await RolePlayDbController.SetPlayerParticular("default", false, "default", "default", "default", "default",
                    "default", "1,85", true).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{this} - {e.Message}");
            }
        }

        private async Task SetDefaultPlayerPayAsync()
        {
            try
            {
                await RolePlayDbController.SetPlayerPaycheck("default").ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{this} - {e.Message}");
            }
        }

        private async Task SetDefaultPlayerPosAsync()
        {
            try
            {
                await RolePlayDbController.SetPlayerPosInDb("default", 0, 0, 0, 0, 0).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{this} - {e.Message}");
            }
        }

        private async Task SetDefaultPlayerRecipeBookAsync()
        {
            try
            {
                await RolePlayDbController.SetPlayerRecipeBook("default").ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{this} - {e.Message}");
            }
        }

        private async Task SetDefaultPlayerWeaponAsync()
        {
            try
            {
                await RolePlayDbController.SetPlayerWeapon("default", (uint)WeaponHashes.Firework, 3).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{this} - {e.Message}");
            }
        }

        #endregion

        #region SetDefaultForServer

        private async Task SetDefaultPlayerCountAsync()
        {
            try
            {
                await RolePlayDbController.SetServerPlayerCount().ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{this} - {e.Message}");
            }
        }

        private async Task SetDefaultPlayerWhiteListAsync()
        {
            try
            {
                await RolePlayDbController.SetServerWhiteListEntry("xXWhoozaXx").ConfigureAwait(false);
                await RolePlayDbController.SetServerWhiteListEntry("OttoPlays").ConfigureAwait(false);
                await RolePlayDbController.SetServerWhiteListEntry("Verminator_TV").ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{this} - {e.Message}");
            }
        }

        #endregion

        #region SetDefaultsForShop

        private async Task SetDefaultVehicleShopAsync()
        {
            try
            {
                await MapObjectDbController.SetShopSlot((uint)MarkNameEnums.NoNameNeeded, true, (uint)VehicleHashEnums.adder,
                    1, 1, DateTime.Now).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{this} - {e.Message}");
            }
        }

        private async Task SetAllShopSlotMarks()
        {
            try
            {
                var allShopSlotsinDb = await MapObjectDbController.GetVehicleShopPosListFromDb().ConfigureAwait(false);

                foreach (var item in VehShopMarkCfg.VehicleShopMarkPositions)
                {
                    if (item.MarkName == MarkNameEnums.NoNameNeeded) continue;

                    try
                    {
                        allShopSlotsinDb.First(x => x.MarkName == (uint)item.MarkName);
                    }
                    catch
                    {
                        await MapObjectDbController.SetShopSlot((uint)item.MarkName, false, 0, 1, 1, DateTime.Now)
                            .ConfigureAwait(false);
                    }
                }
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{this} - {e.Message}");
            }
        }

        #endregion

        #region SetDefaudltsForVehicle

        private async Task SetDefaultVehicleFuelAsync()
        {
            try
            {
                await RolePlayDbController.SetVehicleFuelInDb(1, (uint)VehicleHashEnums.panto).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{this} - {e.Message}");
            }
        }

        private async Task SetDefaultVehicleInventoryItemAsync()
        {
            try
            {
                await RolePlayDbController.SetVehicleInventoryItem(1, (uint)ItemNameEnums.ToolKit, 1).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{this} - {e.Message}");
            }
        }

        private async Task SetDefaultVehicleKeyAsync()
        {
            try
            {
                await RolePlayDbController.SetVehicleKeyInDb(1, "default").ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{this} - {e.Message}");
            }
        }

        private async Task SetDefaultVehicleAsync()
        {
            try
            {
                await RolePlayDbController.SetVehicle("default", (uint)VehicleHashEnums.panto, (uint)FactionEnums.Civ,
                    100.0f, DateTime.Now).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{this} - {e.Message}");
            }
        }

        private async Task SetDefaultVehiclePositionsAsync()
        {
            try
            {
                await RolePlayDbController.SetVehiclePosInDb(1, 0f, 0f, 0f, 0f, 1).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{this} - {e.Message}");
            }
        }

        private async Task SetDefaultVehicleSettingsAsync()
        {
            try
            {
                await RolePlayDbController.SetVehicleSettingsInDb(1, 1, 1).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{this} - {e.Message}");
            }
        }

        #endregion
    }
}