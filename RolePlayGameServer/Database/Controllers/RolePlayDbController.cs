﻿using GTANetworkAPI;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using RolePlayGameData.Configs;
using RolePlayGameData.Configs.Vehicle;
using RolePlayGameData.Database.RolePlayModels;
using RolePlayGameData.EntityDataConsts;
using RolePlayGameData.Models;
using RolePlayGameServer.Database.Contexts;
using RolePlayGameServer.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace RolePlayGameServer.Database.Controllers
{
    public static class RolePlayDbController
    {
         #region Auctions

        public static async Task<bool> SetAuction(string owner, uint itemName, uint amount, float startPrice,
            bool isInstantBuy, float instantBuyPrice, uint duration)
        {
            try
            {
                var newAuction = new AuctionModel
                {
                    IsActive = true,
                    Owner = owner,
                    ItemName = itemName,
                    Amount = amount,
                    StartPrice = startPrice,
                    IsInstantBuy = isInstantBuy,
                    InstantBuyPrice = instantBuyPrice,
                    Duration = duration,
                    EndTime = DateTime.Now.AddDays(duration),
                    OldTenderer = JsonConvert.SerializeObject(new SortedDictionary<string, float>())
                };

                using (var db = new RolePlayContext())
                {
                    await db.Auctions.AddAsync(newAuction).ConfigureAwait(false);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        public static async Task<AuctionModel> GetAuction(int auctionId)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    return await db.Auctions.FirstAsync(x => x.Id == auctionId).ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return null;
            }
        }

        public static async Task<bool> UpdateAuction(AuctionModel auction)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    db.Auctions.Update(auction);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                    return true;
                }
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        #endregion

        #region AuctionBuffer

        public static async Task<bool> SetAuctionBuffer(string owner, uint itemName, uint amount)
        {
            try
            {
                var newAuctionBuffer = new AuctionBufferModel
                {
                    Owner = owner,
                    ItemName = itemName,
                    Amount = amount,
                    StorageTime = DateTime.Now
                };

                using (var db = new RolePlayContext())
                {
                    await db.AuctionBuffer.AddAsync(newAuctionBuffer).ConfigureAwait(false);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        public static async Task<AuctionBufferModel> GetAuctionBuffer(int auctionBufferId)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    return await db.AuctionBuffer.FirstAsync(x => x.Id == auctionBufferId).ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return null;
            }
        }

        public static async Task<bool> UpdateAuctionBuffer(AuctionBufferModel auctionBuffer)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    db.AuctionBuffer.Update(auctionBuffer);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                    return true;
                }
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        #endregion

        #region GarageBig

        public static async Task<bool> SetGarageTruckByParams(string social, int accountId)
        {
            try
            {
                var newGarage = new GarageTruckModel
                {
                    SocialClub = social,
                    Dimension = (uint)accountId,
                    OneIsOwned = false,
                    TwoIsOwned = false,
                    OneIsPayed = false,
                    TwoIsPayed = false,
                    OneVehicleSlots = JsonConvert.SerializeObject(new int[]{ -1, -1, -1 }),
                    TwoVehicleSlots = JsonConvert.SerializeObject(new int[]{ -1, -1, -1 })
                };

                using (var db = new RolePlayContext())
                {
                    await db.GarageTruck.AddAsync(newGarage).ConfigureAwait(false);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        public static async Task<GarageTruckModel> GetGarageTruck(string social)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    return await db.GarageTruck.FirstAsync(x => x.SocialClub == social).ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return null;
            }
        }

        public static async Task<bool> UpdateGarageTruck(GarageTruckModel garage)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    db.GarageTruck.Update(garage);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                    return true;
                }
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        #endregion

        #region GarageBig

        public static async Task<bool> SetGarageBigByParams(string social, int accountId)
        {
            try
            {
                var newGarage = new GarageBigModel
                {
                    SocialClub = social,
                    Dimension = (uint)accountId,
                    OneIsOwned = false,
                    TwoIsOwned = false,
                    OneIsPayed = false,
                    TwoIsPayed = false,
                    OneVehicleSlots = JsonConvert.SerializeObject(new int[]{ -1, -1, -1, -1, -1, -1, -1, -1 }),
                    TwoVehicleSlots = JsonConvert.SerializeObject(new int[]{ -1, -1, -1, -1, -1, -1, -1, -1 })
                };

                using (var db = new RolePlayContext())
                {
                    await db.GarageBig.AddAsync(newGarage).ConfigureAwait(false);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        public static async Task<GarageBigModel> GetGarageBig(string social)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    return await db.GarageBig.FirstAsync(x => x.SocialClub == social).ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return null;
            }
        }

        public static async Task<bool> UpdateGarageBig(GarageBigModel garage)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    db.GarageBig.Update(garage);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                    return true;
                }
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        #endregion

        #region GarageMedium

        public static async Task<bool> SetGarageMediumByParams(string social, int accountId)
        {
            try
            {
                var newGarage = new GarageMediumModel
                {
                    SocialClub = social,
                    Dimension = (uint)accountId,
                    OneIsOwned = false,
                    TwoIsOwned = false,
                    ThreeIsOwned = false,
                    FourIsOwned = false,
                    FiveIsOwned = false,
                    SixIsOwned = false,
                    OneIsPayed = false,
                    TwoIsPayed = false,
                    ThreeIsPayed = false,
                    FourIsPayed = false,
                    FiveIsPayed = false,
                    SixIsPayed = false,
                    OneVehicleSlots = JsonConvert.SerializeObject(new int[]{ -1, -1, -1 }),
                    TwoVehicleSlots = JsonConvert.SerializeObject(new int[]{ -1, -1, -1 }),
                    ThreeVehicleSlots = JsonConvert.SerializeObject(new int[]{ -1, -1, -1 }),
                    FourVehicleSlots = JsonConvert.SerializeObject(new int[]{ -1, -1, -1 }),
                    FiveVehicleSlots = JsonConvert.SerializeObject(new int[]{ -1, -1, -1 }),
                    SixVehicleSlots = JsonConvert.SerializeObject(new int[]{ -1, -1, -1 })
                };

                using (var db = new RolePlayContext())
                {
                    await db.GarageMedium.AddAsync(newGarage).ConfigureAwait(false);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        public static async Task<GarageMediumModel> GetGarageMedium(string social)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    return await db.GarageMedium.FirstAsync(x => x.SocialClub == social).ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return null;
            }
        }

        public static async Task<bool> UpdateGarageMedium(GarageMediumModel garage)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    db.GarageMedium.Update(garage);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                    return true;
                }
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        #endregion

        #region GarageSmall

        public static async Task<bool> SetGarageSmallByParams(string social, int accountId)
        {
            try
            {
                var newGarage = new GarageSmallModel
                {
                    SocialClub = social,
                    Dimension = (uint)accountId,
                    OneIsOwned = false,
                    TwoIsOwned = false,
                    ThreeIsOwned = false,
                    FourIsOwned = false,
                    FiveIsOwned = false,
                    SixIsOwned = false,
                    SevenIsOwned = false,
                    AightIsOwned = false,
                    NineIsOwned = false,
                    TenIsOwned = false,
                    ElevenIsOwned = false,
                    TwelveIsOwned = false,
                    OneIsPayed = false,
                    TwoIsPayed = false,
                    ThreeIsPayed = false,
                    FourIsPayed = false,
                    FiveIsPayed = false,
                    SixIsPayed = false,
                    SevenIsPayed = false,
                    AightIsPayed = false,
                    NineIsPayed = false,
                    TenIsPayed = false,
                    ElevenIsPayed = false,
                    TwelveIsPayed = false,
                    OneVehicleSlots = JsonConvert.SerializeObject(new int[]{ -1 }),
                    TwoVehicleSlots = JsonConvert.SerializeObject(new int[]{ -1 }),
                    ThreeVehicleSlots = JsonConvert.SerializeObject(new int[]{ -1 }),
                    FourVehicleSlots = JsonConvert.SerializeObject(new int[]{ -1 }),
                    FiveVehicleSlots = JsonConvert.SerializeObject(new int[]{ -1 }),
                    SixVehicleSlots = JsonConvert.SerializeObject(new int[]{ -1 }),
                    SevenVehicleSlots = JsonConvert.SerializeObject(new int[]{ -1 }),
                    AightVehicleSlots = JsonConvert.SerializeObject(new int[]{ -1 }),
                    NineVehicleSlots = JsonConvert.SerializeObject(new int[]{ -1 }),
                    TenVehicleSlots = JsonConvert.SerializeObject(new int[]{ -1 }),
                    ElevenVehicleSlots = JsonConvert.SerializeObject(new int[]{ -1 }),
                    TwelveVehicleSlots = JsonConvert.SerializeObject(new int[]{ -1 })
                };

                using (var db = new RolePlayContext())
                {
                    await db.GarageSmall.AddAsync(newGarage).ConfigureAwait(false);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        public static async Task<GarageSmallModel> GetGarageSmall(string social)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    return await db.GarageSmall.FirstAsync(x => x.SocialClub == social).ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return null;
            }
        }

        public static async Task<bool> UpdateGarageSmall(GarageSmallModel garage)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    db.GarageSmall.Update(garage);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                    return true;
                }
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        #endregion

        #region ServerPlayerCount

        public static async Task<bool> SetServerPlayerCount()
        {
            try
            {
                var newPlayerCount = new ServerPlayerCountModel
                {
                    CivCount = 0,
                    LscCount = 0,
                    LsrsCount = 0,
                    LspdCount = 0,
                    FibCount = 0,
                    LssdCount = 0,
                    DojCount = 0,
                    DcrCount = 0
                };

                using (var db = new RolePlayContext())
                {
                    await db.ServerPlayerCounts.AddAsync(newPlayerCount).ConfigureAwait(false);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        #endregion

        #region BankFleeca

        public static async Task<BankFleecaModel> GetBankFleeca(string social)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    return await db.BankFleeca.FirstAsync(x => x.SocialClub == social).ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return null;
            }
        }

        public static async Task<bool> SetBankFleeca(string social)
        {
            try
            {
                var newBank = new BankFleecaModel
                {
                    SocialClub = social,
                    IsAccountActive = false,
                    IsCreditCardActive = false,
                    PinCode = string.Empty,
                    AccountBalance = 0.0f,
                    CreditCardBalance = 0.0f
                };

                using (var db = new RolePlayContext())
                {
                    await db.BankFleeca.AddAsync(newBank).ConfigureAwait(false);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        public static async Task<bool> UpdateBankFleeca(BankFleecaModel fleeca)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    db.BankFleeca.Update(fleeca);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        #endregion

        #region BankMaze

        public static async Task<BankMazeModel> GetBankMaze(string social)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    return await db.BankMaze.FirstAsync(x => x.SocialClub == social).ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return null;
            }
        }

        public static async Task SetBankMaze(string social)
        {
            try
            {
                var newBank = new BankMazeModel
                {
                    SocialClub = social,
                    IsAccountActive = false,
                    IsCreditCardActive = false,
                    PinCode = string.Empty,
                    AccountBalance = 0.0f,
                    CreditCardBalance = 0.0f
                };

                using (var db = new RolePlayContext())
                {
                    await db.BankMaze.AddAsync(newBank).ConfigureAwait(false);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
            }
        }

        public static async Task UpdateBankMaze(BankMazeModel maze)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    db.BankMaze.Update(maze);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
            }
        }

        #endregion

        #region BankUnion

        public static async Task<BankUnionModel> GetBankUnion(string social)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    return await db.BankUnion.FirstAsync(x => x.SocialClub == social).ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return null;
            }
        }

        public static async Task SetBankUnion(string social)
        {
            try
            {
                var newBank = new BankUnionModel
                {
                    SocialClub = social,
                    IsAccountActive = false,
                    IsCreditCardActive = false,
                    PinCode = string.Empty,
                    AccountBalance = 0.0f,
                    CreditCardBalance = 0.0f
                };

                using (var db = new RolePlayContext())
                {
                    await db.BankUnion.AddAsync(newBank).ConfigureAwait(false);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
            }
        }

        public static async Task UpdateBankUnion(BankUnionModel union)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    db.BankUnion.Update(union);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
            }
        }

        #endregion

        #region PlayerAccount

        public static async Task<PlayerAccountModel> GetPlayerAccBySocial(string social)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    return await db.PlayerAccounts.FirstAsync(x => x.SocialClubName == social).ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return null;
            }
        }

        public static async Task<bool> SetPlayerAccByParams(string social, string mail, string userPass)
        {
            try
            {
                var newAccount = new PlayerAccountModel
                {
                    SocialClubName = social,
                    UserMail = mail,
                    UserPass = userPass,
                    AdminLevel = 0,
                    SupportLevel = 0,
                    LscLevel = 0,
                    LsrsLevel = 0,
                    LspdLevel = 0,
                    FibLevel = 0,
                    LssdLevel = 0,
                    DojLevel = 0,
                    DcrLevel = 0,
                    CharCreatorComplete = false
                };

                using (var db = new RolePlayContext())
                {
                    await db.PlayerAccounts.AddAsync(newAccount).ConfigureAwait(false);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        public static async Task<bool> UpdatePlayerAcc(PlayerAccountModel account)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    db.PlayerAccounts.Update(account);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        public static async Task<bool> DeletePlayerAcc(PlayerAccountModel account)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    db.PlayerAccounts.Remove(account);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        #endregion

        #region PlayerBiology

        public static async Task<PlayerBiologyModel> GetPlayerBioFromDb(string social)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    return await db.PlayerBiology.FirstAsync(x => x.SocialClub == social).ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return null;
            }
        }

        public static async Task<bool> SetPlayerBioByParams(string social)
        {
            try
            {
                var newBiology = new PlayerBiologyModel
                {
                    SocialClub = social,
                    Thirst = 100.0f,
                    Hunger = 100.0f
                };

                using (var db = new RolePlayContext())
                {
                    await db.PlayerBiology.AddAsync(newBiology).ConfigureAwait(false);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        public static async Task<bool> UpdatePlayerBioInDb(PlayerBiologyModel biology)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    db.PlayerBiology.Update(biology);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        #endregion

        #region PlayerCharSetting

        public static async Task<PlayerCharSettingModel> GetPlayerCharSettings(string social)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    return await db.PlayerCharSettings.FirstAsync(x => x.SocialClub == social).ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return null;
            }
        }

        public static async Task<bool> SetPlayerCharSettings(string social)
        {
            try
            {
                var newCharSettings = new PlayerCharSettingModel
                {
                    SocialClub = social,
                    FirstHeadShape = 0,
                    SecondHeadShape = 0,
                    FirstSkinTone = 0,
                    SecondSkinTone = 0,
                    HeadMix = 0.5f,
                    SkinMix = 0.5f,
                    HairModel = 0,
                    FirstHairColor = 0,
                    SecondHairColor = 0,
                    BeardModel = 255,
                    BeardColor = 0,
                    ChestModel = 255,
                    ChestColor = 0,
                    BlemishesModel = 255,
                    AgingModel = 255,
                    ComplexionModel = 255,
                    SunDamageModel = 255,
                    FrecklesModel = 255,
                    EyesColor = 0,
                    EyebrowsModel = 255,
                    EyebrowsColor = 0,
                    MakeupModel = 255,
                    BlushModel = 255,
                    BlushColor = 0,
                    LipstickModel = 255,
                    LipstickColor = 0,
                    NoseWidth = 0.0f,
                    NoseHeight = 0.0f,
                    NoseLength = 0.0f,
                    NoseBridge = 0.0f,
                    NoseTip = 0.0f,
                    NoseShift = 0.0f,
                    BrowHeight = 0.0f,
                    BrowWidth = 0.0f,
                    CheekboneHeight = 0.0f,
                    CheekboneWidth = 0.0f,
                    CheeksWidth = 0.0f,
                    Eyes = 0.0f,
                    Lips = 0.0f,
                    JawWidth = 0.0f,
                    JawHeight = 0.0f,
                    ChinLength = 0.0f,
                    ChinPosition = 0.0f,
                    ChinWidth = 0.0f,
                    ChinShape = 0.0f,
                    NeckWidth = 0.0f
                };

                using (var db = new RolePlayContext())
                {
                    await db.PlayerCharSettings.AddAsync(newCharSettings).ConfigureAwait(false);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        public static async Task<bool> UpdatePlayerCharSettings(PlayerCharSettingModel settings)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    db.PlayerCharSettings.Update(settings);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                    return true;
                }
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        #endregion

        #region PlayerCooking

        public static async Task<PlayerCookingModel> GetPlayerCooking(string social)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    return await db.PlayerCookings.FirstAsync(x => x.SocialClub == social).ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return null;
            }
        }

        public static async Task<bool> SetPlayerCooking(string social)
        {
            try
            {
                var newCooking = new PlayerCookingModel
                {
                    SocialClub = social,
                    CookingPoints = 0f
                };

                using (var db = new RolePlayContext())
                {
                    await db.PlayerCookings.AddAsync(newCooking).ConfigureAwait(false);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        public static async Task<bool> UpdatePlayerCooking(PlayerCookingModel cooking)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    db.PlayerCookings.Update(cooking);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        #endregion

        #region PlayerInvItem

        public static async Task<PlayerInvItemModel> GetPlayerInvItem(string social, uint itemName)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    return await db.PlayerInvItems.Where(x => x.SocialClub == social)
                        .FirstAsync(x => x.Item == itemName).ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return null;
            }
        }

        public static async Task<List<PlayerInvItemModel>> GetPlayerInvList(string social)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    return await db.PlayerInvItems.Where(x => x.SocialClub == social).ToListAsync()
                        .ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return null;
            }
        }

        public static async Task<bool> SetPlayerInvItemInDb(string social, uint itemName, uint amount)
        {
            try
            {
                var newInvItem = new PlayerInvItemModel
                {
                    SocialClub = social,
                    Item = itemName,
                    Amount = amount
                };

                using (var db = new RolePlayContext())
                {
                    await db.PlayerInvItems.AddAsync(newInvItem).ConfigureAwait(false);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        public static async Task<bool> UpdatePlayerInvItem(PlayerInvItemModel invItem)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    db.PlayerInvItems.Update(invItem);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        public static async Task<bool> UpdatePlayerInvItemList(List<PlayerInvItemModel> inventory)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    foreach (var item in inventory) db.PlayerInvItems.Update(item);

                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        public static async Task<bool> DeletePlayerInvItem(PlayerInvItemModel invItem)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    db.PlayerInvItems.Remove(invItem);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        #endregion

        #region PlayerMoney

        public static async Task<PlayerMoneyModel> GetPlayerMoney(string social)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    return await db.PlayerMoney.FirstAsync(x => x.SocialClub == social).ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return null;
            }
        }

        public static async Task<bool> SetPlayerMoney(string social, float cashMoney)
        {
            try
            {
                var newPlayerMoney = new PlayerMoneyModel
                {
                    SocialClub = social,
                    CashMoney = cashMoney,
                    BackUpMoney = 0.0f,
                    SocialPaymentBank = -1,
                    StatePaymentBank = -1
                };

                using (var db = new RolePlayContext())
                {
                    await db.PlayerMoney.AddAsync(newPlayerMoney).ConfigureAwait(false);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        public static async Task<bool> UpdatePlayerMoney(PlayerMoneyModel playerMoney)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    db.PlayerMoney.Update(playerMoney);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        public static async Task<bool> DeletePlayerMoney(PlayerMoneyModel playerMoney)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    db.PlayerMoney.Remove(playerMoney);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        #endregion

        #region PlayerParticular

        public static async Task<PlayerParticularModel> GetPlayerParticular(string social)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    return await db.PlayerParticulars.FirstAsync(x => x.SocialClub == social).ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return null;
            }
        }

        public static async Task<bool> SetPlayerParticular(string social, bool hasIdentityCard, string firstName,
            string lastName, string birthday, string nationality, string eyeColor, string height, bool gender)
        {
            try
            {
                var newParticular = new PlayerParticularModel
                {
                    SocialClub = social,
                    HasIdentityCard = hasIdentityCard,
                    FirstName = firstName,
                    LastName = lastName,
                    Birthday = birthday,
                    Nationality = nationality,
                    EyeColor = eyeColor,
                    Height = height,
                    Gender = gender
                };

                using (var db = new RolePlayContext())
                {
                    await db.PlayerParticulars.AddAsync(newParticular).ConfigureAwait(false);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        public static async Task UpdatePlayerParticular(PlayerParticularModel particular)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    db.PlayerParticulars.Update(particular);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
            }
        }

        #endregion

        #region PlayerPaycheck

        public static async Task<PlayerPaycheckModel> GetPlayerPaycheckFromDb(string social)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    return await db.PlayerPaychecks.FirstAsync(x => x.SocialClub == social).ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return null;
            }
        }

        public static async Task<bool> SetPlayerPaycheck(string social)
        {
            try
            {
                var newPaycheck = new PlayerPaycheckModel
                {
                    SocialClub = social,
                    SocialPayIsActive = false,
                    SocialPayMinutes = 0,
                    InCivActive = false,
                    LscMinutes = 0,
                    InLscActive = false,
                    LsrsMinutes = 0,
                    InLsrsActive = false,
                    LspdMinutes = 0,
                    InLspdActive = false,
                    FibMinutes = 0,
                    InFibActive = false,
                    LssdMinutes = 0,
                    InLssdActive = false,
                    DojMinutes = 0,
                    InDojActive = false,
                    DcrMinutes = 0,
                    InDcrActive = false,
                    LastPayment = DateTime.Now
                };

                using (var db = new RolePlayContext())
                {
                    await db.PlayerPaychecks.AddAsync(newPaycheck).ConfigureAwait(false);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        public static async Task<bool> UpdatePlayerPaycheck(PlayerPaycheckModel paycheck)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    db.PlayerPaychecks.Update(paycheck);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        #endregion

        #region PlayerPhone

        #endregion

        #region PlayerPosition

        public static async Task<PlayerPositionModel> GetPlayerPosFromDb(string social)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    return await db.PlayerPositions.FirstAsync(x => x.SocialClub == social).ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return null;
            }
        }

        public static async Task<bool> SetPlayerPosInDb(string social, float lastPosX,
            float lastPosY, float lastPosZ, float rotation, uint dimension)
        {
            try
            {
                var newPosition = new PlayerPositionModel
                {
                    SocialClub = social,
                    IsDead = false,
                    TimeUntilRespawn = 0,
                    LastPosX = lastPosX,
                    LastPosY = lastPosY,
                    LastPosZ = lastPosZ + 1.0f,
                    Rotation = rotation,
                    Dimension = dimension
                };

                using (var db = new RolePlayContext())
                {
                    await db.PlayerPositions.AddAsync(newPosition).ConfigureAwait(false);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        public static async Task<bool> UpdatePlayerPosInDb(PlayerPositionModel position)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    db.PlayerPositions.Update(position);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        #endregion

        #region PlayerCookingRecipeBook

        public static async Task<RecipeBookModel> GetPlayerRecipeBook(string social)
        {
            try
            {
                PlayerRecipeBookModel recipeBook;

                using (var db = new RolePlayContext())
                {
                    recipeBook = await db.PlayerRecipeBook.FirstAsync(x => x.SocialClub == social).ConfigureAwait(false);
                }

                return JsonConvert.DeserializeObject<RecipeBookModel>(recipeBook.RecipeBook);
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return null;
            }
        }

        public static async Task<bool> SetPlayerRecipeBook(string social)
        {
            try
            {
                var recipeBook = new RecipeBookModel
                {
                    Butcher = new List<int> { 0 },
                    ButcherPoints = 0f,
                    Carpenter = new List<int> { 0 },
                    CarpenterPoints = 0f,
                    CementFactory = new List<int> { 0 },
                    CementFactoryPoints = 0f,
                    Dairy = new List<int> { 0 },
                    DairyPoints = 0f,
                    Distillery = new List<int> { 0 },
                    DistilleryPoints = 0f,
                    DrugFactory = new List<int> { 0 },
                    DrugFactoryPoints = 0f,
                    FlourMill = new List<int> { 0 },
                    FlourMillPoints = 0f,
                    Forge = new List<int> { 0 },
                    ForgePoints = 0f,
                    Glassworks = new List<int> { 0 },
                    GlassworksPoints = 0f,
                    JuicePress = new List<int> { 0 },
                    JuicePressPoints = 0f,
                    KetchupFactory = new List<int> { 0 },
                    KetchupFactoryPoints = 0f,
                    MarijuhanaFactory = new List<int> { 0 },
                    MarijuhanaFactoryPoints = 0f,
                    Melt = new List<int> { 0 },
                    MeltPoints = 0f,
                    Minter = new List<int> { 0 },
                    MinterPoints = 0f,
                    PaperFactory = new List<int> { 0 },
                    PaperFactoryPoints = 0f,
                    RollingMill = new List<int> { 0 },
                    RollingMillPoints = 0f,
                    Sawmill = new List<int> { 0 },
                    SawmillPoints = 0f,
                    StoneMill = new List<int> { 0 },
                    StoneMillPoints = 0f,
                    WalnutProcessing = new List<int> { 0 },
                    WalnutProcessingPoints = 0f
                };

                var newRecipeBook = new PlayerRecipeBookModel
                {
                    SocialClub = social,
                    RecipeBook = JsonConvert.SerializeObject(recipeBook)
                };

                using (var db = new RolePlayContext())
                {
                    await db.PlayerRecipeBook.AddAsync(newRecipeBook).ConfigureAwait(false);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        public static async Task<bool> UpdatePlayerRecipeBook(string social, RecipeBookModel recipeBook)
        {
            try
            {
                var newRecipeBook = new PlayerRecipeBookModel
                {
                    SocialClub = social,
                    RecipeBook = JsonConvert.SerializeObject(recipeBook)
                };

                using (var db = new RolePlayContext())
                {
                    db.PlayerRecipeBook.Update(newRecipeBook);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        #endregion

        #region PlayerWeapon

        public static async Task<List<PlayerWeaponModel>> GetPlayerWeaponList(string social)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    return await db.PlayerWeapons.Where(x => x.SocialClub == social).ToListAsync().ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return null;
            }
        }

        public static async Task<bool> SetPlayerWeapon(string social, uint weapon, uint amount)
        {
            try
            {
                var newWeapon = new PlayerWeaponModel
                {
                    SocialClub = social,
                    Weapon = weapon,
                    Amount = amount
                };

                using (var db = new RolePlayContext())
                {
                    await db.PlayerWeapons.AddAsync(newWeapon).ConfigureAwait(false);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        public static async Task<bool> UpdatePlayerWeapons(List<PlayerWeaponModel> weapons)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    foreach (var item in weapons) db.PlayerWeapons.Update(item);

                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        #endregion

        #region ServerPlayerWhiteList

        public static async Task<ServerPlayerWhiteListModel> GetServerWhitelistEntry(string socialClub)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    return await db.ServerPlayerWhiteList.FirstAsync(x => x.SocialClubName == socialClub).ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return null;
            }
        }

        public static async Task<List<ServerPlayerWhiteListModel>> GetServerWhitelistList()
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    return await db.ServerPlayerWhiteList.ToListAsync().ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return null;
            }
        }

        public static async Task<bool> SetServerWhiteListEntry(string social)
        {
            try
            {
                var newWhitelist = new ServerPlayerWhiteListModel
                {
                    SocialClubName = social,
                    ListingDate = DateTime.Now,
                    LastLogin = DateTime.Now,
                    LastIp = "127.0.0.1",
                    IsOnline = false,
                    BannedUntil = DateTime.Now,
                    BanReason = string.Empty
                };

                using (var db = new RolePlayContext())
                {
                    await db.ServerPlayerWhiteList.AddAsync(newWhitelist).ConfigureAwait(false);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        public static async Task<bool> UpdateServerWhiteListEntry(ServerPlayerWhiteListModel whitelist)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    db.ServerPlayerWhiteList.Update(whitelist);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        #endregion

        #region VehicleFuel

        public static async Task<VehicleFuelModel> GetVehicleFuelFromDb(int vehicleId)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    return await db.VehicleFuel.FirstAsync(x => x.Id == vehicleId).ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return null;
            }
        }

        public static async Task<bool> SetVehicleFuelInDb(int vehicleId, uint vehicleHash)
        {
            try
            {
                var vehConfig = VehicleCfg.AllVehicles.Single(x => (uint)x.VehicleHash == vehicleHash);

                var newVehFuel = new VehicleFuelModel
                {
                    VehicleId = vehicleId,
                    TankStatus = vehConfig.TankCapacity,
                    EngineStatus = false
                };

                using (var db = new RolePlayContext())
                {
                    await db.VehicleFuel.AddAsync(newVehFuel).ConfigureAwait(false);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        public static async Task<bool> UpdateVehicleFuel(VehicleFuelModel vehicleFuel)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    db.Update(vehicleFuel);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        public static async Task<bool> DeleteVehicleFuelFromDb(VehicleFuelModel vehicleFuel)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    db.Remove(vehicleFuel);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        #endregion

        #region VehicleInvItem

        public static async Task<VehicleInvItemModel> GetVehicleInventoryItem(int vehId, int itemName)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    return await db.VehicleInvItems.Where(x => x.Id == vehId).FirstAsync(x => x.Item == itemName)
                        .ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return null;
            }
        }

        public static async Task<List<VehicleInvItemModel>> GetVehicleInventoryList(int vehicleId)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    return await db.VehicleInvItems.Where(x => x.VehicleId == vehicleId).ToListAsync().ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return null;
            }
        }

        public static async Task<List<VehicleInvItemModel>> GetVehicleInventoryList(Vehicle vehicle)
        {
            try
            {
                var vehicleData = (VehicleModel)vehicle.GetData(VehicleDataConst.CivVehicleData);

                using (var db = new RolePlayContext())
                {
                    return await db.VehicleInvItems.Where(x => x.VehicleId == vehicleData.Id).ToListAsync()
                        .ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return null;
            }
        }

        public static async Task<bool> SetVehicleInventoryItem(int vehicleId, uint item, uint amount)
        {
            try
            {
                var invItem = new VehicleInvItemModel
                {
                    VehicleId = vehicleId,
                    Item = item,
                    Amount = amount
                };

                using (var db = new RolePlayContext())
                {
                    await db.VehicleInvItems.AddAsync(invItem).ConfigureAwait(false);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        public static async Task<bool> UpdateVehicleInvItem(VehicleInvItemModel vehInvItem)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    db.VehicleInvItems.Update(vehInvItem);

                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        public static async Task<bool> DeleteVehicleInventoryItem(VehicleInvItemModel vehInvItem)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    db.VehicleInvItems.Remove(vehInvItem);

                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        #endregion

        #region VehicleKey

        public static async Task<VehicleKeyModel> GetVehicleKeyFromDb(int vehicleId)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    return await db.VehicleKeys.FirstAsync(x => x.VehicleId == vehicleId).ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return null;
            }
        }

        public static async Task<bool> SetVehicleKeyInDb(int vehicleId, string social)
        {
            try
            {
                var newKey = new VehicleKeyModel
                {
                    VehicleId = vehicleId,
                    FirstKeyPlayerSocial = social,
                    SecondKeyPlayerIdSocial = social,
                    ThirdKeyPlayerIdSocial = social
                };

                using (var db = new RolePlayContext())
                {
                    await db.VehicleKeys.AddAsync(newKey).ConfigureAwait(false);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        public static async Task<bool> UpdateVehicleKeyInDb(VehicleKeyModel key)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    db.VehicleKeys.Update(key);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        public static async Task<bool> DeleteVehicleKeyFromDb(VehicleKeyModel key)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    db.VehicleKeys.Remove(key);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        #endregion

        #region Vehicle

        public static async Task<VehicleModel> GetVehicle(int vehicleId)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    return await db.Vehicles.FirstAsync(x => x.Id == vehicleId).ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return null;
            }
        }

        public static async Task<List<VehicleModel>> GetVehicleList()
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    return await db.Vehicles.ToListAsync().ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return null;
            }
        }

        public static async Task<VehicleModel> GetVehicleByBuyData(string social, DateTime buyDate)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    return await db.Vehicles.Where(x => x.SocialClub == social).FirstAsync(x => x.BuyDate == buyDate)
                        .ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return null;
            }
        }

        public static async Task<bool> SetVehicle(string social, uint hash, uint faction,
            float tankStatus, DateTime buyDate)
        {
            try
            {
                var newVehicle = new VehicleModel
                {
                    SocialClub = social,
                    VehicleHash = hash,
                    Faction = faction,
                    LockStatus = true,
                    PlateText = string.Empty,
                    BuyDate = buyDate
                };

                using (var db = new RolePlayContext())
                {
                    await db.Vehicles.AddAsync(newVehicle).ConfigureAwait(false);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        public static async Task<bool> UpdateVehicle(VehicleModel vehData)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    db.Vehicles.Update(vehData);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        #endregion

        #region VehiclePosition

        public static async Task<VehiclePositionModel> GetVehiclePosFromDb(int vehicleId)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    return await db.VehiclePositions.FirstAsync(x => x.Id == vehicleId).ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return null;
            }
        }

        public static async Task<bool> SetVehiclePosInDb(int vehicleId, float posX, float posY,
            float posZ, float rotation, uint dimension)
        {
            try
            {
                var newVehPos = new VehiclePositionModel
                {
                    VehicleId = vehicleId,
                    LastPosX = posX,
                    LastPosY = posY,
                    LastPosZ = posZ,
                    Rotation = rotation,
                    Dimension = dimension
                };

                using (var db = new RolePlayContext())
                {
                    await db.VehiclePositions.AddAsync(newVehPos).ConfigureAwait(false);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        public static async Task<bool> UpdateVehiclePosInDb(VehiclePositionModel pos)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    db.VehiclePositions.Update(pos);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        #endregion

        #region VehicleSetting

        public static async Task<VehicleSettingModel> GetVehicleSettingFromDb(int vehicleId)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    return await db.VehicleSettings.FirstAsync(x => x.Id == vehicleId).ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return null;
            }
        }

        public static async Task<bool> SetVehicleSettingsInDb(int vehicleId, int firstColor, int secondColor)
        {
            try
            {
                var newVehSettings = new VehicleSettingModel
                {
                    VehicleId = vehicleId,
                    FirstColor = firstColor,
                    SecondColor = secondColor,
                    Pearlescent = false,
                    Spoiler = -1,
                    FrontBumper = -1,
                    RearBumper = -1,
                    SideSkirt = -1,
                    Exhaust = -1,
                    Frame = -1,
                    Grille = -1,
                    Hood = -1,
                    Fender = -1,
                    RightFender = -1,
                    Roof = -1,
                    Engine = -1,
                    Brakes = -1,
                    Transmission = -1,
                    Horn = -1,
                    Suspension = -1,
                    Armor = -1,
                    Turbo = -1,
                    Xenon = -1,
                    FrontWheels = -1,
                    BackWheels = -1,
                    UtilShadowSilver = -1,
                    PlateHolders = -1,
                    TrimDesign = -1,
                    Ornaments = -1,
                    DialDesign = -1,
                    SteeringWheel = -1,
                    ShiftLever = -1,
                    Plaques = -1,
                    Hydraulics = -1,
                    Livery = -1,
                    Plate = -1,
                    WindowTint = -1
                };

                using (var db = new RolePlayContext())
                {
                    await db.VehicleSettings.AddAsync(newVehSettings).ConfigureAwait(false);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.DatabaseErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        #endregion

        #region MarketSystem

        public static async Task<List<MarketSystemModel>> GetCurrentPrices()
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    return await db.MarketSystemPrices.ToListAsync().ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput(e.Message);
                return null;
            }
        }

        public static async Task<List<MarketSystemModel>> GetCurrentPricesByItemType(uint itemType)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    return await db.MarketSystemPrices.Where(x => x.ItemType == itemType).ToListAsync().ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput(e.Message);
                return null;
            }
        }

        public static async Task<MarketSystemModel> GetCurrentPriceByItemName(uint itemName)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    return await db.MarketSystemPrices.FirstAsync(x => x.ItemName == itemName).ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput(e.Message);
                return null;
            }
        }

        public static async Task<List<MarketSystemModel>> GetCurrentPriceByTrader(uint traderName)
        {
            try
            {
                var results = new List<MarketSystemModel>();
                TraderCfg.TraderItems.TryGetValue(traderName, out var itemList);

                foreach (var item in itemList)
                {
                    using (var db = new RolePlayContext())
                    {
                        results.Add(await db.MarketSystemPrices.FirstAsync(x => x.ItemName == item));
                    }
                }

                return results;
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput(e.Message);
                return null;
            }
        }

        public static async Task<bool> SetCurrentPrice(uint itemName, uint itemType, float currPrice)
        {
            try
            {
                var newMarketSys = new MarketSystemModel
                {
                    ItemName = itemName,
                    ItemType = itemType,
                    CurrentPrice = currPrice,
                    ChangeDate = DateTime.Now
                };

                using (var db = new RolePlayContext())
                {
                    await db.MarketSystemPrices.AddAsync(newMarketSys).ConfigureAwait(false);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput(e.Message);
                return false;
            }
        }

        public static async Task<bool> UpdateMarketPrice(MarketSystemModel marketSysData)
        {
            try
            {
                using (var db = new RolePlayContext())
                {
                    db.MarketSystemPrices.Update(marketSysData);
                    await db.SaveChangesAsync().ConfigureAwait(false);

                    return true;
                }
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput(e.Message);
                return false;
            }
        }

        #endregion
    }
}