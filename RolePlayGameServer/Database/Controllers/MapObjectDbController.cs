﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using RolePlayGameData.Database.MapMarkModels;
using RolePlayGameData.Models;
using RolePlayGameServer.Database.Contexts;
using RolePlayGameServer.Util;

namespace RolePlayGameServer.Database.Controllers
{
    public class MapObjectDbController
    {
        #region ShopSlot

        public static async Task<List<ShopSlotPosModel>> GetVehicleShopPosListFromDb()
        {
            try
            {
                using (var db = new MapObjectContext())
                {
                    return await db.ShopSlotPositions
                        .Where(x => x.MarkName != 0)
                        .ToListAsync()
                        .ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return null;
            }
        }

        public static async Task<ShopSlotPosModel> GetVehicleShopPosFromDb(uint markName)
        {
            try
            {
                using (var db = new MapObjectContext())
                {
                    return await db.ShopSlotPositions.FirstAsync(x => x.MarkName == markName)
                        .ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return null;
            }
        }

        public static async Task<bool> SetShopSlot(uint markName, bool isActive, uint vehicleHash,
            uint firstColor, uint secondColor, DateTime creationDate)
        {
            try
            {
                var shopSlot = new ShopSlotPosModel
                {
                    MarkName = markName,
                    IsActive = isActive,
                    VehicleHash = vehicleHash,
                    FirstColor = firstColor,
                    SecondColor = secondColor,
                    CreationDate = creationDate
                };

                using (var db = new MapObjectContext())
                {
                    await db.ShopSlotPositions.AddAsync(shopSlot)
                        .ConfigureAwait(false);
                    await db.SaveChangesAsync()
                        .ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        public static async Task<bool> UpdateVehShopInDb(ShopSlotPosModel vehShop)
        {
            try
            {
                using (var db = new MapObjectContext())
                {
                    db.ShopSlotPositions.Update(vehShop);
                    await db.SaveChangesAsync().ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        #endregion

        #region MapMarkPosition

        public static async Task<List<MarkPositionModel>> GetMarkPositionListFromDb()
        {
            try
            {
                using (var db = new MapObjectContext())
                {
                    return await db.MarkPositions
                        .ToListAsync()
                        .ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return null;
            }
        }

        public static async Task<MarkPositionModel> GetMarkPosition(int id)
        {
            try
            {
                using (var db = new MapObjectContext())
                {
                    return await db.MarkPositions
                        .FirstAsync(x => x.Id == id)
                        .ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return null;
            }
        }

        public static async Task<bool> SetMarkPosInDb(uint markFac, uint markType, uint markName,
            float posX, float posY, float posZ, float rotX, float rotY, float rotZ, uint dimension)
        {
            try
            {
                var newLocationPos = new MarkPositionModel
                {
                    MarkFaction = markFac,
                    MarkType = markType,
                    MarkName = markName,
                    PositionX = posX,
                    PositionY = posY,
                    PositionZ = posZ,
                    RotationX = rotX,
                    RotationY = rotY,
                    RotationZ = rotZ,
                    Dimension = dimension
                };

                using (var db = new MapObjectContext())
                {
                    await db.MarkPositions
                        .AddAsync(newLocationPos)
                        .ConfigureAwait(false);
                    await db.SaveChangesAsync()
                        .ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        public static async Task DeleteMarkPosition(MarkPositionModel pos)
        {
            try
            {
                using (var db = new MapObjectContext())
                {
                    db.MarkPositions.Remove(pos);
                    await db.SaveChangesAsync()
                        .ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
            }
        }

        #endregion

        #region ShopSlot

        public static async Task<List<WeedPlantModel>> GetWeedPlantList()
        {
            try
            {
                using (var db = new MapObjectContext())
                {
                    return await db.WeedPlants
                        .ToListAsync()
                        .ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return null;
            }
        }

        public static async Task<WeedPlantModel> GetWeedPlantById(int plantId)
        {
            try
            {
                using (var db = new MapObjectContext())
                {
                    return await db.WeedPlants.FirstAsync(x => x.Id == plantId)
                        .ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return null;
            }
        }

        public static async Task<bool> SetWeedPlant(uint weedType, bool growBoost, bool budBoost, bool fK, bool fM,
            bool fS,
            float posX, float posY, float posZ)
        {
            try
            {
                var plantData = new WeedPlantModel
                {
                    Type = weedType,
                    Gender = false,
                    GrowBooster = growBoost,
                    BudBooster = budBoost,
                    FertilizerK = fK,
                    FertilizerM = fM,
                    FertilizerS = fS,
                    PosX = posX,
                    PosY = posY,
                    PosZ = posZ,
                    PlantTime = DateTime.Now
                };

                using (var db = new MapObjectContext())
                {
                    await db.WeedPlants.AddAsync(plantData)
                        .ConfigureAwait(false);
                    await db.SaveChangesAsync()
                        .ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        public static async Task<bool> UpdateWeedPlant(WeedPlantModel plantData)
        {
            try
            {
                using (var db = new MapObjectContext())
                {
                    db.WeedPlants.Update(plantData);
                    await db.SaveChangesAsync()
                        .ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        public static async Task<bool> DeleteWeedPlant(WeedPlantModel plantData)
        {
            try
            {
                using (var db = new MapObjectContext())
                {
                    db.WeedPlants.Remove(plantData);
                    await db.SaveChangesAsync()
                        .ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        #endregion

        #region Dumpsters

        public static async Task<DumpsterModel> GetDumpster(int id)
        {
            try
            {
                using (var db = new MapObjectContext())
                {
                    return await db.Dumpsters.FirstAsync(x => x.Id == id)
                        .ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return null;
            }
        }

        public static async Task<List<DumpsterModel>> GetDumpsterList()
        {
            try
            {
                using (var db = new MapObjectContext())
                {
                    return await db.Dumpsters.ToListAsync()
                        .ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return null;
            }
        }

        public static async Task<bool> SetDumpster(float posX, float posY, float posZ, uint dim)
        {
            try
            {
                var newDumpster = new DumpsterModel
                {
                    ItemList = JsonConvert.SerializeObject(new List<DumpsterInvItemModel>()),
                    PositionX = posX,
                    PositionY = posY,
                    PositionZ = posZ,
                    Dimension = dim
                };

                using (var db = new MapObjectContext())
                {
                    await db.Dumpsters
                        .AddAsync(newDumpster)
                        .ConfigureAwait(false);
                    await db.SaveChangesAsync()
                        .ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
                return false;
            }
        }

        public static async Task UpdateDumpster(DumpsterModel dumpster)
        {
            try
            {
                using (var db = new MapObjectContext())
                {
                    db.Dumpsters.Update(dumpster);
                    await db.SaveChangesAsync()
                        .ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
            }
        }

        public static async Task DeleteDumpster(DumpsterModel dumpster)
        {
            try
            {
                using (var db = new MapObjectContext())
                {
                    db.Dumpsters.Remove(dumpster);
                    await db.SaveChangesAsync()
                        .ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
            }
        }

        public static async Task DeleteDumpsterById(int id)
        {
            try
            {
                using (var db = new MapObjectContext())
                {
                    var dumpster = await db.Dumpsters.FirstAsync(x => x.Id == id);
                    db.Dumpsters.Remove(dumpster);
                    await db.SaveChangesAsync()
                        .ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"{MethodBase.GetCurrentMethod().DeclaringType} - {e.Message}");
            }
        }

        #endregion
    }
}