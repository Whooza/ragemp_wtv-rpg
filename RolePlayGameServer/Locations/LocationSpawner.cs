﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GTANetworkAPI;
using RolePlayGameData.Configs;
using RolePlayGameData.Database.MapMarkModels;
using RolePlayGameData.EntityDataConsts;
using RolePlayGameData.Enums;
using RolePlayGameData.Models;
using RolePlayGameServer.Database.Controllers;
using RolePlayGameServer.Util;

namespace RolePlayGameServer.Locations
{
    internal class LocationSpawner
    {
        private List<MarkPositionModel> MarkPosList => GetDbPositions().Result;

        public void LoadMapMarks()
        {
            try
            {
                foreach (var pos in MarkPosList)
                {
                    NAPI.Task.Run(() =>
                    {
                        var setting = MarkCfg.LocationMarkSettings.First(x => (uint)x.MarkType == pos.MarkType);

                        var markerPos = new Vector3(pos.PositionX, pos.PositionY, pos.PositionZ - 1);
                        var cylinderPos = new Vector3(pos.PositionX, pos.PositionY, pos.PositionZ - 25f);

                        var markData = new MarkDataModel
                        {
                            MarkId = (uint)pos.Id,
                            MarkFaction = pos.MarkFaction,
                            MarkType = pos.MarkType,
                            MarkName = pos.MarkName,
                            PosX = pos.PositionX,
                            PosY = pos.PositionY,
                            PosZ = pos.PositionZ
                        };

                        if (setting.MarkType.ToString().Contains("Farmspot")) markData.MarkName = (uint)pos.Id;

                        switch (pos.MarkType)
                        {
                            case (uint)MarkTypeEnums.ChemicalFarmspot:
                                var colShape1 = NAPI.ColShape.CreateCylinderColShape(cylinderPos, 3f, 50f, 0);
                                colShape1.SetData(MarkDataConst.MarkData, markData);
                                colShape1.SetData(MarkDataConst.LocationFlag, true);
                                break;
                            case (uint)MarkTypeEnums.AlmondFarmspot:
                                var colShape2 = NAPI.ColShape.CreateSphereColShape(markerPos, setting.ColShapeRange, pos.Dimension);
                                colShape2.SetData(MarkDataConst.MarkData, markData);
                                colShape2.SetData(MarkDataConst.LocationFlag, true);
                                var hash2 = NAPI.Util.GetHashKey("prop_tree_jacada_02");
                                var mapObject2 = NAPI.Object.CreateObject(hash2, markerPos, new Vector3(0, 0, 0), 255, 0);
                                mapObject2.SetData(MarkDataConst.MarkData, markData);
                                mapObject2.SetData(MarkDataConst.LocationFlag, true);
                                break;
                            case (uint)MarkTypeEnums.AvocadoFarmspot:
                                var colShape3 = NAPI.ColShape.CreateSphereColShape(markerPos, setting.ColShapeRange, pos.Dimension);
                                colShape3.SetData(MarkDataConst.MarkData, markData);
                                colShape3.SetData(MarkDataConst.LocationFlag, true);
                                var hash3 = NAPI.Util.GetHashKey("prop_tree_olive_cr2");
                                var mapObject3 = NAPI.Object.CreateObject(hash3, markerPos, new Vector3(0, 0, 0), 255, 0);
                                mapObject3.SetData(MarkDataConst.MarkData, markData);
                                mapObject3.SetData(MarkDataConst.LocationFlag, true);
                                break;
                            case (uint)MarkTypeEnums.LemonFarmspot:
                                var colShape4 = NAPI.ColShape.CreateSphereColShape(markerPos, setting.ColShapeRange, pos.Dimension);
                                colShape4.SetData(MarkDataConst.MarkData, markData);
                                colShape4.SetData(MarkDataConst.LocationFlag, true);
                                var hash4 = NAPI.Util.GetHashKey("prop_tree_birch_05");
                                var mapObject4 = NAPI.Object.CreateObject(hash4, markerPos, new Vector3(0, 0, 0), 255, 0);
                                mapObject4.SetData(MarkDataConst.MarkData, markData);
                                mapObject4.SetData(MarkDataConst.LocationFlag, true);
                                break;
                            default:
                                var colShape = NAPI.ColShape.CreateSphereColShape(markerPos, setting.ColShapeRange, pos.Dimension);
                                colShape.SetData(MarkDataConst.MarkData, markData);
                                colShape.SetData(MarkDataConst.LocationFlag, true);
                                break;
                        }

                        if (ServerCfg.ShowMarkIds)
                        {
                            var idPos = new Vector3(0, 0, 0);

                            if (pos.MarkType >= 85 && pos.MarkType <= 128) idPos = new Vector3(pos.PositionX + 1, pos.PositionY + 1, pos.PositionZ + 2);
                            else idPos = new Vector3(pos.PositionX, pos.PositionY, pos.PositionZ + 2);

                            var idLabel = NAPI.TextLabel.CreateTextLabel($"[Id={pos.Id}] [Name={pos.MarkName}]",
                                idPos, setting.TextLabelRange, setting.TextLabelSize, setting.TextLabelFont,
                                new Color(255, 165, 0), setting.TextLabelEntitySeeThrough, pos.Dimension);

                            idLabel.SetData(MarkDataConst.MarkData, markData);
                            idLabel.SetData(MarkDataConst.LocationFlag, true);
                        }

                        if (setting.HasWorldMark)
                        {
                            var mark = NAPI.Marker.CreateMarker(setting.MarkerType, markerPos, new Vector3(), new Vector3(),
                                        setting.MarkerScale, new Color(setting.MarkerColorRed, setting.MarkerColorGreen,
                                        setting.MarkerColorBlue, setting.MarkerColorAlpha), setting.MarkerBlobUpAndDown,
                                        pos.Dimension);

                            mark.SetData(MarkDataConst.MarkData, markData);
                            mark.SetData(MarkDataConst.LocationFlag, true);
                        }

                        if (setting.HasTextLabel)
                        {
                            var textPos = new Vector3(pos.PositionX, pos.PositionY, pos.PositionZ + 1);
                            var textLabel = NAPI.TextLabel.CreateTextLabel(setting.MarkText, textPos,
                                setting.TextLabelRange, setting.TextLabelSize, setting.TextLabelFont,
                                new Color(setting.MarkerColorRed, setting.TextLabelColorGreen,
                                    setting.TextLabelColorBlue, setting.TextLabelColorAlpha),
                                setting.TextLabelEntitySeeThrough, pos.Dimension);

                            textLabel.SetData(MarkDataConst.MarkData, markData);
                            textLabel.SetData(MarkDataConst.LocationFlag, true);
                        }
                    });
                }
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"{this} - {e.Message}");
            }
        }

        private async Task<List<MarkPositionModel>> GetDbPositions() =>
            await MapObjectDbController.GetMarkPositionListFromDb().ConfigureAwait(false);
    }
}