﻿using GTANetworkAPI;
using RolePlayGameData.Configs;
using RolePlayGameData.Database.MapMarkModels;
using RolePlayGameData.EntityDataConsts;
using RolePlayGameData.Texts;
using RolePlayGameServer.Database.Controllers;
using RolePlayGameServer.Util;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RolePlayGameServer.Locations
{
    internal class LocationCreator
    {
        private readonly Client _player;

        public LocationCreator(Client player) => _player = player;

        public async void CreateMarkAsync(uint locFaction, uint locType)
        {
            try
            {
                var settings = MarkCfg.LocationMarkSettings.Single(x => (uint)x.MarkType == locType);

                var newPos = new MarkPositionModel
                {
                    MarkFaction = locFaction,
                    MarkType = locType,
                    MarkName = 0,
                    PositionX = _player.Position.X,
                    PositionY = _player.Position.Y,
                    PositionZ = _player.Position.Z,
                    RotationX = _player.Rotation.X,
                    RotationY = _player.Rotation.Y,
                    RotationZ = _player.Rotation.Z,
                    Dimension = _player.Dimension
                };

                await MapObjectDbController.SetMarkPosInDb(locFaction, locType, 0, _player.Position.X,
                        _player.Position.Y, _player.Position.Z, _player.Rotation.X, _player.Rotation.Y,
                        _player.Rotation.Z, _player.Dimension)
                    .ConfigureAwait(false);

                _player.SendNotification(PlayerNotificationTexts.MarkSuccessfulCreated);

                ReloadMarks();
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"{this} - {e.Message}");
            }
        }

        public async void DeleteMarkAsync(int markId)
        {
            try
            {
                var markToDelete = await MapObjectDbController.GetMarkPosition(markId)
                    .ConfigureAwait(false);

                await MapObjectDbController.DeleteMarkPosition(markToDelete).ConfigureAwait(false);

                ReloadMarks();

                _player.SendNotification(PlayerNotificationTexts.MarkSuccessfulDeleted);
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"{this} - {e.Message}");
            }
        }

        public async void ReloadMarks()
        {
            try
            {
                foreach (var item in NAPI.Pools.GetAllBlips())
                {
                    NAPI.Task.Run(() =>
                    {
                        if (item.HasData(MarkDataConst.LocationFlag)) item.Delete();
                    });
                }

                foreach (var item in NAPI.Pools.GetAllColShapes())
                {
                    NAPI.Task.Run(() =>
                    {
                        if (item.HasData(MarkDataConst.LocationFlag)) item.Delete();
                    });
                }

                foreach (var item in NAPI.Pools.GetAllMarkers())
                {
                    NAPI.Task.Run(() =>
                    {
                        if (item.HasData(MarkDataConst.LocationFlag)) item.Delete();
                    });
                }

                foreach (var item in NAPI.Pools.GetAllTextLabels())
                {
                    NAPI.Task.Run(() =>
                    {
                        if (item.HasData(MarkDataConst.LocationFlag)) item.Delete();
                    });
                }

                await Task.Delay(1000);

                new LocationSpawner().LoadMapMarks();

                _player.SendNotification(PlayerNotificationTexts.MarksSuccessfulReloaded);
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"{this} - {e.Message}");
            }
        }
    }
}