﻿using GTANetworkAPI;
using RolePlayGameData.Database.RolePlayModels;
using RolePlayGameServer.Database.Controllers;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RolePlayGameServer.Util
{
    public class CharCustomization
    {
        private readonly Client _player;
        private readonly PlayerCharSettingModel _charSettings;
        private readonly PlayerParticularModel _particulars;

        public CharCustomization(Client player, PlayerParticularModel particulars)
        {
            _player = player;
            _charSettings = GetCharSettings().Result;
            _particulars = particulars;
        }

        private async Task<PlayerCharSettingModel> GetCharSettings() =>
            await RolePlayDbController.GetPlayerCharSettings(_player.SocialClubName).ConfigureAwait(false);


        public void ApplyPlayerCustomization()
        {
            try
            {
                var headBlend = new HeadBlend();
                {
                    headBlend.ShapeFirst = Convert.ToByte(_charSettings.FirstHeadShape);
                    headBlend.ShapeSecond = Convert.ToByte(_charSettings.SecondHeadShape);
                    headBlend.SkinFirst = Convert.ToByte(_charSettings.FirstSkinTone);
                    headBlend.SkinSecond = Convert.ToByte(_charSettings.SecondSkinTone);
                    headBlend.ShapeMix = _charSettings.HeadMix;
                    headBlend.SkinMix = _charSettings.SkinMix;
                }

                var eyeColor = Convert.ToByte(_charSettings.EyesColor);
                var hairColor = Convert.ToByte(_charSettings.FirstHairColor);
                var highlightColor = Convert.ToByte(_charSettings.SecondHairColor);

                float[] faceFeatures =
                {
                    _charSettings.NoseWidth, _charSettings.NoseHeight,
                    _charSettings.NoseLength,
                    _charSettings.NoseBridge, _charSettings.NoseTip,
                    _charSettings.NoseShift,
                    _charSettings.BrowHeight, _charSettings.BrowWidth,
                    _charSettings.CheekboneHeight,
                    _charSettings.CheekboneWidth, _charSettings.CheeksWidth,
                    _charSettings.Eyes,
                    _charSettings.Lips, _charSettings.JawWidth,
                    _charSettings.JawHeight,
                    _charSettings.ChinLength, _charSettings.ChinPosition,
                    _charSettings.ChinWidth,
                    _charSettings.ChinShape, _charSettings.NeckWidth
                };

                var headOverlays = new Dictionary<int, HeadOverlay>();

                for (var i = 0; i < 11; i++)
                {
                    var overlayData = GetOverlayData(i);

                    var headOverlay = new HeadOverlay();
                    {
                        headOverlay.Index = Convert.ToByte(overlayData[0]);
                        headOverlay.Color = Convert.ToByte(overlayData[1]);
                        headOverlay.SecondaryColor = 0;
                        headOverlay.Opacity = 1.0f;
                    }

                    headOverlays[i] = headOverlay;
                }

                _player.SetCustomization(_particulars.Gender, headBlend, eyeColor, hairColor, highlightColor,
                    faceFeatures, headOverlays, new Decoration[] { });
                _player.SetClothes(2, _charSettings.HairModel, 0);
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"{this} - {e.Message}");
            }
        }

        private int[] GetOverlayData(int index)
        {
            try
            {
                var overlayData = new int[2];

                switch (index)
                {
                    case 0:
                        overlayData[0] = _charSettings.BlemishesModel;
                        overlayData[1] = 0;
                        break;
                    case 1:
                        overlayData[0] = _charSettings.BeardModel;
                        overlayData[1] = _charSettings.BeardColor;
                        break;
                    case 2:
                        overlayData[0] = _charSettings.EyebrowsModel;
                        overlayData[1] = _charSettings.EyebrowsColor;
                        break;
                    case 3:
                        overlayData[0] = _charSettings.AgingModel;
                        overlayData[1] = 0;
                        break;
                    case 4:
                        overlayData[0] = _charSettings.MakeupModel;
                        overlayData[1] = 0;
                        break;
                    case 5:
                        overlayData[0] = _charSettings.BlushModel;
                        overlayData[1] = _charSettings.BlushColor;
                        break;
                    case 6:
                        overlayData[0] = _charSettings.ComplexionModel;
                        overlayData[1] = 0;
                        break;
                    case 7:
                        overlayData[0] = _charSettings.SunDamageModel;
                        overlayData[1] = 0;
                        break;
                    case 8:
                        overlayData[0] = _charSettings.LipstickModel;
                        overlayData[1] = _charSettings.LipstickColor;
                        break;
                    case 9:
                        overlayData[0] = _charSettings.FrecklesModel;
                        overlayData[1] = 0;
                        break;
                    case 10:
                        overlayData[0] = _charSettings.ChestModel;
                        overlayData[1] = _charSettings.ChestColor;
                        break;
                }

                return overlayData;
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"{this} - {e.Message}");
                return Array.Empty<int>();
            }
        }
    }
}
