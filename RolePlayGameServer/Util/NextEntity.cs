﻿using System;
using System.Threading.Tasks;
using GTANetworkAPI;
using RolePlayGameData.EntityDataConsts;

namespace RolePlayGameServer.Util
{
    internal class NextEntity
    {
        public static Task<Vehicle> NextVehicle(Entity player, float distance)
        {
            return Task.Run(() =>
            {
                try
                {
                    Vehicle nextVehicle = null;
                    var nextDistance = 1000.0f;

                    foreach (var item in NAPI.Pools.GetAllVehicles())
                    {
                        if (player.Dimension != item.Dimension) continue;
                        if (!item.HasData(VehicleDataConst.OwnedVehicleFlag)) continue;

                        var distanceVehicleToPlayer = player.Position.DistanceTo(item.Position);

                        if (distanceVehicleToPlayer > distance) continue;

                        if (nextVehicle == null)
                        {
                            nextVehicle = item;
                            nextDistance = distanceVehicleToPlayer;
                        }

                        if (distanceVehicleToPlayer > nextDistance) continue;

                        nextVehicle = item;
                        nextDistance = distanceVehicleToPlayer;
                    }

                    if (nextVehicle == null) NAPI.Util.ConsoleOutput("NextEntity.NextVehicle - vehicle is null");

                    return nextVehicle;
                }
                catch (Exception e)
                {
                    Logging.ServerErrorLog($"NextEntity.NextVehicle - {e.Message}");
                    return null;
                }
            });
        }

        public static Task<Client> NextPlayer(Entity player)
        {
            return Task.Run(() =>
            {
                try
                {
                    Client nextPlayer = null;
                    var nextDistance = 1000.0f;

                    foreach (var item in NAPI.Pools.GetAllPlayers())
                    {
                        if (player.Dimension != item.Dimension) continue;

                        var distancePlayerToPlayer = player.Position.DistanceTo(item.Position);

                        if (distancePlayerToPlayer > 3f) continue;

                        if (nextPlayer == null)
                        {
                            nextPlayer = item;
                            nextDistance = distancePlayerToPlayer;
                        }

                        if (!(distancePlayerToPlayer < nextDistance)) continue;

                        nextPlayer = item;
                        nextDistance = distancePlayerToPlayer;
                    }

                    if (nextPlayer == null)
                        NAPI.Util.ConsoleOutput("NextEntity.NextPlayer - player is null");

                    return nextPlayer;
                }
                catch (Exception e)
                {
                    Logging.ServerErrorLog($"NextEntity.NextPlayer - {e.Message}");
                    return null;
                }
            });
        }
    }
}