﻿using GTANetworkAPI;
using RolePlayGameData.Database.RolePlayModels;
using RolePlayGameData.EntityDataConsts;
using RolePlayGameData.Texts;

namespace RolePlayGameServer.Util
{
    internal class RankChecker
    {
        private readonly Client _player;
        private readonly PlayerAccountModel _playerData;

        public RankChecker(Client player)
        {
            _player = player;
            _playerData = (PlayerAccountModel)player.GetData(PlayerDataConst.PlayerAccount);
        }

        public bool CheckPlayerIsAdmin(int minAdminLvl)
        {
            if (_playerData.AdminLevel == 0)
            {
                _player.SendNotification(PlayerNotificationTexts.NoAdmin);
                return false;
            }

            if (_playerData.AdminLevel < minAdminLvl)
            {
                _player.SendNotification(PlayerNotificationTexts.LowAdmin);
                return false;
            }

            return true;
        }

        public bool CheckPlayerIsSupport(int minSupportLvl)
        {
            if(_playerData.SupportLevel == 0)
            {
                _player.SendNotification(PlayerNotificationTexts.NoSupport);
                return false;
            }

            if (_playerData.SupportLevel < minSupportLvl)
            {
                _player.SendNotification(PlayerNotificationTexts.LowSupport);
                return false;
            }

            return true;
        }
    }
}