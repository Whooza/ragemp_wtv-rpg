﻿using GTANetworkAPI;

namespace RolePlayGameServer.Util
{
    public class Notifications
    {
        public static void SendInteractionInfo(Client player, string body)
        {
            player.TriggerEvent("showLeftMessage", body, "silver");
        }

        public static void SendInteractionWarning(Client player, string body)
        {
            player.TriggerEvent("showLeftMessage", body, "orange");
        }

        public static void SendInteractionError(Client player, string body)
        {
            player.TriggerEvent("showLeftMessage", body, "darkred");
        }

        public static void SendSystemInfo(Client player, string head, string body)
        {
            player.TriggerEvent("showRightMessage", head, body, "silver");
        }

        public static void SendSystemWarning(Client player, string head, string body)
        {
            player.TriggerEvent("showRightMessage", head, body, "orange");
        }

        public static void SendSystemError(Client player, string head, string body)
        {
            player.TriggerEvent("showRightMessage", head, body, "darkred");
        }

        public static void SendSystemInfoToAll(string head, string body)
        {
            foreach (var item in NAPI.Pools.GetAllPlayers())
            {
                item.TriggerEvent("showRightMessage", head, body, "silver");
            }            
        }

        public static void SendSystemWarningToAll(string head, string body)
        {
            foreach (var item in NAPI.Pools.GetAllPlayers())
            {
                item.TriggerEvent("showRightMessage", head, body, "orange");
            }
        }

        public static void SendSystemErrorToAll(string head, string body)
        {
            foreach (var item in NAPI.Pools.GetAllPlayers())
            {
                item.TriggerEvent("showRightMessage", head, body, "darkred");
            }
        }
    }
}
