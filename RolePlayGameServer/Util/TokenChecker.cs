﻿using RolePlayGameData.Configs;

namespace RolePlayGameServer.Util
{
    public static class TokenChecker
    {
        public static bool Check(string token)
        {
            return token == ServerCfg.ApiToken;
        }
    }
}