﻿using GTANetworkAPI;
using RolePlayGameData.Configs;

namespace RolePlayGameServer.Util
{
    public static class UiHelper
    {
        private static readonly bool ClientSiteRunsLocal = ServerCfg.ClientSiteRunsLocal;

        #region MainUiWindow

        public static void ShowMainUi(Client player)
        {
            if (ClientSiteRunsLocal)
                player.TriggerEvent("createMainUiWnd", $"http://localhost:4444/home/ui/?id={ServerCfg.WebToken}");
            else
                player.TriggerEvent("createMainUiWnd", $"https://mui.the-golden-state.de/home/ui/?id={ServerCfg.WebToken}");
        }

        #endregion MainUiWindow

        #region DetroyBrowserWindow

        public static void DestroyBrowserWindow(Client player)
        {
            player.TriggerEvent("destroyCustomWnd");
        }

        #endregion DetroyBrowserWindow

        #region BrowserWindows

        public static void ShowInventory(Client player)
        {
            if (ClientSiteRunsLocal)
                player.TriggerEvent("createCustomWnd",
                    $"http://localhost:5555/inventory/{player.SocialClubName}/?token={ServerCfg.WebToken}",
                    true, false, true);

            else
                player.TriggerEvent("createCustomWnd",
                    $"https://cui.the-golden-state.de/inventory/{player.SocialClubName}/?token={ServerCfg.WebToken}",
                    true, false, true);
        }

        public static void ShowDumpster(Client player)
        {
            if (ClientSiteRunsLocal)
                player.TriggerEvent("createCustomWnd",
                    $"http://localhost:5555/dumpster/{player.SocialClubName}/?token={ServerCfg.WebToken}",
                    true, false, true);

            else
                player.TriggerEvent("createCustomWnd",
                    $"https://cui.the-golden-state.de/dumpster/{player.SocialClubName}/?token={ServerCfg.WebToken}",
                    true, false, true);
        }

        public static void ShowCharCreator(Client player)
        {
            if (ClientSiteRunsLocal)
                player.TriggerEvent("createCustomWnd",
                    $"http://localhost:5555/CharCreator/{player.SocialClubName}/?token={ServerCfg.WebToken}",
                    true, false, true);

            else
                player.TriggerEvent("createCustomWnd",
                    $"https://cui.the-golden-state.de/CharCreator/{player.SocialClubName}/?token={ServerCfg.WebToken}",
                    true, false, true);
        }

        public static void ShowLoginWindow(Client player)
        {
            if (ClientSiteRunsLocal)
                player.TriggerEvent("createCustomWnd",
                    $"http://localhost:5555/login/{player.SocialClubName}/?token={ServerCfg.WebToken}",
                    true, false, true);

            else
                player.TriggerEvent("createCustomWnd",
                    $"https://cui.the-golden-state.de/login/{player.SocialClubName}/?token={ServerCfg.WebToken}",
                    true, false, true);
        }

        public static void ShowRegisterWindow(Client player)
        {
            if (ClientSiteRunsLocal)
                player.TriggerEvent("createCustomWnd",
                    $"http://localhost:5555/register/{player.SocialClubName}/?token={ServerCfg.WebToken}",
                    true, false, true);

            else
                player.TriggerEvent("createCustomWnd",
                    $"https://cui.the-golden-state.de/register/{player.SocialClubName}/?token={ServerCfg.WebToken}",
                    true, false, true);
        }

        public static void ShowBanWindow(Client player)
        {
            if (ClientSiteRunsLocal)
                player.TriggerEvent("createCustomWnd",
                    $"http://localhost:5555/banned/{player.SocialClubName}/?token={ServerCfg.WebToken}",
                    false, false, true);

            else
                player.TriggerEvent("createCustomWnd",
                    $"https://cui.the-golden-state.de/banned/{player.SocialClubName}/?token={ServerCfg.WebToken}",
                    false, false, true);
        }

        public static void ShowNotWhitelisted(Client player)
        {
            if (ClientSiteRunsLocal)
                player.TriggerEvent("createCustomWnd",
                    $"http://localhost:5555/notwhitelisted/{player.SocialClubName}/",
                    false, false, true);

            else
                player.TriggerEvent("createCustomWnd",
                    $"https://cui.the-golden-state.de/notwhitelisted/{player.SocialClubName}/",
                    false, false, true);
        }

        public static void ShowVehicleInteraction(Client player)
        {
            if (ClientSiteRunsLocal)
                player.TriggerEvent("createCustomWnd",
                    $"http://localhost:5555/vehicleinteraction/{player.SocialClubName}/?token={ServerCfg.WebToken}",
                    true, false, true);

            else
                player.TriggerEvent("createCustomWnd",
                    $"https://cui.the-golden-state.de/vehicleinteraction/{player.SocialClubName}/?token={ServerCfg.WebToken}",
                    true, false, true);
        }

        public static void ShowVehicleTrunk(Client player)
        {
            if (ClientSiteRunsLocal)
                player.TriggerEvent("createCustomWnd",
                    $"http://localhost:5555/vehicletrunk/{player.SocialClubName}/?token={ServerCfg.WebToken}",
                    true, false, true);

            else
                player.TriggerEvent("createCustomWnd",
                    $"https://cui.the-golden-state.de/vehicletrunk/{player.SocialClubName}/?token={ServerCfg.WebToken}",
                    true, false, true);
        }

        public static void ShowVehicleBuy(Client player, uint markName)
        {
            if (ClientSiteRunsLocal)
                player.TriggerEvent("createCustomWnd",
                    $"http://localhost:5555/vehiclebuy/{player.SocialClubName}/{markName}/?token={ServerCfg.WebToken}",
                    true, false, true);

            else
                player.TriggerEvent("createCustomWnd",
                    $"https://cui.the-golden-state.de/vehiclebuy/{player.SocialClubName}/{markName}/?token={ServerCfg.WebToken}",
                    true, false, true);
        }

        public static void ShowAtm(Client player)
        {
            if (ClientSiteRunsLocal)
                player.TriggerEvent("createCustomWnd",
                    $"http://localhost:5555/atm/{player.SocialClubName}/?token={ServerCfg.WebToken}",
                    true, false, true);

            else
                player.TriggerEvent("createCustomWnd",
                    $"https://cui.the-golden-state.de/atm/{player.SocialClubName}/?token={ServerCfg.WebToken}",
                    true, false, true);
        }

        public static void ShowCooking(Client player)
        {
            if (ClientSiteRunsLocal)
                player.TriggerEvent("createCustomWnd",
                    $"http://localhost:5555/cooking/{player.SocialClubName}/?token={ServerCfg.WebToken}",
                    true, false, true);

            else
                player.TriggerEvent("createCustomWnd",
                    $"https://cui.the-golden-state.de/cooking/{player.SocialClubName}/?token={ServerCfg.WebToken}",
                    true, false, true);
        }

        public static void ShowMapMarkEditor(Client player)
        {
            if (ClientSiteRunsLocal)
                player.TriggerEvent("createCustomWnd",
                    $"http://localhost:5555/mapmarkeditor/{player.SocialClubName}/?token={ServerCfg.WebToken}",
                    true, false, true);

            else
                player.TriggerEvent("createCustomWnd",
                    $"https://cui.the-golden-state.de/mapmarkeditor/{player.SocialClubName}/?token={ServerCfg.WebToken}",
                    true, false, true);
        }

        public static void ShowItemSpawner(Client player)
        {
            if (ClientSiteRunsLocal)
                player.TriggerEvent("createCustomWnd",
                    $"http://localhost:5555/ItemSpawner/{player.SocialClubName}/?token={ServerCfg.WebToken}",
                    true, false, true);

            else
                player.TriggerEvent("createCustomWnd",
                    $"https://cui.the-golden-state.de/ItemSpawner/{player.SocialClubName}/?token={ServerCfg.WebToken}",
                    true, false, true);
        }

        public static void ShowClothingSpawner(Client player)
        {
            if (ClientSiteRunsLocal)
                player.TriggerEvent("createCustomWnd",
                    $"http://localhost:5555/ClothingSpawner/{player.SocialClubName}/?token={ServerCfg.WebToken}",
                    true, false, true);

            else
                player.TriggerEvent("createCustomWnd",
                    $"https://cui.the-golden-state.de/ClothingSpawner/{player.SocialClubName}/?token={ServerCfg.WebToken}",
                    true, false, true);
        }

        public static void ShowLocationTeleporter(Client player)
        {
            if (ClientSiteRunsLocal)
                player.TriggerEvent("createCustomWnd",
                    $"http://localhost:5555/LocationTeleporter/{player.SocialClubName}/?token={ServerCfg.WebToken}",
                    true, false, true);

            else
                player.TriggerEvent("createCustomWnd",
                    $"https://cui.the-golden-state.de/LocationTeleporter/{player.SocialClubName}/?token={ServerCfg.WebToken}",
                    true, false, true);
        }

        public static void ShowFuelStation(Client player)
        {
            if (ClientSiteRunsLocal)
                player.TriggerEvent("createCustomWnd",
                    $"http://localhost:5555/fuelstation/{player.SocialClubName}/?token={ServerCfg.WebToken}",
                    true, false, true);

            else
                player.TriggerEvent("createCustomWnd",
                    $"https://cui.the-golden-state.de/fuelstation/{player.SocialClubName}/?token={ServerCfg.WebToken}",
                    true, false, true);
        }

        public static void ShowItemShop(Client player, uint markName)
        {
            if (ClientSiteRunsLocal)
                player.TriggerEvent("createCustomWnd",
                    $"http://localhost:5555/itemshop/{player.SocialClubName}/{markName}/?token={ServerCfg.WebToken}",
                    true, false, true);

            else
                player.TriggerEvent("createCustomWnd",
                    $"https://cui.the-golden-state.de/itemshop/{player.SocialClubName}/{markName}/?token={ServerCfg.WebToken}",
                    true, false, true);
        }

        public static void ShowJobCenter(Client player)
        {
            if (ClientSiteRunsLocal)
                player.TriggerEvent("createCustomWnd",
                    $"http://localhost:5555/jobcenter/{player.SocialClubName}/?token={ServerCfg.WebToken}",
                    true, false, true);

            else
                player.TriggerEvent("createCustomWnd",
                    $"https://cui.the-golden-state.de/jobcenter/{player.SocialClubName}/?token={ServerCfg.WebToken}",
                    true, false, true);
        }

        public static void ShowCityHall(Client player)
        {
            if (ClientSiteRunsLocal)
                player.TriggerEvent("createCustomWnd",
                    $"http://localhost:5555/cityhall/{player.SocialClubName}/?token={ServerCfg.WebToken}",
                    true, false, true);

            else
                player.TriggerEvent("createCustomWnd",
                    $"https://cui.the-golden-state.de/cityhall/{player.SocialClubName}/?token={ServerCfg.WebToken}",
                    true, false, true);
        }

        public static void ShowProcessor(Client player, uint markType)
        {
            if (ClientSiteRunsLocal)
                player.TriggerEvent("createCustomWnd",
                    $"http://localhost:5555/processor/{player.SocialClubName}/{markType}/?token={ServerCfg.WebToken}",
                    true, false, true);

            else
                player.TriggerEvent("createCustomWnd",
                    $"https://cui.the-golden-state.de/processor/{player.SocialClubName}/{markType}/?token={ServerCfg.WebToken}",
                    true, false, true);
        }

        public static void ShowTrader(Client player, uint markType)
        {
            if (ClientSiteRunsLocal)
                player.TriggerEvent("createCustomWnd",
                    $"http://localhost:5555/trader/{player.SocialClubName}/{markType}/?token={ServerCfg.WebToken}",
                    true, false, true);

            else
                player.TriggerEvent("createCustomWnd",
                    $"https://cui.the-golden-state.de/trader/{player.SocialClubName}/{markType}/?token={ServerCfg.WebToken}",
                    true, false, true);
        }

        #endregion BrowserWindows

        #region BrowserWindowSpecials

        public static void SendHungerWarning(Client player, float hunger)
        {
            if(hunger > 20f)
            {
                player.TriggerEvent("showHideLowHunger", 0);
                return;
            }
            else if (hunger <= 20f && hunger > 5f)
            {
                player.TriggerEvent("showHideLowHunger", 1);
                return;
            }
            else player.TriggerEvent("showHideLowHunger", 2);
        }

        public static void SendThirstWarning(Client player, float thirst)
        {
            if (thirst > 20f)
            {
                player.TriggerEvent("showHideLowThirst", 0);
                return;
            }
            else if (thirst <= 20f && thirst > 5f)
            {
                player.TriggerEvent("showHideLowThirst", 1);
                return;
            }
            else player.TriggerEvent("showHideLowThirst", 2);
        }


        public static void UpdateCustomWindow(Client player)
        {
            player.TriggerEvent("updateCustomWindow");
        }

        public static void SendLeftMessageToPlayer(Client player, string head, string text)
        {
            player.TriggerEvent("showLeftMessage", head, text);
        }

        public static void SendRightMessageToPlayer(Client player, string head, string text)
        {
            player.TriggerEvent("showRightMessage", head, text);
        }

        public static void UpdatePlayerBiology(Client player, float hunger, float thirst)
        {
            player.TriggerEvent("updateBiology", hunger, thirst);
        }

        public static void UpdatePlayerCash(Client player, float cash)
        {
            player.TriggerEvent("updateCash", cash);
        }

        public static void ShowBadCredentials(Client player)
        {
            player.TriggerEvent("showBadCredentials");
        }

        public static void SetCreatorCamera(Client player)
        {
            player.TriggerEvent("setCreatorCamera", true);
        }

        #endregion BrowserWindowSpecials
    }
}