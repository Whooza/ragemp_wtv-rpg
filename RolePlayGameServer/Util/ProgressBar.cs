﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using GTANetworkAPI;

namespace RolePlayGameServer.Util
{
    internal class ProgressBar
    {
        private readonly Client _player;

        public ProgressBar(Client player)
        {
            _player = player;
        }

        public static Dictionary<Client, CancellationToken> ActiveProgesses { get; } =
            new Dictionary<Client, CancellationToken>();

        public bool RunProgress(string progressName, int progressTime, Action action)
        {
            if (ActiveProgesses.Keys.Contains(_player)) return false;

            var src = new CancellationTokenSource();

            ActiveProgesses.Add(_player, src.Token);

            Task.Run(() => RunProgressBar(progressName, progressTime, action), src.Token).ConfigureAwait(false);

            return true;
        }

        private async Task RunProgressBar(string progressName, int progressTime, Action action)
        {
            _player.TriggerEvent("updateProgressBar", progressName, progressTime);

            await Task.Delay(progressTime + 1250).ConfigureAwait(false);

            action.Invoke();

            ActiveProgesses.Remove(_player);
        }

        public void CancelTask()
        {
        }
    }
}