﻿using System.Linq;
using System.Threading.Tasks;
using GTANetworkAPI;

namespace RolePlayGameServer.Util
{
    public class ApiPlayer
    {
        public static Task<Client> GetClientOrDefault(string social)
        {
            return Task.Run(() => NAPI.Pools.GetAllPlayers().FirstOrDefault(x => x.SocialClubName == social));
        }
    }
}