﻿using GTANetworkAPI;
using System.Linq;
using System.Threading.Tasks;

namespace RolePlayGameServer.Util
{
    public class ApiVehicle
    {
        public static Task<Vehicle> GetVehicleOrDefault(int vehicleValue)
        {
            return Task.Run(() => NAPI.Pools.GetAllVehicles().FirstOrDefault(x => x.Value == vehicleValue));
        }
    }
}