﻿using System;
using System.Globalization;
using GTANetworkAPI;

namespace RolePlayGameServer.Util
{
    public static class Logging
    {
        public static void AuctionHouseInfoLog(string text)
        {
            NAPI.Util.ConsoleOutput("<[RPG-LOG]> AuctionHouseINFO: " + text + " - " +
                                    Convert.ToString(DateTime.Now, CultureInfo.CurrentCulture));
        }

        public static void AuctionHouseErrorLog(string text)
        {
            NAPI.Util.ConsoleOutput("<[RPG-LOG]> AuctionHouseERROR: " + text + " - " +
                                    Convert.ToString(DateTime.Now, CultureInfo.CurrentCulture));
        }

        public static void DatabaseInfoLog(string text)
        {
            NAPI.Util.ConsoleOutput("<[RPG-LOG]> DatabaseINFO: " + text + " - " +
                                    Convert.ToString(DateTime.Now, CultureInfo.CurrentCulture));
        }

        public static void DatabaseErrorLog(string text)
        {
            NAPI.Util.ConsoleOutput("<[RPG-LOG]> DatabaseERROR: " + text + " - " +
                                    Convert.ToString(DateTime.Now, CultureInfo.CurrentCulture));
        }

        public static void ServerInfoLog(string text)
        {
            NAPI.Util.ConsoleOutput("<[RPG-LOG]> ServerINFO: " + text + " - " +
                                    Convert.ToString(DateTime.Now, CultureInfo.CurrentCulture));
        }

        public static void ServerErrorLog(string text)
        {
            NAPI.Util.ConsoleOutput("<[RPG-LOG]> ServerERROR: " + text + " - " +
                                    Convert.ToString(DateTime.Now, CultureInfo.CurrentCulture));
        }

        public static void PlayerInfoLog(string text)
        {
            NAPI.Util.ConsoleOutput("<[RPG-LOG]> PlayerINFO: " + text + " - " +
                                    Convert.ToString(DateTime.Now, CultureInfo.CurrentCulture));
        }

        public static void PlayerErrorLog(string text)
        {
            NAPI.Util.ConsoleOutput("<[RPG-LOG]> PlayerERROR: " + text + " - " +
                                    Convert.ToString(DateTime.Now, CultureInfo.CurrentCulture));
        }

        public static void PlayerPosInfoLog(string text)
        {
            NAPI.Util.ConsoleOutput("<[RPG-LOG]> PlayerPositionINFO: " + text + " - " +
                                    Convert.ToString(DateTime.Now, CultureInfo.CurrentCulture));
        }

        public static void PlayerPosErrorLog(string text)
        {
            NAPI.Util.ConsoleOutput("<[RPG-LOG]> PlayerPositionERROR: " + text + " - " +
                                    Convert.ToString(DateTime.Now, CultureInfo.CurrentCulture));
        }

        public static void PlayerPaycheckInfoLog(string text)
        {
            NAPI.Util.ConsoleOutput("<[RPG-LOG]> PlayerPaycheckINFO: " + text + " - " +
                                    Convert.ToString(DateTime.Now, CultureInfo.CurrentCulture));
        }

        public static void PlayerPaycheckErrorLog(string text)
        {
            NAPI.Util.ConsoleOutput("<[RPG-LOG]> PlayerPaycheckERROR: " + text + " - " +
                                    Convert.ToString(DateTime.Now, CultureInfo.CurrentCulture));
        }

        public static void PlayerBiologyInfoLog(string text)
        {
            NAPI.Util.ConsoleOutput("<[RPG-LOG]> PlayerBiologyINFO: " + text + " - " +
                                    Convert.ToString(DateTime.Now, CultureInfo.CurrentCulture));
        }

        public static void PlayerBiologyErrorLog(string text)
        {
            NAPI.Util.ConsoleOutput("<[RPG-LOG]> PlayerBiologyERROR: " + text + " - " +
                                    Convert.ToString(DateTime.Now, CultureInfo.CurrentCulture));
        }

        public static void VehicleFuelInfoLog(string text)
        {
            NAPI.Util.ConsoleOutput("<[RPG-LOG]> VehicleFuelINFO: " + text + " - " +
                                    Convert.ToString(DateTime.Now, CultureInfo.CurrentCulture));
        }

        public static void VehicleFuelErrorLog(string text)
        {
            NAPI.Util.ConsoleOutput("<[RPG-LOG]> VehicleFuelERROR: " + text + " - " +
                                    Convert.ToString(DateTime.Now, CultureInfo.CurrentCulture));
        }

        public static void LocationInfoLog(string text)
        {
            NAPI.Util.ConsoleOutput("<[RPG-LOG]> LocationINFO: " + text + " - " +
                                    Convert.ToString(DateTime.Now, CultureInfo.CurrentCulture));
        }

        public static void LocationErrorLog(string text)
        {
            NAPI.Util.ConsoleOutput("<[RPG-LOG]> LocationERROR: " + text + " - " +
                                    Convert.ToString(DateTime.Now, CultureInfo.CurrentCulture));
        }

        public static void InventoryInfoLog(string text)
        {
            NAPI.Util.ConsoleOutput("<[RPG-LOG]> InventoryINFO: " + text + " - " +
                                    Convert.ToString(DateTime.Now, CultureInfo.CurrentCulture));
        }

        public static void InventoryErrorLog(string text)
        {
            NAPI.Util.ConsoleOutput("<[RPG-LOG]> InventoryERROR: " + text + " - " +
                                    Convert.ToString(DateTime.Now, CultureInfo.CurrentCulture));
        }
    }
}