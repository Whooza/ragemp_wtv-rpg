﻿using GTANetworkAPI;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RolePlayGameServer.Database.Controllers;
using RolePlayGameServer.Players;
using RolePlayGameServer.Util;
using System;
using System.Threading.Tasks;

namespace RolePlayGameServer.Api.Player
{
    [Route("api/[controller]")]
    [ApiController]
    public class InventoryController : ControllerBase
    {
        [HttpGet("[action]")]
        public async Task<IActionResult> GetInventory([FromHeader] string token, [FromHeader] string social)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                return Ok(JsonConvert.SerializeObject(await RolePlayDbController.GetPlayerInvList(social)
                    .ConfigureAwait(false)));
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"[apiError]: Api/Inventory/GetInventory - {e.Message}");
                return BadRequest();
            }
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> UseInventoryItem([FromHeader] string token, [FromHeader] string social,
            [FromHeader] uint itemName)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                var player = await ApiPlayer.GetClientOrDefault(social).ConfigureAwait(false);

                if (player == default) return BadRequest();

                await Task.Run(() => new PlayerInventory(player).UseInvItem(itemName))
                    .ConfigureAwait(false);

                return Ok(JsonConvert.SerializeObject(await RolePlayDbController.GetPlayerInvList(social)
                    .ConfigureAwait(false)));
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"[apiError]: Api/Inventory/UseInventoryItem - {e.Message}");
                return BadRequest();
            }
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> AddInventoryItem([FromHeader] string token, [FromHeader] string social,
            [FromHeader] uint itemName, [FromHeader] uint amount)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                var player = await ApiPlayer.GetClientOrDefault(social).ConfigureAwait(false);

                if (player == default) return BadRequest();

                await Task.Run(() => new PlayerInventory(player).AddItem(itemName, amount)).ConfigureAwait(false);

                return Ok(JsonConvert.SerializeObject(await RolePlayDbController.GetPlayerInvList(social)
                    .ConfigureAwait(false)));
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"[apiError]: Api/Inventory/AddInventoryItem - {e.Message}");
                return BadRequest();
            }
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> DestroyInventoryItem([FromHeader] string token, [FromHeader] string social,
            [FromHeader] uint itemName)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                var player = await ApiPlayer.GetClientOrDefault(social).ConfigureAwait(false);

                if (player == default) return BadRequest();

                await Task.Run(() => new PlayerInventory(player).DestroyInvItem(itemName))
                    .ConfigureAwait(false);

                return Ok(JsonConvert.SerializeObject(await RolePlayDbController.GetPlayerInvList(social)
                    .ConfigureAwait(false)));
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"[apiError]: Api/Inventory/DestroyInventoryItem - {e.Message}");
                return BadRequest();
            }
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> GiveInventoryItem([FromHeader] string token, [FromHeader] string social,
            [FromHeader] uint itemName, [FromHeader] string targetSocial, [FromHeader] uint amount)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                var player = await ApiPlayer.GetClientOrDefault(social)
                    .ConfigureAwait(false);

                if (player == default) return BadRequest();

                var target = await ApiPlayer.GetClientOrDefault(targetSocial)
                    .ConfigureAwait(false);

                if (target == default) return BadRequest();

                await Task.Run(() => new PlayerInventory(player).GiveInvItem(target, itemName, amount))
                    .ConfigureAwait(false);

                return Ok();
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"[apiError]: Api/Inventory/GiveInventoryItem - {e.Message}");
                return BadRequest();
            }
        }
    }
}