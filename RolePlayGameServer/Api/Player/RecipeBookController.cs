﻿using GTANetworkAPI;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RolePlayGameServer.Database.Controllers;
using RolePlayGameServer.Util;
using System;
using System.Threading.Tasks;

namespace RolePlayGameServer.Api.Player
{
    [Route("api/[controller]")]
    [ApiController]
    public class RecipeBookController : ControllerBase
    {
        [HttpGet("[action]")]
        public async Task<IActionResult> GetRecipeBook([FromHeader] string token, [FromHeader] string social)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                return Ok(JsonConvert.SerializeObject(await RolePlayDbController.GetPlayerRecipeBook(social)
                    .ConfigureAwait(false)));
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"[apiError]: Api/RecipeBook/GetRecipeBook - {e.Message}");
                return BadRequest();
            }
        }
    }
}