﻿using GTANetworkAPI;
using Microsoft.AspNetCore.Mvc;
using RolePlayGameServer.Util;
using System.Threading.Tasks;

namespace RolePlayGameServer.Api.Global
{
    [Route("api/[controller]")]
    [ApiController]
    public class WindowController : ControllerBase
    {
        [HttpGet("[action]")]
        public async Task<IActionResult> DestroyWindow([FromHeader] string token, [FromHeader] string social)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                var player = await ApiPlayer.GetClientOrDefault(social).ConfigureAwait(false);

                if (player == default) return BadRequest();

                UiHelper.DestroyBrowserWindow(player);

                player.TriggerEvent("setClothingShopCam", 0);

                return Ok();
            }
            catch
            {
                NAPI.Util.ConsoleOutput("[apiError]: Api/Window/DestroyWindow");
                return BadRequest();
            }
        }
    }
}