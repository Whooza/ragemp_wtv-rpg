﻿using GTANetworkAPI;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RolePlayGameServer.Database.Controllers;
using RolePlayGameServer.Util;
using System;
using System.Threading.Tasks;

namespace RolePlayGameServer.Api.Global
{
    [Route("api/[controller]")]
    [ApiController]
    public class MarketSystemController : ControllerBase
    {
        [HttpGet("[action]")]
        public async Task<IActionResult> GetPricesByMarkType([FromHeader] string token, [FromHeader] string social,
            [FromHeader] uint markType)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                var player = await ApiPlayer.GetClientOrDefault(social).ConfigureAwait(false);

                if (player == default) return BadRequest();
                if (markType == default) return NotFound();

                return Ok(JsonConvert.SerializeObject(await RolePlayDbController.GetCurrentPriceByTrader(markType)
                    .ConfigureAwait(false)));
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"[apiError]: Api/MarketSystem/GetPricesByMarkType - {e.Message}");
                return NotFound();
            }
        }
    }
}