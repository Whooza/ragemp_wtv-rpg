﻿using GTANetworkAPI;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RolePlayGameServer.ServerLists;
using RolePlayGameServer.Util;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RolePlayGameServer.Api.Global
{
    [Route("api/[controller]")]
    [ApiController]
    public class MarkDataController : ControllerBase
    {
        [HttpGet("[action]")]
        public async Task<IActionResult> GetMarkData([FromHeader] string token, [FromHeader] string social)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                var player = await ApiPlayer.GetClientOrDefault(social).ConfigureAwait(false);

                if (player == default) return BadRequest();

                var markData = LocationList.LocationPlayers.FirstOrDefault(x => x.Key == social).Value;

                if (markData == default) return NotFound();

                return Ok(JsonConvert.SerializeObject(markData));
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"[apiError]: Api/MarkData/GetMarkData - {e.Message}");
                return BadRequest();
            }
        }
    }
}