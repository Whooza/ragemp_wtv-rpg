﻿using GTANetworkAPI;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RolePlayGameData.Models;
using RolePlayGameServer.Util;
using System;
using System.Threading.Tasks;

namespace RolePlayGameServer.Api.Global
{
    [Route("api/[controller]")]
    [ApiController]
    public class InformationController : ControllerBase
    {
        [HttpGet("[action]")]
        public async Task<IActionResult> GetCurrentInfo([FromHeader] string token, [FromHeader] string social)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                var player = await ApiPlayer.GetClientOrDefault(social).ConfigureAwait(false);

                if (player == default) return BadRequest();

                var info = new UiInfoModel
                {
                    Head = "Benutzeroberfläche",
                    Body = "Derzeit kann es zu Problemen in Verbindung mit dem Ui kommen.",
                    CreationDate = DateTime.Now
                };

                return Ok(JsonConvert.SerializeObject(info));
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"[apiError]: Api/Information/GetCurrentInfo - {e.Message}");
                return BadRequest();
            }
        }
    }
}