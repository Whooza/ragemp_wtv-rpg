﻿using GTANetworkAPI;
using Microsoft.AspNetCore.Mvc;
using RolePlayGameServer.Players;
using RolePlayGameServer.Util;
using System;
using System.Threading.Tasks;

namespace RolePlayGameServer.Api.Account
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        [HttpGet("[action]")]
        public async Task<IActionResult> LoginPlayer([FromHeader] string token, [FromHeader] string social,
            [FromHeader] string pass)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                var player = await ApiPlayer.GetClientOrDefault(social).ConfigureAwait(false);

                if (player == default) return BadRequest();

                if (await Task.Run(() => new PlayerLogin(player).LoginRequestAsync(pass)).ConfigureAwait(false)) return Ok();

                return Unauthorized();
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"[apiError]: Api/Login/LoginPlayer - {e.Message}");
                return BadRequest();
            }
        }
    }
}