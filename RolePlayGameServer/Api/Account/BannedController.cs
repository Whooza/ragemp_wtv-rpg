﻿using GTANetworkAPI;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RolePlayGameData.Models;
using RolePlayGameServer.Database.Controllers;
using RolePlayGameServer.Util;
using System;
using System.Threading.Tasks;

namespace RolePlayGameServer.Api.Account
{
    [Route("api/[controller]")]
    [ApiController]
    public class BannedController : ControllerBase
    {
        [HttpGet("[action]")]
        public async Task<IActionResult> GetBanInfo([FromHeader] string token, [FromHeader] string social)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                var whiteList = await RolePlayDbController.GetServerWhitelistEntry(social).ConfigureAwait(false);

                if (whiteList == null) return NotFound();

                return Ok(JsonConvert.SerializeObject(new BanInfoModel
                {
                    BanReason = whiteList.BanReason,
                    BanTime = whiteList.BannedUntil.ToString("dd/MM/yyyy")
                }));
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"[apiError]: Api/Banned/GetBanInfo - {e.Message}");
                return BadRequest();
            }
        }
    }
}