﻿using GTANetworkAPI;
using Microsoft.AspNetCore.Mvc;
using RolePlayGameServer.Players;
using RolePlayGameServer.Util;
using System;
using System.Threading.Tasks;

namespace RolePlayGameServer.Api.Account
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegisterController : ControllerBase
    {
        [HttpGet("[action]")]
        public async Task<IActionResult> RegisterPlayer([FromHeader] string token, [FromHeader] string social,
            [FromHeader] string mail, [FromHeader] string pass)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                var player = await ApiPlayer.GetClientOrDefault(social).ConfigureAwait(false);

                if (player == default) return BadRequest();

                if (await Task.Run(() => new PlayerRegistration(player).RegisterRequestAsync(mail, pass))) return Ok();

                return Unauthorized();
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"[apiError]: Api/Register/RegisterPlayer - {e.Message}");
                return BadRequest();
            }
        }
    }
}