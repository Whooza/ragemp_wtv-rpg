﻿using GTANetworkAPI;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RolePlayGameData.Configs;
using RolePlayGameData.Database.RolePlayModels;
using RolePlayGameData.EntityDataConsts;
using RolePlayGameServer.Database.Controllers;
using RolePlayGameServer.Market;
using RolePlayGameServer.Players;
using RolePlayGameServer.Util;
using RolePlayGameServer.Vehicles;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RolePlayGameServer.Api.Job
{
    [Route("api/[controller]")]
    [ApiController]
    public class TraderController : ControllerBase
    {
        [HttpGet("[action]")]
        public async Task<IActionResult> GetInventory([FromHeader] string token, [FromHeader] string social)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                return Ok(JsonConvert.SerializeObject(await RolePlayDbController.GetPlayerInvList(social).ConfigureAwait(false)));
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"[apiError]: Api/Trader/GetInventory - {e.Message}");
                return BadRequest();
            }
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> GetVehicleHandleValue([FromHeader] string token, [FromHeader] string social)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                var player = await ApiPlayer.GetClientOrDefault(social).ConfigureAwait(false);

                if (player == default) return NotFound();

                var vehicle = await NextEntity.NextVehicle(player, 25f).ConfigureAwait(false);

                return Ok(JsonConvert.SerializeObject(vehicle.Value));
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"[apiError]: Api/Trader/GetVehicleHandleValue - {e.Message}");
                return BadRequest();
            }
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> SellTraderItems([FromHeader] string token, [FromHeader] string social, [FromHeader] string invType,
            [FromHeader] int vehicleHandle, [FromHeader] uint itemName, [FromHeader] uint amount)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                var player = await ApiPlayer.GetClientOrDefault(social).ConfigureAwait(false);

                if (player == default) return NotFound();

                var vehicle = await ApiVehicle.GetVehicleOrDefault(vehicleHandle).ConfigureAwait(false);

                if (vehicle == default) return NotFound();

                var itemCfg = ItemCfg.ItemConfig.FirstOrDefault(x => (uint)x.ItemName == itemName);

                switch (invType)
                {
                    case "player":
                        await new PlayerInventory(player).DestroyInvItem(itemName, amount).ConfigureAwait(false);

                        var playerInv = await RolePlayDbController.GetPlayerInvList(social).ConfigureAwait(false);

                        await new PlayerMoney(player).AddCashMoney(itemCfg.DefaultPrice * amount).ConfigureAwait(false);

                        await Task.Run(() => MarketSystem.Instance.AddItem((uint)itemCfg.ItemName, (uint)itemCfg.ItemType, amount, true)).ConfigureAwait(false);

                        return Ok(JsonConvert.SerializeObject(playerInv));
                    case "vehicle":
                        await new VehicleInventory(vehicle, player).DestroyInvItem(itemName, amount).ConfigureAwait(false);

                        var vehInv = await RolePlayDbController.GetVehicleInventoryList(vehicle).ConfigureAwait(false);

                        await new PlayerMoney(player).AddCashMoney(itemCfg.DefaultPrice * amount).ConfigureAwait(false);

                        await Task.Run(() => MarketSystem.Instance.AddItem((uint)itemCfg.ItemName, (uint)itemCfg.ItemType, amount, true)).ConfigureAwait(false);

                        return Ok(JsonConvert.SerializeObject(vehInv));
                    default:
                        return BadRequest();
                }
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"[apiError]: Api/Trader/SellTraderItems - {e.Message}");
                return BadRequest();
            }
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> FindNextVehicleInventory([FromHeader] string token, [FromHeader] string social)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                var player = await ApiPlayer.GetClientOrDefault(social).ConfigureAwait(false);

                if (player == default) return NotFound();

                try
                {
                    var nextVehicle = await NextEntity.NextVehicle(player, 25f).ConfigureAwait(false);

                    if (nextVehicle == null) return Ok();

                    var vehData = (VehicleModel)nextVehicle.GetData(VehicleDataConst.CivVehicleData);

                    var vehInv = await RolePlayDbController.GetVehicleInventoryList(vehData.Id).ConfigureAwait(false);

                    return Ok(JsonConvert.SerializeObject(vehInv));
                }
                catch
                {
                    return Ok();
                }
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"[apiError]: Api/Trader/FindNextVehicleInventory - {e.Message}");
                return BadRequest();
            }
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> GetPricesByMarkType([FromHeader] string token, [FromHeader] string social,
            [FromHeader] uint markType)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                var player = await ApiPlayer.GetClientOrDefault(social).ConfigureAwait(false);

                if (player == default) return BadRequest();
                if (markType == default) return NotFound();

                return Ok(JsonConvert.SerializeObject(await RolePlayDbController.GetCurrentPriceByTrader(markType).ConfigureAwait(false)));
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"[apiError]: Api/Trader/GetPricesByMarkType - {e.Message}");
                return NotFound();
            }
        }
    }
}