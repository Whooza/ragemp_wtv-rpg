﻿using GTANetworkAPI;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RolePlayGameData.Configs;
using RolePlayGameData.Models;
using RolePlayGameServer.Database.Controllers;
using RolePlayGameServer.Players;
using RolePlayGameServer.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RolePlayGameServer.Api.Job
{
    [Route("api/[controller]")]
    [ApiController]
    public class CookingController : ControllerBase
    {
        [HttpGet("[action]")]
        public async Task<IActionResult> GetCooking([FromHeader] string token, [FromHeader] string social)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                var playerCooking = await RolePlayDbController.GetPlayerCooking(social)
                    .ConfigureAwait(false);

                return Ok(JsonConvert.SerializeObject(playerCooking));
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"[apiError]: Api/Cooking/GetCooking - {e.Message}");
                return BadRequest();
            }
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> GetInventory([FromHeader] string token, [FromHeader] string social)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                return Ok(JsonConvert.SerializeObject(await RolePlayDbController.GetPlayerInvList(social)
                    .ConfigureAwait(false)));
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"[apiError]: Api/Cooking/GetInventory - {e.Message}");
                return BadRequest();
            }
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> UpdateProcessedItem([FromHeader] string token, [FromHeader] string social,
            [FromHeader] uint itemName, [FromHeader] uint amount)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                var player = await ApiPlayer.GetClientOrDefault(social)
                    .ConfigureAwait(false);

                if (player == default) return BadRequest();

                var RecipeList = new List<CookingRecipeModel>();
                RecipeList.AddRange(CookingCfg.CookingLvLOneRecipes);
                RecipeList.AddRange(CookingCfg.CookingLvLTwoRecipes);
                RecipeList.AddRange(CookingCfg.CookingLvLThreeRecipes);
                RecipeList.AddRange(CookingCfg.CookingLvLFourRecipes);
                RecipeList.AddRange(CookingCfg.CookingLvLFiveRecipes);
                RecipeList.AddRange(CookingCfg.CookingLvLSixRecipes);

                var recipe = RecipeList.FirstOrDefault(x => (uint) x.CookedItemType == itemName);

                foreach (var (MatKey, MatValue) in recipe.MatsList)
                    await new PlayerInventory(player).DestroyInvItem((uint) MatKey, (uint) MatValue * amount)
                        .ConfigureAwait(false);

                await Task.Run(() => new PlayerInventory(player).AddItem((uint) recipe.CookedItemType, amount))
                    .ConfigureAwait(false);

                return Ok(JsonConvert.SerializeObject(await RolePlayDbController.GetPlayerInvList(player.SocialClubName)
                    .ConfigureAwait(false)));
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"[apiError]: Api/Cooking/UpdateProcessedItem - {e.Message}");
                return BadRequest();
            }
        }
    }
}