﻿using GTANetworkAPI;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RolePlayGameData.Models;
using RolePlayGameServer.Database.Controllers;
using RolePlayGameServer.Dumpsters;
using RolePlayGameServer.Players;
using RolePlayGameServer.ServerLists;
using RolePlayGameServer.Util;
using System;
using System.Threading.Tasks;

namespace RolePlayGameServer.Api.Server
{
    [Route("api/[controller]")]
    [ApiController]
    public class DumpsterController : ControllerBase
    {
        [HttpGet("[action]")]
        public async Task<IActionResult> GetDumpster([FromHeader] string token, [FromHeader] string social)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                var player = await ApiPlayer.GetClientOrDefault(social).ConfigureAwait(false);

                if (player == default) return BadRequest();

                if (!LocationList.LocationPlayers.TryGetValue(social, out MarkDataModel data)) return NotFound();

                return Ok(JsonConvert.SerializeObject(await MapObjectDbController.GetDumpster((int)data.MarkId).ConfigureAwait(false)));
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"[apiError]: Api/Dumpster/GetDumpster - {e.Message}");
                return BadRequest();
            }
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> GetInventory([FromHeader] string token, [FromHeader] string social)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                return Ok(JsonConvert.SerializeObject(await RolePlayDbController.GetPlayerInvList(social).ConfigureAwait(false)));
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"[apiError]: Api/Dumpster/GetInventory - {e.Message}");
                return BadRequest();
            }
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> ToDumpster([FromHeader] string token, [FromHeader] string social, [FromHeader] uint itemName, [FromHeader] uint amount)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                var player = await ApiPlayer.GetClientOrDefault(social).ConfigureAwait(false);

                if (player == default) return BadRequest();

                if (!LocationList.LocationPlayers.TryGetValue(social, out MarkDataModel data)) return NotFound();

                if(await DumpsterManager.Instance.AddItem(player, data, itemName, amount).ConfigureAwait(false))
                {
                    await new PlayerInventory(player).DestroyInvItem(itemName, amount).ConfigureAwait(false);
                    return Ok();
                }

                return Ok();
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"[apiError]: Api/Dumpster/ToDumpster - {e.Message}");
                return BadRequest();
            }
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> ToInvntory([FromHeader] string token, [FromHeader] string social, [FromHeader] uint itemName, [FromHeader] uint amount)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                var player = await ApiPlayer.GetClientOrDefault(social).ConfigureAwait(false);

                if (player == default) return BadRequest();

                if (!LocationList.LocationPlayers.TryGetValue(social, out MarkDataModel data)) return NotFound();

                if(await new PlayerInventory(player).AddItem(itemName, amount).ConfigureAwait(false))
                {
                    await DumpsterManager.Instance.RemoveItem(player, data, itemName, amount).ConfigureAwait(false);
                    return Ok();
                }

                return Ok();
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"[apiError]: Api/Dumpster/ToInvntory - {e.Message}");
                return BadRequest();
            }
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> CloseDumpster([FromHeader] string token, [FromHeader] string social)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                var player = await ApiPlayer.GetClientOrDefault(social).ConfigureAwait(false);

                if (player == default) return BadRequest();

                if (!LocationList.LocationPlayers.TryGetValue(social, out MarkDataModel data)) return NotFound();

                await Task.Run(() => DumpsterManager.Instance.CloseDumpster(data)).ConfigureAwait(false);

                UiHelper.DestroyBrowserWindow(player);

                return Ok();
            }
            catch
            {
                NAPI.Util.ConsoleOutput("[apiError]: Api/Dumpster/CloseDumpster");
                return BadRequest();
            }
        }
    }
}