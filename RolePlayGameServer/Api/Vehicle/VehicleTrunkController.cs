﻿using GTANetworkAPI;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RolePlayGameData.Database.RolePlayModels;
using RolePlayGameData.EntityDataConsts;
using RolePlayGameData.Models;
using RolePlayGameServer.Database.Controllers;
using RolePlayGameServer.Players;
using RolePlayGameServer.Util;
using RolePlayGameServer.Vehicles;
using System;
using System.Threading.Tasks;

namespace RolePlayGameServer.Api.Vehicle
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehicleTrunkController : ControllerBase
    {
        [HttpGet("[action]")]
        public async Task<IActionResult> MoveItem([FromHeader] string token, [FromHeader] string social,
            [FromHeader] uint itemName, [FromHeader] uint amount, [FromHeader] string toVehicle)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                var player = await ApiPlayer.GetClientOrDefault(social)
                    .ConfigureAwait(false);

                if (player == default) return NotFound();

                try
                {
                    var nextVehicle = await NextEntity.NextVehicle(player, 2f)
                        .ConfigureAwait(false);

                    if (nextVehicle == null) return Ok();

                    var vehData = (VehicleModel) nextVehicle.GetData(VehicleDataConst.CivVehicleData);

                    if (Convert.ToBoolean(toVehicle))
                    {
                        await new PlayerInventory(player).DestroyInvItem(itemName, amount)
                            .ConfigureAwait(false);

                        await new VehicleInventory(nextVehicle, player).AddItem(itemName, amount)
                            .ConfigureAwait(false);
                    }
                    else
                    {
                        await new VehicleInventory(nextVehicle, player).DestroyInvItem(itemName, amount)
                            .ConfigureAwait(false);

                        await new PlayerInventory(player).AddItem(Convert.ToUInt32(itemName), amount)
                            .ConfigureAwait(false);
                    }

                    var vehInv = await RolePlayDbController.GetVehicleInventoryList(vehData.Id)
                        .ConfigureAwait(false);

                    return Ok(JsonConvert.SerializeObject(vehInv));
                }
                catch
                {
                    return Ok();
                }
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"[apiError]: Api/VehicleTrunk/MoveItem - {e.Message}");
                return BadRequest();
            }
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> FindNextVehicleInventory([FromHeader] string token, [FromHeader] string social)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                var player = await ApiPlayer.GetClientOrDefault(social)
                    .ConfigureAwait(false);

                if (player == default) return NotFound();

                try
                {
                    var nextVehicle = await NextEntity.NextVehicle(player, 5f)
                        .ConfigureAwait(false);

                    if (nextVehicle == null) return Ok();

                    var vehData = (VehicleModel) nextVehicle.GetData(VehicleDataConst.CivVehicleData);

                    var vehInv = await RolePlayDbController.GetVehicleInventoryList(vehData.Id)
                        .ConfigureAwait(false);

                    return Ok(JsonConvert.SerializeObject(vehInv));
                }
                catch
                {
                    return Ok();
                }
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"[apiError]: Api/VehicleTrunk/FindNextVehicleInventory - {e.Message}");
                return BadRequest();
            }
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> GetInventory([FromHeader] string token, [FromHeader] string social)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                return Ok(JsonConvert.SerializeObject(await RolePlayDbController.GetPlayerInvList(social)
                    .ConfigureAwait(false)));
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"[apiError]: Api/VehicleTrunk/GetInventory - {e.Message}");
                return BadRequest();
            }
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> GetVehicleInfoModel([FromHeader] string token, [FromHeader] string social)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                var player = await ApiPlayer.GetClientOrDefault(social).ConfigureAwait(false);

                if (player == default) return NotFound();

                var nextVehicle = await NextEntity.NextVehicle(player, 5f).ConfigureAwait(false);

                if (nextVehicle == null) return NotFound();

                return Ok(JsonConvert.SerializeObject(new ApiVehicleInfoModel
                {
                    DisplayedName = nextVehicle.DisplayName,
                    NumberPlate = nextVehicle.NumberPlate,
                    EngineStatus = nextVehicle.EngineStatus,
                    Locked = nextVehicle.Locked,
                    Model = nextVehicle.Model,
                    EntityValue = nextVehicle.Value
                }));
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"[apiError]: Api/VehicleTrunk/GetVehicleInfoModel - {e.Message}");
                return BadRequest();
            }
        }
    }
}