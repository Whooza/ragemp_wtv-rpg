﻿using GTANetworkAPI;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RolePlayGameData.Database.RolePlayModels;
using RolePlayGameData.EntityDataConsts;
using RolePlayGameData.Models;
using RolePlayGameServer.Players;
using RolePlayGameServer.Util;
using RolePlayGameServer.Vehicles;
using System;
using System.Threading.Tasks;

namespace RolePlayGameServer.Api.Vehicle
{
    [Route("api/[controller]")]
    [ApiController]
    public class FuelStationController : ControllerBase
    {
        [HttpGet("[action]")]
        public async Task<IActionResult> GetVehicleData([FromHeader] string token, [FromHeader] string social)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                var player = await ApiPlayer.GetClientOrDefault(social).ConfigureAwait(false);

                if (player == default) return NotFound();

                var vehicle = await NextEntity.NextVehicle(player, 5f).ConfigureAwait(false);

                if (vehicle != null) return Ok(JsonConvert.SerializeObject(new FuelStationModel
                {
                    Vehicle = (VehicleModel)vehicle.GetData(VehicleDataConst.CivVehicleData),
                    VehFuel = (VehicleFuelModel)vehicle.GetData(VehicleDataConst.VehicleFuel),
                    PlayerMoney = (PlayerMoneyModel)player.GetData(PlayerDataConst.PlayerMoney)
                }));
                else return NoContent();
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"[apiError]: Api/FuelStation/GetVehicleData - {e.Message}");
                return BadRequest();
            }
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> PayFuel([FromHeader] string token, [FromHeader] string social,
            [FromHeader] float amount, [FromHeader] float price, [FromHeader] bool wrongFuel)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                var player = await ApiPlayer.GetClientOrDefault(social).ConfigureAwait(false);

                if (player == default) return NotFound("player");

                var vehicle = await NextEntity.NextVehicle(player, 5f).ConfigureAwait(false);

                if (vehicle == null) return NotFound("vehicle");

                vehicle.SetData(VehicleDataConst.IsRefueling, true);

                await new PlayerMoney(player).RemoveCashMoney(price).ConfigureAwait(false);
                await new VehicleFuelStation(player, vehicle).RefuelVehicle(amount).ConfigureAwait(false);

                vehicle.ResetData(VehicleDataConst.IsRefueling);

                UiHelper.DestroyBrowserWindow(player);

                return Ok();
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"[apiError]: Api/FuelStation/PayFuel - {e.Message}");
                return BadRequest();
            }
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> CloseFuelStation([FromHeader] string token, [FromHeader] string social)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                var player = await ApiPlayer.GetClientOrDefault(social).ConfigureAwait(false);

                if (player == default) return NotFound();

                // TODO: if amount and price not 0f => msg to police

                UiHelper.DestroyBrowserWindow(player);

                return Ok();
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"[apiError]: Api/FuelStation/CloseFuelStation - {e.Message}");
                return BadRequest();
            }
        }
    }
}