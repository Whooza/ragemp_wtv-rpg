﻿using GTANetworkAPI;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RolePlayGameServer.Database.Controllers;
using RolePlayGameServer.Util;
using RolePlayGameServer.Vehicles;
using System;
using System.Threading.Tasks;

namespace RolePlayGameServer.Api.Vehicle
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehicleBuyController : ControllerBase
    {
        [HttpGet("[action]")]
        public async Task<IActionResult> GetNewVehicleData([FromHeader] string token, [FromHeader] string social,
            [FromHeader] uint markName)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                var player = await ApiPlayer.GetClientOrDefault(social)
                    .ConfigureAwait(false);

                if (player == default) return BadRequest();

                var vehicleData = await MapObjectDbController.GetVehicleShopPosFromDb(markName)
                    .ConfigureAwait(false);

                if (vehicleData == default) return NotFound();

                return Ok(JsonConvert.SerializeObject(vehicleData));
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"[apiError]: Api/VehicleBuy/GetNewVehicleData - {e.Message}");
                return BadRequest();
            }
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> BuyVehicle([FromHeader] string token, [FromHeader] string social,
            [FromHeader] uint markName)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                var player = await ApiPlayer.GetClientOrDefault(social).ConfigureAwait(false);

                if (player == default) return NotFound();

                NAPI.Task.Run(() => new VehicleShop().BuyShopVehicle(player, markName));
                UiHelper.DestroyBrowserWindow(player);

                return Ok();
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"[apiError]: Api/VehicleBuy/BuyVehicle - {e.Message}");
                return BadRequest();
            }
        }
    }
}