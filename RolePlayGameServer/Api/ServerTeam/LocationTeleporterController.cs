﻿using GTANetworkAPI;
using Microsoft.AspNetCore.Mvc;
using RolePlayGameData.Configs;
using RolePlayGameServer.Util;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RolePlayGameServer.Api.ServerTeam
{
    [Route("api/[controller]")]
    [ApiController]
    public class LocationTeleporterController : ControllerBase
    {
        [HttpGet("[action]")]
        public async Task<IActionResult> TeleportToLocation([FromHeader] string token, [FromHeader] string social,
            [FromHeader] string location)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                var player = await ApiPlayer.GetClientOrDefault(social).ConfigureAwait(false);

                if (player == default) return BadRequest();

                if (!new RankChecker(player).CheckPlayerIsAdmin(1)) return Unauthorized();

                var pos = LocationCfg.LocationList.FirstOrDefault(x => x.Key == location);

                if (pos.Key == default) return BadRequest();

                NAPI.Task.Run(() => NAPI.Entity.SetEntityPosition(player, pos.Value.LocationPosition));

                return Ok();
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"[apiError]: Api/LocationTeleporter/TeleportToLocation - {e.Message}");
                return BadRequest();
            }
        }
    }
}