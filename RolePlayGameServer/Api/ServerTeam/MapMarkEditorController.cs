﻿using GTANetworkAPI;
using Microsoft.AspNetCore.Mvc;
using RolePlayGameData.Configs.Vehicle;
using RolePlayGameData.Database.RolePlayModels;
using RolePlayGameData.EntityDataConsts;
using RolePlayGameData.Texts;
using RolePlayGameServer.Database.Controllers;
using RolePlayGameServer.Dumpsters;
using RolePlayGameServer.Locations;
using RolePlayGameServer.Market;
using RolePlayGameServer.Players;
using RolePlayGameServer.Util;
using RolePlayGameServer.Vehicles;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RolePlayGameServer.Api.ServerTeam
{
    [Route("api/[controller]")]
    [ApiController]
    public class MapMarkEditorController : ControllerBase
    {
        [HttpGet("[action]")]
        public async Task<IActionResult> CreateMark([FromHeader] string token, [FromHeader] string social,
            [FromHeader] uint faction, [FromHeader] uint markType)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                var player = await ApiPlayer.GetClientOrDefault(social).ConfigureAwait(false);

                if (player == default) return BadRequest();

                if (!new RankChecker(player).CheckPlayerIsAdmin(1)) return Unauthorized();

                new LocationCreator(player).CreateMarkAsync(faction, markType);

                return Ok();
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"[apiError]: Api/MapMarkEditor/CreateMark - {e.Message}");
                return BadRequest();
            }
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> DeleteMark([FromHeader] string token, [FromHeader] string social,
            [FromHeader] int id)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                var player = await ApiPlayer.GetClientOrDefault(social).ConfigureAwait(false);

                if (player == default) return BadRequest();

                if (!new RankChecker(player).CheckPlayerIsAdmin(1)) return Unauthorized();

                new LocationCreator(player).DeleteMarkAsync(id);

                return Ok();
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"[apiError]: Api/MapMarkEditor/DeleteMark - {e.Message}");
                return BadRequest();
            }
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> DeleteDumpster([FromHeader] string token, [FromHeader] string social, [FromHeader] int id)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                var player = await ApiPlayer.GetClientOrDefault(social)
                    .ConfigureAwait(false);

                if (player == default) return BadRequest();

               new DumpsterCreator(player).DeleteDumpster(id);

                return Ok();
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"[apiError]: Api/MapMarkEditor/DeleteDumpster - {e.Message}");
                return BadRequest();
            }
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> ReloadMarks([FromHeader] string token, [FromHeader] string social)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                var player = await ApiPlayer.GetClientOrDefault(social).ConfigureAwait(false);

                if (player == default) return BadRequest();

                if (!new RankChecker(player).CheckPlayerIsAdmin(1)) return Unauthorized();

               new LocationCreator(player).ReloadMarks();

                return Ok();
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"[apiError]: Api/MapMarkEditor/ReloadMarks - {e.Message}");
                return BadRequest();
            }
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> ReloadDumpsters([FromHeader] string token, [FromHeader] string social)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                var player = await ApiPlayer.GetClientOrDefault(social).ConfigureAwait(false);

                if (player == default) return BadRequest();

                if (!new RankChecker(player).CheckPlayerIsAdmin(1)) return Unauthorized();

                new DumpsterCreator(player).ReloadDumpsters();

                return Ok();
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"[apiError]: Api/MapMarkEditor/ReloadDumpsters - {e.Message}");
                return BadRequest();
            }
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> SpawnMapperCar([FromHeader] string token, [FromHeader] string social)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                var player = await ApiPlayer.GetClientOrDefault(social).ConfigureAwait(false);

                if (player == default) return BadRequest();

                if (!new RankChecker(player).CheckPlayerIsAdmin(1)) return Unauthorized();

                NAPI.Task.Run(() => new VehicleSpawner().SpawnMappersCar(player));

                return Ok();
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"[apiError]: Api/MapMarkEditor/SpawnMapperCar - {e.Message}");
                return BadRequest();
            }
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> RepairCar([FromHeader] string token, [FromHeader] string social)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                var player = await ApiPlayer.GetClientOrDefault(social).ConfigureAwait(false);

                if (player == default) return BadRequest();

                if (!new RankChecker(player).CheckPlayerIsAdmin(1)) return Unauthorized();

                var car = player.Vehicle;

                if (car != null)
                {
                    NAPI.Task.Run(() => car.Repair());

                    var vehData = (VehicleModel)car.GetData(VehicleDataConst.CivVehicleData);

                    var vehFuel = (VehicleFuelModel)car.GetData(VehicleDataConst.VehicleFuel);

                    var vehConfig = VehicleCfg.AllVehicles.First(x => (uint)x.VehicleHash == vehData.VehicleHash);

                    vehFuel.TankStatus = vehConfig.TankCapacity;

                    NAPI.Task.Run(() => car.SetSharedData(VehicleDataConst.FuelShared, vehFuel.TankStatus));

                    await RolePlayDbController.UpdateVehicleFuel(vehFuel)
                        .ConfigureAwait(false);

                    return Ok();
                }

                player.SendNotification(PlayerNotificationTexts.NotInVehicle);
                return BadRequest();
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"[apiError]: Api/MapMarkEditor/RepairCar - {e.Message}");
                return BadRequest();
            }
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> ResetBiology([FromHeader] string token, [FromHeader] string social)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                var player = await ApiPlayer.GetClientOrDefault(social).ConfigureAwait(false);

                if (player == default) return BadRequest();

                if (!new RankChecker(player).CheckPlayerIsAdmin(1)) return Unauthorized();

                await PlayerBiology.SetFullBiology(player).ConfigureAwait(false);

                return Ok();
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"[apiError]: Api/MapMarkEditor/ResetBiology - {e.Message}");
                return BadRequest();
            }
        }
    }
}