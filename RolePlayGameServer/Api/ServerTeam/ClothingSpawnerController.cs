﻿using GTANetworkAPI;
using Microsoft.AspNetCore.Mvc;
using RolePlayGameServer.Util;
using System;
using System.Threading.Tasks;

namespace RolePlayGameServer.Api.ServerTeam
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClothingSpawnerController : ControllerBase
    {
        [HttpGet("[action]")]
        public async Task<IActionResult> ChangeClothes([FromHeader] string token, [FromHeader] string social,
            [FromHeader] int slot, [FromHeader] int drawable, [FromHeader] int texture)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                var player = await ApiPlayer.GetClientOrDefault(social).ConfigureAwait(false);

                if (player == default) return BadRequest();

                if (!new RankChecker(player).CheckPlayerIsAdmin(1)) return Unauthorized();

                NAPI.Task.Run(() => player.SetClothes(slot, drawable, texture));

                return Ok();
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"[apiError]: Api/ClothingSpawner/ChangeClothes - {e.Message}");
                return BadRequest();
            }
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> RotateCharacter([FromHeader] string token, [FromHeader] string social,
            [FromHeader] double direction)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                var player = await ApiPlayer.GetClientOrDefault(social).ConfigureAwait(false);

                if (player == default) return BadRequest();

                if (!new RankChecker(player).CheckPlayerIsAdmin(1)) return Unauthorized();

                NAPI.Task.Run(() => NAPI.Entity.SetEntityRotation(player, new Vector3(0, 0, direction)));

                return Ok();
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"[apiError]: Api/ClothingSpawner/RotateCharacter - {e.Message}");
                return BadRequest();
            }
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> ChangeGender([FromHeader] string token, [FromHeader] string social,
            [FromHeader] string gender)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                var player = await ApiPlayer.GetClientOrDefault(social)
                    .ConfigureAwait(false);

                if (player == default) return BadRequest();

                if (!new RankChecker(player).CheckPlayerIsAdmin(1)) return Unauthorized();

                for (var i = 0; i < 11; i++) player.SetClothes(i, 0, 0);

                player.SetSkin(gender == "male" ? NAPI.Util.GetHashKey("mp_m_freemode_01") : NAPI.Util.GetHashKey("mp_f_freemode_01"));

                return Ok();
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"[apiError]: Api/ClothingSpawner/ChangeGender - {e.Message}");
                return BadRequest();
            }
        }
    }
}