﻿using GTANetworkAPI;
using Microsoft.AspNetCore.Mvc;
using RolePlayGameData.Configs;
using RolePlayGameServer.Players;
using RolePlayGameServer.Util;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RolePlayGameServer.Api.ServerTeam
{
    [Route("api/[controller]")]
    [ApiController]
    public class ItemSpawnerController : ControllerBase
    {
        [HttpGet("[action]")]
        public async Task<IActionResult> GiveItem([FromHeader] string token, [FromHeader] string social,
            [FromHeader] uint item)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                var player = await ApiPlayer.GetClientOrDefault(social).ConfigureAwait(false);

                if (player == default) return BadRequest();

                if (!new RankChecker(player).CheckPlayerIsAdmin(1)) return Unauthorized();

                await new PlayerInventory(player).AddItem(item, 1).ConfigureAwait(false);

                return Ok();
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"[apiError]: Api/ItemSpawner/GiveItem - {e.Message}");
                return BadRequest();
            }
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> GiveWeapon([FromHeader] string token, [FromHeader] string social,
            [FromHeader] string weapon)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                var player = await ApiPlayer.GetClientOrDefault(social).ConfigureAwait(false);

                if (player == default) return BadRequest();

                if (!new RankChecker(player).CheckPlayerIsAdmin(1)) return Unauthorized();

                var cfg = WeaponCfg.WeaponConfig.FirstOrDefault(x => x.DisplayedName == weapon);

                if (cfg == default) return BadRequest();

                player.GiveWeapon((WeaponHash)(uint)cfg.Hash, 1000);

                return Ok();
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"[apiError]: Api/ItemSpawner/GiveWeapon - {e.Message}");
                return BadRequest();
            }
        }
    }
}