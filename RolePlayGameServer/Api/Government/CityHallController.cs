﻿using GTANetworkAPI;
using Microsoft.AspNetCore.Mvc;
using RolePlayGameServer.Government;
using RolePlayGameServer.Util;
using System.Threading.Tasks;

namespace RolePlayGameServer.Api.Government
{
    [Route("api/[controller]")]
    [ApiController]
    public class CityHallController : ControllerBase
    {
        [HttpGet("[action]")]
        public async Task<IActionResult> GetIdentityCard([FromHeader] string token, [FromHeader] string social,
            [FromHeader] string firstName, [FromHeader] string lastName, [FromHeader] string nationality)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                var player = await ApiPlayer.GetClientOrDefault(social).ConfigureAwait(false);

                if (player == default) return NotFound();

                var finished = await Task.Run(() => new CityHall(player).CreateIdentyCard(firstName, lastName, nationality))
                    .ConfigureAwait(false);

                if (finished) return Ok();

                return BadRequest();
            }
            catch
            {
                NAPI.Util.ConsoleOutput("[apiError]: Api/CityHall/GetIdentityCard");
                return BadRequest();
            }
        }
    }
}