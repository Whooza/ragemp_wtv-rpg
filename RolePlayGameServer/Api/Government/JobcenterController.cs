﻿using GTANetworkAPI;
using Microsoft.AspNetCore.Mvc;
using RolePlayGameServer.Government;
using RolePlayGameServer.Util;
using System;
using System.Threading.Tasks;

namespace RolePlayGameServer.Api.Government
{
    [Route("api/[controller]")]
    [ApiController]
    public class JobcenterController : ControllerBase
    {
        [HttpGet("[action]")]
        public async Task<IActionResult> ActivateSocialPayment([FromHeader] string token, [FromHeader] string social,
            [FromHeader] string bankName)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                var player = await ApiPlayer.GetClientOrDefault(social).ConfigureAwait(false);

                if (player == default) return NotFound();

                await Task.Run(() => new Jobcenter(player).ActivateSocialPayment(Convert.ToUInt32(bankName)))
                    .ConfigureAwait(false);

                return Ok();
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"[apiError]: Api/Jobcenter/ActivateSocialPayment - {e.Message}");
                return BadRequest();
            }
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> DeactivateSocialPayment([FromHeader] string token, [FromHeader] string social)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                var player = await ApiPlayer.GetClientOrDefault(social).ConfigureAwait(false);

                if (player == default) return NotFound();

                await Task.Run(() => new Jobcenter(player).DeactivateSocialPayment()).ConfigureAwait(false);

                return Ok();
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"[apiError]: Api/Jobcenter/DeactivateSocialPayment - {e.Message}");
                return BadRequest();
            }
        }
    }
}