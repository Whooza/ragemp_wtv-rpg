﻿using GTANetworkAPI;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RolePlayGameData.Models;
using RolePlayGameServer.Database.Controllers;
using RolePlayGameServer.Util;
using System;
using System.Threading.Tasks;

namespace RolePlayGameServer.Api.Character
{
    [Route("api/[controller]")]
    [ApiController]
    public class CharCreatorController : ControllerBase
    {
        [HttpGet("[action]")]
        public async Task<IActionResult> SummitCreator([FromHeader] string token, [FromHeader] string social,
            [FromHeader] string settings)
        {
            try
            {
                if (!TokenChecker.Check(token)) return Unauthorized();

                var player = await ApiPlayer.GetClientOrDefault(social)
                    .ConfigureAwait(false);

                if (player == default) return BadRequest();

                var settingsModel = JsonConvert.DeserializeObject<CharSettingsModel>(settings);

                var playerSettings = await RolePlayDbController.GetPlayerCharSettings(player.SocialClubName)
                    .ConfigureAwait(false);

                playerSettings.FirstHeadShape = settingsModel.FirstHeadShape;
                playerSettings.SecondHeadShape = settingsModel.SecondHeadShape;
                playerSettings.FirstSkinTone = settingsModel.FirstSkinTone;
                playerSettings.SecondSkinTone = settingsModel.SecondSkinTone;
                playerSettings.HeadMix = settingsModel.HeadMix;
                playerSettings.SkinMix = settingsModel.SkinMix;
                playerSettings.HairModel = settingsModel.HairModel;
                playerSettings.FirstHairColor = settingsModel.FirstHairColor;
                playerSettings.SecondHairColor = settingsModel.SecondHairColor;
                playerSettings.BeardModel = settingsModel.BeardModel;
                playerSettings.BeardColor = settingsModel.BeardColor;
                playerSettings.ChestModel = settingsModel.ChestModel;
                playerSettings.ChestColor = settingsModel.ChestColor;
                playerSettings.BlemishesModel = settingsModel.BlemishesModel;
                playerSettings.AgingModel = settingsModel.AgingModel;
                playerSettings.ComplexionModel = settingsModel.ComplexionModel;
                playerSettings.SunDamageModel = settingsModel.SunDamageModel;
                playerSettings.FrecklesModel = settingsModel.FrecklesModel;
                playerSettings.NoseWidth = settingsModel.NoseWidth;
                playerSettings.NoseHeight = settingsModel.NoseHeight;
                playerSettings.NoseLength = settingsModel.NoseLength;
                playerSettings.NoseBridge = settingsModel.NoseBridge;
                playerSettings.NoseTip = settingsModel.NoseTip;
                playerSettings.NoseShift = settingsModel.NoseShift;
                playerSettings.BrowHeight = settingsModel.BrowHeight;
                playerSettings.BrowWidth = settingsModel.BrowWidth;
                playerSettings.CheekboneHeight = settingsModel.CheekboneHeight;
                playerSettings.CheekboneWidth = settingsModel.CheekboneWidth;
                playerSettings.CheeksWidth = settingsModel.CheeksWidth;
                playerSettings.Eyes = settingsModel.Eyes;
                playerSettings.Lips = settingsModel.Lips;
                playerSettings.JawWidth = settingsModel.JawWidth;
                playerSettings.JawHeight = settingsModel.JawHeight;
                playerSettings.ChinLength = settingsModel.ChinLength;
                playerSettings.ChinPosition = settingsModel.ChinPosition;
                playerSettings.ChinShape = settingsModel.ChinShape;
                playerSettings.ChinWidth = settingsModel.ChinWidth;
                playerSettings.NeckWidth = settingsModel.NeckWidth;
                playerSettings.EyesColor = settingsModel.EyesColor;
                playerSettings.EyebrowsModel = settingsModel.EyebrowsModel;
                playerSettings.EyebrowsColor = settingsModel.EyebrowsColor;
                playerSettings.MakeupModel = settingsModel.MakeupModel;
                playerSettings.BlushModel = settingsModel.BlushModel;
                playerSettings.BlushColor = settingsModel.BlushColor;
                playerSettings.LipstickModel = settingsModel.LipstickModel;
                playerSettings.LipstickColor = settingsModel.LipstickColor;

                await RolePlayDbController.UpdatePlayerCharSettings(playerSettings).ConfigureAwait(false);

                var playerAcc = await RolePlayDbController.GetPlayerAccBySocial(social).ConfigureAwait(false);

                playerAcc.CharCreatorComplete = true;

                await RolePlayDbController.UpdatePlayerAcc(playerAcc).ConfigureAwait(false);

                UiHelper.ShowLoginWindow(player);

                return Ok();
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"[apiError]: Api/CharCreator/SummitCreator - {e.Message}");
                return BadRequest();
            }
        }
    }
}