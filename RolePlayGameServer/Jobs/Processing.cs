﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GTANetworkAPI;
using RolePlayGameData.Configs;
using RolePlayGameData.Database.RolePlayModels;
using RolePlayGameData.EntityDataConsts;
using RolePlayGameData.Enums;
using RolePlayGameData.Models;
using RolePlayGameServer.Database.Controllers;
using RolePlayGameServer.Players;
using RolePlayGameServer.Util;

namespace RolePlayGameServer.Jobs
{
    internal class Processing
    {
        private readonly Client _player;
        private readonly PlayerAccountModel _playerData;
        private readonly List<PlayerInvItemModel> _playerInv;

        public Processing(Client player)
        {
            _player = player;
            _playerData = (PlayerAccountModel) player.GetData(PlayerDataConst.PlayerAccount);
            _playerInv = GetPlayerInv().Result;
        }

        private IEnumerable<JobRecipeModel> RecipeList => ProcessorCfg.ProcessingRecipes;

        public async void StartProcessing(uint markType, int recipeId, int processAmount)
        {
            try
            {
                var checkedItems = new List<bool>();

                var recipe = RecipeList
                    .Where(x => (uint) x.Processor == markType)
                    .SingleOrDefault(x => x.RecipeId == recipeId);

                foreach (var item in recipe.MatsList)
                    checkedItems.Add(CheckExistItem(item.Key, item.Value * processAmount));

                if (checkedItems.Contains(false)) return;

                await Task.Run(() => new ProgressBar(_player).RunProgress(
                    $"{processAmount}x {recipe.RecipeName}", recipe.ProcessTime * processAmount, () =>
                    {
                        foreach (var item in recipe.MatsList)
                            new PlayerInventory(_player)
                                .DestroyInvItem((uint) item.Key, (uint) (item.Value * processAmount))
                                .ConfigureAwait(false);

                        foreach (var item in recipe.ProcessedList)
                            new PlayerInventory(_player).AddItem((uint) item.Key, (uint) (item.Value * processAmount))
                                .ConfigureAwait(false);
                    })).ConfigureAwait(false);

                _player.StopAnimation();
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"StartProcessing - {e.Message}");
            }
        }

        public bool CheckExistItem(ItemNameEnums itemName, int amount)
        {
            try
            {
                var itemCfg = ItemCfg.ItemConfig.Single(x => x.ItemName == itemName);
                var item = _playerInv.SingleOrDefault(x => x.Item == (uint) itemName);

                if (item == null)
                {
                    _player.SendNotification($"~r~Du hast keine ~o~{itemCfg.DisplayedName}~r~!");
                    return false;
                }

                if (item.Amount < amount)
                {
                    var playerAmount = amount - item.Amount;
                    _player.SendNotification($"~r~Dir fehlen ~o~{playerAmount}x {itemCfg.DisplayedName}~r~!");
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"CheckExistItem - {e.Message}");
                return false;
            }
        }

        #region Helper

        private async Task<List<PlayerInvItemModel>> GetPlayerInv()
        {
            return await RolePlayDbController.GetPlayerInvList(_playerData.SocialClubName)
                .ConfigureAwait(false);
        }

        #endregion
    }
}