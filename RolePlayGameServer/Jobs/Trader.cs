﻿using System;
using System.Linq;
using System.Threading.Tasks;
using GTANetworkAPI;
using RolePlayGameData.Configs;
using RolePlayGameData.Database.RolePlayModels;
using RolePlayGameData.EntityDataConsts;
using RolePlayGameData.Models;
using RolePlayGameServer.Database.Controllers;
using RolePlayGameServer.Players;
using RolePlayGameServer.Util;

namespace RolePlayGameServer.Jobs
{
    internal class Trader
    {
        private readonly MarkDataModel _markData;
        private readonly Client _player;
        private readonly int _vehicleId;

        public Trader(Client player)
        {
            _player = player;
            _markData = (MarkDataModel) player.GetData(MarkDataConst.MarkData);
            _vehicleId = GetPlayersCar().Result;
        }

        public void ShowTraderWindow()
        {
            UiHelper.ShowTrader(_player, _markData.MarkType);
        }

        public async void SellItems(uint itemName, int amount)
        {
            var itemCfg = ItemCfg.ItemConfig.SingleOrDefault(x => (uint) x.ItemName == itemName);

            await new PlayerMoney(_player).AddCashMoney(itemCfg.DefaultPrice * amount).ConfigureAwait(false);

            // AddReputation(amount);
        }

        public void BuyRecipe(uint processor, uint recipeId)
        {
            var recipe = ProcessorCfg.ProcessingRecipes
                .Where(x => (uint) x.Processor == processor)
                .Single(x => x.RecipeId == recipeId);

            var payed = new PlayerMoney(_player).RemoveCashMoney(recipe.RecipePrice);

            //if (payed) ... add recipe to recipebook
        }

        private void AddReputation(int amount)
        {
            // 0.1/item 
        }

        #region Helpers

        private async Task<int> GetPlayersCar()
        {
            try
            {
                var markpos = await MapObjectDbController.GetMarkPosition((int) _markData.MarkId)
                    .ConfigureAwait(false);

                Vehicle nextVehicle = null;
                var nextDistance = 100.0f;

                foreach (var item in NAPI.Pools.GetAllVehicles())
                {
                    if (_player.Dimension != item.Dimension) continue;

                    var distanceVehicleToMark = new Vector3(markpos.PositionX, markpos.PositionY, markpos.PositionZ)
                        .DistanceTo(item.Position);

                    if (distanceVehicleToMark > 25f) continue;

                    if (nextVehicle == null)
                    {
                        nextVehicle = item;
                        nextDistance = distanceVehicleToMark;
                    }

                    if (!(distanceVehicleToMark < nextDistance)) continue;

                    nextVehicle = item;
                    nextDistance = distanceVehicleToMark;
                }

                if (nextVehicle == null)
                {
                    NAPI.Util.ConsoleOutput("VehicleInteraction.FindNearestVehicle - vehicle is null");
                    return -1;
                }

                var vehData = (VehicleModel) nextVehicle.GetData(VehicleDataConst.CivVehicleData);

                return vehData.Id;
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"FindNearestVehicle - {e.Message}");
                return -1;
            }
        }

        #endregion
    }
}