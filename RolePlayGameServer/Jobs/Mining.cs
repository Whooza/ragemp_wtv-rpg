﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GTANetworkAPI;
using RolePlayGameData.Database.RolePlayModels;
using RolePlayGameData.EntityDataConsts;
using RolePlayGameData.Enums;
using RolePlayGameData.Models;
using RolePlayGameData.Texts;
using RolePlayGameServer.Players;
using RolePlayGameServer.Util;

namespace RolePlayGameServer.Jobs
{
    internal class Mining
    {
        private static readonly List<MiningModel> MiningList = new List<MiningModel>();
        private readonly MarkDataModel _markData;

        private readonly Client _player;
        private readonly PlayerAccountModel _playerData;

        public Mining(Client player, MarkDataModel markData)
        {
            _player = player;
            _markData = markData;
            _playerData = (PlayerAccountModel) player.GetData(PlayerDataConst.PlayerAccount);
        }

        public async Task StartMining(string processBarText)
        {
            if (ProgressBar.ActiveProgesses.Keys.Contains(_player))
            {
                _player.SetData(MarkDataConst.MarkData, null);
                return;
            }

            if (_markData.MarkType == 0 || _markData.MarkId == 0)
            {
                _player.SendNotification("~r~Kein Abbaupunkt gefunden!");
                return;
            }

            var entry = MiningList.SingleOrDefault(x => x.Player == _player);

            if (entry != null)
            {
                if ((DateTime.Now - entry.LastFarmAction).TotalMinutes > 3) entry.FarmedSpots = new List<uint>();

                if (entry.FarmedSpots.Count >= 4) entry.FarmedSpots = new List<uint>();

                if (entry.FarmedSpots.Contains(_markData.MarkId))
                {
                    _player.SendNotification(PlayerNotificationTexts.FarmspotEmty);
                    _player.SetData(MarkDataConst.MarkData, null);
                    return;
                }

                entry.FarmedSpots.Add(_markData.MarkId);
                entry.LastFarmAction = DateTime.Now;
            }
            else
            {
                var newMining = new MiningModel
                {
                    Player = _player,
                    FarmedSpots = new List<uint> {_markData.MarkId},
                    LastFarmAction = DateTime.Now
                };

                MiningList.Add(newMining);
            }

            _player.StopAnimation();
            _player.PlayAnimation("amb@medic@standing@kneel@base", "base", (int) AnimFlagsEnums.Loop);
            _player.SetData(MarkDataConst.MarkData, null);

            var finished = await Task.Run(() => new ProgressBar(_player)
                .RunProgress(processBarText, 7500, () =>
                {
                    var item = GetRandomMiningResource(_markData.MarkType);

                    new PlayerInventory(_player).AddItem(item.Item, item.Amount)
                        .ConfigureAwait(false);

                    _player.StopAnimation();
                })).ConfigureAwait(false);
        }

        private PlayerInvItemModel GetRandomMiningResource(uint markType)
        {
            var rnd = new Random();

            uint[] chemicals =
            {
                (uint) ItemNameEnums.Methylbenzodioxolbutanamin,
                (uint) ItemNameEnums.Methylendioxyethylamphetamin,
                (uint) ItemNameEnums.Methylendioxymethampheamin,
                (uint) ItemNameEnums.Phenylacetone,
                (uint) ItemNameEnums.LysergicAcid,
                (uint) ItemNameEnums.Methylamine,
                (uint) ItemNameEnums.Phenylacetone,
                (uint) ItemNameEnums.Ephedrine
            };

            uint[] wheat =
            {
                (uint) ItemNameEnums.Wheat,
                (uint) ItemNameEnums.Ergot,
                (uint) ItemNameEnums.Wheat
            };

            uint[] marijuana =
            {
                (uint) ItemNameEnums.SpecialQueenBud,
                (uint) ItemNameEnums.SpecialQueenBud,
                (uint) ItemNameEnums.SpecialQueenSeed,
                (uint) ItemNameEnums.SpecialQueenBud,
                (uint) ItemNameEnums.SpecialQueenBud
            };

            uint[] cocaine =
            {
                (uint) ItemNameEnums.CocaineLeaves,
                (uint) ItemNameEnums.CocaineSeeds,
                (uint) ItemNameEnums.CocaineLeaves
            };

            uint[] fishing =
            {
                (uint) ItemNameEnums.Cod,
                (uint) ItemNameEnums.Perch,
                (uint) ItemNameEnums.Mackerel
            };

            uint[] sand =
            {
                (uint) ItemNameEnums.BucketGravel,
                (uint) ItemNameEnums.BucketSand
            };

            var newItem = new PlayerInvItemModel
            {
                SocialClub = _playerData.SocialClubName,
                Item = 0,
                Amount = (uint) rnd.Next(1, 4)
            };

            switch (markType)
            {
                case (uint) MarkTypeEnums.AppleFarmspot:
                    newItem.Item = (uint) ItemNameEnums.AppleCrate;
                    break;
                case (uint) MarkTypeEnums.OrangeFarmspot:
                    newItem.Item = (uint) ItemNameEnums.OrangeSack;
                    break;
                case (uint) MarkTypeEnums.LemonFarmspot:
                    newItem.Item = (uint) ItemNameEnums.LemonSack;
                    break;
                case (uint) MarkTypeEnums.AlmondFarmspot:
                    break;
                case (uint) MarkTypeEnums.WalnutFarmspot:
                    newItem.Item = (uint) ItemNameEnums.WalnutsSack;
                    break;
                case (uint) MarkTypeEnums.PistacioFarmspot:
                    newItem.Item = (uint) ItemNameEnums.PistachioSack;
                    break;
                case (uint) MarkTypeEnums.AvocadoFarmspot:
                    newItem.Item = (uint) ItemNameEnums.AvocadoCrate;
                    break;
                case (uint) MarkTypeEnums.GrapeFarmspot:
                    newItem.Item = (uint) ItemNameEnums.GrapevineCrate;
                    break;
                case (uint) MarkTypeEnums.WheatFarmspot:
                    newItem.Item = wheat[rnd.Next(0, wheat.Length)];
                    break;
                case (uint) MarkTypeEnums.RockFarmspot:
                    newItem.Item = (uint) ItemNameEnums.Rock;
                    break;
                case (uint) MarkTypeEnums.TomatoFarmspot:
                    newItem.Item = (uint) ItemNameEnums.TomatoCrate;
                    break;
                case (uint) MarkTypeEnums.MariuhanaFarmspot:
                    newItem.Item = marijuana[rnd.Next(0, marijuana.Length)];
                    break;
                case (uint) MarkTypeEnums.WoodFarmspot:
                    newItem.Item = (uint) ItemNameEnums.Log;
                    break;
                case (uint) MarkTypeEnums.CocaineFarmspot:
                    newItem.Item = cocaine[rnd.Next(0, 2)];
                    break;
                case (uint) MarkTypeEnums.PotatoFarmspot:
                    newItem.Item = (uint) ItemNameEnums.PotatoSack;
                    break;
                case (uint) MarkTypeEnums.FrogFarmspot:
                    newItem.Item = (uint) ItemNameEnums.Frog;
                    break;
                case (uint) MarkTypeEnums.ShellFarmspot:
                    newItem.Item = (uint) ItemNameEnums.Shell;
                    break;
                case (uint) MarkTypeEnums.FishingFarmspot:
                    newItem.Item = fishing[rnd.Next(0, fishing.Length)];
                    break;
                case (uint) MarkTypeEnums.ChemicalFarmspot:
                    newItem.Item = chemicals[rnd.Next(0, chemicals.Length)];
                    break;
                case (uint) MarkTypeEnums.MushroomFarmspot:
                    // todo //
                    break;
                case (uint) MarkTypeEnums.MilkFarmspot:
                    newItem.Item = (uint) ItemNameEnums.RawMilk;
                    break;
                case (uint) MarkTypeEnums.SandFarmspot:
                    newItem.Item = sand[rnd.Next(0, sand.Length)];
                    break;
            }

            return newItem;
        }
    }
}