﻿using System;
using System.Threading.Tasks;
using GTANetworkAPI;
using RolePlayGameData.Database.MapMarkModels;
using RolePlayGameData.EntityDataConsts;
using RolePlayGameData.Enums;
using RolePlayGameServer.Database.Controllers;

namespace RolePlayGameServer.Jobs
{
    public class Growing
    {
        private readonly Vector3 _setPos;

        public Growing(Client player)
        {
            _setPos = NAPI.Entity.GetEntityPosition(player);
        }

        private static uint LevelOne => NAPI.Util.GetHashKey("bkr_prop_weed_bud_pruned_01a");
        private static uint LevelTwo => NAPI.Util.GetHashKey("bkr_prop_weed_01_small_01c");
        private static uint LevelThree => NAPI.Util.GetHashKey("bkr_prop_weed_01_small_01b");
        private static uint LevelFour => NAPI.Util.GetHashKey("bkr_prop_weed_01_small_01a");

        public async Task SetPlant(uint weedType, bool growBoost, bool budBoost, bool fM, bool fK, bool fS)
        {
            var plantData = new WeedPlantModel
            {
                Type = weedType,
                Gender = false,
                GrowBooster = growBoost,
                BudBooster = budBoost,
                FertilizerK = fK,
                FertilizerM = fM,
                FertilizerS = fS,
                PosX = _setPos.X,
                PosY = _setPos.Y,
                PosZ = _setPos.Z,
                PlantTime = DateTime.Now
            };

            var obj = NAPI.Object.CreateObject(LevelOne, _setPos,
                new Vector3(0, 0, 0), 0);

            await MapObjectDbController.SetWeedPlant(weedType, growBoost, budBoost,
                fK, fM, fS, _setPos.X, _setPos.Y, _setPos.Z).ConfigureAwait(false);

            obj.SetData(PlantDataConst.PlantData, plantData);
        }

        public void CheckPlant(uint irgendwas)
        {
            switch (irgendwas)
            {
                case (uint) WeedTypeEnums.SpecialQueen:
                    break;
                case (uint) WeedTypeEnums.HollandsHope:
                    break;
                case (uint) WeedTypeEnums.WhiteWidow:
                    break;
                case (uint) WeedTypeEnums.OgKush:
                    break;
                case (uint) WeedTypeEnums.Critical:
                    break;
                case (uint) WeedTypeEnums.AmnesiaHaze:
                    break;
                default: return;
            }
        }
    }
}