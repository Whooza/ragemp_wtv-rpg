﻿namespace RolePlayGameServer.Interfaces
{
    internal interface IBankAccount
    {
        bool IsAccountActive { get; }
        bool IsCreditCardActive { get; }

        float AccountBalance { get; }
        float CreditBalance { get; }
        float CreditLimit { get; }
        float CreditRePay { get; }

        float ManagementFee { get; }
        float WithdrawFee { get; }
        float DepositFee { get; }
        float TransferFee { get; }
        float CreditCardFee { get; }
        float CreditLimitValue { get; }
        float CreditRePayValue { get; }

        bool PayManagementFee();
        bool PayCreditCardFee();

        bool RePayCredit();

        bool Withdraw(string pincode, float amount);
        void Deposit(string pincode, float amount);

        bool TransferFrom(string pincode, float amount);
        void TransferTo(float amount);
    }
}