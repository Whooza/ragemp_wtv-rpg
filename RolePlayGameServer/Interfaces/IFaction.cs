﻿using GTANetworkAPI;
using System.Collections.Generic;

namespace RolePlayGameServer.Interfaces
{
    internal interface IFaction
    {
        void SetPlayerInService(Client player);
        void RemovePlayerFromService(Client player);

        void SetPlayerAsControlCenter(Client player);
        void RemovePlayerFromControlCenter(Client player);

        void CalculateSaleryMinutes();
        void PayOutSalary();

        void SendFactionMessageToAll();
        void SendFactionMessageToPlayer(Client player);
        void SendControlCenterMessageToAll();
        void SendControlCenterMessageToPlayer(Client player);

        List<Client> GetAllInService();
        List<string> GetAllInFaction();
        
        void SetPlayerToFaction();                
        void RemovePlayerFromFaction();
    }
}