﻿using System;
using GTANetworkAPI;
using RolePlayGameData.Database.RolePlayModels;
using RolePlayGameData.EntityDataConsts;
using RolePlayGameData.Enums;
using RolePlayGameServer.Players;
using RolePlayGameServer.Util;

namespace RolePlayGameServer.Banks
{
    internal class Atm
    {
        private readonly uint _bankName;
        private readonly PlayerMoneyModel _money;
        private readonly Client _player;

        public Atm(Client player, uint bankName)
        {
            _player = player;
            _bankName = bankName;
            _money = (PlayerMoneyModel) player.GetData(PlayerDataConst.PlayerMoney);
        }

        public async void Withdraw(string pinCode, float amount)
        {
            try
            {
                switch (_bankName)
                {
                    case (uint) BankNameEnums.FleecaBank:
                        var fleeca = new FleecaAccount(_player);
                        if (!fleeca.IsAccountActive)
                        {
                            Notifications.SendInteractionError(_player, "Sie haben bei dieser Bank kein Konto!");
                            return;
                        }

                        if (fleeca.Withdraw(pinCode, amount))
                            await new PlayerMoney(_player).AddCashMoney(amount).ConfigureAwait(false);
                        break;

                    case (uint) BankNameEnums.MazeBank:
                        var maze = new MazeAccount(_player);
                        if (!maze.IsAccountActive)
                        {
                            Notifications.SendInteractionError(_player, "Sie haben bei dieser Bank kein Konto!");
                            return;
                        }

                        if (maze.Withdraw(pinCode, amount))
                            await new PlayerMoney(_player).AddCashMoney(amount).ConfigureAwait(false);
                        break;

                    case (uint) BankNameEnums.UnionDepository:
                        var union = new UnionAccount(_player);
                        if (!union.IsAccountActive)
                        {
                            Notifications.SendInteractionError(_player, "Sie haben bei dieser Bank kein Konto!");
                            return;
                        }

                        if (union.Withdraw(pinCode, amount))
                            await new PlayerMoney(_player).AddCashMoney(amount).ConfigureAwait(false);
                        break;

                    default:
                        Logging.ServerErrorLog("BankAccount not found. (Withdraw)");
                        return;
                }
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"AddBankMoney - {e.Message}");
            }
        }

        public async void Deposit(string pinCode, float amount)
        {
            try
            {
                switch (_bankName)
                {
                    case (uint) BankNameEnums.FleecaBank:
                        var fleeca = new FleecaAccount(_player);
                        if (!fleeca.IsAccountActive)
                        {
                            Notifications.SendInteractionError(_player, "Sie haben bei dieser Bank kein Konto!");
                            return;
                        }

                        if (await new PlayerMoney(_player).RemoveCashMoney(amount).ConfigureAwait(false)) 
                            fleeca.Deposit(pinCode, amount);
                        break;

                    case (uint) BankNameEnums.MazeBank:
                        var maze = new MazeAccount(_player);
                        if (!maze.IsAccountActive)
                        {
                            Notifications.SendInteractionError(_player, "Sie haben bei dieser Bank kein Konto!");
                            return;
                        }

                        if (await new PlayerMoney(_player).RemoveCashMoney(amount).ConfigureAwait(false))
                            maze.Deposit(pinCode, amount);
                        break;

                    case (uint) BankNameEnums.UnionDepository:
                        var union = new UnionAccount(_player);
                        if (!union.IsAccountActive)
                        {
                            Notifications.SendInteractionError(_player, "Sie haben bei dieser Bank kein Konto!");
                            return;
                        }

                        if (await new PlayerMoney(_player).RemoveCashMoney(amount).ConfigureAwait(false))
                            union.Deposit(pinCode, amount);
                        break;

                    default:
                        Logging.ServerErrorLog("BankAccount not found. (Deposit)");
                        return;
                }
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"AddBankMoney - {e.Message}");
            }
        }
    }
}