﻿using GTANetworkAPI;
using RolePlayGameData.Configs;
using RolePlayGameData.Database.RolePlayModels;
using RolePlayGameData.EntityDataConsts;
using RolePlayGameServer.Database.Controllers;
using RolePlayGameServer.Interfaces;
using RolePlayGameServer.Util;
using System.Threading.Tasks;

namespace RolePlayGameServer.Banks
{
    internal class UnionAccount : IBankAccount
    {
        private readonly BankUnionModel _bank;
        private readonly Client _player;
        private readonly PlayerAccountModel _playerData;

        public UnionAccount(Client player)
        {
            _player = player;
            _playerData = (PlayerAccountModel) player.GetData(PlayerDataConst.PlayerAccount);
            _bank = GetUnionAccount().Result;
            CreditLimit = _bank.AccountBalance * CreditLimitValue;
            CreditRePay = _bank.CreditCardBalance * CreditRePayValue;
            IsCreditCardActive = _bank.IsCreditCardActive;
            IsAccountActive = _bank.IsAccountActive;
        }

        public bool IsAccountActive { get; }
        public bool IsCreditCardActive { get; }

        public float AccountBalance { get; private set; }
        public float CreditBalance { get; private set; }
        public float CreditLimit { get; }
        public float CreditRePay { get; }

        public float ManagementFee => BankCfg.UnionManangeFee;
        public float WithdrawFee => BankCfg.UnionWithdrawFee;
        public float DepositFee => BankCfg.UnionDepositFee;
        public float TransferFee => BankCfg.UnionTransferFee;
        public float CreditCardFee => BankCfg.UnionCreditCardFee;
        public float CreditLimitValue => BankCfg.UnionCreditLimitValue;
        public float CreditRePayValue => BankCfg.UnionCreditRePayValue;

        public bool PayManagementFee()
        {
            if (AccountBalance - ManagementFee > 0f)
            {
                AccountBalance = AccountBalance -= ManagementFee;
                _bank.IsAccountActive = true;
                Notifications.SendSystemInfo(_player,
                    "Union Despository", $"Es wurden ~r~{ManagementFee}$ Kontoführungsgebühr von Ihrem Konto abgebucht.");

                RolePlayDbController.UpdateBankUnion(_bank).ConfigureAwait(false);
                return true;
            }

            _bank.IsAccountActive = false;
            
            Notifications.SendSystemError(_player, "Union Despository",
                $"Die Kontoführungsgebühr in Höhe von {ManagementFee}$ konnte nicht von Ihrem Konto abgehoben werden.");

            return false;
        }

        public bool PayCreditCardFee()
        {
            if (AccountBalance - ManagementFee > 0f)
            {
                AccountBalance = AccountBalance -= ManagementFee;
                _bank.IsCreditCardActive = true;
                
                Notifications.SendSystemInfo(_player, "Union Despository",
                    $"Es wurden ~r~{ManagementFee}$ Kreditkartengebühr von Ihrem Konto abgebucht.");

                RolePlayDbController.UpdateBankUnion(_bank).ConfigureAwait(false);
                return true;
            }

            _bank.IsCreditCardActive = false;
            
            Notifications.SendSystemError(_player, "Union Despository",
                $"Die Kreditkartengebühr in Höhe von ~y~{ManagementFee}$ konnte nicht von Ihrem Konto abgebucht werden.");

            return false;
        }

        public bool RePayCredit()
        {
            if (AccountBalance - CreditRePay > 0f)
            {
                AccountBalance -= CreditRePay;
                _bank.IsCreditCardActive = true;
                
                Notifications.SendSystemInfo(_player, "Union Despository",
                    $"Es wurden ~r~{CreditRePay}$ Kreditkartenrückzahlung von Ihrem Konto abgebucht.");

                RolePlayDbController.UpdateBankUnion(_bank).ConfigureAwait(false);
                return true;
            }

            _bank.IsCreditCardActive = false;
            
            Notifications.SendSystemError(_player, "Union Despository",
                $"Die Kreditkartenrückzahlung in Höhe von ~y~{CreditRePay}$ konnte nicht von Ihrem Konto abgebucht werden.");

            return false;
        }

        public bool Withdraw(string PinCode, float amount)
        {
            if (PinCode != _bank.PinCode)
            {
                Notifications.SendInteractionError(_player, "Der eigegebene Pincode ist falsch!");
                return false;
            }

            if (AccountBalance - (amount + WithdrawFee) > 0f)
            {
                AccountBalance = AccountBalance -= amount + WithdrawFee;
                
                Notifications.SendInteractionInfo(_player, 
                    $"Es wurden {amount}$ +{WithdrawFee}$ von Ihrem Konto abgehoben.");

                RolePlayDbController.UpdateBankUnion(_bank).ConfigureAwait(false);

                return true;
            }

            Notifications.SendInteractionError(_player, 
                $"Der Betrag in Höhe von {amount} +{WithdrawFee}$ konnte nicht von Ihrem Konto abgebucht werden.");

            return false;
        }

        public void Deposit(string PinCode, float amount)
        {
            if (PinCode != _bank.PinCode)
            {
                Notifications.SendInteractionError(_player, "Der eigegebene Pincode ist falsch!");

                return;
            }

            AccountBalance = AccountBalance += amount - DepositFee;
            
            Notifications.SendInteractionInfo(_player, 
                $"Es wurden ~g~{amount}$ ~r~-{DepositFee}$ Ihrem Konto gutgeschrieben.");

            RolePlayDbController.UpdateBankUnion(_bank).ConfigureAwait(false);
        }

        public bool TransferFrom(string PinCode, float amount)
        {
            if (PinCode != _bank.PinCode)
            {
                Notifications.SendInteractionError(_player, "Der eigegebene Pincode ist falsch!");

                return false;
            }

            if (AccountBalance - (amount + TransferFee) > 0f)
            {
                AccountBalance = AccountBalance -= amount + TransferFee;
                
                Notifications.SendInteractionInfo(_player, 
                    $"Es wurden ~r~{amount} + {TransferFee}$ von Ihrem Konto überwiesen.");

                RolePlayDbController.UpdateBankUnion(_bank).ConfigureAwait(false);
                return true;
            }

            Notifications.SendInteractionError(_player, 
                $"Der Betrag in Höhe von ~y~{amount} + {WithdrawFee}$ konnte nicht von Ihrem Konto überwiesen werden.");

            return false;
        }

        public void TransferTo(float amount)
        {
            AccountBalance = AccountBalance += amount - TransferFee;
            
            Notifications.SendSystemInfo(_player, "Union Despository", 
                $"Ihrem Konto wurden ~g~{amount}$ ~r~-{TransferFee}$ gutgeschrieben.");

            RolePlayDbController.UpdateBankUnion(_bank).ConfigureAwait(false);
        }

        private async Task<BankUnionModel> GetUnionAccount() =>
            await RolePlayDbController.GetBankUnion(_playerData.SocialClubName).ConfigureAwait(false);
    }
}