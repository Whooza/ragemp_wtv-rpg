﻿using System;
using GTANetworkAPI;
using RolePlayGameData.Enums;
using RolePlayGameServer.Util;

namespace RolePlayGameServer.Banks
{
    internal class Bank
    {
        private readonly Client _player;

        public Bank(Client player)
        {
            _player = player;
        }

        public void Transfer(uint clientBank, float amount, Client target, uint targetBank)
        {
            try
            {
                switch (clientBank)
                {
                    case (uint) BankNameEnums.FleecaBank:
                        var fleeca = new FleecaAccount(_player);
                        break;

                    case (uint) BankNameEnums.MazeBank:
                        var maze = new MazeAccount(_player);
                        break;

                    case (uint) BankNameEnums.UnionDepository:
                        var union = new UnionAccount(_player);
                        break;

                    default:
                        Logging.ServerErrorLog("BankAccount not found. (Withdraw)");
                        return;
                }
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"AddBankMoney - {e.Message}");
            }
        }
    }
}