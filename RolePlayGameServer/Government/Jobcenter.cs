﻿using System;
using GTANetworkAPI;
using RolePlayGameData.Database.RolePlayModels;
using RolePlayGameData.EntityDataConsts;
using RolePlayGameData.Enums;
using RolePlayGameServer.Database.Controllers;

namespace RolePlayGameServer.Government
{
    internal class Jobcenter
    {
        private readonly PlayerMoneyModel _money;
        private readonly PlayerParticularModel _particular;
        private readonly PlayerPaycheckModel _paycheck;
        private readonly Client _player;

        public Jobcenter(Client player)
        {
            _player = player;
            _money = (PlayerMoneyModel) player.GetData(PlayerDataConst.PlayerMoney);
            _paycheck = (PlayerPaycheckModel) player.GetData(PlayerDataConst.PlayerPaycheck);
            _particular = (PlayerParticularModel) player.GetData(PlayerDataConst.PlayerParticular);
        }

        public void ActivateSocialPayment(uint bankName)
        {
            if (!_particular.HasIdentityCard)
            {
                _player.SendNotification(
                    "Sie besitzen keinen Personalausweis. Besuchen Sie bitte das Rathaus," +
                    " um einen zu beantragen.");
                return;
            }

            if (!_paycheck.SocialPayIsActive)
            {
                var bank = (BankNameEnums) Enum.ToObject(typeof(BankNameEnums), bankName);

                _money.SocialPaymentBank = (int) bankName;
                _paycheck.SocialPayIsActive = true;
                SaveAll();

                _player.SendNotification(
                    "~b~Soziale Leistungen wurden gewährt und werden auf Ihr " +
                    $"~y~{bank}Konto ~b~gezahlt.");
            }
            else
            {
                _player.SendNotification(
                    "~r~Sie erhalten bereits soziale Leistungen!");
            }
        }

        public void DeactivateSocialPayment()
        {
            if (_paycheck.SocialPayIsActive)
            {
                _paycheck.SocialPayIsActive = false;
                SaveAll();
                _player.SendNotification(
                    "~b~Die sozialen Leistungen wurden gestoppt." +
                    " Wir wünschen Ihnen alle Gute!");
            }
            else
            {
                _player.SendNotification(
                    "~r~Sie wurden in unsrem System nicht gefunden." +
                    " Sie erhalten keine sozialen Leistungen.");
            }
        }

        public void SaveAll()
        {
            _player.SetData(PlayerDataConst.PlayerMoney, _money);
            _player.SetData(PlayerDataConst.PlayerPaycheck, _paycheck);

            RolePlayDbController.UpdatePlayerMoney(_money)
                .ConfigureAwait(false);

            RolePlayDbController.UpdatePlayerPaycheck(_paycheck)
                .ConfigureAwait(false);
        }
    }
}