﻿using GTANetworkAPI;
using RolePlayGameData.Database.RolePlayModels;
using RolePlayGameData.EntityDataConsts;
using RolePlayGameServer.Database.Controllers;
using RolePlayGameServer.Players;
using System.Threading.Tasks;

namespace RolePlayGameServer.Government
{
    internal class CityHall
    {
        private readonly PlayerParticularModel _particulars;
        private readonly Client _player;

        public CityHall(Client player)
        {
            _player = player;
            _particulars = (PlayerParticularModel) player.GetData(PlayerDataConst.PlayerParticular);
        }

        public async Task<bool> CreateIdentyCard(string firstName, string lastName, string nationality)
        {
            if (firstName != _particulars.FirstName || lastName != _particulars.LastName ||
                _particulars.Nationality != nationality)
            {
                _player.SendNotification("~r~Ihre Daten konnten nicht verifiziert werden." +
                                         " Eventuell haben Sie sich verschrieben.");
                return false;
            }

            _particulars.HasIdentityCard = true;

            await new PlayerMoney(_player).RemoveCashMoney(75f);

            _player.SetData(PlayerDataConst.PlayerParticular, _particulars);

            await RolePlayDbController.UpdatePlayerParticular(_particulars)
                .ConfigureAwait(false);

            return true;
        }
    }
}