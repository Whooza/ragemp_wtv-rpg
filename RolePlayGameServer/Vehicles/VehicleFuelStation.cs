﻿using GTANetworkAPI;
using RolePlayGameData.Database.RolePlayModels;
using RolePlayGameData.EntityDataConsts;
using RolePlayGameServer.Database.Controllers;
using System.Threading.Tasks;

namespace RolePlayGameServer.Vehicles
{
    internal class VehicleFuelStation
    {
        private readonly Client _player;
        private readonly Vehicle _vehicle;

        public VehicleFuelStation(Client player, Vehicle vehicle)
        {
            _player = player;
            _vehicle = vehicle;
        }

        public async Task<bool> RefuelVehicle(float amount)
        {
            var vehFuel = (VehicleFuelModel)_vehicle.GetData(VehicleDataConst.VehicleFuel);

            vehFuel.TankStatus += amount;

            if (vehFuel.TankStatus > 100f) vehFuel.TankStatus = 100f;

            await RolePlayDbController.UpdateVehicleFuel(vehFuel).ConfigureAwait(false);

            _vehicle.SetData(VehicleDataConst.VehicleFuel, vehFuel);
            _vehicle.SetSharedData(VehicleDataConst.FuelShared, vehFuel.TankStatus);

            _player.SendNotification("Fahrzeug wurde betankt.");

            return true;
        }

        public void PayFuel()
        {

        }

        public void LeaveFuelStation()
        {

        }
    }
}