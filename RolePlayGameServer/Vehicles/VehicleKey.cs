﻿using System;
using System.Threading.Tasks;
using GTANetworkAPI;
using RolePlayGameData.Database.RolePlayModels;
using RolePlayGameData.EntityDataConsts;
using RolePlayGameServer.Util;

namespace RolePlayGameServer.Vehicles
{
    internal class VehicleKey
    {
        private readonly Client _player;
        private readonly PlayerAccountModel _playerData;

        public VehicleKey(Client player)
        {
            _player = player;
            _playerData = (PlayerAccountModel) player.GetData(PlayerDataConst.PlayerAccount);
        }

        public bool CheckVehicleKey(Vehicle vehicle)
        {
            try
            {
                var key = (VehicleKeyModel) vehicle.GetData(VehicleDataConst.VehicleKey);

                if (key.FirstKeyPlayerSocial == _playerData.SocialClubName) return true;

                if (key.SecondKeyPlayerIdSocial == _playerData.SocialClubName) return true;

                return key.ThirdKeyPlayerIdSocial == _playerData.SocialClubName;
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"CheckVehicleKey - CheckVehicleKey - {e.Message}");
                return false;
            }
        }
    }
}