﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GTANetworkAPI;
using RolePlayGameData.Configs;
using RolePlayGameData.Configs.Vehicle;
using RolePlayGameData.Database.RolePlayModels;
using RolePlayGameData.EntityDataConsts;
using RolePlayGameData.Enums;
using RolePlayGameData.Models;
using RolePlayGameServer.Database.Controllers;
using RolePlayGameServer.Handlers;
using RolePlayGameServer.Util;

namespace RolePlayGameServer.Vehicles
{
    internal class VehicleInventory
    {
        private readonly List<VehicleInvItemModel> _inventory;
        private readonly VehicleModel _vehicleData;
        private readonly Client _player;

        public VehicleInventory(Vehicle vehicle, Client player)
        {
            _player = player;
            _vehicleData = (VehicleModel)vehicle.GetData(VehicleDataConst.CivVehicleData);
            _inventory = GetInventory().Result;
        }

        #region InvActions

        public async Task AddItem(uint itemName, uint amount)
        {
            try
            {
                var itemModel = ItemCfg.ItemConfig.Single(x => (uint)x.ItemName == itemName);
                var vehicleItem = _inventory.SingleOrDefault(x => x.Item == itemName);

                if (CheckInventoryWeight(_inventory, itemModel, amount))
                {
                    _player.SendNotification("~r~Fahrzeugiventar ist voll!");

                    return;
                }

                if (vehicleItem == null)
                {
                    await RolePlayDbController.SetVehicleInventoryItem(_vehicleData.Id, itemName, amount).ConfigureAwait(false);

                    _player.SendNotification($"~b~In das Fahzeug gelegt: ~o~{amount}x {itemModel.DisplayedName}~b~.");

                    return;
                }

                vehicleItem.Amount += amount;

                await RolePlayDbController.UpdateVehicleInvItem(vehicleItem).ConfigureAwait(false);

                _player.SendNotification($"~b~In das Fahzeug gelegt: ~o~{amount}x {itemModel.DisplayedName}~b~.");
            }
            catch (Exception e)
            {
                Logging.InventoryErrorLog($"GiveInvItem - {e.Message}");
            }
        }

        public void UseInvItem(uint itemName)
        {
            try
            {
                var currentItem = _inventory.FirstOrDefault(x => x.Item == itemName);

                if (currentItem == null)
                {
                    _player.SendNotification("~r~Item wurde nicht gefunden!");

                    return;
                }

                var itemSettings = ItemCfg.ItemConfig.Single(x => (int)x.ItemName == itemName);

                if (itemSettings.ItemType == ItemTypeEnums.CrateItem) UnpackItem(currentItem, itemSettings);
                else ConsumeItem(currentItem, itemSettings);
            }
            catch (Exception e)
            {
                Logging.InventoryErrorLog($"UseInvItem - {e.Message}");
            }
        }

        private async void UnpackItem(VehicleInvItemModel currentItem, ItemModel itemSettings)
        {
            try
            {
                var unpackedItems = UnpackCfg.UnpackItems.Single(x => (uint)x.Key == currentItem.Item);

                if (CheckInventoryUnpackWeight(_inventory, unpackedItems.Value))
                {
                    _player.SendNotification("~r~Nicht genug Platz im Fahrzeug-Inventar zum Entpacken!");
                    return;
                }

                currentItem.Amount -= 1;

                if (currentItem.Amount == 0) await RolePlayDbController.DeleteVehicleInventoryItem(currentItem).ConfigureAwait(false);
                else await RolePlayDbController.UpdateVehicleInvItem(currentItem).ConfigureAwait(false);

                foreach (var item in unpackedItems.Value)
                    await AddItem((uint)item.Key, (uint)item.Value).ConfigureAwait(false);

                _player.SendNotification($"~b~Item entpackt: ~y~{itemSettings.DisplayedName}~b~.");
            }
            catch (Exception e)
            {
                Logging.InventoryErrorLog($"UseInvItem - {e.Message}");
            }
        }

        private async void ConsumeItem(VehicleInvItemModel currentItem, ItemModel itemSettings)
        {
            try
            {
                var itemUsed = new ItemActions(_player).ExecuteItemAction(itemSettings.ItemType, itemSettings.Value);

                if (!itemUsed) return;

                currentItem.Amount -= 1;

                if (currentItem.Amount == 0)
                    await RolePlayDbController.DeleteVehicleInventoryItem(currentItem).ConfigureAwait(false);
                else
                    await RolePlayDbController.UpdateVehicleInvItem(currentItem).ConfigureAwait(false);

                _player.SendNotification($"~b~Item benutzt: ~y~{itemSettings.DisplayedName}~b~.");
            }
            catch (Exception e)
            {
                Logging.InventoryErrorLog($"UseInvItem - {e.Message}");
            }
        }

        public async Task<bool> DestroyInvItem(uint itemName, uint amount = 10000)
        {
            try
            {
                var currentItem = _inventory.FirstOrDefault(x => x.Item == itemName);

                if (currentItem == default)
                {
                    _player.SendNotification("~r~Item wurde nicht gefunden!");

                    return false;
                }

                var itemSettings = ItemCfg.ItemConfig.Single(x => (int)x.ItemName == itemName);

                if (amount >= currentItem.Amount)
                {
                    await RolePlayDbController.DeleteVehicleInventoryItem(currentItem).ConfigureAwait(false);

                    _player.SendNotification($"~b~Item entfernt: ~y~{currentItem.Amount}x {itemSettings.DisplayedName}~b~.");

                    return true;
                }

                currentItem.Amount -= amount;

                await RolePlayDbController.UpdateVehicleInvItem(currentItem).ConfigureAwait(false);

                _player.SendNotification($"~b~Item entfernt: ~y~{amount}x {itemSettings.DisplayedName}~b~.");

                return true;
            }
            catch (Exception e)
            {
                Logging.InventoryErrorLog($"DestroyInvItem - {e.Message}");
                return false;
            }
        }

        #endregion

        #region Helpers

        private bool CheckInventoryWeight(List<VehicleInvItemModel> inventory, ItemModel itemModel, uint amount)
        {
            var weight = 0f;

            weight += itemModel.Weight * amount;

            foreach (var item in inventory)
            {
                var settings = ItemCfg.ItemConfig.Single(x => (uint)x.ItemName == item.Item);

                weight += settings.Weight * item.Amount;
            }

            var maxWeight = VehicleCfg.AllVehicles.First(x => (uint)x.VehicleHash == _vehicleData.VehicleHash)
                .InventoryCapacity;

            if (weight >= maxWeight) return true;

            return false;
        }

        private bool CheckInventoryUnpackWeight(List<VehicleInvItemModel> inventory,
            Dictionary<ItemNameEnums, int> unpackItems)
        {
            var weight = 0f;

            foreach (var item in inventory)
            {
                var settings = ItemCfg.ItemConfig.Single(x => (uint)x.ItemName == item.Item);

                weight += settings.Weight * item.Amount;
            }

            foreach (var item in unpackItems)
            {
                var settings = ItemCfg.ItemConfig.Single(x => x.ItemName == item.Key);

                weight += settings.Weight * item.Value;
            }

            if (weight >= 100f) return true;

            return false;
        }

        private async Task<List<VehicleInvItemModel>> GetInventory()
        {
            return await RolePlayDbController.GetVehicleInventoryList(_vehicleData.Id)
                .ConfigureAwait(false);
        }

        #endregion
    }
}