﻿using System;
using System.Linq;
using System.Threading.Tasks;
using GTANetworkAPI;
using RolePlayGameData.Database.RolePlayModels;
using RolePlayGameData.EntityDataConsts;
using RolePlayGameServer.Database.Controllers;
using RolePlayGameServer.Util;

namespace RolePlayGameServer.Vehicles
{
    internal class VehiclePosition
    {
        public async Task StartVehiclePosAsync()
        {
#if DEBUG
            Logging.ServerInfoLog($"starting {this}");
#endif
            try
            {
                var allVeh = NAPI.Pools.GetAllVehicles();

                if (!allVeh.Any())
                {
#if DEBUG
                    Logging.PlayerPosInfoLog("StartVehiclePosAsync - no Vehicle found");
#endif
                    return;
                }

                foreach (var item in allVeh)
                {
                    if (!item.HasData(VehicleDataConst.OwnedVehicleFlag)) continue;

                    await VehiclePositionAction(item).ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"StartVehiclePosAsync - {e.Message}");
            }
        }

        private async Task VehiclePositionAction(Vehicle vehicle)
        {
            try
            {
                var vehPos = (VehiclePositionModel) vehicle.GetData(VehicleDataConst.VehiclePos);

                vehPos.LastPosX = vehicle.Position.X;
                vehPos.LastPosY = vehicle.Position.Y;
                vehPos.LastPosZ = vehicle.Position.Z;
                vehPos.Rotation = vehicle.Rotation.Z;
                vehPos.Dimension = vehicle.Dimension;

                vehicle.SetData(VehicleDataConst.VehiclePos, vehPos);

                await RolePlayDbController.UpdateVehiclePosInDb(vehPos)
                    .ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"VehiclePositionAction - {e.Message}");
            }
        }
    }
}