﻿using GTANetworkAPI;
using RolePlayGameData.Configs.Vehicle;
using RolePlayGameData.Database.RolePlayModels;
using RolePlayGameData.EntityDataConsts;
using RolePlayGameData.Enums;
using RolePlayGameData.Models;
using RolePlayGameServer.Database.Controllers;
using RolePlayGameServer.Util;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RolePlayGameServer.Vehicles
{
    internal class VehicleSpawner
    {
        public void SpawnShopVehicle(PositionModel position, NewVehicleModel vehicle, int colorOne, int colorTwo)
        {
            NAPI.Task.Run(() =>
            {
                var newVehicle = NAPI.Vehicle.CreateVehicle((uint)vehicle.VehicleHash,
                    new Vector3(position.PosX, position.PosY, position.PosZ), position.Rotation,
                    colorOne, colorTwo, "TGS  RPG", 255, true, false);

                newVehicle.NumberPlate = "TGS  RPG";

                newVehicle.SetData(VehicleDataConst.NewVehicleFlag, true);

                var markData = new MarkDataModel
                {
                    MarkId = 0,
                    MarkFaction = (uint)position.MarkFaction,
                    MarkType = (uint)position.MarkType,
                    MarkName = (uint)position.MarkName,
                    PosX = position.PosX,
                    PosY = position.PosY,
                    PosZ = position.PosZ
                };

                newVehicle.SetData(MarkDataConst.MarkData, markData);

                var newColShape = NAPI.ColShape.CreateCylinderColShape(
                    new Vector3(position.PosX, position.PosY, position.PosZ), 2f, 4f, 0);

                newColShape.SetData(VehicleDataConst.NewVehicleFlag, true);
                newColShape.SetData(MarkDataConst.MarkData, markData);
            });
        }

        #region AllOwnedVehicles

        public async void SpawnAllOwnedVehicles()
        {
            try
            {
                var allVehicles = await RolePlayDbController.GetVehicleList()
                    .ConfigureAwait(false);

                foreach (var vehicle in allVehicles)
                {
                    var vehFuel = await RolePlayDbController.GetVehicleFuelFromDb(vehicle.Id)
                        .ConfigureAwait(false);

                    var vehKey = await RolePlayDbController.GetVehicleKeyFromDb(vehicle.Id)
                        .ConfigureAwait(false);

                    var vehPos = await RolePlayDbController.GetVehiclePosFromDb(vehicle.Id)
                        .ConfigureAwait(false);

                    var vehSet = await RolePlayDbController.GetVehicleSettingFromDb(vehicle.Id)
                        .ConfigureAwait(false);

                    SpawnOwnedVehicle(vehicle, vehFuel, vehKey, vehPos, vehSet);

                    await Task.Delay(50);
                }
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"SpawnAllOwnedVehicles - {e.Message}");
            }
        }

        public void SpawnOwnedVehicle(VehicleModel vehData, VehicleFuelModel vehFuel, VehicleKeyModel vehKey,
            VehiclePositionModel vehPos, VehicleSettingModel vehSettings)
        {
            NAPI.Task.Run(() =>
            {
                var currentVehicle = NAPI.Vehicle.CreateVehicle(vehData.VehicleHash,
                    new Vector3(vehPos.LastPosX, vehPos.LastPosY, vehPos.LastPosZ), vehPos.Rotation,
                    vehSettings.FirstColor, vehSettings.SecondColor, vehData.PlateText,
                    255, vehData.LockStatus, vehFuel.EngineStatus, vehPos.Dimension);

                var vehCfg = VehicleCfg.AllVehicles.First(x => (uint)x.VehicleHash == vehData.VehicleHash);

                currentVehicle.Locked = vehData.LockStatus;
                currentVehicle.EngineStatus = vehFuel.EngineStatus;

                currentVehicle.SetData(VehicleDataConst.OwnedVehicleFlag, true);
                currentVehicle.SetData(VehicleDataConst.CivVehicleData, vehData);
                currentVehicle.SetData(VehicleDataConst.VehicleFuel, vehFuel);
                currentVehicle.SetData(VehicleDataConst.VehicleKey, vehKey);
                currentVehicle.SetData(VehicleDataConst.VehiclePos, vehPos);
                currentVehicle.SetData(VehicleDataConst.VehicleSet, vehSettings);

                currentVehicle.SetSharedData(VehicleDataConst.FuelShared, vehFuel.TankStatus);
                currentVehicle.SetSharedData(VehicleDataConst.TankCapacityShared, vehCfg.TankCapacity);
                currentVehicle.SetSharedData(VehicleDataConst.FuelConsShared, vehCfg.FuelConsumption);
                currentVehicle.SetSharedData("RADIO_STATION", string.Empty);

                currentVehicle.SetMod(0, vehSettings.Spoiler);
                currentVehicle.SetMod(1, vehSettings.FrontBumper);
                currentVehicle.SetMod(2, vehSettings.RearBumper);
                currentVehicle.SetMod(3, vehSettings.SideSkirt);
                currentVehicle.SetMod(4, vehSettings.Exhaust);
                currentVehicle.SetMod(5, vehSettings.Frame);
                currentVehicle.SetMod(6, vehSettings.Grille);
                currentVehicle.SetMod(7, vehSettings.Hood);
                currentVehicle.SetMod(8, vehSettings.Fender);
                currentVehicle.SetMod(9, vehSettings.RightFender);
                currentVehicle.SetMod(10, vehSettings.Roof);
                currentVehicle.SetMod(11, vehSettings.Engine);
                currentVehicle.SetMod(12, vehSettings.Brakes);
                currentVehicle.SetMod(13, vehSettings.Transmission);
                currentVehicle.SetMod(14, vehSettings.Horn);
                currentVehicle.SetMod(15, vehSettings.Suspension);
                currentVehicle.SetMod(16, vehSettings.Armor);
                currentVehicle.SetMod(18, vehSettings.Turbo);
                currentVehicle.SetMod(20, vehSettings.UtilShadowSilver);
                currentVehicle.SetMod(22, vehSettings.Xenon);
                currentVehicle.SetMod(23, vehSettings.FrontWheels);
                currentVehicle.SetMod(24, vehSettings.BackWheels);
                currentVehicle.SetMod(25, vehSettings.PlateHolders);
                currentVehicle.SetMod(27, vehSettings.TrimDesign);
                currentVehicle.SetMod(28, vehSettings.Ornaments);
                currentVehicle.SetMod(30, vehSettings.DialDesign);
                currentVehicle.SetMod(33, vehSettings.SteeringWheel);
                currentVehicle.SetMod(34, vehSettings.ShiftLever);
                currentVehicle.SetMod(35, vehSettings.Plaques);
                currentVehicle.SetMod(38, vehSettings.Hydraulics);
                currentVehicle.SetMod(46, vehSettings.WindowTint);
                currentVehicle.SetMod(48, vehSettings.Livery);
                currentVehicle.SetMod(62, vehSettings.Plate);
            });
        }

        #endregion

        #region GarageVehicle

        public async void SpawnInGarageVehicle(int vehicleId, uint dimension, Vector3 newPos, Vector3 newRot)
        {
            try
            {
                var vehicle = await RolePlayDbController.GetVehicle(vehicleId).ConfigureAwait(false);

                var vehFuel = await RolePlayDbController.GetVehicleFuelFromDb(vehicleId).ConfigureAwait(false);

                var vehKey = await RolePlayDbController.GetVehicleKeyFromDb(vehicleId).ConfigureAwait(false);

                var vehSet = await RolePlayDbController.GetVehicleSettingFromDb(vehicleId).ConfigureAwait(false);

                var vehPos = await RolePlayDbController.GetVehiclePosFromDb(vehicleId).ConfigureAwait(false);

                vehFuel.EngineStatus = false;

                vehPos.LastPosX = newPos.X;
                vehPos.LastPosY = newPos.Y;
                vehPos.LastPosZ = newPos.Z;
                vehPos.Rotation = newRot.Z;
                vehPos.Dimension = dimension;

                await RolePlayDbController.UpdateVehicleFuel(vehFuel).ConfigureAwait(false);
                await RolePlayDbController.UpdateVehiclePosInDb(vehPos).ConfigureAwait(false);

                SpawnGarageVehicle(vehicle, vehFuel, vehKey, vehSet, vehPos);
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"SpawnAllOwnedVehicles - {e.Message}");
            }
        }

        public void SpawnGarageVehicle(VehicleModel vehData, VehicleFuelModel vehFuel, VehicleKeyModel vehKey,
            VehicleSettingModel vehSet, VehiclePositionModel vehPos)
        {
            NAPI.Task.Run(() =>
            {
                var currVeh = NAPI.Vehicle.CreateVehicle(vehData.VehicleHash,
                    new Vector3(vehPos.LastPosX, vehPos.LastPosY, vehPos.LastPosZ), vehPos.Rotation,
                    vehSet.FirstColor, vehSet.SecondColor, vehData.PlateText,
                    255, vehData.LockStatus, vehFuel.EngineStatus, vehPos.Dimension);

                var vehCfg = VehicleCfg.AllVehicles.First(x => (uint)x.VehicleHash == vehData.VehicleHash);

                currVeh.Locked = vehData.LockStatus;
                currVeh.EngineStatus = vehFuel.EngineStatus;

                currVeh.SetData(VehicleDataConst.OwnedVehicleFlag, true);
                currVeh.SetData(VehicleDataConst.CivVehicleData, vehData);
                currVeh.SetData(VehicleDataConst.VehicleFuel, vehFuel);
                currVeh.SetData(VehicleDataConst.VehicleKey, vehKey);
                currVeh.SetData(VehicleDataConst.VehiclePos, vehPos);
                currVeh.SetData(VehicleDataConst.VehicleSet, vehSet);

                currVeh.SetSharedData(VehicleDataConst.FuelShared, vehFuel.TankStatus);
                currVeh.SetSharedData(VehicleDataConst.TankCapacityShared, vehCfg.TankCapacity);
                currVeh.SetSharedData(VehicleDataConst.FuelConsShared, vehCfg.FuelConsumption);
                currVeh.SetSharedData("RADIO_STATION", string.Empty);

                currVeh.SetMod(0, vehSet.Spoiler);
                currVeh.SetMod(1, vehSet.FrontBumper);
                currVeh.SetMod(2, vehSet.RearBumper);
                currVeh.SetMod(3, vehSet.SideSkirt);
                currVeh.SetMod(4, vehSet.Exhaust);
                currVeh.SetMod(5, vehSet.Frame);
                currVeh.SetMod(6, vehSet.Grille);
                currVeh.SetMod(7, vehSet.Hood);
                currVeh.SetMod(8, vehSet.Fender);
                currVeh.SetMod(9, vehSet.RightFender);
                currVeh.SetMod(10, vehSet.Roof);
                currVeh.SetMod(11, vehSet.Engine);
                currVeh.SetMod(12, vehSet.Brakes);
                currVeh.SetMod(13, vehSet.Transmission);
                currVeh.SetMod(14, vehSet.Horn);
                currVeh.SetMod(15, vehSet.Suspension);
                currVeh.SetMod(16, vehSet.Armor);
                currVeh.SetMod(18, vehSet.Turbo);
                currVeh.SetMod(20, vehSet.UtilShadowSilver);
                currVeh.SetMod(22, vehSet.Xenon);
                currVeh.SetMod(23, vehSet.FrontWheels);
                currVeh.SetMod(24, vehSet.BackWheels);
                currVeh.SetMod(25, vehSet.PlateHolders);
                currVeh.SetMod(27, vehSet.TrimDesign);
                currVeh.SetMod(28, vehSet.Ornaments);
                currVeh.SetMod(30, vehSet.DialDesign);
                currVeh.SetMod(33, vehSet.SteeringWheel);
                currVeh.SetMod(34, vehSet.ShiftLever);
                currVeh.SetMod(35, vehSet.Plaques);
                currVeh.SetMod(38, vehSet.Hydraulics);
                currVeh.SetMod(46, vehSet.WindowTint);
                currVeh.SetMod(48, vehSet.Livery);
                currVeh.SetMod(62, vehSet.Plate);
            });
        }

        #endregion


        public void SpawnMappersCar(Client player)
        {
            if (!new RankChecker(player).CheckPlayerIsAdmin(1)) return;

            NAPI.Task.Run(() =>
            {
                var playerPos = NAPI.Entity.GetEntityPosition(player);
                var newX = playerPos.X + 2.5;
                var newY = playerPos.Y + 2.5;
                var newZ = playerPos.Z + 0.5;
                var newRot = player.Rotation;
                var vehData = VehicleCfg.AllVehicles.First(x => x.VehicleName == "adder");
                var car = NAPI.Vehicle.CreateVehicle((uint)vehData.VehicleHash, new Vector3(newX, newY, newZ),
                    newRot.Z, (int)ColorEnums.Chrome, (int)ColorEnums.MatteBlack);

                car.SetData(VehicleDataConst.AdminVehicleFlag, true);
                car.SetSharedData(VehicleDataConst.FuelShared, 50);
                car.SetSharedData(VehicleDataConst.TankCapacityShared, 100);
                car.SetSharedData(VehicleDataConst.FuelConsShared, 20);

                car.NumberPlate = "admincar";
                car.NumberPlateStyle = 1;
                car.EngineStatus = false;
                car.CustomPrimaryColor = new Color(0, 0, 225);
                car.CustomSecondaryColor = new Color(255, 140, 0);

                car.SetMod(14, 1);
                car.SetMod(11, 3);
                car.SetMod(12, 2);
                car.SetMod(13, 2);
                car.SetMod(15, 3);
                car.SetMod(16, 4);
                car.SetMod(18, 0);
                car.SetMod(22, 0);
                car.SetMod(46, 2);
                car.SetMod(40, 3);
            });
        }
    }
}