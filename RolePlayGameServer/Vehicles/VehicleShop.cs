﻿using System;
using System.Linq;
using System.Threading.Tasks;
using GTANetworkAPI;
using RolePlayGameData.Configs.Vehicle;
using RolePlayGameData.Database.MapMarkModels;
using RolePlayGameData.Database.RolePlayModels;
using RolePlayGameData.EntityDataConsts;
using RolePlayGameData.Enums;
using RolePlayGameData.Models;
using RolePlayGameServer.Database.Controllers;
using RolePlayGameServer.Players;
using RolePlayGameServer.ServerLists;
using RolePlayGameServer.Util;

namespace RolePlayGameServer.Vehicles
{
    internal class VehicleShop
    {
        private readonly Random _rnd = new Random();

        public async void CheckCarsAction()
        {
            try
            {
                var shopVehiclesInDb = await MapObjectDbController.GetVehicleShopPosListFromDb().ConfigureAwait(false);

                foreach (var item in shopVehiclesInDb)
                {
                    if (!item.IsActive) CreateNewShopVehicle(item);
                    else CreateExistingShopVehicle(item);

                    await Task.Delay(50);
                }
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"CheckCarsAction - {e.Message}");
            }
        }

        public async void ResetAllShopVehicles()
        {
            try
            {
                var shopVehiclesInDb = await MapObjectDbController.GetVehicleShopPosListFromDb().ConfigureAwait(false);

                foreach (var item in shopVehiclesInDb)
                {
                    item.IsActive = false;
                    await MapObjectDbController.UpdateVehShopInDb(item).ConfigureAwait(false);
                }

                foreach (var item in NAPI.Pools.GetAllVehicles())
                {
                    if (!item.HasData(VehicleDataConst.NewVehicleFlag)) continue;
                    NAPI.Task.Run(item.Delete);
                }

                foreach (var item in NAPI.Pools.GetAllColShapes())
                {
                    if (!item.HasData(VehicleDataConst.NewVehicleFlag)) continue;
                    NAPI.Task.Run(item.Delete);
                }

                NAPI.Task.Run(() => CheckCarsAction());
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"ResetAllShopVehicles - {e.Message}");
            }
        }

        public async void BuyShopVehicle(Client player, uint markName)
        {
            try
            {
                // get data //
                var playerData = (PlayerAccountModel)player.GetData(PlayerDataConst.PlayerAccount);

                var vehShopDbEntry = await MapObjectDbController.GetVehicleShopPosFromDb(markName).ConfigureAwait(false);

                var vehConfig = VehicleCfg.AllVehicles.First(x => (uint)x.VehicleHash == vehShopDbEntry.VehicleHash);

                // try to pay vehicle //
                var payed = await new PlayerMoney(player).RemoveCashMoney(vehConfig.Price).ConfigureAwait(false);

                if (!payed) return;

                // set vehicle sold in db //
                vehShopDbEntry.IsActive = false;

                await MapObjectDbController.UpdateVehShopInDb(vehShopDbEntry).ConfigureAwait(false);

                DeleteShopVehicle(vehShopDbEntry.MarkName);

                await Task.Run(() => LocationList.Instance.RemovePlayer(player)).ConfigureAwait(false);

                // create owned vehicle //
                var buyDate = DateTime.Now;

                await RolePlayDbController.SetVehicle(playerData.SocialClubName, vehShopDbEntry.VehicleHash,
                        (uint)FactionEnums.Civ, vehConfig.TankCapacity, buyDate).ConfigureAwait(false);

                var ownedVehicle = await RolePlayDbController.GetVehicleByBuyData(playerData.SocialClubName, buyDate)
                    .ConfigureAwait(false);

                // create owned veh fuel //
                await RolePlayDbController.SetVehicleFuelInDb(ownedVehicle.Id, vehShopDbEntry.VehicleHash).ConfigureAwait(false);

                // create owned veh inv items //
                await RolePlayDbController.SetVehicleInventoryItem(ownedVehicle.Id, (uint)ItemNameEnums.ToolKit, 1)
                    .ConfigureAwait(false);

                await RolePlayDbController.SetVehicleInventoryItem(ownedVehicle.Id, (uint)ItemNameEnums.FirstAidKit, 1)
                    .ConfigureAwait(false);

                // create owned veh key //
                await RolePlayDbController.SetVehicleKeyInDb(ownedVehicle.Id, playerData.SocialClubName).ConfigureAwait(false);

                var shopMarkCfg = VehShopMarkCfg.VehicleShopMarkPositions
                    .First(x => (uint)x.MarkName == vehShopDbEntry.MarkName);

                // create owned vehicle pos //
                await RolePlayDbController.SetVehiclePosInDb(ownedVehicle.Id, shopMarkCfg.PosX,shopMarkCfg.PosY, shopMarkCfg.PosZ,
                    shopMarkCfg.Rotation, 0).ConfigureAwait(false);

                // create owned veh settings //
                await RolePlayDbController.SetVehicleSettingsInDb(ownedVehicle.Id, (int)vehShopDbEntry.FirstColor,
                    (int)vehShopDbEntry.SecondColor).ConfigureAwait(false);

                // spawn owned vehicle //
                var vehFuel = await RolePlayDbController.GetVehicleFuelFromDb(ownedVehicle.Id).ConfigureAwait(false);
                var vehKey = await RolePlayDbController.GetVehicleKeyFromDb(ownedVehicle.Id).ConfigureAwait(false);
                var vehPos = await RolePlayDbController.GetVehiclePosFromDb(ownedVehicle.Id).ConfigureAwait(false);
                var vehSet = await RolePlayDbController.GetVehicleSettingFromDb(ownedVehicle.Id).ConfigureAwait(false);

                NAPI.Task.Run(() => new VehicleSpawner().SpawnOwnedVehicle(ownedVehicle, vehFuel, vehKey, vehPos, vehSet));
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"BuyShopVehicle - {e.Message}");
            }
        }

        private void DeleteShopVehicle(uint markName)
        {
            try
            {
                foreach (var item in NAPI.Pools.GetAllVehicles())
                {
                    if (!item.HasData(VehicleDataConst.NewVehicleFlag)) continue;

                    var markData = (MarkDataModel)item.GetData(MarkDataConst.MarkData);

                    if (markData.MarkName != markName) continue;

                    NAPI.Task.Run(item.Delete);
                }

                foreach (var shape in NAPI.Pools.GetAllColShapes())
                {
                    if (!shape.HasData(VehicleDataConst.NewVehicleFlag)) continue;

                    var markData = (MarkDataModel)shape.GetData(MarkDataConst.MarkData);

                    if (markData.MarkName != markName) continue;

                    NAPI.Task.Run(shape.Delete);
                }
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"DeleteShopVehicle - {e.Message}");
            }
        }

        private async void CreateNewShopVehicle(ShopSlotPosModel vehShopModel)
        {
            try
            {
                var markPos = VehShopMarkCfg.VehicleShopMarkPositions.First(x => (uint)x.MarkName == vehShopModel.MarkName);
                var vehicleList = VehicleCfg.AllVehicles.Where(x => (uint)x.VehicleClass == (uint)markPos.VehicleClass).ToList();
                var newVehicle = vehicleList[_rnd.Next(0, vehicleList.Count)];
                var colorOne = _rnd.Next(0, 159);
                var colorTwo = _rnd.Next(0, 159);

                new VehicleSpawner().SpawnShopVehicle(markPos, newVehicle, colorOne, colorTwo);

                vehShopModel.IsActive = true;
                vehShopModel.VehicleHash = (uint)newVehicle.VehicleHash;
                vehShopModel.FirstColor = (uint)colorOne;
                vehShopModel.SecondColor = (uint)colorTwo;

                await MapObjectDbController.UpdateVehShopInDb(vehShopModel).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"CreateNewShopVehicle - {e.Message}");
            }
        }

        private void CreateExistingShopVehicle(ShopSlotPosModel vehShopModel)
        {
            try
            {
                var markPos = VehShopMarkCfg.VehicleShopMarkPositions.Single(x => (uint)x.MarkName == vehShopModel.MarkName);
                var vehicle = VehicleCfg.AllVehicles.Single(x => (uint)x.VehicleHash == vehShopModel.VehicleHash);

                new VehicleSpawner().SpawnShopVehicle(markPos, vehicle, (int)vehShopModel.FirstColor,
                    (int)vehShopModel.SecondColor);
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"CreateExistingShopVehicle - {e.Message}");
            }
        }
    }
}