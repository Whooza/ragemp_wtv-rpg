﻿using System;
using System.Linq;
using System.Threading.Tasks;
using GTANetworkAPI;
using RolePlayGameData.Configs.Vehicle;
using RolePlayGameData.Database.RolePlayModels;
using RolePlayGameData.EntityDataConsts;
using RolePlayGameServer.Database.Controllers;
using RolePlayGameServer.Util;

namespace RolePlayGameServer.Vehicles
{
    internal class VehicleFuel
    {
        public async Task StartVehicleFuelAsync()
        {
#if DEBUG
            Logging.ServerInfoLog($"starting {this}");
#endif
            try
            {
                var allVeh = NAPI.Pools.GetAllVehicles();

                if (!allVeh.Any())
                {
#if DEBUG
                    Logging.VehicleFuelInfoLog("StartVehicleFuelAsync - no vehicle found");
#endif
                    return;
                }

                foreach (var item in allVeh)
                {
                    if (!item.HasData(VehicleDataConst.OwnedVehicleFlag)) continue;
                    if (item.HasData(VehicleDataConst.IsRefueling)) continue;
                    if (!item.EngineStatus) continue;

                    await VehicleFuelAction(item).ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"StartVehicleFuelAsync - {e.Message}");
            }
        }

        private async Task VehicleFuelAction(Vehicle vehicle)
        {
            try
            {
                var currentCarCfg = VehicleCfg.AllVehicles
                    .Single(x => (uint) x.VehicleHash == vehicle.Model);

                if (currentCarCfg == null)
                {
                    Logging.VehicleFuelErrorLog(
                        $"Error VehicleFuelAction() - {vehicle.Model} not configured");
                    return;
                }

                var vehFuel = (VehicleFuelModel) vehicle.GetData(VehicleDataConst.VehicleFuel);

                if (vehFuel.TankStatus > currentCarCfg.FuelConsumption)
                {
                    vehFuel.TankStatus = vehFuel.TankStatus - currentCarCfg.FuelConsumption / 10;
                }
                else
                {
                    vehFuel.TankStatus = 0.0f;
                    vehFuel.EngineStatus = false;
                    vehicle.EngineStatus = false;
                }

                vehicle.SetData(VehicleDataConst.VehicleFuel, vehFuel);
                vehicle.SetSharedData(VehicleDataConst.FuelShared, vehFuel.TankStatus);

                await RolePlayDbController.UpdateVehicleFuel(vehFuel)
                    .ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"VehicleFuelAction - {e.Message}");
            }
        }
    }
}