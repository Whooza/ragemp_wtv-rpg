﻿using GTANetworkAPI;
using RolePlayGameData.Database.RolePlayModels;
using RolePlayGameData.EntityDataConsts;
using RolePlayGameData.Texts;
using RolePlayGameServer.Database.Controllers;
using RolePlayGameServer.Util;
using System;
using System.Threading.Tasks;

namespace RolePlayGameServer.Vehicles
{
    internal class VehicleInteraction
    {
        private readonly Client _player;

        public VehicleInteraction(Client player) => _player = player;

        public async Task StartStopEngine()
        {
            try
            {
                if (_player.Vehicle == null)
                {
                    Notifications.SendInteractionError(_player, PlayerNotificationTexts.NotInVehicle);
                }
                else
                {
                    var nextVehicle = await NextEntity.NextVehicle(_player, 3f).ConfigureAwait(false);

                    if (_player.VehicleSeat != -1) return;

                    var hasKey = new VehicleKey(_player).CheckVehicleKey(nextVehicle);

                    var vehFuel = (VehicleFuelModel) _player.Vehicle.GetData(VehicleDataConst.VehicleFuel);

                    if (vehFuel.TankStatus <= 0)
                    {
                        Notifications.SendInteractionError(_player, PlayerNotificationTexts.NoFuel);
                        return;
                    }

                    if (hasKey)
                    {
                        if (_player.Vehicle.EngineStatus)
                        {
                            _player.Vehicle.EngineStatus = false;
                            vehFuel.EngineStatus = false;
                            Notifications.SendInteractionInfo(_player, PlayerNotificationTexts.StopEngine);
                        }
                        else
                        {
                            _player.Vehicle.EngineStatus = true;
                            vehFuel.EngineStatus = true;
                            Notifications.SendInteractionInfo(_player, PlayerNotificationTexts.StartEngine);
                        }

                        nextVehicle.SetData(VehicleDataConst.VehicleFuel, vehFuel);

                        await RolePlayDbController.UpdateVehicleFuel(vehFuel).ConfigureAwait(false);
                    }
                    else
                    {
                        Notifications.SendInteractionError(_player, PlayerNotificationTexts.NoVehicleKey);
                    }
                }
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"StartStopEngine - {e.Message}");
            }
        }

        public async Task LockUnlockDoorsAsync()
        {
            try
            {
                var nextVehicle = await NextEntity.NextVehicle(_player, 3f).ConfigureAwait(false);

                var vehicleData = (VehicleModel) nextVehicle.GetData(VehicleDataConst.CivVehicleData);

                if (nextVehicle == null)
                {
                    Notifications.SendInteractionError(_player, PlayerNotificationTexts.NoVehicleInRange);
                }
                else
                {
                    var hasKey = new VehicleKey(_player).CheckVehicleKey(nextVehicle);

                    if (hasKey)
                    {
                        if (nextVehicle.Locked)
                        {
                            nextVehicle.Locked = false;
                            vehicleData.LockStatus = false;
                            Notifications.SendInteractionInfo(_player, PlayerNotificationTexts.UnlockVehicle);
                        }
                        else if (!nextVehicle.Locked)
                        {
                            nextVehicle.Locked = true;
                            vehicleData.LockStatus = true;
                            Notifications.SendInteractionInfo(_player, PlayerNotificationTexts.LockVehicle);
                        }
                    }
                    else
                    {
                        Notifications.SendInteractionError(_player, PlayerNotificationTexts.NoVehicleKey);
                    }

                    nextVehicle.SetData(VehicleDataConst.CivVehicleData, vehicleData);

                    await RolePlayDbController.UpdateVehicle(vehicleData).ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"LockUnlockDoorsAsync - {e.Message}");
            }
        }
    }
}