﻿using GTANetworkAPI;
using RolePlayGameData.EntityDataConsts;
using RolePlayGameServer.Dumpsters;
using RolePlayGameServer.Handlers;
using RolePlayGameServer.Util;
using RolePlayGameServer.Vehicles;
using System.Threading.Tasks;

namespace RolePlayGameServer.Events
{
    internal class RemoteEvents : Script
    {
        [RemoteEvent("createDumpster")]
        public void OnCreateDumpster(Client player)
        {
            if (!new RankChecker(player).CheckPlayerIsAdmin(1)) return;

            NAPI.Task.Run(() => new DumpsterCreator(player).CreateDumpster());
        }

        [RemoteEvent("changeVoiceRange")]
        public void OnChangeVoiceRange(Client player)
        {
            NAPI.Util.ConsoleOutput("changeVoiceRange triggered");

            var voiceRange = "Normal";

            if (player.HasSharedData("VOICE_RANGE")) voiceRange = player.GetSharedData("VOICE_RANGE");

            switch (voiceRange)
            {
                case "Normal":
                    voiceRange = "Weit";
                    player.TriggerEvent("changeVoiceIcon", 4);
                    break;
                case "Weit":
                    voiceRange = "Kurz";
                    player.TriggerEvent("changeVoiceIcon", 2);
                    break;
                case "Kurz":
                    voiceRange = "Normal";
                    player.TriggerEvent("changeVoiceIcon", 3);
                    break;
            }

            player.SetSharedData("VOICE_RANGE", voiceRange);
        }

        [RemoteEvent("showClothingSpawner")]
        public void OnShowClothingSpawner(Client player)
        {
            if (!new RankChecker(player).CheckPlayerIsAdmin(1)) return;

            UiHelper.ShowClothingSpawner(player);
            player.TriggerEvent("setClothingShopCam", true);
        }

        [RemoteEvent("showLocationTeleporter")]
        public void OnShowLocationTeleporter(Client player)
        {
            if (!new RankChecker(player).CheckPlayerIsAdmin(1)) return;

            UiHelper.ShowLocationTeleporter(player);
        }

        [RemoteEvent("showItemSpawner")]
        public void OnShowItemSpawner(Client player)
        {
            if (!new RankChecker(player).CheckPlayerIsAdmin(1)) return;

            UiHelper.ShowItemSpawner(player);
        }

        [RemoteEvent("showMapMarkEditor")]
        public void OnShowMapMarkEditor(Client player)
        {
            if (!new RankChecker(player).CheckPlayerIsAdmin(1)) return;

            UiHelper.ShowMapMarkEditor(player);
        }

        [RemoteEvent("showInventory")]
        public void OnGetInventory(Client player) =>
            UiHelper.ShowInventory(player);

        [RemoteEvent("atm")]
        public void OnOpenAtm(Client player, uint markName, uint bankName) =>
            UiHelper.ShowAtm(player);

        [RemoteEvent("startStopEngine")]
        public void OnStartStopEngine(Client player) =>
            Task.Run(() => new VehicleInteraction(player).StartStopEngine());

        [RemoteEvent("lockUnlockDoors")]
        public void OnLockUnlockDoors(Client player) =>
            Task.Run(() => new VehicleInteraction(player).LockUnlockDoorsAsync());

        [RemoteEvent("syncRadio")]
        public void OnRadioSync(Client player, string radioStation)
        {
            NAPI.Util.ConsoleOutput($">>>>> RADIO SYNC TRIGGERED WITH STATION: {radioStation} <<<<<");
            player.Vehicle.SetSharedData("RADIO_STATION", radioStation);
        }

        [RemoteEvent("seatBelt")]
        public void OnSeatBelt(Client player) =>
            player.Vehicle.AttachTo(player, "SKEL_ROOT", new Vector3(0, 0, 0), new Vector3(0, 0, 0));

        [RemoteEvent("interactWithLocation")]
        public void OnInteractWithLocation(Client player)
        {
            if (player.HasData(PlayerDataConst.IsInLoginActionFlag)) return;

            if (player.Vehicle == null) new InteractionButton(player).ExecuteAction();
            else new GarageInteraction(player).Interact();            
        }
    }
}