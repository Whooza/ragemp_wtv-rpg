﻿using GTANetworkAPI;
using RolePlayGameData.Configs;
using RolePlayGameData.Configs.Vehicle;
using RolePlayGameData.EntityDataConsts;
using RolePlayGameData.Enums;
using RolePlayGameData.Texts;
using RolePlayGameServer.Database.Controllers;
using RolePlayGameServer.Market;
using RolePlayGameServer.Players;
using RolePlayGameServer.Util;
using RolePlayGameServer.Vehicles;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace RolePlayGameServer.Events
{
    public class ConsoleEvents
    {
        [RemoteEvent("marketCalculations")]
        public async void OnMarketCalculations(Client player)
        {
            if (!new RankChecker(player).CheckPlayerIsAdmin(1)) return;

            try
            {
                for (int i = 0; i < 100; i++)
                {
                    await Task.Run(() => MarketSystem.Instance.AddItem(0, 0, 75, true)).ConfigureAwait(false);
                    await Task.Run(() => MarketSystem.Instance.AddItem(47, 1, 75, true)).ConfigureAwait(false);
                    await Task.Run(() => MarketSystem.Instance.AddItem(55, 2, 75, true)).ConfigureAwait(false);
                    await Task.Run(() => MarketSystem.Instance.AddItem(60, 3, 75, true)).ConfigureAwait(false);
                    await Task.Run(() => MarketSystem.Instance.AddItem(85, 4, 75, true)).ConfigureAwait(false);

                    await Task.Run(() => MarketSystem.Instance.AddItem(24, 0, 75, false)).ConfigureAwait(false);
                    await Task.Run(() => MarketSystem.Instance.AddItem(50, 1, 75, false)).ConfigureAwait(false);
                    await Task.Run(() => MarketSystem.Instance.AddItem(59, 2, 75, false)).ConfigureAwait(false);
                    await Task.Run(() => MarketSystem.Instance.AddItem(65, 3, 75, false)).ConfigureAwait(false);
                    await Task.Run(() => MarketSystem.Instance.AddItem(89, 4, 75, false)).ConfigureAwait(false);
                }

                Notifications.SendInteractionInfo(player, "Marktsystem-Brechnungen wurden hinzugefügt.");
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"Error ConsoleEvents.marketCalculations - {e.Message}");
            }
        }

        [RemoteEvent("marketSystemReset")]
        public void OnMarketSystemReset(Client player)
        {
            if (!new RankChecker(player).CheckPlayerIsAdmin(1)) return;

            try
            {
                Task.Run(() => MarketSystem.Instance.ResetAllPrices()).ConfigureAwait(false);

                Notifications.SendInteractionInfo(player, "Marktsystem wurde zurückgesetzt.");
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"Error ConsoleEvents.marketCalculations - {e.Message}");
            }
        }

        [RemoteEvent("char")]
        public void OnChar(Client player)
        {
            if (!new RankChecker(player).CheckPlayerIsAdmin(1)) return;

            player.SetClothes(1, 121, 0);
            player.SetClothes(2, 0, 0);
            player.SetClothes(3, 30, 0);
            player.SetClothes(4, 13, 0);
            player.SetClothes(5, 0, 0);
            player.SetClothes(6, 82, 0);
            player.SetClothes(7, 0, 0);
            player.SetClothes(8, 58, 0);
            player.SetClothes(9, 0, 0);
            player.SetClothes(10, 8, 2);
            player.SetClothes(11, 55, 0);
            player.SetAccessories(0, 46, 0);
            player.SetAccessories(1, 9, 0);
            player.SetAccessories(2, 0, 0);
            player.SetAccessories(6, 3, 0);
            player.SetAccessories(7, 0, 0);

            Notifications.SendInteractionInfo(player, "Kleidung eines Polizisten wurden angelegt.");
        }

        [RemoteEvent("addProp")]
        public void OnAddProp(Client player, string propName)
        {
            if (!new RankChecker(player).CheckPlayerIsAdmin(1)) return;

            player.TriggerEvent("addProp", propName);
        }

        [RemoteEvent("removeProp")]
        public void OnRemoveProp(Client player, string propName)
        {
            if (!new RankChecker(player).CheckPlayerIsAdmin(1)) return;

            player.TriggerEvent("removeProp", propName);
        }

        [RemoteEvent("close")]
        public void OnClose(Client player)
        {
            if (!new RankChecker(player).CheckPlayerIsAdmin(1)) return;

            Task.Run(() => UiHelper.DestroyBrowserWindow(player));
        }

        [RemoteEvent("playAnim")]
        public void OnPlayAnim(Client player, int type, int anim)
        {
            if (!new RankChecker(player).CheckPlayerIsAdmin(1)) return;

            Task.Run(() => new PlayerAnimations(player, type, anim).PlayAnimation());
        }

        [RemoteEvent("stopAnim")]
        public void OnStopAnim(Client player)
        {
            if (!new RankChecker(player).CheckPlayerIsAdmin(1)) return;

            Task.Run(() => new PlayerAnimations(player, -1, -1).PlayAnimation());
        }

        [RemoteEvent("charCreator")]
        public void OnCharCreator(Client player)
        {
            if (!new RankChecker(player).CheckPlayerIsAdmin(1)) return;

            Task.Run(() =>
            {
                try
                {
                    NAPI.Player.SetPlayerSkin(player, PedHash.FreemodeMale01);

                    player.SetClothes(11, 15, 0);
                    player.SetClothes(3, 15, 0);
                    player.SetClothes(8, 15, 0);

                    player.Transparency = 255;
                    player.Rotation = new Vector3(0.0f, 0.0f, 180.0f);
                    player.Position = new Vector3(152.3787f, -1000.644f, -99f);

                    UiHelper.SetCreatorCamera(player);
                    UiHelper.ShowCharCreator(player);
                }
                catch (Exception e)
                {
                    Logging.ServerErrorLog($"{this} - {e.Message}");
                }
            });
        }

        [RemoteEvent("shopreset")]
        public void ShopResetCmd(Client player)
        {
            if (!new RankChecker(player).CheckPlayerIsAdmin(1)) return;

            try
            {
                NAPI.Task.Run(() => new VehicleShop().ResetAllShopVehicles());

                Notifications.SendInteractionInfo(player, "Alle Shops wurden zurückgesetzt.");
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"{this} - {e.Message}");
            }
        }

        [RemoteEvent("tpp")]
        public void TppCmd(Client player, string social)
        {
            if (!new RankChecker(player).CheckPlayerIsAdmin(1)) return;

            try
            {
                try
                {
                    var playerPos = NAPI.Entity.GetEntityPosition(player);
                    var target = NAPI.Pools.GetAllPlayers().Single(x => x.SocialClubName == social);

                    NAPI.Entity.SetEntityPosition(target, new Vector3(playerPos.X, playerPos.Y, playerPos.Z));

                    Notifications.SendInteractionInfo(player, "Spieler wurde erfolgreich teleportiert.");
                }
                catch
                {
                    Notifications.SendInteractionError(player, PlayerNotificationTexts.WrongInput);
                }
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"{this} - {e.Message}");
            }
        }

        [RemoteEvent("tpm")]
        public void TpmCmd(Client player, string social)
        {
            if (!new RankChecker(player).CheckPlayerIsAdmin(1)) return;

            try
            {
                try
                {
                    var target = NAPI.Pools.GetAllPlayers().Single(x => x.SocialClubName == social);
                    var targetPos = NAPI.Entity.GetEntityPosition(target);

                    NAPI.Entity.SetEntityPosition(player, new Vector3(targetPos.X, targetPos.Y, targetPos.Z));

                    Notifications.SendInteractionInfo(player, "Du wurdest erfolgreich teleportiert.");
                }
                catch
                {
                    player.SendNotification(PlayerNotificationTexts.WrongInput);
                }
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"{this} - {e.Message}");
            }
        }

        [RemoteEvent("addmod")]
        public void AddModCmd(Client player, int modType, int mod)
        {
            if (!new RankChecker(player).CheckPlayerIsAdmin(1)) return;

            try
            {
                try
                {
                    if (!player.IsInVehicle)
                    {
                        Notifications.SendInteractionError(player, PlayerNotificationTexts.NotInVehicle);
                    }
                    else
                    {
                        var veh = NAPI.Player.GetPlayerVehicle(player);
                        veh.SetMod(modType, mod);

                        Notifications.SendInteractionInfo(player, "Fahrzeug-Modifikation wurde installiert.");
                    }
                }
                catch
                {
                    Notifications.SendInteractionError(player, PlayerNotificationTexts.WrongInput);
                }
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"{this} - {e.Message}");
            }
        }

        [RemoteEvent("addwl")]
        public async void AddWlCmdAsync(Client player, string socialClubName)
        {
            if (!new RankChecker(player).CheckPlayerIsAdmin(1)) return;

            try
            {
                await RolePlayDbController.SetServerWhiteListEntry(socialClubName).ConfigureAwait(false);

                Notifications.SendInteractionInfo(player, socialClubName + PlayerNotificationTexts.WhitelistAddSuccessful);
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"{this} - {e.Message}");
            }
        }

        [RemoteEvent("car")]
        public void CarCmd(Client player, string model)
        {
            if (!new RankChecker(player).CheckPlayerIsAdmin(1)) return;

            NAPI.Task.Run(() =>
            {
                try
                {
                    var playerPos = NAPI.Entity.GetEntityPosition(player);
                    var newX = playerPos.X + 2.5;
                    var newY = playerPos.Y + 2.5;
                    var newZ = playerPos.Z + 0.5;
                    var newRot = player.Rotation;
                    var vehData = VehicleCfg.AllVehicles.Single(x => x.VehicleName == model);
                    var car = NAPI.Vehicle.CreateVehicle((uint)vehData.VehicleHash,
                        new Vector3(newX, newY, newZ), newRot.Z, (int)ColorEnums.Chrome, (int)ColorEnums.MatteBlack);

                    car.SetData(VehicleDataConst.AdminVehicleFlag, true);
                    car.SetSharedData(VehicleDataConst.FuelShared, 60);
                    car.SetSharedData(VehicleDataConst.TankCapacityShared, 120);
                    car.SetSharedData(VehicleDataConst.FuelConsShared, 5);

                    ApplyVehicleMods(car);
                }
                catch
                {
                    Notifications.SendInteractionError(player, "Der Fahrzeugname wurde nicht gefunden!");
                }
            });
        }

        private void ApplyVehicleMods(Vehicle car)
        {
            car.NumberPlate = "admincar";
            car.NumberPlateStyle = 1;
            car.EngineStatus = false;
            car.CustomPrimaryColor = new Color(0, 0, 225);
            car.CustomSecondaryColor = new Color(255, 140, 0);
            car.NeonColor = new Color(0, 0, 255);
            car.TrimColor = 1;
            car.SetMod(14, 1);
            car.SetMod(11, 3);
            car.SetMod(12, 2);
            car.SetMod(13, 2);
            car.SetMod(15, 3);
            car.SetMod(16, 4);
            car.SetMod(18, 0);
            car.SetMod(22, 0);
            car.SetMod(46, 2);
            car.SetMod(40, 3);
        }

        [RemoteEvent("garage")]
        public void GarageCmd(Client player, string number)
        {
            if (!new RankChecker(player).CheckPlayerIsAdmin(1)) return;

            try
            {
                player.Dimension = 0;

                switch (number)
                {
                    case "1":
                        player.Position = VehGarageCfg.SpawnPosSmall;
                        break;

                    case "2":
                        player.Position = VehGarageCfg.SpawnPosMed;
                        break;

                    case "3":
                        player.Position = VehGarageCfg.SpawnPosBig;
                        break;

                    case "4":
                        player.Position = ServerCfg.WorldSpawn;
                        break;

                    default:
                        Notifications.SendInteractionError(player, PlayerNotificationTexts.GarageCmdUsage);
                        break;
                }
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"{this} - {e.Message}");
            }
        }

        [RemoteEvent("getpos")]
        public async void GetPosCmd(Client player, string name)
        {
            if (!new RankChecker(player).CheckPlayerIsAdmin(1)) return;

            try
            {
                var playerPos = NAPI.Entity.GetEntityPosition(player);
                var playerRot = NAPI.Entity.GetEntityRotation(player);

                Notifications.SendInteractionInfo(player, $"POSITION = X: {playerPos.X} Y: {playerPos.Y} Z: {playerPos.Z}");
                Notifications.SendInteractionInfo(player, $"ROTATION = X: {playerRot.X} Y: {playerRot.Y} Z: {playerRot.Z}");

                var fs = new FileStream("MyPositions.txt", FileMode.Append, FileAccess.Write);
                var sw = new StreamWriter(fs);

                await sw.WriteLineAsync($"[{name}]  PosX = {playerPos.X}f, PosY = {playerPos.Y}f, PosZ = {playerPos.Z}f," +
                                    $" Rotation = {playerRot.Z}f").ConfigureAwait(false);

                sw.Close();
                fs.Close();
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"{this} - {e.Message}");
            }
        }
    }
}
