﻿using GTANetworkAPI;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using RolePlayGameData.EntityDataConsts;
using RolePlayGameData.Enums;
using RolePlayGameData.Models;
using RolePlayGameServer.Api;
using RolePlayGameServer.Database.Contexts;
using RolePlayGameServer.Database.Controllers;
using RolePlayGameServer.Dumpsters;
using RolePlayGameServer.Factions;
using RolePlayGameServer.Garages;
using RolePlayGameServer.Handlers;
using RolePlayGameServer.Locations;
using RolePlayGameServer.Players;
using RolePlayGameServer.Server;
using RolePlayGameServer.ServerLists;
using RolePlayGameServer.Util;
using RolePlayGameServer.Vehicles;
using System.Threading.Tasks;

namespace RolePlayGameServer.Events
{
    internal class ServerEvents : Script
    {
        [ServerEvent(Event.ResourceStart)]
        public async void OnResourceStartAsync()
        {
            Settings.SetSettings();
            Settings.SetupEnvironment();

            Task.Run(WebHost.CreateDefaultBuilder().UseStartup<Startup>().Build().Run).ConfigureAwait(false);
#if DEBUG
            using (var context = new MapObjectContext())
            {
                await context.Database.EnsureCreatedAsync().ConfigureAwait(false);
            }

            using (var context = new RolePlayContext())
            {
                await context.Database.EnsureCreatedAsync().ConfigureAwait(false);
            }
#endif
            await Task.Run(new DbDefaultController().InitDbDefaultAsync).ConfigureAwait(false);
            await Task.Run(OnlineState.SetAllPlayersOffline).ConfigureAwait(false);
            await Task.Run(new BlipLists().UpdateCivillianBlips).ConfigureAwait(false);

            Task.Run(new Loops().Init).ConfigureAwait(false);
            Task.Run(LoginQueue.StartLoginQueue).ConfigureAwait(false);

            NAPI.Task.Run(new GarageAreaSpawner(0).SpawnGarageDefaultLocations);
            NAPI.Task.Run(new WorldModification().DeleteWorldProps);
            NAPI.Task.Run(new VehicleShop().CheckCarsAction);
            NAPI.Task.Run(new FactionCars().Init);
            NAPI.Task.Run(new LocationSpawner().LoadMapMarks);
            NAPI.Task.Run(new DumpsterSpawner().LoadDumpsters);
            NAPI.Task.Run(new VehicleSpawner().SpawnAllOwnedVehicles);
        }

        [ServerEvent(Event.PlayerDamage)]
        public void OnPlayerDamage(Client player, float healthLoss, float armorLoss)
        {
            player.TriggerEvent("showHitMarker");
        }

        [ServerEvent(Event.ResourceStop)]
        public void OnResourceStop()
        {
            foreach (var item in NAPI.Pools.GetAllPlayers())
                item.TriggerEvent("disconnectTs");
        }

        [ServerEvent(Event.PlayerConnected)]
        public void OnPlayerConnected(Client player)
        {
            player.ResetData(PlayerDataConst.IsInLoginActionFlag);
            player.ResetData(PlayerDataConst.IsLoggedInFlag);

            Logging.PlayerInfoLog($"Player: {player.SocialClubName} connected");
        }

        [ServerEvent(Event.PlayerDisconnected)]
        public void OnPlayerDisconnected(Client player, DisconnectionType type, string reason)
        {
            player.ResetData(PlayerDataConst.IsLoggedInFlag);

            Logging.PlayerInfoLog($"Player: {player.SocialClubName} disconnected. Type: {type.ToString()} Reason: {reason}");
        }

        [ServerEvent(Event.PlayerSpawn)]
        public void OnPlayerSpawn(Client player)
        {
            player.Dimension = 0;
            player.SetSharedData(VoiceDataConst.VoiceStatus, VoiceDataConst.StatusAlive);

            if (player.HasData(PlayerDataConst.IsLoggedInFlag)) return;

            Task.Run(() => LoginQueue.AddPlayerToLogin(player));
        }

        [ServerEvent(Event.PlayerDeath)]
        public void OnPlayerDeath(Client player, Client killer, uint weapon)
        {
            player.SetSharedData(VoiceDataConst.VoiceStatus, VoiceDataConst.StatusDeath);

            PlayerBiology.SetFullBiology(player).ConfigureAwait(false);

            Logging.PlayerInfoLog($"Player: {player.SocialClubName} killed. Murder: {killer.SocialClubName} Weapon: {weapon}");
        }

        [ServerEvent(Event.Update)]
        public void OnUpdate()
        {
            Settings.RealTime();
        }

        [ServerEvent(Event.PlayerEnterColshape)]
        public async void OnPlayerEnterColShape(ColShape shape, Client player)
        {
            player.SendNotification("COLSHAPE TRGGERED");

            if (!player.IsInVehicle) await Task.Run(() => new ColShapeEnterExit(player, shape).OnEnter()).ConfigureAwait(false);
            else
            {
                if (player.Vehicle.HasData(VehicleDataConst.AdminVehicleFlag)) return;

                var markData = (MarkDataModel)shape.GetData(MarkDataConst.MarkData);

                switch (markData.MarkType)
                {
                    case (uint)MarkTypeEnums.CivSmallGarage:
                        await Task.Run(() => SmallGarage.Instance.ToGarage(player, markData)).ConfigureAwait(false);
                        break;
                    case (uint)MarkTypeEnums.CivMediumGarage:
                        await Task.Run(() => MediumGarage.Instance.ToGarage(player, markData)).ConfigureAwait(false);
                        break;
                    case (uint)MarkTypeEnums.CivBigGarage:
                        await Task.Run(() => BigGarage.Instance.ToGarage(player, markData)).ConfigureAwait(false);
                        break;
                    case (uint)MarkTypeEnums.CivTruckGarage:
                        break;
                    default:
                        return;
                }
            }
        }

        [ServerEvent(Event.PlayerExitColshape)]
        public void OnPlayerExitColShape(ColShape shape, Client player)
        {
            if (player.IsInVehicle) return;

            Task.Run(() => new ColShapeEnterExit(player, shape).OnExit()).ConfigureAwait(false);
        }
    }
}