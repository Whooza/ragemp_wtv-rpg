﻿using GTANetworkAPI;
using RolePlayGameData.Configs;
using RolePlayGameData.Database.RolePlayModels;
using RolePlayGameData.Models;
using RolePlayGameServer.Database.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RolePlayGameServer.Market
{
    internal sealed class MarketSystem
    {
        public static Queue<CalculationModel> ActiveCalculations { get; private set; } = new Queue<CalculationModel>();
        public static MarketEventModel LastMarketEvent { get; private set; } = null;

        private static List<ItemModel> _itemCfg => ItemCfg.ItemConfig;
        private List<MarketSystemModel> _currentMarketPrices = GetCurrentPrices().Result;

        private static readonly Lazy<MarketSystem> _instance = new Lazy<MarketSystem>(() => new MarketSystem());

        private MarketSystem() { }

        public static MarketSystem Instance
        {
            get
            {
                return _instance.Value;
            }
        }

        public async Task<bool> CalucateAction()
        {
            if (!ActiveCalculations.Any())
            {
#if DEBUG
                NAPI.Util.ConsoleOutput("[MarketSystem]: no calc items found");
#endif
                return true;
            }

            var entry = ActiveCalculations.Dequeue();
            var dbItems = await RolePlayDbController.GetCurrentPricesByItemType(entry.ItemType).ConfigureAwait(false);

            foreach (var item in dbItems)
            {
                if (item.ItemName == entry.ItemName) entry.IsSell = !entry.IsSell;

                var cfg = _itemCfg.First(x => (uint)x.ItemName == item.ItemName);
                var pChange = cfg.DefaultPrice / 1000;

                if ((item.ChangeDate - DateTime.Now).TotalMinutes > 120)
                {
                    if (item.CurrentPrice == cfg.MaxPrice) item.CurrentPrice -= (cfg.MaxPrice - cfg.DefaultPrice) / 2;
                    if (item.CurrentPrice == cfg.MinPrice) item.CurrentPrice += (cfg.DefaultPrice - cfg.MinPrice) / 2;

                    item.ChangeDate = DateTime.Now;
                }
                else
                {
                    if (entry.IsSell)
                    {
                        if (item.CurrentPrice + (pChange * entry.Amount) >= cfg.MaxPrice) item.CurrentPrice = cfg.MaxPrice;
                        else
                        {
                            item.CurrentPrice += pChange * entry.Amount;
                            item.ChangeDate = DateTime.Now;                            
                        }
                    }
                    else
                    {
                        if (item.CurrentPrice - (pChange * entry.Amount) <= cfg.MinPrice) item.CurrentPrice = cfg.MinPrice;
                        else
                        {
                            item.CurrentPrice -= pChange * entry.Amount;
                            item.ChangeDate = DateTime.Now;
                        }
                    }
                }

                if (item.ItemName == entry.ItemName) entry.IsSell = !entry.IsSell;

                await RolePlayDbController.UpdateMarketPrice(item).ConfigureAwait(false);
            }

            await ProcessRandomEvent(dbItems[new Random().Next(0, dbItems.Count())]);
#if DEBUG
            NAPI.Util.ConsoleOutput("[MarketSystem]: calculation complete");
#endif
            return true;
        }

        private async Task ProcessRandomEvent(MarketSystemModel item)
        {
            var rnd = new Random();
            var chance = rnd.Next(0, 3000);

            if (chance != 2999) return;

            var events = MarketEvents.MarktetEventList;

            if (LastMarketEvent != null) events.Remove(LastMarketEvent);

            var selected = events[rnd.Next(0, events.Count)];

            foreach (var itemName in selected.ItemNames)
            {
                var cfg = _itemCfg.First(x => (uint)x.ItemName == itemName);

                var dbEntry = await RolePlayDbController.GetCurrentPriceByItemName(itemName).ConfigureAwait(false);

                if (selected.IsPositive)
                {
                    if (dbEntry.CurrentPrice > cfg.DefaultPrice) dbEntry.CurrentPrice *= 1.25f;
                    else dbEntry.CurrentPrice = cfg.DefaultPrice * 1.25f;
                }
                else
                {
                    if (dbEntry.CurrentPrice > cfg.DefaultPrice) dbEntry.CurrentPrice *= 0.75f;
                    else dbEntry.CurrentPrice = cfg.DefaultPrice * 0.75f;
                }

                dbEntry.ChangeDate = DateTime.Now;

                await RolePlayDbController.UpdateMarketPrice(item).ConfigureAwait(false);
            }

            NAPI.Notification.SendNotificationToAll($"[Marktsystem]: {selected.EventName} - {selected.EventText}");
#if DEBUG
            NAPI.Util.ConsoleOutput($"[MarketSystem]: {selected.EventName} - event triggered!");
#endif            
        }

        public bool AddItem(uint itemName, uint itemType, uint amount, bool isSell)
        {
            var newCalc = new CalculationModel
            {
                ItemName = itemName,
                ItemType = itemType,
                Amount = amount,
                IsSell = isSell
            };

            ActiveCalculations.Enqueue(newCalc);
#if DEBUG
            NAPI.Util.ConsoleOutput($"[MarketSystem]: ITEM ADDED");
#endif
            return true;
        }

        public async Task<bool> ResetAllPrices()
        {
            ActiveCalculations.Clear();

            foreach (var item in _currentMarketPrices)
            {
                item.CurrentPrice = _itemCfg.First(x => (uint)x.ItemName == item.ItemName).DefaultPrice;

                await Task.Run(() => RolePlayDbController.UpdateMarketPrice(item)).ConfigureAwait(false);
            }
#if DEBUG
            NAPI.Util.ConsoleOutput($"[MarketSystem]: PRICE RESET COMPLETE");
#endif
            return true;
        }

        private static async Task<List<MarketSystemModel>> GetCurrentPrices() =>
            await RolePlayDbController.GetCurrentPrices().ConfigureAwait(false);
    }
}
