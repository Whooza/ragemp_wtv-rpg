﻿using RolePlayGameData.Enums;
using RolePlayGameData.Models;
using System.Collections.Generic;

namespace RolePlayGameServer.Market
{
    internal class MarketEvents
    {
        public static readonly List<MarketEventModel> MarktetEventList = new List<MarketEventModel>
        {
            new MarketEventModel
            { 
                EventName = "Polizei meldet Erfolg",
                EventText = "Ein Kartell wurde von der Polizei zerschlagen. Hauptsächlich handelte dieses mit chemischen Drogen.",
                ItemNames = new uint[]{ (uint)ItemNameEnums.Lsd, (uint)ItemNameEnums.Ecstasy, (uint)ItemNameEnums.CrystalMeth },
                IsPositive = true
            },
            new MarketEventModel
            { 
                EventName = "Gelder bewilligt",
                EventText = "Die Stadt Los Santos kauft vermehrt Baumaterialen ein, um öffentliche Gebäude zu sanieren.",
                ItemNames = new uint[]{ (uint)ItemNameEnums.Cement },
                IsPositive = true
            },
            new MarketEventModel
            { 
                EventName = "Drogenwelle rollt an",
                EventText = "Im Hafen von Los Santos sollen Berichten zu folge große Mengen Kokain geschmuggelt worden sein.",
                ItemNames = new uint[]{ (uint)ItemNameEnums.Cocaine },
                IsPositive = false
            },
            new MarketEventModel
            { 
                EventName = "Reparaturen abgeschlossen",
                EventText = "Die Stadt Los Santos hat ihr umfassenden Sanierungsabeiten abgeschlossen.",
                ItemNames = new uint[]{ (uint)ItemNameEnums.Cement },
                IsPositive = false
            }
        };
    }
}
