﻿using Newtonsoft.Json;
using RolePlayGameData.Configs;
using RolePlayGameData.Database.RolePlayModels;
using RolePlayGameData.EntityDataConsts;
using RolePlayGameData.Models;
using RolePlayGameData.Texts;
using RolePlayGameServer.Database.Controllers;
using RolePlayGameServer.Players;
using RolePlayGameServer.Util;
using System;
using System.Collections.Generic;

namespace RolePlayGameServer.Market
{
    internal class AuctionHouse
    {
        public static bool IsProcess { get; private set; }

        private AuctionQueueModel _auction;

        public AuctionHouse(AuctionQueueModel auctionData)
        {
            _auction = auctionData;
        }

        public async void CreateAuction()
        {
            try
            {
                var playerMoney = (PlayerMoneyModel)_auction.Player.GetData(PlayerDataConst.PlayerMoney);
                var cash = playerMoney.CashMoney;

                var pricePerDay = _auction.Duration * AuctionCfg.DailyPrice;
                var instantBuy = 0f;

                if (_auction.IsInstantBuy) instantBuy = AuctionCfg.InstantBuyTax;

                var totalPrice = AuctionCfg.StartPrice + pricePerDay + instantBuy;

                if (!await new PlayerMoney(_auction.Player).RemoveCashMoney(totalPrice).ConfigureAwait(false)) return;

                if (await RolePlayDbController.SetAuction(_auction.Player.SocialClubName, _auction.ItemName, _auction.Amount,
                    _auction.StartPrice, _auction.IsInstantBuy, _auction.InstantBuyPrice, _auction.Duration)
                    .ConfigureAwait(false))
                {
                    await new PlayerInventory(_auction.Player).DestroyInvItem(_auction.ItemName, _auction.Amount)
                        .ConfigureAwait(false);
                    _auction.Player.SendNotification(PlayerNotificationTexts.AuctionCreated);
                }
                else
                {
                    _auction.Player.SendNotification(PlayerNotificationTexts.AuctionNotCreated);
                    await new PlayerMoney(_auction.Player).AddCashMoney(totalPrice).ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                Logging.AuctionHouseErrorLog(e.Message);
            }
        }

        public async void CancelAuction()
        {
            try
            {
                AuctionModel dbAuction = await RolePlayDbController.GetAuction(_auction.AuctionId).ConfigureAwait(false);

                dbAuction.IsActive = false;

                if (await RolePlayDbController.UpdateAuction(dbAuction).ConfigureAwait(false))
                    _auction.Player.SendNotification(PlayerNotificationTexts.AuctionCanceled);
            }
            catch (Exception e)
            {
                Logging.AuctionHouseErrorLog(e.Message);
            }
        }

        public async void OfferAuction()
        {
            try
            {
                AuctionModel dbAuction = await RolePlayDbController.GetAuction(_auction.AuctionId).ConfigureAwait(false);

                if ((dbAuction.StartPrice < _auction.Offer) && (dbAuction.MaxOffer < _auction.Offer))
                {
                    dbAuction.MaxOffer = _auction.Offer;
                    dbAuction.MaxTenderer = _auction.Player.SocialClubName;

                    var tendererDic = JsonConvert.DeserializeObject<SortedDictionary<string, float>>(dbAuction.OldTenderer);

                    tendererDic.TryGetValue(_auction.Player.SocialClubName, out var bitPrice);

                    if (bitPrice != default) 
                    {
                        tendererDic[_auction.Player.SocialClubName] = _auction.Offer -= bitPrice;
                    }
                    else tendererDic.Add(_auction.Player.SocialClubName, _auction.Offer);

                    dbAuction.OldTenderer = JsonConvert.SerializeObject(tendererDic);

                    if (!await new PlayerMoney(_auction.Player).RemoveCashMoney(_auction.Offer).ConfigureAwait(false)) return;

                    if (await RolePlayDbController.UpdateAuction(dbAuction).ConfigureAwait(false))
                        _auction.Player.SendNotification(PlayerNotificationTexts.AuctionOfferSet);
                    else
                    {
                        await new PlayerMoney(_auction.Player).AddCashMoney(_auction.Offer).ConfigureAwait(false);
                        _auction.Player.SendNotification(PlayerNotificationTexts.AuctionOfferNotSet);
                    }
                }
            }
            catch (Exception e)
            {
                Logging.AuctionHouseErrorLog(e.Message);
            }
        }

        public void BuyAuction()
        {
            try
            {



            }
            catch (Exception e)
            {
                Logging.AuctionHouseErrorLog(e.Message);
            }
        }

        public void PickUpItem()
        {
            try
            {

            }
            catch (Exception e)
            {
                Logging.AuctionHouseErrorLog(e.Message);
            }
        }
    }
}
