﻿using GTANetworkAPI;
using RolePlayGameData.Enums;
using RolePlayGameData.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RolePlayGameServer.Market
{
    public class AuctionHouseQueue
    {
        public static Queue<AuctionQueueModel> ActiveProcesses { get; private set; } = new Queue<AuctionQueueModel>();

        private static bool _inProgress;

        public async void QueueExit()
        {
            while (true)
            {
                await Task.Delay(1000).ConfigureAwait(false);
                
                if (AuctionHouse.IsProcess || _inProgress) continue;

                if (ActiveProcesses.Any())
                {
                    AuctionQueueModel item = ActiveProcesses.Dequeue();

                    switch (item.Action)
                    {
                        case AuctionActionEnums.Create:
                            new AuctionHouse(item).CreateAuction();
                            break;
                        case AuctionActionEnums.Cancel:
                            new AuctionHouse(item).CancelAuction();
                            break;
                        case AuctionActionEnums.Offer:
                            new AuctionHouse(item).OfferAuction();
                            break;
                        case AuctionActionEnums.Buy:
                            new AuctionHouse(item).BuyAuction();
                            break;
                        case AuctionActionEnums.PickUp:
                            new AuctionHouse(item).PickUpItem();
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        public bool Create(Client player, uint itemName, uint amount, float startPrice, bool isInstantBuy,
            float instantBuyPrice, uint duration)
        {
            _inProgress = true;

            var auction = new AuctionQueueModel
            {
                Player = player,
                Action = AuctionActionEnums.Create,
                ItemName = itemName,
                Amount = amount,
                StartPrice = startPrice,
                IsInstantBuy = isInstantBuy,
                InstantBuyPrice = instantBuyPrice,
                Duration = duration
            };

            ActiveProcesses.Enqueue(auction);

            _inProgress = false;

            return true;
        }

        public bool Cancel(Client player, int auctionId)
        {
            _inProgress = true;

            var auction = new AuctionQueueModel
            { 
                Player = player,
                AuctionId = auctionId,
                Action = AuctionActionEnums.Cancel
            };

            ActiveProcesses.Enqueue(auction);

            _inProgress = false;

            return true;
        }

        public bool Offer(Client player, int auctionId, uint offerPrice)
        {
            _inProgress = true;

            var auction = new AuctionQueueModel
            { 
                Player = player,
                AuctionId = auctionId,
                Action = AuctionActionEnums.Offer,
                Offer = offerPrice
            };

            ActiveProcesses.Enqueue(auction);

            _inProgress = false;

            return true;
        }

        public bool Buy(Client player, int auctionId)
        {
            _inProgress = true;

            var auction = new AuctionQueueModel
            { 
                Player = player,
                AuctionId = auctionId,
                Action = AuctionActionEnums.Buy
            };

            ActiveProcesses.Enqueue(auction);

            _inProgress = false;

            return true;
        }

        public bool PickUp(Client player, int auctionId)
        {
            _inProgress = true;

            var auction = new AuctionQueueModel
            { 
                Player = player,
                AuctionId = auctionId,
                Action = AuctionActionEnums.PickUp
            };

            ActiveProcesses.Enqueue(auction);

            _inProgress = false;

            return true;
        }
    }
}