﻿using System.Threading.Tasks;
using RolePlayGameServer.Players;
using RolePlayGameServer.Util;
using RolePlayGameServer.Vehicles;
using RolePlayGameServer.Market;

namespace RolePlayGameServer.Server
{
    internal class Loops
    {
        private readonly PlayerBiology _playerBiology = new PlayerBiology();
        private readonly PlayerPaycheck _playerPaycheck = new PlayerPaycheck();
        private readonly PlayerPosition _playerPosition = new PlayerPosition();
        private readonly VehicleFuel _vehicleFuel = new VehicleFuel();
        private readonly VehiclePosition _vehiclePos = new VehiclePosition();

        public void Init()
        {
#if DEBUG
            Logging.ServerInfoLog($"Starting: {this}");
#endif
            Task.Run(Weather);
            Task.Run(Positions);
            Task.Run(PlayerPaycheck);
            Task.Run(PlayerBiology);
            Task.Run(VehicleFuel);
            Task.Run(MarketCalc);
        }

        private async Task Weather()
        {
            while (true)
            {
                await Task.Delay(1000).ConfigureAwait(false);

                if (WeatherSystem.WeatherSequenceActive) continue;

                new WeatherSystem().ExecuteWeatherChange();
            }
        }

        private async Task Positions()
        {
            await Task.Delay(10000).ConfigureAwait(false);

            while (true)
            {
                await _playerPosition.StartPlayerPosAsync().ConfigureAwait(false);

                await Task.Delay(1000).ConfigureAwait(false);

                await _vehiclePos.StartVehiclePosAsync().ConfigureAwait(false);

                await Task.Delay(1000).ConfigureAwait(false);
            }
        }

        private async Task PlayerPaycheck()
        {
            await Task.Delay(20000).ConfigureAwait(false);

            while (true)
            {
                await _playerPaycheck.StartPlayerPayAsync().ConfigureAwait(false);

                await Task.Delay(60000).ConfigureAwait(false);
            }
        }

        private async Task PlayerBiology()
        {
            await Task.Delay(30000).ConfigureAwait(false);

            while (true)
            {
                await _playerBiology.StartPlayerBioAsync().ConfigureAwait(false);

                await Task.Delay(60000).ConfigureAwait(false);
            }
        }

        private async Task VehicleFuel()
        {
            await Task.Delay(40000).ConfigureAwait(false);

            while (true)
            {
                await _vehicleFuel.StartVehicleFuelAsync().ConfigureAwait(false);

                await Task.Delay(60000).ConfigureAwait(false);
            }
        }

        private async Task MarketCalc()
        {
            await Task.Delay(50000).ConfigureAwait(false);

            while (true)
            {
                await MarketSystem.Instance.CalucateAction().ConfigureAwait(false);

                await Task.Delay(1000).ConfigureAwait(false);
            }
        }
    }
}