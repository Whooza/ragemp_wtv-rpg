﻿using System;
using System.Threading.Tasks;
using GTANetworkAPI;
using RolePlayGameServer.Database.Controllers;
using RolePlayGameServer.Util;

namespace RolePlayGameServer.Server
{
    internal class OnlineState
    {
        private readonly Client _player;

        public OnlineState(Client player)
        {
            _player = player;
        }

        public async Task<bool> SetPlayerToOnlineAsync()
        {
            try
            {
                var whiteListEntry = await RolePlayDbController.GetServerWhitelistEntry(_player.SocialClubName)
                    .ConfigureAwait(false);

                whiteListEntry.LastIp = _player.Address == "127.0.0.1" ? "::1" : _player.Address;
                whiteListEntry.IsOnline = true;

                await RolePlayDbController.UpdateServerWhiteListEntry(whiteListEntry)
                    .ConfigureAwait(false);

                Logging.PlayerInfoLog($"Player: {_player.SocialClubName} is now set to online!");
                return true;
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"{this} - {e.Message}");
                return false;
            }
        }

        public async Task<bool> SetPlayerToOfflineAsync()
        {
            try
            {
                var whiteListEntry = await RolePlayDbController.GetServerWhitelistEntry(_player.SocialClubName)
                    .ConfigureAwait(false);

                whiteListEntry.IsOnline = false;

                await RolePlayDbController.UpdateServerWhiteListEntry(whiteListEntry)
                    .ConfigureAwait(false);

                return true;
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"{this} - {e.Message}");
                return false;
            }
        }

        public static async Task<bool> SetAllPlayersOffline()
        {
            try
            {
                var whitelist = await RolePlayDbController.GetServerWhitelistList()
                    .ConfigureAwait(false);

                foreach (var entry in whitelist)
                {
                    entry.IsOnline = false;
                    await RolePlayDbController.UpdateServerWhiteListEntry(entry)
                        .ConfigureAwait(false);
                }

                return true;
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"SetAllPlayersOffline - {e.Message}");
                return false;
            }
        }
    }
}