﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GTANetworkAPI;
using RolePlayGameData.EntityDataConsts;
using RolePlayGameServer.Util;

namespace RolePlayGameServer.Server
{
    internal class LoginQueue
    {
        private static readonly Queue<Client> WaitList = new Queue<Client>();

        public static void AddPlayerToLogin(Client player)
        {
            if (WaitList.Contains(player)) return;

            WaitList.Enqueue(player);
        }

        public static async Task StartLoginQueue()
        {
            while (true)
            {
                await Task.Delay(1000).ConfigureAwait(false);

                try
                {
                    if (WaitList.Count == 0) continue;
                    if (GetPlayersInLoginAction() > 50) continue;

                    var player = WaitList.Dequeue();

                    player.SetData(PlayerDataConst.IsInLoginActionFlag, true);

                    await Task.Run(() => new Whitelist(player).CheckWhitelistEntry())
                        .ConfigureAwait(false);
                }
                catch (Exception e)
                {
                    Logging.ServerErrorLog(e.Message);
                }
            }
        }

        private static int GetPlayersInLoginAction()
        {
            try
            {
                var count = 0;

                foreach (var item in NAPI.Pools.GetAllPlayers())
                {
                    if (!item.HasData(PlayerDataConst.IsInLoginActionFlag)) continue;

                    count += 1;
                }

                return count;
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog(e.Message);
                return 0;
            }
        }
    }
}