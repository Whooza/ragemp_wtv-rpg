﻿using GTANetworkAPI;
using RolePlayGameData.Configs;
using RolePlayGameServer.Util;
using System;
using System.Threading.Tasks;

namespace RolePlayGameServer.Server
{
    internal class WeatherSystem
    {
        public static bool WeatherSequenceActive { get; private set; } = false;

        private readonly Random _rnd = new Random();
        private int WeatherDuration = 0;
        private int WeatherType = 0;

        public WeatherSystem()
        {
            if (WeatherDuration == 0) WeatherDuration = _rnd.Next(30, 90);
            if (WeatherType == 0) WeatherType = _rnd.Next(1, 6);
            if (WeatherType == 1) WeatherDuration *= 2;
        }

        public void ExecuteWeatherChange()
        {
            WeatherSequenceActive = true;

            switch (WeatherType)
            {
                case 1:
                    Task.Run(() => BasicSequence()).ConfigureAwait(false);
#if DEBUG
                    Notifications.SendSystemInfoToAll("Wettersystem", $"{WeatherDuration} Minuten sonniges Wetter!");
#endif
                    break;
                case 2:
                    Task.Run(() => RainSequence()).ConfigureAwait(false);
#if DEBUG
                    Notifications.SendSystemInfoToAll("Wettersystem", $"{WeatherDuration + 12} Minuten regnerisches Wetter!");
#endif
                    break;
                case 3:
                    Task.Run(() => BasicSequence()).ConfigureAwait(false);
#if DEBUG
                    Notifications.SendSystemInfoToAll("Wettersystem", $"{WeatherDuration} Minuten sonniges Wetter!");

#endif
                    break;
                case 4:
                    Task.Run(() => ThunderSequence()).ConfigureAwait(false);
#if DEBUG
                    Notifications.SendSystemInfoToAll("Wettersystem", $"{WeatherDuration + 14} Minuten Gewitter!");
#endif
                    break;
                case 5:
                    Task.Run(() => BasicSequence()).ConfigureAwait(false);
#if DEBUG
                    Notifications.SendSystemInfoToAll("Wettersystem", $"{WeatherDuration} Minuten sonniges Wetter!");
#endif
                    break;

            }
        }

        private async void BasicSequence()
        {
            for (int i = 0; i < WeatherDuration; i++)
            {
                TriggerWeatherUpdate(WeatherCfg.Sunny);
                await Task.Delay(55000);
            }

            WeatherType = 0;
            WeatherDuration = 0;
            WeatherSequenceActive = false;
        }

        private async void RainSequence()
        {
            foreach (var item in WeatherCfg.RainIntro)
            {
                TriggerWeatherUpdate(item);
                await Task.Delay(55000);
            }

            for (int i = 0; i < WeatherDuration; i++)
            {
                TriggerWeatherUpdate(WeatherCfg.Rain);
                await Task.Delay(55000);
            }

            foreach (var item in WeatherCfg.RainOutro)
            {
                TriggerWeatherUpdate(item);
                await Task.Delay(55000);
            }

            WeatherType = 0;
            WeatherDuration = 0;
            WeatherSequenceActive = false;
        }

        private async void ThunderSequence()
        {
            foreach (var item in WeatherCfg.ThunderIntro)
            {
                TriggerWeatherUpdate(item);
                await Task.Delay(55000);
            }

            for (int i = 0; i < WeatherDuration; i++)
            {
                TriggerWeatherUpdate(WeatherCfg.Thunder);
                await Task.Delay(55000);
            }

            foreach (var item in WeatherCfg.ThunderOutro)
            {
                TriggerWeatherUpdate(item);
                await Task.Delay(55000);
            }

            WeatherType = 0;
            WeatherDuration = 0;
            WeatherSequenceActive = false;
        }

        private void TriggerWeatherUpdate(string weather)
        {
            foreach (var item in NAPI.Pools.GetAllPlayers())
            {
                item.TriggerEvent("changeWeather", weather);
#if DEBUG
                Notifications.SendSystemInfo(item, "Aktuelles Wetter", weather);
#endif                
            }
        }
    }
}