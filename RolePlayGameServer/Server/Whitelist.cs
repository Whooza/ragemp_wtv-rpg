﻿using GTANetworkAPI;
using RolePlayGameServer.Database.Controllers;
using RolePlayGameServer.Util;
using System;
using System.Globalization;
using System.Threading.Tasks;

namespace RolePlayGameServer.Server
{
    internal class Whitelist
    {
        private readonly Client _player;

        public Whitelist(Client player) => _player = player;

        public async Task CheckWhitelistEntry()
        {
            try
            {
                var whitelistEntry = await RolePlayDbController.GetServerWhitelistEntry(_player.SocialClubName)
                    .ConfigureAwait(false);

                if (whitelistEntry == null)
                {
                    UiHelper.ShowNotWhitelisted(_player);
                    return;
                }

                if (whitelistEntry.BannedUntil >= DateTime.Now)
                {
                    whitelistEntry.BannedUntil.ToString("dd/MM/yyyy", CultureInfo.CurrentCulture);
                    UiHelper.ShowBanWindow(_player);
                    _player.Kick();
                    return;
                }

                var account = await RolePlayDbController.GetPlayerAccBySocial(_player.SocialClubName)
                    .ConfigureAwait(false);

                if (account == null)
                {
                    await new OnlineState(_player).SetPlayerToOnlineAsync()
                        .ConfigureAwait(false);

                    UiHelper.ShowRegisterWindow(_player);
                    return;
                }

                await new OnlineState(_player).SetPlayerToOnlineAsync()
                    .ConfigureAwait(false);

                UiHelper.ShowLoginWindow(_player);
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"{this} - {e.Message}");
            }
        }
    }
}