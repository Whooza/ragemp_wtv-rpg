﻿using GTANetworkAPI;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace RolePlayGameServer.Server
{
    internal class Settings
    {
        public static void SetupEnvironment()
        {
            ThreadPool.GetMinThreads(out var minWorker, out var minComp);
            NAPI.Util.ConsoleOutput($"minWorker: {minWorker} - minComp: {minComp}");
            ThreadPool.GetMaxThreads(out var maxWorker, out var maxComp);
            NAPI.Util.ConsoleOutput($"maxWorker: {maxWorker} - maxComp: {maxComp}");

            ThreadPool.SetMinThreads(1000, 100);
        }

        public static void SetSettings()
        {
            NAPI.Server.SetGlobalServerChat(false);
            NAPI.Server.SetDefaultSpawnLocation(new Vector3(275.446, -1361.11, 25));
        }

        public static void RealTime()
        {
            var dT = DateTime.Now;

            NAPI.World.SetTime(dT.Hour, dT.Minute, dT.Second);
        }
    }
}