﻿using System;
using System.Threading.Tasks;
using GTANetworkAPI;
using RolePlayGameData.Database.RolePlayModels;
using RolePlayGameData.EntityDataConsts;
using RolePlayGameData.Enums;
using RolePlayGameServer.Database.Controllers;
using RolePlayGameServer.Util;

namespace RolePlayGameServer.Handlers
{
    internal class ItemActions
    {
        private readonly Client _player;

        public ItemActions(Client player)
        {
            _player = player;
        }

        public bool ExecuteItemAction(ItemTypeEnums itemType, float value)
        {
            switch (itemType)
            {
                case ItemTypeEnums.Food:
                    return ChangeBiology(true, value).Result;

                case ItemTypeEnums.SoftDrink:
                    return ChangeBiology(false, value).Result;
                /*
                case ItemTypeEnums.Drink:
                    break;

                case ItemTypeEnums.Smoke:
                    break;

                case ItemTypeEnums.Drug:
                    break;

                case ItemTypeEnums.Tool:
                    break;

                case ItemTypeEnums.Medical:
                    break;

                case ItemTypeEnums.Technical:
                    break;
                    
                case ItemTypeEnums.JobItem:
                    break;

                case ItemTypeEnums.MissionItem:
                    break;
                    */
                default:
                    Logging.ServerErrorLog($"{this} - itemType not found ({itemType.ToString()})");
                    return false;
            }
        }

        private async Task<bool> ChangeBiology(bool bioType, float value)
        {
            try
            {
                var biology = (PlayerBiologyModel) _player.GetData(PlayerDataConst.PlayerBiology);

                if (bioType)
                {
                    biology.Hunger += value;

                    if (biology.Hunger > 100f) biology.Hunger = 100f;

                    UiHelper.SendHungerWarning(_player, biology.Hunger);
                }

                if (!bioType)
                {
                    biology.Thirst += value;

                    if (biology.Thirst > 100f) biology.Thirst = 100f;

                    UiHelper.SendThirstWarning(_player, biology.Thirst);
                }

                _player.SetData(PlayerDataConst.PlayerBiology, biology);

                await RolePlayDbController.UpdatePlayerBioInDb(biology)
                    .ConfigureAwait(false);

                UiHelper.UpdatePlayerBiology(_player, biology.Hunger, biology.Thirst);

                return true;
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"SetFullBiologyAsync - {e.Message}");
                return false;
            }
        }
    }
}