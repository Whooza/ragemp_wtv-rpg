﻿using GTANetworkAPI;
using RolePlayGameData.Database.RolePlayModels;
using RolePlayGameData.EntityDataConsts;
using RolePlayGameData.Enums;
using RolePlayGameData.Models;
using RolePlayGameServer.ServerLists;
using System.Threading.Tasks;

namespace RolePlayGameServer.Handlers
{
    internal class ColShapeEnterExit
    {
        private readonly Client _player;
        private readonly PlayerAccountModel _playerData;
        private readonly MarkDataModel _shapeData;

        public ColShapeEnterExit(Client player, ColShape shape)
        {
            _player = player;
            _playerData = (PlayerAccountModel)player.GetData(PlayerDataConst.PlayerAccount);
            _shapeData = (MarkDataModel)shape.GetData(MarkDataConst.MarkData);
        }

        public async void OnEnter()
        {
            if (_shapeData == null)
            {
                NAPI.Util.ConsoleOutput("SHAPEDATA WAR NULL");
                return;
            }

            switch (_shapeData.MarkFaction)
            {
                case (uint)FactionEnums.Civ:
                    await Task.Run(() => LocationList.Instance.AddPlayer(_player, _shapeData)).ConfigureAwait(false);
                    break;
                case (uint)FactionEnums.Lsc:
                    if (_playerData.LscLevel == 0) return;
                    await Task.Run(() => LocationList.Instance.AddPlayer(_player, _shapeData)).ConfigureAwait(false);
                    break;
                case (uint)FactionEnums.Lsrs:
                    if (_playerData.LsrsLevel == 0) return;
                    await Task.Run(() => LocationList.Instance.AddPlayer(_player, _shapeData)).ConfigureAwait(false);
                    break;
                case (uint)FactionEnums.Lspd:
                    if (_playerData.LspdLevel == 0) return;
                    await Task.Run(() => LocationList.Instance.AddPlayer(_player, _shapeData)).ConfigureAwait(false);
                    break;
                case (uint)FactionEnums.Fib:
                    if (_playerData.FibLevel == 0) return;
                    await Task.Run(() => LocationList.Instance.AddPlayer(_player, _shapeData)).ConfigureAwait(false);
                    break;
                case (uint)FactionEnums.Lssd:
                    if (_playerData.LssdLevel == 0) return;
                    await Task.Run(() => LocationList.Instance.AddPlayer(_player, _shapeData)).ConfigureAwait(false);
                    break;
                case (uint)FactionEnums.Doj:
                    if (_playerData.DojLevel == 0) return;
                    await Task.Run(() => LocationList.Instance.AddPlayer(_player, _shapeData)).ConfigureAwait(false);
                    break;
                case (uint)FactionEnums.Dcr:
                    if (_playerData.DcrLevel == 0) return;
                    await Task.Run(() => LocationList.Instance.AddPlayer(_player, _shapeData)).ConfigureAwait(false);
                    break;
                default:
                    _player.SendNotification("~r~ShapeFaction nicht gefunden!");
                    break;
            }
        }

        public void OnExit() => Task.Run(() => LocationList.Instance.RemovePlayer(_player)).ConfigureAwait(false);
    }
}