﻿using GTANetworkAPI;
using RolePlayGameData.Database.RolePlayModels;
using RolePlayGameData.EntityDataConsts;
using RolePlayGameData.Enums;
using RolePlayGameData.Models;
using RolePlayGameServer.Dumpsters;
using RolePlayGameServer.Garages;
using RolePlayGameServer.Jobs;
using RolePlayGameServer.ServerLists;
using RolePlayGameServer.Util;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RolePlayGameServer.Handlers
{
    internal class InteractionButton
    {
        private readonly MarkDataModel _markData;
        private readonly Client _player;
        private readonly PlayerAccountModel _playerData;

        public InteractionButton(Client player)
        {
            _player = player;
            _playerData = (PlayerAccountModel)player.GetData(PlayerDataConst.PlayerAccount);
            _markData = LocationList.LocationPlayers.FirstOrDefault(x => x.Key == _player.SocialClubName).Value;
        }

        private async void InteractionWithInventory()
        {
            try
            {
                var vehicle = await NextEntity.NextVehicle(_player, 2f).ConfigureAwait(false);

                if (vehicle == default) return;

                if (vehicle.Locked)
                {
                    _player.SendNotification("~r~Das Fahrzeug ist verschlossen!");
                    return;
                }

                UiHelper.ShowVehicleTrunk(_player);
            }
            catch (Exception e)
            {
                NAPI.Util.ConsoleOutput($"InteractionWithInventory - {e.Message}");
                NAPI.Util.ConsoleOutput($"InteractionWithInventory - {e.InnerException}");
            }
        }

        public void ExecuteAction()
        {
            if (_markData == default)
            {
                InteractionWithInventory();
                return;
            }

            switch (_markData.MarkFaction)
            {
                case (uint)FactionEnums.Civ:
                    CivAction();
                    break;
                case (uint)FactionEnums.Lspd:
                    if (_playerData.LspdLevel > 0) LspdAction();
                    break;
                case (uint)FactionEnums.Lssd:
                    if (_playerData.LssdLevel > 0) LssdAction();
                    break;
                case (uint)FactionEnums.Fib:
                    if (_playerData.FibLevel > 0) FibAction();
                    break;
                case (uint)FactionEnums.Lsrs:
                    if (_playerData.LsrsLevel > 0) LsrsAction();
                    break;
                case (uint)FactionEnums.Lsc:
                    if (_playerData.LscLevel > 0) LscAction();
                    break;
                case (uint)FactionEnums.Doj:
                    if (_playerData.DojLevel > 0) DojAction();
                    break;
                case (uint)FactionEnums.Dcr:
                    if (_playerData.DcrLevel > 0) DcrAction();
                    break;
                default:
                    return;
            }
        }

        public void CivAction()
        {
            switch (_markData.MarkType)
            {
                case (uint)MarkTypeEnums.GarageExitSmall:
                    Task.Run(() => SmallGarage.Instance.OutOfGarage(_player, _markData)).ConfigureAwait(false);
                    break;
                case (uint)MarkTypeEnums.GarageExitMedium:
                    Task.Run(() => MediumGarage.Instance.OutOfGarage(_player, _markData)).ConfigureAwait(false);
                    break;
                case (uint)MarkTypeEnums.GarageExitBig:
                    Task.Run(() => BigGarage.Instance.OutOfGarage(_player, _markData)).ConfigureAwait(false);
                    break;
                case (uint)MarkTypeEnums.CivGouvernment:
                    _player.SendNotification($"~b~[{MarkTypeEnums.CivGouvernment}] ~y~noch nicht implementiert");
                    break;
                case (uint)MarkTypeEnums.CivCityhall:
                    UiHelper.ShowCityHall(_player);
                    break;
                case (uint)MarkTypeEnums.CivJobcenter:
                    UiHelper.ShowJobCenter(_player);
                    break;
                case (uint)MarkTypeEnums.CivLicenseOffice:
                    _player.SendNotification($"~b~[{MarkTypeEnums.CivLicenseOffice}] ~y~noch nicht implementiert");
                    break;
                case (uint)MarkTypeEnums.CivAtm:
                    UiHelper.ShowAtm(_player);
                    break;
                case (uint)MarkTypeEnums.CivBank:
                    _player.SendNotification($"~b~[{MarkTypeEnums.CivBank}] ~y~noch nicht implementiert");
                    break;
                case (uint)MarkTypeEnums.CivCentralbank:
                    _player.SendNotification($"~b~[{MarkTypeEnums.CivCentralbank}] ~y~noch nicht implementiert");
                    break;
                case (uint)MarkTypeEnums.CivSmallGarage:
                    Task.Run(() => SmallGarage.Instance.ToGarage(_player, _markData)).ConfigureAwait(false);
                    break;
                case (uint)MarkTypeEnums.CivMediumGarage:
                    Task.Run(() => MediumGarage.Instance.ToGarage(_player, _markData)).ConfigureAwait(false);
                    break;
                case (uint)MarkTypeEnums.CivBigGarage:
                    Task.Run(() => BigGarage.Instance.ToGarage(_player, _markData)).ConfigureAwait(false);
                    break;
                case (uint)MarkTypeEnums.CivTruckGarage:
                    // Task.Run(() => TruckGarage.Instance.ToGarage(_player)).ConfigureAwait(false);
                    break;
                case (uint)MarkTypeEnums.CivFuelpump:
                    UiHelper.ShowFuelStation(_player);
                    break;
                case (uint)MarkTypeEnums.CivFuelstation:
                    _player.SendNotification($"~b~[{MarkTypeEnums.CivFuelstation}] ~y~noch nicht implementiert");
                    break;
                case (uint)MarkTypeEnums.CivFuelstationShop:
                    _player.SendNotification($"~b~[{MarkTypeEnums.CivFuelstationShop}] ~y~noch nicht implementiert");
                    break;
                case (uint)MarkTypeEnums.CivAuctionHouse:
                    _player.SendNotification($"~b~[{MarkTypeEnums.CivAuctionHouse}] ~y~noch nicht implementiert");
                    break;
                case (uint)MarkTypeEnums.CivClothingShop:
                    _player.SendNotification($"~b~[{MarkTypeEnums.CivClothingShop}] ~y~noch nicht implementiert");
                    break;
                case (uint)MarkTypeEnums.CivHairCutsShop:
                    _player.SendNotification($"~b~[{MarkTypeEnums.CivHairCutsShop}] ~y~noch nicht implementiert");
                    break;
                case (uint)MarkTypeEnums.CivTattooShop:
                    _player.SendNotification($"~b~[{MarkTypeEnums.CivTattooShop}] ~y~noch nicht implementiert");
                    break;
                case (uint)MarkTypeEnums.CivHatShop:
                    _player.SendNotification($"~b~[{MarkTypeEnums.CivHatShop}] ~y~noch nicht implementiert");
                    break;
                case (uint)MarkTypeEnums.CivSnackShop:
                    UiHelper.ShowItemShop(_player, (uint)MarkTypeEnums.CivSnackShop);
                    break;
                case (uint)MarkTypeEnums.CivRestaurant:
                    UiHelper.ShowItemShop(_player, (uint)MarkTypeEnums.CivRestaurant);
                    break;
                case (uint)MarkTypeEnums.CivToolShop:
                    UiHelper.ShowItemShop(_player, (uint)MarkTypeEnums.CivToolShop);
                    break;
                case (uint)MarkTypeEnums.CivWeaponShop:
                    UiHelper.ShowItemShop(_player, (uint)MarkTypeEnums.CivWeaponShop);
                    break;
                case (uint)MarkTypeEnums.CivBoatTrader:
                case (uint)MarkTypeEnums.CivCompactCoupeTrader:
                case (uint)MarkTypeEnums.CivCycleTrader:
                case (uint)MarkTypeEnums.CivHelicopterTrader:
                case (uint)MarkTypeEnums.CivMotorcycleTrader:
                case (uint)MarkTypeEnums.CivMuscleTrader:
                case (uint)MarkTypeEnums.CivOffRoadTrader:
                case (uint)MarkTypeEnums.CivPlaneTrader:
                case (uint)MarkTypeEnums.CivSuvTrader:
                case (uint)MarkTypeEnums.CivSedanTrader:
                case (uint)MarkTypeEnums.CivSportsCarTrader:
                case (uint)MarkTypeEnums.CivSuperSportTrader:
                case (uint)MarkTypeEnums.CivVanTrader:
                case (uint)MarkTypeEnums.CivTruckTrader:
                    if (_markData.MarkName != 0) UiHelper.ShowVehicleBuy(_player, _markData.MarkName);
                    else _player.SendNotification("~b~Herzlich willkommen! Gehe nah an ein Fahrzeug," +
                                                 " um die Informationen zu sehen.");
                    break;
                case (uint)MarkTypeEnums.AppleFarmspot:
                    new Mining(_player, _markData).StartMining("Sammle Äpfel...").ConfigureAwait(false);
                    break;
                case (uint)MarkTypeEnums.OrangeFarmspot:
                    new Mining(_player, _markData).StartMining("Sammle Orangen...").ConfigureAwait(false);
                    break;
                case (uint)MarkTypeEnums.LemonFarmspot:
                    new Mining(_player, _markData).StartMining("Sammle Zitronen...").ConfigureAwait(false);
                    break;
                case (uint)MarkTypeEnums.AlmondFarmspot:
                    new Mining(_player, _markData).StartMining("Sammle Mandeln...").ConfigureAwait(false);
                    break;
                case (uint)MarkTypeEnums.WalnutFarmspot:
                    new Mining(_player, _markData).StartMining("Sammle Walnüsse...").ConfigureAwait(false);
                    break;
                case (uint)MarkTypeEnums.PistacioFarmspot:
                    new Mining(_player, _markData).StartMining("Sammle Pistazien...").ConfigureAwait(false);
                    break;
                case (uint)MarkTypeEnums.AvocadoFarmspot:
                    new Mining(_player, _markData).StartMining("Sammle Avocados...").ConfigureAwait(false);
                    break;
                case (uint)MarkTypeEnums.GrapeFarmspot:
                    new Mining(_player, _markData).StartMining("Sammle Weintrauben...").ConfigureAwait(false);
                    break;
                case (uint)MarkTypeEnums.WheatFarmspot:
                    new Mining(_player, _markData).StartMining("Sammle Weizen...").ConfigureAwait(false);
                    break;
                case (uint)MarkTypeEnums.RockFarmspot:
                    new Mining(_player, _markData).StartMining("Sammle Steine...").ConfigureAwait(false);
                    break;
                case (uint)MarkTypeEnums.TomatoFarmspot:
                    new Mining(_player, _markData).StartMining("Sammle Tomaten...").ConfigureAwait(false);
                    break;
                case (uint)MarkTypeEnums.MariuhanaFarmspot:
                    new Mining(_player, _markData).StartMining("Sammle Gras...").ConfigureAwait(false);
                    break;
                case (uint)MarkTypeEnums.WoodFarmspot:
                    new Mining(_player, _markData).StartMining("Säge Holz...").ConfigureAwait(false);
                    break;
                case (uint)MarkTypeEnums.CocaineFarmspot:
                    new Mining(_player, _markData).StartMining("Sammle Kokain...").ConfigureAwait(false);
                    break;
                case (uint)MarkTypeEnums.PotatoFarmspot:
                    new Mining(_player, _markData).StartMining("Sammle Kartoffeln...").ConfigureAwait(false);
                    break;
                case (uint)MarkTypeEnums.FrogFarmspot:
                    new Mining(_player, _markData).StartMining("Sammle Frösche...").ConfigureAwait(false);
                    break;
                case (uint)MarkTypeEnums.ShellFarmspot:
                    new Mining(_player, _markData).StartMining("Sammle Muscheln...").ConfigureAwait(false);
                    break;
                case (uint)MarkTypeEnums.FishingFarmspot:
                    new Mining(_player, _markData).StartMining("Angle Fische...").ConfigureAwait(false);
                    break;
                case (uint)MarkTypeEnums.ChemicalFarmspot:
                    new Mining(_player, _markData).StartMining("Klaue Chemikalien...").ConfigureAwait(false);
                    break;
                case (uint)MarkTypeEnums.MushroomFarmspot:
                    new Mining(_player, _markData).StartMining("Sammle Pilze...").ConfigureAwait(false);
                    break;
                case (uint)MarkTypeEnums.MilkFarmspot:
                    new Mining(_player, _markData).StartMining("Melke Kuh...").ConfigureAwait(false);
                    break;
                case (uint)MarkTypeEnums.SandFarmspot:
                    new Mining(_player, _markData).StartMining("Baue Sand ab...").ConfigureAwait(false);
                    break;
                case (uint)MarkTypeEnums.Process_Butcher:
                case (uint)MarkTypeEnums.Process_Carpenter:
                case (uint)MarkTypeEnums.Process_CementFactory:
                case (uint)MarkTypeEnums.Process_Dairy:
                case (uint)MarkTypeEnums.Process_Distillery:
                case (uint)MarkTypeEnums.Process_DrugFactory:
                case (uint)MarkTypeEnums.Process_FlourMill:
                case (uint)MarkTypeEnums.Process_Forge:
                case (uint)MarkTypeEnums.Process_Glassworks:
                case (uint)MarkTypeEnums.Process_JuicePress:
                case (uint)MarkTypeEnums.Process_KetchupFactory:
                case (uint)MarkTypeEnums.Process_MarijuhanaFactory:
                case (uint)MarkTypeEnums.Process_Melt:
                case (uint)MarkTypeEnums.Process_Minter:
                case (uint)MarkTypeEnums.Process_PaperFactory:
                case (uint)MarkTypeEnums.Process_RollingMill:
                case (uint)MarkTypeEnums.Process_Sawmill:
                case (uint)MarkTypeEnums.Process_StoneMill:
                case (uint)MarkTypeEnums.Process_WalnutProcessing:
                    UiHelper.ShowProcessor(_player, _markData.MarkType);
                    break;
                case (uint)MarkTypeEnums.Trader_Exporter:
                case (uint)MarkTypeEnums.Trader_FurnitureShop:
                case (uint)MarkTypeEnums.Trader_BuilderMerchants:
                case (uint)MarkTypeEnums.Trader_Bakery:
                case (uint)MarkTypeEnums.Trader_BigKitchen:
                case (uint)MarkTypeEnums.Trader_Jeweler:
                case (uint)MarkTypeEnums.Trader_Fruiterer:
                case (uint)MarkTypeEnums.Trader_Dealer:
                    UiHelper.ShowTrader(_player, _markData.MarkType);
                    break;
                case (uint)MarkTypeEnums.Dumpster:
                    Task.Run(() => DumpsterManager.Instance.CheckDumpster(_player, _markData)).ConfigureAwait(false);
                    break;
                default:
                    _player.SendNotification("~y~[Interaction] ~r~noch nicht implementiert.");
                    break;
            }
        }

        public void LscAction()
        {
            switch (_markData.MarkName)
            {
                case (uint)MarkTypeEnums.LscStation:
                    _player.SendNotification($"~b~[{MarkTypeEnums.LscStation}] ~y~noch nicht implementiert");
                    break;

                case (uint)MarkTypeEnums.LscUniforms:
                    _player.SendNotification($"~b~[{MarkTypeEnums.LscUniforms}] ~y~noch nicht implementiert");
                    break;

                case (uint)MarkTypeEnums.LscCanteen:
                    _player.SendNotification($"~b~[{MarkTypeEnums.LscCanteen}] ~y~noch nicht implementiert");
                    break;

                case (uint)MarkTypeEnums.LscTools:
                    _player.SendNotification($"~b~[{MarkTypeEnums.LscTools}] ~y~noch nicht implementiert");
                    break;

                default:
                    _player.SendNotification("~y~[Interaction] ~r~noch nicht implementiert.");
                    break;
            }
        }

        public void LsrsAction()
        {
            switch (_markData.MarkName)
            {
                case (uint)MarkTypeEnums.LsrsStation:
                    _player.SendNotification($"~b~[{MarkTypeEnums.LsrsStation}] ~y~noch nicht implementiert");
                    break;

                case (uint)MarkTypeEnums.LsrsUniforms:
                    _player.SendNotification($"~b~[{MarkTypeEnums.LsrsUniforms}] ~y~noch nicht implementiert");
                    break;

                case (uint)MarkTypeEnums.LsrsCanteen:
                    _player.SendNotification($"~b~[{MarkTypeEnums.LsrsCanteen}] ~y~noch nicht implementiert");
                    break;

                case (uint)MarkTypeEnums.LsrsTools:
                    _player.SendNotification($"~b~[{MarkTypeEnums.LsrsTools}] ~y~noch nicht implementiert");
                    break;

                default:
                    _player.SendNotification("~y~[Interaction] ~r~noch nicht implementiert.");
                    break;
            }
        }

        public void LspdAction()
        {
            switch (_markData.MarkName)
            {
                case (uint)MarkTypeEnums.LspdStation:
                    _player.SendNotification($"~b~[{MarkTypeEnums.LspdStation}] ~y~noch nicht implementiert");
                    break;

                case (uint)MarkTypeEnums.LspdUniforms:
                    _player.SendNotification($"~b~[{MarkTypeEnums.LspdUniforms}] ~y~noch nicht implementiert");
                    break;

                case (uint)MarkTypeEnums.LspdCanteen:
                    _player.SendNotification($"~b~[{MarkTypeEnums.LspdCanteen}] ~y~noch nicht implementiert");
                    break;

                case (uint)MarkTypeEnums.LspdTools:
                    _player.SendNotification($"~b~[{MarkTypeEnums.LspdTools}] ~y~noch nicht implementiert");
                    break;

                case (uint)MarkTypeEnums.LspdWeapons:
                    _player.SendNotification($"~b~[{MarkTypeEnums.LspdWeapons}] ~y~noch nicht implementiert");
                    break;

                default:
                    _player.SendNotification("~y~[Interaction] ~r~noch nicht implementiert.");
                    break;
            }
        }

        public void FibAction()
        {
            switch (_markData.MarkName)
            {
                case (uint)MarkTypeEnums.FibStation:
                    _player.SendNotification($"~b~[{MarkTypeEnums.FibStation}] ~y~noch nicht implementiert");
                    break;

                case (uint)MarkTypeEnums.FibUniforms:
                    _player.SendNotification($"~b~[{MarkTypeEnums.FibUniforms}] ~y~noch nicht implementiert");
                    break;

                case (uint)MarkTypeEnums.FibCanteen:
                    _player.SendNotification($"~b~[{MarkTypeEnums.FibCanteen}] ~y~noch nicht implementiert");
                    break;

                case (uint)MarkTypeEnums.FibTools:
                    _player.SendNotification($"~b~[{MarkTypeEnums.FibTools}] ~y~noch nicht implementiert");
                    break;

                case (uint)MarkTypeEnums.FibWeapons:
                    _player.SendNotification($"~b~[{MarkTypeEnums.FibWeapons}] ~y~noch nicht implementiert");
                    break;

                default:
                    _player.SendNotification("~y~[Interaction] ~r~noch nicht implementiert.");
                    break;
            }
        }

        public void LssdAction()
        {
            switch (_markData.MarkName)
            {
                default:
                    _player.SendNotification("~y~[Interaction] ~r~noch nicht implementiert.");
                    break;
            }
        }

        public void DojAction()
        {
            switch (_markData.MarkName)
            {
                default:
                    _player.SendNotification("~y~[Interaction] ~r~noch nicht implementiert.");
                    break;
            }
        }

        public void DcrAction()
        {
            switch (_markData.MarkName)
            {
                default:
                    _player.SendNotification("~y~[Interaction] ~r~noch nicht implementiert.");
                    break;
            }
        }
    }
}