﻿using GTANetworkAPI;
using RolePlayGameData.EntityDataConsts;
using RolePlayGameData.Enums;
using RolePlayGameData.Models;
using RolePlayGameServer.Garages;
using RolePlayGameServer.ServerLists;
using System.Linq;
using System.Threading.Tasks;

namespace RolePlayGameServer.Handlers
{
    public class GarageInteraction
    {
        private readonly MarkDataModel _markData;
        private readonly Client _player;

        public GarageInteraction(Client player)
        {
            _player = player;
            _markData = LocationList.LocationPlayers.FirstOrDefault(x => x.Key == _player.SocialClubName).Value;
        }

        public void Interact()
        {
            if (_player.Vehicle == null || _markData == null || !_player.HasData(PlayerDataConst.IsInGarageFlag)) return;

            switch (_markData.MarkType)
            {
                case (uint)MarkTypeEnums.GarageAreaSmall:
                    Task.Run(() => SmallGarage.Instance.OutOfGarage(_player, _markData)).ConfigureAwait(false);
                    break;
                case (uint)MarkTypeEnums.GarageAreaMedium:
                    Task.Run(() => MediumGarage.Instance.OutOfGarage(_player, _markData)).ConfigureAwait(false);
                    break;
                case (uint)MarkTypeEnums.GarageAreaBig:
                    Task.Run(() => BigGarage.Instance.OutOfGarage(_player, _markData)).ConfigureAwait(false);
                    break;
                default:
                    return;
            }
        }
    }
}
