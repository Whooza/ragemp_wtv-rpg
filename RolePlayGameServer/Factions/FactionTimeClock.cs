﻿using GTANetworkAPI;
using RolePlayGameData.Database.RolePlayModels;
using RolePlayGameData.EntityDataConsts;
using RolePlayGameData.Enums;

namespace RolePlayGameServer.Factions
{
    internal class FactionTimeClock
    {
        private readonly Client _player;
        private readonly PlayerPaycheckModel _playerPaycheck;
        private PlayerAccountModel _playerData;

        public FactionTimeClock(Client player)
        {
            _player = player;
            _playerData = (PlayerAccountModel) player.GetData(PlayerDataConst.PlayerAccount);
            _playerPaycheck = (PlayerPaycheckModel) player.GetData(PlayerDataConst.PlayerPaycheck);
        }

        public void SetActive(uint markType)
        {
            switch (markType)
            {
                case (uint) MarkTypeEnums.LscTimeClock:
                    _playerPaycheck.InLscActive = true;
                    break;
                case (uint) MarkTypeEnums.LsrsTimeClock:
                    _playerPaycheck.InLsrsActive = true;
                    break;
                case (uint) MarkTypeEnums.LspdTimeClock:
                    _playerPaycheck.InLspdActive = true;
                    break;
                case (uint) MarkTypeEnums.FibTimeClock:
                    _playerPaycheck.InFibActive = true;
                    break;
                case (uint) MarkTypeEnums.LssdTimeClock:
                    _playerPaycheck.InLssdActive = true;
                    break;
                case (uint) MarkTypeEnums.DojTimeClock:
                    _playerPaycheck.InDojActive = true;
                    break;
                case (uint) MarkTypeEnums.DcrTimeClock:
                    _playerPaycheck.InDcrActive = true;
                    break;
            }
        }

        public void SetInactive(uint markType)
        {
            switch (markType)
            {
                case (uint) MarkTypeEnums.LscTimeClock:
                    _playerPaycheck.InLscActive = true;
                    break;
                case (uint) MarkTypeEnums.LsrsTimeClock:
                    _playerPaycheck.InLsrsActive = true;
                    break;
                case (uint) MarkTypeEnums.LspdTimeClock:
                    _playerPaycheck.InLspdActive = true;
                    break;
                case (uint) MarkTypeEnums.FibTimeClock:
                    _playerPaycheck.InFibActive = true;
                    break;
                case (uint) MarkTypeEnums.LssdTimeClock:
                    _playerPaycheck.InLssdActive = true;
                    break;
                case (uint) MarkTypeEnums.DojTimeClock:
                    _playerPaycheck.InDojActive = true;
                    break;
                case (uint) MarkTypeEnums.DcrTimeClock:
                    _playerPaycheck.InDcrActive = true;
                    break;
            }
        }

        public void SetInactive()
        {
        }
    }
}