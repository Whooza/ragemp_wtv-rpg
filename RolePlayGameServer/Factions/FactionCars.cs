﻿using System;
using GTANetworkAPI;
using RolePlayGameData.Configs;
using RolePlayGameData.EntityDataConsts;
using RolePlayGameData.Models;
using RolePlayGameServer.Util;

namespace RolePlayGameServer.Factions
{
    internal class FactionCars
    {
        public void Init()
        {
            try
            {
#if DEBUG
                Logging.ServerInfoLog($"Starting: {this}");
#endif
                foreach (var (key, value) in FactionCarCfg.LspdCityPositions)
                    NAPI.Task.Run(() => SpawnFactionCar(key, value));
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"{this} - {e.Message}");
            }
        }

        private static void SpawnFactionCar(string key, FactionVehPosModel facPos)
        {
            try
            {
                var vehicle = NAPI.Vehicle.CreateVehicle((int) facPos.VehicleHash,
                    new Vector3(facPos.PosX, facPos.PosY, facPos.PosZ), facPos.Rotation,
                    131, 12, facPos.VehicleNumber, 255, false, false);

                vehicle.SetData(VehicleDataConst.FactionVehicleFlag, true);

                vehicle.SetMod(11, 3);
                vehicle.SetMod(12, 2);
                vehicle.SetMod(13, 2);
                vehicle.SetMod(22, 0);
                vehicle.SetMod(0, 0);
                vehicle.SetMod(14, 1);
                vehicle.SetMod(23, 0);
                vehicle.SetMod(18, 0);
                /*
                var colShape = NAPI.ColShape.CreateCylinderColShape(new Vector3(facPos.PosX, facPos.PosY, facPos.PosZ),
                    1.75f, 3f, 0);

                colShape.SetData("CopCity", key);
                */
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog(e.Message);
            }
        }
    }
}