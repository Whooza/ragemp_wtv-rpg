﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GTANetworkAPI;
using RolePlayGameData.Database.RolePlayModels;
using RolePlayGameData.EntityDataConsts;
using RolePlayGameData.Models;

namespace RolePlayGameServer.Voice
{
    internal class PhoneController
    {
        private static readonly List<PhoneCallModel> PhoneCallList = new List<PhoneCallModel>();
        private readonly Client _caller;
        private readonly string _callerName;

        public PhoneController(Client player)
        {
            _caller = player;
            _callerName = GetName(_caller);
        }

        public void StartVoiceCall(Client target)
        {
            if (target == _caller)
            {
                _caller.SendNotification("~w~[Handy]: ~r~Du kannst Dich nicht selbst anrufen!");
                return;
            }

            if (HasAlreadyStartedCall(_callerName)) return;

            var targetName = GetName(target);

            if (IsInActiveCall(targetName)) return;

            var newCall = new PhoneCallModel
            {
                Caller = _caller,
                CallerName = _callerName,
                Targets = new Dictionary<Client, string> {{target, targetName}},
                StartTime = DateTime.Now
            };

            _caller.TriggerEvent("startVoiceCall", true, targetName);
            target.TriggerEvent("startVoiceCall", true, _callerName);
        }

        public void AddMemberToConference(Client target)
        {
            var targetName = GetName(target);

            if (IsInActiveCall(targetName)) return;

            var callData = PhoneCallList.Single(x => x.Caller == _caller);

            if (callData.Targets.Count() >= 4)
                _caller.SendNotification("~w~[Handy]: ~r~Die maximale Anzahl von Teilnehmern ist erreicht!");
        }

        public void RemoveMemberFromConference(Client target)
        {
            var targetName = GetName(target);

            if (IsInActiveCall(targetName)) return;
        }

        public void StopVoiceCall()
        {
            var callData = PhoneCallList.Single(x => x.Caller == _caller);
            var totalMinutes = (DateTime.Now - callData.StartTime).TotalMinutes + 1;

            if (callData.Targets.Any())
            {
                // TODO: subract conference from selected bankAccount
            }

            PhoneCallList.Remove(callData);

            _caller.TriggerEvent("stopVoiceCall", false);

            foreach (var member in callData.Targets) member.Key.TriggerEvent("stopVoiceCall", false);
        }

        private string GetName(Client player)
        {
            var particular = (PlayerParticularModel) player.GetData(PlayerDataConst.PlayerParticular);

            return $"{particular.FirstName} {particular.LastName}";
        }

        private bool HasAlreadyStartedCall(string name)
        {
            foreach (var call in PhoneCallList)
                if (call.Caller == _caller)
                {
                    _caller.SendNotification("~w~[Handy]: ~r~Du hast bereits ein Telefonat gestartet.");
                    return true;
                }

            return false;
        }

        private bool IsInActiveCall(string name)
        {
            foreach (var call in PhoneCallList)
                if (call.Targets.Values.Contains(name))
                {
                    _caller.SendNotification("~w~[Handy]: ~y~Der Teilnehmer befindet sich in einem Telefonat.");
                    return true;
                }

            return false;
        }

        private bool IsPhoneActive(string name)
        {
            return true; // Todo: check phone is active in phoneModel
        }
    }
}