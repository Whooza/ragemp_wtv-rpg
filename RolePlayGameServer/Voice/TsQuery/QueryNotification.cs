using System.Diagnostics;

namespace TeamSpeak3QueryApi.Net
{
    internal class QueryNotification
    {
        public QueryNotification(string name, NotificationData data)
        {
            Debug.Assert(name != null);
            Name = name;
            Data = data;
        }

        public string Name { get; set; }
        public NotificationData Data { get; set; }
    }
}