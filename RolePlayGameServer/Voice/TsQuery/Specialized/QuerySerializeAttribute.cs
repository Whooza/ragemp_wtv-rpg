using System;

namespace TeamSpeak3QueryApi.Net.Specialized
{
    [AttributeUsage(AttributeTargets.Field)]
    internal class QuerySerializeAttribute : Attribute
    {
        public QuerySerializeAttribute(string name)
        {
            Name = string.IsNullOrWhiteSpace(name) ? null : name;
        }

        public string Name { get; }
    }
}