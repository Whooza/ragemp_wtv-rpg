namespace TeamSpeak3QueryApi.Net.Specialized.Responses
{
    public class GetServerGroupListInfo : Response
    {
        [QuerySerialize("iconid")] public int IconId;

        [QuerySerialize("sgid")] public int Id;

        [QuerySerialize("name")] public string Name;

        [QuerySerialize("namemode")] public int NamingMode;

        [QuerySerialize("n_member_addp")] public int NeededMemberAddPower;

        [QuerySerialize("n_member_remove_p")] public int NeededMemberRemovePower;

        [QuerySerialize("n_modifyp")] public int NeededModifyPower;

        [QuerySerialize("savedb")] public int SaveDb;

        [QuerySerialize("type")] public ServerGroupType ServerGroupType;

        [QuerySerialize("sortid")] public int SortId;
    }
}