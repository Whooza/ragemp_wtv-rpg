namespace TeamSpeak3QueryApi.Net.Specialized.Responses
{
    public class GetClientInfo : Response
    {
        [QuerySerialize("cid")] public int ChannelId;

        [QuerySerialize("client_database_id")] public int DatabaseId;

        [QuerySerialize("clid")] public int Id;

        [QuerySerialize("client_nickname")] public string NickName;

        [QuerySerialize("client_flag_talking")]
        public bool Talk;

        [QuerySerialize("client_type")] public ClientType Type;
    }
}