namespace TeamSpeak3QueryApi.Net.Specialized.Notifications
{
    public abstract class InvokerInformation : Notification
    {
        [QuerySerialize("invokerid")] public int InvokerId;

        [QuerySerialize("invokername")] public string InvokerName;

        [QuerySerialize("invokeruid")] public string InvokerUid;

        [QuerySerialize("reasonid")] public ReasonId Reason;
    }
}