namespace TeamSpeak3QueryApi.Net.Specialized.Notifications
{
    public class TextMessage : InvokerInformation
    {
        [QuerySerialize("msg")] public string Message;

        [QuerySerialize("target")]
        public int TargetClientId; // (clid des Empfängers; Parameter nur bei textprivate vorhanden)

        [QuerySerialize("targetmode")] public MessageTarget TargetMode;
    }
}