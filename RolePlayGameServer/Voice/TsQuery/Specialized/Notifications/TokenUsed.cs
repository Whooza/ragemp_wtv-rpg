namespace TeamSpeak3QueryApi.Net.Specialized.Notifications
{
    public class TokenUsed : InvokerInformation
    {
        [QuerySerialize("cldbid")] public int ClientDatabaseId;

        [QuerySerialize("clid")] public int ClientId;

        [QuerySerialize("cluid")] public string ClientUid;

        [QuerySerialize("token1")] public string Token1;

        [QuerySerialize("token2")] public string Token2;

        [QuerySerialize("tokencustomset")] public string TokenCustomSet;

        [QuerySerialize("token")] public string UsedToken;
    }
}