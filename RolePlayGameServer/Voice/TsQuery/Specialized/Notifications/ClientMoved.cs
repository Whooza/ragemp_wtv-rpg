namespace TeamSpeak3QueryApi.Net.Specialized.Notifications
{
    public class ClientMoved : InvokerInformation
    {
        [QuerySerialize("clid")] public int[] ClientIds;

        [QuerySerialize("ctid")] public int TargetChannel;
    }
}