﻿using System;
using System.Linq;
using System.Threading.Tasks;
using GTANetworkAPI;
using RolePlayGameData.Configs;
using TeamSpeak3QueryApi.Net;
using TeamSpeak3QueryApi.Net.Specialized;

namespace RolePlayGameServer.Voice
{
    internal class VoiceController : Script
    {
        private static string TeamspeakQueryAddress => VoiceCfg.TeamspeakQueryAddress;
        private static short TeamspeakQueryPort => VoiceCfg.TeamspeakQueryPort;
        private static string TeamspeakLogin => VoiceCfg.TeamspeakLogin;
        private static string TeamspeakPassword => VoiceCfg.TeamspeakPassword;
        private static string TeamspeakChannel => VoiceCfg.TeamspeakChannel;

        [ServerEvent(Event.ResourceStart)]
        public void OnResourceStart()
        {
            Task.Run(async () =>
            {
                try
                {
                    await CheckSpeakingClients().ConfigureAwait(false);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }

                Console.WriteLine("Teamspeak Wrapper Initialised!");
            });
        }

        public void Connect(Client client, string characterName)
        {
            client.SetSharedData("VOICE_RANGE", "Normal");
            client.SetSharedData("TsName", characterName);
            client.TriggerEvent("connectTs",
                "http://localhost:15555/custom_players2/InGameVoice/reinDa", client.SocialClubName);
        }

        private async Task CheckSpeakingClients()
        {
            var rc = new TeamSpeakClient(TeamspeakQueryAddress, TeamspeakQueryPort); // Create rich client instance

            try
            {
                await rc.Connect().ConfigureAwait(false); // connect to the server
                await rc.Login(TeamspeakLogin, TeamspeakPassword)
                    .ConfigureAwait(false); // login to do some stuff that requires permission
                await rc.UseServer(1).ConfigureAwait(false); // Use the server with id '1'
                var me = await rc.WhoAmI().ConfigureAwait(false); // Get information about yourself!
            }
            catch (QueryException ex)
            {
                Console.WriteLine(ex.ToString());
            }

            var channel = (await rc.FindChannel(TeamspeakChannel)
                .ConfigureAwait(false)).FirstOrDefault();

            while (rc.Client.IsConnected)
            {
                var clients = await rc.GetClients(GetClientOptions.Voice).ConfigureAwait(false);
                var clientChannel = clients.ToList().FindAll(c => c.ChannelId == channel.Id);

                var players = NAPI.Pools.GetAllPlayers().FindAll(p => p.Exists && p.HasSharedData("TsName"));

                for (var i = 0; i < players.Count; i++)
                {
                    if (players[i] == null)
                        continue;

                    var name = players[i].GetSharedData("TsName");
                    var tsPlayer = clientChannel.Find(p => p.NickName == name);
                    var player = players[i];

                    if (!player.Exists) continue;

                    if (tsPlayer != null)
                    {
                        if (tsPlayer.Talk && !player.HasData("IS_SPEAKING"))
                        {
                            players.FindAll(p => p.Exists && p.Position.DistanceTo2D(player.Position) < 5f)
                                .ForEach(client => client.TriggerEvent("tsLipSync", player.Handle.Value, true));

                            player.SetData("IS_SPEAKING", true);
                        }
                        else if (!tsPlayer.Talk && player.HasData("IS_SPEAKING"))
                        {
                            players.FindAll(p => p.Exists && p.Position.DistanceTo2D(player.Position) < 5f)
                                .ForEach(client => client.TriggerEvent("tsLipSync", player.Handle.Value, false));

                            player.ResetData("IS_SPEAKING");
                        }
                    }

                    await Task.Delay(10).ConfigureAwait(false);
                }

                await Task.Delay(50).ConfigureAwait(false);
            }
        }
    }
}