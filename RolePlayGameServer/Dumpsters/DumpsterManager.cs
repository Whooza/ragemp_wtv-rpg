﻿using GTANetworkAPI;
using Newtonsoft.Json;
using RolePlayGameData.Configs;
using RolePlayGameData.Models;
using RolePlayGameServer.Database.Controllers;
using RolePlayGameServer.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RolePlayGameServer.Dumpsters
{
    internal sealed class DumpsterManager
    {
        public static SortedDictionary<uint, DateTime> DumpsterTimer { get; private set; }
            = new SortedDictionary<uint, DateTime>();
        public static SortedDictionary<uint, string> ActiveDumpsters { get; private set; }
            = new SortedDictionary<uint, string>();

        private static readonly Lazy<DumpsterManager> _instance = new Lazy<DumpsterManager>(() => new DumpsterManager());

        private DumpsterManager() { }

        public static DumpsterManager Instance => _instance.Value;

        public void CheckDumpster(Client player, MarkDataModel markData)
        {
            if (ActiveDumpsters.ContainsKey(markData.MarkId))
            {
                if (ActiveDumpsters[markData.MarkId] == player.SocialClubName)
                {
                    OpenDumpster(player, markData);
                    return;
                }
                else if (DumpsterTimer.ContainsKey(markData.MarkId) &&
                    (DumpsterTimer[markData.MarkId] - DateTime.Now).TotalMinutes > 2)
                {
                    OpenDumpster(player, markData);
                    return;
                }
                else
                {
                    Notifications.SendInteractionError(player, "Dieser Container wird bereits von jemanden benutzt!");
                    return;
                }
            }
            else OpenDumpster(player, markData);
        }

        private void OpenDumpster(Client player, MarkDataModel markData)
        {
            CloseDumpster(markData);

            ActiveDumpsters.Add(markData.MarkId, player.SocialClubName);
            DumpsterTimer.Add(markData.MarkId, DateTime.Now);

            UiHelper.ShowDumpster(player);
        }

        public async Task<bool> AddItem(Client player, MarkDataModel markData, uint itemName, uint amount)
        {
            var dumpster = await MapObjectDbController.GetDumpster((int)markData.MarkId).ConfigureAwait(false);
            var inventory = JsonConvert.DeserializeObject<List<DumpsterInvItemModel>>(dumpster.ItemList);

            if (!CheckInventoryWeight(inventory, itemName, amount))
            {
                Notifications.SendInteractionError(player, "Das passt nicht mehr in den Container!");
                return false;
            }

            var existingItem = inventory.FirstOrDefault(x => x.Item == itemName);

            if (existingItem == default) inventory.Add(new DumpsterInvItemModel
            {
                Item = itemName,
                Amount = amount,
                StoreTime = DateTime.Now
            });
            else existingItem.Amount += amount;

            dumpster.ItemList = JsonConvert.SerializeObject(inventory);

            await MapObjectDbController.UpdateDumpster(dumpster).ConfigureAwait(false);

            return true;
        }

        public async Task<bool> RemoveItem(Client player, MarkDataModel markData, uint itemName, uint amount)
        {
            var dumpster = await MapObjectDbController.GetDumpster((int)markData.MarkId).ConfigureAwait(false);
            var inventory = JsonConvert.DeserializeObject<List<DumpsterInvItemModel>>(dumpster.ItemList);

            var existingItem = inventory.FirstOrDefault(x => x.Item == itemName);

            if (existingItem == default)
            {
                Notifications.SendInteractionError(player, "Gegenstand nicht gefunden!");
                return false;
            }

            if (existingItem.Amount - amount > 0) existingItem.Amount -= amount;
            else inventory.Remove(existingItem);

            dumpster.ItemList = JsonConvert.SerializeObject(inventory);

            await MapObjectDbController.UpdateDumpster(dumpster).ConfigureAwait(false);

            return true;
        }

        private bool CheckInventoryWeight(List<DumpsterInvItemModel> inventory, uint itemName, uint amount)
        {
            var itemCfg = ItemCfg.ItemConfig.First(x => (uint)x.ItemName == itemName);
            var weight = 0f;

            weight += itemCfg.Weight * amount;

            foreach (var item in inventory)
            {
                var settings = ItemCfg.ItemConfig.First(x => (uint)x.ItemName == item.Item);

                weight += settings.Weight * item.Amount;
            }

            if (weight <= 250f) return true;

            return false;
        }

        public void CloseDumpster(MarkDataModel markData)
        {
            if (ActiveDumpsters.ContainsKey(markData.MarkId)) ActiveDumpsters.Remove(markData.MarkId);
            if (DumpsterTimer.ContainsKey(markData.MarkId)) DumpsterTimer.Remove(markData.MarkId);
        }
    }
}