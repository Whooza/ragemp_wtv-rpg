﻿using GTANetworkAPI;
using RolePlayGameData.EntityDataConsts;
using RolePlayGameData.Texts;
using RolePlayGameServer.Database.Controllers;
using RolePlayGameServer.Util;
using System;
using System.Threading.Tasks;

namespace RolePlayGameServer.Dumpsters
{
    public class DumpsterCreator
    {
        private readonly Client _player;

        public DumpsterCreator(Client player) => _player = player;

        public async void CreateDumpster()
        {
            try
            {
                var pos = _player.Position;

                await MapObjectDbController.SetDumpster(pos.X, pos.Y, pos.Z, _player.Dimension)
                    .ConfigureAwait(false);

                ReloadDumpsters();

                Notifications.SendInteractionInfo(_player, PlayerNotificationTexts.DumpsterSuccessfulCreated);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async void DeleteDumpster(int id)
        {
            await MapObjectDbController.DeleteDumpsterById(id);

            ReloadDumpsters();

            Notifications.SendInteractionInfo(_player, PlayerNotificationTexts.DumpsterSuccessfulDeleted);
        }

        public async void ReloadDumpsters()
        {
            try
            {
                foreach (var item in NAPI.Pools.GetAllColShapes())
                {
                    NAPI.Task.Run(() =>
                    {
                        if (item.HasData(MarkDataConst.DumpsterFlag)) item.Delete();
                    });
                }

                foreach (var item in NAPI.Pools.GetAllTextLabels())
                {
                    NAPI.Task.Run(() =>
                    {
                        if (item.HasData(MarkDataConst.DumpsterFlag)) item.Delete();
                    });
                }

                await Task.Delay(1000);

                new DumpsterSpawner().LoadDumpsters();

                Notifications.SendInteractionInfo(_player, PlayerNotificationTexts.DumpstersSuccessfulReloaded);
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"{this} - {e.Message}");
            }
        }
    }
}
