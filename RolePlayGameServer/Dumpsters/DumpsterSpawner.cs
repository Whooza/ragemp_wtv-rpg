﻿using GTANetworkAPI;
using RolePlayGameData.Configs;
using RolePlayGameData.Database.MapMarkModels;
using RolePlayGameData.EntityDataConsts;
using RolePlayGameData.Enums;
using RolePlayGameData.Models;
using RolePlayGameServer.Database.Controllers;
using RolePlayGameServer.Util;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RolePlayGameServer.Dumpsters
{
    public class DumpsterSpawner
    {
        private List<DumpsterModel> DumpsterPosList => GetDbPositions().Result;

        public void LoadDumpsters()
        {
            try
            {
                foreach (var dumpster in DumpsterPosList)
                {
                    NAPI.Task.Run(() =>
                    {
                        var colShape = NAPI.ColShape.CreateCylinderColShape(new Vector3(dumpster.PositionX, dumpster.PositionY, dumpster.PositionZ),
                            1.1f, 4f, dumpster.Dimension);

                        var markData = new MarkDataModel
                        {
                            MarkId = (uint)dumpster.Id,
                            MarkFaction = (uint)FactionEnums.Civ,
                            MarkType = (uint)MarkTypeEnums.Dumpster,
                            MarkName = 0,
                            PosX = dumpster.PositionX,
                            PosY = dumpster.PositionY,
                            PosZ = dumpster.PositionZ
                        };

                        colShape.SetData(MarkDataConst.MarkData, markData);
                        colShape.SetData(MarkDataConst.DumpsterFlag, true);

                        if (ServerCfg.ShowMarkIds)
                        {
                            var idPos = new Vector3(dumpster.PositionX, dumpster.PositionY, dumpster.PositionZ + 3);

                            var idLabel = NAPI.TextLabel.CreateTextLabel("[ID: " + dumpster.Id + "] ",
                                idPos, 50f, 50f, 0,
                                new Color(0, 0, 155), false, dumpster.Dimension);

                            idLabel.SetData(MarkDataConst.MarkData, markData);
                            idLabel.SetData(MarkDataConst.DumpsterFlag, true);
                        }
                    });
                }
            }
            catch (Exception e)
            {
                Logging.ServerErrorLog($"LoadDumpsters - {e.Message}");
            }
        }

        private async Task<List<DumpsterModel>> GetDbPositions() =>
            await MapObjectDbController.GetDumpsterList().ConfigureAwait(false);
    }
}
