﻿namespace RolePlayGameData.Models
{
    public class BanInfoModel
    {
        public string BanReason { get; set; }
        public string BanTime { get; set; }
    }
}