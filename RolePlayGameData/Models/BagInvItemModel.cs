﻿using System.ComponentModel.DataAnnotations;

namespace RolePlayGameData.Models
{
    public class BagInvItemModel
    {
        [Key] public int Id { get; set; }
        [Required] public uint Item { get; set; }
        [Required] public uint Amount { get; set; }
    }
}
