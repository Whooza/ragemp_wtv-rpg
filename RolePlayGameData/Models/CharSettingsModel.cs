﻿namespace RolePlayGameData.Models
{
    public class CharSettingsModel
    {
        public CharSettingsModel()
        {
            FirstHeadShape = 0;
            SecondHeadShape = 0;
            FirstSkinTone = 0;
            SecondSkinTone = 0;
            HeadMix = 0.5f;
            SkinMix = 0.5f;
            HairModel = 0;
            FirstHairColor = 0;
            SecondHairColor = 0;
            BeardModel = 255;
            BeardColor = 0;
            ChestModel = 255;
            ChestColor = 0;
            BlemishesModel = 255;
            AgingModel = 255;
            ComplexionModel = 255;
            SunDamageModel = 255;
            FrecklesModel = 255;
            EyesColor = 0;
            EyebrowsModel = 255;
            EyebrowsColor = 0;
            MakeupModel = 255;
            BlushModel = 255;
            BlushColor = 0;
            LipstickModel = 255;
            LipstickColor = 0;
            NoseWidth = 0.0f;
            NoseHeight = 0.0f;
            NoseLength = 0.0f;
            NoseBridge = 0.0f;
            NoseTip = 0.0f;
            NoseShift = 0.0f;
            BrowHeight = 0.0f;
            BrowWidth = 0.0f;
            CheekboneHeight = 0.0f;
            CheekboneWidth = 0.0f;
            CheeksWidth = 0.0f;
            Eyes = 0.0f;
            Lips = 0.0f;
            JawWidth = 0.0f;
            JawHeight = 0.0f;
            ChinLength = 0.0f;
            ChinPosition = 0.0f;
            ChinWidth = 0.0f;
            ChinShape = 0.0f;
            NeckWidth = 0.0f;
        }

        public int FirstHeadShape { get; set; }
        public int SecondHeadShape { get; set; }
        public int FirstSkinTone { get; set; }
        public int SecondSkinTone { get; set; }
        public float HeadMix { get; set; }
        public float SkinMix { get; set; }
        public int HairModel { get; set; }
        public int FirstHairColor { get; set; }
        public int SecondHairColor { get; set; }
        public int BeardModel { get; set; }
        public int BeardColor { get; set; }
        public int ChestModel { get; set; }
        public int ChestColor { get; set; }
        public int BlemishesModel { get; set; }
        public int AgingModel { get; set; }
        public int ComplexionModel { get; set; }
        public int SunDamageModel { get; set; }
        public int FrecklesModel { get; set; }
        public float NoseWidth { get; set; }
        public float NoseHeight { get; set; }
        public float NoseLength { get; set; }
        public float NoseBridge { get; set; }
        public float NoseTip { get; set; }
        public float NoseShift { get; set; }
        public float BrowHeight { get; set; }
        public float BrowWidth { get; set; }
        public float CheekboneHeight { get; set; }
        public float CheekboneWidth { get; set; }
        public float CheeksWidth { get; set; }
        public float Eyes { get; set; }
        public float Lips { get; set; }
        public float JawWidth { get; set; }
        public float JawHeight { get; set; }
        public float ChinLength { get; set; }
        public float ChinPosition { get; set; }
        public float ChinWidth { get; set; }
        public float ChinShape { get; set; }
        public float NeckWidth { get; set; }
        public int EyesColor { get; set; }
        public int EyebrowsModel { get; set; }
        public int EyebrowsColor { get; set; }
        public int MakeupModel { get; set; }
        public int BlushModel { get; set; }
        public int BlushColor { get; set; }
        public int LipstickModel { get; set; }
        public int LipstickColor { get; set; }
    }
}