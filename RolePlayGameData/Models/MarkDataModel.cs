﻿namespace RolePlayGameData.Models
{
    public class MarkDataModel
    {
        public uint MarkId { get; set; }
        public uint MarkFaction { get; set; }
        public uint MarkType { get; set; }
        public uint MarkName { get; set; }
        public float PosX { get; set; }
        public float PosY { get; set; }
        public float PosZ { get; set; }
    }
}