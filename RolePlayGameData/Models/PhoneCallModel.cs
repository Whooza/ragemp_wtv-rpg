﻿using System;
using System.Collections.Generic;
using GTANetworkAPI;

namespace RolePlayGameData.Models
{
    public class PhoneCallModel
    {
        public Client Caller { get; set; }
        public string CallerName { get; set; }
        public Dictionary<Client, string> Targets { get; set; }
        public DateTime StartTime { get; set; }
    }
}