﻿using RolePlayGameData.Enums;

namespace RolePlayGameData.Models
{
    public class WeaponModel
    {
        public WeaponHashes Hash { get; set; }
        public WeaponTypeEnums WeaponType { get; set; }
        public FactionEnums Faction { get; set; }
        public string Description { get; set; }
        public string DisplayedName { get; set; }
        public float DefaultPrice { get; set; }
        public float CurrentPrice { get; set; }
        public float MinPrice { get; set; }
        public float MaxPrice { get; set; }
        public float Weight { get; set; }
    }
}