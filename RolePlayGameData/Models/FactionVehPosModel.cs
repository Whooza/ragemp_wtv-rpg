﻿using RolePlayGameData.Enums;

namespace RolePlayGameData.Models
{
    public class FactionVehPosModel
    {
        public VehicleHashEnums VehicleHash { get; set; }
        public string VehicleNumber { get; set; }
        public float PosX { get; set; }
        public float PosY { get; set; }
        public float PosZ { get; set; }
        public float Rotation { get; set; }
    }
}