﻿using System;
using System.Collections.Generic;
using GTANetworkAPI;

namespace RolePlayGameData.Models
{
    public class MiningModel
    {
        public Client Player { get; set; }
        public List<uint> FarmedSpots { get; set; }
        public DateTime LastFarmAction { get; set; }
    }
}