﻿using RolePlayGameData.Enums;

namespace RolePlayGameData.Models
{
    public class NewVehicleModel
    {
        public string VehicleName { get; set; }
        public VehicleHashEnums VehicleHash { get; set; }
        public VehicleClassEnums VehicleClass { get; set; }
        public VehicleFuelEnums FuelType { get; set; }
        public float InventoryCapacity { get; set; }
        public float FuelConsumption { get; set; }
        public float TankCapacity { get; set; }
        public float Price { get; set; }
    }
}