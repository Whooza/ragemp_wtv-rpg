﻿namespace RolePlayGameData.Models
{
    public class InteriorPropModel
    {
        public int PropId { get; set; }
        public string PropString { get; set; }
        public float PropPrice { get; set; }
    }
}