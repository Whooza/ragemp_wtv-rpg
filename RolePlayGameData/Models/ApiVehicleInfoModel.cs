﻿namespace RolePlayGameData.Models
{
    public class ApiVehicleInfoModel
    {
        public string DisplayedName { get; set; }
        public string NumberPlate { get; set; }
        public bool EngineStatus { get; set; }
        public bool Locked { get; set; }
        public uint Model { get; set; }
        public int EntityValue { get; set; }
    }
}