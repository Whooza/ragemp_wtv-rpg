﻿using RolePlayGameData.Enums;

namespace RolePlayGameData.Models
{
    public class MarkSettingsModel
    {
        public FactionEnums MarkFaction { get; set; }
        public MarkTypeEnums MarkType { get; set; }
        public MarkNameEnums MarkName { get; set; }
        public string MarkText { get; set; }
        public bool HasMapMark { get; set; }
        public bool HasWorldMark { get; set; }
        public bool HasTextLabel { get; set; }
        public uint BlipSprite { get; set; }
        public float BlipScale { get; set; }
        public byte BlipColor { get; set; }
        public byte BlipAlpha { get; set; }
        public float BlipDrawDistance { get; set; }
        public bool BlipShortRange { get; set; }
        public short BlipRotation { get; set; }
        public int MarkerType { get; set; }
        public float MarkerScale { get; set; }
        public int MarkerColorRed { get; set; }
        public int MarkerColorGreen { get; set; }
        public int MarkerColorBlue { get; set; }
        public int MarkerColorAlpha { get; set; }
        public bool MarkerBlobUpAndDown { get; set; }
        public float ColShapeX { get; set; }
        public float ColShapeY { get; set; }
        public float ColShapeRange { get; set; }
        public float ColShapeHeight { get; set; }
        public float TextLabelRange { get; set; }
        public float TextLabelSize { get; set; }
        public int TextLabelFont { get; set; }
        public int TextLabelColorRed { get; set; }
        public int TextLabelColorGreen { get; set; }
        public int TextLabelColorBlue { get; set; }
        public int TextLabelColorAlpha { get; set; }
        public bool TextLabelEntitySeeThrough { get; set; }
    }
}