﻿namespace RolePlayGameData.Models
{
    public class InteriorPropInfo
    {
        public int PropId { get; set; }
        public string PropName { get; set; }
        public float PropPrice { get; set; }
    }
}