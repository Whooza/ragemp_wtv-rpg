﻿using RolePlayGameData.Database.RolePlayModels;

namespace RolePlayGameData.Models
{
    public class FuelStationModel
    {
        public VehicleModel Vehicle { get; set; }
        public VehicleFuelModel VehFuel { get; set; }
        public PlayerMoneyModel PlayerMoney { get; set; }
    }
}