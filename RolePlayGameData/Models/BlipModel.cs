﻿using GTANetworkAPI;

namespace RolePlayGameData.Models
{
    public class BlipModel
    {
        public uint Sprite { get; set; }
        public Vector3 Position { get; set; }
        public string Name { get; set; }
        public float Scale { get; set; }
        public int Color { get; set; }
        public int Alpha { get; set; }
        public float DrawDistance { get; set; }
        public bool ShortRange { get; set; }
        public int Rotation { get; set; }
        public uint Dimension { get; set; }
    }
}