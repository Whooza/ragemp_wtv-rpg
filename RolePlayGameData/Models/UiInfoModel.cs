﻿using System;

namespace RolePlayGameData.Models
{
    public class UiInfoModel
    {
        public string Head { get; set; }
        public string Body { get; set; }
        public DateTime CreationDate { get; set; }
    }
}