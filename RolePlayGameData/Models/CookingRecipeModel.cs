﻿using System.Collections.Generic;
using RolePlayGameData.Enums;

namespace RolePlayGameData.Models
{
    public class CookingRecipeModel
    {
        public CookingRecipeModel(CookingCategoryEnums category, CookingLevelEnums level, int recipeId)
        {
            Category = category;
            Level = level;
            RecipeId = recipeId;
        }

        public CookingCategoryEnums Category { get; set; }
        public CookingLevelEnums Level { get; set; }
        public string RecipeName { get; set; }
        public int RecipeId { get; set; }
        public int ProcessTime { get; set; }
        public Dictionary<ItemNameEnums, int> MatsList { get; set; }
        public ItemNameEnums CookedItemType { get; set; }
        public int CookedItemAmount { get; set; }
    }
}