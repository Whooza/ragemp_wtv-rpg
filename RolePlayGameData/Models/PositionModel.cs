﻿using RolePlayGameData.Enums;

namespace RolePlayGameData.Models
{
    public class PositionModel
    {
        public FactionEnums MarkFaction { get; set; }
        public MarkTypeEnums MarkType { get; set; }
        public MarkNameEnums MarkName { get; set; }
        public VehicleClassEnums VehicleClass { get; set; }
        public float PosX { get; set; }
        public float PosY { get; set; }
        public float PosZ { get; set; }
        public float Rotation { get; set; }
    }
}