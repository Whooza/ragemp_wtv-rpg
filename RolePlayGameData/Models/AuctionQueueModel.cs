﻿using GTANetworkAPI;
using RolePlayGameData.Enums;

namespace RolePlayGameData.Models
{
    public class AuctionQueueModel
    {
        public Client Player { get; set; }
        public int AuctionId { get; set; }
        public AuctionActionEnums Action { get; set; }
        public uint ItemName { get; set; }
        public uint Amount { get; set; }
        public float StartPrice { get; set; }
        public float Offer { get; set; }
        public bool IsInstantBuy { get; set; }
        public float InstantBuyPrice { get; set; }
        public uint Duration { get; set; }
    }
}
