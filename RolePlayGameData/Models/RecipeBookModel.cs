﻿using System.Collections.Generic;

namespace RolePlayGameData.Models
{
    public class RecipeBookModel
    {
        public List<int> Butcher { get; set; }
        public float ButcherPoints { get; set; }
        public List<int> Carpenter { get; set; }
        public float CarpenterPoints { get; set; }
        public List<int> CementFactory { get; set; }
        public float CementFactoryPoints { get; set; }
        public List<int> Dairy { get; set; }
        public float DairyPoints { get; set; }
        public List<int> Distillery { get; set; }
        public float DistilleryPoints { get; set; }
        public List<int> DrugFactory { get; set; }
        public float DrugFactoryPoints { get; set; }
        public List<int> FlourMill { get; set; }
        public float FlourMillPoints { get; set; }
        public List<int> Forge { get; set; }
        public float ForgePoints { get; set; }
        public List<int> Glassworks { get; set; }
        public float GlassworksPoints { get; set; }
        public List<int> JuicePress { get; set; }
        public float JuicePressPoints { get; set; }
        public List<int> KetchupFactory { get; set; }
        public float KetchupFactoryPoints { get; set; }
        public List<int> MarijuhanaFactory { get; set; }
        public float MarijuhanaFactoryPoints { get; set; }
        public List<int> Melt { get; set; }
        public float MeltPoints { get; set; }
        public List<int> Minter { get; set; }
        public float MinterPoints { get; set; }
        public List<int> PaperFactory { get; set; }
        public float PaperFactoryPoints { get; set; }
        public List<int> RollingMill { get; set; }
        public float RollingMillPoints { get; set; }
        public List<int> Sawmill { get; set; }
        public float SawmillPoints { get; set; }
        public List<int> StoneMill { get; set; }
        public float StoneMillPoints { get; set; }
        public List<int> WalnutProcessing { get; set; }
        public float WalnutProcessingPoints { get; set; }
    }
}