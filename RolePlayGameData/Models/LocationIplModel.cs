﻿using GTANetworkAPI;

namespace RolePlayGameData.Models
{
    public class LocationIplModel
    {
        public string IplName { get; set; }
        public bool LoadIpl { get; set; }
        public float IplPrice { get; set; }
        public Vector3 LocationPosition { get; set; }
    }
}