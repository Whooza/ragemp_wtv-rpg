﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RolePlayGameData.Models
{
    public class DumpsterInvItemModel
    {
        [Key] public int Id { get; set; }
        [Required] public uint Item { get; set; }
        [Required] public uint Amount { get; set; }
        [Required] public DateTime StoreTime { get; set; }
    }
}
