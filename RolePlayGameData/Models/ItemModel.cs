﻿using RolePlayGameData.Enums;

namespace RolePlayGameData.Models
{
    public class ItemModel
    {
        public FactionEnums ItemFaction { get; set; }
        public ItemTypeEnums ItemType { get; set; }
        public ItemNameEnums ItemName { get; set; }
        public string Description { get; set; }
        public string DisplayedName { get; set; }
        public float DefaultPrice { get; set; }
        public float CurrentPrice { get; set; }
        public float MinPrice { get; set; }
        public float MaxPrice { get; set; }
        public float Weight { get; set; }
        public float Value { get; set; }
    }
}