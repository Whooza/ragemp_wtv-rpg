﻿using System.Collections.Generic;
using RolePlayGameData.Enums;

namespace RolePlayGameData.Models
{
    public class JobRecipeModel
    {
        public JobRecipeModel(MarkTypeEnums processor, int recipeId, float recipePoints, float recipePrice)
        {
            Processor = processor;
            RecipeId = recipeId;
            RecipePoints = recipePoints;
            RecipePrice = recipePrice;
        }

        public string RecipeName { get; set; }
        public float RecipePoints { get; set; }
        public float RecipePrice { get; set; }
        public MarkTypeEnums Processor { get; set; }
        public int RecipeId { get; set; }
        public int ProcessTime { get; set; }
        public bool RandomProcess { get; set; }
        public int MinRndAmount { get; set; }
        public int MaxRndAmount { get; set; }
        public Dictionary<ItemNameEnums, int> MatsList { get; set; }
        public Dictionary<ItemNameEnums, int> ProcessedList { get; set; }
    }
}