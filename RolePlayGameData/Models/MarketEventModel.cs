﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RolePlayGameData.Models
{
    public class MarketEventModel
    {
        public string EventName { get; set; }
        public string EventText { get; set; }
        public uint[] ItemNames { get; set; }
        public bool IsPositive { get; set; }
    }
}
