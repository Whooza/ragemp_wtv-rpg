﻿using GTANetworkAPI;
using System.Collections.Generic;

namespace RolePlayGameData.Models
{
    public class GarageSpawnModel
    {
        public uint MarkName { get; set; }
        public Dictionary<Vector3, Vector3> SpawnList { get; set; }
    }
}
