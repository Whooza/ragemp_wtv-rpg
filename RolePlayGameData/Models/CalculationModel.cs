﻿namespace RolePlayGameData.Models
{
    public class CalculationModel
    {
        public uint ItemName { get; set; }
        public uint ItemType { get; set; }
        public uint Amount { get; set; }
        public bool IsSell { get; set; }
    }
}
