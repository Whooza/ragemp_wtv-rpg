﻿namespace RolePlayGameData.Texts
{
    public class UserInterfaceTexts
    {
        // jobCenter //
        public const string JobCenterInfo =
            "Soziale Leistungen stehen allen Bürgern zu, welche keiner festen Tätigkeit" +
            " nachgehen. Die Auszahlung erfolgt auf ein vom Antragsteller gewähltes Konto." +
            " Sollten Leistungen unrechtmäßig bezogen werden, hat das strafrechtliche" +
            " Konsequenzen!";

        public const string JobCenterPunish =
            "Sollte ein Bezug sozialer Leitungen, durch die Aufnahme einer beruflichen" +
            " Tätigkeit nicht mehr gerechtfertigt sein, sind die Leistungen am selben" +
            " Tag abzumelden. Im Falle eines Versäumnisses kann dies eine strafrechtliche" +
            " Ahndung nach sich ziehen.";
    }
}