﻿namespace RolePlayGameData.Texts
{
    public class PlayerNotificationTexts
    {
        #region PlayerInventory

        public const string ItemNotFound = "Item wurde nicht gefunden!";

        #endregion

        #region Mining

        public const string FarmspotEmty =
            "Hier ist gerade nichts zu finden! Suche an einer anderen Stelle.";

        #endregion

        #region Commands

        public const string NoAdmin = "Du bist kein Admin!";
        public const string LowAdmin = "Dein Admin-Rang reicht für diese Aktion nicht aus!";
        public const string NoSupport = "Du bist kein Supporter!";
        public const string LowSupport = "Dein Support-Rang reicht für diese Aktion nicht aus!";
        public const string WrongInput = "Bitte gib Ganzzahlen für ModTyp und Mod ein!  /addmod <Zahl> <Zahl>";
        public const string WhitelistAddSuccessful = "~g~ was added to the whitelist";
        public const string VehicleNotFound = "Fahrzeug nicht gefunden";

        public const string GarageCmdUsage =
            "Die Garagennummer gibt es nicht. Garagen-Nummern: 1-3 oder 4: zum Worldspawn";

        #endregion

        #region VehicleInteraction

        public const string StopEngine = "Du hast den Motor gestoppt.";
        public const string StartEngine = "Du hast den Motor gestartet.";
        public const string LockVehicle = "Du hast das Fahrzeug verschlossen.";
        public const string UnlockVehicle = "Du hast das Fahrzeug aufgeschlossen.";
        public const string NoVehicleInRange = "Kein Fahrzeug in der Nähe!";
        public const string NotInVehicle = "Du sitzt in keinem Fahrzeug!";
        public const string NoVehicleKey = "Du hast keinen Schlüssel für dieses Fahrzeug!";
        public const string NoFuel = "Das Fahrzeug hat keinen Treibstoff mehr im Tank";

        #endregion

        #region MapMarkEditor

        public const string VehicleSpawnError = "Fahrzeug konnte nicht gespawn werden!";
        public const string DeleteMarkNoOneError = "Du kannst den ersten Marker mit [Id: 1] nicht löschen!";
        public const string ResetBiologyMsg = "Dein Hunger und Durst wurde aufgefüllt.";

        #endregion

        #region DumpsterEditor

        public const string  DumpsterSuccessfulCreated = "Container erfolgreich gesetzt.";
        public const string  DumpstersSuccessfulReloaded = "Container wurden neu geladen.";
        public const string  DumpsterSuccessfulDeleted = "Container erfolgreich gelöscht.";

        #endregion

        #region LocationEditor

        public const string MarkSuccessfulCreated = "Mark erfolgreich gesetzt.";
        public const string MarksSuccessfulReloaded = "Marker wurden neu geladen.";
        public const string MarkSuccessfulDeleted = "Mark erfolgreich gelöscht.";

        #endregion

        #region MoneySystem

        public const string NotEnoughCashMoney = "Du hast nicht genug Bargeld bei Dir!";
        public const string NotEnoughBankMoney = "Du hast nicht genug Geld auf Deinem Konto!";

        #endregion

        #region FuelStation

        public const string NotInStationRange = "Dein Fahrzeug steht zu weit von der Zapfsäule entfernt!";
        public const string FuelNotPayed = "Du bist ohne zu zahlen weggefahren! Es gibt Videobeweise dafür!";
        public const string TurnOffEngine = "Aus Gründen der Sicherheit muss der Motor aus sein!";

        #endregion

        #region Auction

        public const string AuctionCreated = "Die Auktion wurde erfolgreich erstellt.";
        public const string AuctionNotCreated = "Auktion konnte nicht erstellt werden!";
        public const string AuctionCanceled = "Die Auktion wurde abgebrochen.";
        public const string AuctionOfferSet = "Du hast ein Gebot abgegeben.";
        public const string AuctionOfferNotSet = "Gebot konnte nicht abgegeben werden!";

        #endregion
    }
}