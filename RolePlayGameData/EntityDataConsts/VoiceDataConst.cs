﻿namespace RolePlayGameData.EntityDataConsts
{
    public class VoiceDataConst
    {
        public const string VoiceStatus = "V_STATUS";
        public const string StatusDeath = "V_DEATH";
        public const string StatusAlive = "V_ALIVE";

        public const string CallPosition = "~10~0~0~3";

        public const string VoiceRange = "V_RANGE";
        public const string RangeHigh = "V_HIGH";
        public const string RangeMedium = "V_MED";
        public const string RangeLow = "V_LOW";

        public const string Mute = "V_MUTE";
    }
}