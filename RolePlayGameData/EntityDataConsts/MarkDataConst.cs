﻿namespace RolePlayGameData.EntityDataConsts
{
    public class MarkDataConst
    {
        public const string MarkData = "MARK_DATA";

        public const string LocationFlag = "FLAG_LOCATION";
        public const string DumpsterFlag = "FLAG_DUMPSTER";
        public const string GarageExitFrag = "FLAG_GARAGEEXIT";
    }
}