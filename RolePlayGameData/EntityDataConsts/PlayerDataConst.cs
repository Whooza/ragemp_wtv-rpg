﻿namespace RolePlayGameData.EntityDataConsts
{
    public class PlayerDataConst
    {
        // server site player flags //
        public const string IsLoggedInFlag = "IS_LOGGED_IN";
        public const string IsInLoginActionFlag = "IN_LOGIN_ACTION";
        public const string IsInGarageFlag = "IS_IN_GARAGE";

        // mining flags //
        public const string LocationData = "LOCATIONDATA";

        // server site player data //
        public const string PlayerAccount = "PLAYER_ACCOUNT";
        public const string PlayerBiology = "PLAYER_BIOLOGY";
        public const string PlayerCharSettings = "PLAYER_CHARSETTINGS";
        public const string PlayerInventory = "PLAYER_INVENTORY";
        public const string PlayerMoney = "PLAYER_MONEY";
        public const string PlayerParticular = "PLAYER_PARTICULAR";
        public const string PlayerPaycheck = "PLAYER_PAYCHECK";
        public const string PlayerPosition = "PLAYER_POSITION";
        public const string PlayerWeapon = "PLAYER_WEAPON";
    }
}