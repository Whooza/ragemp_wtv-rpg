﻿namespace RolePlayGameData.EntityDataConsts
{
    public class VehicleDataConst
    {
        // server site vehicle flags //
        public const string NewVehicleFlag = "VEH_NEW";
        public const string OwnedVehicleFlag = "VEH_OWNED";
        public const string FactionVehicleFlag = "VEH_FACTION";
        public const string AdminVehicleFlag = "VEH_ADMIN";
        public const string IsNearFuelPump = "VEH_NEAR_FUELPUMP";
        public const string IsRefueling = "VEH_ISREFUELING";

        // server site vehicle data //
        public const string CivVehicleData = "VEH_CIV_DATA";
        public const string FacVehicleData = "VEH_FAC_DATA";
        public const string NewVehicleData = "VEH_NEW_VEHICLE";

        public const string VehicleFuel = "VEH_FUEL";
        public const string VehicleInv = "VEH_INV";
        public const string VehicleKey = "VEH_KEY";
        public const string VehiclePos = "VEH_POS";
        public const string VehicleSet = "VEH_SET";

        // client site shared data //
        public const string FuelShared = "VEH_FUEL_SHARED";
        public const string TankCapacityShared = "VEH_TANK_CAPACITY";
        public const string FuelConsShared = "VEH_FUEL_CONSUMPTION";
    }
}