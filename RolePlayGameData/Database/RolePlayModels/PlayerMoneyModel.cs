﻿using System.ComponentModel.DataAnnotations;

namespace RolePlayGameData.Database.RolePlayModels
{
    public class PlayerMoneyModel
    {
        [Key] public int Id { get; set; }
        [Required][StringLength(64)] public string SocialClub { get; set; }
        [Required] public float CashMoney { get; set; }
        [Required] public float BackUpMoney { get; set; }
        [Required] public int SocialPaymentBank { get; set; }
        [Required] public int StatePaymentBank { get; set; }
    }
}