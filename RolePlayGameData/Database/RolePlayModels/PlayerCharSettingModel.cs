﻿using System.ComponentModel.DataAnnotations;

namespace RolePlayGameData.Database.RolePlayModels
{
    public class PlayerCharSettingModel
    {
        [Key] public int Id { get; set; }
        [Required][StringLength(64)] public string SocialClub { get; set; }
        [Required] public int FirstHeadShape { get; set; }
        [Required] public int SecondHeadShape { get; set; }
        [Required] public int FirstSkinTone { get; set; }
        [Required] public int SecondSkinTone { get; set; }
        [Required] public float HeadMix { get; set; }
        [Required] public float SkinMix { get; set; }
        [Required] public int HairModel { get; set; }
        [Required] public int FirstHairColor { get; set; }
        [Required] public int SecondHairColor { get; set; }
        [Required] public int BeardModel { get; set; }
        [Required] public int BeardColor { get; set; }
        [Required] public int ChestModel { get; set; }
        [Required] public int ChestColor { get; set; }
        [Required] public int BlemishesModel { get; set; }
        [Required] public int AgingModel { get; set; }
        [Required] public int ComplexionModel { get; set; }
        [Required] public int SunDamageModel { get; set; }
        [Required] public int FrecklesModel { get; set; }
        [Required] public float NoseWidth { get; set; }
        [Required] public float NoseHeight { get; set; }
        [Required] public float NoseLength { get; set; }
        [Required] public float NoseBridge { get; set; }
        [Required] public float NoseTip { get; set; }
        [Required] public float NoseShift { get; set; }
        [Required] public float BrowHeight { get; set; }
        [Required] public float BrowWidth { get; set; }
        [Required] public float CheekboneHeight { get; set; }
        [Required] public float CheekboneWidth { get; set; }
        [Required] public float CheeksWidth { get; set; }
        [Required] public float Eyes { get; set; }
        [Required] public float Lips { get; set; }
        [Required] public float JawWidth { get; set; }
        [Required] public float JawHeight { get; set; }
        [Required] public float ChinLength { get; set; }
        [Required] public float ChinPosition { get; set; }
        [Required] public float ChinWidth { get; set; }
        [Required] public float ChinShape { get; set; }
        [Required] public float NeckWidth { get; set; }
        [Required] public int EyesColor { get; set; }
        [Required] public int EyebrowsModel { get; set; }
        [Required] public int EyebrowsColor { get; set; }
        [Required] public int MakeupModel { get; set; }
        [Required] public int BlushModel { get; set; }
        [Required] public int BlushColor { get; set; }
        [Required] public int LipstickModel { get; set; }
        [Required] public int LipstickColor { get; set; }
    }
}