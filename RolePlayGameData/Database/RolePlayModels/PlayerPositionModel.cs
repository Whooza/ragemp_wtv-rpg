﻿using System.ComponentModel.DataAnnotations;

namespace RolePlayGameData.Database.RolePlayModels
{
    public class PlayerPositionModel
    {
        [Key] public int Id { get; set; }
        [Required][StringLength(64)] public string SocialClub { get; set; }
        [Required] public bool IsDead { get; set; }
        [Required] public uint TimeUntilRespawn { get; set; }
        [Required] public float LastPosX { get; set; }
        [Required] public float LastPosY { get; set; }
        [Required] public float LastPosZ { get; set; }
        [Required] public float Rotation { get; set; }
        [Required] public uint Dimension { get; set; }
    }
}