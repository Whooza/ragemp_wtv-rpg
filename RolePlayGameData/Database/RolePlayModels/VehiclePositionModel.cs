﻿using System.ComponentModel.DataAnnotations;

namespace RolePlayGameData.Database.RolePlayModels
{
    public class VehiclePositionModel
    {
        [Key] public int Id { get; set; }
        [Required] public int VehicleId { get; set; }
        [Required] public float LastPosX { get; set; }
        [Required] public float LastPosY { get; set; }
        [Required] public float LastPosZ { get; set; }
        [Required] public float Rotation { get; set; }
        [Required] public uint Dimension { get; set; }
    }
}