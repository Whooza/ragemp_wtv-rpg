﻿using System.ComponentModel.DataAnnotations;

namespace RolePlayGameData.Database.RolePlayModels
{
    public class VehicleFuelModel
    {
        [Key] public int Id { get; set; }
        [Required] public int VehicleId { get; set; }
        [Required] public float TankStatus { get; set; }
        [Required] public bool EngineStatus { get; set; }
    }
}