﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RolePlayGameData.Database.RolePlayModels
{
    public class MarketSystemModel
    {
        [Key] public int Id { get; set; }
        [Required] public uint ItemName { get; set; }
        [Required] public uint ItemType { get; set; }
        [Required] public float CurrentPrice { get; set; }
        [Required] public DateTime ChangeDate { get; set; }
    }
}
