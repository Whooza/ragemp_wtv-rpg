﻿using System.ComponentModel.DataAnnotations;

namespace RolePlayGameData.Database.RolePlayModels
{
    public class PlayerWeaponModel
    {
        [Key] public int Id { get; set; }
        [Required][StringLength(64)] public string SocialClub { get; set; }
        [Required] public uint Weapon { get; set; }
        [Required] public uint Amount { get; set; }
    }
}