﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RolePlayGameData.Database.RolePlayModels
{
    public class ServerPlayerWhiteListModel
    {
        [Key] public int Id { get; set; }
        [Required][StringLength(64)] public string SocialClubName { get; set; }
        [Required] public DateTime ListingDate { get; set; }
        [Required] public DateTime LastLogin { get; set; }
        [Required][StringLength(32)] public string LastIp { get; set; }
        [Required] public bool IsOnline { get; set; }
        [Required] public DateTime BannedUntil { get; set; }
        [Required][StringLength(128)] public string BanReason { get; set; }
    }
}