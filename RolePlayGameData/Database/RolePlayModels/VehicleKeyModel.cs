﻿using System.ComponentModel.DataAnnotations;

namespace RolePlayGameData.Database.RolePlayModels
{
    public class VehicleKeyModel
    {
        [Key] public int Id { get; set; }
        [Required] public int VehicleId { get; set; }
        [Required][StringLength(64)] public string FirstKeyPlayerSocial { get; set; }
        [Required][StringLength(64)] public string SecondKeyPlayerIdSocial { get; set; }
        [Required][StringLength(64)] public string ThirdKeyPlayerIdSocial { get; set; }
    }
}