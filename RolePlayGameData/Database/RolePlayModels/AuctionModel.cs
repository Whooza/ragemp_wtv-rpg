﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RolePlayGameData.Database.RolePlayModels
{
    public class AuctionModel
    {
        [Key] public int Id { get; set; }
        [Required] public bool IsActive { get; set; }
        [Required][StringLength(64)] public string Owner { get; set; }
        public string MaxTenderer { get; set; }
        [Required][StringLength(4096)] public string OldTenderer { get; set; }
        [Required] public uint ItemName { get; set; }
        [Required] public uint Amount { get; set; }
        [Required] public float StartPrice { get; set; }
        public float MaxOffer { get; set; }
        [Required] public bool IsInstantBuy { get; set; }        
        [Required] public float InstantBuyPrice { get; set; }        
        [Required] public uint Duration { get; set; }
        [Required] public DateTime EndTime { get; set; }
    }
}
