﻿using System.ComponentModel.DataAnnotations;

namespace RolePlayGameData.Database.RolePlayModels
{
    public class PlayerParticularModel
    {
        [Key] public int Id { get; set; }
        [Required][StringLength(64)] public string SocialClub { get; set; }
        [Required] public bool HasIdentityCard { get; set; }
        [Required][StringLength(64)] public string FirstName { get; set; }
        [Required][StringLength(64)] public string LastName { get; set; }
        [Required][StringLength(64)] public string Birthday { get; set; }
        [Required][StringLength(64)] public string Nationality { get; set; }
        [Required][StringLength(64)] public string EyeColor { get; set; }
        [Required][StringLength(64)] public string Height { get; set; }
        [Required] public bool Gender { get; set; }
    }
}