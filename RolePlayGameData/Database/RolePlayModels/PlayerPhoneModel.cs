﻿using System.ComponentModel.DataAnnotations;

namespace RolePlayGameData.Database.RolePlayModels
{
    public class PlayerPhoneModel
    {
        [Key] public int Id { get; set; }
        [Required][StringLength(64)] public string SocialClub { get; set; }
        [Required] public bool HasPhone { get; set; }
        [Required] public bool IsPhoneActive { get; set; }
        [Required] public uint PhoneNumber { get; set; }
    }
}