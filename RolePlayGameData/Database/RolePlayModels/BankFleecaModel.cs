﻿using System.ComponentModel.DataAnnotations;

namespace RolePlayGameData.Database.RolePlayModels
{
    public class BankFleecaModel
    {
        [Key] public int Id { get; set; }
        [Required][StringLength(64)] public string SocialClub { get; set; }
        [Required] public bool IsAccountActive { get; set; }
        [Required] public bool IsCreditCardActive { get; set; }
        [Required][StringLength(6)] public string PinCode { get; set; }
        [Required] public float AccountBalance { get; set; }
        [Required] public float CreditCardBalance { get; set; }
    }
}