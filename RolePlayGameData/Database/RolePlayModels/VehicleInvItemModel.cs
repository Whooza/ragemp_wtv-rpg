﻿using System.ComponentModel.DataAnnotations;

namespace RolePlayGameData.Database.RolePlayModels
{
    public class VehicleInvItemModel
    {
        [Key] public int Id { get; set; }
        [Required] public int VehicleId { get; set; }
        [Required] public uint Item { get; set; }
        [Required] public uint Amount { get; set; }
    }
}