﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RolePlayGameData.Database.RolePlayModels
{
    public class PlayerPaycheckModel
    {
        [Key] public int Id { get; set; }
        [Required][StringLength(64)] public string SocialClub { get; set; }
        [Required] public uint SocialPayMinutes { get; set; }
        [Required] public bool SocialPayIsActive { get; set; }
        [Required] public bool InCivActive { get; set; }
        [Required] public uint LscMinutes { get; set; }
        [Required] public bool InLscActive { get; set; }
        [Required] public uint LsrsMinutes { get; set; }
        [Required] public bool InLsrsActive { get; set; }
        [Required] public uint LspdMinutes { get; set; }
        [Required] public bool InLspdActive { get; set; }
        [Required] public uint FibMinutes { get; set; }
        [Required] public bool InFibActive { get; set; }
        [Required] public uint LssdMinutes { get; set; }
        [Required] public bool InLssdActive { get; set; }
        [Required] public uint DojMinutes { get; set; }
        [Required] public bool InDojActive { get; set; }
        [Required] public uint DcrMinutes { get; set; }
        [Required] public bool InDcrActive { get; set; }
        [Required] public DateTime LastPayment { get; set; }
    }
}