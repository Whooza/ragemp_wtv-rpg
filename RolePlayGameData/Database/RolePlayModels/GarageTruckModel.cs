﻿using System.ComponentModel.DataAnnotations;

namespace RolePlayGameData.Database.RolePlayModels
{
    public class GarageTruckModel
    {
        [Key] public int Id { get; set; }
        [Required] [StringLength(64)] public string SocialClub { get; set; }
        [Required] public uint Dimension { get; set; }

        [Required] public bool OneIsOwned { get; set; }
        [Required] public bool TwoIsOwned { get; set; }

        [Required] public bool OneIsPayed { get; set; }
        [Required] public bool TwoIsPayed { get; set; }

        [Required] [StringLength(256)] public string OneVehicleSlots { get; set; }
        [Required] [StringLength(256)] public string TwoVehicleSlots { get; set; }

        public float LastEntryX { get; set; }
        public float LastEntryY { get; set; }
        public float LastEntryZ { get; set; }
    }
}
