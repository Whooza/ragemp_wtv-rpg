﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RolePlayGameData.Database.RolePlayModels
{
    public class AuctionBufferModel
    {
        [Key] public int Id { get; set; }
        [Required][StringLength(64)] public string Owner { get; set; }
        [Required] public uint ItemName { get; set; }
        [Required] public uint Amount { get; set; }
        [Required] public DateTime StorageTime { get; set; }
    }
}
