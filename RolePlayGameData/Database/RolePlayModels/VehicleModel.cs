﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RolePlayGameData.Database.RolePlayModels
{
    public class VehicleModel
    {
        [Key] public int Id { get; set; }
        [Required][StringLength(64)] public string SocialClub { get; set; }
        [Required] public uint VehicleHash { get; set; }
        [Required] public uint Faction { get; set; }
        [Required] public bool LockStatus { get; set; }
        [Required][StringLength(12)] public string PlateText { get; set; }
        [Required] public DateTime BuyDate { get; set; }
    }
}