﻿using System.ComponentModel.DataAnnotations;

namespace RolePlayGameData.Database.RolePlayModels
{
    public class GarageSmallModel
    {
        [Key] public int Id { get; set; }
        [Required][StringLength(64)] public string SocialClub { get; set; }
        [Required] public uint Dimension { get; set; }

        [Required] public bool OneIsOwned { get; set; }
        [Required] public bool TwoIsOwned { get; set; }
        [Required] public bool ThreeIsOwned { get; set; }
        [Required] public bool FourIsOwned { get; set; }
        [Required] public bool FiveIsOwned { get; set; }
        [Required] public bool SixIsOwned { get; set; }
        [Required] public bool SevenIsOwned { get; set; }
        [Required] public bool AightIsOwned { get; set; }
        [Required] public bool NineIsOwned { get; set; }
        [Required] public bool TenIsOwned { get; set; }
        [Required] public bool ElevenIsOwned { get; set; }
        [Required] public bool TwelveIsOwned { get; set; }

        [Required] public bool OneIsPayed { get; set; }
        [Required] public bool TwoIsPayed { get; set; }
        [Required] public bool ThreeIsPayed { get; set; }
        [Required] public bool FourIsPayed { get; set; }
        [Required] public bool FiveIsPayed { get; set; }
        [Required] public bool SixIsPayed { get; set; }
        [Required] public bool SevenIsPayed { get; set; }
        [Required] public bool AightIsPayed { get; set; }
        [Required] public bool NineIsPayed { get; set; }
        [Required] public bool TenIsPayed { get; set; }
        [Required] public bool ElevenIsPayed { get; set; }
        [Required] public bool TwelveIsPayed { get; set; }
        
        [Required] [StringLength(256)] public string OneVehicleSlots { get; set; }
        [Required] [StringLength(256)] public string TwoVehicleSlots { get; set; }
        [Required] [StringLength(256)] public string ThreeVehicleSlots { get; set; }
        [Required] [StringLength(256)] public string FourVehicleSlots { get; set; }
        [Required] [StringLength(256)] public string FiveVehicleSlots { get; set; }
        [Required] [StringLength(256)] public string SixVehicleSlots { get; set; }
        [Required] [StringLength(256)] public string SevenVehicleSlots { get; set; }
        [Required] [StringLength(256)] public string AightVehicleSlots { get; set; }
        [Required] [StringLength(256)] public string NineVehicleSlots { get; set; }
        [Required] [StringLength(256)] public string TenVehicleSlots { get; set; }
        [Required] [StringLength(256)] public string ElevenVehicleSlots { get; set; }
        [Required] [StringLength(256)] public string TwelveVehicleSlots { get; set; }

        public float LastEntryX { get; set; }
        public float LastEntryY { get; set; }
        public float LastEntryZ { get; set; }
    }
}