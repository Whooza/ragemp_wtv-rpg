﻿using System.ComponentModel.DataAnnotations;

namespace RolePlayGameData.Database.RolePlayModels
{
    public class PlayerAccountModel
    {
        [Key] public int Id { get; set; }
        [Required][StringLength(64)] public string SocialClubName { get; set; }
        [Required][StringLength(64)] public string UserMail { get; set; }
        [Required][StringLength(64)] public string UserPass { get; set; }
        [Required] public int AdminLevel { get; set; }
        [Required] public int SupportLevel { get; set; }
        [Required] public int LscLevel { get; set; }
        [Required] public int LsrsLevel { get; set; }
        [Required] public int LspdLevel { get; set; }
        [Required] public int FibLevel { get; set; }
        [Required] public int LssdLevel { get; set; }
        [Required] public int DojLevel { get; set; }
        [Required] public int DcrLevel { get; set; }
        [Required] public bool CharCreatorComplete { get; set; }
    }
}