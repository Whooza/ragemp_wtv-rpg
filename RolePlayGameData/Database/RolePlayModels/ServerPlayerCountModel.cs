﻿using System.ComponentModel.DataAnnotations;

namespace RolePlayGameData.Database.RolePlayModels
{
    public class ServerPlayerCountModel
    {
        [Key] public int Id { get; set; }
        [Required] public uint CivCount { get; set; }
        [Required] public uint LscCount { get; set; }
        [Required] public uint LsrsCount { get; set; }
        [Required] public uint LspdCount { get; set; }
        [Required] public uint FibCount { get; set; }
        [Required] public uint LssdCount { get; set; }
        [Required] public uint DojCount { get; set; }
        [Required] public uint DcrCount { get; set; }
    }
}