﻿using System.ComponentModel.DataAnnotations;

namespace RolePlayGameData.Database.RolePlayModels
{
    public class VehicleSettingModel
    {
        [Key] public int Id { get; set; }
        [Required] public int VehicleId { get; set; }

        // COLOR //
        [Required] public int FirstColor { get; set; }
        [Required] public int SecondColor { get; set; }
        [Required] public bool Pearlescent { get; set; }

        // CUSTOMS - MODS //
        [Required] public int Spoiler { get; set; }
        [Required] public int FrontBumper { get; set; }
        [Required] public int RearBumper { get; set; }
        [Required] public int SideSkirt { get; set; }
        [Required] public int Exhaust { get; set; }
        [Required] public int Frame { get; set; }
        [Required] public int Grille { get; set; }
        [Required] public int Hood { get; set; }
        [Required] public int Fender { get; set; }
        [Required] public int RightFender { get; set; }
        [Required] public int Roof { get; set; }
        [Required] public int Engine { get; set; } // -1 to 3
        [Required] public int Brakes { get; set; } // -1 to 2
        [Required] public int Transmission { get; set; }
        [Required] public int Horn { get; set; } // -1 to 34
        [Required] public int Suspension { get; set; }
        [Required] public int Armor { get; set; } // -1 to 4
        [Required] public int Turbo { get; set; }
        [Required] public int Xenon { get; set; } // -1 to 0
        [Required] public int FrontWheels { get; set; }
        [Required] public int BackWheels { get; set; }
        [Required] public int UtilShadowSilver { get; set; }
        [Required] public int PlateHolders { get; set; }
        [Required] public int TrimDesign { get; set; }
        [Required] public int Ornaments { get; set; }
        [Required] public int DialDesign { get; set; }
        [Required] public int SteeringWheel { get; set; }
        [Required] public int ShiftLever { get; set; }
        [Required] public int Plaques { get; set; }
        [Required] public int Hydraulics { get; set; }
        [Required] public int Livery { get; set; }
        [Required] public int Plate { get; set; } // -1 to 3
        [Required] public int WindowTint { get; set; } // -1 to 2
    }
}