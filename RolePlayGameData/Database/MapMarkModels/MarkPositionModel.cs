﻿using System.ComponentModel.DataAnnotations;

namespace RolePlayGameData.Database.MapMarkModels
{
    public class MarkPositionModel
    {
        [Key] public int Id { get; set; }
        [Required] public uint MarkFaction { get; set; }
        [Required] public uint MarkType { get; set; }
        [Required] public uint MarkName { get; set; }
        [Required] public float PositionX { get; set; }
        [Required] public float PositionY { get; set; }
        [Required] public float PositionZ { get; set; }
        [Required] public float RotationX { get; set; }
        [Required] public float RotationY { get; set; }
        [Required] public float RotationZ { get; set; }
        [Required] public uint Dimension { get; set; }
    }
}