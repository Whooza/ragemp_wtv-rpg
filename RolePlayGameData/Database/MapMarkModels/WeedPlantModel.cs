﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RolePlayGameData.Database.MapMarkModels
{
    public class WeedPlantModel
    {
        [Key] public int Id { get; set; }
        [Required] public uint Type { get; set; }
        [Required] public bool Gender { get; set; }
        [Required] public bool GrowBooster { get; set; }
        [Required] public bool BudBooster { get; set; }
        [Required] public bool FertilizerK { get; set; }
        [Required] public bool FertilizerM { get; set; }
        [Required] public bool FertilizerS { get; set; }
        [Required] public float PosX { get; set; }
        [Required] public float PosY { get; set; }
        [Required] public float PosZ { get; set; }
        [Required] public DateTime PlantTime { get; set; }
    }
}