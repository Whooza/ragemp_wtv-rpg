﻿using System.ComponentModel.DataAnnotations;

namespace RolePlayGameData.Database.MapMarkModels
{
    public class DumpsterModel
    {
        [Key] public int Id { get; set; }
        [Required][StringLength(8192)] public string ItemList { get; set; }
        [Required] public float PositionX { get; set; }
        [Required] public float PositionY { get; set; }
        [Required] public float PositionZ { get; set; }
        [Required] public uint Dimension { get; set; }
    }
}