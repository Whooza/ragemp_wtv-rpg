﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RolePlayGameData.Database.MapMarkModels
{
    public class ShopSlotPosModel
    {
        [Key] public int Id { get; set; }
        [Required] public uint MarkName { get; set; }
        [Required] public bool IsActive { get; set; }
        [Required] public uint VehicleHash { get; set; }
        [Required] public uint FirstColor { get; set; }
        [Required] public uint SecondColor { get; set; }
        [Required] public DateTime CreationDate { get; set; }
    }
}