﻿namespace RolePlayGameData.Enums
{
    public enum AccessEnums
    {
        Error,
        NotWhitelisted,
        NotOnline,
        IpNotFound,
        Success
    }

    public enum LoginStateEnums
    {
        Whitelisted,
        NotWhiteListed,
        CharEdited,
        CharNotEdited,
        Success
    }
}