﻿namespace RolePlayGameData.Enums
{
    public enum FactionEnums
    {
        Civ,
        Lspd,
        Lssd,
        Fib,
        Lsrs,
        Lsc,
        Doj,
        Dcr
    }
}