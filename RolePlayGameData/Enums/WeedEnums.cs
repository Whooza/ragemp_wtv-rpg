﻿namespace RolePlayGameData.Enums
{
    public enum WeedTypeEnums
    {
        SpecialQueen,
        HollandsHope,
        WhiteWidow,
        OgKush,
        Critical,
        AmnesiaHaze
    }
}