﻿namespace RolePlayGameData.Enums
{
    public enum WeaponTypeEnums
    {
        Melee,
        Handgun,
        SubmachineGun,
        Shotgun,
        AssaultRifles,
        LightMachineGun,
        SniperRifle,
        HeavyWeapon,
        Throwable,
        Miscellaneous
    }

    public enum WeaponHashes : uint
    {
        // melee //
        Dagger = 0x92A27487,
        Bat = 0x958A4A8F,
        Bottle = 0xF9E6AA4B,
        Crowbar = 0x84BD7BFD,
        Unarmed = 0xA2719263,
        Flashlight = 0x8BB05FD7,
        Golfclub = 0x440E4788,
        Hammer = 0x4E875F73,
        Hatchet = 0xF9DCBF2D,
        Knuckle = 0xD8DF3C3C,
        Knife = 0x99B507EA,
        Machete = 0xDD5DF8D9,
        Switchblade = 0xDFE37640,
        Nightstick = 0x678B81B1,
        Wrench = 0x19044EE0,
        Battleaxe = 0xCD274149,
        Poolcue = 0x94117305,
        StoneHatchet = 0x3813FC08,

        // handguns //
        Pistol = 0x1B06D571,
        PistolMk2 = 0xBFE256D4,
        Combatpistol = 0x5EF9FEC4,
        Appistol = 0x22D8FE39,
        Stungun = 0x3656C8C1,
        Pistol50 = 0x99AEEB3B,
        Snspistol = 0xBFD21232,
        Snspistol_mk2 = 0x88374054,
        Heavypistol = 0xD205520E,
        VintagePistol = 0x83839C4,
        Flaregun = 0x47757124,
        MarksmanPistol = 0xDC4DB296,
        Revolver = 0xC1B3C3D1,
        RevolverMk2 = 0xCB96392F,
        DoubleAction = 0x97EA20B8,
        RayPistol = 0xAF3696A1,

        // smg //
        MicroSmg = 0x13532244,
        Smg = 0x2BE6766B,
        SmgMk2 = 0x78A97CD0,
        AssaultSmg = 0xEFE7E2DF,
        CombatPdw = 0xA3D4D34,
        MachinePistol = 0xDB1AA450,
        MiniSmg = 0xBD248B55,
        RayCarbine = 0x476BF155,

        // shotgun //
        PumpshotGun = 0x1D073A89,
        PumpshotgunMk2 = 0x555AF99A,
        SawnoffShotgun = 0x7846A318,
        AssaultShotgun = 0xE284C527,
        BullpupShotgun = 0x9D61E50F,
        Musket = 0xA89CB99E,
        HeavyShotgun = 0x3AABBBAA,
        DbShotgun = 0xEF951FBB,
        AutoShotgun = 0x12E82D3D,

        // assaultRifles //
        AssaultRifle = 0xBFEFFF6D,
        AssaultRifleMk2 = 0x394F415C,
        CarbineRifle = 0x83BF0278,
        CarbineRifleMk2 = 0xFAD1F1C9,
        Advancedrifle = 0xAF113F99,
        SpecialCarbine = 0xC0A3098D,
        SpecialCarbineMk2 = 0x969C3D67,
        BullpupRifle = 0x7F229F94,
        BullpupRifleMk2 = 0x84D6FAFD,
        CompactRifle = 0x624FE830,

        // machineGuns //
        Mg = 0x9D07F764,
        Combatmg = 0x7FD62962,
        Combatmg_mk2 = 0xDBBD7280,
        Gusenberg = 0x61012683,

        // sniperRifles //
        sniperrifle = 0x5FC3C11,
        HeavySniper = 0xC472FE2,
        HeavySniperMk2 = 0xA914799,
        MarksmanRifle = 0xC734385A,
        MarksmanRifleMk2 = 0x6A6C02E0,

        // heavy_weapons //
        Rpg = 0xB1CA77B1,
        Grenadelauncher = 0xA284510B,
        GrenadelauncherSmoke = 0x4DD2DC56,
        Minigun = 0x42BF8A85,
        Firework = 0x7F7497E5,
        Railgun = 0x6D544C99,
        HomingLauncher = 0x63AB0442,
        CompactLauncher = 0x781FE4A,
        RayMinigun = 0xB62D1F67,

        // throwables //
        Grenade = 0x93E220BD,
        BzGas = 0xA0973D5E,
        Smokegrenade = 0xFDBC8A50,
        Flare = 0x497FACC3,
        Molotov = 0x24B17070,
        Stickybomb = 0x2C3731D9,
        Proxmine = 0xAB564B93,
        Snowball = 0x787F0BB,
        Pipebomb = 0xBA45E8B8,
        Ball = 0x23C9F95C,

        // misc //
        Petrolcan = 0x34A67B97,
        Fireextinguisher = 0x60EC506,
        Parachute = 0xFBAB5776
    }
}