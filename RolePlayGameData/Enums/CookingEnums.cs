﻿namespace RolePlayGameData.Enums
{
    public enum CookingCategoryEnums
    {
        Meat,
        Fish,
        FingerFood,
        SoftDrink
    }

    public enum CookingLevelEnums
    {
        LevelOne,
        LevelTwo,
        LevelThree,
        LevelFour,
        LevelFive,
        LevelSix
    }
}