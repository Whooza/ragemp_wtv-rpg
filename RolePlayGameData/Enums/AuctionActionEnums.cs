﻿namespace RolePlayGameData.Enums
{
    public enum AuctionActionEnums
    {
        Create,
        Cancel,
        Offer,
        Buy,
        PickUp
    }
}
