﻿namespace RolePlayGameData.Enums
{
    public enum AnimFlagsEnums
    {
        Loop = 1 << 0,
        StopOnLastFrame = 1 << 1,
        OnlyAnimateUpperBody = 1 << 4,
        AllowPlayerControl = 1 << 5,
        Cancellable = 1 << 7
    }

    public enum CivilianAnimations
    {
        MoneyGrab,
        FacePalm,
        YouLoco,
        FreakOut,
        ThumbOnEars,
        Victory,
        Crouch,
        Dig,
        Cry,
        HurryUp
    }

    public enum DanceAnimations
    {
        Dj
    }

    public enum FactionAnimations
    {
        HandCuffed,
        MedicRevive,
        MedicKneel,
        MechanicVehicle,
        MechanicFixing
    }
}