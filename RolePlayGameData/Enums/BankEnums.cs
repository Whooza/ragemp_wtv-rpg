﻿namespace RolePlayGameData.Enums
{
    public enum BankNameEnums
    {
        FleecaBank,
        MazeBank,
        UnionDepository
    }

    public enum BankActionEnums
    {
        Withdraw,
        Deposit,
        Tranfer
    }
}