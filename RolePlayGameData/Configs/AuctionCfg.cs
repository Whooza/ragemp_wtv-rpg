﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RolePlayGameData.Configs
{
    public class AuctionCfg
    {
        public static float CommissionAuction => 13.123f;
        public static float CommissionInstant => 13.123f;
        public static float DailyPrice => 100.9f;
        public static float InstantBuyTax => 10.5f;
        public static float StorageCosts => 10.5f;
        public static float StartPrice => 10.5f;
    }
}
