﻿using System.Collections.Generic;
using RolePlayGameData.Enums;

namespace RolePlayGameData.Configs
{
    public static class UnpackCfg
    {
        public static readonly Dictionary<ItemNameEnums, Dictionary<ItemNameEnums, int>> UnpackItems =
            new Dictionary<ItemNameEnums, Dictionary<ItemNameEnums, int>>
            {
                {
                    ItemNameEnums.AppleCrate, new Dictionary<ItemNameEnums, int>
                    {
                        {ItemNameEnums.Apple, 36}
                    }
                },
                {
                    ItemNameEnums.AvocadoCrate, new Dictionary<ItemNameEnums, int>
                    {
                        {ItemNameEnums.Avocado, 6}
                    }
                },
                {
                    ItemNameEnums.CigarettePack, new Dictionary<ItemNameEnums, int>
                    {
                        {ItemNameEnums.Cigarette, 20}
                    }
                },
                {
                    ItemNameEnums.GrapevineCrate, new Dictionary<ItemNameEnums, int>
                    {
                        {ItemNameEnums.Grapevine, 18}
                    }
                },
                {
                    ItemNameEnums.LemonSack, new Dictionary<ItemNameEnums, int>
                    {
                        {ItemNameEnums.Lemon, 12}
                    }
                },
                {
                    ItemNameEnums.OrangeSack, new Dictionary<ItemNameEnums, int>
                    {
                        {ItemNameEnums.Orange, 18}
                    }
                },
                {
                    ItemNameEnums.PistachioSack, new Dictionary<ItemNameEnums, int>
                    {
                        {ItemNameEnums.Pistachio, 4}
                    }
                },
                {
                    ItemNameEnums.TomatoCrate, new Dictionary<ItemNameEnums, int>
                    {
                        {ItemNameEnums.Tomato, 30}
                    }
                },
                {
                    ItemNameEnums.WalnutsSack, new Dictionary<ItemNameEnums, int>
                    {
                        {ItemNameEnums.Walnut, 20}
                    }
                }
            };
    }
}