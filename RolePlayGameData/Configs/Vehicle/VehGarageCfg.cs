﻿using GTANetworkAPI;

namespace RolePlayGameData.Configs.Vehicle
{
    public class VehGarageCfg
    {
        // garage dimensions //
        public static readonly uint DimS1 = 2500;
        public static readonly uint DimS2 = 5000;
        public static readonly uint DimS3 = 7500;
        public static readonly uint DimS4 = 10000;
        public static readonly uint DimS5 = 12500;
        public static readonly uint DimS6 = 15000;
        public static readonly uint DimS7 = 17500;
        public static readonly uint DimS8 = 20000;
        public static readonly uint DimS9 = 22500;
        public static readonly uint DimS10 = 25000;
        public static readonly uint DimS11 = 27500;
        public static readonly uint DimS12 = 30000;

        public static readonly uint DimMed1 = 32500;
        public static readonly uint DimMed2 = 35000;
        public static readonly uint DimMed3 = 37500;
        public static readonly uint DimMed4 = 40000;
        public static readonly uint DimMed5 = 42500;
        public static readonly uint DimMed6 = 45000;

        public static readonly uint DimBig1 = 47500;
        public static readonly uint DimBig2 = 50000;

        public static readonly uint DimTruck1 = 52500;
        public static readonly uint DimTruck2 = 55000;

        // basic configs //
        public static readonly int SlotCountSmall = 1;
        public static readonly int SlotCountMed = 3;
        public static readonly int SlotCountGarage = 8;

        // garage rental //
        public static readonly float RentSmall = 99.95f;
        public static readonly float RentMedium = 269.98f;
        public static readonly float RentBig = 639.99f;
        public static readonly float RentTruck = 598.49f;

        // garage locations //
        public static readonly Vector3 SpawnPosSmall = new Vector3(178.7454f, -1006.817f, -98f);
        public static readonly Vector3 SpawnRotSmall = new Vector3(0, 0, 88.80618f);

        public static readonly Vector3 SpawnPosMed = new Vector3(206.0794f, -1006.796f, -98f);
        public static readonly Vector3 SpawnRotMed = new Vector3(0, 0, 53.97828f);

        public static readonly Vector3 SpawnPosBig = new Vector3(237.8493f, -1004.858f, -98f);
        public static readonly Vector3 SpawnRotBig = new Vector3(0, 0, 82.01811f);

        // playerOutsideSpawn //
        public static readonly Vector3 ExitSpawnPosS1 = new Vector3(-435.7766f, 6154.672f, 31.4782f);
        public static readonly Vector3 ExitSpawnRotS1 = new Vector3(0, 0, 43.91854f);
        public static readonly Vector3 ExitSpawnPosS2 = new Vector3(-1402.488f, -452.1781f, 34.48264f);
        public static readonly Vector3 ExitSpawnRotS2 = new Vector3(0, 0, 212.041f);
        public static readonly Vector3 ExitSpawnPosS3 = new Vector3(223.0209f, -8.24767f, 73.78012f);
        public static readonly Vector3 ExitSpawnRotS3 = new Vector3(0, 0, 341.0817f);
        public static readonly Vector3 ExitSpawnPosS4 = new Vector3(2891.005f, 4503.72f, 48.09026f);
        public static readonly Vector3 ExitSpawnRotS4 = new Vector3(0, 0, 240.6052f);
        public static readonly Vector3 ExitSpawnPosS5 = new Vector3(1698.573f, 6425.945f, 32.7638f);
        public static readonly Vector3 ExitSpawnRotS5 = new Vector3(0, 0, 151.982f);
        public static readonly Vector3 ExitSpawnPosS6 = new Vector3(550.4942f, 2656.31f, 42.21941f);
        public static readonly Vector3 ExitSpawnRotS6 = new Vector3(0, 0, 192.3092f);
        public static readonly Vector3 ExitSpawnPosS7 = new Vector3(1952.951f, 3753.519f, 32.21045f);
        public static readonly Vector3 ExitSpawnRotS7 = new Vector3(0, 0, 27.17355f);
        public static readonly Vector3 ExitSpawnPosS8 = new Vector3(-1345.439f, -889.9881f, 13.43789f);
        public static readonly Vector3 ExitSpawnRotS8 = new Vector3(0, 0, 216.5782f);
        public static readonly Vector3 ExitSpawnPosS9 = new Vector3(2520.016f, 2613.906f, 37.94487f);
        public static readonly Vector3 ExitSpawnRotS9 = new Vector3(0, 0, 196.1859f);
        public static readonly Vector3 ExitSpawnPosS10 = new Vector3(569.0734f, -2667.153f, 6.073102f);
        public static readonly Vector3 ExitSpawnRotS10 = new Vector3(0, 0, 0.6198658f);
        public static readonly Vector3 ExitSpawnPosS11 = new Vector3(1651.326f, 4829.683f, 42.02613f);
        public static readonly Vector3 ExitSpawnRotS11 = new Vector3(0, 0, 184.6564f);
        public static readonly Vector3 ExitSpawnPosS12 = new Vector3(357.0743f, -1816.867f, 28.51308f);
        public static readonly Vector3 ExitSpawnRotS12 = new Vector3(0, 0, 227.7466f);

        public static readonly Vector3 ExitSpawnPosM1 = new Vector3(-1194.141f, -1298.408f, 5.172181f);
        public static readonly Vector3 ExitSpawnRotM1 = new Vector3(0, 0, 26.25217f);
        public static readonly Vector3 ExitSpawnPosM2 = new Vector3(51.65468f, -1317.758f, 29.28906f);
        public static readonly Vector3 ExitSpawnRotM2 = new Vector3(0, 0, 317.8298f);
        public static readonly Vector3 ExitSpawnPosM3 = new Vector3(-358.6409f, 6061.799f, 31.50011f);
        public static readonly Vector3 ExitSpawnRotM3 = new Vector3(0, 0, 41.32493f);
        public static readonly Vector3 ExitSpawnPosM4 = new Vector3(-38.94519f, 6420.181f, 31.49056f);
        public static readonly Vector3 ExitSpawnRotM4 = new Vector3(0, 0, 230.3248f);
        public static readonly Vector3 ExitSpawnPosM5 = new Vector3(-153.7014f, -41.30181f, 54.39616f);
        public static readonly Vector3 ExitSpawnRotM5 = new Vector3(0, 0, 68.14226f);
        public static readonly Vector3 ExitSpawnPosM6 = new Vector3(1737.834f, 3709.414f, 34.13646f);
        public static readonly Vector3 ExitSpawnRotM6 = new Vector3(0, 0, 17.77703f);
        
        public static readonly Vector3 ExitSpawnPosB1 = new Vector3(-116.1055f, -1772.53f, 29.82443f);
        public static readonly Vector3 ExitSpawnRotB1 = new Vector3(0, 0, 36.38596f);
        public static readonly Vector3 ExitSpawnPosB2 = new Vector3(-1254.443f, -671.1176f, 25.99364f);
        public static readonly Vector3 ExitSpawnRotB2 = new Vector3(0, 0, 305.9352f);

        // smallGarage slots //
        public static readonly Vector3 Pos1Small = new Vector3(173.2447f, -1003.898f, -99.00863f);
        public static readonly Vector3 Rot1Small = new Vector3(0, 0, 180f);

        // mediumGarage slots //
        public static readonly Vector3 Pos1Med = new Vector3(197.9994f, -1000.857f, -98.5f);
        public static readonly Vector3 Rot1Med = new Vector3(0, 0, 180f);

        public static readonly Vector3 Pos2Med = new Vector3(203.7067f, -1001.336f, -98.5f);
        public static readonly Vector3 Rot2Med = new Vector3(0, 0, 180f);

        public static readonly Vector3 Pos3Med = new Vector3(193.3358f, -1001.127f, -98.5f);
        public static readonly Vector3 Rot3Med = new Vector3(0, 0, 180f);

        // bigGarage slots //
        public static readonly Vector3 Pos1Big = new Vector3(232.5f, -980.123f, -98.5f);
        public static readonly Vector3 Rot1Big = new Vector3(0, 0, 90f);

        public static readonly Vector3 Pos2Big = new Vector3(232.5f, -985.123f, -98.5f);
        public static readonly Vector3 Rot2Big = new Vector3(0, 0, 90f);

        public static readonly Vector3 Pos3Big = new Vector3(232.5f, -990.123f, -98.5f);
        public static readonly Vector3 Rot3Big = new Vector3(0, 0, 90f);

        public static readonly Vector3 Pos4Big = new Vector3(232.5f, -995.123f, -98.5f);
        public static readonly Vector3 Rot4Big = new Vector3(0, 0, 90f);

        public static readonly Vector3 Pos5Big = new Vector3(224.5f, -980.123f, -98.5f);
        public static readonly Vector3 Rot5Big = new Vector3(0, 0, 270f);

        public static readonly Vector3 Pos6Big = new Vector3(224.5f, -985.123f, -98.5f);
        public static readonly Vector3 Rot6Big = new Vector3(0, 0, 270f);

        public static readonly Vector3 Pos7Big = new Vector3(224.5f, -990.123f, -98.5f);
        public static readonly Vector3 Rot7Big = new Vector3(0, 0, 270f);

        public static readonly Vector3 Pos8Big = new Vector3(224.5f, -995.123f, -98.5f);
        public static readonly Vector3 Rot8Big = new Vector3(0, 0, 270f);
    }
}