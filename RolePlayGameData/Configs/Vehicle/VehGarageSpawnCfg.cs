﻿using GTANetworkAPI;
using RolePlayGameData.Enums;
using RolePlayGameData.Models;
using System.Collections.Generic;

namespace RolePlayGameData.Configs.Vehicle
{
    public class VehGarageSpawnCfg
    {
        public static readonly Dictionary<uint, List<GarageSpawnModel>> GarageSpawnList = new Dictionary<uint, List<GarageSpawnModel>>
        {
            { (uint)MarkTypeEnums.CivSmallGarage, new List<GarageSpawnModel>
            {
                new GarageSpawnModel
                {
                    MarkName    = 1,
                    SpawnList   = new Dictionary<Vector3, Vector3>
                    {
                        { new Vector3(-445.665f, 6142.755f, 31.47833f), new Vector3(0, 0, 251.2916f) },
                        { new Vector3(-441.1625f, 6138.182f, 31.47833f), new Vector3(0, 0, 265.5366f) },
                        { new Vector3(-433.7234f, 6133.958f, 31.47834f), new Vector3(0, 0, 226.5811f) }
                    }
                },
                new GarageSpawnModel
                {
                    MarkName    = 2,
                    SpawnList   = new Dictionary<Vector3, Vector3>
                    {
                        { new Vector3(-1417.904f, -465.351f, 34.46004f), new Vector3(0, 0, 118.4599f) },
                        { new Vector3(-1373.12f, -447.8668f, 34.47756f), new Vector3(0, 0, 220.7996f) },
                        { new Vector3(-1371.512f, -456.4778f, 34.47755f), new Vector3(0, 0, 94.32478f) }
                    }
                },
                new GarageSpawnModel
                {
                    MarkName    = 3,
                    SpawnList   = new Dictionary<Vector3, Vector3>
                    {
                        { new Vector3(217.0014f, 27.54715f, 79.4351f), new Vector3(0, 0, 247.8433f) },
                        { new Vector3(256.4935f, -13.58957f, 73.68513f), new Vector3(0, 0, 72.65955f) },
                        { new Vector3(222.9249f, 18.24286f, 79.46677f), new Vector3(0, 0, 337.2053f) }
                    }
                },
                new GarageSpawnModel
                {
                    MarkName    = 4,
                    SpawnList   = new Dictionary<Vector3, Vector3>
                    {
                        { new Vector3(2906.168f, 4467.458f, 48.2621f), new Vector3(0, 0, 123.8008f) },
                        { new Vector3(2882.694f, 4463.817f, 48.40493f), new Vector3(0, 0, 284.3263f) },
                        { new Vector3(2864.004f, 4472.896f, 48.36124f), new Vector3(0, 0, 286.9201f) }                        
                    }
                },
                new GarageSpawnModel
                {
                    MarkName    = 5,
                    SpawnList   = new Dictionary<Vector3, Vector3>
                    {
                        { new Vector3(1684.365f, 6434.783f, 32.27665f), new Vector3(0, 0, 207.2446f) },
                        { new Vector3(1684.296f, 6422.022f, 32.28568f), new Vector3(0, 0, 195.8335f) },
                        { new Vector3(1722.327f, 6427.387f, 33.51078f), new Vector3(0, 0, 133.3512f) }
                    }
                },
                new GarageSpawnModel
                {
                    MarkName    =  6,
                    SpawnList   = new Dictionary<Vector3, Vector3>
                    {
                        { new Vector3(532.9669f, 2663.495f, 42.31953f), new Vector3(0, 0, 86.29713f) },
                        { new Vector3(539.908f, 2651.483f, 42.3081f), new Vector3(0, 0, 118.0984f) },
                        { new Vector3(510.3841f, 2652.411f, 42.80374f), new Vector3(0, 0, 289.9734f) }
                    }
                },
                new GarageSpawnModel
                {
                    MarkName    =  7,
                    SpawnList   = new Dictionary<Vector3, Vector3>
                    {
                        { new Vector3(1970.948f, 3749.128f, 32.29441f), new Vector3(0, 0, 209.7481f) },
                        { new Vector3(1967.153f, 3769.484f, 32.19434f), new Vector3(0, 0, 33.18183f) },
                        { new Vector3(1965.432f, 3778.953f, 32.19916f), new Vector3(0, 0, 30.80926f) }
                    }
                },
                new GarageSpawnModel
                {
                    MarkName    =  8,
                    SpawnList   = new Dictionary<Vector3, Vector3>
                    {
                        { new Vector3(-1344.197f, -897.3618f, 13.44281f), new Vector3(0, 0, 126.8572f) },
                        { new Vector3(-1336.293f, -899.0081f, 13.40597f), new Vector3(0, 0, 302.7546f) },
                        { new Vector3(-1372.68f, -890.705f, 13.77027f), new Vector3(0, 0, 317.7169f) }
                    }
                },
                new GarageSpawnModel
                {
                    MarkName    =  9,
                    SpawnList   = new Dictionary<Vector3, Vector3>
                    {
                        { new Vector3(2534.908f, 2626.777f, 37.94487f), new Vector3(0, 0, 357.5892f) },
                        { new Vector3(2553.115f, 2642.671f, 38.29858f), new Vector3(0, 0, 234.1274f) },
                        { new Vector3(2561.523f, 2616.45f, 37.73164f), new Vector3(0, 0, 21.24776f) }
                    }
                },
                new GarageSpawnModel
                {
                    MarkName    =  10,
                    SpawnList   = new Dictionary<Vector3, Vector3>
                    {
                        { new Vector3(589.1521f, -2678.656f, 6.139485f), new Vector3(0, 0, 236.9164f) },
                        { new Vector3(595.0289f, -2695.652f, 6.14231f), new Vector3(0, 0, 269.8715f) },
                        { new Vector3(594.9429f, -2688.091f, 6.174107f), new Vector3(0, 0, 280.8323f) }
                    }
                },
                new GarageSpawnModel
                {
                    MarkName    =  11,
                    SpawnList   = new Dictionary<Vector3, Vector3>
                    {
                        { new Vector3(1637.247f, 4856.409f, 42.02235f), new Vector3(0, 0, 185.3065f) },
                        { new Vector3(1638.275f, 4832.346f, 41.98003f), new Vector3(0, 0, 190.13f) },
                        { new Vector3(1644.013f, 4820.926f, 42.0742f), new Vector3(0, 0, 278.8633f) }
                    }
                },
                new GarageSpawnModel
                {
                    MarkName    =  12,
                    SpawnList   = new Dictionary<Vector3, Vector3>
                    {
                        { new Vector3(356.3439f, -1807.458f, 28.91303f), new Vector3(0, 0, 52.19429f) },
                        { new Vector3(393.8784f, -1819.941f, 28.79545f), new Vector3(0, 0, 122.2795f) },
                        { new Vector3(335.5005f, -1818.923f, 27.86748f), new Vector3(0, 0, 316.4428f) }
                    }
                }
            } },
            { (uint)MarkTypeEnums.CivMediumGarage, new List<GarageSpawnModel>
            {
                new GarageSpawnModel
                {
                    MarkName    =  1,
                    SpawnList   = new Dictionary<Vector3, Vector3>
                    {
                        { new Vector3(-1209.268f, -1306.069f, 4.72123f), new Vector3(0, 0, 118.1356f) },
                        { new Vector3(-1203.634f, -1333.048f, 4.830985f), new Vector3(0, 0, 196.3811f) },
                        { new Vector3(-1227.918f, -1320.09f, 4.162745f), new Vector3(0, 0, 229.6389f) }
                    }
                },
                new GarageSpawnModel
                {
                    MarkName    =  2,
                    SpawnList   = new Dictionary<Vector3, Vector3>
                    {
                        { new Vector3(38.47391f, -1312.08f, 29.31215f), new Vector3(0, 0, 3.528638f) },
                        { new Vector3(43.61426f, -1311.776f, 29.29671f), new Vector3(0, 0, 358.9098f) },
                        { new Vector3(17.18995f, -1313.241f, 29.25676f), new Vector3(0, 0, 349.7465f) }
                    }
                },
                new GarageSpawnModel
                {
                    MarkName    =  3,
                    SpawnList   = new Dictionary<Vector3, Vector3>
                    {
                        { new Vector3(-356.392f, 6085.021f, 31.43094f), new Vector3(0, 0, 224.7028f) },
                        { new Vector3(-360.8935f, 6081.142f, 31.49179f), new Vector3(0, 0, 231.1174f) },
                        { new Vector3(-365.6566f, 6076.097f, 31.49487f), new Vector3(0, 0, 223.3121f) }
                    }
                },
                new GarageSpawnModel
                {
                    MarkName    =  4,
                    SpawnList   = new Dictionary<Vector3, Vector3>
                    {
                        { new Vector3(-22.53424f, 6411.902f, 31.49044f), new Vector3(0, 0, 223.3386f) },
                        { new Vector3(-53.15184f, 6417.691f, 31.49037f), new Vector3(0, 0, 223.9346f) },
                        { new Vector3(-13.94765f, 6402.188f, 31.47224f), new Vector3(0, 0, 225.5122f) }
                    }
                },
                new GarageSpawnModel
                {
                    MarkName    =  5,
                    SpawnList   = new Dictionary<Vector3, Vector3>
                    {
                        { new Vector3(-174.1804f, -48.10554f, 52.38931f), new Vector3(0, 0, 159.2709f) },
                        { new Vector3(-162.6314f, -31.90842f, 52.78598f), new Vector3(0, 0, 157.0716f) },
                        { new Vector3(-160.5216f, -38.52836f, 53.054f), new Vector3(0, 0, 105.4314f) }
                    }
                },
                new GarageSpawnModel
                {
                    MarkName    =  6,
                    SpawnList   = new Dictionary<Vector3, Vector3>
                    {
                        { new Vector3(1720.087f, 3714.807f, 34.20621f), new Vector3(0, 0, 16.15002f) },
                        { new Vector3(1738.308f, 3714.187f, 34.11891f), new Vector3(0, 0, 21.35094f) },
                        { new Vector3(1748.496f, 3714.862f, 34.08411f), new Vector3(0, 0, 38.05573f) }
                    }
                }
            } },
            { (uint)MarkTypeEnums.CivBigGarage, new List<GarageSpawnModel>
            {
                new GarageSpawnModel
                {
                    MarkName    =  1,
                    SpawnList   = new Dictionary<Vector3, Vector3>
                    {
                        { new Vector3(-70.16714f, -1833.412f, 26.9415f), new Vector3(0, 0, 308.9977f) },
                        { new Vector3(-63.25312f, -1837.823f, 26.74468f), new Vector3(0, 0, 337.3607f) },
                        { new Vector3(-44.87783f, -1840.63f, 26.21266f), new Vector3(0, 0, 140.0212f) }
                    }
                },
                new GarageSpawnModel
                {
                    MarkName    =  2,
                    SpawnList   = new Dictionary<Vector3, Vector3>
                    {
                        { new Vector3(-1248.583f, -666.9229f, 25.78782f), new Vector3(0, 0, 216.5135f) },
                        { new Vector3(-1241.528f, -675.4889f, 25.02415f), new Vector3(0, 0, 218.1718f) },
                        { new Vector3(-1221.697f, -706.8517f, 22.43705f), new Vector3(0, 0, 306.2227f) }                        
                    }
                }
            } },
            { (uint)MarkTypeEnums.CivTruckGarage, new List<GarageSpawnModel>
            {
                new GarageSpawnModel
                {
                    MarkName    =  1,
                    SpawnList   = new Dictionary<Vector3, Vector3>
                    {
                        { new Vector3(0f, 0f, 0f), new Vector3(0, 0, 0f) },
                        { new Vector3(0f, 0f, 0f), new Vector3(0, 0, 0f) },
                        { new Vector3(0f, 0f, 0f), new Vector3(0, 0, 0f) }
                    }
                },
                new GarageSpawnModel
                {
                    MarkName    =  2,
                    SpawnList   = new Dictionary<Vector3, Vector3>
                    {
                        { new Vector3(0f, 0f, 0f), new Vector3(0, 0, 0f) },
                        { new Vector3(0f, 0f, 0f), new Vector3(0, 0, 0f) },
                        { new Vector3(0f, 0f, 0f), new Vector3(0, 0, 0f) }
                    }
                }
            } }
        };
    }
}