﻿using System.Collections.Generic;
using RolePlayGameData.Enums;
using RolePlayGameData.Models;

namespace RolePlayGameData.Configs.Vehicle
{
    public class VehShopMarkCfg
    {
        public static readonly List<PositionModel> VehicleShopMarkPositions = new List<PositionModel>
        {
            // super //
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivSportsCarTrader,
                MarkName = MarkNameEnums.SportsSlot1, VehicleClass = VehicleClassEnums.Super, PosX = -63.28791f,
                PosY = -1117.188f, PosZ = 26.4307f, Rotation = 5f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivSportsCarTrader,
                MarkName = MarkNameEnums.SportsSlot2, VehicleClass = VehicleClassEnums.Super, PosX = -59.06092f,
                PosY = -1116.905f, PosZ = 26.43412f, Rotation = 5f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivSportsCarTrader,
                MarkName = MarkNameEnums.SportsSlot3, VehicleClass = VehicleClassEnums.Super, PosX = -54.90405f,
                PosY = -1116.757f, PosZ = 26.43454f, Rotation = 5f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivSportsCarTrader,
                MarkName = MarkNameEnums.SportsSlot4, VehicleClass = VehicleClassEnums.Super, PosX = -50.56716f,
                PosY = -1116.301f, PosZ = 26.43465f, Rotation = 5f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivSportsCarTrader,
                MarkName = MarkNameEnums.SportsSlot5, VehicleClass = VehicleClassEnums.Super, PosX = -46.20789f,
                PosY = -1116.237f, PosZ = 26.43427f, Rotation = 5f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivSportsCarTrader,
                MarkName = MarkNameEnums.SportsSlot6, VehicleClass = VehicleClassEnums.Super, PosX = -41.45039f,
                PosY = -1116.543f, PosZ = 26.43513f, Rotation = 5f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivSportsCarTrader,
                MarkName = MarkNameEnums.SportsSlot7, VehicleClass = VehicleClassEnums.Super, PosX = -43.79637f,
                PosY = -1109.639f, PosZ = 26.43809f, Rotation = 75f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivSportsCarTrader,
                MarkName = MarkNameEnums.SportsSlot8, VehicleClass = VehicleClassEnums.Super, PosX = -49.9005f,
                PosY = -1107.39f, PosZ = 26.43809f, Rotation = 75f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivSportsCarTrader,
                MarkName = MarkNameEnums.SportsSlot9, VehicleClass = VehicleClassEnums.Super, PosX = -55.17018f,
                PosY = -1105.497f, PosZ = 26.43809f, Rotation = 75f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivSportsCarTrader,
                MarkName = MarkNameEnums.SportsSlot10, VehicleClass = VehicleClassEnums.Super, PosX = -60.65337f,
                PosY = -1103.6f, PosZ = 26.42464f, Rotation = 75f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivSportsCarTrader,
                MarkName = MarkNameEnums.SportsSlot11, VehicleClass = VehicleClassEnums.Super, PosX = -56.67496f,
                PosY = -1097.145f, PosZ = 26.42235f, Rotation = 115.0f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivSportsCarTrader,
                MarkName = MarkNameEnums.SportsSlot12, VehicleClass = VehicleClassEnums.Super, PosX = -50.57174f,
                PosY = -1094.151f, PosZ = 26.42235f, Rotation = 210.0f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivSportsCarTrader,
                MarkName = MarkNameEnums.SportsSlot13, VehicleClass = VehicleClassEnums.Super, PosX = -46.01278f,
                PosY = -1095.837f, PosZ = 26.42235f, Rotation = 210.0f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivSportsCarTrader,
                MarkName = MarkNameEnums.SportsSlot14, VehicleClass = VehicleClassEnums.Super, PosX = -41.58806f,
                PosY = -1097.523f, PosZ = 26.42235f, Rotation = 210.0f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivSportsCarTrader,
                MarkName = MarkNameEnums.SportsSlot15, VehicleClass = VehicleClassEnums.Super, PosX = -48.2829f,
                PosY = -1099.353f, PosZ = 26.42235f, Rotation = 210.0f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivSportsCarTrader,
                MarkName = MarkNameEnums.SportsSlot16, VehicleClass = VehicleClassEnums.Super, PosX = -43.79685f,
                PosY = -1100.92f, PosZ = 26.42235f, Rotation = 210.0f
            },

            // motorcycle //
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivMotorcycleTrader,
                MarkName = MarkNameEnums.MotorcyclesSlot1, VehicleClass = VehicleClassEnums.Motocycles,
                PosX = 243.5493f, PosY = -1149.333f, PosZ = 29.0f, Rotation = 176.0f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivMotorcycleTrader,
                MarkName = MarkNameEnums.MotorcyclesSlot2, VehicleClass = VehicleClassEnums.Motocycles,
                PosX = 246.7420f, PosY = -1149.333f, PosZ = 29.0f, Rotation = 176.0f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivMotorcycleTrader,
                MarkName = MarkNameEnums.MotorcyclesSlot3, VehicleClass = VehicleClassEnums.Motocycles,
                PosX = 250.1491f, PosY = -1149.333f, PosZ = 29.0f, Rotation = 176.0f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivMotorcycleTrader,
                MarkName = MarkNameEnums.MotorcyclesSlot4, VehicleClass = VehicleClassEnums.Motocycles,
                PosX = 253.2802f, PosY = -1149.333f, PosZ = 29.0f, Rotation = 176.0f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivMotorcycleTrader,
                MarkName = MarkNameEnums.MotorcyclesSlot5, VehicleClass = VehicleClassEnums.Motocycles,
                PosX = 256.4308f, PosY = -1149.333f, PosZ = 29.0f, Rotation = 176.0f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivMotorcycleTrader,
                MarkName = MarkNameEnums.MotorcyclesSlot6, VehicleClass = VehicleClassEnums.Motocycles,
                PosX = 259.4618f, PosY = -1149.333f, PosZ = 29.0f, Rotation = 176.0f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivMotorcycleTrader,
                MarkName = MarkNameEnums.MotorcyclesSlot7, VehicleClass = VehicleClassEnums.Motocycles,
                PosX = 262.4778f, PosY = -1149.333f, PosZ = 29.0f, Rotation = 176.0f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivMotorcycleTrader,
                MarkName = MarkNameEnums.MotorcyclesSlot8, VehicleClass = VehicleClassEnums.Motocycles,
                PosX = 265.5634f, PosY = -1149.333f, PosZ = 29.0f, Rotation = 176.0f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivMotorcycleTrader,
                MarkName = MarkNameEnums.MotorcyclesSlot9, VehicleClass = VehicleClassEnums.Motocycles,
                PosX = 265.5239f, PosY = -1162.750f, PosZ = 29.0f, Rotation = 358.0f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivMotorcycleTrader,
                MarkName = MarkNameEnums.MotorcyclesSlot10, VehicleClass = VehicleClassEnums.Motocycles,
                PosX = 262.4032f, PosY = -1162.750f, PosZ = 29.0f, Rotation = 358.0f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivMotorcycleTrader,
                MarkName = MarkNameEnums.MotorcyclesSlot11, VehicleClass = VehicleClassEnums.Motocycles,
                PosX = 259.4289f, PosY = -1162.750f, PosZ = 29.0f, Rotation = 358.0f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivMotorcycleTrader,
                MarkName = MarkNameEnums.MotorcyclesSlot12, VehicleClass = VehicleClassEnums.Motocycles,
                PosX = 256.2948f, PosY = -1162.750f, PosZ = 29.0f, Rotation = 358.0f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivMotorcycleTrader,
                MarkName = MarkNameEnums.MotorcyclesSlot13, VehicleClass = VehicleClassEnums.Motocycles,
                PosX = 253.2653f, PosY = -1162.750f, PosZ = 29.0f, Rotation = 358.0f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivMotorcycleTrader,
                MarkName = MarkNameEnums.MotorcyclesSlot14, VehicleClass = VehicleClassEnums.Motocycles,
                PosX = 249.9835f, PosY = -1162.750f, PosZ = 29.0f, Rotation = 358.0f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivMotorcycleTrader,
                MarkName = MarkNameEnums.MotorcyclesSlot15, VehicleClass = VehicleClassEnums.Motocycles,
                PosX = 246.7330f, PosY = -1162.750f, PosZ = 29.0f, Rotation = 358.0f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivMotorcycleTrader,
                MarkName = MarkNameEnums.MotorcyclesSlot16, VehicleClass = VehicleClassEnums.Motocycles,
                PosX = 243.6465f, PosY = -1162.750f, PosZ = 29.0f, Rotation = 358.0f
            },

            // muscle //
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivMuscleTrader,
                MarkName = MarkNameEnums.MuscleSlot1, VehicleClass = VehicleClassEnums.Muscle, PosX = 1814.535f,
                PosY = 4598.15f, PosZ = 37.29554f, Rotation = 181.0983f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivMuscleTrader,
                MarkName = MarkNameEnums.MuscleSlot2, VehicleClass = VehicleClassEnums.Muscle, PosX = 1819.571f,
                PosY = 4593.344f, PosZ = 36.35019f, Rotation = 96.45618f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivMuscleTrader,
                MarkName = MarkNameEnums.MuscleSlot3, VehicleClass = VehicleClassEnums.Muscle, PosX = 1816.625f,
                PosY = 4589.016f, PosZ = 36.30661f, Rotation = 92.66613f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivMuscleTrader,
                MarkName = MarkNameEnums.MuscleSlot4, VehicleClass = VehicleClassEnums.Muscle, PosX = 1805.964f,
                PosY = 4586.327f, PosZ = 36.73016f, Rotation = 186.2767f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivMuscleTrader,
                MarkName = MarkNameEnums.MuscleSlot5, VehicleClass = VehicleClassEnums.Muscle, PosX = 1801.155f,
                PosY = 4585.345f, PosZ = 36.82222f, Rotation = 187.4802f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivMuscleTrader,
                MarkName = MarkNameEnums.MuscleSlot6, VehicleClass = VehicleClassEnums.Muscle, PosX = 1797.509f,
                PosY = 4584.324f, PosZ = 36.90864f, Rotation = 186.2119f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivMuscleTrader,
                MarkName = MarkNameEnums.MuscleSlot7, VehicleClass = VehicleClassEnums.Muscle, PosX = 1793.841f,
                PosY = 4583.789f, PosZ = 37.0381f, Rotation = 182.1559f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivMuscleTrader,
                MarkName = MarkNameEnums.MuscleSlot8, VehicleClass = VehicleClassEnums.Muscle, PosX = 1789.521f,
                PosY = 4583.464f, PosZ = 37.25056f, Rotation = 182.9167f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivMuscleTrader,
                MarkName = MarkNameEnums.MuscleSlot9, VehicleClass = VehicleClassEnums.Muscle, PosX = 1785.136f,
                PosY = 4582.823f, PosZ = 37.38405f, Rotation = 186.4889f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivMuscleTrader,
                MarkName = MarkNameEnums.MuscleSlot10, VehicleClass = VehicleClassEnums.Muscle, PosX = 1780.21f,
                PosY = 4582.403f, PosZ = 37.55471f, Rotation = 184.6425f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivMuscleTrader,
                MarkName = MarkNameEnums.MuscleSlot11, VehicleClass = VehicleClassEnums.Muscle, PosX = 1776.359f,
                PosY = 4590.657f, PosZ = 37.73814f, Rotation = 164.0181f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivMuscleTrader,
                MarkName = MarkNameEnums.MuscleSlot12, VehicleClass = VehicleClassEnums.Muscle, PosX = 1770.765f,
                PosY = 4583.37f, PosZ = 37.66245f, Rotation = 153.351f
            },

            // truck //
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivTruckTrader,
                MarkName = MarkNameEnums.Truck1, VehicleClass = VehicleClassEnums.Commercial, PosX = 144.6574f,
                PosY = 6575.108f, PosZ = 31.8952f, Rotation = 275.0751f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivTruckTrader,
                MarkName = MarkNameEnums.Truck2, VehicleClass = VehicleClassEnums.Commercial, PosX = 139.4596f,
                PosY = 6580.176f, PosZ = 31.96947f, Rotation = 275.4437f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivTruckTrader,
                MarkName = MarkNameEnums.Truck3, VehicleClass = VehicleClassEnums.Commercial, PosX = 134.4107f,
                PosY = 6585.062f, PosZ = 31.96218f, Rotation = 271.3615f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivTruckTrader,
                MarkName = MarkNameEnums.Truck4, VehicleClass = VehicleClassEnums.Commercial, PosX = 129.4364f,
                PosY = 6589.988f, PosZ = 31.93385f, Rotation = 269.685f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivTruckTrader,
                MarkName = MarkNameEnums.Truck5, VehicleClass = VehicleClassEnums.Commercial, PosX = 124.8839f,
                PosY = 6594.551f, PosZ = 31.97029f, Rotation = 270.5749f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivTruckTrader,
                MarkName = MarkNameEnums.Truck6, VehicleClass = VehicleClassEnums.Commercial, PosX = 120.0878f,
                PosY = 6599.346f, PosZ = 32.02137f, Rotation = 271.2421f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivTruckTrader,
                MarkName = MarkNameEnums.Truck7, VehicleClass = VehicleClassEnums.Commercial, PosX = 115.1293f,
                PosY = 6604.384f, PosZ = 31.94759f, Rotation = 271.6902f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivTruckTrader,
                MarkName = MarkNameEnums.Truck8, VehicleClass = VehicleClassEnums.Commercial, PosX = 121.7376f,
                PosY = 6616.155f, PosZ = 31.83957f, Rotation = 311.4768f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivTruckTrader,
                MarkName = MarkNameEnums.Truck9, VehicleClass = VehicleClassEnums.Commercial, PosX = 132.5802f,
                PosY = 6627.045f, PosZ = 31.68776f, Rotation = 314.5264f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivTruckTrader,
                MarkName = MarkNameEnums.Truck10, VehicleClass = VehicleClassEnums.Commercial, PosX = 145.6133f,
                PosY = 6640.504f, PosZ = 31.57346f, Rotation = 221.2995f
            },

            // van //
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivVanTrader, MarkName = MarkNameEnums.Van1,
                VehicleClass = VehicleClassEnums.Vans, PosX = -197.0184f, PosY = 6227.957f, PosZ = 31.49762f,
                Rotation = 227.8901f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivVanTrader, MarkName = MarkNameEnums.Van2,
                VehicleClass = VehicleClassEnums.Vans, PosX = -199.4029f, PosY = 6225.537f, PosZ = 31.49605f,
                Rotation = 223.657f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivVanTrader, MarkName = MarkNameEnums.Van3,
                VehicleClass = VehicleClassEnums.Vans, PosX = -201.6977f, PosY = 6223.228f, PosZ = 31.48974f,
                Rotation = 225.5993f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivVanTrader, MarkName = MarkNameEnums.Van4,
                VehicleClass = VehicleClassEnums.Vans, PosX = -204.0624f, PosY = 6220.919f, PosZ = 31.49072f,
                Rotation = 225.4771f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivVanTrader, MarkName = MarkNameEnums.Van5,
                VehicleClass = VehicleClassEnums.Vans, PosX = -206.3731f, PosY = 6218.4f, PosZ = 31.49071f,
                Rotation = 222.4591f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivVanTrader, MarkName = MarkNameEnums.Van6,
                VehicleClass = VehicleClassEnums.Vans, PosX = -238.5583f, PosY = 6196.616f, PosZ = 31.48923f,
                Rotation = 134.1022f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivVanTrader, MarkName = MarkNameEnums.Van7,
                VehicleClass = VehicleClassEnums.Vans, PosX = -241.1739f, PosY = 6198.914f, PosZ = 31.48923f,
                Rotation = 136.1356f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivVanTrader, MarkName = MarkNameEnums.Van8,
                VehicleClass = VehicleClassEnums.Vans, PosX = -243.5138f, PosY = 6201.196f, PosZ = 31.48923f,
                Rotation = 134.988f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivVanTrader, MarkName = MarkNameEnums.Van9,
                VehicleClass = VehicleClassEnums.Vans, PosX = -245.8029f, PosY = 6203.558f, PosZ = 31.48923f,
                Rotation = 134.0055f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivVanTrader, MarkName = MarkNameEnums.Van10,
                VehicleClass = VehicleClassEnums.Vans, PosX = -248.1428f, PosY = 6205.905f, PosZ = 31.48923f,
                Rotation = 135.3993f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivVanTrader, MarkName = MarkNameEnums.Van11,
                VehicleClass = VehicleClassEnums.Vans, PosX = -250.809f, PosY = 6208.597f, PosZ = 31.48923f,
                Rotation = 135.183f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivVanTrader, MarkName = MarkNameEnums.Van12,
                VehicleClass = VehicleClassEnums.Vans, PosX = -253.8869f, PosY = 6211.506f, PosZ = 31.48923f,
                Rotation = 133.9137f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivVanTrader, MarkName = MarkNameEnums.Van13,
                VehicleClass = VehicleClassEnums.Vans, PosX = -222.9191f, PosY = 6193.167f, PosZ = 31.49004f,
                Rotation = 225.0832f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivVanTrader, MarkName = MarkNameEnums.Van14,
                VehicleClass = VehicleClassEnums.Vans, PosX = -218.2878f, PosY = 6198.114f, PosZ = 31.49004f,
                Rotation = 229.9864f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivVanTrader, MarkName = MarkNameEnums.Van15,
                VehicleClass = VehicleClassEnums.Vans, PosX = -215.103f, PosY = 6201.612f, PosZ = 31.49005f,
                Rotation = 224.6658f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivVanTrader, MarkName = MarkNameEnums.Van16,
                VehicleClass = VehicleClassEnums.Vans, PosX = -210.7107f, PosY = 6205.957f, PosZ = 31.48996f,
                Rotation = 223.887f
            },

            // sedan //
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivSedanTrader,
                MarkName = MarkNameEnums.Sedan1, VehicleClass = VehicleClassEnums.Sedans, PosX = 2553.957f,
                PosY = 2616.323f, PosZ = 37.94482f, Rotation = 16.5414f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivSedanTrader,
                MarkName = MarkNameEnums.Sedan2, VehicleClass = VehicleClassEnums.Sedans, PosX = 2549.778f,
                PosY = 2615.542f, PosZ = 37.94482f, Rotation = 17.29775f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivSedanTrader,
                MarkName = MarkNameEnums.Sedan3, VehicleClass = VehicleClassEnums.Sedans, PosX = 2546.729f,
                PosY = 2614.747f, PosZ = 37.94482f, Rotation = 20.49651f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivSedanTrader,
                MarkName = MarkNameEnums.Sedan4, VehicleClass = VehicleClassEnums.Sedans, PosX = 2543.645f,
                PosY = 2613.848f, PosZ = 37.94482f, Rotation = 21.442f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivSedanTrader,
                MarkName = MarkNameEnums.Sedan5, VehicleClass = VehicleClassEnums.Sedans, PosX = 2540.647f,
                PosY = 2612.649f, PosZ = 37.94482f, Rotation = 20.62823f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivSedanTrader,
                MarkName = MarkNameEnums.Sedan6, VehicleClass = VehicleClassEnums.Sedans, PosX = 2537.329f,
                PosY = 2611.053f, PosZ = 37.95742f, Rotation = 21.29649f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivSedanTrader,
                MarkName = MarkNameEnums.Sedan7, VehicleClass = VehicleClassEnums.Sedans, PosX = 2524.089f,
                PosY = 2609.242f, PosZ = 37.94487f, Rotation = 280.0576f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivSedanTrader,
                MarkName = MarkNameEnums.Sedan8, VehicleClass = VehicleClassEnums.Sedans, PosX = 2532.646f,
                PosY = 2605.79f, PosZ = 37.94487f, Rotation = 334.3681f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivSedanTrader,
                MarkName = MarkNameEnums.Sedan9, VehicleClass = VehicleClassEnums.Sedans, PosX = 2524.356f,
                PosY = 2623.914f, PosZ = 37.94487f, Rotation = 268.3321f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivSedanTrader,
                MarkName = MarkNameEnums.Sedan10, VehicleClass = VehicleClassEnums.Sedans, PosX = 2525.361f,
                PosY = 2628.04f, PosZ = 37.94487f, Rotation = 261.0363f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivSedanTrader,
                MarkName = MarkNameEnums.Sedan11, VehicleClass = VehicleClassEnums.Sedans, PosX = 2526.875f,
                PosY = 2632.22f, PosZ = 37.94487f, Rotation = 257.6385f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivSedanTrader,
                MarkName = MarkNameEnums.Sedan12, VehicleClass = VehicleClassEnums.Sedans, PosX = 2528.427f,
                PosY = 2636.196f, PosZ = 37.95068f, Rotation = 250.3237f
            },

            // offroad //
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivOffRoadTrader,
                MarkName = MarkNameEnums.Offroad1, VehicleClass = VehicleClassEnums.OffRoad, PosX = 1215.483f,
                PosY = 2716.835f, PosZ = 38.00574f, Rotation = 274.0728f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivOffRoadTrader,
                MarkName = MarkNameEnums.Offroad2, VehicleClass = VehicleClassEnums.OffRoad, PosX = 1215.418f,
                PosY = 2711.736f, PosZ = 38.0058f, Rotation = 267.1454f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivOffRoadTrader,
                MarkName = MarkNameEnums.Offroad3, VehicleClass = VehicleClassEnums.OffRoad, PosX = 1214.86f,
                PosY = 2703.188f, PosZ = 38.00614f, Rotation = 273.8523f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivOffRoadTrader,
                MarkName = MarkNameEnums.Offroad4, VehicleClass = VehicleClassEnums.OffRoad, PosX = 1214.845f,
                PosY = 2698.257f, PosZ = 38.00617f, Rotation = 268.1841f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivOffRoadTrader,
                MarkName = MarkNameEnums.Offroad5, VehicleClass = VehicleClassEnums.OffRoad, PosX = 1230.487f,
                PosY = 2700.542f, PosZ = 38.00631f, Rotation = 270.2701f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivOffRoadTrader,
                MarkName = MarkNameEnums.Offroad6, VehicleClass = VehicleClassEnums.OffRoad, PosX = 1230.708f,
                PosY = 2705.649f, PosZ = 38.00593f, Rotation = 275.1225f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivOffRoadTrader,
                MarkName = MarkNameEnums.Offroad7, VehicleClass = VehicleClassEnums.OffRoad, PosX = 1231.656f,
                PosY = 2710.755f, PosZ = 38.00579f, Rotation = 278.7896f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivOffRoadTrader,
                MarkName = MarkNameEnums.Offroad8, VehicleClass = VehicleClassEnums.OffRoad, PosX = 1231.333f,
                PosY = 2716.328f, PosZ = 38.00579f, Rotation = 278.6997f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivOffRoadTrader,
                MarkName = MarkNameEnums.Offroad9, VehicleClass = VehicleClassEnums.OffRoad, PosX = 1245.602f,
                PosY = 2718.537f, PosZ = 38.00525f, Rotation = 105.517f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivOffRoadTrader,
                MarkName = MarkNameEnums.Offroad10, VehicleClass = VehicleClassEnums.OffRoad, PosX = 1245.82f,
                PosY = 2714.946f, PosZ = 38.00579f, Rotation = 102.7659f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivOffRoadTrader,
                MarkName = MarkNameEnums.Offroad11, VehicleClass = VehicleClassEnums.OffRoad, PosX = 1246.538f,
                PosY = 2711.34f, PosZ = 38.00579f, Rotation = 103.8246f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivOffRoadTrader,
                MarkName = MarkNameEnums.Offroad12, VehicleClass = VehicleClassEnums.OffRoad, PosX = 1246.702f,
                PosY = 2707.595f, PosZ = 38.00579f, Rotation = 97.75573f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivOffRoadTrader,
                MarkName = MarkNameEnums.Offroad13, VehicleClass = VehicleClassEnums.OffRoad, PosX = 1212.872f,
                PosY = 2722.828f, PosZ = 38.00413f, Rotation = 179.3235f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivOffRoadTrader,
                MarkName = MarkNameEnums.Offroad14, VehicleClass = VehicleClassEnums.OffRoad, PosX = 1208.49f,
                PosY = 2722.533f, PosZ = 38.00413f, Rotation = 178.4108f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivOffRoadTrader,
                MarkName = MarkNameEnums.Offroad15, VehicleClass = VehicleClassEnums.OffRoad, PosX = 1206.538f,
                PosY = 2715.064f, PosZ = 38.00431f, Rotation = 173.7802f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivOffRoadTrader,
                MarkName = MarkNameEnums.Offroad16, VehicleClass = VehicleClassEnums.OffRoad, PosX = 1206.527f,
                PosY = 2706.725f, PosZ = 38.00477f, Rotation = 180.9031f
            },

            // boat //
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivBoatTrader, MarkName = MarkNameEnums.Boat1,
                VehicleClass = VehicleClassEnums.Boats, PosX = -921.4083f, PosY = -1372.566f, PosZ = 0.0f,
                Rotation = 200.0f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivBoatTrader, MarkName = MarkNameEnums.Boat2,
                VehicleClass = VehicleClassEnums.Boats, PosX = -930.2149f, PosY = -1375.563f, PosZ = 0.0f,
                Rotation = 200.0f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivBoatTrader, MarkName = MarkNameEnums.Boat3,
                VehicleClass = VehicleClassEnums.Boats, PosX = -938.5382f, PosY = -1378.365f, PosZ = 0.0f,
                Rotation = 200.0f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivBoatTrader, MarkName = MarkNameEnums.Boat4,
                VehicleClass = VehicleClassEnums.Boats, PosX = -946.9828f, PosY = -1381.568f, PosZ = 0.0f,
                Rotation = 200.0f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivBoatTrader, MarkName = MarkNameEnums.Boat5,
                VehicleClass = VehicleClassEnums.Boats, PosX = -955.1397f, PosY = -1385.217f, PosZ = 0.0f,
                Rotation = 200.0f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivBoatTrader, MarkName = MarkNameEnums.Boat6,
                VehicleClass = VehicleClassEnums.Boats, PosX = -965.3807f, PosY = -1388.357f, PosZ = 0.0f,
                Rotation = 200.0f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivBoatTrader, MarkName = MarkNameEnums.Boat7,
                VehicleClass = VehicleClassEnums.Boats, PosX = -973.4476f, PosY = -1392.088f, PosZ = 0.0f,
                Rotation = 200.0f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivBoatTrader, MarkName = MarkNameEnums.Boat8,
                VehicleClass = VehicleClassEnums.Boats, PosX = -982.4448f, PosY = -1395.09f, PosZ = 0.0f,
                Rotation = 200.0f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivBoatTrader, MarkName = MarkNameEnums.Boat9,
                VehicleClass = VehicleClassEnums.Boats, PosX = -990.6974f, PosY = -1398.12f, PosZ = 0.0f,
                Rotation = 200.0f
            },
            new PositionModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivBoatTrader, MarkName = MarkNameEnums.Boat10,
                VehicleClass = VehicleClassEnums.Boats, PosX = -999.0239f, PosY = -1401.352f, PosZ = 0.0f,
                Rotation = 200.0f
            }

            //// planes //
            //new PositionModel{ MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivPlaneTrader, MarkName = MarkNameEnums.Plane1, VehicleClass = VehicleClassEnums.Planes, PosX = -967.1655f, PosY = -2954.234f, PosZ = 13.94508f, Rotation = 148.9821f },
            //new PositionModel{ MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivPlaneTrader, MarkName = MarkNameEnums.Plane2, VehicleClass = VehicleClassEnums.Planes, PosX = -947.9687f, PosY = -2985.318f, PosZ = 13.94507f, Rotation = 107.7778f },
            //new PositionModel{ MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivPlaneTrader, MarkName = MarkNameEnums.Plane3, VehicleClass = VehicleClassEnums.Planes, PosX = -965.1079f, PosY = -3023.082f, PosZ = 13.94507f, Rotation = 41.34464f }
            
            // helicopter //
            //new PositionModel{ MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivHelicopterTrader, MarkName = MarkNameEnums.Helicopter1, VehicleClass = VehicleClassEnums.Helicopters, PosX = 2130.086f, PosY = 4808.774f, PosZ = 41.19597f, Rotation = 205.6332f },
            //new PositionModel{ MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivHelicopterTrader, MarkName = MarkNameEnums.Helicopter2, VehicleClass = VehicleClassEnums.Helicopters, PosX = 2117.157f, PosY = 4802.807f, PosZ = 41.19595f, Rotation = 199.9902f },
            //new PositionModel{ MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivHelicopterTrader, MarkName = MarkNameEnums.Helicopter3, VehicleClass = VehicleClassEnums.Helicopters, PosX = 2104.261f, PosY = 4796.729f, PosZ = 41.06274f, Rotation = 203.0137f },
            //new PositionModel{ MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivHelicopterTrader, MarkName = MarkNameEnums.Helicopter4, VehicleClass = VehicleClassEnums.Helicopters, PosX = 2091.295f, PosY = 4790.774f, PosZ = 41.06044f, Rotation = 202.6492f },
            //new PositionModel{ MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivHelicopterTrader, MarkName = MarkNameEnums.Helicopter5, VehicleClass = VehicleClassEnums.Helicopters, PosX = 2078.39f, PosY = 4784.654f, PosZ = 41.06044f, Rotation = 201.1929f },
            //new PositionModel{ MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivHelicopterTrader, MarkName = MarkNameEnums.Helicopter6, VehicleClass = VehicleClassEnums.Helicopters, PosX = 2065.49f, PosY = 4778.603f, PosZ = 41.06044f, Rotation = 203.1264f },
            //new PositionModel{ MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivHelicopterTrader, MarkName = MarkNameEnums.Helicopter7, VehicleClass = VehicleClassEnums.Helicopters, PosX = 2051.811f, PosY = 4772.154f, PosZ = 41.06044f, Rotation = 204.5126f },
            //new PositionModel{ MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivHelicopterTrader, MarkName = MarkNameEnums.Helicopter8, VehicleClass = VehicleClassEnums.Helicopters, PosX = 2038.773f, PosY = 4765.965f, PosZ = 41.06044f, Rotation = 207.138f },
            //new PositionModel{ MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivHelicopterTrader, MarkName = MarkNameEnums.Helicopter9, VehicleClass = VehicleClassEnums.Helicopters, PosX = 2025.881f, PosY = 4759.942f, PosZ = 41.06044f, Rotation = 201.9489f },
            //new PositionModel{ MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivHelicopterTrader, MarkName = MarkNameEnums.Helicopter10, VehicleClass = VehicleClassEnums.Helicopters, PosX = 2012.369f, PosY = 4753.699f, PosZ = 41.06044f, Rotation = 206.2341f },
            //new PositionModel{ MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivHelicopterTrader, MarkName = MarkNameEnums.Helicopter11, VehicleClass = VehicleClassEnums.Helicopters, PosX = 1999.442f, PosY = 4747.302f, PosZ = 41.06044f, Rotation = 201.0599f },
            //new PositionModel{ MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivHelicopterTrader, MarkName = MarkNameEnums.Helicopter12, VehicleClass = VehicleClassEnums.Helicopters, PosX = 1985.695f, PosY = 4740.699f, PosZ = 41.06044f, Rotation = 203.6822f },
        };
    }
}