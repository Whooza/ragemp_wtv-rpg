﻿using System;

namespace RolePlayGameData.Configs.Vehicle
{
    public class VehFuelCfg
    {
        private static readonly float ElectricPrice = 0.89f;
        private static readonly float DieselPrice = 1.19f;
        private static readonly float PetrolPrice = 1.92f;
        private readonly DateTime _dateTime;

        public VehFuelCfg() => _dateTime = DateTime.Now;

        public float GetElectricPrice() => ElectricPrice + (_dateTime.Hour / 100f) - (_dateTime.Minute / 400f);
        public float GetDieselPrice() => DieselPrice + (_dateTime.Hour / 75f) - (_dateTime.Minute / 300f);
        public float GetPetrolPrice() => PetrolPrice + (_dateTime.Hour / 50f) - (_dateTime.Minute / 200f);
    }
}