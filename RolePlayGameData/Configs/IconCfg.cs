﻿using RolePlayGameData.Enums;
using System.Collections.Generic;

namespace RolePlayGameData.Configs
{
    public class IconCfg
    {
        public static readonly SortedDictionary<ItemTypeEnums, string> IconsConfig
            = new SortedDictionary<ItemTypeEnums, string>
            {
                { ItemTypeEnums.Food, "fa-utensils" },
                { ItemTypeEnums.SoftDrink, "fa-glass-whiskey" },
                { ItemTypeEnums.Drink, "fa-cocktail" },
                { ItemTypeEnums.Ingredient, "fa-box" },
                { ItemTypeEnums.Tool, "fa-tools" },
                { ItemTypeEnums.Medical, "fa-medkit" },
                { ItemTypeEnums.Technical, "fa-briefcase" },
                { ItemTypeEnums.Smoke, "fa-smoking" },
                { ItemTypeEnums.Drug, "fa-cannabis" },
                { ItemTypeEnums.JobItem, "fa-cube" },
                { ItemTypeEnums.CrateItem, "fa-box-open" },
                { ItemTypeEnums.Ammo, "fa-box-open" }, // TODO change icon
                { ItemTypeEnums.Weapon, "fa-box-open" }, // TODO change icon
                { ItemTypeEnums.MissionItem, "fa-suitcase" },
            };
    }
}
