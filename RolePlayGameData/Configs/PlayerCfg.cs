﻿namespace RolePlayGameData.Configs
{
    public class PlayerCfg
    {
        public const float Hunger = 1.21f;
        public const float Thirst = 1.49f;

        public const float SocialSalary = 489.55f;
        public const float LscSalary = 1798.32f;
        public const float LsrsSalary = 1804.17f;
        public const float LspdSalary = 1895.55f;
        public const float FibSalary = 1998.71f;
        public const float LssdSalary = 1991.85f;
        public const float DojSalary = 2048.26f;
        public const float DcrSalary = 1989.61f;
    }
}