﻿using System.Collections.Generic;

namespace RolePlayGameData.Configs
{
    internal class WhiteListCfg
    {
        public static readonly List<string> DefaultWhitelistEntries = new List<string>
        {
            "xXWhoozaXx",
            "Boeny85"
        };
    }
}