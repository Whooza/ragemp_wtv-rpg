﻿using System.Collections.Generic;
using RolePlayGameData.Enums;
using RolePlayGameData.Models;

namespace RolePlayGameData.Configs
{
    public static class ProcessorCfg
    {
        public static readonly uint[] Processors =
        {
            (uint) MarkTypeEnums.Process_Butcher,
            (uint) MarkTypeEnums.Process_Carpenter,
            (uint) MarkTypeEnums.Process_CementFactory,
            (uint) MarkTypeEnums.Process_Dairy,
            (uint) MarkTypeEnums.Process_Distillery,
            (uint) MarkTypeEnums.Process_DrugFactory,
            (uint) MarkTypeEnums.Process_FlourMill,
            (uint) MarkTypeEnums.Process_Forge,
            (uint) MarkTypeEnums.Process_Glassworks,
            (uint) MarkTypeEnums.Process_JuicePress,
            (uint) MarkTypeEnums.Process_Melt,
            (uint) MarkTypeEnums.Process_PaperFactory,
            (uint) MarkTypeEnums.Process_RollingMill,
            (uint) MarkTypeEnums.Process_Sawmill,
            (uint) MarkTypeEnums.Process_StoneMill,
            (uint) MarkTypeEnums.Process_WalnutProcessing
        };

        public static readonly List<JobRecipeModel> ProcessingRecipes = new List<JobRecipeModel>
        {
            // #-Butcher-#
            new JobRecipeModel(MarkTypeEnums.Process_Butcher, 0, 0, 0)
            {
                RecipeName = "Huhn zerlegen", RandomProcess = false, MinRndAmount = 1, MaxRndAmount = 1,
                ProcessTime = 30000,
                MatsList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.Chicken, 1}},
                ProcessedList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.ChickenMeat, 4}}
            },
            new JobRecipeModel(MarkTypeEnums.Process_Butcher, 1, 50, 10000)
            {
                RecipeName = "Schwein zerlegen", RandomProcess = false, MinRndAmount = 1, MaxRndAmount = 1,
                ProcessTime = 30000,
                MatsList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.Pig, 1}},
                ProcessedList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.Pork, 6}}
            },
            new JobRecipeModel(MarkTypeEnums.Process_Butcher, 2, 100, 25000)
            {
                RecipeName = "Rind zerlegen", RandomProcess = false, MinRndAmount = 1, MaxRndAmount = 1,
                ProcessTime = 30000,
                MatsList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.Cow, 1}},
                ProcessedList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.CattleMeat, 8}}
            },

            // #-Carpenter-#
            new JobRecipeModel(MarkTypeEnums.Process_Carpenter, 0, 0, 0)
            {
                RecipeName = "Regal bauen", RandomProcess = false, MinRndAmount = 1, MaxRndAmount = 1,
                ProcessTime = 30000,
                MatsList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.WoodenBoards, 5}},
                ProcessedList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.Shelf, 1}}
            },
            new JobRecipeModel(MarkTypeEnums.Process_Carpenter, 1, 50, 10000)
            {
                RecipeName = "Stuhl bauen", RandomProcess = false, MinRndAmount = 1, MaxRndAmount = 1,
                ProcessTime = 30000,
                MatsList = new Dictionary<ItemNameEnums, int>
                    {{ItemNameEnums.WoodenBoards, 3}, {ItemNameEnums.WoodenSlats, 2}},
                ProcessedList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.Chair, 1}}
            },
            new JobRecipeModel(MarkTypeEnums.Process_Carpenter, 2, 100, 25000)
            {
                RecipeName = "Tisch bauen", RandomProcess = false, MinRndAmount = 1, MaxRndAmount = 1,
                ProcessTime = 30000,
                MatsList = new Dictionary<ItemNameEnums, int>
                    {{ItemNameEnums.WoodenBoards, 5}, {ItemNameEnums.WoodenBeam, 2}, {ItemNameEnums.WoodenSlats, 2}},
                ProcessedList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.Table, 1}}
            },

            // #-CementFactory-#
            new JobRecipeModel(MarkTypeEnums.Process_CementFactory, 0, 0, 0)
            {
                RecipeName = "Zement mischen", RandomProcess = false, MinRndAmount = 1, MaxRndAmount = 1,
                ProcessTime = 30000,
                MatsList = new Dictionary<ItemNameEnums, int>
                    {{ItemNameEnums.BucketGravel, 10}, {ItemNameEnums.BucketSand, 5}},
                ProcessedList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.Cement, 1}}
            },

            // #-JuicePress-#
            new JobRecipeModel(MarkTypeEnums.Process_JuicePress, 0, 0, 0)
            {
                RecipeName = "Apfelsaft pressen", RandomProcess = false, MinRndAmount = 1, MaxRndAmount = 1,
                ProcessTime = 30000,
                MatsList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.AppleCrate, 1}},
                ProcessedList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.AppleJuice, 2}}
            },
            new JobRecipeModel(MarkTypeEnums.Process_JuicePress, 1, 50, 10000)
            {
                RecipeName = "Orangensaft pressen", RandomProcess = false, MinRndAmount = 1, MaxRndAmount = 1,
                ProcessTime = 30000,
                MatsList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.OrangeSack, 1}},
                ProcessedList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.OrangeJuice, 1}}
            },
            new JobRecipeModel(MarkTypeEnums.Process_JuicePress, 2, 100, 25000)
            {
                RecipeName = "Tomatensaft pressen", RandomProcess = false, MinRndAmount = 1, MaxRndAmount = 1,
                ProcessTime = 30000,
                MatsList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.TomatoCrate, 1}},
                ProcessedList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.TomatoJuice, 2}}
            },

            // #-Dairy-#
            new JobRecipeModel(MarkTypeEnums.Process_Dairy, 0, 0, 0)
            {
                RecipeName = "Milch herstellen", RandomProcess = false, MinRndAmount = 1, MaxRndAmount = 1,
                ProcessTime = 30000,
                MatsList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.RawMilk, 2}},
                ProcessedList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.Milk, 1}}
            },
            new JobRecipeModel(MarkTypeEnums.Process_Dairy, 0, 50, 10000)
            {
                RecipeName = "Butter herstellen", RandomProcess = false, MinRndAmount = 1, MaxRndAmount = 1,
                ProcessTime = 30000,
                MatsList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.RawMilk, 3}},
                ProcessedList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.ContainerButter, 2}}
            },
            new JobRecipeModel(MarkTypeEnums.Process_Dairy, 0, 100, 25000)
            {
                RecipeName = "Sahne herstellen", RandomProcess = false, MinRndAmount = 1, MaxRndAmount = 1,
                ProcessTime = 30000,
                MatsList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.RawMilk, 2}},
                ProcessedList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.ContainerCream, 1}}
            },

            // #-Distillery-#
            new JobRecipeModel(MarkTypeEnums.Process_Distillery, 0, 0, 0)
            {
                RecipeName = "Schnaps brennen", RandomProcess = false, MinRndAmount = 1, MaxRndAmount = 1,
                ProcessTime = 15000,
                MatsList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.PotatoSack, 1}},
                ProcessedList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.PotatoSchnapps, 1}}
            },
            new JobRecipeModel(MarkTypeEnums.Process_Distillery, 1, 50, 10000)
            {
                RecipeName = "Wein herstellen", RandomProcess = false, MinRndAmount = 1, MaxRndAmount = 1,
                ProcessTime = 15000,
                MatsList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.GrapevineCrate, 1}},
                ProcessedList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.Wine, 1}}
            },
            new JobRecipeModel(MarkTypeEnums.Process_Distillery, 1, 100, 25000)
            {
                RecipeName = "Fruchtlikör herstellen", RandomProcess = false, MinRndAmount = 1, MaxRndAmount = 1,
                ProcessTime = 30000,
                MatsList = new Dictionary<ItemNameEnums, int>
                {
                    {ItemNameEnums.PotatoSack, 1}, {ItemNameEnums.GrapevineCrate, 1}, {ItemNameEnums.OrangeSack, 1},
                    {ItemNameEnums.LemonSack, 1}
                },
                ProcessedList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.Cordial, 2}}
            },

            // #-DrugFactory-#
            new JobRecipeModel(MarkTypeEnums.Process_DrugFactory, 0, 0, 0)
            {
                RecipeName = "LSD kochen", RandomProcess = false, MinRndAmount = 1, MaxRndAmount = 1,
                ProcessTime = 30000,
                MatsList = new Dictionary<ItemNameEnums, int>
                    {{ItemNameEnums.LysergicAcid, 1}, {ItemNameEnums.Ergot, 1}},
                ProcessedList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.Lsd, 1}}
            },
            new JobRecipeModel(MarkTypeEnums.Process_DrugFactory, 1, 50, 50000)
            {
                RecipeName = "Ecstasy kochen", RandomProcess = false, MinRndAmount = 1, MaxRndAmount = 1,
                ProcessTime = 30000,
                MatsList = new Dictionary<ItemNameEnums, int>
                {
                    {ItemNameEnums.Methylbenzodioxolbutanamin, 1}, {ItemNameEnums.Methylendioxyethylamphetamin, 1},
                    {ItemNameEnums.Methylendioxymethampheamin, 1}
                },
                ProcessedList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.Ecstasy, 1}}
            },
            new JobRecipeModel(MarkTypeEnums.Process_DrugFactory, 2, 100, 275000)
            {
                RecipeName = "CrystalMeth kochen", RandomProcess = false, MinRndAmount = 1, MaxRndAmount = 1,
                ProcessTime = 30000,
                MatsList = new Dictionary<ItemNameEnums, int>
                    {{ItemNameEnums.Methylamine, 1}, {ItemNameEnums.Phenylacetone, 1}, {ItemNameEnums.Ephedrine, 1}},
                ProcessedList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.CrystalMeth, 1}}
            },
            new JobRecipeModel(MarkTypeEnums.Process_DrugFactory, 3, 250, 500000)
            {
                RecipeName = "Kokain kochen", RandomProcess = false, MinRndAmount = 1, MaxRndAmount = 1,
                ProcessTime = 30000,
                MatsList = new Dictionary<ItemNameEnums, int>
                {
                    {ItemNameEnums.Benzoylchloride, 1}, {ItemNameEnums.CocaineLeaves, 3}, {ItemNameEnums.Solvent, 1},
                    {ItemNameEnums.Methanol, 1}
                },
                ProcessedList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.Cocaine, 1}}
            },

            // #-FlourMill-#
            new JobRecipeModel(MarkTypeEnums.Process_FlourMill, 0, 0, 0)
            {
                RecipeName = "Mehl mühlen", RandomProcess = false, MinRndAmount = 1, MaxRndAmount = 1,
                ProcessTime = 30000,
                MatsList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.Wheat, 10}},
                ProcessedList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.FlourSack, 1}}
            },

            // #-Forge-#
            new JobRecipeModel(MarkTypeEnums.Process_Forge, 0, 0, 0)
            {
                RecipeName = "Test", RandomProcess = false, MinRndAmount = 1, MaxRndAmount = 1, ProcessTime = 30000,
                MatsList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.SilverIngot, 2}},
                ProcessedList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.SilverCutlery, 1}}
            },
            new JobRecipeModel(MarkTypeEnums.Process_Forge, 1, 50, 50000)
            {
                RecipeName = "Test", RandomProcess = false, MinRndAmount = 1, MaxRndAmount = 1, ProcessTime = 30000,
                MatsList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.GoldIngot, 1}},
                ProcessedList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.GoldCoins, 1}}
            },
            new JobRecipeModel(MarkTypeEnums.Process_Forge, 2, 150, 120000)
            {
                RecipeName = "Test", RandomProcess = false, MinRndAmount = 1, MaxRndAmount = 1, ProcessTime = 60000,
                MatsList = new Dictionary<ItemNameEnums, int>
                    {{ItemNameEnums.GoldIngot, 2}, {ItemNameEnums.SilverIngot, 1}},
                ProcessedList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.GoldChain, 1}}
            },
            new JobRecipeModel(MarkTypeEnums.Process_Forge, 3, 300, 1000000)
            {
                RecipeName = "Test", RandomProcess = false, MinRndAmount = 1, MaxRndAmount = 1, ProcessTime = 60000,
                MatsList = new Dictionary<ItemNameEnums, int>
                    {{ItemNameEnums.GoldIngot, 1}, {ItemNameEnums.RoughDiamond, 1}},
                ProcessedList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.DiamondRing, 1}}
            },

            // #-Glassworks-#
            new JobRecipeModel(MarkTypeEnums.Process_Glassworks, 0, 0, 0)
            {
                RecipeName = "Test", RandomProcess = false, MinRndAmount = 1, MaxRndAmount = 1, ProcessTime = 30000,
                MatsList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.BucketSand, 10}},
                ProcessedList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.Pane, 1}}
            },

            // #-Melt-#
            new JobRecipeModel(MarkTypeEnums.Process_Melt, 0, 0, 0)
            {
                RecipeName = "Eisenbarren schmelzen", RandomProcess = false, MinRndAmount = 1, MaxRndAmount = 1,
                ProcessTime = 30000,
                MatsList = new Dictionary<ItemNameEnums, int>
                    {{ItemNameEnums.IronFlakes, 8}, {ItemNameEnums.HardCoal, 2}},
                ProcessedList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.IronIngot, 1}}
            },
            new JobRecipeModel(MarkTypeEnums.Process_Melt, 1, 25, 2500)
            {
                RecipeName = "Kupferbarren schmelzen", RandomProcess = false, MinRndAmount = 1, MaxRndAmount = 1,
                ProcessTime = 30000,
                MatsList = new Dictionary<ItemNameEnums, int>
                    {{ItemNameEnums.CopperFlakes, 6}, {ItemNameEnums.HardCoal, 2}},
                ProcessedList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.CopperIngot, 1}}
            },
            new JobRecipeModel(MarkTypeEnums.Process_Melt, 2, 50, 10000)
            {
                RecipeName = "Silberbarren schmelzen", RandomProcess = false, MinRndAmount = 1, MaxRndAmount = 1,
                ProcessTime = 30000,
                MatsList = new Dictionary<ItemNameEnums, int>
                    {{ItemNameEnums.SilverFlakes, 4}, {ItemNameEnums.HardCoal, 2}},
                ProcessedList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.SilverIngot, 1}}
            },
            new JobRecipeModel(MarkTypeEnums.Process_Melt, 3, 100, 250000)
            {
                RecipeName = "Goldbarren schmelzen", RandomProcess = false, MinRndAmount = 1, MaxRndAmount = 1,
                ProcessTime = 30000,
                MatsList = new Dictionary<ItemNameEnums, int>
                    {{ItemNameEnums.GoldFlakes, 4}, {ItemNameEnums.HardCoal, 2}},
                ProcessedList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.GoldIngot, 1}}
            },

            // #-PaperFactory-#
            new JobRecipeModel(MarkTypeEnums.Process_PaperFactory, 0, 0, 0)
            {
                RecipeName = "Pappe herstellen", RandomProcess = false, MinRndAmount = 1, MaxRndAmount = 1,
                ProcessTime = 10000,
                MatsList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.Sawdust, 1}},
                ProcessedList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.CardBoard, 5}}
            },
            new JobRecipeModel(MarkTypeEnums.Process_PaperFactory, 1, 25, 2500)
            {
                RecipeName = "Papier hestellen", RandomProcess = false, MinRndAmount = 1, MaxRndAmount = 1,
                ProcessTime = 10000,
                MatsList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.Sawdust, 1}},
                ProcessedList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.Paper, 10}}
            },

            // #-RollingMill-#
            new JobRecipeModel(MarkTypeEnums.Process_RollingMill, 0, 0, 0)
            {
                RecipeName = "Eisenpaltte herstellen", RandomProcess = false, MinRndAmount = 1, MaxRndAmount = 1,
                ProcessTime = 30000,
                MatsList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.IronIngot, 2}},
                ProcessedList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.IronPlate, 1}}
            },
            new JobRecipeModel(MarkTypeEnums.Process_RollingMill, 1, 25, 25000)
            {
                RecipeName = "Kupferkabel herstellen", RandomProcess = false, MinRndAmount = 1, MaxRndAmount = 1,
                ProcessTime = 30000,
                MatsList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.CopperIngot, 2}},
                ProcessedList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.CopperWire, 4}}
            },
            new JobRecipeModel(MarkTypeEnums.Process_RollingMill, 2, 50, 35000)
            {
                RecipeName = "Eisenrohr herstellen", RandomProcess = false, MinRndAmount = 1, MaxRndAmount = 1,
                ProcessTime = 30000,
                MatsList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.IronIngot, 2}},
                ProcessedList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.IronPipe, 2}}
            },

            // #-Sawmill-#
            new JobRecipeModel(MarkTypeEnums.Process_Sawmill, 0, 0, 0)
            {
                RecipeName = "Bretter zusägen", RandomProcess = false, MinRndAmount = 1, MaxRndAmount = 1,
                ProcessTime = 30000,
                MatsList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.Log, 1}},
                ProcessedList = new Dictionary<ItemNameEnums, int>
                    {{ItemNameEnums.Sawdust, 2}, {ItemNameEnums.WoodenBoards, 4}}
            },
            new JobRecipeModel(MarkTypeEnums.Process_Sawmill, 0, 50, 10000)
            {
                RecipeName = "Latten zusägen", RandomProcess = false, MinRndAmount = 1, MaxRndAmount = 1,
                ProcessTime = 30000,
                MatsList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.Log, 1}},
                ProcessedList = new Dictionary<ItemNameEnums, int>
                    {{ItemNameEnums.Sawdust, 2}, {ItemNameEnums.WoodenSlats, 8}}
            },
            new JobRecipeModel(MarkTypeEnums.Process_Sawmill, 0, 100, 25000)
            {
                RecipeName = "Balken zusägen", RandomProcess = false, MinRndAmount = 1, MaxRndAmount = 1,
                ProcessTime = 30000,
                MatsList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.Log, 1}},
                ProcessedList = new Dictionary<ItemNameEnums, int>
                    {{ItemNameEnums.Sawdust, 4}, {ItemNameEnums.WoodenBeam, 1}}
            },

            // #-StoneMill-#
            new JobRecipeModel(MarkTypeEnums.Process_StoneMill, 0, 0, 0)
            {
                RecipeName = "Gestein grob zermalen", RandomProcess = true, MinRndAmount = 1, MaxRndAmount = 2,
                ProcessTime = 30000,
                MatsList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.Rock, 2}},
                ProcessedList = new Dictionary<ItemNameEnums, int>
                    {{ItemNameEnums.BucketGravel, 15}, {ItemNameEnums.HardCoal, 4}, {ItemNameEnums.IronFlakes, 2}}
            },
            new JobRecipeModel(MarkTypeEnums.Process_StoneMill, 1, 50, 10000)
            {
                RecipeName = "Gestein fein zermalen", RandomProcess = true, MinRndAmount = 1, MaxRndAmount = 2,
                ProcessTime = 30000,
                MatsList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.Rock, 2}},
                ProcessedList = new Dictionary<ItemNameEnums, int>
                {
                    {ItemNameEnums.SilverFlakes, 3}, {ItemNameEnums.IronFlakes, 2}, {ItemNameEnums.CopperFlakes, 2},
                    {ItemNameEnums.BucketGravel, 15}
                }
            },
            new JobRecipeModel(MarkTypeEnums.Process_StoneMill, 2, 100, 75000)
            {
                RecipeName = "Gestein feinst zermalen", RandomProcess = true, MinRndAmount = 1, MaxRndAmount = 2,
                ProcessTime = 30000,
                MatsList =
                    new Dictionary<ItemNameEnums, int> {{ItemNameEnums.Rock, 2}, {ItemNameEnums.BucketGravel, 5}},
                ProcessedList = new Dictionary<ItemNameEnums, int>
                {
                    {ItemNameEnums.GoldFlakes, 3}, {ItemNameEnums.SilverFlakes, 2}, {ItemNameEnums.RoughDiamond, 1},
                    {ItemNameEnums.BucketSand, 15}
                }
            },

            // #-WalnutProcessing-#
            new JobRecipeModel(MarkTypeEnums.Process_WalnutProcessing, 0, 0, 0)
            {
                RecipeName = "Walnüsse knacken", RandomProcess = true, MinRndAmount = 1, MaxRndAmount = 3,
                ProcessTime = 30000,
                MatsList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.WalnutsSack, 3}},
                ProcessedList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.WalnutKernelsSack, 1}}
            }
        };
    }
}