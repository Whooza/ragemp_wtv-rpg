﻿using System.Collections.Generic;
using RolePlayGameData.Enums;
using RolePlayGameData.Models;

namespace RolePlayGameData.Configs
{
    public class MarkCfg
    {
        public static List<MarkSettingsModel> LocationMarkSettings = new List<MarkSettingsModel>
        {
            // civ //
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivGouvernment, MarkText = "Regierung",
                HasMapMark = true, HasTextLabel = true, HasWorldMark = true, BlipSprite = 419, BlipScale = 1.5f,
                BlipColor = 1, BlipAlpha = 200, BlipDrawDistance = 1f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f,
                ColShapeRange = 1.1f, ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivCityhall, MarkText = "Rathaus",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 487, BlipScale = 1f,
                BlipColor = 1, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f,
                ColShapeRange = 1.1f, ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivJobcenter, MarkText = "Jobcenter",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 498, BlipScale = 1f,
                BlipColor = 1, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f,
                ColShapeRange = 1.1f, ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivLicenseOffice, MarkText = "Lizenzamt",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 498, BlipScale = 1f,
                BlipColor = 1, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f,
                ColShapeRange = 1.1f, ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivAtm, MarkText = "Geldautomat",
                HasMapMark = false, HasWorldMark = true, HasTextLabel = true, BlipSprite = 374, BlipScale = 1f,
                BlipColor = 24, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f,
                ColShapeRange = 1.1f, ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivBank, MarkText = "Hausbank",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 375, BlipScale = 1f,
                BlipColor = 24, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f,
                ColShapeRange = 1.1f, ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivCentralbank, MarkText = "Zentralbank",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 375, BlipScale = 1f,
                BlipColor = 24, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f,
                ColShapeRange = 1.1f, ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivSmallGarage, MarkText = "KleineGarage",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 357, BlipScale = 1f,
                BlipColor = 15, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f,
                ColShapeRange = 2.5f, ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivMediumGarage, MarkText = "MittlereGarage",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 50, BlipScale = 1f,
                BlipColor = 15, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f,
                ColShapeRange = 2.5f, ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivBigGarage, MarkText = "GroßeGarage",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 473, BlipScale = 1f,
                BlipColor = 15, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f,
                ColShapeRange = 2.5f, ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivFuelpump, MarkText = "Zapfsaeule",
                HasMapMark = false, HasWorldMark = true, HasTextLabel = true, BlipSprite = 361, BlipScale = 1f,
                BlipColor = 10, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f,
                ColShapeRange = 1.1f, ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivFuelstation, MarkText = "Tankstelle",
                HasMapMark = false, HasWorldMark = true, HasTextLabel = true, BlipSprite = 483, BlipScale = 1f,
                BlipColor = 54, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f,
                ColShapeRange = 1.1f, ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivFuelstationShop, MarkText = "24/7-Shop",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 446, BlipScale = 1f,
                BlipColor = 1, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f,
                ColShapeRange = 1.1f, ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivAuctionHouse, MarkText = "Auktionshaus",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 642, BlipScale = 1f,
                BlipColor = 52, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f,
                ColShapeRange = 1.1f, ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivClothingShop, MarkText = "Kleidungsladen",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 73, BlipScale = 1f,
                BlipColor = 52, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f,
                ColShapeRange = 1.1f, ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivHairCutsShop, MarkText = "Frisuer",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 71, BlipScale = 1f,
                BlipColor = 52, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f,
                ColShapeRange = 1.1f, ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivTruckGarage, MarkText = "Truck-Garage",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 71, BlipScale = 1f,
                BlipColor = 52, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f,
                ColShapeRange = 1.1f, ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivTattooShop, MarkText = "Tatoos",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 71, BlipScale = 1f,
                BlipColor = 52, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f,
                ColShapeRange = 1.1f, ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivHatShop, MarkText = "Hutladen",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 102, BlipScale = 1f,
                BlipColor = 52, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f,
                ColShapeRange = 1.1f, ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivSnackShop, MarkText = "Imbiss",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 52, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f,
                ColShapeRange = 1.1f, ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivRestaurant, MarkText = "Restaurant",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 52, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f,
                ColShapeRange = 1.1f, ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivToolShop, MarkText = "Werkzeugladen",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 566, BlipScale = 1f,
                BlipColor = 52, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f,
                ColShapeRange = 1.1f, ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivWeaponShop, MarkText = "Waffenladen",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 567, BlipScale = 1f,
                BlipColor = 52, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f,
                ColShapeRange = 1.1f, ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },

            // civ - vehicle trader //
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivBoatTrader, MarkText = "Bootladen",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 455, BlipScale = 1f,
                BlipColor = 44, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivCompactCoupeTrader,
                MarkText = "Mittelklassewagen", HasMapMark = true, HasWorldMark = true, HasTextLabel = true,
                BlipSprite = 523, BlipScale = 1f, BlipColor = 44, BlipAlpha = 200, BlipDrawDistance = 0f,
                BlipShortRange = true, BlipRotation = 0, MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255,
                MarkerColorGreen = 0, MarkerColorBlue = 0, MarkerColorAlpha = 155, MarkerBlobUpAndDown = false,
                ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f, ColShapeHeight = 1f, TextLabelRange = 50f,
                TextLabelSize = 50f, TextLabelFont = 0, TextLabelColorRed = 255, TextLabelColorGreen = 0,
                TextLabelColorBlue = 0, TextLabelColorAlpha = 155, TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivCycleTrader, MarkText = "Fahradladen",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 512, BlipScale = 1f,
                BlipColor = 44, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivHelicopterTrader,
                MarkText = "Helikopterhandel", HasMapMark = true, HasWorldMark = true, HasTextLabel = true,
                BlipSprite = 574, BlipScale = 1f, BlipColor = 44, BlipAlpha = 200, BlipDrawDistance = 0f,
                BlipShortRange = true, BlipRotation = 0, MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255,
                MarkerColorGreen = 0, MarkerColorBlue = 0, MarkerColorAlpha = 155, MarkerBlobUpAndDown = false,
                ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f, ColShapeHeight = 1f, TextLabelRange = 50f,
                TextLabelSize = 50f, TextLabelFont = 0, TextLabelColorRed = 255, TextLabelColorGreen = 0,
                TextLabelColorBlue = 0, TextLabelColorAlpha = 155, TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivMotorcycleTrader,
                MarkText = "Motorradhandel", HasMapMark = true, HasWorldMark = true, HasTextLabel = true,
                BlipSprite = 661, BlipScale = 1f, BlipColor = 44, BlipAlpha = 200, BlipDrawDistance = 0f,
                BlipShortRange = true, BlipRotation = 0, MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255,
                MarkerColorGreen = 0, MarkerColorBlue = 0, MarkerColorAlpha = 155, MarkerBlobUpAndDown = false,
                ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f, ColShapeHeight = 1f, TextLabelRange = 50f,
                TextLabelSize = 50f, TextLabelFont = 0, TextLabelColorRed = 255, TextLabelColorGreen = 0,
                TextLabelColorBlue = 0, TextLabelColorAlpha = 155, TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivMuscleTrader, MarkText = "Musclecars",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 523, BlipScale = 1f,
                BlipColor = 44, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivOffRoadTrader,
                MarkText = "Offroad-Fahrzeuge", HasMapMark = true, HasWorldMark = true, HasTextLabel = true,
                BlipSprite = 523, BlipScale = 1f, BlipColor = 44, BlipAlpha = 200, BlipDrawDistance = 0f,
                BlipShortRange = true, BlipRotation = 0, MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255,
                MarkerColorGreen = 0, MarkerColorBlue = 0, MarkerColorAlpha = 155, MarkerBlobUpAndDown = false,
                ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f, ColShapeHeight = 1f, TextLabelRange = 50f,
                TextLabelSize = 50f, TextLabelFont = 0, TextLabelColorRed = 255, TextLabelColorGreen = 0,
                TextLabelColorBlue = 0, TextLabelColorAlpha = 155, TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivPlaneTrader, MarkText = "Flugzeughandel",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 575, BlipScale = 1f,
                BlipColor = 44, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivSuvTrader, MarkText = "SUV-Handel",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 523, BlipScale = 1f,
                BlipColor = 44, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivSedanTrader, MarkText = "Limousinen",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 523, BlipScale = 1f,
                BlipColor = 44, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivSportsCarTrader,
                MarkText = "Sportfahrzeughandel", HasMapMark = true, HasWorldMark = true, HasTextLabel = true,
                BlipSprite = 523, BlipScale = 1f, BlipColor = 44, BlipAlpha = 200, BlipDrawDistance = 0f,
                BlipShortRange = true, BlipRotation = 0, MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255,
                MarkerColorGreen = 0, MarkerColorBlue = 0, MarkerColorAlpha = 155, MarkerBlobUpAndDown = false,
                ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f, ColShapeHeight = 1f, TextLabelRange = 50f,
                TextLabelSize = 50f, TextLabelFont = 0, TextLabelColorRed = 255, TextLabelColorGreen = 0,
                TextLabelColorBlue = 0, TextLabelColorAlpha = 155, TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivSuperSportTrader,
                MarkText = "Supersportwagen", HasMapMark = true, HasWorldMark = true, HasTextLabel = true,
                BlipSprite = 523, BlipScale = 1f, BlipColor = 44, BlipAlpha = 200, BlipDrawDistance = 0f,
                BlipShortRange = true, BlipRotation = 0, MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255,
                MarkerColorGreen = 0, MarkerColorBlue = 0, MarkerColorAlpha = 155, MarkerBlobUpAndDown = false,
                ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f, ColShapeHeight = 1f, TextLabelRange = 50f,
                TextLabelSize = 50f, TextLabelFont = 0, TextLabelColorRed = 255, TextLabelColorGreen = 0,
                TextLabelColorBlue = 0, TextLabelColorAlpha = 155, TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivVanTrader, MarkText = "Transporter",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 523, BlipScale = 1f,
                BlipColor = 44, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CivTruckTrader, MarkText = "Lastkraftwagen",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 523, BlipScale = 1f,
                BlipColor = 44, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },

            // lsc //
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Lsc, MarkType = MarkTypeEnums.LscStation, MarkText = "LSC-Station",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 446, BlipScale = 1f,
                BlipColor = 46, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Lsc, MarkType = MarkTypeEnums.LscTimeClock, MarkText = "LSC-Stechuhr",
                HasMapMark = false, HasWorldMark = true, HasTextLabel = true, BlipSprite = 446, BlipScale = 1f,
                BlipColor = 46, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Lsc, MarkType = MarkTypeEnums.LscControlCenter, MarkText = "LSC-Leitstelle",
                HasMapMark = false, HasWorldMark = true, HasTextLabel = true, BlipSprite = 446, BlipScale = 1f,
                BlipColor = 46, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Lsc, MarkType = MarkTypeEnums.LscUniforms, MarkText = "LSC-Uniformen",
                HasMapMark = false, HasWorldMark = true, HasTextLabel = true, BlipSprite = 175, BlipScale = 1f,
                BlipColor = 46, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Lsc, MarkType = MarkTypeEnums.LscCanteen, MarkText = "LSC-Kantine",
                HasMapMark = false, HasWorldMark = true, HasTextLabel = true, BlipSprite = 515, BlipScale = 1f,
                BlipColor = 46, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Lsc, MarkType = MarkTypeEnums.LscTools, MarkText = "LSC-Werkzeuge",
                HasMapMark = false, HasWorldMark = true, HasTextLabel = true, BlipSprite = 566, BlipScale = 1f,
                BlipColor = 46, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },

            // lsrs //
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Lsrs, MarkType = MarkTypeEnums.LsrsStation, MarkText = "LSRS-Station",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 61, BlipScale = 1f,
                BlipColor = 49, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Lsrs, MarkType = MarkTypeEnums.LsrsTimeClock, MarkText = "LSRS-Stechuhr",
                HasMapMark = false, HasWorldMark = true, HasTextLabel = true, BlipSprite = 61, BlipScale = 1f,
                BlipColor = 49, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Lsrs, MarkType = MarkTypeEnums.LsrsControlCenter,
                MarkText = "LSRS-Leitstelle", HasMapMark = false, HasWorldMark = true, HasTextLabel = true,
                BlipSprite = 61, BlipScale = 1f, BlipColor = 49, BlipAlpha = 200, BlipDrawDistance = 0f,
                BlipShortRange = true, BlipRotation = 0, MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255,
                MarkerColorGreen = 0, MarkerColorBlue = 0, MarkerColorAlpha = 155, MarkerBlobUpAndDown = false,
                ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f, ColShapeHeight = 1f, TextLabelRange = 50f,
                TextLabelSize = 50f, TextLabelFont = 0, TextLabelColorRed = 255, TextLabelColorGreen = 0,
                TextLabelColorBlue = 0, TextLabelColorAlpha = 155, TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Lsrs, MarkType = MarkTypeEnums.LsrsUniforms, MarkText = "LSRS-Uniformen",
                HasMapMark = false, HasWorldMark = true, HasTextLabel = true, BlipSprite = 175, BlipScale = 1f,
                BlipColor = 49, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Lsrs, MarkType = MarkTypeEnums.LsrsCanteen, MarkText = "LSRS-Kantine",
                HasMapMark = false, HasWorldMark = true, HasTextLabel = true, BlipSprite = 515, BlipScale = 1f,
                BlipColor = 49, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Lsrs, MarkType = MarkTypeEnums.LsrsTools, MarkText = "LSRS-Werkzeuge",
                HasMapMark = false, HasWorldMark = true, HasTextLabel = true, BlipSprite = 566, BlipScale = 1f,
                BlipColor = 49, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },

            // lspd //
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Lspd, MarkType = MarkTypeEnums.LspdStation, MarkText = "LSPD-Station",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 586, BlipScale = 1f,
                BlipColor = 38, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Lspd, MarkType = MarkTypeEnums.LspdUniforms, MarkText = "LSPD-Stechuhr",
                HasMapMark = false, HasWorldMark = true, HasTextLabel = true, BlipSprite = 586, BlipScale = 1f,
                BlipColor = 38, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Lspd, MarkType = MarkTypeEnums.LspdTimeClock, MarkText = "LSPD-Leitstelle",
                HasMapMark = false, HasWorldMark = true, HasTextLabel = true, BlipSprite = 586, BlipScale = 1f,
                BlipColor = 38, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Lspd, MarkType = MarkTypeEnums.LspdControlCenter,
                MarkText = "LSPD-Uniformen", HasMapMark = false, HasWorldMark = true, HasTextLabel = true,
                BlipSprite = 175, BlipScale = 1f, BlipColor = 38, BlipAlpha = 200, BlipDrawDistance = 0f,
                BlipShortRange = true, BlipRotation = 0, MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255,
                MarkerColorGreen = 0, MarkerColorBlue = 0, MarkerColorAlpha = 155, MarkerBlobUpAndDown = false,
                ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f, ColShapeHeight = 1f, TextLabelRange = 50f,
                TextLabelSize = 50f, TextLabelFont = 0, TextLabelColorRed = 255, TextLabelColorGreen = 0,
                TextLabelColorBlue = 0, TextLabelColorAlpha = 155, TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Lspd, MarkType = MarkTypeEnums.LspdCanteen, MarkText = "LSPD-Kantine",
                HasMapMark = true, HasWorldMark = false, HasTextLabel = true, BlipSprite = 515, BlipScale = 1f,
                BlipColor = 38, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Lspd, MarkType = MarkTypeEnums.LspdTools, MarkText = "LSPD-Werkzeuge",
                HasMapMark = false, HasWorldMark = true, HasTextLabel = true, BlipSprite = 566, BlipScale = 1f,
                BlipColor = 38, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Lspd, MarkType = MarkTypeEnums.LspdWeapons, MarkText = "LSPD-Waffen",
                HasMapMark = false, HasWorldMark = true, HasTextLabel = true, BlipSprite = 567, BlipScale = 1f,
                BlipColor = 38, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },

            // fib //
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Fib, MarkType = MarkTypeEnums.FibStation, MarkText = "FIB-Station",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 304, BlipScale = 1f,
                BlipColor = 78, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Fib, MarkType = MarkTypeEnums.FibTimeClock, MarkText = "FIB-Stechuhr",
                HasMapMark = false, HasWorldMark = true, HasTextLabel = true, BlipSprite = 304, BlipScale = 1f,
                BlipColor = 78, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Fib, MarkType = MarkTypeEnums.FibControlCenter, MarkText = "FIB-Leitstelle",
                HasMapMark = false, HasWorldMark = true, HasTextLabel = true, BlipSprite = 304, BlipScale = 1f,
                BlipColor = 78, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Fib, MarkType = MarkTypeEnums.FibUniforms, MarkText = "FIB-Uniformen",
                HasMapMark = false, HasWorldMark = true, HasTextLabel = true, BlipSprite = 175, BlipScale = 1f,
                BlipColor = 78, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Fib, MarkType = MarkTypeEnums.FibCanteen, MarkText = "FIB-Kantine",
                HasMapMark = false, HasWorldMark = true, HasTextLabel = true, BlipSprite = 515, BlipScale = 1f,
                BlipColor = 78, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Fib, MarkType = MarkTypeEnums.FibTools, MarkText = "FIB-Werkzeuge",
                HasMapMark = false, HasWorldMark = true, HasTextLabel = true, BlipSprite = 566, BlipScale = 1f,
                BlipColor = 78, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Fib, MarkType = MarkTypeEnums.FibWeapons, MarkText = "FIB-Waffen",
                HasMapMark = false, HasWorldMark = true, HasTextLabel = true, BlipSprite = 567, BlipScale = 1f,
                BlipColor = 78, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },

            // lssd //
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Lssd, MarkType = MarkTypeEnums.LssdStation, MarkText = "Lssd-Station",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 304, BlipScale = 1f,
                BlipColor = 31, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Lssd, MarkType = MarkTypeEnums.LssdTimeClock, MarkText = "Lssd-Stechuhr",
                HasMapMark = false, HasWorldMark = true, HasTextLabel = true, BlipSprite = 304, BlipScale = 1f,
                BlipColor = 31, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Lssd, MarkType = MarkTypeEnums.LssdControlCenter,
                MarkText = "Lssd-Leitstelle", HasMapMark = false, HasWorldMark = true, HasTextLabel = true,
                BlipSprite = 304, BlipScale = 1f, BlipColor = 31, BlipAlpha = 200, BlipDrawDistance = 0f,
                BlipShortRange = true, BlipRotation = 0, MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255,
                MarkerColorGreen = 0, MarkerColorBlue = 0, MarkerColorAlpha = 155, MarkerBlobUpAndDown = false,
                ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f, ColShapeHeight = 1f, TextLabelRange = 50f,
                TextLabelSize = 50f, TextLabelFont = 0, TextLabelColorRed = 255, TextLabelColorGreen = 0,
                TextLabelColorBlue = 0, TextLabelColorAlpha = 155, TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Lssd, MarkType = MarkTypeEnums.LssdUniforms, MarkText = "Lssd-Uniformen",
                HasMapMark = false, HasWorldMark = true, HasTextLabel = true, BlipSprite = 175, BlipScale = 1f,
                BlipColor = 31, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Lssd, MarkType = MarkTypeEnums.LssdCanteen, MarkText = "Lssd-Kantine",
                HasMapMark = false, HasWorldMark = true, HasTextLabel = true, BlipSprite = 515, BlipScale = 1f,
                BlipColor = 31, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Lssd, MarkType = MarkTypeEnums.LssdTools, MarkText = "Lssd-Werkzeuge",
                HasMapMark = false, HasWorldMark = true, HasTextLabel = true, BlipSprite = 566, BlipScale = 1f,
                BlipColor = 31, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Lssd, MarkType = MarkTypeEnums.LssdWeapons, MarkText = "Lssd-Waffen",
                HasMapMark = false, HasWorldMark = true, HasTextLabel = true, BlipSprite = 567, BlipScale = 1f,
                BlipColor = 31, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },

            // doj //
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Doj, MarkType = MarkTypeEnums.DojStation, MarkText = "Doj-Station",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 61, BlipScale = 1f,
                BlipColor = 49, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Doj, MarkType = MarkTypeEnums.DojTimeClock, MarkText = "Doj-Stechuhr",
                HasMapMark = false, HasWorldMark = true, HasTextLabel = true, BlipSprite = 61, BlipScale = 1f,
                BlipColor = 49, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Doj, MarkType = MarkTypeEnums.DojControlCenter, MarkText = "Doj-Leitstelle",
                HasMapMark = false, HasWorldMark = true, HasTextLabel = true, BlipSprite = 61, BlipScale = 1f,
                BlipColor = 49, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Doj, MarkType = MarkTypeEnums.DojUniforms, MarkText = "Doj-Uniformen",
                HasMapMark = false, HasWorldMark = true, HasTextLabel = true, BlipSprite = 175, BlipScale = 1f,
                BlipColor = 49, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Doj, MarkType = MarkTypeEnums.DojCanteen, MarkText = "Doj-Kantine",
                HasMapMark = false, HasWorldMark = true, HasTextLabel = true, BlipSprite = 515, BlipScale = 1f,
                BlipColor = 49, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Doj, MarkType = MarkTypeEnums.DojTools, MarkText = "Doj-Werkzeuge",
                HasMapMark = false, HasWorldMark = true, HasTextLabel = true, BlipSprite = 566, BlipScale = 1f,
                BlipColor = 49, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },

            // dcr //
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Dcr, MarkType = MarkTypeEnums.DcrStation, MarkText = "Dcr-Station",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 304, BlipScale = 1f,
                BlipColor = 55, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Dcr, MarkType = MarkTypeEnums.DcrTimeClock, MarkText = "Dcr-Stechuhr",
                HasMapMark = false, HasWorldMark = true, HasTextLabel = true, BlipSprite = 304, BlipScale = 1f,
                BlipColor = 55, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Dcr, MarkType = MarkTypeEnums.DcrControlCenter, MarkText = "Dcr-Leitstelle",
                HasMapMark = false, HasWorldMark = true, HasTextLabel = true, BlipSprite = 304, BlipScale = 1f,
                BlipColor = 55, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Dcr, MarkType = MarkTypeEnums.DcrUniforms, MarkText = "Dcr-Uniformen",
                HasMapMark = false, HasWorldMark = true, HasTextLabel = true, BlipSprite = 175, BlipScale = 1f,
                BlipColor = 55, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Dcr, MarkType = MarkTypeEnums.DcrCanteen, MarkText = "Dcr-Kantine",
                HasMapMark = false, HasWorldMark = true, HasTextLabel = true, BlipSprite = 515, BlipScale = 1f,
                BlipColor = 55, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Dcr, MarkType = MarkTypeEnums.DcrTools, MarkText = "Dcr-Werkzeuge",
                HasMapMark = false, HasWorldMark = true, HasTextLabel = true, BlipSprite = 566, BlipScale = 1f,
                BlipColor = 55, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Dcr, MarkType = MarkTypeEnums.DcrWeapons, MarkText = "Dcr-Waffen",
                HasMapMark = false, HasWorldMark = true, HasTextLabel = true, BlipSprite = 567, BlipScale = 1f,
                BlipColor = 55, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 255, MarkerColorGreen = 0, MarkerColorBlue = 0,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },

            // mines //
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.AppleMine, MarkText = "Apfelplantage",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.OrangeMine, MarkText = "Orangenplantage",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.LemonMine, MarkText = "Zitronenplantage",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.AlmondMine, MarkText = "Mandelplantage",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.WalnutMine, MarkText = "WalnusPlantage",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.PistacioMine, MarkText = "Pistazienplantage",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.AvocadoMine, MarkText = "Avocadoplantage",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.GrapeMine, MarkText = "Traubenplantage",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.WheatMine, MarkText = "Weizenfeld",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.RockMine, MarkText = "Mine", HasMapMark = true,
                HasWorldMark = true, HasTextLabel = true, BlipSprite = 1, BlipScale = 1f, BlipColor = 21,
                BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0, MarkerType = 1,
                MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.TomatoMine, MarkText = "Tomatenplantage",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.MariuhanaMine, MarkText = "Marihuanaplantage",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 8, BlipScale = 1f,
                BlipColor = 49, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.WoodMine, MarkText = "Forst",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CocaineMine, MarkText = "KokainPlantage",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 8, BlipScale = 1f,
                BlipColor = 49, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.PotatoMine, MarkText = "Kartoffelfeld",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.FrogMine, MarkText = "Froschzucht",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.ShellMine, MarkText = "Muschelzucht",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.FishingMine, MarkText = "Angelstelle",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.ChemicalMine, MarkText = "Chemieanlage",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 8, BlipScale = 1f,
                BlipColor = 49, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.MushroomMine, MarkText = "Pilzvorkommen",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 8, BlipScale = 1f,
                BlipColor = 49, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.MilkMine, MarkText = "Rinderfarm",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 8, BlipScale = 1f,
                BlipColor = 49, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.SandMine, MarkText = "Sandgrube",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },

            // farmSpots //
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.AppleFarmspot, MarkText = "Apfel-Abbaupunkt",
                HasMapMark = false, HasWorldMark = false, HasTextLabel = false, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.OrangeFarmspot,
                MarkText = "Orangen-Abbaupunkt", HasMapMark = false, HasWorldMark = false, HasTextLabel = false,
                BlipSprite = 1, BlipScale = 1f, BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f,
                BlipShortRange = true, BlipRotation = 0, MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139,
                MarkerColorGreen = 69, MarkerColorBlue = 19, MarkerColorAlpha = 155, MarkerBlobUpAndDown = false,
                ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f, ColShapeHeight = 1f, TextLabelRange = 50f,
                TextLabelSize = 50f, TextLabelFont = 0, TextLabelColorRed = 255, TextLabelColorGreen = 0,
                TextLabelColorBlue = 0, TextLabelColorAlpha = 155, TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.LemonFarmspot,
                MarkText = "Zitronen-Abbaupunkt", HasMapMark = false, HasWorldMark = false, HasTextLabel = false,
                BlipSprite = 1, BlipScale = 1f, BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f,
                BlipShortRange = true, BlipRotation = 0, MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139,
                MarkerColorGreen = 69, MarkerColorBlue = 19, MarkerColorAlpha = 155, MarkerBlobUpAndDown = false,
                ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f, ColShapeHeight = 1f, TextLabelRange = 50f,
                TextLabelSize = 50f, TextLabelFont = 0, TextLabelColorRed = 255, TextLabelColorGreen = 0,
                TextLabelColorBlue = 0, TextLabelColorAlpha = 155, TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.AlmondFarmspot, MarkText = "Mandel-Abbaupunkt",
                HasMapMark = false, HasWorldMark = false, HasTextLabel = false, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.WalnutFarmspot,
                MarkText = "Walnuss-Abbaupunkt", HasMapMark = false, HasWorldMark = false, HasTextLabel = false,
                BlipSprite = 1, BlipScale = 1f, BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f,
                BlipShortRange = true, BlipRotation = 0, MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139,
                MarkerColorGreen = 69, MarkerColorBlue = 19, MarkerColorAlpha = 155, MarkerBlobUpAndDown = false,
                ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f, ColShapeHeight = 1f, TextLabelRange = 50f,
                TextLabelSize = 50f, TextLabelFont = 0, TextLabelColorRed = 255, TextLabelColorGreen = 0,
                TextLabelColorBlue = 0, TextLabelColorAlpha = 155, TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.PistacioFarmspot,
                MarkText = "Pistazien-Abbaupunkt", HasMapMark = false, HasWorldMark = false, HasTextLabel = false,
                BlipSprite = 1, BlipScale = 1f, BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f,
                BlipShortRange = true, BlipRotation = 0, MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139,
                MarkerColorGreen = 69, MarkerColorBlue = 19, MarkerColorAlpha = 155, MarkerBlobUpAndDown = false,
                ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f, ColShapeHeight = 1f, TextLabelRange = 50f,
                TextLabelSize = 50f, TextLabelFont = 0, TextLabelColorRed = 255, TextLabelColorGreen = 0,
                TextLabelColorBlue = 0, TextLabelColorAlpha = 155, TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.AvocadoFarmspot,
                MarkText = "Avocado-Abbaupunkt", HasMapMark = false, HasWorldMark = false, HasTextLabel = false,
                BlipSprite = 1, BlipScale = 1f, BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f,
                BlipShortRange = true, BlipRotation = 0, MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139,
                MarkerColorGreen = 69, MarkerColorBlue = 19, MarkerColorAlpha = 155, MarkerBlobUpAndDown = false,
                ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f, ColShapeHeight = 1f, TextLabelRange = 50f,
                TextLabelSize = 50f, TextLabelFont = 0, TextLabelColorRed = 255, TextLabelColorGreen = 0,
                TextLabelColorBlue = 0, TextLabelColorAlpha = 155, TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.GrapeFarmspot, MarkText = "Trauben-Abbaupunkt",
                HasMapMark = false, HasWorldMark = false, HasTextLabel = false, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.WheatFarmspot, MarkText = "Weizen-Abbaupunkt",
                HasMapMark = false, HasWorldMark = false, HasTextLabel = false, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.RockFarmspot, MarkText = "Mine-Abbaupunkt",
                HasMapMark = false, HasWorldMark = false, HasTextLabel = false, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.TomatoFarmspot,
                MarkText = "Tomaten-Abbaupunkt", HasMapMark = false, HasWorldMark = false, HasTextLabel = false,
                BlipSprite = 1, BlipScale = 1f, BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f,
                BlipShortRange = true, BlipRotation = 0, MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139,
                MarkerColorGreen = 69, MarkerColorBlue = 19, MarkerColorAlpha = 155, MarkerBlobUpAndDown = false,
                ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f, ColShapeHeight = 1f, TextLabelRange = 50f,
                TextLabelSize = 50f, TextLabelFont = 0, TextLabelColorRed = 255, TextLabelColorGreen = 0,
                TextLabelColorBlue = 0, TextLabelColorAlpha = 155, TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.MariuhanaFarmspot,
                MarkText = "Mariuhana-Abbaupunkt", HasMapMark = false, HasWorldMark = false, HasTextLabel = false,
                BlipSprite = 1, BlipScale = 1f, BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f,
                BlipShortRange = true, BlipRotation = 0, MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139,
                MarkerColorGreen = 69, MarkerColorBlue = 19, MarkerColorAlpha = 155, MarkerBlobUpAndDown = false,
                ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f, ColShapeHeight = 1f, TextLabelRange = 50f,
                TextLabelSize = 50f, TextLabelFont = 0, TextLabelColorRed = 255, TextLabelColorGreen = 0,
                TextLabelColorBlue = 0, TextLabelColorAlpha = 155, TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.WoodFarmspot, MarkText = "Forst-Abbaupunkt",
                HasMapMark = false, HasWorldMark = false, HasTextLabel = false, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.CocaineFarmspot,
                MarkText = "Kokain-Abbaupunkt", HasMapMark = false, HasWorldMark = false, HasTextLabel = false,
                BlipSprite = 1, BlipScale = 1f, BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f,
                BlipShortRange = true, BlipRotation = 0, MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139,
                MarkerColorGreen = 69, MarkerColorBlue = 19, MarkerColorAlpha = 155, MarkerBlobUpAndDown = false,
                ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f, ColShapeHeight = 1f, TextLabelRange = 50f,
                TextLabelSize = 50f, TextLabelFont = 0, TextLabelColorRed = 255, TextLabelColorGreen = 0,
                TextLabelColorBlue = 0, TextLabelColorAlpha = 155, TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.PotatoFarmspot,
                MarkText = "Kartoffel-Abbaupunkt", HasMapMark = false, HasWorldMark = false, HasTextLabel = false,
                BlipSprite = 1, BlipScale = 1f, BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f,
                BlipShortRange = true, BlipRotation = 0, MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139,
                MarkerColorGreen = 69, MarkerColorBlue = 19, MarkerColorAlpha = 155, MarkerBlobUpAndDown = false,
                ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f, ColShapeHeight = 1f, TextLabelRange = 50f,
                TextLabelSize = 50f, TextLabelFont = 0, TextLabelColorRed = 255, TextLabelColorGreen = 0,
                TextLabelColorBlue = 0, TextLabelColorAlpha = 155, TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.FrogFarmspot, MarkText = "Frosch-Abbaupunkt",
                HasMapMark = false, HasWorldMark = false, HasTextLabel = false, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.ShellFarmspot, MarkText = "Muschel-Abbaupunkt",
                HasMapMark = false, HasWorldMark = false, HasTextLabel = false, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.FishingFarmspot, MarkText = "Angel-Abbaupunkt",
                HasMapMark = false, HasWorldMark = false, HasTextLabel = false, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.ChemicalFarmspot,
                MarkText = "Chemie-Abbaupunkt", HasMapMark = false, HasWorldMark = false, HasTextLabel = false,
                BlipSprite = 1, BlipScale = 1f, BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f,
                BlipShortRange = true, BlipRotation = 0, MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139,
                MarkerColorGreen = 69, MarkerColorBlue = 19, MarkerColorAlpha = 155, MarkerBlobUpAndDown = false,
                ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f, ColShapeHeight = 1f, TextLabelRange = 50f,
                TextLabelSize = 50f, TextLabelFont = 0, TextLabelColorRed = 255, TextLabelColorGreen = 0,
                TextLabelColorBlue = 0, TextLabelColorAlpha = 155, TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.MushroomFarmspot, MarkText = "Pilz-Abbaupunkt",
                HasMapMark = false, HasWorldMark = false, HasTextLabel = false, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.MilkFarmspot, MarkText = "Rinder-Abbaupunkt",
                HasMapMark = false, HasWorldMark = false, HasTextLabel = false, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.SandFarmspot, MarkText = "Sand-Abbaupunkt",
                HasMapMark = false, HasWorldMark = false, HasTextLabel = false, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },

            // processors //
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.Process_Butcher, MarkText = "Metzger",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.Process_Carpenter, MarkText = "Schreinerei",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.Process_CementFactory,
                MarkText = "Zementfabrik", HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 1,
                BlipScale = 1f, BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true,
                BlipRotation = 0, MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69,
                MarkerColorBlue = 19, MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f,
                ColShapeY = 1f, ColShapeRange = 3f, ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f,
                TextLabelFont = 0, TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0,
                TextLabelColorAlpha = 155, TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.Process_Dairy, MarkText = "Molkerei",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.Process_Distillery, MarkText = "Brennerei",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.Process_DrugFactory, MarkText = "Drogenkueche",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.Process_FlourMill, MarkText = "Muehle",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.Process_Forge, MarkText = "Schmiede",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.Process_Glassworks, MarkText = "Glaserei",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.Process_JuicePress, MarkText = "Mosterei",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.Process_KetchupFactory,
                MarkText = "Ketchupfabrik", HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 1,
                BlipScale = 1f, BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true,
                BlipRotation = 0, MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69,
                MarkerColorBlue = 19, MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f,
                ColShapeY = 1f, ColShapeRange = 3f, ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f,
                TextLabelFont = 0, TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0,
                TextLabelColorAlpha = 155, TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.Process_MarijuhanaFactory,
                MarkText = "Marijuhanatrocknung", HasMapMark = true, HasWorldMark = true, HasTextLabel = true,
                BlipSprite = 1, BlipScale = 1f, BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f,
                BlipShortRange = true, BlipRotation = 0, MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139,
                MarkerColorGreen = 69, MarkerColorBlue = 19, MarkerColorAlpha = 155, MarkerBlobUpAndDown = false,
                ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f, ColShapeHeight = 1f, TextLabelRange = 50f,
                TextLabelSize = 50f, TextLabelFont = 0, TextLabelColorRed = 255, TextLabelColorGreen = 0,
                TextLabelColorBlue = 0, TextLabelColorAlpha = 155, TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.Process_Melt, MarkText = "Schmelze",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.Process_Minter, MarkText = "Muenzpraegerei",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.Process_PaperFactory,
                MarkText = "Papierfabrik", HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 1,
                BlipScale = 1f, BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true,
                BlipRotation = 0, MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69,
                MarkerColorBlue = 19, MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f,
                ColShapeY = 1f, ColShapeRange = 3f, ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f,
                TextLabelFont = 0, TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0,
                TextLabelColorAlpha = 155, TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.Process_RollingMill, MarkText = "Walzwerk",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.Process_Sawmill, MarkText = "Saegewerk",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.Process_StoneMill, MarkText = "Steinmuehle",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.Process_WalnutProcessing,
                MarkText = "Walnussverarbeitung", HasMapMark = true, HasWorldMark = true, HasTextLabel = true,
                BlipSprite = 1, BlipScale = 1f, BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f,
                BlipShortRange = true, BlipRotation = 0, MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139,
                MarkerColorGreen = 69, MarkerColorBlue = 19, MarkerColorAlpha = 155, MarkerBlobUpAndDown = false,
                ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f, ColShapeHeight = 1f, TextLabelRange = 50f,
                TextLabelSize = 50f, TextLabelFont = 0, TextLabelColorRed = 255, TextLabelColorGreen = 0,
                TextLabelColorBlue = 0, TextLabelColorAlpha = 155, TextLabelEntitySeeThrough = false
            },

            // trader //
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.Trader_Exporter, MarkText = "Exporteur",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.Trader_FurnitureShop, MarkText = "Moebelhaus",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.Trader_BuilderMerchants,
                MarkText = "Baumaterialen", HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 1,
                BlipScale = 1f, BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true,
                BlipRotation = 0, MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69,
                MarkerColorBlue = 19, MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f,
                ColShapeY = 1f, ColShapeRange = 3f, ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f,
                TextLabelFont = 0, TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0,
                TextLabelColorAlpha = 155, TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.Trader_Bakery, MarkText = "Baeckerei",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.Trader_BigKitchen, MarkText = "Grosskueche",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.Trader_Jeweler, MarkText = "Juwelier",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.Trader_Fruiterer, MarkText = "Obsthandel",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            },
            new MarkSettingsModel
            {
                MarkFaction = FactionEnums.Civ, MarkType = MarkTypeEnums.Trader_Dealer, MarkText = "Drogendealer",
                HasMapMark = true, HasWorldMark = true, HasTextLabel = true, BlipSprite = 1, BlipScale = 1f,
                BlipColor = 21, BlipAlpha = 200, BlipDrawDistance = 0f, BlipShortRange = true, BlipRotation = 0,
                MarkerType = 1, MarkerScale = 1f, MarkerColorRed = 139, MarkerColorGreen = 69, MarkerColorBlue = 19,
                MarkerColorAlpha = 155, MarkerBlobUpAndDown = false, ColShapeX = 1f, ColShapeY = 1f, ColShapeRange = 3f,
                ColShapeHeight = 1f, TextLabelRange = 50f, TextLabelSize = 50f, TextLabelFont = 0,
                TextLabelColorRed = 255, TextLabelColorGreen = 0, TextLabelColorBlue = 0, TextLabelColorAlpha = 155,
                TextLabelEntitySeeThrough = false
            }
        };
    }
}