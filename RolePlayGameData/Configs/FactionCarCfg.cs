﻿using System.Collections.Generic;
using RolePlayGameData.Enums;
using RolePlayGameData.Models;

namespace RolePlayGameData.Configs
{
    public class FactionCarCfg
    {
        public static readonly Dictionary<string, FactionVehPosModel> LspdEastPositions =
            new Dictionary<string, FactionVehPosModel>();

        public static readonly Dictionary<string, FactionVehPosModel> LspdCityPositions =
            new Dictionary<string, FactionVehPosModel>
            {
                {
                    "CopCity_1",
                    new FactionVehPosModel
                    {
                        VehicleHash = VehicleHashEnums.police2, VehicleNumber = "LSPD 011", PosX = 407.9543f,
                        PosY = -979.9074f, PosZ = 29.0f, Rotation = 51.0f
                    }
                },
                {
                    "CopCity_2",
                    new FactionVehPosModel
                    {
                        VehicleHash = VehicleHashEnums.police2, VehicleNumber = "LSPD 012", PosX = 407.9543f,
                        PosY = -984.2317f, PosZ = 29.0f, Rotation = 51.0f
                    }
                },
                {
                    "CopCity_3",
                    new FactionVehPosModel
                    {
                        VehicleHash = VehicleHashEnums.police, VehicleNumber = "LSPD 013", PosX = 407.9543f,
                        PosY = -988.7577f, PosZ = 29.0f, Rotation = 51.0f
                    }
                },
                {
                    "CopCity_4",
                    new FactionVehPosModel
                    {
                        VehicleHash = VehicleHashEnums.police3, VehicleNumber = "LSPD 014", PosX = 407.9543f,
                        PosY = -993.2132f, PosZ = 29.0f, Rotation = 51.0f
                    }
                },
                {
                    "CopCity_5",
                    new FactionVehPosModel
                    {
                        VehicleHash = VehicleHashEnums.police3, VehicleNumber = "LSPD 015", PosX = 407.9543f,
                        PosY = -997.9438f, PosZ = 29.0f, Rotation = 51.0f
                    }
                },
                {
                    "CopCity_6",
                    new FactionVehPosModel
                    {
                        VehicleHash = VehicleHashEnums.policeb, VehicleNumber = "LSPD 016", PosX = 424.7927f,
                        PosY = -1014.343f, PosZ = 29.0f, Rotation = 178.5f
                    }
                },
                {
                    "CopCity_7",
                    new FactionVehPosModel
                    {
                        VehicleHash = VehicleHashEnums.policeb, VehicleNumber = "LSPD 017", PosX = 420.9503f,
                        PosY = -1014.343f, PosZ = 29.0f, Rotation = 178.5f
                    }
                },
                {
                    "CopCity_8",
                    new FactionVehPosModel
                    {
                        VehicleHash = VehicleHashEnums.policeb, VehicleNumber = "LSPD 018", PosX = 417.2050f,
                        PosY = -1014.343f, PosZ = 29.0f, Rotation = 178.5f
                    }
                },
                {
                    "CopCity_9",
                    new FactionVehPosModel
                    {
                        VehicleHash = VehicleHashEnums.policeb, VehicleNumber = "LSPD 019", PosX = 413.8304f,
                        PosY = -1014.343f, PosZ = 29.0f, Rotation = 178.5f
                    }
                }
            };

        public static readonly Dictionary<string, FactionVehPosModel> LspdVinewoodPositions =
            new Dictionary<string, FactionVehPosModel>();

        public static readonly Dictionary<string, FactionVehPosModel> LssdSandyShoresPositions =
            new Dictionary<string, FactionVehPosModel>();

        public static readonly Dictionary<string, FactionVehPosModel> LssdPaletoBayPositions =
            new Dictionary<string, FactionVehPosModel>();
    }
}