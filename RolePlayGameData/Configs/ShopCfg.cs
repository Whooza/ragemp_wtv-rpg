﻿using System.Collections.Generic;
using RolePlayGameData.Enums;

namespace RolePlayGameData.Configs
{
    internal class ShopCfg
    {
        public static readonly Dictionary<MarkTypeEnums, List<ItemNameEnums>> ItemShopOfferList =
            new Dictionary<MarkTypeEnums, List<ItemNameEnums>>
            {
                {
                    MarkTypeEnums.CivSnackShop, new List<ItemNameEnums>
                    {
                        ItemNameEnums.Apple, ItemNameEnums.Avocado, ItemNameEnums.Banane, ItemNameEnums.Bun,
                        ItemNameEnums.Burger, ItemNameEnums.Chocolate, ItemNameEnums.Donut, ItemNameEnums.Pistachio,
                        ItemNameEnums.Salad, ItemNameEnums.Strawberries, ItemNameEnums.Coffee, ItemNameEnums.Cola,
                        ItemNameEnums.Water, ItemNameEnums.Beer, ItemNameEnums.Martini, ItemNameEnums.Tequila,
                        ItemNameEnums.Vodka, ItemNameEnums.Wine, ItemNameEnums.Butter, ItemNameEnums.Cheese,
                        ItemNameEnums.ChocolateMuesli, ItemNameEnums.CookedHam, ItemNameEnums.Cucumber,
                        ItemNameEnums.FertilizerK, ItemNameEnums.FertilizerM, ItemNameEnums.FertilizerS,
                        ItemNameEnums.Flour, ItemNameEnums.JointPaper, ItemNameEnums.Methanol, ItemNameEnums.Mustard,
                        ItemNameEnums.Noodles, ItemNameEnums.OliveOil, ItemNameEnums.Salami, ItemNameEnums.Solvent,
                        ItemNameEnums.TurkeyMeat, ItemNameEnums.Yeast, ItemNameEnums.Yogurt, ItemNameEnums.Wrap,
                        ItemNameEnums.Bandage, ItemNameEnums.Cigar, ItemNameEnums.Cigarette, ItemNameEnums.CigarettePack
                    }
                },

                //{ MarkTypeEnums.CivSoftDrinkMachine, new List<ItemNameEnums>
                //{ ItemNameEnums.Coffee, ItemNameEnums.Cola, ItemNameEnums.Water, }},

                {
                    MarkTypeEnums.CivSnackShop, new List<ItemNameEnums>
                        {ItemNameEnums.Apple, ItemNameEnums.Donut, ItemNameEnums.Hammer}
                },

                {
                    MarkTypeEnums.CivToolShop, new List<ItemNameEnums>
                    {
                        ItemNameEnums.FertilizerK, ItemNameEnums.FertilizerM, ItemNameEnums.FertilizerS,
                        ItemNameEnums.Axe, ItemNameEnums.BatteryPack, ItemNameEnums.FishingRod, ItemNameEnums.FuelCan,
                        ItemNameEnums.Gloves, ItemNameEnums.LockPick, ItemNameEnums.Sickle, ItemNameEnums.Spade,
                        ItemNameEnums.Bandage, ItemNameEnums.RepairKit, ItemNameEnums.Fireextinguisher,
                        ItemNameEnums.Petrolcan
                    }
                },

                {
                    MarkTypeEnums.CivWeaponShop, new List<ItemNameEnums>
                    {
                        ItemNameEnums.Golfclub, ItemNameEnums.Hatchet, ItemNameEnums.Knife, ItemNameEnums.Pistol,
                        ItemNameEnums.VintagePistol, ItemNameEnums.Flare, ItemNameEnums.Smokegrenade,
                        ItemNameEnums.Parachute
                    }
                },

                {
                    MarkTypeEnums.LspdWeapons, new List<ItemNameEnums>
                    {
                        ItemNameEnums.Flashlight, ItemNameEnums.Nightstick, ItemNameEnums.DoubleAction,
                        ItemNameEnums.Heavypistol, ItemNameEnums.Pistol, ItemNameEnums.PistolMk2,
                        ItemNameEnums.Revolver, ItemNameEnums.Snspistol_mk2, ItemNameEnums.Stungun,
                        ItemNameEnums.CombatPdw, ItemNameEnums.MiniSmg, ItemNameEnums.Smg, ItemNameEnums.PumpshotGun,
                        ItemNameEnums.CarbineRifle
                    }
                }
            };

        public static readonly Dictionary<ItemNameEnums, float> BlackMarketOfferList =
            new Dictionary<ItemNameEnums, float>
            {
                {ItemNameEnums.BudBoost, 0.01f}, {ItemNameEnums.GrowthBoost, 0.01f}, {ItemNameEnums.Bandage, 0f},
                {ItemNameEnums.AmnesiaHazeSeed, 0.01f}, {ItemNameEnums.CriticalSeed, 0.025f},
                {ItemNameEnums.HollandsHopeSeed, 0.1f}, {ItemNameEnums.OGKushSeed, 0.05f},
                {ItemNameEnums.SpecialQueenSeed, 0.75f}, {ItemNameEnums.WhiteWidowSeed, 0.1f},
                {ItemNameEnums.Bat, 0.02f}, {ItemNameEnums.Knife, 0.2f}, {ItemNameEnums.Switchblade, 0.05f},
                {ItemNameEnums.Pistol, 0.3f}, {ItemNameEnums.Pistol50, 0.01f}, {ItemNameEnums.RevolverMk2, 0.01f},
                {ItemNameEnums.Snspistol, 0.01f}, {ItemNameEnums.MicroSmg, 0.01f}
            };
    }
}