﻿using GTANetworkAPI;

namespace RolePlayGameData.Configs
{
    public class ServerCfg
    {
        public static Vector3 WorldSpawn => new Vector3(-423.8049, 1127.8, 325.8553);
        public static bool ClientSiteRunsLocal => true;
        public static bool AccessServicesEnabled => false;
        public static bool ShowMarkIds => true;

        public static string ApiLocal => "http://localhost:5000/";
        public static string ApiRemote => "https://api.the-golden-state.de/";

        public static string WebToken => "AdUwFqw23EvAwDsW2g";
        public static string ApiToken => "FdVwXtw63QEWEqWeDG";
    }
}