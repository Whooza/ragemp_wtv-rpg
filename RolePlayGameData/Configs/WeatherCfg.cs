﻿namespace RolePlayGameData.Configs
{
    public class WeatherCfg
    {
        public const string Sunny = "EXTRASUNNY";        
        public const string Thunder = "THUNDER";
        public const string Rain = "RAIN";

        public static readonly string[] RainIntro = { "EXTRASUNNY", "CLEAR", "CLOUDS", "OVERCAST", "CLEARING", "RAIN" };
        public static readonly string[] RainOutro = { "RAIN", "CLEARING", "OVERCAST", "CLOUDS" , "CLEAR", "EXTRASUNNY" };

        public static readonly string[] ThunderIntro = { "EXTRASUNNY", "CLEAR", "CLOUDS", "OVERCAST", "CLEARING", "RAIN", "THUNDER" };
        public static readonly string[] ThunderOutro = { "THUNDER", "RAIN", "CLEARING", "OVERCAST", "CLOUDS", "CLEAR", "EXTRASUNNY" };
    }
}
