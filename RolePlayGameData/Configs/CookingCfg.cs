﻿using System.Collections.Generic;
using RolePlayGameData.Enums;
using RolePlayGameData.Models;

namespace RolePlayGameData.Configs
{
    public class CookingCfg
    {
        public static readonly List<CookingRecipeModel> CookingLvLOneRecipes = new List<CookingRecipeModel>
        {
            // #-Stufe 1-#
            new CookingRecipeModel(CookingCategoryEnums.FingerFood, CookingLevelEnums.LevelOne, 0)
            {
                RecipeName = "Salamisandwich", ProcessTime = 10000,
                MatsList = new Dictionary<ItemNameEnums, int>
                    {{ItemNameEnums.Bun, 1}, {ItemNameEnums.Salami, 1}, {ItemNameEnums.Butter, 1}},
                CookedItemType = ItemNameEnums.SalamiSandwich, CookedItemAmount = 1
            },
            new CookingRecipeModel(CookingCategoryEnums.FingerFood, CookingLevelEnums.LevelOne, 1)
            {
                RecipeName = "Käsesandwich", ProcessTime = 10000,
                MatsList = new Dictionary<ItemNameEnums, int>
                    {{ItemNameEnums.Bun, 1}, {ItemNameEnums.Cheese, 1}, {ItemNameEnums.Butter, 1}},
                CookedItemType = ItemNameEnums.CheeseSandwich, CookedItemAmount = 1
            },
            new CookingRecipeModel(CookingCategoryEnums.FingerFood, CookingLevelEnums.LevelOne, 2)
            {
                RecipeName = "Puten-Wrap", ProcessTime = 10000,
                MatsList = new Dictionary<ItemNameEnums, int>
                    {{ItemNameEnums.Wrap, 1}, {ItemNameEnums.Cheese, 1}, {ItemNameEnums.TurkeyMeat, 1}},
                CookedItemType = ItemNameEnums.TurkeyWrap, CookedItemAmount = 1
            },
            new CookingRecipeModel(CookingCategoryEnums.FingerFood, CookingLevelEnums.LevelOne, 3)
            {
                RecipeName = "Schoko-Banane-Wrap", ProcessTime = 10000,
                MatsList = new Dictionary<ItemNameEnums, int>
                    {{ItemNameEnums.Wrap, 1}, {ItemNameEnums.ChocolateMuesli, 1}, {ItemNameEnums.Banane, 1}},
                CookedItemType = ItemNameEnums.ChocolateBananaWrap, CookedItemAmount = 1
            },
            new CookingRecipeModel(CookingCategoryEnums.FingerFood, CookingLevelEnums.LevelOne, 4)
            {
                RecipeName = "Schoko-Müsli mit Milch", ProcessTime = 10000,
                MatsList = new Dictionary<ItemNameEnums, int>
                    {{ItemNameEnums.ChocolateMuesli, 1}, {ItemNameEnums.Milk, 1}},
                CookedItemType = ItemNameEnums.ChocolateCerealWitchMilk, CookedItemAmount = 2
            },
            new CookingRecipeModel(CookingCategoryEnums.SoftDrink, CookingLevelEnums.LevelOne, 5)
            {
                RecipeName = "Bananen-Milchshake", ProcessTime = 10000,
                MatsList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.Banane, 2}, {ItemNameEnums.Milk, 1}},
                CookedItemType = ItemNameEnums.BananaMilkshake, CookedItemAmount = 2
            },
            new CookingRecipeModel(CookingCategoryEnums.SoftDrink, CookingLevelEnums.LevelOne, 6)
            {
                RecipeName = "Erdbeer-Milchshake", ProcessTime = 10000,
                MatsList =
                    new Dictionary<ItemNameEnums, int> {{ItemNameEnums.Strawberries, 2}, {ItemNameEnums.Milk, 1}},
                CookedItemType = ItemNameEnums.StrawberryMilkshake, CookedItemAmount = 2
            },
            new CookingRecipeModel(CookingCategoryEnums.SoftDrink, CookingLevelEnums.LevelOne, 7)
            {
                RecipeName = "Schoko-Milchshake", ProcessTime = 10000,
                MatsList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.Chocolate, 2}, {ItemNameEnums.Milk, 1}},
                CookedItemType = ItemNameEnums.ChocolateMilkshake, CookedItemAmount = 2
            }
        };

        public static readonly List<CookingRecipeModel> CookingLvLTwoRecipes = new List<CookingRecipeModel>
        {
            // #-Stufe 2-#
            new CookingRecipeModel(CookingCategoryEnums.FingerFood, CookingLevelEnums.LevelTwo, 0)
            {
                RecipeName = "Brötchen", ProcessTime = 10000,
                MatsList = new Dictionary<ItemNameEnums, int>
                {
                    {ItemNameEnums.Flour, 2}, {ItemNameEnums.OliveOil, 1}, {ItemNameEnums.Butter, 1},
                    {ItemNameEnums.Water, 1}, {ItemNameEnums.Yeast, 1}
                },
                CookedItemType = ItemNameEnums.Bun, CookedItemAmount = 10
            },
            new CookingRecipeModel(CookingCategoryEnums.FingerFood, CookingLevelEnums.LevelTwo, 1)
            {
                RecipeName = "Wrap", ProcessTime = 10000,
                MatsList = new Dictionary<ItemNameEnums, int>
                {
                    {ItemNameEnums.Flour, 1}, {ItemNameEnums.OliveOil, 1}, {ItemNameEnums.Water, 1},
                    {ItemNameEnums.Yeast, 1}
                },
                CookedItemType = ItemNameEnums.Wrap, CookedItemAmount = 8
            },
            new CookingRecipeModel(CookingCategoryEnums.FingerFood, CookingLevelEnums.LevelTwo, 2)
            {
                RecipeName = "Lachs-Wrap", ProcessTime = 10000,
                MatsList = new Dictionary<ItemNameEnums, int>
                {
                    {ItemNameEnums.Wrap, 3}, {ItemNameEnums.Salmon, 1}, {ItemNameEnums.Yogurt, 1},
                    {ItemNameEnums.Lemon, 1}, {ItemNameEnums.Cucumber, 1}, {ItemNameEnums.Tomato, 1}
                },
                CookedItemType = ItemNameEnums.SalmonWrap, CookedItemAmount = 3
            },
            new CookingRecipeModel(CookingCategoryEnums.FingerFood, CookingLevelEnums.LevelTwo, 3)
            {
                RecipeName = "Schoko-Keks", ProcessTime = 10000,
                MatsList = new Dictionary<ItemNameEnums, int>
                {
                    {ItemNameEnums.Flour, 1}, {ItemNameEnums.Butter, 1}, {ItemNameEnums.ChocolateMuesli, 1},
                    {ItemNameEnums.Water, 1}
                },
                CookedItemType = ItemNameEnums.ChocolateCookie, CookedItemAmount = 4
            },
            new CookingRecipeModel(CookingCategoryEnums.SoftDrink, CookingLevelEnums.LevelTwo, 4)
            {
                RecipeName = "Bananensmoothie", ProcessTime = 10000,
                MatsList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.Banane, 4}, {ItemNameEnums.Milk, 1}},
                CookedItemType = ItemNameEnums.BananaSmoothie, CookedItemAmount = 2
            },
            new CookingRecipeModel(CookingCategoryEnums.SoftDrink, CookingLevelEnums.LevelTwo, 5)
            {
                RecipeName = "Erdbeersmoothie", ProcessTime = 10000,
                MatsList =
                    new Dictionary<ItemNameEnums, int> {{ItemNameEnums.Strawberries, 4}, {ItemNameEnums.Milk, 1}},
                CookedItemType = ItemNameEnums.StrawberrySmoothie, CookedItemAmount = 2
            }
        };

        public static readonly List<CookingRecipeModel> CookingLvLThreeRecipes = new List<CookingRecipeModel>
        {
            // #-Stufe 3-#
            new CookingRecipeModel(CookingCategoryEnums.FingerFood, CookingLevelEnums.LevelThree, 0)
            {
                RecipeName = "Spaghetti Bolognese", ProcessTime = 10000,
                MatsList = new Dictionary<ItemNameEnums, int>
                    {{ItemNameEnums.Noodles, 1}, {ItemNameEnums.Tomato, 2}, {ItemNameEnums.CattleMeat, 1}},
                CookedItemType = ItemNameEnums.SpaghettiBolognese, CookedItemAmount = 2
            },
            new CookingRecipeModel(CookingCategoryEnums.FingerFood, CookingLevelEnums.LevelThree, 1)
            {
                RecipeName = "Pommes", ProcessTime = 10000,
                MatsList = new Dictionary<ItemNameEnums, int>
                    {{ItemNameEnums.PotatoSack, 1}, {ItemNameEnums.OliveOil, 1}},
                CookedItemType = ItemNameEnums.Fries, CookedItemAmount = 2
            },
            new CookingRecipeModel(CookingCategoryEnums.FingerFood, CookingLevelEnums.LevelThree, 2)
            {
                RecipeName = "Hamburger", ProcessTime = 10000,
                MatsList = new Dictionary<ItemNameEnums, int>
                {
                    {ItemNameEnums.Bun, 1}, {ItemNameEnums.Tomato, 1}, {ItemNameEnums.CattleMeat, 1},
                    {ItemNameEnums.Cucumber, 1}
                },
                CookedItemType = ItemNameEnums.Burger, CookedItemAmount = 1
            },
            new CookingRecipeModel(CookingCategoryEnums.FingerFood, CookingLevelEnums.LevelThree, 3)
            {
                RecipeName = "Fish and Chips V1", ProcessTime = 10000,
                MatsList = new Dictionary<ItemNameEnums, int>
                {
                    {ItemNameEnums.PotatoSack, 1}, {ItemNameEnums.Bun, 1}, {ItemNameEnums.Lemon, 1},
                    {ItemNameEnums.Cucumber, 1}, {ItemNameEnums.Cod, 1}
                },
                CookedItemType = ItemNameEnums.FishAndChips, CookedItemAmount = 1
            },
            new CookingRecipeModel(CookingCategoryEnums.FingerFood, CookingLevelEnums.LevelThree, 4)
            {
                RecipeName = "Fish and Chips V2", ProcessTime = 10000,
                MatsList = new Dictionary<ItemNameEnums, int>
                {
                    {ItemNameEnums.PotatoSack, 1}, {ItemNameEnums.Bun, 1}, {ItemNameEnums.Lemon, 1},
                    {ItemNameEnums.Cucumber, 1}, {ItemNameEnums.Mackerel, 1}
                },
                CookedItemType = ItemNameEnums.FishAndChips, CookedItemAmount = 1
            },
            new CookingRecipeModel(CookingCategoryEnums.FingerFood, CookingLevelEnums.LevelThree, 5)
            {
                RecipeName = "Fish and Chips V3", ProcessTime = 10000,
                MatsList = new Dictionary<ItemNameEnums, int>
                {
                    {ItemNameEnums.PotatoSack, 1}, {ItemNameEnums.Bun, 1}, {ItemNameEnums.Lemon, 1},
                    {ItemNameEnums.Cucumber, 1}, {ItemNameEnums.Perch, 1}
                },
                CookedItemType = ItemNameEnums.FishAndChips, CookedItemAmount = 1
            },
            new CookingRecipeModel(CookingCategoryEnums.SoftDrink, CookingLevelEnums.LevelThree, 6)
            {
                RecipeName = "Heiße Schokolade", ProcessTime = 10000,
                MatsList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.Chocolate, 1}, {ItemNameEnums.Milk, 1}},
                CookedItemType = ItemNameEnums.HotChocolate, CookedItemAmount = 2
            }
        };

        public static readonly List<CookingRecipeModel> CookingLvLFourRecipes = new List<CookingRecipeModel>
        {
            // #-Stufe 4-#
            new CookingRecipeModel(CookingCategoryEnums.FingerFood, CookingLevelEnums.LevelFour, 0)
            {
                RecipeName = "Schoko-Pommes", ProcessTime = 10000,
                MatsList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.Chocolate, 1}, {ItemNameEnums.Fries, 1}},
                CookedItemType = ItemNameEnums.ChocolateFries, CookedItemAmount = 1
            },
            new CookingRecipeModel(CookingCategoryEnums.FingerFood, CookingLevelEnums.LevelFour, 1)
            {
                RecipeName = "Käse-Pommes", ProcessTime = 10000,
                MatsList = new Dictionary<ItemNameEnums, int> {{ItemNameEnums.Cheese, 1}, {ItemNameEnums.Fries, 1}},
                CookedItemType = ItemNameEnums.CheeseFries, CookedItemAmount = 1
            },
            new CookingRecipeModel(CookingCategoryEnums.FingerFood, CookingLevelEnums.LevelFour, 2)
            {
                RecipeName = "Cheeseburger", ProcessTime = 10000,
                MatsList = new Dictionary<ItemNameEnums, int>
                {
                    {ItemNameEnums.Bun, 1}, {ItemNameEnums.Tomato, 1}, {ItemNameEnums.CattleMeat, 1},
                    {ItemNameEnums.Cucumber, 1}, {ItemNameEnums.Cheese, 1}
                },
                CookedItemType = ItemNameEnums.CheeseBurger, CookedItemAmount = 1
            },
            new CookingRecipeModel(CookingCategoryEnums.FingerFood, CookingLevelEnums.LevelFour, 3)
            {
                RecipeName = "Pizza Margarita", ProcessTime = 10000,
                MatsList = new Dictionary<ItemNameEnums, int>
                {
                    {ItemNameEnums.Flour, 1}, {ItemNameEnums.Water, 1}, {ItemNameEnums.OliveOil, 1},
                    {ItemNameEnums.Tomato, 1}, {ItemNameEnums.Cheese, 1}
                },
                CookedItemType = ItemNameEnums.PizzaMargarita, CookedItemAmount = 1
            },
            new CookingRecipeModel(CookingCategoryEnums.Meat, CookingLevelEnums.LevelFour, 4)
            {
                RecipeName = "Hackbraten a la Cordon bleu", ProcessTime = 10000,
                MatsList = new Dictionary<ItemNameEnums, int>
                {
                    {ItemNameEnums.CattleMeat, 1}, {ItemNameEnums.Pork, 1}, {ItemNameEnums.OliveOil, 1},
                    {ItemNameEnums.Tomato, 2}, {ItemNameEnums.CookedHam, 2}, {ItemNameEnums.Cheese, 2}
                },
                CookedItemType = ItemNameEnums.MeatloafALaCordonBleu, CookedItemAmount = 1
            }
        };

        public static readonly List<CookingRecipeModel> CookingLvLFiveRecipes = new List<CookingRecipeModel>
        {
            // #-Stufe 5-#
            new CookingRecipeModel(CookingCategoryEnums.FingerFood, CookingLevelEnums.LevelFive, 0)
            {
                RecipeName = "Gedünsteter Dorsch mit Kartoffeln", ProcessTime = 10000,
                MatsList = new Dictionary<ItemNameEnums, int>
                {
                    {ItemNameEnums.Cod, 1}, {ItemNameEnums.PotatoSack, 1}, {ItemNameEnums.Mustard, 1},
                    {ItemNameEnums.Lemon, 1}, {ItemNameEnums.OliveOil, 1}
                },
                CookedItemType = ItemNameEnums.BraisedCodWithPotatoes, CookedItemAmount = 2
            },
            new CookingRecipeModel(CookingCategoryEnums.Fish, CookingLevelEnums.LevelFive, 1)
            {
                RecipeName = "Gebratener Dorsch mit Gemüse", ProcessTime = 10000,
                MatsList = new Dictionary<ItemNameEnums, int>
                {
                    {ItemNameEnums.Cod, 1}, {ItemNameEnums.Cucumber, 1}, {ItemNameEnums.OliveOil, 1},
                    {ItemNameEnums.Tomato, 2}
                },
                CookedItemType = ItemNameEnums.FriedCodWithVegetables, CookedItemAmount = 1
            },
            new CookingRecipeModel(CookingCategoryEnums.Fish, CookingLevelEnums.LevelFive, 2)
            {
                RecipeName = "Pasta mit Makrelen-Tomatensauce", ProcessTime = 10000,
                MatsList = new Dictionary<ItemNameEnums, int>
                {
                    {ItemNameEnums.Mackerel, 1}, {ItemNameEnums.Noodles, 1}, {ItemNameEnums.Milk, 1},
                    {ItemNameEnums.Tomato, 2}
                },
                CookedItemType = ItemNameEnums.PastaWithMackerelTomatoSauce, CookedItemAmount = 1
            },
            new CookingRecipeModel(CookingCategoryEnums.Fish, CookingLevelEnums.LevelFive, 3)
            {
                RecipeName = "Gebackene Makrele", ProcessTime = 10000,
                MatsList = new Dictionary<ItemNameEnums, int>
                {
                    {ItemNameEnums.PotatoSack, 1}, {ItemNameEnums.Mackerel, 2}, {ItemNameEnums.Cheese, 2},
                    {ItemNameEnums.Tomato, 2}
                },
                CookedItemType = ItemNameEnums.BakedMackerel, CookedItemAmount = 1
            },
            new CookingRecipeModel(CookingCategoryEnums.Fish, CookingLevelEnums.LevelFive, 4)
            {
                RecipeName = "Gebackener Barsch mit Kartoffelecken", ProcessTime = 10000,
                MatsList = new Dictionary<ItemNameEnums, int>
                    {{ItemNameEnums.Perch, 2}, {ItemNameEnums.PotatoSack, 1}, {ItemNameEnums.Lemon, 1}},
                CookedItemType = ItemNameEnums.BakedPerchWithPotatoWedges, CookedItemAmount = 1
            },
            new CookingRecipeModel(CookingCategoryEnums.Fish, CookingLevelEnums.LevelFive, 5)
            {
                RecipeName = "Fisch-Auflauf mit Pilzen V1", ProcessTime = 10000,
                MatsList = new Dictionary<ItemNameEnums, int>
                {
                    {ItemNameEnums.Cucumber, 3}, {ItemNameEnums.Milk, 2}, {ItemNameEnums.Cheese, 2},
                    {ItemNameEnums.Perch, 1}, {ItemNameEnums.Cod, 1}, {ItemNameEnums.Cep, 6}
                },
                CookedItemType = ItemNameEnums.BakedPerchWithPotatoWedges, CookedItemAmount = 3
            },
            new CookingRecipeModel(CookingCategoryEnums.Fish, CookingLevelEnums.LevelFive, 6)
            {
                RecipeName = "Fisch-Auflauf mit Pilzen V2", ProcessTime = 10000,
                MatsList = new Dictionary<ItemNameEnums, int>
                {
                    {ItemNameEnums.Cucumber, 3}, {ItemNameEnums.Milk, 2}, {ItemNameEnums.Cheese, 2},
                    {ItemNameEnums.Perch, 1}, {ItemNameEnums.Cod, 1}, {ItemNameEnums.Chanterelle, 6}
                },
                CookedItemType = ItemNameEnums.BakedPerchWithPotatoWedges, CookedItemAmount = 3
            },
            new CookingRecipeModel(CookingCategoryEnums.Fish, CookingLevelEnums.LevelFive, 7)
            {
                RecipeName = "Fisch-Auflauf mit Pilzen V3", ProcessTime = 10000,
                MatsList = new Dictionary<ItemNameEnums, int>
                {
                    {ItemNameEnums.Cucumber, 3}, {ItemNameEnums.Milk, 2}, {ItemNameEnums.Cheese, 2},
                    {ItemNameEnums.Perch, 1}, {ItemNameEnums.Cod, 1}, {ItemNameEnums.Champignon, 6}
                },
                CookedItemType = ItemNameEnums.BakedPerchWithPotatoWedges, CookedItemAmount = 3
            }
        };

        public static readonly List<CookingRecipeModel> CookingLvLSixRecipes = new List<CookingRecipeModel>
        {
            // #-Stufe 6-#
            new CookingRecipeModel(CookingCategoryEnums.Meat, CookingLevelEnums.LevelSix, 0)
            {
                RecipeName = "Rinderbraten mit Senfkruste", ProcessTime = 10000,
                MatsList = new Dictionary<ItemNameEnums, int>
                {
                    {ItemNameEnums.CattleMeat, 1}, {ItemNameEnums.Mustard, 1}, {ItemNameEnums.PotatoSack, 1},
                    {ItemNameEnums.Cucumber, 1}
                },
                CookedItemType = ItemNameEnums.BeefRoastWithMustardCrust, CookedItemAmount = 1
            },
            new CookingRecipeModel(CookingCategoryEnums.Meat, CookingLevelEnums.LevelSix, 1)
            {
                RecipeName = "Rindfleischspieß", ProcessTime = 10000,
                MatsList = new Dictionary<ItemNameEnums, int>
                    {{ItemNameEnums.CattleMeat, 1}, {ItemNameEnums.Cucumber, 1}},
                CookedItemType = ItemNameEnums.BeefSkewers, CookedItemAmount = 2
            },
            new CookingRecipeModel(CookingCategoryEnums.Meat, CookingLevelEnums.LevelSix, 2)
            {
                RecipeName = "Schweinegeschnetzeltes mit Spätzle", ProcessTime = 10000,
                MatsList = new Dictionary<ItemNameEnums, int>
                {
                    {ItemNameEnums.Pork, 1}, {ItemNameEnums.Noodles, 1}, {ItemNameEnums.Tomato, 1},
                    {ItemNameEnums.Milk, 1}
                },
                CookedItemType = ItemNameEnums.PorkChopsWithSpaetzle, CookedItemAmount = 2
            },
            new CookingRecipeModel(CookingCategoryEnums.Meat, CookingLevelEnums.LevelSix, 3)
            {
                RecipeName = "Schweineschnitzelfleisch", ProcessTime = 10000,
                MatsList = new Dictionary<ItemNameEnums, int>
                {
                    {ItemNameEnums.Pork, 1}, {ItemNameEnums.Bun, 1}, {ItemNameEnums.Champignon, 1},
                    {ItemNameEnums.Tomato, 1}, {ItemNameEnums.Lemon, 1}
                },
                CookedItemType = ItemNameEnums.PorkSteakMeat, CookedItemAmount = 1
            },
            new CookingRecipeModel(CookingCategoryEnums.Meat, CookingLevelEnums.LevelSix, 4)
            {
                RecipeName = "Halbes Grillhähnchen Käse überbacken", ProcessTime = 10000,
                MatsList = new Dictionary<ItemNameEnums, int>
                    {{ItemNameEnums.ChickenMeat, 1}, {ItemNameEnums.Cheese, 1}},
                CookedItemType = ItemNameEnums.HalfGrilledChickenGratinatedCheese, CookedItemAmount = 2
            },
            new CookingRecipeModel(CookingCategoryEnums.Meat, CookingLevelEnums.LevelSix, 5)
            {
                RecipeName = "Hähnchen-Nudel-Auflauf V1", ProcessTime = 10000,
                MatsList = new Dictionary<ItemNameEnums, int>
                {
                    {ItemNameEnums.ChickenMeat, 1}, {ItemNameEnums.Milk, 1}, {ItemNameEnums.Noodles, 1},
                    {ItemNameEnums.Tomato, 2}, {ItemNameEnums.Champignon, 6}
                },
                CookedItemType = ItemNameEnums.ChickenPastaBake, CookedItemAmount = 1
            },
            new CookingRecipeModel(CookingCategoryEnums.Meat, CookingLevelEnums.LevelSix, 6)
            {
                RecipeName = "Hähnchen-Nudel-Auflauf V2", ProcessTime = 10000,
                MatsList = new Dictionary<ItemNameEnums, int>
                {
                    {ItemNameEnums.ChickenMeat, 1}, {ItemNameEnums.Milk, 1}, {ItemNameEnums.Noodles, 1},
                    {ItemNameEnums.Tomato, 2}, {ItemNameEnums.Chanterelle, 6}
                },
                CookedItemType = ItemNameEnums.ChickenPastaBake, CookedItemAmount = 1
            },
            new CookingRecipeModel(CookingCategoryEnums.Meat, CookingLevelEnums.LevelSix, 7)
            {
                RecipeName = "Hähnchen-Nudel-Auflauf V3", ProcessTime = 10000,
                MatsList = new Dictionary<ItemNameEnums, int>
                {
                    {ItemNameEnums.ChickenMeat, 1}, {ItemNameEnums.Milk, 1}, {ItemNameEnums.Noodles, 1},
                    {ItemNameEnums.Tomato, 2}, {ItemNameEnums.Cep, 6}
                },
                CookedItemType = ItemNameEnums.ChickenPastaBake, CookedItemAmount = 1
            }
        };
    }
}