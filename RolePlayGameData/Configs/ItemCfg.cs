﻿using System.Collections.Generic;
using RolePlayGameData.Enums;
using RolePlayGameData.Models;

namespace RolePlayGameData.Configs
{
    public class ItemCfg
    {
        public static readonly List<ItemModel> ItemConfig = new List<ItemModel>
        {
            // #-Food-#
            new ItemModel
            {
                ItemName = ItemNameEnums.Apple, ItemType = ItemTypeEnums.Food, ItemFaction = FactionEnums.Civ,
                Description = "Nahrung", DisplayedName = "Apfel", DefaultPrice = 0.575f, CurrentPrice = 0.58f,
                MinPrice = 0.25f, MaxPrice = 0.9f, Weight = 0.25f, Value = 15f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Avocado, ItemType = ItemTypeEnums.Food, ItemFaction = FactionEnums.Civ,
                Description = "Nahrung", DisplayedName = "Avocado", DefaultPrice = 2.3f, CurrentPrice = 2.3f,
                MinPrice = 1.6f, MaxPrice = 3f, Weight = 0.25f, Value = 15f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.BakedMackerel, ItemType = ItemTypeEnums.Food, ItemFaction = FactionEnums.Civ,
                Description = "Nahrung", DisplayedName = "Gebackene Makrele", DefaultPrice = 0.01f,
                CurrentPrice = 0.01f, MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 0.7f, Value = 65f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.BakedPerchWithPotatoWedges, ItemType = ItemTypeEnums.Food,
                ItemFaction = FactionEnums.Civ, Description = "Nahrung",
                DisplayedName = "Gebackener Barsch mit Kartoffelecken", DefaultPrice = 0.01f, CurrentPrice = 0.01f,
                MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 0.7f, Value = 65f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Banane, ItemType = ItemTypeEnums.Food, ItemFaction = FactionEnums.Civ,
                Description = "Nahrung / Zutat", DisplayedName = "Banane", DefaultPrice = 1.2f, CurrentPrice = 1.2f,
                MinPrice = 0.4f, MaxPrice = 2f, Weight = 0.25f, Value = 15f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.BeefRoastWithMustardCrust, ItemType = ItemTypeEnums.Food,
                ItemFaction = FactionEnums.Civ, Description = "Nahrung", DisplayedName = "Rinderbraten mit Senfkruste",
                DefaultPrice = 0.01f, CurrentPrice = 0.01f, MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 1f, Value = 75f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.BeefSkewers, ItemType = ItemTypeEnums.Food, ItemFaction = FactionEnums.Civ,
                Description = "Nahrung", DisplayedName = "Rindfleischspieß", DefaultPrice = 0.01f, CurrentPrice = 0.01f,
                MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 0.5f, Value = 35f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.BraisedCodWithPotatoes, ItemType = ItemTypeEnums.Food,
                ItemFaction = FactionEnums.Civ, Description = "Nahrung",
                DisplayedName = "Gedünsteter Dorsch mit Kartoffeln", DefaultPrice = 0.01f, CurrentPrice = 0.01f,
                MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 1f, Value = 65f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Bun, ItemType = ItemTypeEnums.Food, ItemFaction = FactionEnums.Civ,
                Description = "Nahrung / Zutat", DisplayedName = "Brötchen", DefaultPrice = 0.495f, CurrentPrice = 0.5f,
                MinPrice = 0.29f, MaxPrice = 0.7f, Weight = 0.2f, Value = 10f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Burger, ItemType = ItemTypeEnums.Food, ItemFaction = FactionEnums.Civ,
                Description = "Nahrung", DisplayedName = "Hamburger", DefaultPrice = 3f, CurrentPrice = 3f,
                MinPrice = 1f, MaxPrice = 5f, Weight = 0.5f, Value = 45f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.CheeseBurger, ItemType = ItemTypeEnums.Food, ItemFaction = FactionEnums.Civ,
                Description = "Nahrung", DisplayedName = "Cheeseburger", DefaultPrice = 0.01f, CurrentPrice = 0.01f,
                MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 0.5f, Value = 50f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.CheeseFries, ItemType = ItemTypeEnums.Food, ItemFaction = FactionEnums.Civ,
                Description = "Nahrung", DisplayedName = "Käse-Pommes", DefaultPrice = 0.01f, CurrentPrice = 0.01f,
                MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 1f, Value = 40f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.CheeseSandwich, ItemType = ItemTypeEnums.Food, ItemFaction = FactionEnums.Civ,
                Description = "Nahrung", DisplayedName = "Käsesandwich", DefaultPrice = 0.01f, CurrentPrice = 0.01f,
                MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 0.5f, Value = 20f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.ChickenPastaBake, ItemType = ItemTypeEnums.Food,
                ItemFaction = FactionEnums.Civ, Description = "Nahrung", DisplayedName = "Hähnchen-Nudel-Auflauf",
                DefaultPrice = 0.01f, CurrentPrice = 0.01f, MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 1f, Value = 65f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Chocolate, ItemType = ItemTypeEnums.Food, ItemFaction = FactionEnums.Civ,
                Description = "Nahrung / Zutat", DisplayedName = "Schokoladentafel", DefaultPrice = 1.95f,
                CurrentPrice = 1.95f, MinPrice = 0.4f, MaxPrice = 3.5f, Weight = 0.25f, Value = 5f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.ChocolateBananaWrap, ItemType = ItemTypeEnums.Food,
                ItemFaction = FactionEnums.Civ, Description = "Nahrung", DisplayedName = "Schoko-Banane-Wrap",
                DefaultPrice = 0.01f, CurrentPrice = 0.01f, MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 0.5f,
                Value = 35f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.ChocolateCerealWitchMilk, ItemType = ItemTypeEnums.Food,
                ItemFaction = FactionEnums.Civ, Description = "Nahrung", DisplayedName = "Schoko-Müsli mit Milch",
                DefaultPrice = 0.01f, CurrentPrice = 0.01f, MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 0.5f,
                Value = 35f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.ChocolateCookie, ItemType = ItemTypeEnums.Food, ItemFaction = FactionEnums.Civ,
                Description = "Nahrung", DisplayedName = "Schoko-Keks", DefaultPrice = 0.01f, CurrentPrice = 0.01f,
                MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 0.25f, Value = 10f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.ChocolateFries, ItemType = ItemTypeEnums.Food, ItemFaction = FactionEnums.Civ,
                Description = "Nahrung", DisplayedName = "Schoko-Pommes", DefaultPrice = 0.01f, CurrentPrice = 0.01f,
                MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 0.5f, Value = 35f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Donut, ItemType = ItemTypeEnums.Food, ItemFaction = FactionEnums.Civ,
                Description = "Nahrung", DisplayedName = "Donut", DefaultPrice = 0.75f, CurrentPrice = 0.75f,
                MinPrice = 0.3f, MaxPrice = 1.2f, Weight = 0.3f, Value = 10f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.FishAndChips, ItemType = ItemTypeEnums.Food, ItemFaction = FactionEnums.Civ,
                Description = "Nahrung", DisplayedName = "Fish and Chips", DefaultPrice = 0.01f, CurrentPrice = 0.01f,
                MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 1f, Value = 50f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.FishCasseroleWithMushrooms, ItemType = ItemTypeEnums.Food,
                ItemFaction = FactionEnums.Civ, Description = "Nahrung", DisplayedName = "Fisch-Auflauf mit Pilzen",
                DefaultPrice = 0.01f, CurrentPrice = 0.01f, MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 1f, Value = 70f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.FriedCodWithVegetables, ItemType = ItemTypeEnums.Food,
                ItemFaction = FactionEnums.Civ, Description = "Nahrung", DisplayedName = "Gebratener Dorsch mit Gemüse",
                DefaultPrice = 0.01f, CurrentPrice = 0.01f, MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 1f, Value = 65f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.FriedPerchWithMushrooms, ItemType = ItemTypeEnums.Food,
                ItemFaction = FactionEnums.Civ, Description = "Nahrung", DisplayedName = "Gebratener Barsch mit Pilzen",
                DefaultPrice = 0.01f, CurrentPrice = 0.01f, MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 1f, Value = 65f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Grapevine, ItemType = ItemTypeEnums.Food, ItemFaction = FactionEnums.Civ,
                Description = "Nahrung", DisplayedName = "Weinrebe", DefaultPrice = 3.25f, CurrentPrice = 3.25f,
                MinPrice = 1.5f, MaxPrice = 5f, Weight = 0.5f, Value = 15f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.HalfGrilledChickenGratinatedCheese, ItemType = ItemTypeEnums.Food,
                ItemFaction = FactionEnums.Civ, Description = "Nahrung",
                DisplayedName = "Halbes Grillhähnchen Käse überbacken", DefaultPrice = 0.01f, CurrentPrice = 0.01f,
                MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 0.5f, Value = 45f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Lemon, ItemType = ItemTypeEnums.Food, ItemFaction = FactionEnums.Civ,
                Description = "Nahrung / Zutat", DisplayedName = "Zitrone", DefaultPrice = 0.825f, CurrentPrice = 0.83f,
                MinPrice = 0.45f, MaxPrice = 1.2f, Weight = 0.2f, Value = 5f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.MeatloafALaCordonBleu, ItemType = ItemTypeEnums.Food,
                ItemFaction = FactionEnums.Civ, Description = "Nahrung", DisplayedName = "Hackbraten a la Cordon bleu",
                DefaultPrice = 0.01f, CurrentPrice = 0.01f, MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 0.75f,
                Value = 65f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Orange, ItemType = ItemTypeEnums.Food, ItemFaction = FactionEnums.Civ,
                Description = "Nahrung", DisplayedName = "Orange", DefaultPrice = 1.4f, CurrentPrice = 1.4f,
                MinPrice = 0.8f, MaxPrice = 2f, Weight = 0.2f, Value = 5f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.PastaWithMackerelTomatoSauce, ItemType = ItemTypeEnums.Food,
                ItemFaction = FactionEnums.Civ, Description = "Nahrung",
                DisplayedName = "Pasta mit Makrelen-Tomatensauce", DefaultPrice = 0.01f, CurrentPrice = 0.01f,
                MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 1f, Value = 75f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Pistachio, ItemType = ItemTypeEnums.Food, ItemFaction = FactionEnums.Civ,
                Description = "Nahrung", DisplayedName = "Tüte Pistazien", DefaultPrice = 7f, CurrentPrice = 7f,
                MinPrice = 4.5f, MaxPrice = 9.5f, Weight = 0.1f, Value = 5f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.PizzaMargarita, ItemType = ItemTypeEnums.Food, ItemFaction = FactionEnums.Civ,
                Description = "Nahrung", DisplayedName = "Pizza Margarita", DefaultPrice = 0.01f, CurrentPrice = 0.01f,
                MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 1f, Value = 65f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.PorkChopsWithSpaetzle, ItemType = ItemTypeEnums.Food,
                ItemFaction = FactionEnums.Civ, Description = "Nahrung",
                DisplayedName = "Schweinegeschnetzeltes mit Spätzle", DefaultPrice = 0.01f, CurrentPrice = 0.01f,
                MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 1f, Value = 70f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.PorkSteakMeat, ItemType = ItemTypeEnums.Food, ItemFaction = FactionEnums.Civ,
                Description = "Nahrung", DisplayedName = "Schweineschnitzelfleisch", DefaultPrice = 0.01f,
                CurrentPrice = 0.01f, MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 1f, Value = 60f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.RoastedAlmonds, ItemType = ItemTypeEnums.Food, ItemFaction = FactionEnums.Civ,
                Description = "Nahrung", DisplayedName = "Geröstete Mandeln", DefaultPrice = 0.01f,
                CurrentPrice = 0.01f, MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 0.25f, Value = 20f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.SalamiSandwich, ItemType = ItemTypeEnums.Food, ItemFaction = FactionEnums.Civ,
                Description = "Nahrung", DisplayedName = "Salamisandwich", DefaultPrice = 0.01f, CurrentPrice = 0.01f,
                MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 0.25f, Value = 20f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Salad, ItemType = ItemTypeEnums.Food, ItemFaction = FactionEnums.Civ,
                Description = "Nahrung", DisplayedName = "Fertigsalat", DefaultPrice = 1.9f, CurrentPrice = 1.9f,
                MinPrice = 0.8f, MaxPrice = 3f, Weight = 0.25f, Value = 15f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.SalmonWrap, ItemType = ItemTypeEnums.Food, ItemFaction = FactionEnums.Civ,
                Description = "Nahrung", DisplayedName = "Lachs-Wrap", DefaultPrice = 0.01f, CurrentPrice = 0.01f,
                MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 0.5f, Value = 35f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.SpaghettiBolognese, ItemType = ItemTypeEnums.Food,
                ItemFaction = FactionEnums.Civ, Description = "Nahrung", DisplayedName = "Spaghetti Bolognese",
                DefaultPrice = 0.01f, CurrentPrice = 0.01f, MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 1f, Value = 55f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Strawberries, ItemType = ItemTypeEnums.Food, ItemFaction = FactionEnums.Civ,
                Description = "Nahrung / Zutat", DisplayedName = "Erdbeeren", DefaultPrice = 0.01f,
                CurrentPrice = 0.01f, MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 0.25f, Value = 10f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Tomato, ItemType = ItemTypeEnums.Food, ItemFaction = FactionEnums.Civ,
                Description = "Nahrung / Zutat", DisplayedName = "Tomate", DefaultPrice = 0.6f, CurrentPrice = 0.6f,
                MinPrice = 0.4f, MaxPrice = 0.8f, Weight = 0.15f, Value = 10f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.TurkeyWrap, ItemType = ItemTypeEnums.Food, ItemFaction = FactionEnums.Civ,
                Description = "Nahrung", DisplayedName = "Puten-Wrap", DefaultPrice = 0.01f, CurrentPrice = 0.01f,
                MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 0.5f, Value = 35f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Walnut, ItemType = ItemTypeEnums.Food, ItemFaction = FactionEnums.Civ,
                Description = "Nahrung", DisplayedName = "Tüte Walnusskerne", DefaultPrice = 2.5f, CurrentPrice = 2.5f,
                MinPrice = 1f, MaxPrice = 4f, Weight = 0.2f, Value = 10f
            },

            // #-SoftDrink-#
            new ItemModel
            {
                ItemName = ItemNameEnums.BananaMilkshake, ItemType = ItemTypeEnums.SoftDrink,
                ItemFaction = FactionEnums.Civ, Description = "SoftDrink", DisplayedName = "Bananen-Milchshake",
                DefaultPrice = 0.01f, CurrentPrice = 0.01f, MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 0.5f,
                Value = 45f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.BananaSmoothie, ItemType = ItemTypeEnums.SoftDrink,
                ItemFaction = FactionEnums.Civ, Description = "SoftDrink", DisplayedName = "Bananensmoothie",
                DefaultPrice = 0.01f, CurrentPrice = 0.01f, MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 0.5f,
                Value = 65f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.ChocolateMilkshake, ItemType = ItemTypeEnums.SoftDrink,
                ItemFaction = FactionEnums.Civ, Description = "SoftDrink", DisplayedName = "Schoko-Milchshake",
                DefaultPrice = 0.01f, CurrentPrice = 0.01f, MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 0.5f,
                Value = 45f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Coffee, ItemType = ItemTypeEnums.SoftDrink, ItemFaction = FactionEnums.Civ,
                Description = "SoftDrink", DisplayedName = "Kaffee", DefaultPrice = 0.49f, CurrentPrice = 0.49f,
                MinPrice = 0.3f, MaxPrice = 0.68f, Weight = 0.2f, Value = 15f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Cola, ItemType = ItemTypeEnums.SoftDrink, ItemFaction = FactionEnums.Civ,
                Description = "SoftDrink", DisplayedName = "Coke", DefaultPrice = 1.1f, CurrentPrice = 1.1f,
                MinPrice = 0.3f, MaxPrice = 1.9f, Weight = 0.5f, Value = 50f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.GrapeJuice, ItemType = ItemTypeEnums.SoftDrink, ItemFaction = FactionEnums.Civ,
                Description = "SoftDrink", DisplayedName = "Weinsaft", DefaultPrice = 1.15f, CurrentPrice = 0.65f,
                MinPrice = 0.3f, MaxPrice = 2f, Weight = 0.35f, Value = 10f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.HotChocolate, ItemType = ItemTypeEnums.SoftDrink,
                ItemFaction = FactionEnums.Civ, Description = "SoftDrink", DisplayedName = "Heiße Schokolade",
                DefaultPrice = 0.01f, CurrentPrice = 0.01f, MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 0.5f,
                Value = 45f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Smoothy, ItemType = ItemTypeEnums.SoftDrink, ItemFaction = FactionEnums.Civ,
                Description = "SoftDrink", DisplayedName = "Fruchtsmoothy", DefaultPrice = 1.2f, CurrentPrice = 1.2f,
                MinPrice = 0.5f, MaxPrice = 1.9f, Weight = 0.35f, Value = 5f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.StrawberryMilkshake, ItemType = ItemTypeEnums.SoftDrink,
                ItemFaction = FactionEnums.Civ, Description = "SoftDrink", DisplayedName = "Erdbeer-Milchshake",
                DefaultPrice = 0.01f, CurrentPrice = 0.01f, MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 0.5f,
                Value = 45f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.StrawberrySmoothie, ItemType = ItemTypeEnums.SoftDrink,
                ItemFaction = FactionEnums.Civ, Description = "SoftDrink", DisplayedName = "Erdbeersmoothie",
                DefaultPrice = 0.01f, CurrentPrice = 0.01f, MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 0.5f,
                Value = 65f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Water, ItemType = ItemTypeEnums.SoftDrink, ItemFaction = FactionEnums.Civ,
                Description = "SoftDrink", DisplayedName = "Mineralwasser", DefaultPrice = 0.5f, CurrentPrice = 0.5f,
                MinPrice = 0.2f, MaxPrice = 0.8f, Weight = 0.5f, Value = 25f
            },

            // #-Drink-#
            new ItemModel
            {
                ItemName = ItemNameEnums.Beer, ItemType = ItemTypeEnums.Drink, ItemFaction = FactionEnums.Civ,
                Description = "Alkohol", DisplayedName = "Bier", DefaultPrice = 2.7f, CurrentPrice = 2.7f,
                MinPrice = 0.6f, MaxPrice = 4.8f, Weight = 1f, Value = 7f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Martini, ItemType = ItemTypeEnums.Drink, ItemFaction = FactionEnums.Civ,
                Description = "Alkohol", DisplayedName = "Martini", DefaultPrice = 8.5f, CurrentPrice = 8.5f,
                MinPrice = 7f, MaxPrice = 10f, Weight = 0.75f, Value = 40f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Tequila, ItemType = ItemTypeEnums.Drink, ItemFaction = FactionEnums.Civ,
                Description = "Alkohol", DisplayedName = "Tequila", DefaultPrice = 6.9f, CurrentPrice = 6.9f,
                MinPrice = 3.8f, MaxPrice = 10f, Weight = 0.3f, Value = 20f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Vodka, ItemType = ItemTypeEnums.Drink, ItemFaction = FactionEnums.Civ,
                Description = "Alkohol", DisplayedName = "Vodka", DefaultPrice = 15.2f, CurrentPrice = 16.7f,
                MinPrice = 4.5f, MaxPrice = 25.9f, Weight = 0.75f, Value = 65f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Wine, ItemType = ItemTypeEnums.Drink, ItemFaction = FactionEnums.Civ,
                Description = "Alkohol", DisplayedName = "Wein", DefaultPrice = 9f, CurrentPrice = 9f, MinPrice = 4f,
                MaxPrice = 14f, Weight = 1f, Value = 15f
            },

            // #-Ingredient-#
            new ItemModel
            {
                ItemName = ItemNameEnums.BudBoost, ItemType = ItemTypeEnums.Ingredient, ItemFaction = FactionEnums.Civ,
                Description = "Zutat", DisplayedName = "Knospen-Booster", DefaultPrice = 13.75f, CurrentPrice = 13.75f,
                MinPrice = 11.75f, MaxPrice = 15.75f, Weight = 0.15f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Butter, ItemType = ItemTypeEnums.Ingredient, ItemFaction = FactionEnums.Civ,
                Description = "Zutat", DisplayedName = "Butter", DefaultPrice = 1.65f, CurrentPrice = 0.65f,
                MinPrice = 0.3f, MaxPrice = 3f, Weight = 0.15f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Cep, ItemType = ItemTypeEnums.Ingredient, ItemFaction = FactionEnums.Civ,
                Description = "Zutat", DisplayedName = "Steinpilz", DefaultPrice = 0.845f, CurrentPrice = 0.85f,
                MinPrice = 0.19f, MaxPrice = 1.5f, Weight = 0.2f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Champignon, ItemType = ItemTypeEnums.Ingredient,
                ItemFaction = FactionEnums.Civ, Description = "Zutat", DisplayedName = "Champignon",
                DefaultPrice = 1.1f, CurrentPrice = 1.1f, MinPrice = 0.2f, MaxPrice = 2f, Weight = 0.2f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Chanterelle, ItemType = ItemTypeEnums.Ingredient,
                ItemFaction = FactionEnums.Civ, Description = "Zutat", DisplayedName = "Pfifferling",
                DefaultPrice = 0.72f, CurrentPrice = 0.72f, MinPrice = 0.19f, MaxPrice = 1.25f, Weight = 0.2f,
                Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Cheese, ItemType = ItemTypeEnums.Ingredient, ItemFaction = FactionEnums.Civ,
                Description = "Zutat", DisplayedName = "Käse", DefaultPrice = 0.725f, CurrentPrice = 0.73f,
                MinPrice = 0.35f, MaxPrice = 1.1f, Weight = 0.1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.ChocolateMuesli, ItemType = ItemTypeEnums.Ingredient,
                ItemFaction = FactionEnums.Civ, Description = "Zutat", DisplayedName = "Schoko-Müsli",
                DefaultPrice = 10.25f, CurrentPrice = 10.25f, MinPrice = 4.5f, MaxPrice = 16f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.CookedHam, ItemType = ItemTypeEnums.Ingredient, ItemFaction = FactionEnums.Civ,
                Description = "Zutat", DisplayedName = "Kochschinken", DefaultPrice = 1.94f, CurrentPrice = 1.94f,
                MinPrice = 0.88f, MaxPrice = 3f, Weight = 0.3f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Cucumber, ItemType = ItemTypeEnums.Ingredient, ItemFaction = FactionEnums.Civ,
                Description = "Zutat", DisplayedName = "Gurke", DefaultPrice = 3.3f, CurrentPrice = 3.3f,
                MinPrice = 1.6f, MaxPrice = 5f, Weight = 0.3f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.FertilizerK, ItemType = ItemTypeEnums.Ingredient,
                ItemFaction = FactionEnums.Civ, Description = "Zutat", DisplayedName = "Dünger K",
                DefaultPrice = 131.5f, CurrentPrice = 131.5f, MinPrice = 117f, MaxPrice = 146f, Weight = 5f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.FertilizerM, ItemType = ItemTypeEnums.Ingredient,
                ItemFaction = FactionEnums.Civ, Description = "Zutat", DisplayedName = "Dünger M", DefaultPrice = 70.5f,
                CurrentPrice = 70.5f, MinPrice = 61f, MaxPrice = 80f, Weight = 5f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.FertilizerS, ItemType = ItemTypeEnums.Ingredient,
                ItemFaction = FactionEnums.Civ, Description = "Zutat", DisplayedName = "Dünger S",
                DefaultPrice = 154.5f, CurrentPrice = 154.5f, MinPrice = 132f, MaxPrice = 177f, Weight = 5f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Flour, ItemType = ItemTypeEnums.Ingredient, ItemFaction = FactionEnums.Civ,
                Description = "Zutat", DisplayedName = "Mehl", DefaultPrice = 2.25f, CurrentPrice = 2.25f,
                MinPrice = 1.5f, MaxPrice = 3f, Weight = 0.5f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.GrowthBoost, ItemType = ItemTypeEnums.Ingredient,
                ItemFaction = FactionEnums.Civ, Description = "Zutat", DisplayedName = "Wachstums-Booster",
                DefaultPrice = 13.75f, CurrentPrice = 13.75f, MinPrice = 11.75f, MaxPrice = 15.75f, Weight = 0.15f,
                Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.JointPaper, ItemType = ItemTypeEnums.Ingredient,
                ItemFaction = FactionEnums.Civ, Description = "Zutat", DisplayedName = "Paper", DefaultPrice = 7.75f,
                CurrentPrice = 10f, MinPrice = 0.5f, MaxPrice = 15f, Weight = 0.1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Methanol, ItemType = ItemTypeEnums.Ingredient, ItemFaction = FactionEnums.Civ,
                Description = "Zutat", DisplayedName = "Methanol", DefaultPrice = 7.25f, CurrentPrice = 7.25f,
                MinPrice = 2.5f, MaxPrice = 12f, Weight = 10f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Mustard, ItemType = ItemTypeEnums.Ingredient, ItemFaction = FactionEnums.Civ,
                Description = "Zutat", DisplayedName = "Senf", DefaultPrice = 1.9f, CurrentPrice = 1.9f,
                MinPrice = 1.3f, MaxPrice = 2.5f, Weight = 0.25f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Noodles, ItemType = ItemTypeEnums.Ingredient, ItemFaction = FactionEnums.Civ,
                Description = "Zutat", DisplayedName = "Nudeln", DefaultPrice = 8.25f, CurrentPrice = 8.25f,
                MinPrice = 1.5f, MaxPrice = 15f, Weight = 0.5f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.OliveOil, ItemType = ItemTypeEnums.Ingredient, ItemFaction = FactionEnums.Civ,
                Description = "Zutat", DisplayedName = "Olivenöl", DefaultPrice = 8.5f, CurrentPrice = 0f,
                MinPrice = 7f, MaxPrice = 10f, Weight = 0.5f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Fries, ItemType = ItemTypeEnums.Ingredient, ItemFaction = FactionEnums.Civ,
                Description = "Zutat", DisplayedName = "Pommes", DefaultPrice = 0.01f, CurrentPrice = 0.01f,
                MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Salami, ItemType = ItemTypeEnums.Ingredient, ItemFaction = FactionEnums.Civ,
                Description = "Zutat", DisplayedName = "Salami", DefaultPrice = 0.8f, CurrentPrice = 0.8f,
                MinPrice = 0.4f, MaxPrice = 1.2f, Weight = 0.1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Solvent, ItemType = ItemTypeEnums.Ingredient, ItemFaction = FactionEnums.Civ,
                Description = "Zutat", DisplayedName = "Lösungsmittel", DefaultPrice = 50f, CurrentPrice = 50f,
                MinPrice = 25f, MaxPrice = 75f, Weight = 0.1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.TurkeyMeat, ItemType = ItemTypeEnums.Ingredient,
                ItemFaction = FactionEnums.Civ, Description = "Zutat", DisplayedName = "Putenfleisch",
                DefaultPrice = 0.01f, CurrentPrice = 0.01f, MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 0.1f,
                Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Yeast, ItemType = ItemTypeEnums.Ingredient, ItemFaction = FactionEnums.Civ,
                Description = "Zutat", DisplayedName = "Hefewürfel", DefaultPrice = 1f, CurrentPrice = 1f,
                MinPrice = 0.5f, MaxPrice = 1.5f, Weight = 0.25f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Yogurt, ItemType = ItemTypeEnums.Ingredient, ItemFaction = FactionEnums.Civ,
                Description = "Zutat", DisplayedName = "Joghurt", DefaultPrice = 2f, CurrentPrice = 2f, MinPrice = 0.8f,
                MaxPrice = 3.2f, Weight = 0.25f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Wrap, ItemType = ItemTypeEnums.Ingredient, ItemFaction = FactionEnums.Civ,
                Description = "Zutat", DisplayedName = "Wrap", DefaultPrice = 0.4f, CurrentPrice = 0.4f,
                MinPrice = 0.2f, MaxPrice = 0.6f, Weight = 0.2f, Value = 0f
            },

            // #-Tool-#
            new ItemModel
            {
                ItemName = ItemNameEnums.Axe, ItemType = ItemTypeEnums.Tool, ItemFaction = FactionEnums.Civ,
                Description = "Werkzeug", DisplayedName = "Axt", DefaultPrice = 145f, CurrentPrice = 145f,
                MinPrice = 40f, MaxPrice = 250f, Weight = 5f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.BatteryPack, ItemType = ItemTypeEnums.Tool, ItemFaction = FactionEnums.Civ,
                Description = "Werkzeug", DisplayedName = "Akku", DefaultPrice = 80f, CurrentPrice = 80f,
                MinPrice = 10f, MaxPrice = 150f, Weight = 0.5f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.FishingRod, ItemType = ItemTypeEnums.Tool, ItemFaction = FactionEnums.Civ,
                Description = "Werkzeug", DisplayedName = "Angel", DefaultPrice = 145f, CurrentPrice = 145f,
                MinPrice = 40f, MaxPrice = 250f, Weight = 5f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.FuelCan, ItemType = ItemTypeEnums.Tool, ItemFaction = FactionEnums.Civ,
                Description = "Werkzeug", DisplayedName = "Benzinkanister", DefaultPrice = 145f, CurrentPrice = 145f,
                MinPrice = 40f, MaxPrice = 250f, Weight = 5f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Gloves, ItemType = ItemTypeEnums.Tool, ItemFaction = FactionEnums.Civ,
                Description = "Werkzeug", DisplayedName = "Handschuhe", DefaultPrice = 145f, CurrentPrice = 145f,
                MinPrice = 40f, MaxPrice = 250f, Weight = 5f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.LockPick, ItemType = ItemTypeEnums.Tool, ItemFaction = FactionEnums.Civ,
                Description = "Werkzeug", DisplayedName = "Dietrich", DefaultPrice = 145f, CurrentPrice = 145f,
                MinPrice = 40f, MaxPrice = 250f, Weight = 5f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Sickle, ItemType = ItemTypeEnums.Tool, ItemFaction = FactionEnums.Civ,
                Description = "Werkzeug", DisplayedName = "Sichel", DefaultPrice = 145f, CurrentPrice = 145f,
                MinPrice = 40f, MaxPrice = 250f, Weight = 5f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Spade, ItemType = ItemTypeEnums.Tool, ItemFaction = FactionEnums.Civ,
                Description = "Werkzeug", DisplayedName = "Spaten", DefaultPrice = 145f, CurrentPrice = 145f,
                MinPrice = 40f, MaxPrice = 250f, Weight = 5f, Value = 0f
            },

            // #-Medical-#
            new ItemModel
            {
                ItemName = ItemNameEnums.Bandage, ItemType = ItemTypeEnums.Medical, ItemFaction = FactionEnums.Civ,
                Description = "Medizin", DisplayedName = "Verband", DefaultPrice = 15f, CurrentPrice = 15f,
                MinPrice = 10f, MaxPrice = 20f, Weight = 1f, Value = 10f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.FirstAidKit, ItemType = ItemTypeEnums.Medical, ItemFaction = FactionEnums.Lsrs,
                Description = "Medizin", DisplayedName = "Erste-Hilfe-Kasten", DefaultPrice = 80f, CurrentPrice = 70f,
                MinPrice = 40f, MaxPrice = 120f, Weight = 5f, Value = 50f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.MedicalSuitcase, ItemType = ItemTypeEnums.Medical,
                ItemFaction = FactionEnums.Lsrs, Description = "Medizin", DisplayedName = "Medizinkoffer",
                DefaultPrice = 325f, CurrentPrice = 325f, MinPrice = 150f, MaxPrice = 500f, Weight = 15f, Value = 100f
            },

            // #-Technical-#
            new ItemModel
            {
                ItemName = ItemNameEnums.RepairKit, ItemType = ItemTypeEnums.Technical, ItemFaction = FactionEnums.Lsc,
                Description = "Technikel", DisplayedName = "Reperatur-Paket", DefaultPrice = 575f, CurrentPrice = 575f,
                MinPrice = 150f, MaxPrice = 1000f, Weight = 15f, Value = 100f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.ToolKit, ItemType = ItemTypeEnums.Technical, ItemFaction = FactionEnums.Civ,
                Description = "Technikel", DisplayedName = "Werkzeugkasten", DefaultPrice = 145f, CurrentPrice = 145f,
                MinPrice = 40f, MaxPrice = 250f, Weight = 5f, Value = 25f
            },

            // #-Smoke-#
            new ItemModel
            {
                ItemName = ItemNameEnums.Cigar, ItemType = ItemTypeEnums.Smoke, ItemFaction = FactionEnums.Civ,
                Description = "Tabak", DisplayedName = "Zigare", DefaultPrice = 3.25f, CurrentPrice = 3.25f,
                MinPrice = 1.5f, MaxPrice = 5f, Weight = 0.1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Cigarette, ItemType = ItemTypeEnums.Smoke, ItemFaction = FactionEnums.Civ,
                Description = "Tabak", DisplayedName = "Zigarette", DefaultPrice = 0.01f, CurrentPrice = 0.01f,
                MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 0.01f, Value = 0f
            },

            // #-Drug-#
            new ItemModel
            {
                ItemName = ItemNameEnums.AmnesiaHazeJoint, ItemType = ItemTypeEnums.Drug,
                ItemFaction = FactionEnums.Civ, Description = "Droge", DisplayedName = "Amnesia Haze Joint",
                DefaultPrice = 0.01f, CurrentPrice = 0.01f, MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 0.1f,
                Value = 5f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Cocaine, ItemType = ItemTypeEnums.Drug, ItemFaction = FactionEnums.Civ,
                Description = "Droge", DisplayedName = "Kokain", DefaultPrice = 10367.5f, CurrentPrice = 10367.5f,
                MinPrice = 1885f, MaxPrice = 18850f, Weight = 0.25f, Value = 10f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.CriticalJoint, ItemType = ItemTypeEnums.Drug, ItemFaction = FactionEnums.Civ,
                Description = "Droge", DisplayedName = "Critical Joint", DefaultPrice = 0.01f, CurrentPrice = 0.01f,
                MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 0.1f, Value = 5f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.CrystalMeth, ItemType = ItemTypeEnums.Drug, ItemFaction = FactionEnums.Civ,
                Description = "Droge", DisplayedName = "CrystelMeth", DefaultPrice = 1306.25f, CurrentPrice = 1306.25f,
                MinPrice = 237.5f, MaxPrice = 2375f, Weight = 0.25f, Value = 50f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Ecstasy, ItemType = ItemTypeEnums.Drug, ItemFaction = FactionEnums.Civ,
                Description = "Droge", DisplayedName = "Ecstasy", DefaultPrice = 1100f, CurrentPrice = 1100f,
                MinPrice = 200f, MaxPrice = 2000f, Weight = 0.25f, Value = 20f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.GlueHead, ItemType = ItemTypeEnums.Drug, ItemFaction = FactionEnums.Civ,
                Description = "Droge", DisplayedName = "Klebkopf", DefaultPrice = 136f, CurrentPrice = 136f,
                MinPrice = 47f, MaxPrice = 225f, Weight = 0.25f, Value = 15f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.HollandsHopeJoint, ItemType = ItemTypeEnums.Drug,
                ItemFaction = FactionEnums.Civ, Description = "Droge", DisplayedName = "Holands Hope Joint",
                DefaultPrice = 0.01f, CurrentPrice = 0.01f, MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 0.1f,
                Value = 5f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Lsd, ItemType = ItemTypeEnums.Drug, ItemFaction = FactionEnums.Civ,
                Description = "Droge", DisplayedName = "LSD", DefaultPrice = 1056f, CurrentPrice = 1056f,
                MinPrice = 192f, MaxPrice = 1920f, Weight = 0.25f, Value = 20f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.MagicMushroom, ItemType = ItemTypeEnums.Drug, ItemFaction = FactionEnums.Civ,
                Description = "Droge", DisplayedName = "Zauberpilz", DefaultPrice = 209f, CurrentPrice = 209f,
                MinPrice = 68f, MaxPrice = 350f, Weight = 0.25f, Value = 25f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.OGKuschJoint, ItemType = ItemTypeEnums.Drug, ItemFaction = FactionEnums.Civ,
                Description = "Droge", DisplayedName = "OG Kush Joint", DefaultPrice = 0.01f, CurrentPrice = 0.01f,
                MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 0.1f, Value = 5f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.SpecialQueenJoint, ItemType = ItemTypeEnums.Drug,
                ItemFaction = FactionEnums.Civ, Description = "Droge", DisplayedName = "Special Queen Joint",
                DefaultPrice = 0.01f, CurrentPrice = 0.01f, MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 0.1f,
                Value = 5f
            },

            // #-JobItem-#
            new ItemModel
            {
                ItemName = ItemNameEnums.AlmondsSack, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Endprodukt", DisplayedName = "Sack Mandeln", DefaultPrice = 11f, CurrentPrice = 11f,
                MinPrice = 5f, MaxPrice = 17f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.AmnesiaHazeBud, ItemType = ItemTypeEnums.JobItem,
                ItemFaction = FactionEnums.Civ, Description = "Bestandteil", DisplayedName = "Amnesia Haze Knospe",
                DefaultPrice = 500f, CurrentPrice = 500f, MinPrice = 400f, MaxPrice = 600f, Weight = 0.15f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.AmnesiaHazeSeed, ItemType = ItemTypeEnums.JobItem,
                ItemFaction = FactionEnums.Civ, Description = "Bestandteil", DisplayedName = "Amnesia Haze Samen",
                DefaultPrice = 10f, CurrentPrice = 10f, MinPrice = 8f, MaxPrice = 12f, Weight = 0.01f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.AppleJuice, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Endprodukt", DisplayedName = "Apfelsaft", DefaultPrice = 1.94f, CurrentPrice = 1.94f,
                MinPrice = 0.88f, MaxPrice = 3f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Benzoylchloride, ItemType = ItemTypeEnums.JobItem,
                ItemFaction = FactionEnums.Civ, Description = "Endprodukt", DisplayedName = "Benzoylchlorid",
                DefaultPrice = 0.01f, CurrentPrice = 0.01f, MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.BucketGravel, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Endprodukt", DisplayedName = "Eimer Kies", DefaultPrice = 0.01f, CurrentPrice = 0.01f,
                MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 5f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.BucketSand, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Endprodukt", DisplayedName = "Eimer Sand", DefaultPrice = 0.01f, CurrentPrice = 0.01f,
                MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 5f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.CardBoard, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Endprodukt", DisplayedName = "Pappe", DefaultPrice = 0.6f, CurrentPrice = 0.6f,
                MinPrice = 0.2f, MaxPrice = 1f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.CattleMeat, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Endprodukt / Zutat", DisplayedName = "Rindfleisch", DefaultPrice = 41f,
                CurrentPrice = 41f, MinPrice = 30f, MaxPrice = 52f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Cement, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Endprodukt", DisplayedName = "Zement", DefaultPrice = 4.5f, CurrentPrice = 4.5f,
                MinPrice = 3f, MaxPrice = 6f, Weight = 3f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Chair, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Endprodukt", DisplayedName = "Stuhl", DefaultPrice = 105f, CurrentPrice = 105f,
                MinPrice = 10f, MaxPrice = 200f, Weight = 3f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Chicken, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Bestandteil", DisplayedName = "Hühnchen", DefaultPrice = 12.5f, CurrentPrice = 12.5f,
                MinPrice = 10f, MaxPrice = 15f, Weight = 25f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.ChickenMeat, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Endprodukt / Zutat", DisplayedName = "Hühnchenfleisch", DefaultPrice = 9f,
                CurrentPrice = 9f, MinPrice = 6f, MaxPrice = 12f, Weight = 2f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.CocaineLeaves, ItemType = ItemTypeEnums.JobItem,
                ItemFaction = FactionEnums.Civ, Description = "Bestandteil", DisplayedName = "Kokain Blätter",
                DefaultPrice = 0.01f, CurrentPrice = 0.01f, MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.CocaineSeeds, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Endprodukt", DisplayedName = "Kokainsamen", DefaultPrice = 0.01f, CurrentPrice = 0.01f,
                MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 0.5f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Cod, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Endprodukt / Zutat", DisplayedName = "Dorsch", DefaultPrice = 11.5f,
                CurrentPrice = 11.5f, MinPrice = 8f, MaxPrice = 15f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.ContainerButter, ItemType = ItemTypeEnums.JobItem,
                ItemFaction = FactionEnums.Civ, Description = "Endprodukt", DisplayedName = "Behälter Butter",
                DefaultPrice = 5.5f, CurrentPrice = 5.5f, MinPrice = 3f, MaxPrice = 8f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.ContainerCream, ItemType = ItemTypeEnums.JobItem,
                ItemFaction = FactionEnums.Civ, Description = "Endprodukt", DisplayedName = "Behälter Sahne",
                DefaultPrice = 8f, CurrentPrice = 8f, MinPrice = 4f, MaxPrice = 12f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.CopperIngot, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Bestandteil", DisplayedName = "Kupfer Barren", DefaultPrice = 0.01f,
                CurrentPrice = 0.01f, MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.CopperFlakes, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Bestandteil", DisplayedName = "Kupferflocken", DefaultPrice = 0.01f,
                CurrentPrice = 0.01f, MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 0.2f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.CopperWire, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Endprodukt", DisplayedName = "Kupferkabel", DefaultPrice = 19.6f, CurrentPrice = 19.6f,
                MinPrice = 4.2f, MaxPrice = 35f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Cordial, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Endprodukt", DisplayedName = "Fruchtlikör", DefaultPrice = 33f, CurrentPrice = 33f,
                MinPrice = 6f, MaxPrice = 60f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Cow, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Bestandteil", DisplayedName = "Rind", DefaultPrice = 1000f, CurrentPrice = 1000f,
                MinPrice = 200f, MaxPrice = 1800f, Weight = 150f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.CriticalBud, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Bestandteil", DisplayedName = "Critical Knospe", DefaultPrice = 450f,
                CurrentPrice = 450f, MinPrice = 350f, MaxPrice = 550f, Weight = 0.15f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.CriticalSeed, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Bestandteil", DisplayedName = "Critical Samen", DefaultPrice = 9f, CurrentPrice = 9f,
                MinPrice = 7f, MaxPrice = 11f, Weight = 0.01f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.DiamondRing, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Bestandteil", DisplayedName = "Diamant Ring", DefaultPrice = 101250f,
                CurrentPrice = 101250f, MinPrice = 2500f, MaxPrice = 200000f, Weight = 0.1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Ephedrine, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Bestandteil", DisplayedName = "Ephedrin", DefaultPrice = 0.01f, CurrentPrice = 0.01f,
                MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Ergot, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Bestandteil", DisplayedName = "Mutterkorn", DefaultPrice = 0.01f, CurrentPrice = 0.01f,
                MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 0.2f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.FlourSack, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Endprodukt", DisplayedName = "Sack Mehl", DefaultPrice = 12.5f, CurrentPrice = 0f,
                MinPrice = 5f, MaxPrice = 20f, Weight = 10f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Frog, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Endprodukt", DisplayedName = "Frosch", DefaultPrice = 32.5f, CurrentPrice = 32.5f,
                MinPrice = 25f, MaxPrice = 40f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.GoldIngot, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Bestandteil", DisplayedName = "Goldbarren", DefaultPrice = 0.01f, CurrentPrice = 0.01f,
                MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.GoldChain, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Endprodukt", DisplayedName = "Goldkette", DefaultPrice = 34150f, CurrentPrice = 34150f,
                MinPrice = 300f, MaxPrice = 68000f, Weight = 0.3f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.GoldCoins, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Endprodukt", DisplayedName = "Goldmünzen", DefaultPrice = 3750f, CurrentPrice = 3750f,
                MinPrice = 1500f, MaxPrice = 6000f, Weight = 0.1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.GoldFlakes, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Bestandteil", DisplayedName = "Goldflocken", DefaultPrice = 0.01f, CurrentPrice = 0.01f,
                MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 0.2f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.HardCoal, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Bestandteil", DisplayedName = "Steinkohle", DefaultPrice = 0.01f, CurrentPrice = 0.01f,
                MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.HollandsHopeBud, ItemType = ItemTypeEnums.JobItem,
                ItemFaction = FactionEnums.Civ, Description = "Bestandteil", DisplayedName = "Hollands Hope Knospe",
                DefaultPrice = 375f, CurrentPrice = 375f, MinPrice = 275f, MaxPrice = 475f, Weight = 0.15f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.HollandsHopeSeed, ItemType = ItemTypeEnums.JobItem,
                ItemFaction = FactionEnums.Civ, Description = "Bestandteil", DisplayedName = "Hollands Hope Samen",
                DefaultPrice = 7.5f, CurrentPrice = 7.5f, MinPrice = 5.5f, MaxPrice = 9.5f, Weight = 0.01f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.IronFlakes, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Bestandteil", DisplayedName = "Eisenflocken", DefaultPrice = 0.01f, CurrentPrice = 0.01f,
                MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 0.2f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.IronIngot, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Bestandteil", DisplayedName = "Eisenbarren", DefaultPrice = 0.01f, CurrentPrice = 0.01f,
                MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.IronPipe, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Endprodukt", DisplayedName = "Eisenrohr", DefaultPrice = 32.5f, CurrentPrice = 32.5f,
                MinPrice = 15f, MaxPrice = 50f, Weight = 1.5f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.IronPlate, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Endprodukt", DisplayedName = "Eisenplatte", DefaultPrice = 50f, CurrentPrice = 50f,
                MinPrice = 25f, MaxPrice = 75f, Weight = 3f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Log, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Bestandteil", DisplayedName = "Baumstamm", DefaultPrice = 0.01f, CurrentPrice = 0.01f,
                MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 10f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.LysergicAcid, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Bestandteil", DisplayedName = "LysergicAcid", DefaultPrice = 0.01f, CurrentPrice = 0.01f,
                MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Mackerel, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Endprodukt", DisplayedName = "Makrele", DefaultPrice = 18.75f, CurrentPrice = 18.75f,
                MinPrice = 12.5f, MaxPrice = 25f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Methylamine, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Bestandteil", DisplayedName = "Methylamin", DefaultPrice = 0.01f, CurrentPrice = 0.01f,
                MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Methylbenzodioxolbutanamin, ItemType = ItemTypeEnums.JobItem,
                ItemFaction = FactionEnums.Civ, Description = "Bestandteil",
                DisplayedName = "Methylbenzodioxolbutanamin", DefaultPrice = 0.01f, CurrentPrice = 0.01f,
                MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Methylendioxyethylamphetamin, ItemType = ItemTypeEnums.JobItem,
                ItemFaction = FactionEnums.Civ, Description = "Bestandteil",
                DisplayedName = "Methylendioxyethylamphetamin", DefaultPrice = 0.01f, CurrentPrice = 0.01f,
                MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Methylendioxymethampheamin, ItemType = ItemTypeEnums.JobItem,
                ItemFaction = FactionEnums.Civ, Description = "Bestandteil",
                DisplayedName = "Methylendioxymethampheamin", DefaultPrice = 0.01f, CurrentPrice = 0.01f,
                MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Milk, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Endprodukt / Zutat", DisplayedName = "Milch", DefaultPrice = 5.16f, CurrentPrice = 5.16f,
                MinPrice = 0.32f, MaxPrice = 10f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.OGKushBud, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Bestandteil", DisplayedName = "OG Kush Knospe", DefaultPrice = 425f, CurrentPrice = 425f,
                MinPrice = 325f, MaxPrice = 525f, Weight = 0.15f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.OGKushSeed, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Bestandteil", DisplayedName = "OG Kush Samen", DefaultPrice = 8.5f, CurrentPrice = 8.5f,
                MinPrice = 6.5f, MaxPrice = 10.5f, Weight = 0.01f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.OrangeJuice, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Endprodukt", DisplayedName = "Orangensaft", DefaultPrice = 5.25f, CurrentPrice = 5.25f,
                MinPrice = 0.5f, MaxPrice = 10f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Pane, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Endprodukt", DisplayedName = "Glasscheibe", DefaultPrice = 105f, CurrentPrice = 105f,
                MinPrice = 50f, MaxPrice = 160f, Weight = 3f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Paper, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Endprodukt", DisplayedName = "Papier", DefaultPrice = 1.95f, CurrentPrice = 1.95f,
                MinPrice = 0.9f, MaxPrice = 3f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Perch, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Endprodukt", DisplayedName = "Barsch", DefaultPrice = 13.75f, CurrentPrice = 13.75f,
                MinPrice = 9f, MaxPrice = 18.5f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Phenylacetone, ItemType = ItemTypeEnums.JobItem,
                ItemFaction = FactionEnums.Civ, Description = "Bestandteil", DisplayedName = "Phenylaceton",
                DefaultPrice = 300f, CurrentPrice = 0f, MinPrice = 120f, MaxPrice = 480f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Pig, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Bestandteil", DisplayedName = "Schwein", DefaultPrice = 113.5f, CurrentPrice = 113.5f,
                MinPrice = 60f, MaxPrice = 167f, Weight = 75f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.PistachioSack, ItemType = ItemTypeEnums.JobItem,
                ItemFaction = FactionEnums.Civ, Description = "Endprodukt", DisplayedName = "Sack Pistazien",
                DefaultPrice = 25.45f, CurrentPrice = 25.45f, MinPrice = 20.9f, MaxPrice = 30f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Pork, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Bestandteil / Zutat", DisplayedName = "Schweinefleisch", DefaultPrice = 15.5f,
                CurrentPrice = 0f, MinPrice = 6f, MaxPrice = 25f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.PotatoSack, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Bestandteil / Zutat", DisplayedName = "Sack Kartoffeln", DefaultPrice = 0.01f,
                CurrentPrice = 0.01f, MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.PotatoSchnapps, ItemType = ItemTypeEnums.JobItem,
                ItemFaction = FactionEnums.Civ, Description = "Endprodukt", DisplayedName = "Kartoffelschnaps",
                DefaultPrice = 51f, CurrentPrice = 51f, MinPrice = 12f, MaxPrice = 90f, Weight = 5f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.RawMilk, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Bestandteil", DisplayedName = "Rohmilch", DefaultPrice = 0.01f, CurrentPrice = 0.01f,
                MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 2f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Rock, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Bestandteil", DisplayedName = "Gestein", DefaultPrice = 0.01f, CurrentPrice = 0.01f,
                MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 10f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.RoughDiamond, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Endprodukt / Bestandteil", DisplayedName = "Rohdiamant", DefaultPrice = 26260f,
                CurrentPrice = 26260f, MinPrice = 20250f, MaxPrice = 32270f, Weight = 0.2f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Salmon, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Endprodukt / Bestandteil", DisplayedName = "Lachs", DefaultPrice = 18f,
                CurrentPrice = 18f, MinPrice = 11f, MaxPrice = 25f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Sawdust, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Bestandteil", DisplayedName = "Sägespäne", DefaultPrice = 0.01f, CurrentPrice = 0.01f,
                MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Shelf, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Endprodukt", DisplayedName = "Regal", DefaultPrice = 150f, CurrentPrice = 150f,
                MinPrice = 20f, MaxPrice = 280f, Weight = 10f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Shell, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Endprodukt", DisplayedName = "Muscheln", DefaultPrice = 15.25f, CurrentPrice = 15.25f,
                MinPrice = 5.5f, MaxPrice = 25f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.SilverCutlery, ItemType = ItemTypeEnums.JobItem,
                ItemFaction = FactionEnums.Civ, Description = "Endprodukt", DisplayedName = "Silberbesteck",
                DefaultPrice = 21.5f, CurrentPrice = 21.5f, MinPrice = 13f, MaxPrice = 30f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.SilverFlakes, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Bestandteil", DisplayedName = "Silberflocken", DefaultPrice = 0.01f,
                CurrentPrice = 0.01f, MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 0.2f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.SilverIngot, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Endprodukt", DisplayedName = "Silberbarren", DefaultPrice = 0.01f, CurrentPrice = 0.01f,
                MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.SpecialQueenBud, ItemType = ItemTypeEnums.JobItem,
                ItemFaction = FactionEnums.Civ, Description = "Bestandteil", DisplayedName = "Special Queen Knospe",
                DefaultPrice = 225f, CurrentPrice = 225f, MinPrice = 125f, MaxPrice = 325f, Weight = 0.15f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.SpecialQueenSeed, ItemType = ItemTypeEnums.JobItem,
                ItemFaction = FactionEnums.Civ, Description = "Endprodukt", DisplayedName = "Special Queen Samen",
                DefaultPrice = 4.5f, CurrentPrice = 4.5f, MinPrice = 2.5f, MaxPrice = 6.5f, Weight = 0.01f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Table, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Endprodukt", DisplayedName = "Tisch", DefaultPrice = 475f, CurrentPrice = 475f,
                MinPrice = 100f, MaxPrice = 850f, Weight = 15f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.TomatoJuice, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Endprodukt", DisplayedName = "Tomatensaft", DefaultPrice = 10.4f, CurrentPrice = 10.4f,
                MinPrice = 0.8f, MaxPrice = 20f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.WalnutKernelsSack, ItemType = ItemTypeEnums.JobItem,
                ItemFaction = FactionEnums.Civ, Description = "Endprodukt", DisplayedName = "Sack Walnusskerne",
                DefaultPrice = 13.5f, CurrentPrice = 13.5f, MinPrice = 3f, MaxPrice = 24f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.WalnutsSack, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Bestandteil", DisplayedName = "Sack Walnüsse", DefaultPrice = 0.01f,
                CurrentPrice = 0.01f, MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 10f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Wheat, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Bestandteil", DisplayedName = "Weizen", DefaultPrice = 0f, CurrentPrice = 0f,
                MinPrice = 0f, MaxPrice = 0f, Weight = 3f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.WhiteWidowBud, ItemType = ItemTypeEnums.JobItem,
                ItemFaction = FactionEnums.Civ, Description = "Bestandteil", DisplayedName = "White Widow Knospe",
                DefaultPrice = 400f, CurrentPrice = 400f, MinPrice = 300f, MaxPrice = 500f, Weight = 0.15f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.WhiteWidowSeed, ItemType = ItemTypeEnums.JobItem,
                ItemFaction = FactionEnums.Civ, Description = "Endprodukt", DisplayedName = "White Widow Samen",
                DefaultPrice = 9f, CurrentPrice = 9f, MinPrice = 8f, MaxPrice = 10f, Weight = 0.01f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.WoodenBeam, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Bestandteil", DisplayedName = "Holzbalken", DefaultPrice = 0.01f, CurrentPrice = 0.01f,
                MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 5f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.WoodenBoards, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Bestandteil", DisplayedName = "Holzbretter", DefaultPrice = 0.01f, CurrentPrice = 0.01f,
                MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 2f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.WoodenSlats, ItemType = ItemTypeEnums.JobItem, ItemFaction = FactionEnums.Civ,
                Description = "Bestandteil", DisplayedName = "Holzlatten", DefaultPrice = 0.01f, CurrentPrice = 0.01f,
                MinPrice = 0.01f, MaxPrice = 0.01f, Weight = 1f, Value = 0f
            },

            // #-CrateItem-#
            new ItemModel
            {
                ItemName = ItemNameEnums.AppleCrate, ItemType = ItemTypeEnums.CrateItem, ItemFaction = FactionEnums.Civ,
                Description = "Paket", DisplayedName = "Kiste Äpfel", DefaultPrice = 15f, CurrentPrice = 15f,
                MinPrice = 5f, MaxPrice = 25f, Weight = 10f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.AvocadoCrate, ItemType = ItemTypeEnums.CrateItem,
                ItemFaction = FactionEnums.Civ, Description = "Paket", DisplayedName = "Kiste Avokados",
                DefaultPrice = 22f, CurrentPrice = 22f, MinPrice = 4f, MaxPrice = 40f, Weight = 2f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.CigarettePack, ItemType = ItemTypeEnums.CrateItem,
                ItemFaction = FactionEnums.Civ, Description = "Paket", DisplayedName = "Zigarettenschachtel",
                DefaultPrice = 8.6f, CurrentPrice = 8.6f, MinPrice = 5.2f, MaxPrice = 12f, Weight = 0.4f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.GrapevineCrate, ItemType = ItemTypeEnums.CrateItem,
                ItemFaction = FactionEnums.Civ, Description = "Paket", DisplayedName = "Kiste Weinreben",
                DefaultPrice = 26f, CurrentPrice = 26f, MinPrice = 12f, MaxPrice = 40f, Weight = 5f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.LemonSack, ItemType = ItemTypeEnums.CrateItem, ItemFaction = FactionEnums.Civ,
                Description = "Paket", DisplayedName = "Sack Zitronen", DefaultPrice = 10f, CurrentPrice = 10f,
                MinPrice = 5f, MaxPrice = 15f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.OrangeSack, ItemType = ItemTypeEnums.CrateItem, ItemFaction = FactionEnums.Civ,
                Description = "Paket", DisplayedName = "Sack Orangen", DefaultPrice = 12.5f, CurrentPrice = 12.5f,
                MinPrice = 10f, MaxPrice = 15f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.TomatoCrate, ItemType = ItemTypeEnums.CrateItem,
                ItemFaction = FactionEnums.Civ, Description = "Paket", DisplayedName = "Kiste Tomaten",
                DefaultPrice = 15f, CurrentPrice = 15f, MinPrice = 5f, MaxPrice = 25f, Weight = 5f, Value = 0f
            },

            // #-Weapon-#
            new ItemModel
            {
                ItemName = ItemNameEnums.Bat, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ,
                Description = "Sportgerät", DisplayedName = "Baseballschläger", DefaultPrice = 118.5f,
                CurrentPrice = 118.5f, MinPrice = 10f, MaxPrice = 227f, Weight = 2f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Battleaxe, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ,
                Description = "Waffe", DisplayedName = "Kampfaxt", DefaultPrice = 114.5f, CurrentPrice = 114.5f,
                MinPrice = 49f, MaxPrice = 180f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Bottle, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ,
                Description = "Müll", DisplayedName = "Flasche", DefaultPrice = 3f, CurrentPrice = 3f, MinPrice = 1f,
                MaxPrice = 5f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Crowbar, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ,
                Description = "Werkzeug", DisplayedName = "Brechstange", DefaultPrice = 21.5f, CurrentPrice = 21.5f,
                MinPrice = 13f, MaxPrice = 30f, Weight = 2f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Dagger, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ,
                Description = "Waffe", DisplayedName = "Antiker Dolch", DefaultPrice = 1750f, CurrentPrice = 1750f,
                MinPrice = 500f, MaxPrice = 3000f, Weight = 0.5f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Flashlight, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ,
                Description = "Werkzeug", DisplayedName = "Taschenlampe", DefaultPrice = 205f, CurrentPrice = 205f,
                MinPrice = 60f, MaxPrice = 350f, Weight = 0.5f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Golfclub, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ,
                Description = "Sportgerät", DisplayedName = "Golfschläger", DefaultPrice = 975f, CurrentPrice = 975f,
                MinPrice = 450f, MaxPrice = 1500f, Weight = 2f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Hammer, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ,
                Description = "Werkzeug", DisplayedName = "Hammer", DefaultPrice = 105f, CurrentPrice = 105f,
                MinPrice = 10f, MaxPrice = 200f, Weight = 0.5f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Hatchet, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ,
                Description = "Werkzeug", DisplayedName = "Axt", DefaultPrice = 65f, CurrentPrice = 65f, MinPrice = 30f,
                MaxPrice = 100f, Weight = 2f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Knife, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ,
                Description = "Waffe", DisplayedName = "Messer", DefaultPrice = 90f, CurrentPrice = 90f, MinPrice = 40f,
                MaxPrice = 140f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Knuckle, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ,
                Description = "Waffe", DisplayedName = "Schlagring", DefaultPrice = 34.5f, CurrentPrice = 34.5f,
                MinPrice = 9f, MaxPrice = 60f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Machete, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ,
                Description = "Waffe", DisplayedName = "Machete", DefaultPrice = 147.5f, CurrentPrice = 147.5f,
                MinPrice = 25f, MaxPrice = 270f, Weight = 2f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Nightstick, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Lspd,
                Description = "Waffe", DisplayedName = "Schlagstock", DefaultPrice = 46.5f, CurrentPrice = 46.5f,
                MinPrice = 13f, MaxPrice = 80f, Weight = 2f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Poolcue, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ,
                Description = "Sportgerät", DisplayedName = "Billardstock", DefaultPrice = 121f, CurrentPrice = 121f,
                MinPrice = 12f, MaxPrice = 230f, Weight = 1f, Value = 0f
            },
            //new ItemModel{ ItemName = ItemNameEnums.StoneHatchet, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Waffe", DisplayedName = "Steinaxt", DefaultPrice = 20f, CurrentPrice = 20f, MinPrice =10f, MaxPrice = 30f, Weight = 2f, Value = 0f },
            new ItemModel
            {
                ItemName = ItemNameEnums.Switchblade, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ,
                Description = "Waffe", DisplayedName = "Springmesser", DefaultPrice = 135f, CurrentPrice = 135f,
                MinPrice = 20f, MaxPrice = 250f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Wrench, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ,
                Description = "Werkzeug", DisplayedName = "Rohrzange", DefaultPrice = 42.5f, CurrentPrice = 42.5f,
                MinPrice = 15f, MaxPrice = 70f, Weight = 3f, Value = 0f
            },
            //new ItemModel{ ItemName = ItemNameEnums.Appistol, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Waffe", DisplayedName = "AP-Pistole", DefaultPrice = 680f, CurrentPrice = 680f, MinPrice =100f, MaxPrice = 1260f, Weight = 2f, Value = 0f },
            new ItemModel
            {
                ItemName = ItemNameEnums.Combatpistol, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Lspd,
                Description = "Waffe", DisplayedName = "Gefechtspistole", DefaultPrice = 425f, CurrentPrice = 425f,
                MinPrice = 150f, MaxPrice = 700f, Weight = 2f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.DoubleAction, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ,
                Description = "Waffe", DisplayedName = "Klassischer Revolver", DefaultPrice = 1050f,
                CurrentPrice = 1050f, MinPrice = 600f, MaxPrice = 1500f, Weight = 2f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Flaregun, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ,
                Description = "Werkzeug", DisplayedName = "Signalpistole", DefaultPrice = 35f, CurrentPrice = 35f,
                MinPrice = 20f, MaxPrice = 50f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Heavypistol, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ,
                Description = "Waffe", DisplayedName = "Schwere Pistole", DefaultPrice = 2075f, CurrentPrice = 2075f,
                MinPrice = 750f, MaxPrice = 3400f, Weight = 2.5f, Value = 0f
            },
            //new ItemModel{ ItemName = ItemNameEnums.MarksmanPistol, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Waffe", DisplayedName = "Schützenpistole", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice =0f, MaxPrice = 0f, Weight = 3f, Value = 0f },
            new ItemModel
            {
                ItemName = ItemNameEnums.Pistol, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Lspd,
                Description = "Waffe", DisplayedName = "Pistole", DefaultPrice = 450f, CurrentPrice = 450f,
                MinPrice = 300f, MaxPrice = 600f, Weight = 2f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.PistolMk2, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Lspd,
                Description = "Waffe", DisplayedName = "Pistole Mk2", DefaultPrice = 850f, CurrentPrice = 850f,
                MinPrice = 500f, MaxPrice = 1200f, Weight = 2f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Pistol50, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ,
                Description = "Waffe", DisplayedName = "Pistole .50", DefaultPrice = 850f, CurrentPrice = 850f,
                MinPrice = 400f, MaxPrice = 1300f, Weight = 2.5f, Value = 0f
            },
            //new ItemModel{ ItemName = ItemNameEnums.RayPistol, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Waffe", DisplayedName = "Atomizer", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice =0f, MaxPrice = 0f, Weight = 15f, Value = 0f },
            new ItemModel
            {
                ItemName = ItemNameEnums.Revolver, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ,
                Description = "Waffe", DisplayedName = "Revolver", DefaultPrice = 1100f, CurrentPrice = 1100f,
                MinPrice = 600f, MaxPrice = 1600f, Weight = 2f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.RevolverMk2, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ,
                Description = "Waffe", DisplayedName = "Revolver Mk2", DefaultPrice = 1245f, CurrentPrice = 1245f,
                MinPrice = 690f, MaxPrice = 1800f, Weight = 2f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Snspistol, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ,
                Description = "Waffe", DisplayedName = "Billigknarre", DefaultPrice = 200f, CurrentPrice = 200f,
                MinPrice = 100f, MaxPrice = 300f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Snspistol_mk2, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ,
                Description = "Waffe", DisplayedName = "Billigknarre Mk2", DefaultPrice = 212.5f, CurrentPrice = 212.5f,
                MinPrice = 125f, MaxPrice = 300f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Stungun, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Lspd,
                Description = "Waffe", DisplayedName = "Elektroschoker", DefaultPrice = 0f, CurrentPrice = 0f,
                MinPrice = 0f, MaxPrice = 0f, Weight = 1f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.VintagePistol, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ,
                Description = "Waffe", DisplayedName = "Klassische Pistole", DefaultPrice = 2250f, CurrentPrice = 2250f,
                MinPrice = 1000f, MaxPrice = 3500f, Weight = 1.5f, Value = 0f
            },
            //new ItemModel{ ItemName = ItemNameEnums.AssaultSmg, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Waffe", DisplayedName = "Kampfgewehr", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice =0f, MaxPrice = 0f, Weight = 5f, Value = 0f },
            new ItemModel
            {
                ItemName = ItemNameEnums.CombatPdw, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Lspd,
                Description = "Waffe", DisplayedName = "Einsatz PDW", DefaultPrice = 1635f, CurrentPrice = 1635f,
                MinPrice = 1380f, MaxPrice = 1890f, Weight = 1f, Value = 18f
            },
            //new ItemModel{ ItemName = ItemNameEnums.MachinePistol, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Waffe", DisplayedName = "Reihenfeuerpistole", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice =0f, MaxPrice = 0f, Weight = 3f, Value = 30f },
            new ItemModel
            {
                ItemName = ItemNameEnums.MicroSmg, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ,
                Description = "Waffe", DisplayedName = "Micro-MP", DefaultPrice = 1125f, CurrentPrice = 1125f,
                MinPrice = 950f, MaxPrice = 1300f, Weight = 2f, Value = 7f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.MiniSmg, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ,
                Description = "Waffe", DisplayedName = "Mini-MP", DefaultPrice = 1480f, CurrentPrice = 1480f,
                MinPrice = 1360f, MaxPrice = 1600f, Weight = 3f, Value = 8f
            },
            //new ItemModel{ ItemName = ItemNameEnums.RayCarbine, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Waffe", DisplayedName = "Unheiliger Höllenbringer", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice =0f, MaxPrice = 0f, Weight = 10f, Value = 10f },
            new ItemModel
            {
                ItemName = ItemNameEnums.Smg, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Lspd,
                Description = "Waffe", DisplayedName = "MP", DefaultPrice = 1500f, CurrentPrice = 1500f,
                MinPrice = 1000f, MaxPrice = 2000f, Weight = 5f, Value = 30f
            },
            //new ItemModel{ ItemName = ItemNameEnums.SmgMk2, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Waffe", DisplayedName = "MP Mk2", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice =0f, MaxPrice = 0f, Weight = 5f, Value = 54f },
            //new ItemModel{ ItemName = ItemNameEnums.AssaultShotgun, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Waffe", DisplayedName = "Sturm-Schrotflinte", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice =0f, MaxPrice = 0f, Weight = 5f, Value = 500f },
            //new ItemModel{ ItemName = ItemNameEnums.AutoShotgun, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Waffe", DisplayedName = "Automatische Schrotflinte", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice =0f, MaxPrice = 0f, Weight = 7f, Value = 10f },
            //new ItemModel{ ItemName = ItemNameEnums.BullpupShotgun, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Waffe", DisplayedName = "Bullpup-Schrotflinte", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice =0f, MaxPrice = 0f, Weight = 3f, Value = 7f },
            //new ItemModel{ ItemName = ItemNameEnums.DbShotgun, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Waffe", DisplayedName = "Doppelläufige Schrotflinte", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice =0f, MaxPrice = 0f, Weight = 3f, Value = 7f },
            //new ItemModel{ ItemName = ItemNameEnums.HeavyShotgun, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Waffe", DisplayedName = "Schwere Schrotflinte", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice =0f, MaxPrice = 0f, Weight = 5f, Value = 8f },
            //new ItemModel{ ItemName = ItemNameEnums.Musket, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Waffe", DisplayedName = "Muskete", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice =0f, MaxPrice = 0f, Weight = 3f, Value = 10f },
            new ItemModel
            {
                ItemName = ItemNameEnums.PumpshotGun, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Lspd,
                Description = "Waffe", DisplayedName = "Pump-Action-Schrotflinte", DefaultPrice = 1136f,
                CurrentPrice = 1136f, MinPrice = 830f, MaxPrice = 1442f, Weight = 3f, Value = 30f
            },
            //new ItemModel{ ItemName = ItemNameEnums.PumpshotgunMk2, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Waffe", DisplayedName = "Pump-Action-Schrotflinte Mk2", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice =0f, MaxPrice = 0f, Weight = 3f, Value = 54f },
            //new ItemModel{ ItemName = ItemNameEnums.SawnoffShotgun, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Waffe", DisplayedName = "Abgesägte Schrotflinte", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice =0f, MaxPrice = 0f, Weight = 2f, Value = 500f },
            //new ItemModel{ ItemName = ItemNameEnums.Advancedrifle, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Waffe", DisplayedName = "Kampfgewehr", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice =0f, MaxPrice = 0f, Weight = 4f, Value = 10f },
            //new ItemModel{ ItemName = ItemNameEnums.AssaultRifle, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Waffe", DisplayedName = "Sturmgewehr", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice =0f, MaxPrice = 0f, Weight = 5f, Value = 8f },
            //new ItemModel{ ItemName = ItemNameEnums.AssaultRifleMk2, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Waffe", DisplayedName = "Sturmgewehr Mk2", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice =0f, MaxPrice = 0f, Weight = 5f, Value = 8f },
            //new ItemModel{ ItemName = ItemNameEnums.BullpupRifle, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Waffe", DisplayedName = "Bullpup Gewehr", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice =0f, MaxPrice = 0f, Weight = 3f, Value = 10f },
            //new ItemModel{ ItemName = ItemNameEnums.BullpupRifleMk2, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Waffe", DisplayedName = "Bullpup Gewehr Mk2", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice =0f, MaxPrice = 0f, Weight = 3f, Value = 6f },
            new ItemModel
            {
                ItemName = ItemNameEnums.CarbineRifle, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Lspd,
                Description = "Waffe", DisplayedName = "Karabinergewehr", DefaultPrice = 939.5f, CurrentPrice = 939f,
                MinPrice = 699f, MaxPrice = 1180f, Weight = 4f, Value = 9f
            },
            //new ItemModel{ ItemName = ItemNameEnums.CarbineRifleMk2, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Waffe", DisplayedName = "Karabinergewehr Mk2", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice =0f, MaxPrice = 0f, Weight = 4f, Value = 30f },
            //new ItemModel{ ItemName = ItemNameEnums.CompactRifle, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Waffe", DisplayedName = "Kompaktes Gewehr", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice =0f, MaxPrice = 0f, Weight = 3f, Value = 6f },
            //new ItemModel{ ItemName = ItemNameEnums.SpecialCarbine, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Waffe", DisplayedName = "Spezieal Karabiner", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice =0f, MaxPrice = 0f, Weight = 4f, Value = 12f },
            //new ItemModel{ ItemName = ItemNameEnums.SpecialCarbineMk2, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Waffe", DisplayedName = "Spezieal Karabiner Mk2", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice =0f, MaxPrice = 0f, Weight = 4f, Value = 16f },
            //new ItemModel{ ItemName = ItemNameEnums.Combatmg, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Waffe", DisplayedName = "Schweres MG", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice =0f, MaxPrice = 0f, Weight = 10f, Value = 18f },
            //new ItemModel{ ItemName = ItemNameEnums.Combatmg_mk2, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Waffe", DisplayedName = "Schweres MG Mk2", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice =0f, MaxPrice = 0f, Weight = 10f, Value = 30f },
            //new ItemModel{ ItemName = ItemNameEnums.Gusenberg, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Waffe", DisplayedName = "Gusenberg-Bleispritze", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice =0f, MaxPrice = 0f, Weight = 8f, Value = 7f },
            //new ItemModel{ ItemName = ItemNameEnums.Mg, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Waffe", DisplayedName = "MG", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice =0f, MaxPrice = 0f, Weight = 9f, Value = 8f },
            //new ItemModel{ ItemName = ItemNameEnums.HeavySniper, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Waffe", DisplayedName = "Schweres Scharfschützengewehr", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice =0f, MaxPrice = 0f, Weight = 10f, Value = 10f },
            //new ItemModel{ ItemName = ItemNameEnums.HeavySniperMk2, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Waffe", DisplayedName = "Schweres Scharfschützengewehr Mk2", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice =0f, MaxPrice = 0f, Weight = 10f, Value = 30f },
            //new ItemModel{ ItemName = ItemNameEnums.MarksmanRifle, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Waffe", DisplayedName = "Präzisionsgewehr", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice =0f, MaxPrice = 0f, Weight = 6f, Value = 54f },
            //new ItemModel{ ItemName = ItemNameEnums.MarksmanRifleMk2, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Waffe", DisplayedName = "Präzisionsgewehr Mk2", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice =0f, MaxPrice = 0f, Weight = 6f, Value = 500f },
            //new ItemModel{ ItemName = ItemNameEnums.sniperrifle, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Waffe", DisplayedName = "Scharfschützengewehr Mk3", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice =0f, MaxPrice = 0f, Weight = 5f, Value = 10f },
            //new ItemModel{ ItemName = ItemNameEnums.CompactLauncher, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Waffe", DisplayedName = "Granatwerfer", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice =0f, MaxPrice = 0f, Weight = 10f, Value = 10f },
            //new ItemModel{ ItemName = ItemNameEnums.Firework, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Werkzeug", DisplayedName = "Feuerwerkabschussgerät", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice =0f, MaxPrice = 0f, Weight = 10f, Value = 8f },
            //new ItemModel{ ItemName = ItemNameEnums.Grenadelauncher, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Waffe", DisplayedName = "Automatischer Granatwerfer", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice =0f, MaxPrice = 0f, Weight = 15f, Value = 8f },
            //new ItemModel{ ItemName = ItemNameEnums.GrenadelauncherSmoke, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Waffe", DisplayedName = "Smoke-Granatwerfer", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice =0f, MaxPrice = 0f, Weight = 5f, Value = 10f },
            //new ItemModel{ ItemName = ItemNameEnums.HomingLauncher, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Waffe", DisplayedName = "Lenkraketenwerfer", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice =0f, MaxPrice = 0f, Weight = 10f, Value = 6f },
            //new ItemModel{ ItemName = ItemNameEnums.Minigun, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Waffe", DisplayedName = "Minigun", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice =0f, MaxPrice = 0f, Weight = 20f, Value = 9f },
            //new ItemModel{ ItemName = ItemNameEnums.Railgun, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Waffe", DisplayedName = "Railgun", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice =0f, MaxPrice = 0f, Weight = 10f, Value = 30f },
            //new ItemModel{ ItemName = ItemNameEnums.RayMinigun, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Waffe", DisplayedName = "Witwenmacher", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice =0f, MaxPrice = 0f, Weight = 15f, Value = 6f },
            //new ItemModel{ ItemName = ItemNameEnums.Rpg, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Waffe", DisplayedName = "Panzerfaust", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice =0f, MaxPrice = 0f, Weight = 10f, Value = 12f },
            //new ItemModel{ ItemName = ItemNameEnums.Ball, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Spielzeug", DisplayedName = "Ball", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice =0f, MaxPrice = 0f, Weight = 1f, Value = 16f },
            //new ItemModel{ ItemName = ItemNameEnums.BzGas, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Waffe", DisplayedName = "Tränengasgranate", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice =0f, MaxPrice = 0f, Weight = 0.5f, Value = 18f },
            new ItemModel
            {
                ItemName = ItemNameEnums.Flare, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ,
                Description = "Werkzeug", DisplayedName = "Signalfackel", DefaultPrice = 22.5f, CurrentPrice = 22.5f,
                MinPrice = 11f, MaxPrice = 34f, Weight = 0.5f, Value = 30f
            },
            //new ItemModel{ ItemName = ItemNameEnums.Grenade, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Waffe", DisplayedName = "Granate", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice =0f, MaxPrice = 0f, Weight = 0.5f, Value = 7f },
            //new ItemModel{ ItemName = ItemNameEnums.Molotov, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Waffe", DisplayedName = "Molotowcocktail", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice =0f, MaxPrice = 0f, Weight = 1f, Value = 8f },
            //new ItemModel{ ItemName = ItemNameEnums.Pipebomb, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Waffe", DisplayedName = "Rohrbombe", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice =0f, MaxPrice = 0f, Weight = 1.5f, Value = 10f },
            //new ItemModel{ ItemName = ItemNameEnums.Proxmine, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Waffe", DisplayedName = "Annährungsmine", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice =0f, MaxPrice = 0f, Weight = 1f, Value = 30f },
            new ItemModel
            {
                ItemName = ItemNameEnums.Smokegrenade, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Lspd,
                Description = "Waffe", DisplayedName = "Rauchgranate", DefaultPrice = 16.5f, CurrentPrice = 16.5f,
                MinPrice = 10f, MaxPrice = 23f, Weight = 0.5f, Value = 54f
            },
            //new ItemModel{ ItemName = ItemNameEnums.Snowball, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Spielzeug", DisplayedName = "Schneeball", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice =0f, MaxPrice = 0f, Weight = 0.2f, Value = 500f },
            //new ItemModel{ ItemName = ItemNameEnums.Stickybomb, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ, Description = "Waffe", DisplayedName = "Haftbombe", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice =0f, MaxPrice = 0f, Weight = 2f, Value = 10f },
            new ItemModel
            {
                ItemName = ItemNameEnums.Fireextinguisher, ItemType = ItemTypeEnums.Weapon,
                ItemFaction = FactionEnums.Civ, Description = "Werkzeug", DisplayedName = "Feuerlöscher",
                DefaultPrice = 30f, CurrentPrice = 30f, MinPrice = 10f, MaxPrice = 50f, Weight = 10f, Value = 16f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Parachute, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ,
                Description = "Werkzeug", DisplayedName = "Fallschirm", DefaultPrice = 2150f, CurrentPrice = 2150f,
                MinPrice = 1500f, MaxPrice = 2800f, Weight = 10f, Value = 18f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.Petrolcan, ItemType = ItemTypeEnums.Weapon, ItemFaction = FactionEnums.Civ,
                Description = "Werkzeug", DisplayedName = "Benzinkanister", DefaultPrice = 35f, CurrentPrice = 35f,
                MinPrice = 20f, MaxPrice = 50f, Weight = 25f, Value = 30f
            },

            // #-Ammo-#
            //new ItemModel{ ItemName = ItemNameEnums._83mmRocket, ItemType = ItemTypeEnums.Ammo, ItemFaction = FactionEnums.Civ, Description = "Panzerfaust", DisplayedName = "83mm Rakete", DefaultPrice = 0.01f, CurrentPrice = 0.01f, MinPrice =0.01f, MaxPrice = 0.01f, Weight = 1f, Value = 1f },
            //new ItemModel{ ItemName = ItemNameEnums._83mmFireworkRocket, ItemType = ItemTypeEnums.Ammo, ItemFaction = FactionEnums.Civ, Description = "Feuerwerkabschussgerät", DisplayedName = "83mm Feuerwerksrakete", DefaultPrice = 0.01f, CurrentPrice = 0.01f, MinPrice =0.01f, MaxPrice = 0.01f, Weight = 1f, Value = 1f },
            //new ItemModel{ ItemName = ItemNameEnums._70_1mmSteeringRocket, ItemType = ItemTypeEnums.Ammo, ItemFaction = FactionEnums.Civ, Description = "Lenkraketenwerfer", DisplayedName = "70.1mm Lenkrakete", DefaultPrice = 0.01f, CurrentPrice = 0.01f, MinPrice =0.01f, MaxPrice = 0.01f, Weight = 1f, Value = 1f },
            //new ItemModel{ ItemName = ItemNameEnums._40mmGrenade_1Shot, ItemType = ItemTypeEnums.Ammo, ItemFaction = FactionEnums.Civ, Description = "Granatwerfer", DisplayedName = "40mm Granate - 1 ", DefaultPrice = 0.01f, CurrentPrice = 0.01f, MinPrice =0.01f, MaxPrice = 0.01f, Weight = 0.2f, Value = 1f },
            //new ItemModel{ ItemName = ItemNameEnums._40mmGrenade_10Shot, ItemType = ItemTypeEnums.Ammo, ItemFaction = FactionEnums.Civ, Description = "Granatwerfer", DisplayedName = "40mm Granate - 10 Schuss", DefaultPrice = 0.01f, CurrentPrice = 0.01f, MinPrice =0.01f, MaxPrice = 0.01f, Weight = 2f, Value = 10f },
            new ItemModel
            {
                ItemName = ItemNameEnums.IlluminatedCartridge_1Shot, ItemType = ItemTypeEnums.Ammo,
                ItemFaction = FactionEnums.Civ, Description = "Signalpistole",
                DisplayedName = "Leuchtpatrone - 1 Schuss", DefaultPrice = 7.5f, CurrentPrice = 7.5f, MinPrice = 5f,
                MaxPrice = 10f, Weight = 0.2f, Value = 1f
            },
            //new ItemModel{ ItemName = ItemNameEnums._12gauge_14Shots, ItemType = ItemTypeEnums.Ammo, ItemFaction = FactionEnums.Civ, Description = "Bullpup-Schrotflinte", DisplayedName = "12 gauge - 14 Schuss", DefaultPrice = 77f, CurrentPrice = 77f, MinPrice =4f, MaxPrice = 150f, Weight = 2.8f, Value = 14f },
            //new ItemModel{ ItemName = ItemNameEnums._410bore_6Shots, ItemType = ItemTypeEnums.Ammo, ItemFaction = FactionEnums.Lspd, Description = "Schwere Schrotflinte", DisplayedName = ".410 bore - 6 Schuss", DefaultPrice = 22f, CurrentPrice = 22f, MinPrice =2f, MaxPrice = 42f, Weight = 1.2f, Value = 6f },
            new ItemModel
            {
                ItemName = ItemNameEnums._410bore_8Shots, ItemType = ItemTypeEnums.Ammo, ItemFaction = FactionEnums.Civ,
                Description = "Pump-Acrion-Schrotflinte, Abgesäge Schrotflinte, Sturm-Schrotflinte",
                DisplayedName = ".410 bore - 8 Schuss", DefaultPrice = 29.4f, CurrentPrice = 29.4f, MinPrice = 2.8f,
                MaxPrice = 56f, Weight = 1.6f, Value = 8f
            },
            //new ItemModel{ ItemName = ItemNameEnums._75_1Shot, ItemType = ItemTypeEnums.Ammo, ItemFaction = FactionEnums.Civ, Description = "Muskete", DisplayedName = ".75 - 10 Schuss", DefaultPrice = 21.25f, CurrentPrice = 21.25f, MinPrice =15f, MaxPrice = 27.5f, Weight = 0.2f, Value = 10f },
            //new ItemModel{ ItemName = ItemNameEnums._50Magazine_6Shots, ItemType = ItemTypeEnums.Ammo, ItemFaction = FactionEnums.Civ, Description = "Schw. Scharfschützengewehr", DisplayedName = ".50 Magazin - 6 Schuss", DefaultPrice = 28.5f, CurrentPrice = 28.5f, MinPrice =18f, MaxPrice = 39f, Weight = 1f, Value = 6f },
            new ItemModel
            {
                ItemName = ItemNameEnums._50Magazine_9Shots, ItemType = ItemTypeEnums.Ammo,
                ItemFaction = FactionEnums.Civ, Description = ".50 Pistole", DisplayedName = ".50 Magazin - 9 Schuss",
                DefaultPrice = 41.5f, CurrentPrice = 41.5f, MinPrice = 27f, MaxPrice = 56f, Weight = 1.3f, Value = 9f
            },
            //new ItemModel{ ItemName = ItemNameEnums._45ACPMagazine_30Shots, ItemType = ItemTypeEnums.Ammo, ItemFaction = FactionEnums.Civ, Description = "Gusenberg-Bleispritze", DisplayedName = ".45 ACP Magazin - 30 Schuss", DefaultPrice = 44.25f, CurrentPrice = 44.25f, MinPrice =7.5f, MaxPrice = 81f, Weight = 1.5f, Value = 30f },
            new ItemModel
            {
                ItemName = ItemNameEnums._9mmMagazine_6Shots, ItemType = ItemTypeEnums.Ammo,
                ItemFaction = FactionEnums.Civ, Description = "Billigknarre", DisplayedName = "9mm Magazin - 6 Schuss",
                DefaultPrice = 6.35f, CurrentPrice = 6.35f, MinPrice = 1f, MaxPrice = 11.7f, Weight = 0.3f, Value = 6f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums._9mmMagazine_12Shots, ItemType = ItemTypeEnums.Ammo,
                ItemFaction = FactionEnums.Civ, Description = "Pistole, Gefechtspistole, Reihenfeuerpistole",
                DisplayedName = "9mm Magazin - 12 Schuss", DefaultPrice = 12.75f, CurrentPrice = 12.75f, MinPrice = 2f,
                MaxPrice = 23.5f, Weight = 0.6f, Value = 12f
            },
            //new ItemModel{ ItemName = ItemNameEnums._9mmMagazine_16Shots, ItemType = ItemTypeEnums.Ammo, ItemFaction = FactionEnums.Civ, Description = "Micro-MP", DisplayedName = "9mm Magazin - 16 Schuss", DefaultPrice = 16.875f, CurrentPrice = 16.88f, MinPrice =2.5f, MaxPrice = 31.25f, Weight = 0.8f, Value = 16f },
            new ItemModel
            {
                ItemName = ItemNameEnums._9mmMagazine_18Shots, ItemType = ItemTypeEnums.Ammo,
                ItemFaction = FactionEnums.Lspd, Description = "Schwere Pistole, AP-Pistole",
                DisplayedName = "9mm Magazin - 18 Schuss", DefaultPrice = 19.025f, CurrentPrice = 19.03f,
                MinPrice = 2.8f, MaxPrice = 35.25f, Weight = 0.9f, Value = 18f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums._9mmMagazine_30Shots, ItemType = ItemTypeEnums.Ammo,
                ItemFaction = FactionEnums.Lspd, Description = "MP, Sturm-MP, Sturm-PDW",
                DisplayedName = "9mm Magazin - 30 Schuss", DefaultPrice = 31.625f, CurrentPrice = 31.63f,
                MinPrice = 4.75f, MaxPrice = 58.5f, Weight = 1.5f, Value = 30f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums._7_65mmMagazine_7Shots, ItemType = ItemTypeEnums.Ammo,
                ItemFaction = FactionEnums.Civ, Description = "Klassische Pistole",
                DisplayedName = "7,65mm Magazin - 7 Schuss", DefaultPrice = 3.25f, CurrentPrice = 3.25f,
                MinPrice = 2.75f, MaxPrice = 3.75f, Weight = 0.5f, Value = 7f
            },
            //new ItemModel{ ItemName = ItemNameEnums._7_62mmMagazine_8Shots, ItemType = ItemTypeEnums.Ammo, ItemFaction = FactionEnums.Civ, Description = "Präzisionsgewehr", DisplayedName = "7,62mm Magazin - 8 Schuss", DefaultPrice = 4.5f, CurrentPrice = 4.5f, MinPrice =4f, MaxPrice = 5f, Weight = 0.5f, Value = 8f },
            //new ItemModel{ ItemName = ItemNameEnums._7_62mmMagazine_10Shots, ItemType = ItemTypeEnums.Ammo, ItemFaction = FactionEnums.Civ, Description = "Scharfschützengewehr", DisplayedName = "7,62mm Magazin - 10 Schuss", DefaultPrice = 5.5f, CurrentPrice = 5.5f, MinPrice =5f, MaxPrice = 6f, Weight = 0.6f, Value = 10f },
            new ItemModel
            {
                ItemName = ItemNameEnums._7_62mmMagazine_30Shots, ItemType = ItemTypeEnums.Ammo,
                ItemFaction = FactionEnums.Lspd,
                Description = "Sturmgewehr,  Kampfgewehr, Bullpupgewehr, Karabiner, Speziealkarabiner",
                DisplayedName = "7,62mm Magazin - 30 Schuss", DefaultPrice = 16f, CurrentPrice = 16f, MinPrice = 14.5f,
                MaxPrice = 17.5f, Weight = 1.8f, Value = 30f
            },
            //new ItemModel{ ItemName = ItemNameEnums._7_62mmMagazine_54Shots, ItemType = ItemTypeEnums.Ammo, ItemFaction = FactionEnums.Civ, Description = "MG", DisplayedName = "7,62mm Magazin - 54 Schuss", DefaultPrice = 28.75f, CurrentPrice = 28.75f, MinPrice =26f, MaxPrice = 31.5f, Weight = 2f, Value = 54f },
            //new ItemModel{ ItemName = ItemNameEnums._7_62mmMagazine_500Shots, ItemType = ItemTypeEnums.Ammo, ItemFaction = FactionEnums.Civ, Description = "Minigun", DisplayedName = "7,62mm Magazin - 500 Schuss", DefaultPrice = 265f, CurrentPrice = 265f, MinPrice =240f, MaxPrice = 290f, Weight = 20f, Value = 500f },
            //new ItemModel{ ItemName = ItemNameEnums._5_72mm_10Shot, ItemType = ItemTypeEnums.Ammo, ItemFaction = FactionEnums.Civ, Description = "Präzisionspistole", DisplayedName = "5,72mm - 10 Schuss", DefaultPrice = 5.875f, CurrentPrice = 0f, MinPrice =5.5f, MaxPrice = 6.25f, Weight = 0.2f, Value = 10f },
            //new ItemModel{ ItemName = ItemNameEnums._5_56mmMagazine_100Shots, ItemType = ItemTypeEnums.Ammo, ItemFaction = FactionEnums.Civ, Description = "Gefechts-MG", DisplayedName = "5,56mm Magazin - 100 Schuss", DefaultPrice = 47.75f, CurrentPrice = 0f, MinPrice =37.5f, MaxPrice = 58f, Weight = 2f, Value = 100f },

            // #-MissionItem-#
            new ItemModel
            {
                ItemName = ItemNameEnums.MissionFinger, ItemType = ItemTypeEnums.MissionItem,
                ItemFaction = FactionEnums.Civ, Description = "abgeschnittener Finger",
                DisplayedName = "Abgeschnittener Finger", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f,
                MaxPrice = 0f, Weight = 0.3f, Value = 0f
            },
            new ItemModel
            {
                ItemName = ItemNameEnums.MissionSuitcase, ItemType = ItemTypeEnums.MissionItem,
                ItemFaction = FactionEnums.Civ, Description = "geheimnisvoller Aktenkoffer",
                DisplayedName = "Aktenkoffer", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f,
                Weight = 25f, Value = 0f
            }
        };
    }
}