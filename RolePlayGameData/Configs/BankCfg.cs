﻿namespace RolePlayGameData.Configs
{
    public static class BankCfg
    {
        public static readonly float FleecaManangeFee = 4.65f;
        public static readonly float FleecaTransferFee = 4.65f;
        public static readonly float FleecaWithdrawFee = 4.65f;
        public static readonly float FleecaDepositFee = 4.65f;
        public static readonly float FleecaCreditCardFee = 4.65f;
        public static readonly float FleecaCreditLimitValue = 0.6f;
        public static readonly float FleecaCreditRePayValue = 0.1f;

        public static readonly float MazeManangeFee = 4.65f;
        public static readonly float MazeTransferFee = 4.65f;
        public static readonly float MazeWithdrawFee = 4.65f;
        public static readonly float MazeDepositFee = 4.65f;
        public static readonly float MazeCreditCardFee = 4.65f;
        public static readonly float MazeCreditLimitValue = 0.6f;
        public static readonly float MazeCreditRePayValue = 0.1f;

        public static readonly float UnionManangeFee = 4.65f;
        public static readonly float UnionTransferFee = 4.65f;
        public static readonly float UnionWithdrawFee = 4.65f;
        public static readonly float UnionDepositFee = 4.65f;
        public static readonly float UnionCreditCardFee = 4.65f;
        public static readonly float UnionCreditLimitValue = 0.6f;
        public static readonly float UnionCreditRePayValue = 0.1f;
    }
}