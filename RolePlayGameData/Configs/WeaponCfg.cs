﻿using System.Collections.Generic;
using RolePlayGameData.Enums;
using RolePlayGameData.Models;

namespace RolePlayGameData.Configs
{
    public class WeaponCfg
    {
        public static readonly List<WeaponModel> WeaponConfig = new List<WeaponModel>
        {
            // #-Melee-#
            new WeaponModel
            {
                Hash = WeaponHashes.Bat, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.Melee,
                Description = "Sportgerät", DisplayedName = "Baseballschläger", DefaultPrice = 118.5f,
                CurrentPrice = 118.5f, MinPrice = 10f, MaxPrice = 227f, Weight = 2f
            },
            new WeaponModel
            {
                Hash = WeaponHashes.Battleaxe, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.Melee,
                Description = "Waffe", DisplayedName = "Kampfaxt", DefaultPrice = 114.5f, CurrentPrice = 114.5f,
                MinPrice = 49f, MaxPrice = 180f, Weight = 1f
            },
            new WeaponModel
            {
                Hash = WeaponHashes.Bottle, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.Melee,
                Description = "Müll", DisplayedName = "Flasche", DefaultPrice = 3f, CurrentPrice = 3f, MinPrice = 1f,
                MaxPrice = 5f, Weight = 1f
            },
            new WeaponModel
            {
                Hash = WeaponHashes.Crowbar, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.Melee,
                Description = "Werkzeug", DisplayedName = "Brechstange", DefaultPrice = 21.5f, CurrentPrice = 21.5f,
                MinPrice = 13f, MaxPrice = 30f, Weight = 2f
            },
            new WeaponModel
            {
                Hash = WeaponHashes.Dagger, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.Melee,
                Description = "Waffe", DisplayedName = "Antiker Dolch", DefaultPrice = 1750f, CurrentPrice = 1750f,
                MinPrice = 500f, MaxPrice = 3000f, Weight = 0.5f
            },
            new WeaponModel
            {
                Hash = WeaponHashes.Flashlight, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.Melee,
                Description = "Werkzeug", DisplayedName = "Taschenlampe", DefaultPrice = 205f, CurrentPrice = 205f,
                MinPrice = 60f, MaxPrice = 350f, Weight = 0.5f
            },
            new WeaponModel
            {
                Hash = WeaponHashes.Golfclub, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.Melee,
                Description = "Sportgerät", DisplayedName = "Golfschläger", DefaultPrice = 975f, CurrentPrice = 975f,
                MinPrice = 450f, MaxPrice = 1500f, Weight = 2f
            },
            new WeaponModel
            {
                Hash = WeaponHashes.Hammer, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.Melee,
                Description = "Werkzeug", DisplayedName = "Hammer", DefaultPrice = 105f, CurrentPrice = 105f,
                MinPrice = 10f, MaxPrice = 200f, Weight = 0.5f
            },
            new WeaponModel
            {
                Hash = WeaponHashes.Hatchet, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.Melee,
                Description = "Werkzeug", DisplayedName = "Axt", DefaultPrice = 65f, CurrentPrice = 65f, MinPrice = 30f,
                MaxPrice = 100f, Weight = 2f
            },
            new WeaponModel
            {
                Hash = WeaponHashes.Knife, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.Melee,
                Description = "Waffe", DisplayedName = "Messer", DefaultPrice = 90f, CurrentPrice = 90f, MinPrice = 40f,
                MaxPrice = 140f, Weight = 1f
            },
            new WeaponModel
            {
                Hash = WeaponHashes.Knuckle, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.Melee,
                Description = "Waffe", DisplayedName = "Schlagring", DefaultPrice = 34.5f, CurrentPrice = 34.5f,
                MinPrice = 9f, MaxPrice = 60f, Weight = 1f
            },
            new WeaponModel
            {
                Hash = WeaponHashes.Machete, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.Melee,
                Description = "Waffe", DisplayedName = "Machete", DefaultPrice = 147.5f, CurrentPrice = 147.5f,
                MinPrice = 25f, MaxPrice = 270f, Weight = 2f
            },
            new WeaponModel
            {
                Hash = WeaponHashes.Nightstick, Faction = FactionEnums.Lspd, WeaponType = WeaponTypeEnums.Melee,
                Description = "Waffe", DisplayedName = "Schlagstock", DefaultPrice = 46.5f, CurrentPrice = 46.5f,
                MinPrice = 13f, MaxPrice = 80f, Weight = 2f
            },
            new WeaponModel
            {
                Hash = WeaponHashes.Poolcue, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.Melee,
                Description = "Sportgerät", DisplayedName = "Billardstock", DefaultPrice = 121f, CurrentPrice = 121f,
                MinPrice = 12f, MaxPrice = 230f, Weight = 1f
            },
            //new WeaponModel(){ Hash = WeaponHashes.StoneHatchet, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.Melee, Description = "Waffe", DisplayedName = "Steinaxt", DefaultPrice = 20f, CurrentPrice = 20f, MinPrice = 10f, MaxPrice = 30f, Weight = 2f },
            new WeaponModel
            {
                Hash = WeaponHashes.Switchblade, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.Melee,
                Description = "Waffe", DisplayedName = "Springmesser", DefaultPrice = 135f, CurrentPrice = 135f,
                MinPrice = 20f, MaxPrice = 250f, Weight = 1f
            },
            //new WeaponModel(){ Hash = WeaponHashes.Unarmed, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.Melee, Description = "Sonstiges", DisplayedName = "Unbewaffnet", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f, Weight = 1f },
            new WeaponModel
            {
                Hash = WeaponHashes.Wrench, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.Melee,
                Description = "Werkzeug", DisplayedName = "Rohrzange", DefaultPrice = 42.5f, CurrentPrice = 42.5f,
                MinPrice = 15f, MaxPrice = 70f, Weight = 3f
            },

            // #-Handgun-#
            //new WeaponModel(){ Hash = WeaponHashes.Appistol, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.Handgun, Description = "Waffe", DisplayedName = "AP-Pistole", DefaultPrice = 680f, CurrentPrice = 680f, MinPrice = 100f, MaxPrice = 1260f, Weight = 2f },
            new WeaponModel
            {
                Hash = WeaponHashes.Combatpistol, Faction = FactionEnums.Lspd, WeaponType = WeaponTypeEnums.Handgun,
                Description = "Waffe", DisplayedName = "Gefechtspistole", DefaultPrice = 425f, CurrentPrice = 425f,
                MinPrice = 150f, MaxPrice = 700f, Weight = 2f
            },
            new WeaponModel
            {
                Hash = WeaponHashes.DoubleAction, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.Handgun,
                Description = "Waffe", DisplayedName = "Klassischer Revolver", DefaultPrice = 1050f,
                CurrentPrice = 1050f, MinPrice = 600f, MaxPrice = 1500f, Weight = 2f
            },
            new WeaponModel
            {
                Hash = WeaponHashes.Flaregun, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.Handgun,
                Description = "Werkzeug", DisplayedName = "Signalpistole", DefaultPrice = 35f, CurrentPrice = 35f,
                MinPrice = 20f, MaxPrice = 50f, Weight = 1f
            },
            new WeaponModel
            {
                Hash = WeaponHashes.Heavypistol, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.Handgun,
                Description = "Waffe", DisplayedName = "Schwere Pistole", DefaultPrice = 2075f, CurrentPrice = 2075f,
                MinPrice = 750f, MaxPrice = 3400f, Weight = 2.5f
            },
            //new WeaponModel(){ Hash = WeaponHashes.MarksmanPistol, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.Handgun, Description = "Waffe", DisplayedName = "Schützenpistole", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f, Weight = 3f },
            new WeaponModel
            {
                Hash = WeaponHashes.Pistol, Faction = FactionEnums.Lspd, WeaponType = WeaponTypeEnums.Handgun,
                Description = "Waffe", DisplayedName = "Pistole", DefaultPrice = 450f, CurrentPrice = 450f,
                MinPrice = 300f, MaxPrice = 600f, Weight = 2f
            },
            new WeaponModel
            {
                Hash = WeaponHashes.PistolMk2, Faction = FactionEnums.Lspd, WeaponType = WeaponTypeEnums.Handgun,
                Description = "Waffe", DisplayedName = "Pistole Mk2", DefaultPrice = 850f, CurrentPrice = 850f,
                MinPrice = 500f, MaxPrice = 1200f, Weight = 2f
            },
            new WeaponModel
            {
                Hash = WeaponHashes.Pistol50, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.Handgun,
                Description = "Waffe", DisplayedName = "Pistole .50", DefaultPrice = 850f, CurrentPrice = 850f,
                MinPrice = 400f, MaxPrice = 1300f, Weight = 2.5f
            },
            //new WeaponModel(){ Hash = WeaponHashes.RayPistol, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.Handgun, Description = "Waffe", DisplayedName = "Atomizer", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f, Weight = 15f },
            new WeaponModel
            {
                Hash = WeaponHashes.Revolver, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.Handgun,
                Description = "Waffe", DisplayedName = "Revolver", DefaultPrice = 1100f, CurrentPrice = 1100f,
                MinPrice = 600f, MaxPrice = 1600f, Weight = 2f
            },
            new WeaponModel
            {
                Hash = WeaponHashes.RevolverMk2, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.Handgun,
                Description = "Waffe", DisplayedName = "Revolver Mk2", DefaultPrice = 1245f, CurrentPrice = 1245f,
                MinPrice = 690f, MaxPrice = 1800f, Weight = 2f
            },
            new WeaponModel
            {
                Hash = WeaponHashes.Snspistol, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.Handgun,
                Description = "Waffe", DisplayedName = "Billigknarre", DefaultPrice = 200f, CurrentPrice = 200f,
                MinPrice = 100f, MaxPrice = 300f, Weight = 1f
            },
            new WeaponModel
            {
                Hash = WeaponHashes.Snspistol_mk2, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.Handgun,
                Description = "Waffe", DisplayedName = "Billigknarre Mk2", DefaultPrice = 212.5f, CurrentPrice = 212.5f,
                MinPrice = 125f, MaxPrice = 300f, Weight = 1f
            },
            new WeaponModel
            {
                Hash = WeaponHashes.Stungun, Faction = FactionEnums.Lspd, WeaponType = WeaponTypeEnums.Handgun,
                Description = "Waffe", DisplayedName = "Elektroschoker", DefaultPrice = 0f, CurrentPrice = 0f,
                MinPrice = 0f, MaxPrice = 0f, Weight = 1f
            },
            new WeaponModel
            {
                Hash = WeaponHashes.VintagePistol, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.Handgun,
                Description = "Waffe", DisplayedName = "Klassische Pistole", DefaultPrice = 2250f, CurrentPrice = 2250f,
                MinPrice = 1000f, MaxPrice = 3500f, Weight = 1.5f
            },

            // #-SubmachineGun-#
            //new WeaponModel(){ Hash = WeaponHashes.AssaultSmg, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.SubmachineGun, Description = "Waffe", DisplayedName = "Kampfgewehr", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f, Weight = 5f },
            new WeaponModel
            {
                Hash = WeaponHashes.CombatPdw, Faction = FactionEnums.Lspd, WeaponType = WeaponTypeEnums.SubmachineGun,
                Description = "Waffe", DisplayedName = "Einsatz PDW", DefaultPrice = 1635f, CurrentPrice = 1635f,
                MinPrice = 1380f, MaxPrice = 1890f, Weight = 1f
            },
            //new WeaponModel(){ Hash = WeaponHashes.MachinePistol, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.SubmachineGun, Description = "Waffe", DisplayedName = "Reihenfeuerpistole", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f, Weight = 3f },
            new WeaponModel
            {
                Hash = WeaponHashes.MicroSmg, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.SubmachineGun,
                Description = "Waffe", DisplayedName = "Micro-MP", DefaultPrice = 1125f, CurrentPrice = 1125f,
                MinPrice = 950f, MaxPrice = 1300f, Weight = 2f
            },
            new WeaponModel
            {
                Hash = WeaponHashes.MiniSmg, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.SubmachineGun,
                Description = "Waffe", DisplayedName = "Mini-MP", DefaultPrice = 1480f, CurrentPrice = 1480f,
                MinPrice = 1360f, MaxPrice = 1600f, Weight = 3f
            },
            //new WeaponModel(){ Hash = WeaponHashes.RayCarbine, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.SubmachineGun, Description = "Waffe", DisplayedName = "Unheiliger Höllenbringer", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f, Weight = 10f },
            new WeaponModel
            {
                Hash = WeaponHashes.Smg, Faction = FactionEnums.Lspd, WeaponType = WeaponTypeEnums.SubmachineGun,
                Description = "Waffe", DisplayedName = "MP", DefaultPrice = 1500f, CurrentPrice = 1500f,
                MinPrice = 1000f, MaxPrice = 2000f, Weight = 5f
            },
            //new WeaponModel(){ Hash = WeaponHashes.SmgMk2, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.SubmachineGun, Description = "Waffe", DisplayedName = "MP Mk2", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f, Weight = 5f },

            // #-Shotgun-#
            //new WeaponModel(){ Hash = WeaponHashes.AssaultShotgun, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.Shotgun, Description = "Waffe", DisplayedName = "Sturm-Schrotflinte", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f, Weight = 5f },
            //new WeaponModel(){ Hash = WeaponHashes.AutoShotgun, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.Shotgun, Description = "Waffe", DisplayedName = "Automatische Schrotflinte", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f, Weight = 7f },
            //new WeaponModel(){ Hash = WeaponHashes.BullpupShotgun, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.Shotgun, Description = "Waffe", DisplayedName = "Bullpup-Schrotflinte", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f, Weight = 3f },
            //new WeaponModel(){ Hash = WeaponHashes.DbShotgun, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.Shotgun, Description = "Waffe", DisplayedName = "Doppelläufige Schrotflinte", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f, Weight = 3f },
            //new WeaponModel(){ Hash = WeaponHashes.HeavyShotgun, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.Shotgun, Description = "Waffe", DisplayedName = "Schwere Schrotflinte", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f, Weight = 5f },
            //new WeaponModel(){ Hash = WeaponHashes.Musket, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.Shotgun, Description = "Waffe", DisplayedName = "Muskete", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f, Weight = 3f },
            new WeaponModel
            {
                Hash = WeaponHashes.PumpshotGun, Faction = FactionEnums.Lspd, WeaponType = WeaponTypeEnums.Shotgun,
                Description = "Waffe", DisplayedName = "Pump-Action-Schrotflinte", DefaultPrice = 1136f,
                CurrentPrice = 1136f, MinPrice = 830f, MaxPrice = 1442f, Weight = 3f
            },
            //new WeaponModel(){ Hash = WeaponHashes.PumpshotgunMk2, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.Shotgun, Description = "Waffe", DisplayedName = "Pump-Action-Schrotflinte Mk2", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f, Weight = 3f },
            //new WeaponModel(){ Hash = WeaponHashes.SawnoffShotgun, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.Shotgun, Description = "Waffe", DisplayedName = "Abgesägte Schrotflinte", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f, Weight = 2f },

            // #-AssaultRifles-#
            //new WeaponModel(){ Hash = WeaponHashes.Advancedrifle, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.AssaultRifles, Description = "Waffe", DisplayedName = "Kampfgewehr", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f, Weight = 4f },
            //new WeaponModel(){ Hash = WeaponHashes.AssaultRifle, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.AssaultRifles, Description = "Waffe", DisplayedName = "Sturmgewehr", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f, Weight = 5f },
            //new WeaponModel(){ Hash = WeaponHashes.AssaultRifleMk2, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.AssaultRifles, Description = "Waffe", DisplayedName = "Sturmgewehr Mk2", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f, Weight = 5f },
            //new WeaponModel(){ Hash = WeaponHashes.BullpupRifle, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.AssaultRifles, Description = "Waffe", DisplayedName = "Bullpup Gewehr", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f, Weight = 3f },
            //new WeaponModel(){ Hash = WeaponHashes.BullpupRifleMk2, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.AssaultRifles, Description = "Waffe", DisplayedName = "Bullpup Gewehr Mk2", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f, Weight = 3f },
            new WeaponModel
            {
                Hash = WeaponHashes.CarbineRifle, Faction = FactionEnums.Lspd,
                WeaponType = WeaponTypeEnums.AssaultRifles, Description = "Waffe", DisplayedName = "Karabinergewehr",
                DefaultPrice = 939.5f, CurrentPrice = 939f, MinPrice = 699f, MaxPrice = 1180f, Weight = 4f
            },
            //new WeaponModel(){ Hash = WeaponHashes.CarbineRifleMk2, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.AssaultRifles, Description = "Waffe", DisplayedName = "Karabinergewehr Mk2", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f, Weight = 4f },
            //new WeaponModel(){ Hash = WeaponHashes.CompactRifle, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.AssaultRifles, Description = "Waffe", DisplayedName = "Kompaktes Gewehr", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f, Weight = 3f },
            //new WeaponModel(){ Hash = WeaponHashes.SpecialCarbine, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.AssaultRifles, Description = "Waffe", DisplayedName = "Spezieal Karabiner", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f, Weight = 4f },
            //new WeaponModel(){ Hash = WeaponHashes.SpecialCarbineMk2, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.AssaultRifles, Description = "Waffe", DisplayedName = "Spezieal Karabiner Mk2", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f, Weight = 4f },

            // #-LightMachineGun-#
            //new WeaponModel(){ Hash = WeaponHashes.Combatmg, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.LightMachineGun, Description = "Waffe", DisplayedName = "Schweres MG", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f, Weight = 10f },
            //new WeaponModel(){ Hash = WeaponHashes.Combatmg_mk2, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.LightMachineGun, Description = "Waffe", DisplayedName = "Schweres MG Mk2", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f, Weight = 10f },
            //new WeaponModel(){ Hash = WeaponHashes.Gusenberg, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.LightMachineGun, Description = "Waffe", DisplayedName = "Gusenberg-Bleispritze", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f, Weight = 8f },
            //new WeaponModel(){ Hash = WeaponHashes.Mg, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.LightMachineGun, Description = "Waffe", DisplayedName = "MG", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f, Weight = 9f },

            // #-SniperRifle-#
            //new WeaponModel(){ Hash = WeaponHashes.HeavySniper, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.SniperRifle, Description = "Waffe", DisplayedName = "Schweres Scharfschützengewehr", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f, Weight = 10f },
            //new WeaponModel(){ Hash = WeaponHashes.HeavySniperMk2, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.SniperRifle, Description = "Waffe", DisplayedName = "Schweres Scharfschützengewehr Mk2", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f, Weight = 10f },
            //new WeaponModel(){ Hash = WeaponHashes.MarksmanRifle, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.SniperRifle, Description = "Waffe", DisplayedName = "Präzisionsgewehr", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f, Weight = 6f },
            //new WeaponModel(){ Hash = WeaponHashes.MarksmanRifleMk2, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.SniperRifle, Description = "Waffe", DisplayedName = "Präzisionsgewehr Mk2", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f, Weight = 6f },
            //new WeaponModel(){ Hash = WeaponHashes.sniperrifle, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.SniperRifle, Description = "Waffe", DisplayedName = "Scharfschützengewehr Mk3", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f, Weight = 5f },

            // #-HeavyWeapon-#
            //new WeaponModel(){ Hash = WeaponHashes.CompactLauncher, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.HeavyWeapon, Description = "Waffe", DisplayedName = "Granatwerfer", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f, Weight = 10f },
            //new WeaponModel(){ Hash = WeaponHashes.Firework, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.HeavyWeapon, Description = "Werkzeug", DisplayedName = "Feuerwerkabschussgerät", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f, Weight = 10f },
            //new WeaponModel(){ Hash = WeaponHashes.Grenadelauncher, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.HeavyWeapon, Description = "Waffe", DisplayedName = "Automatischer Granatwerfer", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f, Weight = 15f },
            //new WeaponModel(){ Hash = WeaponHashes.GrenadelauncherSmoke, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.HeavyWeapon, Description = "Waffe", DisplayedName = "Smoke-Granatwerfer", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f, Weight = 5f },
            //new WeaponModel(){ Hash = WeaponHashes.HomingLauncher, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.HeavyWeapon, Description = "Waffe", DisplayedName = "Lenkraketenwerfer", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f, Weight = 10f },
            //new WeaponModel(){ Hash = WeaponHashes.Minigun, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.HeavyWeapon, Description = "Waffe", DisplayedName = "Minigun", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f, Weight = 20f },
            //new WeaponModel(){ Hash = WeaponHashes.Railgun, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.HeavyWeapon, Description = "Waffe", DisplayedName = "Railgun", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f, Weight = 10f },
            //new WeaponModel(){ Hash = WeaponHashes.RayMinigun, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.HeavyWeapon, Description = "Waffe", DisplayedName = "Witwenmacher", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f, Weight = 15f },
            //new WeaponModel(){ Hash = WeaponHashes.Rpg, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.HeavyWeapon, Description = "Waffe", DisplayedName = "Panzerfaust", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f, Weight = 10f },

            // #-Throwable-#
            //new WeaponModel(){ Hash = WeaponHashes.Ball, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.Throwable, Description = "Spielzeug", DisplayedName = "Ball", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f, Weight = 1f },
            //new WeaponModel(){ Hash = WeaponHashes.BzGas, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.Throwable, Description = "Waffe", DisplayedName = "Tränengasgranate", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f, Weight = 0.5f },
            new WeaponModel
            {
                Hash = WeaponHashes.Flare, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.Throwable,
                Description = "Werkzeug", DisplayedName = "Signalfackel", DefaultPrice = 22.5f, CurrentPrice = 22.5f,
                MinPrice = 11f, MaxPrice = 34f, Weight = 0.5f
            },
            //new WeaponModel(){ Hash = WeaponHashes.Grenade, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.Throwable, Description = "Waffe", DisplayedName = "Granate", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f, Weight = 0.5f },
            //new WeaponModel(){ Hash = WeaponHashes.Molotov, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.Throwable, Description = "Waffe", DisplayedName = "Molotowcocktail", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f, Weight = 1f },
            //new WeaponModel(){ Hash = WeaponHashes.Pipebomb, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.Throwable, Description = "Waffe", DisplayedName = "Rohrbombe", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f, Weight = 1.5f },
            //new WeaponModel(){ Hash = WeaponHashes.Proxmine, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.Throwable, Description = "Waffe", DisplayedName = "Annährungsmine", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f, Weight = 1f },
            new WeaponModel
            {
                Hash = WeaponHashes.Smokegrenade, Faction = FactionEnums.Lspd, WeaponType = WeaponTypeEnums.Throwable,
                Description = "Waffe", DisplayedName = "Rauchgranate", DefaultPrice = 16.5f, CurrentPrice = 16.5f,
                MinPrice = 10f, MaxPrice = 23f, Weight = 0.5f
            },
            //new WeaponModel(){ Hash = WeaponHashes.Snowball, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.Throwable, Description = "Spielzeug", DisplayedName = "Schneeball", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f, Weight = 0.2f },
            //new WeaponModel(){ Hash = WeaponHashes.Stickybomb, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.Throwable, Description = "Waffe", DisplayedName = "Haftbombe", DefaultPrice = 0f, CurrentPrice = 0f, MinPrice = 0f, MaxPrice = 0f, Weight = 2f },

            // #-Miscellaneous-#
            new WeaponModel
            {
                Hash = WeaponHashes.Fireextinguisher, Faction = FactionEnums.Civ,
                WeaponType = WeaponTypeEnums.Miscellaneous, Description = "Werkzeug", DisplayedName = "Feuerlöscher",
                DefaultPrice = 30f, CurrentPrice = 30f, MinPrice = 10f, MaxPrice = 50f, Weight = 10f
            },
            new WeaponModel
            {
                Hash = WeaponHashes.Parachute, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.Miscellaneous,
                Description = "Werkzeug", DisplayedName = "Fallschirm", DefaultPrice = 2150f, CurrentPrice = 2150f,
                MinPrice = 1500f, MaxPrice = 2800f, Weight = 10f
            },
            new WeaponModel
            {
                Hash = WeaponHashes.Petrolcan, Faction = FactionEnums.Civ, WeaponType = WeaponTypeEnums.Miscellaneous,
                Description = "Werkzeug", DisplayedName = "Benzinkanister", DefaultPrice = 35f, CurrentPrice = 35f,
                MinPrice = 20f, MaxPrice = 50f, Weight = 25f
            }
        };
    }
}