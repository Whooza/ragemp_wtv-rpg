﻿using System.Collections.Generic;
using GTANetworkAPI;
using RolePlayGameData.Models;

namespace RolePlayGameData.Configs
{
    public class LocationCfg
    {
        public static readonly SortedDictionary<string, LocationIplModel> LocationList =
            new SortedDictionary<string, LocationIplModel>
            {
                {
                    "DesertBunkerEntrance",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "gr_case0_bunkerclosed", IplPrice = 50000f,
                        LocationPosition = new Vector3(848.6175, 2996.567, 45.81612)
                    }
                },
                {
                    "SmokeTreeBunkerEntrance",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "gr_case1_bunkerclosed", IplPrice = 50000f,
                        LocationPosition = new Vector3(2126.785, 3335.04, 48.21422)
                    }
                },
                {
                    "ScrapyardBunkerEntrance",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "gr_case2_bunkerclosed", IplPrice = 50000f,
                        LocationPosition = new Vector3(2493.654, 3140.399, 51.28789)
                    }
                },
                {
                    "OilfieldsBunkerEntrance",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "gr_case3_bunkerclosed", IplPrice = 50000f,
                        LocationPosition = new Vector3(481.0465, 2995.135, 43.96672)
                    }
                },
                {
                    "RatonCanyonBunkerEntrance",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "gr_case4_bunkerclosed", IplPrice = 50000f,
                        LocationPosition = new Vector3(-391.3216, 4363.728, 58.65862)
                    }
                },
                {
                    "GrapeseedBunkerEntrance",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "gr_case5_bunkerclosed", IplPrice = 50000f,
                        LocationPosition = new Vector3(1823.961, 4708.14, 42.4991)
                    }
                },
                {
                    "FarmhouseBunkerEntrance",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "gr_case6_bunkerclosed", IplPrice = 50000f,
                        LocationPosition = new Vector3(1570.372, 2254.549, 78.89397)
                    }
                },
                {
                    "PalletoBunkerEntrance",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "gr_case7_bunkerclosed", IplPrice = 50000f,
                        LocationPosition = new Vector3(-783.0755, 5934.686, 24.31475)
                    }
                },
                {
                    "Route68BunkerEntrance",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "gr_case9_bunkerclosed", IplPrice = 50000f,
                        LocationPosition = new Vector3(24.43542, 2959.705, 58.35517)
                    }
                },
                {
                    "ZancudoBunkerEntrance",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "gr_case10_bunkerclosed", IplPrice = 50000f,
                        LocationPosition = new Vector3(-3058.714, 3329.19, 12.5844)
                    }
                },
                {
                    "Route1BunkerEntrance",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "gr_case11_bunkerclosed", IplPrice = 50000f,
                        LocationPosition = new Vector3(-3180.466, 1374.192, 19.9597)
                    }
                },
                {
                    "Bunker",
                    new LocationIplModel
                    {
                        LoadIpl = false, IplName = "none", IplPrice = 150000000f,
                        LocationPosition = new Vector3(899.5518, -3246.038, -98.04907)
                    }
                },
                {
                    "ApartmentOneModern",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "apa_v_mp_h_01_a", IplPrice = 2500000f,
                        LocationPosition = new Vector3(-786.8663, 315.7642, 217.6385)
                    }
                },
                {
                    "ApartmentOneMody",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "apa_v_mp_h_02_a", IplPrice = 2500000f,
                        LocationPosition = new Vector3(-787.0749, 315.8198, 217.6386)
                    }
                },
                {
                    "ApartmentOneVibrant",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "apa_v_mp_h_03_a", IplPrice = 2500000f,
                        LocationPosition = new Vector3(-786.6245, 315.6175, 217.6385)
                    }
                },
                {
                    "ApartmentOneSharp",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "apa_v_mp_h_04_a", IplPrice = 2500000f,
                        LocationPosition = new Vector3(-787.0902, 315.7039, 217.6384)
                    }
                },
                {
                    "ApartmentOneMonochrome",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "apa_v_mp_h_05_a", IplPrice = 2500000f,
                        LocationPosition = new Vector3(-786.9887, 315.7393, 217.6386)
                    }
                },
                {
                    "ApartmentOneSeductive",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "apa_v_mp_h_06_a", IplPrice = 2500000f,
                        LocationPosition = new Vector3(-787.1423, 315.6943, 217.6384)
                    }
                },
                {
                    "ApartmentOneRegal",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "apa_v_mp_h_07_a", IplPrice = 2500000f,
                        LocationPosition = new Vector3(-787.029, 315.7113, 217.6385)
                    }
                },
                {
                    "ApartmentOneAqua",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "apa_v_mp_h_08_a", IplPrice = 2500000f,
                        LocationPosition = new Vector3(-786.9469, 315.5655, 217.6383)
                    }
                },
                {
                    "ApartmentTwoModern",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "apa_v_mp_h_01_b", IplPrice = 2500000f,
                        LocationPosition = new Vector3(-774.0126, 342.0428, 196.6864)
                    }
                },
                {
                    "ApartmentTwoMody",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "apa_v_mp_h_02_b", IplPrice = 2500000f,
                        LocationPosition = new Vector3(-774.1382, 342.0316, 196.6864)
                    }
                },
                {
                    "ApartmentTwoVibrant",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "apa_v_mp_h_03_b", IplPrice = 2500000f,
                        LocationPosition = new Vector3(-774.0223, 342.1718, 196.6863)
                    }
                },
                {
                    "ApartmentTwoSharp",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "apa_v_mp_h_04_b", IplPrice = 2500000f,
                        LocationPosition = new Vector3(-773.8976, 342.1525, 196.6863)
                    }
                },
                {
                    "ApartmentTwoMonochrome",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "apa_v_mp_h_05_b", IplPrice = 2500000f,
                        LocationPosition = new Vector3(-774.0675, 342.0773, 196.6864)
                    }
                },
                {
                    "ApartmentTwoSeductive",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "apa_v_mp_h_06_b", IplPrice = 2500000f,
                        LocationPosition = new Vector3(-773.9552, 341.9892, 196.6862)
                    }
                },
                {
                    "ApartmentTwoRegal",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "apa_v_mp_h_07_b", IplPrice = 2500000f,
                        LocationPosition = new Vector3(-774.0109, 342.0965, 196.6863)
                    }
                },
                {
                    "ApartmentTwoAqua",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "apa_v_mp_h_08_b", IplPrice = 2500000f,
                        LocationPosition = new Vector3(-774.0349, 342.0296, 196.6862)
                    }
                },
                {
                    "ApartmentThreeModern",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "apa_v_mp_h_01_c", IplPrice = 2500000f,
                        LocationPosition = new Vector3(-786.9563, 315.6229, 187.9136)
                    }
                },
                {
                    "ApartmentThreeMody",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "apa_v_mp_h_02_c", IplPrice = 2500000f,
                        LocationPosition = new Vector3(-786.8195, 315.5634, 187.9137)
                    }
                },
                {
                    "ApartmentThreeVibrant",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "apa_v_mp_h_03_c", IplPrice = 2500000f,
                        LocationPosition = new Vector3(-786.9584, 315.7974, 187.9135)
                    }
                },
                {
                    "ApartmentThreeSharp",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "apa_v_mp_h_04_c", IplPrice = 2500000f,
                        LocationPosition = new Vector3(-787.0155, 315.7071, 187.9135)
                    }
                },
                {
                    "ApartmentThreeMonochrome",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "apa_v_mp_h_05_c", IplPrice = 2500000f,
                        LocationPosition = new Vector3(-786.8809, 315.6634, 187.9136)
                    }
                },
                {
                    "ApartmentThreeSeductive",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "apa_v_mp_h_06_c", IplPrice = 2500000f,
                        LocationPosition = new Vector3(-787.0961, 315.815, 187.9135)
                    }
                },
                {
                    "ApartmentThreeRegal",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "apa_v_mp_h_07_c", IplPrice = 2500000f,
                        LocationPosition = new Vector3(-787.0574, 315.6567, 187.9135)
                    }
                },
                {
                    "ApartmentThreeAqua",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "apa_v_mp_h_08_c", IplPrice = 2500000f,
                        LocationPosition = new Vector3(-786.9756, 315.723, 187.9134)
                    }
                },
                {
                    "ArcadiusBusinessCentreOldSpiceWarm",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "ex_dt1_02_office_01a", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-141.4966, -620.8292, 168.8204)
                    }
                },
                {
                    "ArcadiusBusinessCentreOldSpiceClassical",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "ex_dt1_02_office_01b", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-141.3997, -620.9006, 168.8204)
                    }
                },
                {
                    "ArcadiusBusinessCentreOldSpiceVintage",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "ex_dt1_02_office_01c", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-141.5361, -620.9186, 168.8204)
                    }
                },
                {
                    "ArcadiusBusinessCentreExecutiveContrast",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "ex_dt1_02_office_02a", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-141.2896, -620.9618, 168.8204)
                    }
                },
                {
                    "ArcadiusBusinessCentreExecutiveRich",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "ex_dt1_02_office_02b", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-141.1987, -620.913, 168.8205)
                    }
                },
                {
                    "ArcadiusBusinessCentreExecutiveCool",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "ex_dt1_02_office_02c", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-141.5429, -620.9524, 168.8204)
                    }
                },
                {
                    "ArcadiusBusinessCentrePowerBrokerIce",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "ex_dt1_02_office_03a", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-141.392, -621.0451, 168.8204)
                    }
                },
                {
                    "ArcadiusBusinessCentrePowerBrokerConservative",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "ex_dt1_02_office_03b", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-141.1945, -620.8729, 168.8204)
                    }
                },
                {
                    "ArcadiusBusinessCentrePowerBrokerPolished",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "ex_dt1_02_office_03c", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-141.4924, -621.0035, 168.8205)
                    }
                },
                {
                    "ArcadiusBusinessCentreGarage1",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "imp_dt1_02_cargarage_a", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-191.0133, -579.1428, 135.0000)
                    }
                },
                {
                    "ArcadiusBusinessCentreGarage2",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "imp_dt1_02_cargarage_b", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-117.4989, -568.1132, 135.0000)
                    }
                },
                {
                    "ArcadiusBusinessCentreGarage3",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "imp_dt1_02_cargarage_c", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-136.0780, -630.1852, 135.0000)
                    }
                },
                {
                    "ArcadiusBusinessCentreModShop",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "imp_dt1_02_modgarage", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-146.6166, -596.6301, 166.0000)
                    }
                },
                {
                    "MazeBankBuildingOldSpiceWarm",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "ex_dt1_11_office_01a", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-75.44054, -827.1487, 243.3859)
                    }
                },
                {
                    "MazeBankBuildingOldSpiceClassical",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "ex_dt1_11_office_01b", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-75.63942, -827.1022, 243.3859)
                    }
                },
                {
                    "MazeBankBuildingOldSpiceVintage",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "ex_dt1_11_office_01c", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-75.47446, -827.2621, 243.386)
                    }
                },
                {
                    "MazeBankBuildingExecutiveContrast",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "ex_dt1_11_office_02a", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-75.49827, -827.1889, 243.386)
                    }
                },
                {
                    "MazeBankBuildingExecutiveRich",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "ex_dt1_11_office_02b", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-75.8466, -826.9893, 243.3859)
                    }
                },
                {
                    "MazeBankBuildingExecutiveCool",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "ex_dt1_11_office_02c", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-75.49945, -827.05, 243.386)
                    }
                },
                {
                    "MazeBankBuildingPowerBrokerIce",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "ex_dt1_11_office_03a", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-75.56978, -827.1152, 243.3859)
                    }
                },
                {
                    "MazeBankBuildingPowerBrokerConservative",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "ex_dt1_11_office_03b", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-75.51953, -827.0786, 243.3859)
                    }
                },
                {
                    "MazeBankBuildingPowerBrokerPolished",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "ex_dt1_11_office_03c", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-75.41915, -827.1118, 243.3858)
                    }
                },
                {
                    "MazeBankBuildingGarage1",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "imp_dt1_11_cargarage_a", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-84.2193, -823.0851, 221.0000)
                    }
                },
                {
                    "MazeBankBuildingGarage2",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "imp_dt1_11_cargarage_b", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-69.8627, -824.7498, 221.0000)
                    }
                },
                {
                    "MazeBankBuildingGarage3",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "imp_dt1_11_cargarage_c", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-80.4318, -813.2536, 221.0000)
                    }
                },
                {
                    "MazeBankBuildingModShop",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "imp_dt1_11_modgarage", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-73.9039, -821.6204, 284.0000)
                    }
                },
                {
                    "LomBankOldSpiceWarm",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "ex_sm_13_office_01a", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-1579.702, -565.0366, 108.5229)
                    }
                },
                {
                    "LomBankOldSpiceClassical",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "ex_sm_13_office_01b", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-1579.643, -564.9685, 108.5229)
                    }
                },
                {
                    "LomBankOldSpiceVintage",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "ex_sm_13_office_01c", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-1579.681, -565.0003, 108.523)
                    }
                },
                {
                    "LomBankExecutiveContrast",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "ex_sm_13_office_02a", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-1579.583, -565.0399, 108.5229)
                    }
                },
                {
                    "LomBankExecutiveRich",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "ex_sm_13_office_02b", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-1579.756, -565.0661, 108.523)
                    }
                },
                {
                    "LomBankExecutiveCool",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "ex_sm_13_office_02c", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-1579.678, -565.0034, 108.5229)
                    }
                },
                {
                    "LomBankPowerBrokerIce",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "ex_sm_13_office_03a", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-1579.677, -565.0689, 108.5229)
                    }
                },
                {
                    "LomBankPowerBrokerConservative",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "ex_sm_13_office_03b", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-1579.708, -564.9634, 108.5229)
                    }
                },
                {
                    "LomBankPowerBrokerPolished",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "ex_sm_13_office_03c", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-1579.693, -564.8981, 108.5229)
                    }
                },
                {
                    "LomBankGarage1",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "imp_sm_13_cargarage_a", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-1581.1120, -567.2450, 85.5000)
                    }
                },
                {
                    "LomBankGarage2",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "imp_sm_13_cargarage_b", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-1568.7390, -562.0455, 85.5000)
                    }
                },
                {
                    "LomBankGarage3",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "imp_sm_13_cargarage_c", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-1563.5570, -574.4314, 85.5000)
                    }
                },
                {
                    "LomBankModShop",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "imp_sm_13_modgarage", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-1578.0230, -576.4251, 104.2000)
                    }
                },
                {
                    "MazeBankWestOldSpiceWarm",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "ex_sm_15_office_01a", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-1392.617, -480.6363, 72.04208)
                    }
                },
                {
                    "MazeBankWestOldSpiceClassical",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "ex_sm_15_office_01b", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-1392.532, -480.7649, 72.04207)
                    }
                },
                {
                    "MazeBankWestOldSpiceVintage",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "ex_sm_15_office_01c", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-1392.611, -480.5562, 72.04214)
                    }
                },
                {
                    "MazeBankWestExecutiveContrast",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "ex_sm_15_office_02a", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-1392.626, -480.4856, 72.04212)
                    }
                },
                {
                    "MazeBankWestExecutiveRich",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "ex_sm_15_office_02b", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-1392.667, -480.4736, 72.04217)
                    }
                },
                {
                    "MazeBankWestExecutiveCool",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "ex_sm_15_office_02c", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-1392.542, -480.4011, 72.04211)
                    }
                },
                {
                    "MazeBankWestPowerBrokerIce",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "ex_sm_15_office_03a", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-1392.563, -480.549, 72.0421)
                    }
                },
                {
                    "MazeBankWestPowerBrokerConservative",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "ex_sm_15_office_03b", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-1392.528, -480.475, 72.04206)
                    }
                },
                {
                    "MazeBankWestPowerBrokerPolished",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "ex_sm_15_office_03c", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-1392.416, -480.7485, 72.04207)
                    }
                },
                {
                    "MazeBankWestGarage1",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "imp_sm_15_cargarage_a", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-1388.8400, -478.7402, 56.1000)
                    }
                },
                {
                    "MazeBankWestGarage2",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "imp_sm_15_cargarage_b", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-1388.8600, -478.7574, 48.1000)
                    }
                },
                {
                    "MazeBankWestGarage3",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "imp_sm_15_cargarage_c", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-1374.6820, -474.3586, 56.1000)
                    }
                },
                {
                    "MazeBankWestModShop",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "imp_sm_15_modgarage", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-1391.2450, -473.9638, 77.2000)
                    }
                },
                {
                    "Clubhouse1",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "bkr_biker_interior_placement_interior_0_biker_dlc_int_01_milo",
                        IplPrice = 15000000f,
                        LocationPosition = new Vector3(1107.04, -3157.399, -37.51859)
                    }
                },
                {
                    "Clubhouse2",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "bkr_biker_interior_placement_interior_1_biker_dlc_int_02_milo",
                        IplPrice = 15000000f,
                        LocationPosition = new Vector3(998.4809, -3164.711, -38.90733)
                    }
                },
                {
                    "MethLab",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "bkr_biker_interior_placement_interior_2_biker_dlc_int_ware01_milo",
                        IplPrice = 10000000f,
                        LocationPosition = new Vector3(1009.5, -3196.6, -38.99682)
                    }
                },
                {
                    "WeedFarm",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "bkr_biker_interior_placement_interior_3_biker_dlc_int_ware02_milo",
                        IplPrice = 10000000f,
                        LocationPosition = new Vector3(1051.491, -3196.536, -39.14842)
                    }
                },
                {
                    "CocaineLockup",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "bkr_biker_interior_placement_interior_4_biker_dlc_int_ware03_milo",
                        IplPrice = 10000000f,
                        LocationPosition = new Vector3(1093.6, -3196.6, -38.99841)
                    }
                },
                {
                    "CounterfeitCashFactory",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "bkr_biker_interior_placement_interior_5_biker_dlc_int_ware04_milo",
                        IplPrice = 10000000f,
                        LocationPosition = new Vector3(1121.897, -3195.338, -40.4025)
                    }
                },
                {
                    "DocumentForgeryOffice",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "bkr_biker_interior_placement_interior_6_biker_dlc_int_ware05_milo",
                        IplPrice = 10000000f,
                        LocationPosition = new Vector3(1165, -3196.6, -39.01306)
                    }
                },
                {
                    "WarehouseSmall",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "ex_exec_warehouse_placement_interior_1_int_warehouse_s_dlc_milo",
                        IplPrice = 250000f,
                        LocationPosition = new Vector3(-1392.416, -480.7485, 72.04207)
                    }
                },
                {
                    "WarehouseMedium",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "ex_exec_warehouse_placement_interior_0_int_warehouse_m_dlc_milo",
                        IplPrice = 500000f,
                        LocationPosition = new Vector3(1056.486, -3105.724, -39.00439)
                    }
                },
                {
                    "WarehouseLarge",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "ex_exec_warehouse_placement_interior_2_int_warehouse_l_dlc_milo",
                        IplPrice = 1250000f,
                        LocationPosition = new Vector3(1006.967, -3102.079, -39.0035)
                    }
                },
                {
                    "VehicleWarehouse",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "imp_impexp_interior_placement_interior_1_impexp_intwaremed_milo_",
                        IplPrice = 5000000f,
                        LocationPosition = new Vector3(994.5925, -3002.594, -39.64699)
                    }
                },
                {
                    "LostMCClubhouse",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "bkr_bi_hw1_13_int", IplPrice = 7500000f,
                        LocationPosition = new Vector3(982.0083, -100.8747, 74.84512)
                    }
                },
                {
                    "UnionDepository",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "FINBANK", IplPrice = 0f,
                        LocationPosition = new Vector3(2.6968, -667.0166, 16.13061)
                    }
                },
                {
                    "FIBLobby",
                    new LocationIplModel
                    {
                        LoadIpl = true, IplName = "FIBlobby", IplPrice = 0f,
                        LocationPosition = new Vector3(110.4, -744.2, 45.7496)
                    }
                },
                {
                    "GunrunningHeistYacht",
                    new LocationIplModel
                    {
                        LoadIpl = false, IplName = "none", IplPrice = 50000000f,
                        LocationPosition = new Vector3(1373.828, 6737.393, 6.707596)
                    }
                },
                {
                    "DignityHeistYacht",
                    new LocationIplModel
                    {
                        LoadIpl = false, IplName = "none", IplPrice = 50000000f,
                        LocationPosition = new Vector3(-2027.946, -1036.695, 6.707587)
                    }
                },
                {
                    "GarageTwoCars",
                    new LocationIplModel
                    {
                        LoadIpl = false, IplName = "none", IplPrice = 1250000f,
                        LocationPosition = new Vector3(173.2903, -1003.6, -99.65707)
                    }
                },
                {
                    "GarageSixCars",
                    new LocationIplModel
                    {
                        LoadIpl = false, IplName = "none", IplPrice = 5000000f,
                        LocationPosition = new Vector3(197.8153, -1002.293, -99.65749)
                    }
                },
                {
                    "GarageTenCars",
                    new LocationIplModel
                    {
                        LoadIpl = false, IplName = "none", IplPrice = 7500000f,
                        LocationPosition = new Vector3(229.9559, -981.7928, -99.66071)
                    }
                },
                {
                    "CharCreator",
                    new LocationIplModel
                    {
                        LoadIpl = false, IplName = "none", IplPrice = 50000000f,
                        LocationPosition = new Vector3(402.5164, -1002.847, -99.2587)
                    }
                },
                {
                    "MissionCarpark",
                    new LocationIplModel
                    {
                        LoadIpl = false, IplName = "none", IplPrice = 50000000f,
                        LocationPosition = new Vector3(405.9228, -954.1149, -99.6627)
                    }
                },
                {
                    "TortureRoom",
                    new LocationIplModel
                    {
                        LoadIpl = false, IplName = "none", IplPrice = 1250000f,
                        LocationPosition = new Vector3(136.5146, -2203.149, 7.30914)
                    }
                },
                {
                    "Solomon'sOffice",
                    new LocationIplModel
                    {
                        LoadIpl = false, IplName = "none", IplPrice = 5000000f,
                        LocationPosition = new Vector3(-1005.84, -478.92, 50.02733)
                    }
                },
                {
                    "Psychiatrist'sOffice",
                    new LocationIplModel
                    {
                        LoadIpl = false, IplName = "none", IplPrice = 7500000f,
                        LocationPosition = new Vector3(-1908.024, -573.4244, 19.09722)
                    }
                },
                {
                    "Omega'sGarage",
                    new LocationIplModel
                    {
                        LoadIpl = false, IplName = "none", IplPrice = 5000f,
                        LocationPosition = new Vector3(2331.344, 2574.073, 46.68137)
                    }
                },
                {
                    "MovieTheatre",
                    new LocationIplModel
                    {
                        LoadIpl = false, IplName = "none", IplPrice = 50000000f,
                        LocationPosition = new Vector3(-1427.299, -245.1012, 16.8039)
                    }
                },
                {
                    "MadrazosRanch",
                    new LocationIplModel
                    {
                        LoadIpl = false, IplName = "none", IplPrice = 11000000f,
                        LocationPosition = new Vector3(1399, 1150, 115)
                    }
                },
                {
                    "LifeInvaderOffice",
                    new LocationIplModel
                    {
                        LoadIpl = false, IplName = "none", IplPrice = 0f,
                        LocationPosition = new Vector3(-1044.193, -236.9535, 37.96496)
                    }
                },
                {
                    "Lester'sHouse",
                    new LocationIplModel
                    {
                        LoadIpl = false, IplName = "none", IplPrice = 0f,
                        LocationPosition = new Vector3(1273.9, -1719.305, 54.77141)
                    }
                },
                {
                    "Smuggler'sRunHangar",
                    new LocationIplModel
                    {
                        LoadIpl = false, IplName = "none", IplPrice = 0f,
                        LocationPosition = new Vector3(-1266.802, -3014.837, -49.000)
                    }
                },
                {
                    "Facility",
                    new LocationIplModel
                    {
                        LoadIpl = false, IplName = "none", IplPrice = 0f,
                        LocationPosition = new Vector3(345.0041, 4842.001, -59.9997)
                    }
                },
                {
                    "Submarine",
                    new LocationIplModel
                    {
                        LoadIpl = false, IplName = "none", IplPrice = 0f,
                        LocationPosition = new Vector3(514.33, 4886.18, -62.59)
                    }
                },
                {
                    "Nightclub",
                    new LocationIplModel
                    {
                        LoadIpl = false, IplName = "none", IplPrice = 0f,
                        LocationPosition = new Vector3(-1604.664, -3012.583, -78.000)
                    }
                },
                {
                    "NightclubWarehouse",
                    new LocationIplModel
                    {
                        LoadIpl = false, IplName = "none", IplPrice = 0f,
                        LocationPosition = new Vector3(-1505.783, -3012.587, -80.000)
                    }
                },
                {
                    "Motel",
                    new LocationIplModel
                    {
                        LoadIpl = false, IplName = "none", IplPrice = 1500f,
                        LocationPosition = new Vector3(152.2605, -1004.471, -98.99999)
                    }
                },
                {
                    "LowEndApartment",
                    new LocationIplModel
                    {
                        LoadIpl = false, IplName = "none", IplPrice = 15000f,
                        LocationPosition = new Vector3(261.4586, -998.8196, -99.00863)
                    }
                },
                {
                    "MediumEndApartment",
                    new LocationIplModel
                    {
                        LoadIpl = false, IplName = "none", IplPrice = 0f,
                        LocationPosition = new Vector3(347.2686, -999.2955, -99.19622)
                    }
                },
                {
                    "4IntegrityWay,Apt30",
                    new LocationIplModel
                    {
                        LoadIpl = false, IplName = "none", IplPrice = 0f,
                        LocationPosition = new Vector3(-35.31277, -580.4199, 88.71221)
                    }
                },
                {
                    "DellPerroHeights, Apt4",
                    new LocationIplModel
                    {
                        LoadIpl = false, IplName = "none", IplPrice = 0f,
                        LocationPosition = new Vector3(-1468.14, -541.815, 73.4442)
                    }
                },
                {
                    "DellPerroHeights,Apt7",
                    new LocationIplModel
                    {
                        LoadIpl = false, IplName = "none", IplPrice = 0f,
                        LocationPosition = new Vector3(-1477.14, -538.7499, 55.5264)
                    }
                },
                {
                    "RichardMajestic, Apt2",
                    new LocationIplModel
                    {
                        LoadIpl = false, IplName = "none", IplPrice = 0f,
                        LocationPosition = new Vector3(-915.811, -379.432, 113.6748)
                    }
                },
                {
                    "4IntegrityWay,Apt28",
                    new LocationIplModel
                    {
                        LoadIpl = false, IplName = "none", IplPrice = 0f,
                        LocationPosition = new Vector3(-614.86, 40.6783, 97.60007)
                    }
                },
                {
                    "EclipseTowers, Apt3",
                    new LocationIplModel
                    {
                        LoadIpl = false, IplName = "none", IplPrice = 0f,
                        LocationPosition = new Vector3(-773.407, 341.766, 211.397)
                    }
                },
                {
                    "3655WildOatsDrive",
                    new LocationIplModel
                    {
                        LoadIpl = false, IplName = "none", IplPrice = 0f,
                        LocationPosition = new Vector3(-169.286, 486.4938, 137.4436)
                    }
                },
                {
                    "2044NorthConkerAvenue",
                    new LocationIplModel
                    {
                        LoadIpl = false, IplName = "none", IplPrice = 0f,
                        LocationPosition = new Vector3(340.9412, 437.1798, 149.3925)
                    }
                },
                {
                    "2045NorthConkerAvenue",
                    new LocationIplModel
                    {
                        LoadIpl = false, IplName = "none", IplPrice = 0f,
                        LocationPosition = new Vector3(373.023, 416.105, 145.7006)
                    }
                },
                {
                    "2862HillcrestAvenue",
                    new LocationIplModel
                    {
                        LoadIpl = false, IplName = "none", IplPrice = 0f,
                        LocationPosition = new Vector3(-676.127, 588.612, 145.1698)
                    }
                },
                {
                    "2868HillcrestAvenue",
                    new LocationIplModel
                    {
                        LoadIpl = false, IplName = "none", IplPrice = 0f,
                        LocationPosition = new Vector3(-763.107, 615.906, 144.1401)
                    }
                },
                {
                    "2874HillcrestAvenue",
                    new LocationIplModel
                    {
                        LoadIpl = false, IplName = "none", IplPrice = 0f,
                        LocationPosition = new Vector3(-857.798, 682.563, 152.6529)
                    }
                },
                {
                    "2677WhispymoundDrive",
                    new LocationIplModel
                    {
                        LoadIpl = false, IplName = "none", IplPrice = 0f,
                        LocationPosition = new Vector3(120.5, 549.952, 184.097)
                    }
                },
                {
                    "2133MadWayneThunder",
                    new LocationIplModel
                    {
                        LoadIpl = false, IplName = "none", IplPrice = 0f,
                        LocationPosition = new Vector3(-1288, 440.748, 97.69459)
                    }
                }
            };

        public static readonly SortedDictionary<string, List<SortedDictionary<string, InteriorPropModel>>>
            InteriorPropList =
                new SortedDictionary<string, List<SortedDictionary<string, InteriorPropModel>>>
                {
                    {
                        "Bunker",
                        new List<SortedDictionary<string, InteriorPropModel>>
                        {
                            new SortedDictionary<string, InteriorPropModel>
                            {
                                {
                                    "BunkerStyle1",
                                    new InteriorPropModel
                                        {PropId = 258561, PropString = "bunker_style_a", PropPrice = 10000f}
                                },
                                {
                                    "BunkerStyle2",
                                    new InteriorPropModel
                                        {PropId = 258561, PropString = "bunker_style_b", PropPrice = 10000f}
                                },
                                {
                                    "BunkerStyle3",
                                    new InteriorPropModel
                                        {PropId = 258561, PropString = "bunker_style_c", PropPrice = 10000f}
                                },
                                {
                                    "StandardBunkerSet",
                                    new InteriorPropModel
                                        {PropId = 258561, PropString = "standard_bunker_set", PropPrice = 100000f}
                                },
                                {
                                    "UpgradeBunkerSet",
                                    new InteriorPropModel
                                        {PropId = 258561, PropString = "upgrade_bunker_set", PropPrice = 100000f}
                                },
                                {
                                    "Security",
                                    new InteriorPropModel
                                        {PropId = 258561, PropString = "standard_security_set", PropPrice = 25000f}
                                },
                                {
                                    "SecurityUpgrade",
                                    new InteriorPropModel
                                        {PropId = 258561, PropString = "security_upgrade", PropPrice = 25000f}
                                },
                                {
                                    "BunkerOffice",
                                    new InteriorPropModel
                                        {PropId = 258561, PropString = "office_blocker_set", PropPrice = 10000f}
                                },
                                {
                                    "OfficeUpgrade",
                                    new InteriorPropModel
                                        {PropId = 258561, PropString = "office_upgrade_set", PropPrice = 10000f}
                                },
                                {
                                    "GunLockerUpgrade",
                                    new InteriorPropModel
                                        {PropId = 258561, PropString = "gun_locker_upgrade", PropPrice = 1000f}
                                },
                                {
                                    "GunRangeUpgrade",
                                    new InteriorPropModel
                                        {PropId = 258561, PropString = "gun_range_blocker_set", PropPrice = 2500f}
                                },
                                {
                                    "GunWallUpgrade",
                                    new InteriorPropModel
                                        {PropId = 258561, PropString = "gun_wall_blocker", PropPrice = 20000f}
                                },
                                {
                                    "GunRangeLightUpgrade",
                                    new InteriorPropModel
                                        {PropId = 258561, PropString = "gun_range_lights", PropPrice = 5000f}
                                },
                                {
                                    "GunSchematics",
                                    new InteriorPropModel
                                        {PropId = 258561, PropString = "Gun_schematic_set", PropPrice = 5000f}
                                }
                            }
                        }
                    },
                    {
                        "VehicleWarehouse",
                        new List<SortedDictionary<string, InteriorPropModel>>
                        {
                            new SortedDictionary<string, InteriorPropModel>
                            {
                                {
                                    "VehicleWarehouseBasicStyle",
                                    new InteriorPropModel
                                        {PropId = 252673, PropString = "basic_style_set", PropPrice = 10000f}
                                },
                                {
                                    "VehicleWarehouseUrbanStyle",
                                    new InteriorPropModel
                                        {PropId = 252673, PropString = "urban_style_set", PropPrice = 10000f}
                                },
                                {
                                    "VehicleWarehouseBrandedStyle",
                                    new InteriorPropModel
                                        {PropId = 252673, PropString = "branded_style_set", PropPrice = 10000f}
                                },
                                {
                                    "Doors",
                                    new InteriorPropModel
                                        {PropId = 252673, PropString = "door_blocker", PropPrice = 100000f}
                                },
                                {
                                    "CarFloor",
                                    new InteriorPropModel
                                        {PropId = 252673, PropString = "car_floor_hatch", PropPrice = 100000f}
                                }
                            }
                        }
                    },
                    {
                        "Nightclub",
                        new List<SortedDictionary<string, InteriorPropModel>>
                        {
                            new SortedDictionary<string, InteriorPropModel>
                            {
                                {
                                    "SecurityUpgrade",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "Int01_ba_security_upgrade", PropPrice = 10000f}
                                },
                                {
                                    "Equipment1",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "Int01_ba_equipment_setup", PropPrice = 10000f}
                                },
                                {
                                    "NightclubStyleOne",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "Int01_ba_Style01", PropPrice = 10000f}
                                },
                                {
                                    "NightclubStyleTwo",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "Int01_ba_Style02", PropPrice = 10000f}
                                },
                                {
                                    "NightclubStyleThree",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "Int01_ba_Style03", PropPrice = 10000f}
                                },
                                {
                                    "PodiumStyleOne",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "Int01_ba_style01_podium", PropPrice = 25000f}
                                },
                                {
                                    "PodiumStyleTwo",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "Int01_ba_style02_podium", PropPrice = 25000f}
                                },
                                {
                                    "PodiumStyleThree",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "Int01_ba_style03_podium", PropPrice = 10000f}
                                },
                                {
                                    "LightScreen",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "int01_ba_lights_screen", PropPrice = 10000f}
                                },
                                {
                                    "Screen",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "Int01_ba_Screen", PropPrice = 1000f}
                                },
                                {
                                    "GunRangeUpgrade",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "gun_range_blocker_set", PropPrice = 2500f}
                                },
                                {
                                    "Bar",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "Int01_ba_bar_content", PropPrice = 20000f}
                                },
                                {
                                    "BoozeOne",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "Int01_ba_booze_01", PropPrice = 10000f}
                                },
                                {
                                    "BoozeTwo",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "Int01_ba_booze_02", PropPrice = 10000f}
                                },
                                {
                                    "BoozeThree",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "Int01_ba_booze_03", PropPrice = 1000f}
                                },
                                {
                                    "DjOne",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "Int01_ba_dj01", PropPrice = 2500f}
                                },
                                {
                                    "DjTwo",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "Int01_ba_dj02", PropPrice = 20000f}
                                },
                                {
                                    "DjThree",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "Int01_ba_dj03", PropPrice = 5000f}
                                },
                                {
                                    "DjFour",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "Int01_ba_dj04", PropPrice = 5000f}
                                },
                                {
                                    "DjLightsOneStyleOne",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "DJ_01_Lights_01", PropPrice = 10000f}
                                },
                                {
                                    "DjLightsOneStyleTwo",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "DJ_01_Lights_02", PropPrice = 10000f}
                                },
                                {
                                    "DjLightsOneStyleThree",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "DJ_01_Lights_03", PropPrice = 10000f}
                                },
                                {
                                    "DjLightsOneStyleFour",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "DJ_01_Lights_04", PropPrice = 10000f}
                                },
                                {
                                    "DjLightsTwoStyleOne",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "DJ_02_Lights_01", PropPrice = 10000f}
                                },
                                {
                                    "DjLightsTwoStyleTwo",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "DJ_02_Lights_02", PropPrice = 10000f}
                                },
                                {
                                    "DjLightsTwoStyleThree",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "DJ_02_Lights_03", PropPrice = 10000f}
                                },
                                {
                                    "DjLightsTwoStyleFour",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "DJ_02_Lights_04", PropPrice = 10000f}
                                },
                                {
                                    "DjLightsThreeStyleOne",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "DJ_03_Lights_01", PropPrice = 10000f}
                                },
                                {
                                    "DjLightsThreeStyleTwo",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "DJ_03_Lights_02", PropPrice = 10000f}
                                },
                                {
                                    "DjLightsThreeStyleThree",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "DJ_03_Lights_03", PropPrice = 10000f}
                                },
                                {
                                    "DjLightsThreeStyleFour",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "DJ_03_Lights_04", PropPrice = 10000f}
                                },
                                {
                                    "DjLightsFourStyleOne",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "DJ_04_Lights_01", PropPrice = 10000f}
                                },
                                {
                                    "DjLightsFourStyleTwo",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "DJ_04_Lights_02", PropPrice = 10000f}
                                },
                                {
                                    "DjLightsFourStyleThree",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "DJ_04_Lights_03", PropPrice = 10000f}
                                },
                                {
                                    "DjLightsFourStyleFour",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "DJ_04_Lights_04", PropPrice = 10000f}
                                },
                                {
                                    "LightRigsOff",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "light_rigs_off", PropPrice = 10000f}
                                },
                                {
                                    "LightGrid",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "Int01_ba_lightgrid_01", PropPrice = 10000f}
                                },
                                {
                                    "Clutter",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "Int01_ba_Clutter", PropPrice = 10000f}
                                },
                                {
                                    "Equipment",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "Int01_ba_equipment_upgrade", PropPrice = 10000f}
                                },
                                {
                                    "ClubnameOne",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "Int01_ba_clubname_01", PropPrice = 10000f}
                                },
                                {
                                    "ClubnameTwo",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "Int01_ba_clubname_02", PropPrice = 10000f}
                                },
                                {
                                    "ClubnameThree",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "Int01_ba_clubname_03", PropPrice = 10000f}
                                },
                                {
                                    "ClubnameFour",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "Int01_ba_clubname_04", PropPrice = 10000f}
                                },
                                {
                                    "ClubnameFive",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "Int01_ba_clubname_05", PropPrice = 10000f}
                                },
                                {
                                    "ClubnameSix",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "Int01_ba_clubname_06", PropPrice = 10000f}
                                },
                                {
                                    "ClubnameSeven",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "Int01_ba_clubname_07", PropPrice = 10000f}
                                },
                                {
                                    "ClubnameEight",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "Int01_ba_clubname_08", PropPrice = 10000f}
                                },
                                {
                                    "ClubnameNine",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "Int01_ba_clubname_09", PropPrice = 10000f}
                                },
                                {
                                    "DryIce",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "Int01_ba_dry_ice", PropPrice = 10000f}
                                },
                                {
                                    "DeliveryTruck",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "Int01_ba_deliverytruck", PropPrice = 10000f}
                                },
                                {
                                    "TrophyOne",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "Int01_ba_trophy01", PropPrice = 10000f}
                                },
                                {
                                    "TrophyTwo",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "Int01_ba_trophy02", PropPrice = 10000f}
                                },
                                {
                                    "TrophyThree",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "Int01_ba_trophy03", PropPrice = 10000f}
                                },
                                {
                                    "TrophyFour",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "Int01_ba_trophy04", PropPrice = 10000f}
                                },
                                {
                                    "TrophyFive",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "Int01_ba_trophy05", PropPrice = 10000f}
                                },
                                {
                                    "TrophySeven",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "Int01_ba_trophy07", PropPrice = 10000f}
                                },
                                {
                                    "TrophyEight",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "Int01_ba_trophy08", PropPrice = 10000f}
                                },
                                {
                                    "TrophyNine",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "Int01_ba_trophy09", PropPrice = 10000f}
                                },
                                {
                                    "TrophyTen",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "Int01_ba_trophy10", PropPrice = 10000f}
                                },
                                {
                                    "TrophyEleven",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "Int01_ba_trophy11", PropPrice = 10000f}
                                },
                                {
                                    "TradLights",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "Int01_ba_trad_lights", PropPrice = 10000f}
                                },
                                {
                                    "WorkLamps",
                                    new InteriorPropModel
                                        {PropId = 271617, PropString = "Int01_ba_Worklamps", PropPrice = 10000f}
                                }
                            }
                        }
                    }
                };
    }
}