﻿using System.Collections.Generic;
using RolePlayGameData.Enums;

namespace RolePlayGameData.Configs
{
    public class TraderCfg
    {
        public static readonly Dictionary<uint, List<uint>> TraderItems = new Dictionary<uint, List<uint>>
        {
            {
                (uint) MarkTypeEnums.Trader_Exporter, new List<uint>
                {
                    (uint) ItemNameEnums.AlmondsSack,
                    (uint) ItemNameEnums.AppleJuice,
                    (uint) ItemNameEnums.SilverCutlery,
                    (uint) ItemNameEnums.WalnutsSack
                }
            },
            {
                (uint) MarkTypeEnums.Trader_FurnitureShop, new List<uint>
                {
                    (uint) ItemNameEnums.Chair,
                    (uint) ItemNameEnums.Shelf,
                    (uint) ItemNameEnums.Table
                }
            },
            {
                (uint) MarkTypeEnums.Trader_BuilderMerchants, new List<uint>
                {
                    (uint) ItemNameEnums.Cement,
                    (uint) ItemNameEnums.CopperWire,
                    (uint) ItemNameEnums.IronPipe,
                    (uint) ItemNameEnums.IronPlate,
                    (uint) ItemNameEnums.Pane
                }
            },
            {
                (uint) MarkTypeEnums.Trader_Bakery, new List<uint>
                {
                    (uint) ItemNameEnums.ContainerButter,
                    (uint) ItemNameEnums.ContainerCream,
                    (uint) ItemNameEnums.FlourSack,
                    (uint) ItemNameEnums.Milk
                }
            },
            {
                (uint) MarkTypeEnums.Trader_BigKitchen, new List<uint>
                {
                    (uint) ItemNameEnums.CattleMeat,
                    (uint) ItemNameEnums.ChickenMeat,
                    (uint) ItemNameEnums.Cod,
                    (uint) ItemNameEnums.Frog,
                    (uint) ItemNameEnums.Mackerel,
                    (uint) ItemNameEnums.Perch,
                    (uint) ItemNameEnums.Pork,
                    (uint) ItemNameEnums.Shell
                }
            },
            {
                (uint) MarkTypeEnums.Trader_Jeweler, new List<uint>
                {
                    (uint) ItemNameEnums.DiamondRing,
                    (uint) ItemNameEnums.GoldChain,
                    (uint) ItemNameEnums.GoldCoins,
                    (uint) ItemNameEnums.RoughDiamond
                }
            },
            {
                (uint) MarkTypeEnums.Trader_Fruiterer, new List<uint>
                {
                    (uint) ItemNameEnums.AppleCrate,
                    (uint) ItemNameEnums.AvocadoCrate,
                    (uint) ItemNameEnums.LemonSack,
                    (uint) ItemNameEnums.OrangeSack,
                    (uint) ItemNameEnums.TomatoCrate
                }
            },
            {
                (uint) MarkTypeEnums.Trader_Dealer, new List<uint>
                {
                    (uint) ItemNameEnums.AmnesiaHazeJoint,
                    (uint) ItemNameEnums.Cocaine,
                    (uint) ItemNameEnums.CriticalJoint,
                    (uint) ItemNameEnums.CrystalMeth,
                    (uint) ItemNameEnums.Ecstasy,
                    (uint) ItemNameEnums.HollandsHopeJoint,
                    (uint) ItemNameEnums.Lsd,
                    (uint) ItemNameEnums.OGKuschJoint,
                    (uint) ItemNameEnums.SpecialQueenJoint
                }
            }
        };
    }
}