﻿using System;
using System.Collections.Generic;
using Entity = RAGE.Game.Entity;
using Events = RAGE.Events;
using Input = RAGE.Input;
using Player = RAGE.Elements.Player;
using Task = System.Threading.Tasks.Task;

namespace RolePlayGameClientScript.cs_packages
{
    internal class KeyBinds : Events.Script
    {
        private const int ResetTime = 1000;
        private static bool _keyStatus = true;

        public KeyBinds() => Events.Tick += KeyTick;

        private static void KeyTick(List<Events.TickNametagData> nameTags)
        {
            if (!WindowMng.IsMainUiActive) return;

            ProtectedKeyDown();
            KeyDown();

            if (!ClientEvents.ShowSpeedometer) return;

            VehicleKeyDown();
        }

        private static void ProtectedKeyDown()
        {
            KeyBind((int)ConsoleKey.NumPad0, () => { Events.CallRemote("showMapMarkEditor"); });
            KeyBind((int)ConsoleKey.NumPad1, () => { Events.CallRemote("showItemSpawner"); });
            KeyBind((int)ConsoleKey.NumPad2, () => { Events.CallRemote("showClothingSpawner"); });
            KeyBind((int)ConsoleKey.NumPad3, () => { Events.CallRemote("showLocationTeleporter"); });
            KeyBind((int)ConsoleKey.NumPad9, () => { Events.CallRemote("createDumpster"); });
        }

        private static void KeyDown()
        {
            KeyBind((int)ConsoleKey.L, () => { Events.CallRemote("lockUnlockDoors"); });
            KeyBind((int)ConsoleKey.K, () => { Events.CallRemote("openCloseTrunk"); });
            KeyBind((int)ConsoleKey.E, () => { Events.CallRemote("interactWithLocation"); });
            KeyBind((int)ConsoleKey.I, () => { Events.CallRemote("showInventory"); });
            KeyBind((int)ConsoleKey.F9, () => { Events.CallRemote("changeVoiceRange"); });
        }

        private static void VehicleKeyDown()
        {
            KeyBind((int)ConsoleKey.UpArrow, Attach);
            KeyBind((int)ConsoleKey.M, () => { Events.CallRemote("startStopEngine"); });
            KeyBind((int)ConsoleKey.T, () => { Events.CallRemote("turnOnOffSpecial"); });
        }

        private static void Attach()
        {
            Entity.AttachEntityToEntity(Player.LocalPlayer.Handle, Player.LocalPlayer.Vehicle.Handle,
                0, 0, 0, 0, 0, 0, 0, true, false, false, false, 0, false);
        }

        private static void KeyBind(int key, Action action)
        {
            if (!Input.IsDown(key)) return;
            if (!_keyStatus) return;

            _keyStatus = false;

            action.Invoke();            

            Task.Delay(ResetTime).ContinueWith(_ => _keyStatus = true);
        }
    }
}