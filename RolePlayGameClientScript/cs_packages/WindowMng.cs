﻿using System;
using System.Linq;
using Cursor = RAGE.Ui.Cursor;
using Events = RAGE.Events;
using HtmlWindow = RAGE.Ui.HtmlWindow;
using Player = RAGE.Elements.Player;
using Task = System.Threading.Tasks.Task;
using Ui = RAGE.Game.Ui;

namespace RolePlayGameClientScript.cs_packages
{
    internal class WindowMng : Events.Script
    {
        public static bool IsMainUiActive { get; private set; }
        public static bool IsCustomActive { get; private set; }

        private static HtmlWindow _mainUiWindow;
        private static HtmlWindow _customWindow;
        private static bool _isCreating;

        private static Player LocalPlayer => Player.LocalPlayer;

        public WindowMng()
        {
            Events.Add("createMainUiWnd", CreateMainUiWnd);
            Events.Add("createCustomWnd", CreateCustomWnd);
            Events.Add("destroyCustomWnd", DestroyCustomWnd);
            Events.Add("executeMainUiWndJs", ExecuteMainUiWndJs);
            Events.Add("executeCustomWndJs", ExecuteCustomWndJs);
        }

        private static async void CreateMainUiWnd(object[] args)
        {
            _mainUiWindow = await Task.Run(() => new HtmlWindow(args[0].ToString())).ConfigureAwait(false);

            IsMainUiActive = true;
        }

        private static async void CreateCustomWnd(object[] args)
        {
            if (ClientEvents.ProgressBarActive) return;
            if (_isCreating) return;

            _isCreating = true;

            if (IsCustomActive)
            {
                Events.CallLocal("destroyCustomWnd");
                await Task.Delay(500).ConfigureAwait(false);
            }

            var freeze = Convert.ToBoolean(args[1]);
            var hud = Convert.ToBoolean(args[2]);
            var cursor = Convert.ToBoolean(args[3]);

            LocalPlayer.FreezePosition(freeze);

            if (!ClientEvents.ShowSpeedometer) Ui.DisplayRadar(hud);

            Cursor.Visible = cursor;

            _customWindow = await Task.Run(() => new HtmlWindow(args[0].ToString())).ConfigureAwait(false);

            IsCustomActive = true;
            _isCreating = false;
        }

        private static void DestroyCustomWnd(object[] args)
        {
            LocalPlayer.FreezePosition(false);

            Cursor.Visible = false;

            _customWindow.Destroy();
            _customWindow = null;

            IsCustomActive = false;
        }

        public static void ExecuteMainUiWndJs(object[] args)
        {
            var input = string.Empty;
            var function = args[0].ToString();
            var arguments = args.Skip(1).ToArray();

            input = arguments.Aggregate(input, (current, arg) =>
            current + (current.Length > 0 ? ", '" + arg + "'" : "'" + arg + "'"));
            _mainUiWindow.ExecuteJs(function + "(" + input + ");");
        }

        public static void ExecuteCustomWndJs(object[] args)
        {
            var input = string.Empty;
            var function = args[0].ToString();
            var arguments = args.Skip(1).ToArray();

            input = arguments.Aggregate(input, (current, arg) =>
            current + (current.Length > 0 ? ", '" + arg + "'" : "'" + arg + "'"));
            _customWindow.ExecuteJs(function + "(" + input + ");");
        }
    }
}