﻿using Newtonsoft.Json;
using RolePlayGameClientScript.cs_packages.Models;
using System;
using System.Collections.Generic;
using Blip = RAGE.Elements.Blip;
using Cam = RAGE.Game.Cam;
using Discord = RAGE.Discord;
using Events = RAGE.Events;
using Interior = RAGE.Game.Interior;
using Misc = RAGE.Game.Misc;
using Nametags = RAGE.Nametags;
using Player = RAGE.Elements.Player;
using Task = System.Threading.Tasks.Task;
using Ui = RAGE.Game.Ui;
using Vehicle = RAGE.Elements.Vehicle;
using Chat = RAGE.Chat;

namespace RolePlayGameClientScript.cs_packages
{
    internal class ClientEvents : Events.Script
    {
        public static Player LocalPlayer { get; private set; } = Player.LocalPlayer;
        public static int VehicleSeat { get; private set; }
        public static bool IsInVehicle { get; set; }
        public static bool ShowSpeedometer { get; set; }
        public static bool ProgressBarActive { get; private set; }

        private static int _customCam = Cam.CreateCamera(Misc.GetHashKey("DEFAULT_SCRIPTED_CAMERA"), true);
        private static uint _lastweather = default;

        public ClientEvents()
        {
            // ui //
            Events.Add("showHideLowHunger", ShowHideLowHunger);
            Events.Add("showHideLowThirst", ShowHideLowThirst);
            Events.Add("showHideLowHealth", ShowHideLowHealth);
            Events.Add("showHideLowArmor", ShowHideLowArmor);
            Events.Add("showHideDmgHud", ShowHideDmgHud);
            Events.Add("changeVoiceIcon", ChangeVoiceIcon);
            Events.Add("showHitMarker", ShowHitMarker);
            Events.Add("showHideSackHud", ShowHideSackHud);
            Events.Add("updateProgressBar", UpdateProgressBar);
            Events.Add("setCivillianBlips", SetCivillianBlips);
            Events.Add("showLeftMessage", ShowLeftMessage);
            Events.Add("showRightMessage", ShowRightMessage);

            // creator //
            Events.Add("setCreatorCamera", SetCreatorCamera);
            Events.Add("changeCameraFocus", ChangeCameraFocus);
            Events.Add("rotateCharacter", RotateCharacter);

            // customizing //
            Events.Add("setHeadOverlay", SetHeadOverlay);
            Events.Add("setHeadOverlayColor", SetHeadOverlayColor);
            Events.Add("setFaceFeature", SetFaceFeature);
            Events.Add("setEyeColor", SetEyeColor);
            Events.Add("setHairColor", SetHairColor);
            Events.Add("setComponentVariation", SetComponentVariation);
            Events.Add("setHeadBlendData", SetHeadBlendData);

            // shop //
            Events.Add("setClothingShopCam", SetClothingShopCam);

            // worldAndMap //
            Events.Add("changeWeather", ChangeWeather);
            Events.Add("removeProp", RemoveProp);
            Events.Add("addProp", AddProp);

            // clothingShop //
            Events.Add("setPlayerClothes", SetPlayerClothes);

            // events //
            Events.OnPlayerEnterVehicle += OnPlayerEnterVehicle;
            Events.OnPlayerLeaveVehicle += OnPlayerLeaveVehicle;
            Events.OnPlayerSpawn += OnPlayerSpawn;
            Events.OnPlayerDeath += OnPlayerDeath;

            // settings //
            Nametags.Enabled = false;
            Chat.Activate(false);
            Chat.Show(false);
        }

        #region Ui

        public void ShowHideLowHunger(object[] args) => WindowMng.ExecuteMainUiWndJs(
            new object[] { "showHideLowHunger", args[0].ToString() });

        public void ShowHideLowThirst(object[] args) => WindowMng.ExecuteMainUiWndJs(
            new object[] { "showHideLowThirst", args[0].ToString() });

        public void ShowHideLowHealth(object[] args) => WindowMng.ExecuteMainUiWndJs(
            new object[] { "showHideLowHealth", args[0].ToString() });

        public void ShowHideLowArmor(object[] args) => WindowMng.ExecuteMainUiWndJs(
            new object[] { "showHideLowArmor", args[0].ToString() });

        public void ShowHideDmgHud(object[] args) => WindowMng.ExecuteMainUiWndJs(
            new object[] { "showHideDmgHud", args[0].ToString() });

        public void ChangeVoiceIcon(object[] args) => WindowMng.ExecuteMainUiWndJs(
            new object[] { "changeVoiceIcon", args[0].ToString() });

        public void ShowHitMarker(object[] args) => WindowMng.ExecuteMainUiWndJs(
            new object[] { "showHitMarker" });

        private static async void UpdateProgressBar(object[] args)
        {
            ProgressBarActive = true;

            var name = args[0].ToString();
            var step = Convert.ToInt32(args[1]) / 50;

            WindowMng.ExecuteMainUiWndJs(new object[] { "updateProgressBar", name, 0 });
            await Task.Delay(500).ConfigureAwait(false);
            WindowMng.ExecuteMainUiWndJs(new object[] { "showProgressBar" });

            for (var i = 0; i <= 50; i++)
            {
                WindowMng.ExecuteMainUiWndJs(new object[] { "updateProgressBar", name, i * 2 });
                await Task.Delay(step).ConfigureAwait(false);
            }

            await Task.Delay(500).ConfigureAwait(false);
            WindowMng.ExecuteMainUiWndJs(new object[] { "hideProgressBar" });

            ProgressBarActive = false;
        }

        public static void ShowHideSackHud(object[] args) => WindowMng.ExecuteMainUiWndJs(
            new object[] { "showHideSackHud", args[0].ToString() });

        private static void ShowLeftMessage(object[] args) => WindowMng.ExecuteMainUiWndJs(
            new object[] { "showLeftMessage", args[0].ToString(), args[1].ToString() });

        private static void ShowRightMessage(object[] args) => WindowMng.ExecuteMainUiWndJs(
           new object[] { "showRightMessage", args[0].ToString(), args[1].ToString(), args[2].ToString() });

        #endregion

        #region Creator

        private static void SetCreatorCamera(object[] args)
        {
            _customCam = Cam.CreateCamera(Misc.GetHashKey("DEFAULT_SCRIPTED_CAMERA"), true);

            if (Convert.ToBoolean(args[0]))
            {
                var playerPos = LocalPlayer.Position;
                var forwardX = playerPos.X + LocalPlayer.GetForwardX() * 2.5f;
                var forwardY = playerPos.Y + LocalPlayer.GetForwardY() * 2.5f;

                Cam.SetCamCoord(_customCam, forwardX, forwardY, playerPos.Z + 0.5f);
                Cam.PointCamAtCoord(_customCam, 152.3787f, -1000.644f, -99f);

                Cam.SetCamActive(_customCam, true);
                Cam.RenderScriptCams(true, false, 0, true, false, 0);

                return;
            }

            Cam.DestroyCam(_customCam, true);
            Cam.RenderScriptCams(false, false, 0, true, false, 0);
        }

        private static void ChangeCameraFocus(object[] args)
        {
            var playerPos = LocalPlayer.Position;
            var forwardX = playerPos.X + LocalPlayer.GetForwardX() * 2f;
            var forwardY = playerPos.Y + LocalPlayer.GetForwardY() * 2f;

            switch (args[0].ToString())
            {
                case "1":
                    forwardX = playerPos.X + LocalPlayer.GetForwardX() * 1.5f;
                    forwardY = playerPos.Y + LocalPlayer.GetForwardY() * 1.5f;
                    Cam.SetCamCoord(_customCam, forwardX, forwardY, playerPos.Z + 0.75f);
                    Cam.PointCamAtCoord(_customCam, 152.3787f, -1000.644f, -98.75f);
                    break;
                case "2":
                    forwardX = playerPos.X + LocalPlayer.GetForwardX() * .75f;
                    forwardY = playerPos.Y + LocalPlayer.GetForwardY() * .75f;
                    Cam.SetCamCoord(_customCam, forwardX, forwardY, playerPos.Z + 0.75f);
                    Cam.PointCamAtCoord(_customCam, 152.3787f, -1000.644f, -98.25f);
                    break;
                default:
                    Cam.SetCamCoord(_customCam, forwardX, forwardY, playerPos.Z + 0.5f);
                    Cam.PointCamAtCoord(_customCam, 152.3787f, -1000.644f, -99f);
                    break;
            }
        }

        private static void RotateCharacter(object[] args) => LocalPlayer.SetHeading(Convert.ToInt32(args[0]));

        private void SetClothingShopCam(object[] args)
        {
            _customCam = Cam.CreateCamera(Misc.GetHashKey("DEFAULT_SCRIPTED_CAMERA"), true);

            if (Convert.ToBoolean(args[0]))
            {
                var playerPos = LocalPlayer.Position;
                var forwardX = playerPos.X + LocalPlayer.GetForwardX() * 2.5f;
                var forwardY = playerPos.Y + LocalPlayer.GetForwardY() * 2.5f;

                Cam.SetCamCoord(_customCam, forwardX, forwardY, playerPos.Z + 0.5f);
                Cam.PointCamAtCoord(_customCam, playerPos.X, playerPos.Y, playerPos.Z);
                Cam.SetCamActive(_customCam, true);
                Cam.RenderScriptCams(true, false, 0, true, false, 0);

                return;
            }

            Cam.DestroyCam(_customCam, true);
            Cam.RenderScriptCams(false, false, 0, true, false, 0);
        }

        #endregion

        #region customizing

        private static void SetHeadOverlay(object[] args) =>
            LocalPlayer.SetHeadOverlay(Convert.ToInt32(args[0]), Convert.ToInt32(args[1]), 1.0f);

        private static void SetHeadOverlayColor(object[] args) =>
            LocalPlayer.SetHeadOverlayColor(Convert.ToInt32(args[0]), Convert.ToInt32(args[1]), Convert.ToInt32(args[2]), 0);

        private static void SetFaceFeature(object[] args) => 
            LocalPlayer.SetFaceFeature(Convert.ToInt32(args[0]), float.Parse(args[1].ToString()));

        private static void SetEyeColor(object[] args) => 
            LocalPlayer.SetEyeColor(Convert.ToInt32(args[0]));

        private static void SetHairColor(object[] args) => 
            LocalPlayer.SetHairColor(Convert.ToInt32(args[0]), Convert.ToInt32(args[1]));

        private static void SetComponentVariation(object[] args) => 
            LocalPlayer.SetComponentVariation(Convert.ToInt32(args[0]), Convert.ToInt32(args[1]), Convert.ToInt32(args[2]), 0);

        private static void SetHeadBlendData(object[] args) =>
            LocalPlayer.SetHeadBlendData(Convert.ToInt32(args[0]), Convert.ToInt32(args[1]), 0, Convert.ToInt32(args[2]),
                Convert.ToInt32(args[3]), 0, float.Parse(args[4].ToString()), float.Parse(args[5].ToString()), 0, false);

        #endregion

        #region worldAndMap

        public static void AddProp(object[] args)
        {
            var playerPos = LocalPlayer.Position;
            var interiorId = Interior.GetInteriorAtCoords(playerPos.X, playerPos.Y, playerPos.Z);

            Interior.EnableInteriorProp(interiorId, args[0].ToString());
            Interior.RefreshInterior(interiorId);
        }

        public static void RemoveProp(object[] args)
        {
            var playerPos = LocalPlayer.Position;
            var interiorId = Interior.GetInteriorAtCoords(playerPos.X, playerPos.Y, playerPos.Z);

            Interior.DisableInteriorProp(interiorId, args[0].ToString());
            Interior.RefreshInterior(interiorId);
        }

        private void SetCivillianBlips(object[] args)
        {
            var blipList = JsonConvert.DeserializeObject<List<BlipModel>>(args[0].ToString());

            foreach (var item in blipList)
                new Blip(item.Sprite, item.Position, item.Name, item.Scale, item.Color, item.Alpha,
                    item.DrawDistance, item.ShortRange, item.Rotation, item.Dimension);
        }

        private static void ChangeWeather(object[] args)
        {
            var weather = Misc.GetHashKey(args[0].ToString());

            Task.Run(async () =>
            {
                if (weather == default) _lastweather = weather;

                for (var i = 0f; i < 1.01f; i += .01f)
                {
                    Misc.SetWeatherTypeTransition(_lastweather, weather, i);

                    await Task.Delay(500);
                }

                _lastweather = weather;

            }).ConfigureAwait(false);
        }

        #endregion

        #region ClothesShop

        private static void SetPlayerClothes(object[] args)
        {
            if (Convert.ToBoolean(args[0])) LocalPlayer.SetComponentVariation(Convert.ToInt32(args[1]), Convert.ToInt32(args[2]), Convert.ToInt32(args[3]), 0);
            else LocalPlayer.SetPropIndex(Convert.ToInt32(args[1]), Convert.ToInt32(args[2]), Convert.ToInt32(args[3]), true);
        }

        #endregion

        #region Events

        private static async void OnPlayerEnterVehicle(Vehicle vehicle, int seatId)
        {
            if (!WindowMng.IsMainUiActive) return;
            if (vehicle.GetDoorLockStatus() == 2) return;

            await Task.Delay(7500).ConfigureAwait(false);

            VehicleSeat = seatId;

            if (LocalPlayer.Vehicle == null) return;

            IsInVehicle = true;

            if (seatId != 0) return;

            WindowMng.ExecuteMainUiWndJs(new object[] { "showSpeedometer" });
            Ui.DisplayRadar(true);
            ShowSpeedometer = true;
        }

        private static void OnPlayerLeaveVehicle()
        {
            ShowSpeedometer = false;
            IsInVehicle = false;

            Ui.DisplayRadar(false);

            if (WindowMng.IsMainUiActive) WindowMng.ExecuteMainUiWndJs(new object[] { "hideSpeedometer", null, null });
        }

        private static void OnPlayerSpawn(Events.CancelEventArgs cancel) =>
            Discord.Update("auf TheGoldenState", "the-golden-state.de");

        private static void OnPlayerDeath(Player player, uint reason, Player killer, Events.CancelEventArgs cancel)
        {
            ShowSpeedometer = false;
            IsInVehicle = false;

            Ui.DisplayRadar(false);

            if (WindowMng.IsMainUiActive) WindowMng.ExecuteMainUiWndJs(new object[] { "hideSpeedometer", null, null });
        }

        #endregion
    }
}