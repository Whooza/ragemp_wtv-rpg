﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Audio = RAGE.Game.Audio;
using Events = RAGE.Events;

namespace RolePlayGameClientScript.cs_packages
{
    internal class GlobalTick : Events.Script
    {
        private static bool _vehUpdate;
        private static bool _fuelUpdate;
        private static bool _kmUpdate;

        private static float _speed = 0f;
        private static double _fuel = 0.0;
        private static double _tank = 0.0;
        private static double _cons = 0.0;

        public GlobalTick() => Events.Tick = Tick;

        private static void Tick(List<Events.TickNametagData> nameTags)
        {
            if (!WindowMng.IsMainUiActive) return;

            if(ClientEvents.LocalPlayer.Vehicle == null)
            {
                ClientEvents.IsInVehicle = false;
                ClientEvents.ShowSpeedometer = false;
                WindowMng.ExecuteMainUiWndJs(new object[] { "hideSpeedometer", null, null });
                return;
            }

            if (!_vehUpdate)
            {
                _vehUpdate = true;
                Task.Run(SyncVehicle).ConfigureAwait(false);
            }

            if (!ClientEvents.ShowSpeedometer) return;

            if (!_fuelUpdate)
            {
                _fuelUpdate = true;
                Task.Run(UpdateFuel).ConfigureAwait(false);
            }

            if (!_kmUpdate)
            {
                _kmUpdate = true;
                Task.Run(UpdateKm).ConfigureAwait(false);
            }

            UpdateSpeedometer();
        }

        private static void SyncVehicle()
        {
            var playerRadio = Audio.GetPlayerRadioStationName();
            var vehicleRadio = ClientEvents.LocalPlayer.Vehicle.GetSharedData("RADIO_STATION").ToString();

            if (playerRadio == vehicleRadio) return;

            if (ClientEvents.VehicleSeat == 0) Events.CallRemote("syncRadio", playerRadio);
            else Audio.SetRadioToStationName(vehicleRadio);

            if (string.IsNullOrEmpty(vehicleRadio) || vehicleRadio == " ") Audio.SetRadioToStationIndex(255);

            Task.Delay(1000).ContinueWith(_ => _vehUpdate = false);
        }

        private static async void UpdateFuel()
        {
            var newFuel = Convert.ToDouble(ClientEvents.LocalPlayer.Vehicle.GetSharedData("VEH_FUEL_SHARED"));
            _tank = Convert.ToDouble(ClientEvents.LocalPlayer.Vehicle.GetSharedData("VEH_TANK_CAPACITY"));
            _cons = Convert.ToDouble(ClientEvents.LocalPlayer.Vehicle.GetSharedData("VEH_FUEL_CONSUMPTION"));

            if (newFuel < _fuel)
            {
                var tick = (_fuel - newFuel) / 20;

                for (int i = 0; i < 20; i++)
                {
                    _fuel -= tick;

                    WindowMng.ExecuteMainUiWndJs(new object[] { "updateFuel", _fuel * 100 / _tank });

                    await Task.Delay(1000);
                }
            }
            else
            {
                _fuel = newFuel;

                WindowMng.ExecuteMainUiWndJs(new object[] { "updateFuel", _fuel * 100 / _tank });

                await Task.Delay(20000);
            }

            _fuelUpdate = false;
        }

        private static async void UpdateKm()
        {
            var km = Math.Round(_fuel / _cons * 100 * (1 - (_speed / 1000)));

            WindowMng.ExecuteMainUiWndJs(new object[] { "updateKm", km + " km" });

            await Task.Delay(250);

            _kmUpdate = false;
        }

        private static void UpdateSpeedometer()
        {
            _speed = ClientEvents.LocalPlayer.Vehicle.GetSpeed() * 3.6f;

            WindowMng.ExecuteMainUiWndJs(new object[] { "updateSpeedometer", _speed });
        }
    }
}