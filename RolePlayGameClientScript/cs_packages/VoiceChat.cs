﻿using System;
using System.Collections.Generic;
using Entities = RAGE.Elements.Entities;
using Events = RAGE.Events;
using HtmlWindow = RAGE.Ui.HtmlWindow;
using Misc = RAGE.Game.Misc;
using Player = RAGE.Elements.Player;

namespace RolePlayGameClientScript.cs_packages
{
    internal class VoiceChat : Events.Script
    {
        private static HtmlWindow _tsBrowser;
        private static string _conString;
        private static int _lastCheck;
        private static string _tsName;

        public VoiceChat()
        {
            Events.Add("connectTs", ConnectTs);
            Events.Add("disconnectTs", DisconnectTs);
            Events.Add("tsLipSync", TsLipSync);
        }

        #region Events

        private static void ConnectTs(object[] args)
        {
            _conString = args[0].ToString();
            _tsName = args[1].ToString();

            Events.Tick += VoiceTick;
        }

        private static void DisconnectTs(object[] args)
        {
            _tsBrowser.Destroy();
            _tsBrowser = null;
        }

        private static void TsLipSync(object[] args)
        {
            var player = Entities.Players.GetAtRemote(Convert.ToUInt16(args[0]));

            if (player == null) return;

            var speak = Convert.ToBoolean(args[1].ToString());

            if (speak) player.PlayFacialAnim("mic_chatter", "mp_facial");
            else player.PlayFacialAnim("mood_normal_1", "facials@gen_male@variations@normal");
        }

        private static void VoiceTick(List<Events.TickNametagData> nametags)
        {
            if (Misc.GetGameTimer() - _lastCheck < 500) return;

            var player = Player.LocalPlayer;
            var rotation = Math.PI / 180 * (player.GetRotation(0).Z * -1);
            var streamedPlayers = Entities.Players.All;
            var playerNames = new List<string>();

            if (player.GetSharedData("CALLING_PLAYER_NAME") != null &&
                player.GetSharedData("CALL_IS_STARTED") != null &&
                (bool)player.GetSharedData("CALL_IS_STARTED"))
            {
                var callingPlayerName = player.GetSharedData("CALLING_PLAYER_NAME").ToString();

                if (callingPlayerName != "") playerNames.Add(callingPlayerName + "~10~0~0~3");
            }

            foreach (var t in streamedPlayers)
            {
                if (t != null && !t.Exists) continue;

                if (t.GetSharedData("TsName") == null) continue;

                var streamedPlayerPos = t.Position;
                var distance = player.Position.DistanceTo2D(streamedPlayerPos);

                var voiceRange = "Normal";

                if (t.GetSharedData("VOICE_RANGE") != null) voiceRange = t.GetSharedData("VOICE_RANGE").ToString();

                float volumeModifier = 0;
                var range = 12;

                if (voiceRange == "Weit") range = 50;
                else if (voiceRange == "Kurz") range = 5;

                if (distance > 5) volumeModifier = distance * -8 / 10;

                if (volumeModifier > 0) volumeModifier = 0;

                if (!(distance < range)) continue;

                var subPos = streamedPlayerPos.Subtract(player.Position);

                var x = subPos.X * Math.Cos(rotation) - subPos.Y * Math.Sin(rotation);
                var y = subPos.X * Math.Sin(rotation) + subPos.Y * Math.Cos(rotation);

                x = x * 10 / range;
                y = y * 10 / range;

                playerNames.Add(t.GetSharedData("TsName") + "~" + Math.Round(x * 1000) / 1000 + "~" +
                                Math.Round(y * 1000) / 1000 + "~0~" + Math.Round(volumeModifier * 1000) / 1000);
            }

            if (_tsBrowser == null) _tsBrowser =
                    new HtmlWindow($"{_conString}/{_tsName}/{string.Join(";", playerNames)}/");
            else _tsBrowser.Url = $"{_conString}/{_tsName}/{string.Join(";", playerNames)}/";

            playerNames.Clear();
            _lastCheck = Misc.GetGameTimer();
        }

        #endregion
    }
}