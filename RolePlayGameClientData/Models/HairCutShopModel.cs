﻿namespace RolePlayGameClientData.Models
{
    public class HairCutShopModel
    {
        public int HairCut { get; set; }
        public int HairColorOne { get; set; }
        public int HairColorTwo { get; set; }
        public int Beard { get; set; }
        public int BeardColor { get; set; }
        public int EyeBrown { get; set; }
        public int EyeBrownColor { get; set; }
        public int Makeup { get; set; }
        public int Lipstick { get; set; }
        public int LipstickColor { get; set; }
    }
}
