﻿namespace RolePlayGameClientData.Models
{
    public class CartItemModel
    {
        public uint ItemName { get; set; }
        public string DisplayedName { get; set; }
        public uint Amount { get; set; }
        public float Price { get; set; }
    }
}