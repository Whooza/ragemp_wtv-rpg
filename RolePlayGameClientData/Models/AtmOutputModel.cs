﻿namespace RolePlayGameClientData.Models
{
    public class AtmOutputModel
    {
        public AtmOutputModel()
        {
            Line1Col1 = " ";
            Line1Col2 = "Willkommen,";
            Line1Col3 = " ";

            Line2Col1 = " ";
            Line2Col2 = " ";
            Line2Col3 = " ";

            Line3Col1 = " ";
            Line3Col2 = "bitte wählen Sie eine Bank...";
            Line3Col3 = " ";

            Line4Col1 = " ";
            Line4Col2 = " ";
            Line4Col3 = " ";
        }

        public AtmOutputModel ShowPinCode(string bankName, string pinCode)
        {
            var pinOutput = "";

            for (var i = 0; i < pinCode.Length; i++) pinOutput += "*";

            return new AtmOutputModel
            {
                Line1Col1 = " ",
                Line1Col2 = $"{bankName} Atm Service",
                Line1Col3 = " ",

                Line2Col1 = " ",
                Line2Col2 = "Pin eingeben",
                Line2Col3 = " ",

                Line3Col1 = " ",
                Line3Col2 = pinOutput,
                Line3Col3 = " ",

                Line4Col1 = " ",
                Line4Col2 = "- - - -",
                Line4Col3 = " "
            };
        }

        public AtmOutputModel ShowInProgress()
        {
            return new AtmOutputModel
            {
                Line1Col1 = " ",
                Line1Col2 = " ",
                Line1Col3 = " ",

                Line2Col1 = " ",
                Line2Col2 = " ",
                Line2Col3 = " ",

                Line3Col1 = " ",
                Line3Col2 = " ",
                Line3Col3 = " ",

                Line4Col1 = " ",
                Line4Col2 = "Voragng in Bearbeitung, bitte warten...",
                Line4Col3 = " "
            };
        }

        public AtmOutputModel ShowBankBalance(string bankName, string amount)
        {
            return new AtmOutputModel
            {
                Line1Col1 = " ",
                Line1Col2 = $"{bankName} Atm Service",
                Line1Col3 = " ",

                Line2Col1 = " ",
                Line2Col2 = "----------",
                Line2Col3 = " ",

                Line3Col1 = "Kontostand:",
                Line3Col2 = amount,
                Line3Col3 = "Dollar",

                Line4Col1 = " ",
                Line4Col2 = "----------",
                Line4Col3 = " "
            };
        }

        public AtmOutputModel ShowByeScreen()
        {
            return new AtmOutputModel
            {
                Line1Col1 = " ",
                Line1Col2 = "Auf wiedersehen!",
                Line1Col3 = " ",

                Line2Col1 = " ",
                Line2Col2 = " ",
                Line2Col3 = " ",

                Line3Col1 = " ",
                Line3Col2 = "Wir wünschen einen schönen Tag.",
                Line3Col3 = " ",

                Line4Col1 = " ",
                Line4Col2 = " ",
                Line4Col3 = " "
            };
        }

        #region Properties

        public string Line1Col1 { get; private set; }
        public string Line1Col2 { get; private set; }
        public string Line1Col3 { get; private set; }
        public string Line2Col1 { get; private set; }
        public string Line2Col2 { get; private set; }
        public string Line2Col3 { get; private set; }
        public string Line3Col1 { get; private set; }
        public string Line3Col2 { get; private set; }
        public string Line3Col3 { get; private set; }
        public string Line4Col1 { get; private set; }
        public string Line4Col2 { get; private set; }
        public string Line4Col3 { get; private set; }

        #endregion
    }
}