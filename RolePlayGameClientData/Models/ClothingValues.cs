﻿namespace RolePlayGameClientData.Models
{
    public class ClothingValues
    {
        public int Slot { get; set; }
        public int Drawable { get; set; }
        public int Texture { get; set; }
    }
}