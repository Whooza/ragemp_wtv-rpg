﻿namespace RolePlayGameClientData.Enums
{
    public enum HairCutViewEnums
    {
        Startscreen,
        HairCutView,
        BeardView,
        EyeBrownView,
        MakeupView
    }
}