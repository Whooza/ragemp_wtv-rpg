﻿namespace RolePlayGameClientData.Enums
{
    public enum SmartPhoneViewEnums
    {
        HomeScreenView,
        ContactsView,
        TelephoneView,
        ShortMessagesView,
        MessengerView,
        EMailView,
        BrowserView,
        NoticesView,
        SettingsView,
        LawBookView
    }
}