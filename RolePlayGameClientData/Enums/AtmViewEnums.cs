﻿namespace RolePlayGameClientData.Enums
{
    public enum AtmViewEnums
    {
        StartScreenView,
        PinCodeInputView,
        InProgressView,
        BankBalanceView,
        ByeScreenView,
    }
}