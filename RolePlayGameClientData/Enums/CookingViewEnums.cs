﻿namespace RolePlayGameClientData.Enums
{
    public enum CookingViewEnums
    {
        ShowCategories,
        ShowMeat,
        ShowFish,
        ShowFingerFood,
        ShowSoftDrink
    }
}